<?php
return [
    'site_title' => 'HOMEBAR',
    'role_ids'=>['admin'=>'1', 'user'=>'2', 'vendor'=>'3', 'bartender'=>'4','driver'=>'5'],
    'role_names'=>['1'=>'Admin', '2'=>'User', '3'=>'Vendor', '4'=>'Bartender','5'=>'Driver'],
    'status_arr' => ['inactive' => '0', 'active' => '1'],
    'status_name_arr' => ['1' => 'Active', '0' => 'Inactive'],
    'admin_default_image' => 'public/images/user.jpg',
    'file_save_path' => 'public',
    'admin_page_length' => 10,
    'mail_username' => 'hharley216@gmail.com',
    'setting_field_arr'=> ['site_title','site_logo', 'delivery_charge', 'tax_percent', 'service_fee_percent', 'restaurant_distance_radius', 'premium_user_message', 'tip_percentage','admin_commission','order_min_amount'],
    'default_menu_item_image'=>'images/default_menu_image.jpg',
    'currency_sign' => '$',
    'distance_num'=>'3959', // for miles
    //'distance_num'=>'6371' // for KM,
    'drink_slug'=>'drinks',
    'type_name_arr'=>['PREMADE'=>'Premade', 'DIY'=> 'Do it yourself', 'BARTENDER'=>'Bartender'],
    'plan_types'=>['Monthly'=>'Monthly', 'Yearly'=>'Yearly']

];
