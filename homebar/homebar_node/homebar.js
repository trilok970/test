var express = require('express');
var app = express();
var fs = require("fs");
var http = require('http').Server(app);
// var io = require('socket.io')(http, { pingInterval: 60000 });
var io = require('socket.io')(http);
var request = require('request');
var mysql = require('mysql');
var moment = require('moment');
var bodyParser = require('body-parser')
// const port = process.env.port || 3031;
const port =  5059;
const sever_url = "http://127.0.0.1:" + port;
const client = require('socket.io-client')(sever_url, { query: "user_id=-1" });

var con = mysql.createConnection({
	host: '127.0.0.1',
	user: 'root',
	password: '<nhzIndia@2018>',
	database: 'homebar',
	charset: 'utf8mb4',
	dateStrings: true
});

// Log any errors connected to the db
con.connect(function (err) {
	if (err) console.log(err)
});



var forEach = require('async-foreach').forEach;
var async = require("async");


app.use(bodyParser.urlencoded())



app.get('/driver', function (req, res) {
	res.sendFile(__dirname + '/tests/index.html');
});
app.get('/od', function (req, res) {
	res.sendFile(__dirname + '/tests/other_driver.html');
});

app.post('/assign-driver', function (req, res) {
	client.emit('assign_driver', req.body);
	res.end();
});


var host = 'http://localhost/lara/homebar';
var api_url = host + '/api/';
var img_base_url = host + '/public/';

var order_fields = 'orders.id, orders.user_id, orders.order_number,orders.address_id, orders.address, orders.address_lat, orders.address_lng, orders.delivery_date, orders.order_type, orders.type_id';
var order_items_fields = 'order_items.id, order_items.status, order_items.restaurant_id, order_items.type, order_items.ingredient_id, order_items.menu_item_id';

var sockets = {}



io.on('connection', function (socket) {
	console.log("connected : " + socket.handshake.query.user_id);
	var user_id = socket.handshake.query.user_id;

	if (!sockets[socket.handshake.query.user_id]) {
		sockets[socket.handshake.query.user_id] = [];
	}
	sockets[socket.handshake.query.user_id].push(socket);
	socket.on('assign_driver', (data, cb) => {
		var order_item_id = data.order_item_id;
		var driver_ids = data.driver_ids;
		console.log('driver_ids', driver_ids);
		var sql = "SELECT order_items.id, order_items.restaurant_id, order_items.order_id, order_items.requested_driver_ids FROM order_items WHERE id=" + order_item_id + " LIMIT 1";
		con.query(sql, (err, items_result) => {
			if (err) {
				console.log(err);
			} else {
				var new_order_response = {
					status: true,
					message: 'New order.',
					data: items_result[0]
				};
				driver_ids.forEach(driver_id => {
					console.log('driver_id', driver_id);
					for (var index in sockets[driver_id]) {
						sockets[driver_id][index].emit('new_order_response', new_order_response);
						console.log('new_order_response----------', new_order_response);
					}
				});
			}
		})
	});
	socket.on('location_update', (data, cb) => {
		var lat = data.lat;
		var lng = data.lng;
		var created_at = moment().utc().format('YYYY-MM-DD HH:mm:ss');
		var sql = "SELECT id FROM user_locations where user_id=" + user_id + " LIMIT 1";
		con.query(sql, (err, result) => {
			if (err) {
				console.log(err);
			} else {
				if (typeof result[0] != 'undefined') {
					var sql = "UPDATE user_locations SET lat='" + lat + "', lng='" + lng + "' WHERE id=" + result[0].id;
					var params = [];
				} else {
					var sql = "INSERT INTO user_locations(user_id,lat,lng,created_at,updated_at) values(?,?,?,?,?)";
					var params = [user_id, lat, lng, created_at, created_at];
				}
				con.query(sql, params, (err, result) => {
					if (err) {
						console.log(err);
					} else {
						var location_update_response = {
							status: true,
							message: 'Location updated.',
							data: []
						};
						/*
						console.log('location_update_response', location_update_response);
						socket.emit('location_update_response', location_update_response);
						*/

						cb(location_update_response);
					}
				})
			}
		})
	})
	socket.on('status_update', (data, cb) => {
		var order_id = data.order_id;
		var restaurant_id = data.restaurant_id;
		var status = data.status;
		var sql = "SELECT order_items.id, order_items.restaurant_id, order_items.order_id, order_items.requested_driver_ids FROM order_items WHERE order_id=" + order_id + " AND restaurant_id=" + restaurant_id + " LIMIT 1";
		con.query(sql, (err, items_result) => {
			if (err) {
				console.log(err);
			} else {
				var item_row = items_result[0];
				var remaining_driver_ids_arr = null;
				var requested_driver_ids = item_row.requested_driver_ids;
				var requested_driver_ids_arr = [];
				var index = null;
				if (requested_driver_ids) {
					requested_driver_ids_arr = requested_driver_ids.split(",");
					index = requested_driver_ids_arr.indexOf(user_id);
				}

				var status_field_str = ", status= '" + status + "'";
				if (status == 'ACCEPTED') {
					if (index > -1) {
						requested_driver_ids_arr.splice(index, 1);
						remaining_driver_ids_arr = requested_driver_ids_arr;
					}
					status_field_str = "";
				}

				if (status == 'REJECT') {
					var requested_driver_ids_new = null;
					if (index > -1) {
						requested_driver_ids_arr.splice(index, 1);
						requested_driver_ids_new = requested_driver_ids_arr.join();
						requested_driver_ids_new = (requested_driver_ids_new) ? requested_driver_ids_new : null;
					}

					sql = "UPDATE order_items SET requested_driver_ids='" + requested_driver_ids_new + "' WHERE order_id=" + order_id + " AND restaurant_id=" + restaurant_id;
				} else {
					sql = "UPDATE order_items SET driver_id=" + user_id + ", requested_driver_ids=" + null + status_field_str + " WHERE order_id=" + order_id + " AND restaurant_id=" + restaurant_id;
				}

				con.query(sql, (err, result) => {
					if (err) {
						console.log(err);
					} else {
						if (remaining_driver_ids_arr) {
							var remove_order_response = {
								status: true,
								message: 'Remove items',
								data: item_row
							};
							remaining_driver_ids_arr.forEach(driver_id => {
								console.log('driver_id', driver_id);
								for (var index in sockets[driver_id]) {
									sockets[driver_id][index].emit('remove_order', remove_order_response);
									console.log('remove_order_response----------', remove_order_response);
								}
							});
						}

						if (status == 'DELIVERED') {

							var formdata = {
								form:
								{
									order_id: order_id,
									restaurant_id: restaurant_id
								}
							};
							/*call api*/
							console.log(formdata);


							request.post(api_url + "order-credit-wallet", formdata, function (error, response, body) {
								console.log("error", error);
								console.log("body", body);
								if (!error && response.statusCode == 200) {
									var status_update_response = {
										status: true,
										message: 'Status updated.',
										data: item_row
									};
									cb(status_update_response);

								}
							});
						} else {
							var status_update_response = {
								status: true,
								message: 'Status updated.',
								data: item_row
							};
							cb(status_update_response);

						}


					}
				});

			}
		});




	});


	socket.on('vendor_status_update', (data, cb) => {
		var order_id = data.order_id;
		var restaurant_id = data.restaurant_id;
		var status = data.status;
		var driver_id = data.driver_id;

		var response_data = {
			status: true,
			message: 'Status changed',
			data: data,
		}
		if (driver_id && status == 'READY TO PICKUP') {
			for (var index in sockets[driver_id]) {
				sockets[driver_id][index].emit('new_order_response', response_data);
				console.log('new_order_response----------', response_data);
			}
		}
		cb(response_data);

	});




	socket.on('disconnect', function (eee_dis) {
		console.log('dis error', eee_dis);
	});

	function getOrder(order_id) {
		return new Promise(function (resolve, reject) {

			var sql = "SELECT " + order_fields + " FROM orders WHERE orders.id=" + order_id + " LIMIT 1";
			con.query(sql, (err, result) => {
				if (err) {
					console.log(err);
				} else {
					resolve({ order: result[0] });;
				}
			});

		});
	}
	function getOrderItems(order_id) {
		return new Promise(function (resolve, reject) {

			var sql = "SELECT " + order_items_fields + " FROM order_items WHERE order_items.order_id=" + order_id;
			con.query(sql, (err, result) => {
				if (err) {
					console.log(err);
				} else {
					resolve({ order_items: result });
				}
			});

		});
	}

});



http.listen(port, function () {
	console.log('listening on *:' + port);
});







