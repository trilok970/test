
$(document).ready(function () {
  $('.slider1').owlCarousel({
    loop: true,
    items: 1,
    responsiveClass: true, autoplayHoverPause: true,
    autoplay: true,
    slideSpeed: 800,
    paginationSpeed: 900,
    autoplayTimeout: 3000,
    //navText : ["<img src='img/left.png'>","<img src='img/right.png'>"],
    responsive: {
      0: {
        items: 1,
        nav: false,
      },
      360: {
        items: 2,
        nav: false
      },
      768: {
        items: 4,
        nav: false,
      },
      1000: {
        items: 7,
        nav: false,
        loop: true
      }
    }
  });
});


/* scroll to section id */
$(document).ready(function () {
  $("header a[href^='#']").on('click', function (e) {
    e.preventDefault();
    var hash = this.hash;
    $("a[href^='#']").removeClass('active');
    $(this).addClass('active');
    $('html, body').animate({
      scrollTop: ($(hash).offset().top - 60)
    }, 900, function () {
      window.location.hash = hash;
    });
  });
});




