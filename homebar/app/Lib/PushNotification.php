<?php

namespace App\Lib;

class PushNotification {

    public static function Notify($users, $message, $type = "", $dic = [], $sound = "", $silent = 0) {
        $UserDevicesIOS = [];
        $user_devices = \App\Model\UserDevice::select('user_devices.*')
                ->leftJoin('users', 'users.id', '=', 'user_devices.user_id')
                ->where('users.is_notification_on', '1')
                ->whereIn('device_type', ['IPHONE', 'ANDROID'])
                ->where('device_id', '!=', 'simulator')
                ->whereIn('user_id', $users)
                ->get();
        
        $ios_device_arr = $android_device_arr = [];
        foreach ($user_devices as $key => $row) {
            if ($row->device_type == 'IPHONE') {
                $ios_device_arr[] = $row->device_id;
            } else {
                $android_device_arr[] = $row->device_id;
            }
        }
        if (!empty($ios_device_arr)) {
            PushNotification::sendAlertMsg($ios_device_arr, $message, $dic, $type, $sound, $silent);
        }
        if (!empty($android_device_arr)) {
            PushNotification::sendGcmNotification($android_device_arr, $message, $type, $dic);
        }
    }

    public static function sendAlertMsg($device_tokens, $message, $dictionary = '', $type = '', $sound = '', $silent = 0) {
        $passphrase = 'intel123';
        if (is_array($sound)) {
            $sound = (count($sound)) ? "default" : $sound;
        } else {
            $sound = ($sound == "") ? "default" : $sound;
        }


        $ctx = stream_context_create();
        //$pem = base_path("public/Nsiight_APN_Dev_Certificates.pem");
        $pem = base_path("public/Nsiight_APN_Dis_Certificates.pem");
        stream_context_set_option($ctx, 'ssl', 'local_cert', $pem);
        //$apns_url = 'gateway.sandbox.push.apple.com';
        $apns_url = 'gateway.push.apple.com';

        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        $fp = stream_socket_client('ssl://' . $apns_url . ':2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);



        if (!$fp) {
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        }
        $result_arr = [];
        foreach ($device_tokens as $device_token) {
            
            if ($silent == 1) {
                $body['aps'] = array(
                    'alert' => $message,
                    'dictionary' => $dictionary,
                    'type' => $type,
                    "content-available" => $silent,
                );
            } else {
                $body['aps'] = array(
                    'alert' => $message,
                    'dictionary' => $dictionary,
                    'type' => $type,
                    'sound' => $sound,
                    'badge' => \App\Model\UserDevice::getAndUpdateBadgecount($device_token),
                    "content-available" => $silent,
                );
            }

            $payload = json_encode($body);
            $device_token = str_replace("", "", $device_token);
            $msg = chr(0) . pack('n', 32) . pack('H*', $device_token) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $msg, strlen($msg));
            
            $result_arr[] = $result;
            
        }
        fclose($fp);
        
    }

    public static function sendGcmNotification($user_devices, $message, $type, $dictionary) {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $server_key = 'AAAATYmQi5o:APA91bFDUtqKJ3adM7FPGlbInpb7UjBZb9MLz-lXwC6PD3jNrSSQ5pdkuFpvHso62KOCHf7y0157JLOmy8FZ9bC5WjlT6_i4qxA39sD2qDfrKdR2fz8Mdt8MBzIiPirF4dGBawQXw4tE';
        $ttl = 86400;
        $randomNum = rand(10, 100); 
        $fields = [
            'priority' => "high",
            'notification' => array("title" => "Nsiight", "body" => $message, 'sound' => 'default'),
            'data' => ['message' => $message,
                'type' => $type,
                'title' => 'Nsiight',
                'body' => $message,
                'content_available' => 1,
                'force-start' => 1, 
                'sound'=> 'default',
                'dictionary'=>$dictionary
            ],
            'delay_while_idle' => false,
            'time_to_live' => 86400,
            
            'collapse_key' => "" . $randomNum . ""  ,
            'android'=>['ttl'=>'86400s','notification'=>['click_action'=>'OPEN_ACTIVITY_1']]
        ];

        
        foreach ($user_devices as $device_token) {
            $fields['to'] = $device_token;
           
            $fields['data']['badge'] = \App\Model\UserDevice::getAndUpdateBadgecount($device_token);
            
           // dd($fields);
            $headers = array(
                'Content-Type:application/json',
                'Authorization:key=' . $server_key
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
        }
    }

}
