<?php

namespace App\Lib;

class StripePayment {

    public $secret_key = '';
    
    function __construct() {
        $this->secret_key = env('STRIPE_SECRET_KEY', '');
        \Stripe\Stripe::setApiKey($this->secret_key);
    }

    public static function getCurrency(){
        return 'USD';
    }
  
    public static function createCustomer($user) 
    {
        $customer = \Stripe\Customer::create(array(
            "description" => "User ID : ".$user->id."Customer Name : " . $user->first_name." ".$user->first_name,
            "phone" => $user->phone_country_code,
            'email'=>$user->email
                  
        ));
        return $customer;
    }

    public function retrieveAccount($stripe_account){
        $response = \Stripe\Account::retrieve($stripe_account);
        return $response;
    }

    
    public static function createCharge($amount, $card_key, $extra_params=[]) {
        $charges = \Stripe\Charge::create([
            "amount" => $amount,
            "currency" => self::getCurrency(),
            "source" => $card_key, // obtained with Stripe.js
            "description" => isset($extra_params['description']) ? $extra_params['description'] : '',
            "customer" => $extra_params['customer']
        ]);
        return $charges;
    }

    public function transfer($amount, $account ){
        $response = \Stripe\Transfer::create([
            'amount' => $amount,
            'currency' => self::getCurrency(),
            'destination' => $account
        ]);
        return $response;
    }

    public function refund($charge_id, $amount){
        $response = \Stripe\Refund::create([
            'charge' => $charge_id,
            'amount'=> $amount
        ]);
        return $response;
    }


    
}
