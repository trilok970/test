<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        try{
            $token = JWTAuth::getToken();
            
            //$user = JWTAuth::toUser($request->input('token'));
            $user = JWTAuth::toUser($token);
            if(empty($user)){
                return response()->json(['status'=>false,'status_code'=>401,'error'=>'User not found'], 401);
            }elseif(empty($user->status) || $user->status == 0){
                $message = ($user->status == 0) ? 'You have been marked as inactive by the administrator' : 'User not found';
                return response()->json(['status'=>false,'status_code'=>401,'error'=>$message], 401);
            }
        }catch (JWTException $e) {
            if($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json(['status'=>false,'error'=>'token_expired']);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json(['status'=>false,'status_code'=>401,'error'=>'token_invalid'], 401);
            }else{
                return response()->json(['status'=>false,'status_code'=>401,'error'=>'Token is required'], 401);
            }
        }
        return $next($request);
    }
}
