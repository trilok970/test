<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\Category;
use App\Model\BrandCategory;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use App\Lib\Uploader;
use Config;

class BrandsController extends Controller
{
    public function index()
    {
        $title_page = 'Brands';
        $breadcumb = [$title_page=>''];
        return view('admin.brands.index', compact('title_page', 'breadcumb'));
    }

    public function datatable(Request $request)
    {
        $columns = ['id', 'name', 'status', 'created_at', 'action'];

        $totalData = Brand::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $dir = $request->input('order.0.dir');
        $order = $columns[$request->input('order.0.column')];


        $results = Brand::select('brands.*');

        if (!empty($request->input('search.value'))) {
            $search = trim($request->input('search.value'));
            $date = explode('-',$search);
            try{
                if(count($date)==2) {
                    $date=$date[1].'-'.$date[0];
                }
                else if(count($date)==1) {
                    $date=$date[0];
                }
                else    {
                    $date=$date[2].'-'.$date[0].'-'.$date[1];
                }
            }
            catch(Exception $e){
                $date='';
            }
            $results = $results->where(function ($query) use ($search,$date) {
                $query->where('name', 'LIKE', "%{$search}%")
                    ->orWhere('updated_at', 'LIKE', "%".$date."%")
                    ->orWhere('slug', 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $results->count();
        $results = $results->offset($start)->limit($limit)->orderBy($order, $dir)->get();

        $status_name_arr = Config::get('params.status_name_arr');
        $data = array();
        if (!empty($results)) {
            foreach ($results as $row) {
                $nestedData['id'] = null;
                $nestedData['name'] = $row->name;
                $nestedData['status'] = isset($status_name_arr[$row->status]) ? $status_name_arr[$row->status] : '-';
                $nestedData['created_at'] = listDateFromat($row->updated_at);
                $buttons = [];
                $buttons[] = ['key' => 'edit', 'link' => route('brands.edit', $row->id)];
                $buttons[] = ['key' => 'delete', 'link' => route('brands.destroy', $row->id)];
                $nestedData['action'] = getButtons($buttons);
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function create()
    {
        $title_page = 'Add Brand';
        $breadcumb = ['Brands'=>route('admin.types.index'), 'Add'=>''];

        $entity = new Brand;

        $categories = Category::getCategory();
        $already_selected_categories = [];

        return view('admin.brands.create', compact('breadcumb', 'title_page', 'entity', 'categories', 'already_selected_categories'));
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ]);
            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }

            $entity = new Brand();
            $entity->name = $request->name;
            $entity->status = $request->status;
            $entity->save();

            $curr_date = date('Y-m-d H:i:s');
            $brand_id = $entity->id;
            $to_be_inserted = $request->categories;
            if(!empty($to_be_inserted)) {
                $insert_data= [];
                foreach($to_be_inserted as $key => $insert_id) {
                    $insert_data[] = [
                        'brand_id'=> $brand_id,
                        'category_id'=> $insert_id,
                        'created_at'=> $curr_date,
                        'updated_at'=> $curr_date
                    ];
                }
                BrandCategory::insert($insert_data);
            }
            Session::flash('success', 'Brand has been added successfully.');
            return redirect()->route('brands.index');
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('error', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function show($id)
    {
    }

    public function edit(Request $request, $id)
    {
        try {
            $entity = Brand::where('id', $id)
            ->first();
            if (empty($entity)) {
                abort(404);
            }
            $title_page = 'Edit Brand';
            $breadcumb = ['Brands'=>route('brands.index'), $title_page=>''];
            $categories = Category::getCategory();
            $already_selected_categories = BrandCategory::getSelectedCategory($id);
            return view('admin.brands.create', compact('breadcumb', 'title_page', 'entity', 'categories', 'already_selected_categories'));
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $entity = Brand::where('id', $id)->first();
            if (empty($entity)) {
                abort(404);
            }
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);
            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }
            $entity->name = $request->name;
            $entity->status = $request->status;
            $entity->save();

            $brand_id = $entity->id;
            $curr_date = date('Y-m-d H:i:s');
            $already_selected_categories = BrandCategory::getSelectedCategory($id);
            $selected_categories = !empty($request->categories) ? $request->categories : [];


            $to_be_inserted = $selected_categories;
            if(!empty($already_selected_categories)){
                $to_be_deleted = array_diff($already_selected_categories, $selected_categories);
                $to_be_inserted = array_diff($selected_categories, $already_selected_categories);
                if(!empty($to_be_deleted)) {
                    BrandCategory::whereIn('category_id', $to_be_deleted)->delete();
                }

            }
            if(!empty($to_be_inserted)) {
                $insert_data= [];

                foreach($to_be_inserted as $key => $insert_id){
                    $insert_data[] = [
                        'brand_id'=> $brand_id,
                        'category_id'=> $insert_id,
                        'created_at'=> $curr_date,
                        'updated_at'=> $curr_date
                    ];
                }
                BrandCategory::insert($insert_data);
            }

            Session::flash('success', 'Brand has been updated successfully.');
            return redirect()->route('brands.index');
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function destroy(Request $request, $id)
    {
        try {
            $row = Brand::where('id', $id)->delete();
            Session::flash('success', 'Brand has been deleted successfully.');
            return redirect()->back();
        } catch (\Exception $e) {
            Session::flash('warning', 'Invalid Request');
            return redirect()->back();
        }
    }
}
