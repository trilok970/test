<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config;
use App\Model\Order;
use App\Model\OrderItem;
use App\Model\Type;
use DB;

class OrderController extends Controller {

    public function getTotalOrderCount($type_id)
    {

        // $items = OrderItem::groupBy('restaurant_id')->having('status','NEW')->get(['restaurant_id',DB::raw("count(*) as order_count") ,'status']);
        // $data['NEW'] =  array_sum($items->pluck('order_count')->toArray());

        // $items = OrderItem::groupBy('restaurant_id')->having('status',['ACCEPTED','IN KITCHEN','READY TO PICKUP','START FOR DELIVERY'])->get(['restaurant_id',DB::raw("count(*) as order_count") ,'status']);
        // $data['In Progress'] =  array_sum($items->pluck('order_count')->toArray());

        // $items = OrderItem::groupBy('restaurant_id')->having('status','REJECTED')->get(['restaurant_id',DB::raw("count(*) as order_count") ,'status']);
        // $data['REJECTED'] =  array_sum($items->pluck('order_count')->toArray());

        // $items = OrderItem::groupBy('restaurant_id')->having('status','DELIVERED')->get(['restaurant_id',DB::raw("count(*) as order_count") ,'status']);
        // $data['COMPLETED'] =  array_sum($items->pluck('order_count')->toArray());

        // return $data;

        $restaurantOrders = OrderItem::groupBy(['restaurant_id','status'])->where('bartender_id', '=', null)->whereHas('order', function($q) use ($type_id) {
            $q->when($type_id,function($query) use ($type_id) {
                $query->where('type_id',$type_id);
            });
        })->get(['restaurant_id',DB::raw("count(*) as order_count") ,'status']);
        $result = ['NEW' => 0,'IN_PROCESS' => 0,'REJECTED' => 0, 'COMPLETED' => 0];

        foreach($restaurantOrders as $order) {
            switch ($order->status) {
                case 'NEW':
                    $result['NEW'] += $order->order_count;
                    break;
                case 'DELIVERED':
                case 'PICKED UP':
                    $result['COMPLETED'] += $order->order_count;
                    break;
                case 'REJECTED':
                case 'CANCELLED':
                    $result['REJECTED'] += $order->order_count;
                    break;
                default:
                    $result['IN_PROCESS'] += $order->order_count;
                    break;
            }
        }

        return $result;
    }


    public function index(Request $request)
    {
        $role = 3;

        $role_names = Config::get('params.role_names');

        if($request->type_id)
            $type_id = $request->type_id;
        else
            $type_id = 0;
        if($type_id) {
            $type = Type::where('id', $type_id)->first();
            $type = ucfirst($type->name);
            $title_page = "Orders ($type)";

        }
        else {
            $title_page = "Orders";
        }
        $breadcumb = [$title_page => ''];

         $statusCountResult  = $this->getTotalOrderCount($type_id);
         $colors = ['aqua','yellow','red','green'];
         $icons = ['first-order','clock-o','times-circle','check'];
        return view('admin.orders.index',compact('title_page','breadcumb', 'role', 'type_id','statusCountResult','colors','icons'));
    }

    public function datatables(Request $request)
    {
        $columns = ['id', 'order_number', 'user', 'total_amount','order_type','reach_type','delivery_date', 'created_at','status', 'action'];
        $conditions = [];



        if($request->user_id){
            $user_id = $request->user_id;
        //    $totalData = Order::select('id')->where(['user_id'=>$request->user_id])->count();
        //    $results = Order::where(['user_id'=>$request->user_id])->with('user');

        }
        else {
            $user_id = 0;
        }
        if($request->type_id > 0)
        {
            $type_id = $request->type_id;
        //    $totalData = Order::select('id')->where(['type_id'=>$request->type_id])->count();
        //    $results = Order::where(['type_id'=>$request->type_id])->with('user');
        }
        else {
            $type_id = 0;
        }

        $totalData = Order::select('id')
        ->when($user_id,function($q)use ($user_id) {
            $q->where('user_id',$user_id);
        })
        ->when($type_id,function($q1)use ($type_id) {
            $q1->where('type_id',$type_id);
        })
        ->where('type_id', '!=', 4)
        ->count();
        $results = Order::when($user_id,function($q)use ($user_id) {
            $q->where('user_id',$user_id);
        })
        ->when($type_id,function($q1)use ($type_id) {
            $q1->where('type_id',$type_id);
        })
        ->where('type_id', '!=', 4)
        ->with('user');



        $limit = $request->input('length');
        $start = $request->input('start');
        $dir = $request->input('order.0.dir');
        $order = $columns[$request->input('order.0.column')];




        if (!empty($request->input('search.value'))) {
            $search = trim($request->input('search.value'));
            $date = explode('-',$search);
            try{
                if(count($date)==2) {
                    $date=$date[1].'-'.$date[0];
                }
                else if(count($date)==1) {
                    $date=$date[0];
                }
                else    {
                    $date=$date[2].'-'.$date[0].'-'.$date[1];
                }
            }
            catch(Exception $e){
                $date='';
            }
            $results = $results->where(function ($query) use ($search,$date) {
                $query->where('order_number', 'LIKE', "%{$search}%")
                ->orWhere('reach_type','LIKE',"%{$search}%")
                ->orWhere('order_type','LIKE',"%{$search}%")
                ->orWhere('updated_at', 'LIKE', "%".$date."%")
                ->orWhere('status','LIKE',"%{$search}%");
            });
        }

        $totalFiltered = $results->count();
        $results = $results->orderBy($order,$dir)->skip($start)->take($limit)->get();

        // return $status_name_arr = Config::get('params.status_name_arr');
        $data = array();
        if (!empty($results)) {
            foreach ($results as $row) {

                $nestedData['id'] = $row->id;
                $nestedData['order_number'] = '<a href="'.route('admin.orders.show',$row->id).'">'.$row->order_number.'</a>';
                $nestedData['user'] = isset($row->user)?ucfirst($row->user->first_name).' '.ucfirst($row->user->last_name):'-';
                $nestedData['total_amount'] = '$'.$row->total_amount;
                $nestedData['order_type'] = $row->order_type;
                $nestedData['reach_type'] = $row->reach_type;
                $nestedData['delivery_date'] = listDateFromat($row->delivery_date);
                $nestedData['created_at'] = listDateFromat($row->updated_at);
                $nestedData['status'] = $row->status;
                $buttons = [];

                $buttons[] = ['key' => 'view', 'link' => route('admin.orders.show',$row->id), 'title'=>'View'];
                $nestedData['action'] = getButtons($buttons);
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function show(Request $request,Order $order)
    {
        $title_page = 'Order Detail';
        $breadcumb = [$title_page => route('admin.orders.index'),$order->order_number =>''];
        return view('admin.orders.show',compact('title_page','breadcumb','order'));
    }
}
