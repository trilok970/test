<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ContactRequest;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use App\Lib\Uploader;
use \Config;

class ContactRequestsController extends Controller {

    public function index() {
        $title_page = 'Contact Requests';
        $breadcumb = [$title_page => ''];
        return view('admin.contact_requests.index', compact('title_page','breadcumb'));
    }

    public function datatable(Request $request) {

        $columns = ['id', 'name', 'faq_category', 'message','image', 'created_at', 'action'];
        $totalData = ContactRequest::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $dir = $request->input('order.0.dir');
        $order = $columns[$request->input('order.0.column')];

        //echo $order.'----'. $dir; die;
        $results = ContactRequest::with(['user', 'faqCategory']);

        if (!empty($request->input('search.value'))) {
            $search = trim($request->input('search.value'));
            $date = explode('-',$search);
            try{
                if(count($date)==2) {
                    $date=$date[1].'-'.$date[0];
                }
                else if(count($date)==1) {
                    $date=$date[0];
                }
                else    {
                    $date=$date[2].'-'.$date[0].'-'.$date[1];
                }
            }
            catch(Exception $e){
                $date='';
            }

            $results = $results->where(function($query) use ($search,$date) {
                $query->where('message', 'LIKE', "%{$search}%")
                ->orWhereHas('user', function ($query) use ($search,$date) {
                    $query->where('first_name', 'LIKE', "%{$search}%")
                    ->orWhere('last_name', 'LIKE', "%{$search}%")
                    ->orWhere('email', 'LIKE', "%{$search}%")
                    ->orWhere('updated_at', 'LIKE', "%".$date."%")
                    ->orWhere('phone', 'LIKE', "%{$search}%");

                })
                ->orWhereHas('faqCategory', function ($query) use ($search) {
                    $query->where('name', 'LIKE', "%{$search}%");
                });
            });
        }

        $totalFiltered = $results->count();
        $results = $results->offset($start)->limit($limit)->orderBy($order, $dir)->get();

        $data = array();
        if (!empty($results)) {
            foreach ($results as $row) {
                $nestedData['id'] = null;
                $nestedData['name'] = isset($row->user->first_name) ? $row->user->first_name.' '. $row->user->last_name : '-';
                $nestedData['faq_category'] = isset($row->faqCategory->name) ? $row->faqCategory->name : '-';
                $nestedData['message'] = strShort($row->message);
                if($row->image) {
                    $nestedData['image'] = '<a href="'.$row->image.'" target="_blank" ><img src="'.$row->image.'" width="50" /></a>';
                }
                else {
                    $nestedData['image'] = '';
                }
                $nestedData['created_at'] = listDateFromat($row->updated_at);

                $nestedData['action'] = getButtons([
                    ['key' => 'view', 'link' => route('admin.contactRequests.view', $row->id)],
                    ['key' => 'delete', 'link' => route('admin.contactRequests.delete', $row->id)]
                ]);
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }


    public function view($id) {

        $title_page = 'View Contact Request';
        $breadcumb = ['Contact Requests'=> route('admin.contactRequests.index'), $title_page => ''];
        $entity = ContactRequest::with(['user.countryData', 'faqCategory'])
        ->where('id', $id)
        ->first();
        if(empty($entity->id)){
            abort(404);
        }
        return view('admin.contact_requests.view', compact('title_page','breadcumb', 'entity'));
    }

    public function destroy(Request $request, $id) {
        try {
            $row = ContactRequest::where('id', $id)->delete();
            Session::flash('success', 'Contact request has been deleted successfully.');
            return redirect()->back();
        } catch (\Exception $e) {
            Session::flash('warning', 'Invalid Request');
            return redirect()->back();
        }
    }


}
