<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Setting;
use DB;
use Illuminate\Support\Facades\Input;
use Session;
use Illuminate\Support\Facades\Validator;
use Config;
use Illuminate\Support\Facades\Auth;
use App\Lib\Uploader;


class SettingsController extends Controller {


    public function settings(){
        try {
            $setting_field_arr = Config::get('params.setting_field_arr');
            $site_settings = Setting::whereIn('slug', $setting_field_arr)->get();

            $title_page = 'Settings';
            $breadcumb = [$title_page => ''];
            return view('admin.settings.settings',compact('title_page', 'breadcumb', 'site_settings'));
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back()->withInput();
        }
    }// end function.


    public function settingsUpdate(Request $request){
        try {
            $data = $request->all();
            $setting_field_arr = Config::get('params.setting_field_arr');
            $settings_sess = Session::get('settings');
            $site_settings_types = Setting::whereIn('slug', $setting_field_arr)
            ->pluck('type', 'slug')
            ->toArray();
            foreach($setting_field_arr as $slug){
                if(isset($data[$slug])) {
                    $value_save = null;
                    if($site_settings_types[$slug] == 'FILE') {

                        if ($request->file($slug)) {
                            $destinationPath = '/uploads/others/';
                            $response_data = Uploader::doUpload($request->file($slug), $destinationPath);
                            if ($response_data['status'] == true) {
                                $value_save = $response_data['file'];
                            }
                        }
                    } else {
                        $value_save = $data[$slug];
                    }
                    if($value_save !== null){
                        Setting::where('slug', $slug)->update(['value'=>$value_save]);
                        if(isset($settings_sess[$slug])){
                            $settings_sess[$slug] = $value_save;
                        }
                    }

                }
            }
            Session::put('settings', $settings_sess);

            Session::flash('success', 'Settings has been updated succesfully.');
            // return redirect()->route('admin.dashboard');
            return redirect()->back();

        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back()->withInput();
        }
    }



}
