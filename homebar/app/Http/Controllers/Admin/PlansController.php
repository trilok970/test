<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Plan;
use App\Model\Category;
use App\Model\BrandCategory;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use App\Lib\Uploader;
use Config;

class PlansController extends Controller
{
    public function index()
    {
        $title_page = 'Plans';
        $breadcumb = [$title_page=>''];
        return view('admin.plans.index', compact('title_page', 'breadcumb'));
    }

    public function datatable(Request $request)
    {
        $columns = ['id', 'name', 'status', 'created_at', 'action'];

        $totalData = Plan::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $dir = $request->input('order.0.dir');
        $order = $columns[$request->input('order.0.column')];


        $results = Plan::select('plans.*');

        if (!empty($request->input('search.value'))) {
            $search = trim($request->input('search.value'));
            $date = explode('-',$search);
            try{
                if(count($date)==2) {
                    $date=$date[1].'-'.$date[0];
                }
                else if(count($date)==1) {
                    $date=$date[0];
                }
                else    {
                    $date=$date[2].'-'.$date[0].'-'.$date[1];
                }
            }
            catch(Exception $e){
                $date='';
            }
            $results = $results->where(function ($query) use ($search,$date) {
                $query->where('name', 'LIKE', "%{$search}%")
                ->orWhere('updated_at', 'LIKE', "%".$date."%");
            });
        }

        $totalFiltered = $results->count();
        $results = $results->offset($start)->limit($limit)->orderBy($order, $dir)->get();

        $status_name_arr = Config::get('params.status_name_arr');
        $data = array();
        if (!empty($results)) {
            foreach ($results as $row) {
                $nestedData['id'] = null;
                $nestedData['name'] = $row->name;
                $nestedData['status'] = isset($status_name_arr[$row->status]) ? $status_name_arr[$row->status] : '-';
                $nestedData['created_at'] = listDateFromat($row->updated_at);
                $buttons = [];
                $buttons[] = ['key' => 'edit', 'link' => route('plans.edit', $row->id)];
                $buttons[] = ['key' => 'delete', 'link' => route('plans.destroy', $row->id)];
                $nestedData['action'] = getButtons($buttons);
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function create()
    {
        $title_page = 'Add Plan';
        $breadcumb = ['Plans'=>route('plans.index'), 'Add'=>''];

        $entity = new Plan;


        return view('admin.plans.create', compact('breadcumb', 'title_page', 'entity'));
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'status' => 'required',
                'type' => 'required',
                'price' => 'required'
            ]);
            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }

            $entity = new Plan();
            $entity->name = $request->name;
            $entity->status = $request->status;
            $entity->type = $request->type;
            $entity->price = $request->price;
            $entity->save();
            Session::flash('success', 'Plan has been added successfully.');
            return redirect()->route('plans.index');
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('error', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function show($id)
    {
    }

    public function edit(Request $request, $id)
    {
        try {
            $entity = Plan::where('id', $id)
            ->first();
            if (empty($entity)) {
                abort(404);
            }
            $title_page = 'Edit Plan';
            $breadcumb = ['Plans'=>route('plans.index'), $title_page=>''];
            return view('admin.plans.create', compact('breadcumb', 'title_page', 'entity'));
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $entity = Plan::where('id', $id)->first();
            if (empty($entity)) {
                abort(404);
            }
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'status' => 'required',
                'type' => 'required',
                'price' => 'required'
            ]);
            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }
            $entity->name = $request->name;
            $entity->status = $request->status;
            $entity->type = $request->type;
            $entity->price = $request->price;
            $entity->save();

            Session::flash('success', 'Plan has been updated successfully.');
            return redirect()->route('plans.index');
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function destroy(Request $request, $id)
    {
        try {
            $row = Plan::where('id', $id)->delete();
            Session::flash('success', 'Plan has been deleted successfully.');
            return redirect()->back();
        } catch (\Exception $e) {
            Session::flash('warning', 'Invalid Request');
            return redirect()->back();
        }
    }
}
