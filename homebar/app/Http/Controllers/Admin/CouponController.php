<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Coupon;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use App\Lib\Uploader;
use Config;

class CouponController extends Controller
{

    public function index()
    {
        $title_page = 'Coupon';
        $breadcumb = ['Coupon'=>route('admin.coupons.index')];

        return view('admin.coupons.index', compact('title_page', 'breadcumb'));
    }

    public function datatable(Request $request)
    {
        $columns = ['id', 'coupon_type', 'coupon_title', 'coupon_code', 'expiry_date', 'status',  'created_at', 'action'];
        $conditions = [];

        $totalData = Coupon::select('id')->where('status','1')->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $dir = $request->input('order.0.dir');
        $order = $columns[$request->input('order.0.column')];


        $results = Coupon::where('status','1');

        if (!empty($request->input('search.value'))) {
            $search = trim($request->input('search.value'));
            $date = explode('-',$search);
            try{
                if(count($date)==2) {
                    $date=$date[1].'-'.$date[0];
                }
                else if(count($date)==1) {
                    $date=$date[0];
                }
                else    {
                    $date=$date[2].'-'.$date[0].'-'.$date[1];
                }
            }
            catch(Exception $e){
                $date='';
            }
            $results = $results->where(function ($query) use ($search,$date) {
                $query->where('coupon_title', 'LIKE', "%{$search}%")
                        ->orWhere('updated_at', 'LIKE', "%".$date."%")
                        ->orWhere('expiry_date', 'LIKE', "%".$date."%")
                        ->orWhere('coupon_code', 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $results->count();
        $results = $results->offset($start)->limit($limit)->orderBy($order, $dir)->get();

        $status_name_arr = Config::get('params.status_name_arr');
        $data = array();
        if (!empty($results)) {
            foreach ($results as $row) {

                $nestedData['id'] = $row->id;
                $nestedData['coupon_title'] = $row->coupon_title;
                $nestedData['coupon_type'] = $row->coupon_type;
                $nestedData['coupon_code'] = $row->coupon_code;
                $nestedData['expiry_date'] = listDateFromat($row->expiry_date);

                $nestedData['status'] = isset($status_name_arr[$row->status]) ? $status_name_arr[$row->status] : '-';

                $nestedData['created_at'] = listDateFromat($row->updated_at);
                $buttons = [];

                $buttons[] = ['key' => 'view', 'link' => route('admin.coupons.view',$row->id), 'title'=>'View'];

                $buttons[] = ['key' => 'edit', 'link' => route('admin.coupons.edit', $row->id)];
                $buttons[] = ['key' => 'delete', 'link' => route('admin.coupons.destroy', $row->id)];

                $nestedData['action'] = getButtons($buttons);
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function create()
    {
        $title_page = 'Add New Coupon';
        $breadcumb = ['Coupon'=>route('admin.coupons.index')];

        $breadcumb['Add'] = '';
        $entity = null;

        $coupon_type = ['Flat' => 'Flat', 'Percentage' => 'Percentage'];

        return view('admin.coupons.create', compact('breadcumb', 'title_page','entity','coupon_type'));
    }

    public function store(Request $request)
    {
        try {

            $data = $request->all();

            $validator = Validator::make($request->all(), [
                'coupon_type' => 'required',
                'coupon_title' => 'required',
                'coupon_code' => 'required',
                'coupon_value' => 'nullable',
                'expiry_date' => 'required|date|after_or_equal:today'
            ]);

            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }

            $entity = new Coupon();

            $entity->coupon_type = $request->coupon_type;
            $entity->coupon_title = $request->coupon_title;
            $entity->coupon_code = $request->coupon_code;
            $entity->coupon_value = $request->coupon_value;
            $entity->expiry_date = $request->expiry_date;

            $entity->save();


            Session::flash('success', 'Coupon has been added successfully.');
            return redirect()->route('admin.coupons.index');
        } catch (\Exception $e) {
            return $msg = $e->getMessage();
            Session::flash('error', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function view($id)
    {
        $entity = Coupon::where('id',$id)->first();

        $title_page = 'Coupon';
        $breadcumb = ['Coupon'=>route('admin.coupons.index')];

        $breadcumb['View'] = '';

        return view('admin.coupons.view', compact('breadcumb', 'title_page','entity'));
    }


    public function edit(Request $request, $id)
    {
        try {

            $entity = Coupon::where('id', $id)->first();

            if (empty($entity)){
                abort(404);
            }

            $title_page = 'Edit Coupon';
            $breadcumb = ['Coupon'=>route('admin.coupons.index')];

            $breadcumb['Edit'] = '';

            $coupon_type = ['Flat' => 'Flat', 'Percentage' => 'Percentage'];

            //dd($entity);

            return view('admin.coupons.create', compact('breadcumb', 'title_page', 'entity','coupon_type'));

        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try {

            $data = $request->all();

            $entity = Coupon::where('id', $id)->first();
            if (empty($entity)) {
                abort(404);
            }

            $validator = Validator::make($request->all(), [
                'coupon_type' => 'required',
                'coupon_title' => 'required',
                'coupon_code' => 'required',
                'coupon_value' => 'nullable',
                'expiry_date' => 'required|date|after_or_equal:today'
            ]);

            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }

            $entity->coupon_type = $request->coupon_type;
            $entity->coupon_title = $request->coupon_title;
            $entity->coupon_code = $request->coupon_code;
            $entity->coupon_value = $request->coupon_value;
            $entity->expiry_date = $request->expiry_date;

            $entity->save();


            Session::flash('success', 'Coupon has been updated successfully.');
            return redirect()->route('admin.coupons.edit',$id);
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function destroy(Request $request, $id)
    {
        try {

            Coupon::where('id', $id)->delete();

            Session::flash('success', 'Coupon has been deleted successfully.');
            return redirect()->back();
        } catch (\Exception $e) {
            Session::flash('warning', 'Invalid Request');
            return redirect()->back();
        }
    }
}
