<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use DB;
use Illuminate\Support\Facades\Input;
use Session;
use Illuminate\Support\Facades\Validator;
use Config;
use Illuminate\Support\Facades\Auth;
use App\Lib\Uploader;
use Illuminate\Support\Facades\Hash;
use App\Model\Type;

class VendorsController extends Controller {

    public function vendors() {
        $role = 3;

        $role_names = Config::get('params.role_names');
        $role_name = $role_names[$role];
        $title_page = $role_name.'s';
        $breadcumb = [$title_page=>''];
        //Session::flash('success', 'User has been deleted successfully.');

        return view('admin.vendors.index',compact('title_page','breadcumb', 'role'));
    }



    public function usersDataTable(Request $request) {
        $role = 3;
        $columns = ['id','first_name','last_name', 'email', 'profile_image', 'created_at', 'status', 'action'];
        $role_ids = Config::get('params.role_ids');
        $totalData = User::where('role', $role)->count();
        $limit = $request->input('length');
        $start = $request->input('start');

        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $users = User::where('role',  $role);

        if(!empty($request->input('search.value'))) {
            $search = trim($request->input('search.value'));
            $date = explode('-',$search);
            try{
                if(count($date)==2) {
                    $date=$date[1].'-'.$date[0];
                }
                else if(count($date)==1) {
                    $date=$date[0];
                }
                else    {
                    $date=$date[2].'-'.$date[0].'-'.$date[1];
                }
            }
            catch(Exception $e){
                $date='';
            }

            $users = $users->where(function($query) use ($search,$date) {
                $query->where('full_name', 'LIKE', "%{$search}%")
                        ->orWhere('email', 'LIKE', "%{$search}%")
                        ->orWhere('phone', 'LIKE', "%{$search}%")
                        ->orWhere('updated_at', 'LIKE', "%".$date."%")
                        ->orWhere('status', 'LIKE', "%{$search}%");
            });
        }


        $totalFiltered = $users->count();
        $users = $users->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();


        $data = [];
        if (!empty($users)) {
            foreach ($users as $key => $row) {
                $nestedData['id'] = null;
                $nestedData['first_name'] = ucfirst($row->first_name);
                $nestedData['last_name'] = ucfirst($row->last_name);
                $nestedData['email'] = $row->email;
                $nestedData['profile_image'] = !empty($row->profile_image_full) ? '<img src="'.$row->profile_image_full.'" width="50"/>' : '';
                $nestedData['created_at'] = listDateFromat($row->updated_at);
                $nestedData['status'] = getStatus($row->status,$row->id);
                $buttons = [['key'=>'view','link'=>route('admin.vendors.show',$row->id)],
                    //['key'=>'edit','link'=>route('chefs.edit',$row->id)],
                ];


                $buttons[] = ['key'=>'delete','link'=>route('admin.vendors.destroy',$row->id)];
                $nestedData['action'] =  getButtons($buttons);

                $data[] = $nestedData;
            }
        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function statusUpdate(Request $request) {
        $user_id = $request->id;
        $row = User::whereId($user_id)->first();
        $row->status = ($row->status == '0') ? '1' : '0';
        $row->reason_for_deactivation = $request->reason_for_deactivation;
        $row->save();
        $html = '';
        switch ($row->status) {
            case '0':
                $html = '<a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Penidng" onClick="changeStatus(' . $user_id . ','.$row->status.')" >Inactive</a>';
                break;
            case '1':
                $html = '<a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeStatus(' . $user_id . ','.$row->status.')" >Active</a>';
                break;

            default:

                break;
        }
        return $html;
    }// end function.


    public function show($id){
         try {
            $entity = User::where(['id'=>$id])->first();

            if ($entity) {

                $bartenders = User::where(['vendor_id'=>$id,'role' => 4])->get();

                $role_ids = Config::get('params.role_ids');
                $role_names = Config::get('params.role_names');
                $role = $entity->role;
                $role_name = $role_names[$role];
                $role_title = $role_name.'s';
                $btypes = '-';
                if(!empty($entity->business_type)) {
                    $types = explode(",",$entity->business_type);
                    $btypes =  Type::whereIn('id',$types)->pluck('name')->toArray();
                    $btypes = implode(", ",$btypes);
                }
                $title_page = "$role_name Details";
                $breadcumb = [$role_title => route('admin.vendors', ['role'=>$role]), $title_page => ''];
                return view('admin.vendors.show', compact('title_page', 'entity', 'breadcumb','bartenders','btypes'));
            } else {
                Session::flash('warning', 'Invalid request');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back()->withInput();
        }
    }


    public function destroy($id) {
        try {
            $role_ids = Config::get('params.role_ids');

            $user = User::where('id', $id)->first();
            if($user && $user->role == $role_ids['admin']){
                abort(404);
            } else {
                User::destroy($id);
                Session::flash('success', 'User has been deleted successfully.');
                return back();
            }

        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back();
        }
    }// end function.


}
