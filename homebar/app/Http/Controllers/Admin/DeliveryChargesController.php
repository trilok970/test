<?php

namespace App\Http\Controllers\Admin;

use App\Model\DeliveryCharges;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Setting;
use DB;

class DeliveryChargesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title_page = 'Delivery Charges';
        $breadcumb = ['Delivery Charges'=>route('admin.delivery-charges.index')];
        $deliveryCharges = DeliveryCharges::all();
        $setting = Setting::where('slug','high_volume_time_charges')->first();
        return view('admin.deliveryCharges.index', compact('title_page', 'breadcumb','deliveryCharges','setting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            DB::table('delivery_charges')->delete();
            $charges = [];
            $index = 0;
            foreach($request->time_from as $timeFrom) {
                $chargeRow = ['time_from' => $request->time_from[$index],'time_to' => $request->time_to[$index],'delivery_charges' => $request->delivery_charges[$index],'is_high_volume_time' => $request->is_high_volume[$index]];
                array_push($charges,$chargeRow);
                $index++;
            }
            $result = DB::table('delivery_charges')->insert($charges);
            $setting = Setting::where('slug','high_volume_time_charges')->first();
            if($setting) {
                $setting->slug = 'high_volume_time_charges';
                $setting->name = 'High Volume Time Charges';
                $setting->value = $request->high_volume_price;
                $setting->type = 'NUMBER';
                $setting->save();
            }else {
                Setting::create([
                    'slug' => 'high_volume_time_charges',
                    'name' => 'High Volume Time Charges',
                    'value' => $request->high_volume_price,
                    'type' => 'NUMBER',
                ]);
            }
            DB::commit();
            $request->session()->flash('success','Delivery Charges Updated');
        } catch(Exception $e) {
            $request->session()->flash('danger',$e->getMessage());
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DeliveryCharges  $deliveryCharges
     * @return \Illuminate\Http\Response
     */
    public function show(DeliveryCharges $deliveryCharges)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DeliveryCharges  $deliveryCharges
     * @return \Illuminate\Http\Response
     */
    public function edit(DeliveryCharges $deliveryCharges)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DeliveryCharges  $deliveryCharges
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeliveryCharges $deliveryCharges)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeliveryCharges  $deliveryCharges
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeliveryCharges $deliveryCharges)
    {
        //
    }
}
