<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\DoItYourself;
use App\Model\DoItYourselfIngredient;
use App\Model\Type;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use App\Lib\Uploader;
use Config;

class DoItYourselfController extends Controller
{

public function index(Request $request)
    {
        $title_page = 'Do It Youself';
        $breadcumb = ['Do It Youself'=>route('admin.do_it_yourself.index')];

        if($request->type_id)
            $type_id = $request->type_id;
        else
            $type_id = 0;

        return view('admin.do_it_yourself.index', compact('title_page', 'breadcumb','type_id'));
    }

    public function datatable(Request $request)
    {
        $columns = ['id','type', 'title', 'category_id', 'image', 'status',  'created_at', 'action'];
        $conditions = [];

        $totalData = DoItYourself::select('id')->count();
        $results = DoItYourself::with(['CategoryData','TypeData']);

        if($request->type_id > 0)
        {
            $totalData = DoItYourself::where('type_id',$request->type_id)->select('id')->count();
            $results = DoItYourself::where('type_id',$request->type_id)->with(['CategoryData','TypeData']);
        }



        $limit = $request->input('length');
        $start = $request->input('start');
        $dir = $request->input('order.0.dir');
        $order = $columns[$request->input('order.0.column')];




        if (!empty($request->input('search.value'))) {
            $search = trim($request->input('search.value'));
            $date = explode('-',$search);
            try{
                if(count($date)==2) {
                    $date=$date[1].'-'.$date[0];
                }
                else if(count($date)==1) {
                    $date=$date[0];
                }
                else    {
                    $date=$date[2].'-'.$date[0].'-'.$date[1];
                }
            }
            catch(Exception $e){
                $date='';
            }

            $results = $results->where(function ($query) use ($search,$date) {
                $query->where('title', 'LIKE', "%{$search}%")
                    ->orWhere('updated_at', 'LIKE', "%".$date."%")
                    ->orWhere('slug', 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $results->count();
        $results = $results->offset($start)->limit($limit)->orderBy($order, $dir)->get();

        $status_name_arr = Config::get('params.status_name_arr');
        $data = array();
        if (!empty($results)) {
            foreach ($results as $row) {

                $category = null;

                if(isset($row->CategoryData)){
                   $cat =  $row->CategoryData->toArray();
                   $category = $cat['name'];
                }

                if(isset($row->TypeData)){
                   $type =  $row->TypeData->toArray();
                   $type = $type['name'];
                }

                $nestedData['id'] = $row->id;
                $nestedData['title'] = $row->title;
                $nestedData['type_id'] = $type;
                $nestedData['category_id'] = $category;
                $nestedData['image'] = getListImage(asset($row->image));
                $nestedData['status'] = isset($status_name_arr[$row->status]) ? $status_name_arr[$row->status] : '-';

                $nestedData['created_at'] = listDateFromat($row->updated_at);
                $buttons = [];
                if(empty($parent_id)){
                    $buttons[] = ['key' => 'view', 'link' => route('admin.do_it_yourself.view',$row->id), 'title'=>'View Recipe'];
                }

                $buttons[] = ['key' => 'edit', 'link' => route('admin.do_it_yourself.edit', $row->id)];
                $buttons[] = ['key' => 'delete', 'link' => route('admin.do_it_yourself.destroy', $row->id)];

                $nestedData['action'] = getButtons($buttons);
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function create()
    {
        $title_page = 'Add New Entry';
        $breadcumb = ['Do It Youself'=>route('admin.do_it_yourself.index')];

        $breadcumb['Add'] = '';
        $entity = null;

        $Ingredient =[];
        $categories = [];
        $type = Type::whereIn('id',[1,2])->get()->pluck('name','id')->toArray();

        return view('admin.do_it_yourself.create', compact('breadcumb', 'title_page','entity','categories','Ingredient','type'));
    }

    public function store(Request $request)
    {
        // return $request->all();
     //   echo "<pre>"; print_r($request->all()); die;
        try {

            $data = $request->all();

            $validator = Validator::make($request->all(), [
                'type' => 'required',
                'category' => 'required',
                'title' => 'required',
                'image' => 'required|mimes:jpeg,jpg,png',
                'description' => 'required',
                'youtube_url' => 'nullable|url',
                'prep_time' => 'nullable',
                'servings' => 'nullable'

            ]);

            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }

            $entity = new DoItYourself();

            $entity->type_id = $request->type;
            $entity->title = $request->title;
            $entity->category_id = $request->category;
            $entity->description = $request->description;
            $entity->youtube_url = $request->youtube_url;
            $entity->prep_time = $request->prep_time;
            $entity->servings = $request->servings;

            if ($request->file('image')) {
                $destinationPath = '/uploads/do_it-yourself/';
                $response_data = Uploader::doUpload($request->file('image'), $destinationPath);
                if ($response_data['status'] == true) {
                    $entity->image = $response_data['file'];
                }
            }

            $entity->save();

            for($i=1;$i<=100;$i++){

                if(isset($data['ingredient_name_'.$i]) && isset($data['ingredient_quantity_'.$i])){

                    $DoItYourselfIngredient = new DoItYourselfIngredient();
                    $DoItYourselfIngredient->do_it_yourself_id = $entity->id;
                    $DoItYourselfIngredient->item_name = $data['ingredient_name_'.$i];
                    $DoItYourselfIngredient->item_quantity = $data['ingredient_quantity_'.$i];
                    $DoItYourselfIngredient->item_value = $data['ingredient_value_'.$i];

                    $DoItYourselfIngredient->save();

                }
            }

            Session::flash('success', 'Do it Youself entry has been added successfully.');
            return redirect()->route('admin.do_it_yourself.index');
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('error', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function view($id)
    {
        $entity = DoItYourself::with('CategoryData')->where('id',$id)->first();

        $title_page = 'Do It Yourself';
        $breadcumb = ['Do It Youself'=>route('admin.do_it_yourself.index')];

        $breadcumb['View'] = '';

        $Ingredient = DoItYourselfIngredient::where('do_it_yourself_id',$entity->id)->get();

        return view('admin.do_it_yourself.view', compact('breadcumb', 'title_page','entity','Ingredient'));
    }


    public function get_categories(Request $request)
    {
        $type_id = $request->type_id;
        $categories = Category::where('type_id',$type_id)->where('is_do_it_yourself',1)->where('status','1')->get()->pluck('name','id')->toArray();

        $str = '<option>Select Category</option>';
        if(count($categories)){
            foreach($categories as $k => $v){
                $str .= '<option value="'.$k.'">'.$v.'</option>';
            }
        }

        return $str;
    }

    public function edit(Request $request, $id)
    {
        try {
            $entity = DoItYourself::with(['CategoryData'])
            ->where('id', $id)
            ->first();
            if (empty($entity) || empty($entity->CategoryData)) {
                abort(404);
            }


            $title_page = 'Edit Do It Yourself';
            $breadcumb = ['Do It Yourself'=>route('admin.do_it_yourself.index')];

            $breadcumb['Edit'] = '';

            $categories = Category::where(['is_do_it_yourself'=>1,'type_id'=>$entity->type_id,'parent_id' => NULL])->where('status','1')
            ->get()->pluck('name','id')->toArray();

            $ingredients = DoItYourselfIngredient::where('do_it_yourself_id',$entity->id)->get();

            $type = Type::whereIn('id',[1,2])->get()->pluck('name','id')->toArray();

            return view('admin.do_it_yourself.create', compact('breadcumb', 'title_page', 'entity','categories','ingredients','type'));
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try {

            $data = $request->all();

            $entity = DoItYourself::with(['CategoryData'])
                ->where('id', $id)->first();
            if (empty($entity) || empty($entity->CategoryData)) {
                abort(404);
            }

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'image' => 'nullable|mimes:jpeg,jpg,png',
                'description' => 'required',
                'youtube_url' => 'nullable',
                'category' => 'required',
                'type' => 'required',
                'prep_time' => 'nullable',
                'servings' => 'nullable'
            ]);

            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }

            $entity->category_id = $request->category;
            $entity->type_id = $request->type;
            $entity->title = $request->title;
            $entity->description = $request->description;
            $entity->youtube_url = $request->youtube_url;

            $entity->prep_time = $request->prep_time;
            $entity->servings = $request->servings;

            if ($request->file('image')) {
                if (!empty($entity->image) && file_exists('public'.$entity->image)) {
                    unlink('public'.$entity->image);
                }
                $destinationPath = '/uploads/do_it_yourself/';
                $response_data = Uploader::doUpload($request->file('image'), $destinationPath);
                if ($response_data['status'] == true) {
                    $entity->image = $response_data['file'];
                }
            }

            $entity->save();

            /*$Ingredient_last = DoItYourselfIngredient::select('id')->where('do_it_yourself_id',$id)->orderBy('id','desc')->first();

            $Ingredient_last_id = $Ingredient_last->id;

            $Ingredient_first = DoItYourselfIngredient::select('id')->where('do_it_yourself_id',$id)->orderBy('id','asc')->first();

            $Ingredient_first_id = $Ingredient_first->id;
            */

            for($i=1;$i<=100;$i++){

                if(isset($data['ingredient_name_'.$i]) && isset($data['ingredient_quantity_'.$i])){

                    if(isset($data['ingredient_'.$i])){

                        DoItYourselfIngredient::where('id',$data['ingredient_'.$i])
                        ->update([
                            'item_name' => $data['ingredient_name_'.$i],
                            'item_quantity' => $data['ingredient_quantity_'.$i]
                        ]);
                    }
                    else{

                        $DoItYourselfIngredient = new DoItYourselfIngredient();
                        $DoItYourselfIngredient->do_it_yourself_id = $id;
                        $DoItYourselfIngredient->item_name = $data['ingredient_name_'.$i];
                        $DoItYourselfIngredient->item_quantity = $data['ingredient_quantity_'.$i];
                        $DoItYourselfIngredient->save();

                    }

                }
            }

            Session::flash('success', 'Do It Yourself entry has been updated successfully.');
            return redirect()->route('admin.do_it_yourself.edit',$id);
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function destroy(Request $request, $id)
    {
        try {

            DoItYourself::where('id', $id)->delete();
            DoItYourselfIngredient::where('do_it_yourself_id',$id)->delete();

            Session::flash('success', 'Do It Youself entry has been deleted successfully.');
            return redirect()->back();
        } catch (\Exception $e) {
            Session::flash('warning', 'Invalid Request');
            return redirect()->back();
        }
    }
}
