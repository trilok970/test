<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\FaqCategory;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use App\Lib\Uploader;
use \Config;

class FaqCategoriesController extends Controller {

    public function index() {
        $title_page = 'FAQ Categories';
        $breadcumb = [$title_page => ''];
        return view('admin.faq_categories.index', compact('title_page','breadcumb'));
    }

    public function datatable(Request $request) {
        $columns = ['id', 'name', 'status', 'created_at', 'action'];
        $totalData = FaqCategory::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $dir = $request->input('order.0.dir');
        $order = $columns[$request->input('order.0.column')];


        $results = FaqCategory::select('faq_categories.*');

        if (!empty($request->input('search.value'))) {
            $search = trim($request->input('search.value'));
            $date = explode('-',$search);
            try{
                if(count($date)==2) {
                    $date=$date[1].'-'.$date[0];
                }
                else if(count($date)==1) {
                    $date=$date[0];
                }
                else    {
                    $date=$date[2].'-'.$date[0].'-'.$date[1];
                }
            }
            catch(Exception $e){
                $date='';
            }
            $results = $results->where(function($query) use ($search,$date) {
                $query->where('name', 'LIKE', "%{$search}%")
                       ->orWhere('updated_at', 'LIKE', "%".$date."%");
            });
        }

        $totalFiltered = $results->count();
        $results = $results->offset($start)->limit($limit)->orderBy($order, $dir)->get();
        $status_name_arr = Config::get('params.status_name_arr');

        $data = array();
        if (!empty($results)) {
            foreach ($results as $row) {
                $nestedData['id'] = null;
                $nestedData['name'] = $row->name;
                $nestedData['status'] = isset($status_name_arr[$row->status]) ? $status_name_arr[$row->status] : '-';
                $nestedData['created_at'] = listDateFromat($row->updated_at);

                $nestedData['action'] = getButtons([
                    ['key' => 'edit', 'link' => route('faq-categories.edit', $row->id)],
                    ['key' => 'delete', 'link' => route('faq-categories.destroy', $row->id)]
                ]);
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function create() {
        $title_page = 'Add FAQ Category';
        $breadcumb = ['Faq Categories' => route('faq-categories.index'), $title_page => ''];
        $entity = new FaqCategory;
        return view('admin.faq_categories.create', compact('breadcumb','title_page', 'entity'));
    }

    public function store(Request $request) {

        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'status' => 'required'
            ]);
            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }
            $entity = new FaqCategory();
            $entity->name = $request->name;
            $entity->status = $request->status;

            $entity->save();
            Session::flash('success', 'FAQ Category has been added successfully.');
            return redirect()->route('faq-categories.index');
        } catch (\Exception $e) {

            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function show($id) {

    }

    public function edit(Request $request, $id) {
        try {
            $entity = FaqCategory::where('id', $id)->first();
            if (empty($entity)) {
                Session::flash('warning', 'Invalid Request');
                return redirect()->back();
            }

            $title_page = 'Edit FAQ Category';
            $breadcumb = ['Faq Categories' => route( 'faq-categories.index'), $title_page => ''];
            return view('admin.faq_categories.create', compact('breadcumb','title_page', 'entity'));

        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back();
        }
    }

    public function update(Request $request, $id) {
        try {
            $entity = FaqCategory::where('id', $id)->first();
            if (empty($entity)) {
                Session::flash('warning', 'Invalid Request');
                return redirect()->back();
            }
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);
            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }

            $entity->name = $request->name;
            $entity->status = $request->status;


            $entity->save();
            Session::flash('success', 'FAQ Category has been updated successfully.');
            return redirect()->route('faq-categories.index');
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function destroy(Request $request, $id) {
        try {
            $row = FaqCategory::where('id', $id)->delete();
            Session::flash('success', 'FAQ Category has been deleted successfully.');
            return redirect()->back();
        } catch (\Exception $e) {
            Session::flash('warning', 'Invalid Request');
            return redirect()->back();
        }
    }


}
