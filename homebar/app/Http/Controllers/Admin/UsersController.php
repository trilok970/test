<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use DB;
use Illuminate\Support\Facades\Input;
use Session;
use Illuminate\Support\Facades\Validator;
use Config;
use Illuminate\Support\Facades\Auth;
use App\Lib\Uploader;
use Exception;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller {


    public function dashboard() {
        $title_page = 'Dashboard';
        //$temp = \App\Model\EmailTemplate::where('slug', 'send-welcome-mail-when-customer-signup')->first()->toArray();
        //pr(json_encode($temp)); die;
        $role_ids = Config::get('params.role_ids');
        $customer_count = User::where('role', $role_ids['user'])->count();

        return view('admin.users.dashboard',compact('title_page', 'customer_count', 'role_ids'));
    }// end function.

    public function users() {
        $role = 2;

        $role_names = Config::get('params.role_names');
        $role_name = $role_names[$role];
        $title_page = $role_name.'s';
        $breadcumb = [$title_page=>''];
        //Session::flash('success', 'User has been deleted successfully.');

        return view('admin.users.index',compact('title_page','breadcumb', 'role'));
    }

    public function usersDataTable(Request $request) {
        $role = 2;
        $columns = ['id','full_name', 'email', 'profile_image', 'created_at', 'status', 'action'];
        $role_ids = Config::get('params.role_ids');
        $totalData = User::where('role', $role)->count();
        $limit = $request->input('length');
        $start = $request->input('start');

        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $users = User::where('role',  $role);


        if(!empty($request->input('search.value'))) {
            $search = trim($request->input('search.value'));
            $date = explode('-',$search);
            try{
                if(count($date)==2) {
                    $date=$date[1].'-'.$date[0];
                }
                else if(count($date)==1) {
                    $date=$date[0];
                }
                else    {
                    $date=$date[2].'-'.$date[0].'-'.$date[1];
                }
            }
            catch(Exception $e){
                $date='';
            }

            $users = $users->where(function($query) use ($search,$date) {
                $query->where('full_name', 'LIKE', "%{$search}%")
                        ->orWhere('email', 'LIKE', "%{$search}%")
                        ->orWhere('phone', 'LIKE', "%{$search}%")
                        ->orWhere('updated_at', 'LIKE', "%".$date."%")
                        ->orWhere('status', 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $users->count();
        $users = $users->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();


        $data = [];
        if (!empty($users)) {
            foreach ($users as $key => $row) {
                $nestedData['id'] = null;
                $nestedData['first_name'] = ucfirst($row->first_name);
                $nestedData['last_name'] = ucfirst($row->last_name);

                $nestedData['phone'] = phoneNumberFormat($row->phone);

                $filename = $row->profile_image_full;
                $nestedData['profile_image'] = !empty($row->profile_image_full) ? '<img src="'.$filename.'" width="50"/>' : '';
                $nestedData['created_at'] = listDateFromat($row->updated_at);
                $nestedData['status'] = getStatus($row->status,$row->id);
                $buttons = [['key'=>'view','link'=>route('admin.users.show',$row->id)],
                    //['key'=>'edit','link'=>route('chefs.edit',$row->id)],
                ];


                $buttons[] = ['key'=>'delete','link'=>route('admin.users.destroy',$row->id)];
                $nestedData['action'] =  getButtons($buttons);

                $data[] = $nestedData;
            }
        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function statusUpdate(Request $request) {
        $user_id = $request->id;
        $row = User::whereId($user_id)->first();
        $row->status = ($row->status == '0') ? '1' : '0';
        $row->reason_for_deactivation = $request->reason_for_deactivation;
        $row->save();
        $html = '';
        switch ($row->status) {
            case '0':
                $html = '<a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Penidng" onClick="changeStatus(' . $user_id . ','.$row->status.')" >Inactive</a>';
                break;
            case '1':
                $html = '<a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeStatus(' . $user_id . ','.$row->status.')" >Active</a>';
                break;




            default:

                break;
        }
        return $html;
    }// end function.


    public function show($id){
         try {
            $entity = User::where(['id'=>$id])->first();

            if ($entity) {

                $role_ids = Config::get('params.role_ids');
                $role_names = Config::get('params.role_names');
                $role = $entity->role;
                $role_name = $role_names[$role];
                $role_title = $role_name.'s';

                $title_page = "$role_name Details";
                $breadcumb = [$role_title => route('admin.users', ['role'=>$role]), $title_page => ''];
                return view('admin.users.show', compact('title_page', 'entity', 'breadcumb'));
            } else {
                Session::flash('warning', 'Invalid request');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back()->withInput();
        }
    }


    public function destroy($id) {
        try {
            $role_ids = Config::get('params.role_ids');

            $user = User::where('id', $id)->first();
            if($user && $user->role == $role_ids['admin']){
                abort(404);
            } else {
                User::destroy($id);
                Session::flash('success', 'User has been deleted successfully.');
                return back();
            }

        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back();
        }
    }// end function.


    public function myProfile(Request $request){
        $user_id = Auth::id();
        try {
            $user = User::find($user_id);
            if ($user) {
                $title_page = 'My Profile';
                $breadcumb = [$title_page => ''];
                if ($request->isMethod('post')) {
                    $rules = array(
                        'full_name' => 'required',
                        'email' => 'required|email|max:255|unique:users,email,' . $user->id,
                        'profile_image' => 'nullable|mimes:jpeg,jpg,png',
                    );
                    $validator = Validator::make($request->all(), $rules);
                    if ($validator->fails()) {
                        Session::flash('error', 'Please correct the errors below and try again');
                        return redirect()->back()->withInput()->withErrors($validator->errors());
                    } else {
                        $user->full_name = $request->full_name;
                        $user->email = $request->email;
                        if ($request->file('profile_image') !== null) {
                            $destinationPath = '/uploads/user/';
                            $response_data = Uploader::doUpload($request->file('profile_image'), $destinationPath);
                            if ($response_data['status'] == true) {
                                $user->profile_image = $response_data['file'];
                            }
                        }
                        if ($user->save()) {
                            Session::flash('success', 'Profile has been updated successfully.');
                            return redirect()->route('admin.dashboard');
                        }
                    }
                }
                return view('admin.users.my_profile', compact('title_page', 'user', 'breadcumb'));
            } else {
                Session::flash('warning', 'Invalid request');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back()->withInput();
        }
    }// end function.

    public function changePassword(Request $request){
        $user_id = Auth::id();
        try {
            $user = User::find($user_id);
            if ($user) {
                $title_page = 'Change Password';
                $breadcumb = ['Profile'=>route('admin.myprofile'), $title_page => ''];
                if ($request->isMethod('post')) {
                    $rules = [
                        'old_password' => 'required',
                        'new_password' => 'required|min:8',
                        'confirm_password' => 'required|same:new_password',
                    ];
                    $validator = Validator::make($request->all(), $rules);
                    if ($validator->fails()) {
                        Session::flash('error', 'Please correct the errors below and try again');
                        return redirect()->back()->withInput()->withErrors($validator->errors());
                    } else {
                        if (Hash::check($request->old_password, $user->password)){
                            $user->password = Hash::make($request->new_password);
                            $user->save();
                            Session::flash('success', 'Password has been updated successfully.');
                            return redirect()->route('admin.dashboard');
                        }
                        else {
                            Session::flash('error', 'Current password is incorrect.');
                            return redirect()->back();
                        }
                    }
                }
                return view('admin.users.change_password', compact('title_page', 'user', 'breadcumb'));
            } else {
                Session::flash('warning', 'Invalid request');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function forgot(Request $request) {
       if ($request->isMethod('post')) {
            try {
                $rules = ['email' => 'required|email'];
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    Session::flash('error', 'Please correct the errors below and try again');
                    return redirect()->back()->withInput()->withErrors($validator);;
                } else {
                    $status_arr = Config::get('params.status_arr');
                    $role_arr = Config::get('params.role_ids');
                    $user = User::where('email', '=', $request['email'])->where('role', $role_arr['admin'])->first();
                    if(empty($user)) {
                        Session::flash('error', 'The Email you entered does not exist.');
                        return redirect()->back()->withInput();
                    }else if($user->status == $status_arr['inactive']) {
                        Session::flash('error', 'Your account is not active.');
                        return redirect()->back()->withInput();
                    }
                    $token = randomToken();
                    $user->token = $token;
                    $user->save();


                    $template = \App\Model\EmailTemplate::where([['slug', 'send-mail-when-admin-forgot-password']])->first();
                    $site_name = getSettings()['site_title'];
                    $subject = $template->subject;
                    $description = $template->description;
                    $link = route('admin.reset', ['token'=>$token]);

                    $subject = str_replace(
                        ['{FULL_NAME}', '{EMAIL}', '{SITE}', '{LINK}'],
                        [$user->full_name, $user->email, $site_name, $link],
                        $subject
                    );

                    $description = str_replace(
                            ['{FULL_NAME}', '{EMAIL}', '{SITE}', '{LINK}'],
                            [$user->full_name, $user->email, $site_name, $link],
                            $description
                            );

                    $mail_data = [
                        'email' => $user->email,
                        'subject' => $subject,
                        'content' => $description
                    ];
                    mailSend($mail_data);


                    Session::flash('success', 'Reset password link has been sent successfully to ' . $request['email']);
                    return redirect()->route('admin.forgot');

                }
            } catch (\Exception $e) {
                Session::flash('error', $e->getMessage());
                return redirect()->back();
            }
        }
        $title_page = 'Forgot Password';
        return view('admin.users.forgot',  compact('title_page'));
    }// end function.

    public function reset(Request $request, $token) {
        $status_arr = Config::get('params.status_arr');

        $user_data = User::where('token', $token)->first();
        if(empty($user_data)){
            Session::flash('error', 'The request page was not found');
            return redirect()->route('admin.login');
        } else if($user_data->status == $status_arr['inactive']) {
            Session::flash('error', 'Your account is not active.');
            return redirect()->route('admin.login');
        }

        if ($request->isMethod('post')) {
            try {
                $rules = [
                    'password' => 'required|min:8|max:15',
                    'confirm_password' => 'required|required_with:password|same:password|min:8|max:15',
                ];
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    Session::flash('error', 'Please correct the errors below and try again');
                    return redirect()->back()->withInput()->withErrors($validator->errors());
                } else {

                    $user_data->password = Hash::make($request->password);

                    $user_data->token = null;
                    $user_data->save();
                    Session::flash('success', 'Password has been updated successfully.');
                    return redirect()->route('admin.login');
                }
            } catch (\Exception $e) {
                Session::flash('danger', $e->getMessage());
                return redirect()->back();
            }
        }
        $title_page = 'Reset Password';
        return view('admin.users.reset',  compact('title_page'));
    }

}
