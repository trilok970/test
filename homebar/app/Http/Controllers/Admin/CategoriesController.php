<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Type;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use App\Lib\Uploader;
use Config;

class CategoriesController extends Controller
{
    public function types()
    {
        $title_page = 'Categories';
        $breadcumb = [$title_page => ''];
        return view('admin.categories.types', compact('title_page', 'breadcumb'));
    }

    public function typesDatatable(Request $request)
    {
        $columns = ['id', 'name', 'children_count', 'created_at', 'action'];
        $totalData = Type::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $dir = $request->input('order.0.dir');
        $order = $columns[$request->input('order.0.column')];
        $results = Type::select('types.*');

        if (!empty($request->input('search.value'))) {
            $search = trim($request->input('search.value'));
            $date = explode('-',$search);
            try{
                if(count($date)==2) {
                    $date=$date[1].'-'.$date[0];
                }
                else if(count($date)==1) {
                    $date=$date[0];
                }
                else    {
                    $date=$date[2].'-'.$date[0].'-'.$date[1];
                }
            }
            catch(Exception $e){
                $date='';
            }
            $results = $results->where(function ($query) use ($search,$date) {
                $query->where('name', 'LIKE', "%{$search}%")
                    ->orWhere('updated_at', 'LIKE', "%".$date."%")
                    ->orWhere('slug', 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $results->count();
        $results = $results->offset($start)->limit($limit)->orderBy($order, $dir)->get();

        $data = [];
        if (!empty($results)) {
            foreach ($results as $row) {
                $children_cound = Category::where(['type_id'=> $row->id])->whereNull('parent_id')->count();
                $nestedData['id'] = null;
                $nestedData['name'] = $row->name;
                $nestedData['children_count'] = $children_cound;
                $nestedData['created_at'] = listDateFromat($row->updated_at);

                $nestedData['action'] = getButtons([
                    [
                        'key' => 'view',
                        'link' => route('admin.categories.index', $row->id),
                        'title'=>'View Children'
                    ]
                ]);
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function index($type_id, $parent_id=null)
    {
        $title_page = 'Categories';
        $breadcumb = ['Categories'=>route('admin.types.index')];

        $type_row = Type::where(['id'=>$type_id])
            ->first();
        if (empty($type_row)) {
            abort(404);
        }
        if ($parent_id) {
            $breadcumb[$type_row->name] = route('admin.categories.index', $type_id);
        } else {
            $breadcumb[$type_row->name] = '';
        }

        if (!empty($parent_id)) {
            $category_row = Category::where(['id'=>$parent_id])
            ->first();
            if (empty($category_row) || !empty($category_row->parent_id)) {
                abort(404);
            }
            $title_page = $category_row->name;
            $breadcumb[$category_row->name] = '';
        } else {
            $title_page = $type_row->name;
        }


        return view('admin.categories.index', compact('title_page', 'breadcumb', 'type_id', 'parent_id'));
    }

    public function datatable(Request $request, $type_id, $parent_id=null)
    {
        $columns = ['id', 'name', 'image', 'status', 'children_count', 'created_at', 'action'];
        $conditions = [];
        if ($type_id) {
            $conditions['type_id'] = $type_id;
        }
        if ($parent_id) {
            $conditions['parent_id'] = $parent_id;
        }
        $totalData = Category::select('categories.id');
        if ($conditions) {
            $totalData = $totalData->where($conditions);
        }
        if(empty($parent_id)){
            $totalData = $totalData->whereNull('parent_id');
        }
        $totalData = $totalData->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $dir = $request->input('order.0.dir');
        $order = $columns[$request->input('order.0.column')];


        $results = Category::select('categories.*');
        if ($conditions) {
            $results = $results->where($conditions);
        }
        if(empty($parent_id)){
            $results = $results->whereNull('parent_id');
        }
        if (!empty($request->input('search.value'))) {
            $search = trim($request->input('search.value'));
            $date = explode('-',$search);
            try{
                if(count($date)==2) {
                    $date=$date[1].'-'.$date[0];
                }
                else if(count($date)==1) {
                    $date=$date[0];
                }
                else    {
                    $date=$date[2].'-'.$date[0].'-'.$date[1];
                }
            }
            catch(Exception $e){
                $date='';
            }
            $results = $results->where(function ($query) use ($search,$date) {
                $query->where('name', 'LIKE', "%{$search}%")
                    ->orWhere('updated_at', 'LIKE', "%".$date."%")
                    ->orWhere('slug', 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $results->count();
        $results = $results->offset($start)->limit($limit)->orderBy($order, $dir)->get();

        $status_name_arr = Config::get('params.status_name_arr');
        $data = array();
        if (!empty($results)) {
            foreach ($results as $row) {

                $is_pre_made = $is_do_it_yourself = 'No';
                if($row->is_pre_made) $is_pre_made = 'Yes';
                if($row->is_do_it_yourself) $is_do_it_yourself = 'Yes';

                $nestedData['is_pre_made'] = $is_pre_made;
                $nestedData['is_do_it_yourself'] = $is_do_it_yourself;

                $nestedData['id'] = null;
                $nestedData['name'] = $row->name;
                $nestedData['image'] = getListImage($row->image_full);
                $nestedData['status'] = isset($status_name_arr[$row->status]) ? $status_name_arr[$row->status] : '-';
                $nestedData['children_count'] = $row->childrenCount->count();

                $nestedData['created_at'] = listDateFromat($row->updated_at);
                $buttons = [];
                if(empty($parent_id)){
                    $buttons[] = ['key' => 'view', 'link' => route('admin.categories.index', [$type_id, $row->id]), 'title'=>'View Children'];
                }

                $buttons[] = ['key' => 'edit', 'link' => route('admin.categories.edit', $row->id)];
                // $buttons[] = ['key' => 'delete', 'link' => route('admin.categories.destroy', $row->id)];

                $nestedData['action'] = getButtons($buttons);
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function create($type_id, $parent_id=null)
    {
        $title_page = 'Add Category';
        $breadcumb = ['Categories'=>route('admin.types.index')];
        $type_row = Type::where(['id'=>$type_id])
            ->first();
        if (empty($type_row)) {
            abort(404);
        }

        $breadcumb[$type_row->name] = route('admin.categories.index', $type_id);

        if (!empty($parent_id)) {
            $category_row = Category::where(['id'=>$parent_id])
            ->first();
            if (empty($category_row) || !empty($category_row->parent_id)) {
                abort(404);
            }
            $breadcumb[$category_row->name] = route('admin.categories.index', [$type_id,$parent_id]);
        }
        $breadcumb['Add'] = '';
        $entity = new Category;
        return view('admin.categories.create', compact('breadcumb', 'title_page', 'entity', 'type_id', 'parent_id'));
    }

    public function store(Request $request, $type_id, $parent_id=null)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'image' => 'required|mimes:jpeg,jpg,png',
            ]);
            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }
            if ($parent_id) {
                $category_row = Category::where(['id'=>$parent_id])
                ->first();
                if (empty($category_row) || !empty($category_row->parent_id)) {
                    abort(404);
                }
            }
            $entity = new Category();

            if(!$parent_id){

                if(isset($request->is_pre_made)) $is_pre_made =1;
                else $is_pre_made = 0;

                if(isset($request->is_do_it_yourself)) $is_do_it_yourself =1;
                else $is_do_it_yourself = 0;

                $entity->is_pre_made = $is_pre_made;
                $entity->is_do_it_yourself = $is_do_it_yourself;
            }

            $entity->name = $request->name;
            $entity->type_id = $type_id;
            $entity->parent_id = $parent_id;
            $entity->status = $request->status;

            if ($request->file('image')) {
                $destinationPath = '/uploads/categories/';
                $response_data = Uploader::doUpload($request->file('image'), $destinationPath);
                if ($response_data['status'] == true) {
                    $entity->image = $response_data['file'];
                }
            }
            $entity->save();
            Session::flash('success', 'Category has been added successfully.');
            return redirect()->route('admin.categories.index', [$type_id, $parent_id]);
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('error', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function show($id)
    {
    }

    public function edit(Request $request, $id)
    {
        try {
            $entity = Category::with(['typeData', 'parentData'])
            ->where('id', $id)
            ->first();
            if (empty($entity) || empty($entity->typeData)) {
                abort(404);
            }

            $parent_id = null;
            $title_page = 'Edit Category';
            $breadcumb = ['Categories'=>route('admin.types.index')];
            $type_row = $entity->typeData;
            $type_id = $type_row->id;

            $parent_row = isset($entity->parentData) ? $entity->parentData : '';

            $breadcumb[$type_row->name] = route('admin.categories.index', $type_row->id);

            //$parent_id = isset($parent_row->id) ? $parent_row->id : null;

            if (!empty($parent_row)) {
                $parent_id = $parent_row->id;
                $breadcumb[$parent_row->name] = route('admin.categories.index', [$type_id,$parent_id]);
            }
            $breadcumb['Edit'] = '';
            return view('admin.categories.create', compact('breadcumb', 'title_page', 'entity', 'type_id', 'parent_id'));
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $entity = Category::with(['typeData', 'parentData'])
                ->where('id', $id)->first();
            if (empty($entity) || empty($entity->typeData)) {
                abort(404);
            }
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);
            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }
            $type_row = $entity->typeData;
            $type_id = $type_row->id;

            $parent_id = isset($entity->parentData->id) ? $entity->parentData->id : null;

            $entity->name = $request->name;
            $entity->status = $request->status;

            if(!$parent_id){

                if(isset($request->is_pre_made)) $is_pre_made =1;
                else $is_pre_made = 0;

                if(isset($request->is_do_it_yourself)) $is_do_it_yourself =1;
                else $is_do_it_yourself = 0;

                $entity->is_do_it_yourself = $is_do_it_yourself;
                $entity->is_pre_made = $is_pre_made;
            }

            if ($request->file('image')) {
                if (!empty($entity->image) && file_exists('public'.$entity->image)) {
                    unlink('public'.$entity->image);
                }
                $destinationPath = '/uploads/categories/';
                $response_data = Uploader::doUpload($request->file('image'), $destinationPath);
                if ($response_data['status'] == true) {
                    $entity->image = $response_data['file'];
                }
            }
            $entity->save();
            Session::flash('success', 'Category has been updated successfully.');
            return redirect()->route('admin.categories.index', [$type_id, $parent_id]);
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function destroy(Request $request, $id)
    {
        try {
            $row = Category::where('id', $id)->delete();
            Session::flash('success', 'Category has been deleted successfully.');
            return redirect()->back();
        } catch (\Exception $e) {
            Session::flash('warning', 'Invalid Request');
            return redirect()->back();
        }
    }
}
