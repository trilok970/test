<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Twilio\Rest\Client;

use App\Model\Type as Type;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function validationHandle($validation) {
        foreach ($validation->getMessages() as $field_name => $messages) {
            if (!isset($firstError)) {
                $firstError = $messages[0];
            }
        }
        return $firstError;
    }// end function.

    public function sendOtp($mobile) {

        $otp = rand(100000, 999999);
        $sid = env('TWILIO_ACCOUNT_SID', '');
        $token = env('TWILIO_AUTH_TOKEN', '');
        $from_number = env('TWILIO_NUMBER', '');
        $messagingServiceSid = env('TWILIO_MESSAGING_SERVICE_SID', '');
        $twilio = new Client($sid, $token);
    
        $body = 'Your Homebar OTP is '. $otp;
        $message = $twilio->messages
            ->create(
                $mobile, // to
                    array(
                        "messagingServiceSid" => $messagingServiceSid,
                        "body" => $body
                    )
            );
        return $otp;
    }

    

    public function __construct() {
        $types = Type::all();
        View::share('types', $types);
    }
}
