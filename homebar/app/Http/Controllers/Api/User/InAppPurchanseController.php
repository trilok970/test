<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Model\Plan;
use App\Model\User;
use App\Model\UserSubscription;
use Illuminate\Http\Request;
use Auth;
use Validator;
class InAppPurchanseController extends Controller
{
    public $successStatus = 200;
	public $failureStatus = 401;
    public function getPlan($name)
    {
        return Plan::where(['name'=>$name])->first();
    }
    public function planCheck($user_id,$plan_id)
    {
        return UserSubscription::where(['user_id'=>$user_id,'plan_id'=>$plan_id])->count();
    }
    public function check_txn_id($txn_id)
    {
        return UserSubscription::where(['txn_id'=>$txn_id])->count();
    }
    public function check_user_txn_id($user_id,$txn_id)
    {
        return UserSubscription::where(['user_id'=>$user_id,'txn_id'=>$txn_id])->count();
    }
    public function purchasePlan(Request $request)
    {
         $path = url('public/images/').'/';
         $user = Auth::user();
    
        $validator = Validator::make($request->all(), [
            'plan_id' => 'required|max:255',
            'platform' => 'required|max:255',
            'receipt' => 'required',
            'txn_id' => 'required',
            'payment_time' => 'required',
            ]);
        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
         }
        $from_date =  date("Y-m-d");
        $to_date =  date("Y-m-d",strtotime("+30 days",strtotime($from_date)));
        $usernew = User::find($user->id);
    
        if($this->check_txn_id($request->txn_id) > 0)
        {
            if($this->check_user_txn_id($user->id,$request->txn_id) > 0)
            {
                $user_subscription = UserSubscription::where(['user_id'=>$user->id,'txn_id'=>$request->txn_id])->first();
                $data['status']  = true;
                $data['message'] = 'Subscription plan already added';
                $data['path']	 = $path;
                $data['data'] = $user_subscription;
                return response()->json($data, $this->successStatus);
            }
            else
            {
                $data['status']  = false;
                $data['message'] = 'Transaction id is already exist.';
                $data['path']	 = $path;
                $data['data'] 	 = '';
                return response()->json($data, $this->successStatus);
            }
        }
        else
        {
            $user_subscription = new UserSubscription;
            $user_subscription->user_id = $user->id;
            $user_subscription->plan_id = $request->plan_id;
            $user_subscription->platform = $request->platform;
            $user_subscription->txn_id = $request->txn_id;
            $user_subscription->receipt = $request->receipt ?? "";
            $user_subscription->payment_time = date("Y-m-d h:i:s",strtotime($request->payment_time)) ?? "";
        }
    
    
        if($user_subscription->save())
        {
    
            if($usernew->subscription_plan_last_date && ($usernew->subscription_plan_last_date >= $from_date))
            $usernew->subscription_plan_last_date = date("Y-m-d",strtotime("+30 days",strtotime($to_date)));
            else
            $usernew->subscription_plan_last_date = $to_date;
            $usernew->save();
    
            $data['status']  = true;
            $data['message'] = 'Subscription plan added';
            $data['path']	 = $path;
            $data['data'] = $user_subscription;
            return response()->json($data, $this->successStatus);
        }
        else
        {
            $data['status']  = false;
            $data['message'] = 'Subscription plan not added';
            $data['path']	 = $path;
            $data['data'] 	 = '';
            return response()->json($data, $this->successStatus);
        }
    }
    public function purchaseRestore(Request $request)
    {
         $path = url('public/images/').'/';
         $user = Auth::user();
    
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|max:255',
            'platform' => 'required|max:255',
            'receipt' => 'required',
            'txn_id' => 'required',
            'payment_time' => 'required',
            ]);
        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
         }
        $from_date =  date("Y-m-d");
        $to_date =  date("Y-m-d",strtotime("+30 days",strtotime($from_date)));
        $usernew = User::find($user->id);
    
        $plan_id = $this->getPlan($request->product_id)->id;
        if($this->planCheck($user->id,$plan_id)) {
            $data['status']  = false;
            $data['message'] = 'This Plan is already exist.';
            $data['path']	 = $path;
            $data['data'] 	 = '';
            return response()->json($data, $this->successStatus);
        }
        else {
            if($this->check_txn_id($request->txn_id) > 0)
            {
                if($this->check_user_txn_id($user->id,$request->txn_id) > 0)
                {
                    $user_subscription = UserSubscription::where(['user_id'=>$user->id,'txn_id'=>$request->txn_id])->first();
                    $data['status']  = true;
                    $data['message'] = 'Subscription plan already added';
                    $data['path']	 = $path;
                    $data['data'] = $user_subscription;
                    return response()->json($data, $this->successStatus);
                }
                else
                {
                    $data['status']  = false;
                    $data['message'] = 'Transaction id is already exist.';
                    $data['path']	 = $path;
                    $data['data'] 	 = '';
                    return response()->json($data, $this->successStatus);
                }
            }
            else
            {
                $user_subscription = new UserSubscription;
                $user_subscription->user_id = $user->id;
                $user_subscription->plan_id = $request->plan_id;
                $user_subscription->platform = $request->platform;
                $user_subscription->txn_id = $request->txn_id;
                $user_subscription->receipt = $request->receipt ?? "";
                $user_subscription->payment_time = date("Y-m-d h:i:s",strtotime($request->payment_time)) ?? "";
            }
        
        
            if($user_subscription->save())
            {
        
                if($usernew->subscription_plan_last_date && ($usernew->subscription_plan_last_date >= $from_date))
                $usernew->subscription_plan_last_date = date("Y-m-d",strtotime("+30 days",strtotime($to_date)));
                else
                $usernew->subscription_plan_last_date = $to_date;
                $usernew->save();
        
                $data['status']  = true;
                $data['message'] = 'Subscription plan added';
                $data['path']	 = $path;
                $data['data'] = $user_subscription;
                return response()->json($data, $this->successStatus);
            }
            else
            {
                $data['status']  = false;
                $data['message'] = 'Subscription plan not added';
                $data['path']	 = $path;
                $data['data'] 	 = '';
                return response()->json($data, $this->successStatus);
            }
        }
        
    }
}
