<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Validator;
use \Config;
use JWTAuth;
use JWTAuthException;
use App\Lib\Uploader;
use App\Lib\StripePayment;
use App\Model\Order;
use App\Model\Cart;
use App\Model\UserAddress;
use App\Model\User;
use App\Model\OrderItem;
use App\Model\Coupon;
use App\Model\VendorIngredient;
use App\Model\DoItYourselfIngredient;
use App\Model\Offer;
use App\Model\Review;
use App\Model\Wallet;

class OrdersController extends Controller
{
    public function confirmOrder(Request $request)
    {
        try {
            $status_arr = Config::get('params.status_arr');
            $rules = [
                'cart_item_ids'=>'required|json',
                'special_instructions'=>'nullable',
                'address_id'=>'nullable',
                'order_type'=>'nullable|in:DELIVERY,PICKUP',
                'coupon_code'=>'nullable',
                'items_special_instruction'=>'nullable|json',
                //'delivery_date'=>'nullable|date_format:Y-m-d H:i:s',
                'delivery_instruction'=>'nullable',
                'card_id' => 'required'
            ];
            if (!empty($request->address_id)) {
                $rules['address_id'] = 'required|exists:user_addresses,id';
            } else {
                $rules['address'] = 'required';
                $rules['address_lat'] = 'required';
                $rules['address_lng'] = 'required';
            }
            $validator = Validator::make($request->all(), $rules);
            if (empty($request->order_type)) {
                $request->order_type = null;
            }
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user = JWTAuth::user();
            $user_id = $user->id;
            //echo $user_id; die;
            $stripe_customer_id = $user->stripe_customer_id;


            $ins_arr = [];
            if (!empty($request->items_special_instruction)) {
                $items_special_instruction = json_decode($request->items_special_instruction, true);

                foreach ($items_special_instruction as $ik => $idata) {
                    $ins_arr[$idata['menu_item_id']] = isset($idata['special_instruction']) ? $idata['special_instruction'] : null;
                }
            }


            $cart_ids_arr = json_decode($request->cart_item_ids, true);
            $cart_data = Cart::with(['menuItemDetails', 'ingredient', 'bartender'=>function ($query) {
                $query->select('users.id', 'users.vendor_id', 'users.first_name', 'users.last_name', 'users.description', 'users.latitude', 'users.longitude', 'users.profile_image', 'users.rating', 'users.experience', 'users.bio', 'users.price_per_hour', 'users.price', DB::raw("IF(users.options='1', 'INDIVIDUAL', 'GROUP') AS options, COUNT(bartender_group_members.id)+1 AS group_of"))->leftJoin('bartender_group_members', function ($join) {
                    $join->on('bartender_group_members.bartender_id', '=', 'users.id');
                });
            }
            ])
            ->whereIn('id', $cart_ids_arr)
            ->get();
            $type_id = $type = null;
            $restaurant_ids = [];
            $cart_data_arr = $cart_price_data = $discounted_prices = [];
            $offers = Offer::getOffers();
            foreach ($cart_data as $key => $row) {
                $restaurant_ids[$row->restaurant_id] = $row->restaurant_id;
                $cart_data_arr[$row->id] = $row;
                if (empty($type)) {
                    $type = $row->type;
                }
                if (empty($type_id)) {
                    $type_id = $row->type_id;
                }
                if ($row->type == 'PREMADE') {
                    $menu_item = $row->menuItemDetails;
                    $discounted_price = Offer::getDiscountedPriceAll($offers, $row->restaurant_id, $type_id, $menu_item->price);
                    $total_item_cost = $discounted_price * $row->quantity;
                    $cart_price_data[$row->id] = $total_item_cost;
                    $discounted_prices[$row->id] = $discounted_price;
                } elseif ($row->type == 'DIY') {
                    $vendor_item_row = VendorIngredient::getVendorIngredient($row->ingredient_id, $row->restaurant_id);
                    if (isset($vendor_item_row['cost'])) {
                        $discounted_price = Offer::getDiscountedPriceAll($offers, $row->restaurant_id, $type_id, $vendor_item_row['cost']);
                        $total_item_cost = $discounted_price * $row->quantity;
                        $cart_price_data[$row->id] = $total_item_cost;
                        $discounted_prices[$row->id] = $discounted_price;
                    }
                } elseif ($row->type == 'BARTENDER') {
                    $bartender = $row->bartender;
                    $params_offer = [
                        'vendor_id'=> $bartender->vendor_id,
                        'type_id'=> $row->type_id,
                        'price'=> $bartender->price_per_hour
                    ];
                    $discounted_price = Offer::getDiscountedPrice($params_offer);
                    $from_time = strtotime($row->from_time);
                    $to_time = strtotime($row->to_time);

                    $total_hours = round(abs($to_time - $from_time) / 3600);
                    if ($bartender->options == 'GROUP') {
                        $total_people = $bartender->group_of;
                        $cost_per_people = $discounted_price/$total_people;
                        $discounted_price = $cost_per_people * $row->quantity;
                    }
                    $total_cost = $discounted_price * $total_hours;
                    $cart_price_data[$row->id] = $total_cost;
                    $discounted_prices[$row->id] = $discounted_price;
                }
            }

            $total_price = array_sum($cart_price_data);

            $coupon_discount_amt = 0;
            $coupon = $coupon_code = null;
            if (!empty($request->coupon_code)) {
                $coupon_check = Coupon::checkCoupon($request->coupon_code, $user_id);
                if ($coupon_check['status'] == false) {
                    return response()->json($coupon_check);
                }
                $coupon = $coupon_check['data'];
            }
            $rest_count = count($restaurant_ids);

            $delivery_date = !empty($request->delivery_date) ? $request->delivery_date : date('Y-m-d H:i:s');

            $params = [
                'price_sum'=>$total_price,
                'restaurant_count'=> $rest_count,
                'coupon'=> $coupon,
                'order_type'=> $request->order_type,
                'delivery_date'=>$delivery_date
            ];

            $prices_cal = Cart::calculatePrices($params);
            //pr($prices_cal); die;
            $amount = $prices_cal['total'] * 100;
            $amount = (int) $amount;
            $order_number = Order::genOrderNumber();
            $stripePay = new StripePayment();
            $description = "Charge for Order: ". $order_number;

            $extra_params = [
                'description'=> $description,
                'customer'=>$stripe_customer_id
            ];
            $strip_res = $stripePay->createCharge($amount, $request->card_id, $extra_params);
            if (isset($strip_res['status']) && $strip_res['status'] == 'succeeded') {
                $order = new Order();
                $order->user_id = $user_id;
                $order->type_id = $type_id;
                $order->order_type = $type;
                $order->coupon_code = $prices_cal['coupon_code'];
                $order->coupon_discount_amount = $prices_cal['discount'];
                $order->delivery_charge = $prices_cal['delivery_fee'];
                $order->tax = $prices_cal['tax'];
                $order->service_fee = $prices_cal['service_fee'];
                $order->sub_total = $prices_cal['sub_total'];
                $order->total_amount = $prices_cal['total'];
                $order->admin_commission = $prices_cal['admin_commission'];
                $order->delivery_date = $delivery_date;
                //$order->delivery_date = date('Y-m-d H:i:s');
                $order->order_number = $order_number;
                if (!empty($request->order_type)) {
                    $order->reach_type = $request->order_type;
                }

                if (!empty($request->address_id)) {
                    $address_row = UserAddress::where(['id'=>$request->address_id])->first();
                    $order->address_id = $address_row->id;
                    $order->address = $address_row->address;
                    $order->address_lat = $address_row->lat;
                    $order->address_lng = $address_row->lng;
                } else {
                    $order->address = $request->address;
                    $order->address_lat = $request->address_lat;
                    $order->address_lng = $request->address_lng;
                }
                isset($request->special_instructions) ? $order->special_instruction = $request->special_instructions : null;
                $order->delivery_instruction = isset($request->delivery_instruction) ? $request->delivery_instruction : null;
                $order->stripe_transaction_id = $strip_res['id'];
                $order->save();
                $order_id = $order->id;
                $curr_date = date('Y-m-d H:i:s');

                //pr($cart_price_data);
                //pr($cart_ids_arr); die;
                $order_items = [];
                $restroid = [];
                foreach ($cart_ids_arr as $key => $cart_id) {
                    if (isset($cart_data_arr[$cart_id])) {
                        $cart_row = $cart_data_arr[$cart_id];
                        $bartender = $cart_row->bartender;
                        $special_ins = null;
                        $menu_item = $cart_row->menuItemDetails;
                        if (!empty($cart_row->menu_item_id)) {
                            $special_ins = isset($ins_arr[$cart_row->menu_item_id]) ? $ins_arr[$cart_row->menu_item_id] : null;
                        }
                        $from_time = strtotime($cart_row->from_time);
                        $to_time = strtotime($cart_row->to_time);

                        $total_hours = round(abs($to_time - $from_time) / 3600);
                        $prices_cal['price'] = $discounted_prices[$cart_id];
                        $prices_cal['quantity'] = $cart_row->quantity;
                        $prices_cal_each = Cart::calculatePricesEachItem($prices_cal);

                        $order_items[] = [
                            'order_id'=> $order_id,
                            'restaurant_id'=> $cart_row->restaurant_id,
                            'type'=> $cart_row->type,
                            'diy_id'=> $cart_row->diy_id,
                            'menu_item_id'=> $cart_row->menu_item_id,
                            'ingredient_id'=> $cart_row->ingredient_id,
                            'bartender_id'=>$cart_row->bartender_id,
                            'book_date'=>$cart_row->book_date,
                            'from_time'=>$cart_row->from_time,
                            'to_time'=>$cart_row->to_time,
                            'time_in_hours'=>$total_hours,
                            'price'=> $discounted_prices[$cart_id],
                            'quantity'=> $cart_row->quantity,
                            'attribute_selected'=> $cart_row->attribute_selected,
                            'special_instructions'=> $special_ins,
                            'no_of_guest'=> $cart_row->no_of_guest,
                            'service_fee'=> $prices_cal_each['service_fee'],
                            'tax'=> $prices_cal_each['tax'],
                            'admin_commission'=> $prices_cal_each['admin_commission'],
                            'created_at'=> $curr_date,
                            'updated_at'=> $curr_date,
                        ];
                    }
                    if(!in_array($cart_row->restaurant_id, $restroid)){
						$title = "Order Confirmed.";
						$message = "Order has been successfully placed.";
						sendNotification($cart_row->restaurant_id,$title,$message,"Order",$order_id);
					}
					$restroid[] = $cart_row->restaurant_id;

                }
                if ($order_items) {
                    OrderItem::insert($order_items);
                }
                $order_row = Order::where(['id'=>$order_id])
                ->first();
                // Cart::deleteCart($user_id);
                Cart::deleteCartByTypeId($cart_ids_arr);

				$title = "Order Confirmed.";
				$message = "Order has been successfully placed.";

				sendNotification($user_id,$title,$message,"Order",$order_id);



                return response()->json([
                    'status' => true,
                    'message' => 'Order has been successfully placed.',
                    'data' =>$order_row
                ]);
            } else {
                if (isset($strip_res['failure_message'])) {
                    return response()->json(['status' => false, 'message' => $strip_res['failure_message']]);
                } else {
                    return response()->json(['status' => false, 'message' => 'Some error occurred']);
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage().$e->getLine(), 'data'=>[]]);
        }
    }

    public function applyCouponCode(Request $request)
    {
        try {
            $rules = [
                'coupon_code'=>'required',
                'lat'=>'required',
                'lng'=>'required',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;

            $coupon_check = Coupon::checkCoupon($request->coupon_code, $user_id);
            if ($coupon_check['status'] == false) {
                return response()->json($coupon_check);
            }
            $coupon = $coupon_check['data'];

            $ep = ['coupon'=>$coupon];
            $carts = Cart::getCarts($user_id, $request->lat, $request->lng, $ep);
            $carts['coupon_data'] = $coupon;
            if ($carts['total'] < 1) {
                return response()->json([
                    'status' => false,
                    'message' => 'This coupon is not valid for this order.'
                ]);
            }
            return response()->json([
                'status' => true,
                'message' => 'Coupon code applied',
                'data'=> $carts
            ]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function addToCartDiy(Request $request)
    {
        try {
            $rules = [
                'diy_id'=>'required|exists:do_it_yourself,id',
                'quantities'=>'required|json',
                'ingredients'=>'required|json',
                'type_id'=>'required|exists:types,id'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;

            // comment for stop different category product in cart
            // $is_premade_added = Cart::where(['user_id'=>$user_id, 'type'=>'PREMADE'])->exists();

            // if ($is_premade_added) {
            //     return response()->json([
            //         'status' => false,
            //         'cart_status'=>false,
            //         'message' => 'Premade items already added in cart.',
            //         'data'=> []
            //     ]);
            // }

            // $other_type = Cart::where(['user_id'=>$user_id])
            //     ->where('type_id', '!=', $request->type_id)
            //     ->first();
            // if (!empty($other_type->id)) {
            //     $message = 'You have product from another category in your cart.';
            //     return response()->json([
            //         'status' => false,
            //         'cart_status'=>true,
            //         'message' => $message,
            //         'data'=> []
            //     ]);
            // }

            $added_rows = Cart::where(['user_id'=>$user_id,'type'=>'DIY','type_id'=>$request->type_id])->get();
            $added_count = count($added_rows);
            $added_restaurants = [];
            foreach ($added_rows as $key => $row) {
                $added_restaurants[$row->restaurant_id] = $row->restaurant_id;
            }


            $quantities = json_decode($request->quantities, true);
            $ingredients = json_decode($request->ingredients, true);

            // comment for stop different category product in cart

            $current_item_count = count($quantities);
            if ($current_item_count > 3) {
                return response()->json([
                    'status' => false,
                    'cart_status'=>false,
                    'message' => 'Only 3 item can be added in cart.',
                    'data'=> []
                ]);
            }
            $count_total = $added_count + $current_item_count;

            if ($count_total > 3) {
                return response()->json([
                    'status' => false,
                    'cart_status'=>false,
                    'message' => 'Only 3 items can be added in cart.',
                    'data'=> []
                ]);
            }
            foreach ($ingredients as $key => $row) {
                $added_restaurants[$row['restaurant_id']] = $row['restaurant_id'];
            }
            $rest_count = count($added_restaurants);
            if ($rest_count > 2) {
                return response()->json([
                    'status' => false,
                    'cart_status'=>false,
                    'message' => "Only 2 restaurant's items can be added in cart.",
                    'data'=> []
                ]);
            }

            $q_arr = [];
            $attribute_selected_arr = [];
            $data = [];
            foreach ($quantities as $key => $row) {
                $q_arr[$row['ingredient_id']] = $row['quantity'];
                if(isset($row['attribute_selected'])) {
                    $attribute_selected_arr[$row['ingredient_id']] = $row['attribute_selected'];
                }
            }
            // echo "<pre>";
            // print_r($q_arr);
            // print_r($attribute_selected_arr);
            // exit;
            $curr_date_time = date('Y-m-d H:i:s');
            $carts = [];
            foreach ($ingredients as $key => $row) {
                $cart['user_id'] = $user_id;
                $cart['diy_id'] = $request->diy_id;
                $cart['restaurant_id'] = $row['restaurant_id'];
                $cart['type_id'] = $request->type_id;
                $cart['type'] = 'DIY';
                $cart['ingredient_id'] = $row['ingredient_id'];
                $cart['quantity'] = $q_arr[$row['ingredient_id']];
                if(isset($attribute_selected_arr[$row['ingredient_id']])) {
                    $cart['attribute_selected'] = $attribute_selected_arr[$row['ingredient_id']];
                }
                $cart['created_at'] = $curr_date_time;
                $cart['updated_at'] = $curr_date_time;
                $carts[] = $cart;
            }
            Cart::insert($carts);

            Cart::where(['quantity'=>0])->delete();
            return response()->json([
                'status' => true,
                'cart_status'=>false,
                'message' => 'Cart updated',
                'data'=> []
            ]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getLine().' '.$e->getMessage(), 'data'=>[]]);
        }
    }

    public function getOrders(Request $request)
    {
        try {
            $rules = [
                'type_id'=>'nullable|exists:types,id'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }

            $user_id = JWTAuth::user()->id;
            $order_items = OrderItem::select('order_items.*')
                ->with(['order', 'bartender', 'menuItemDetail.images', 'ingredientDetail.diy', 'restaurentDetail'])
                ->leftJoin('orders', 'order_items.order_id', '=', 'orders.id')
                ->where(['orders.user_id'=>$user_id, 'orders.type_id'=> $request->type_id])
                ->groupBy('order_id')
                ->groupBy('restaurant_id')
                ->orderBy('order_items.id', 'DESC')
                ->paginate(10);

            return response()->json([
                'status' => true,
                'message' => 'Orders',
                'data'=> $order_items
            ]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function orderDetail(Request $request)
    {
        try {
            $rules = [
                'order_id'=>'nullable|exists:orders,id'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }

            $user_id = JWTAuth::user()->id;
            $order = Order::select('orders.*')
                ->with(['orderItems.menuItemDetail.images', 'orderItems.ingredientDetail', 'orderItems.restaurentDetail', 'orderItems.driverDetail', 'orderItems.bartender', 'orderItems.diyDetails'])
                ->where(['orders.id'=> $request->order_id])
                ->first();

            $diy_org = $item_data = $restaurant_org = $restaurant_items = $restaurants = $diy_arr = [];
            $order_items = $order->orderItems;
            if ($order->order_type != 'BARTENDER') {
                unset($order->orderItems);
            }


            // if ($order->order_type == 'PREMADE') {
                $drivers = [];
                $restaurant_tip = [];
                foreach ($order_items as $key=> $order_item) {
                    if ($order_item->tip_percent) {
                        $restaurant_tip[$order_item->restaurant_id] = $order_item->tip_percent;
                    }
                    // $order_item->diy_details = $order_item->diyDetails;


                    $drivers[$order_item->restaurant_id] = $order_item->driverDetail;
                    $restaurants[$order_item->restaurant_id] = $order_item->restaurentDetail;
                    unset($order_item->restaurentDetail);
                    // if($order_item->type == 'PREMADE') {
                        $restaurant_items[$order_item->restaurant_id][] = $order_item;
                    // }

                    // if($order_item->type == 'DIY') {
                    //     $diy_arr[$order_item->diy_id] = $order_item->diyDetails;
                    //     unset($order_item->diyDetails);
                    //     $item_data[$order_item->diy_id][$order_item->restaurant_id][] = $order_item;
                    // }
                }

                if($restaurants) {
                    foreach ($restaurants as $rest_id => $rest_detail) {
                        $rest_items = isset($restaurant_items[$rest_id]) ? $restaurant_items[$rest_id] : [];
                        // $rest_detail->restaurant_items = $rest_items;
                        $rest_detail->carts = $rest_items;
                        $rest_detail->driverDetail = isset($drivers[$rest_id]) ? $drivers[$rest_id] : null;
                        $rest_detail->is_tip_given = isset($restaurant_tip[$rest_id]) ? 1 : 0;
                        $rest_detail->is_review_given = Review::checkReviewGiven($order->id, $rest_id, $user_id);
                        $restaurant_org[] = $rest_detail;
                    }
                    $order->restaurants = $restaurant_org;
                }
                // if($item_data) {
                //     foreach ($item_data as $diy_id => $diy_data) {
                //         $restaurant_org = [];
                //         foreach ($diy_data as $rest_id => $rest_data) {
                //         $rest_detail = isset($restaurants[$rest_id]) ? $restaurants[$rest_id] : null;
                //         $rest_detail->restaurant_items = $rest_data;
                //         $rest_detail->driverDetail = isset($drivers[$rest_id]) ? $drivers[$rest_id] : null;
                //         $rest_detail->is_tip_given = isset($restaurant_tip[$rest_id]) ? 1 : 0;
                //         $rest_detail->is_review_given = Review::checkReviewGiven($order->id, $rest_id, $user_id);
                //         $restaurant_org[] = $rest_detail;
                //         }
                //         $diy_detail = isset($diy_arr[$diy_id]) ? $diy_arr[$diy_id] :null;
                //         $diy_detail->restaurants = $restaurant_org;
                //         $diy_org[] = $diy_detail;
                //         }
                //     $order->diys = $diy_org;
                // }

            // }
            // elseif ($order->order_type == 'DIY') {
            //     if ($order->order_type == 'DIY') {
            //     foreach ($order_items as $key=> $order_item) {
            //             if ($order_item->tip_percent) {
            //                 $restaurant_tip[$order_item->restaurant_id] = $order_item->tip_percent;
            //             }
            //             $drivers[$order_item->restaurant_id] = $order_item->driverDetail;
            //             $restaurants[$order_item->restaurant_id] = $order_item->restaurentDetail;
            //             unset($order_item->restaurentDetail);
            //             $diy_arr[$order_item->diy_id] = $order_item->diyDetails;
            //             unset($order_item->diyDetails);
            //             $item_data[$order_item->diy_id][$order_item->restaurant_id][] = $order_item;
            //     }

            //     foreach ($item_data as $diy_id => $diy_data) {
            //         $restaurant_org = [];
            //         foreach ($diy_data as $rest_id => $rest_data) {
            //             $rest_detail = isset($restaurants[$rest_id]) ? $restaurants[$rest_id] : null;
            //             $rest_detail->restaurant_items = $rest_data;
            //             $rest_detail->driverDetail = isset($drivers[$rest_id]) ? $drivers[$rest_id] : null;
            //             $rest_detail->is_tip_given = isset($restaurant_tip[$rest_id]) ? 1 : 0;
            //             $rest_detail->is_review_given = Review::checkReviewGiven($order->id, $rest_id, $user_id);
            //             $restaurant_org[] = $rest_detail;
            //         }
            //         $diy_detail = isset($diy_arr[$diy_id]) ? $diy_arr[$diy_id] :null;
            //         $diy_detail->restaurants = $restaurant_org;
            //         $diy_org[] = $diy_detail;
            //     }
            //     $order->diys = $diy_org;
            // }

            //pr($order); die;



            return response()->json([
                'status' => true,
                'message' => 'Order details',
                'data'=> $order
            ]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function getCartBadge()
    {
        $user_id = JWTAuth::user()->id;
        $data = Cart::where(['user_id'=>$user_id])->sum('quantity');
        $carts = Cart::select(DB::raw("sum(quantity) as total_qty"),'type_id')->where(['user_id'=>$user_id])->where('type_id','!=',4)->groupBy('type_id')->get();
        if($carts) {
            foreach($carts as $cart) {
                $d[$cart->type_id] = $cart->total_qty;
            }
        }
        // echo "<pre>";
        // print_r($d);

        return response()->json([
            'status' => true,
            'message' => 'Cart badge Count',
            'data'=> (int)$data,
            'drinks'=> (int)isset($d[1]) ? $d[1] :0,
            'foods'=> (int)isset($d[2]) ? $d[2] :0,
            'ambaince'=> (int)isset($d[3]) ? $d[3] :0,
        ]);
    }

    public function addToCartBartender(Request $request)
    {
        try {
            $rules = [
                'bartender_id'=>'required|exists:users,id',
                'book_date'=>'required',
                'from_time'=>'required',
                'to_time'=>'required',
                'quantity'=>'required',
                'no_of_guest'=>'nullable|numeric'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }

            if ($request->to_time <= $request->from_time) {
                return response()->json(['status' => false, 'message' => 'To time should be greater than from time.']);
            }

            $user_id = JWTAuth::user()->id;

            $type_name_arr = Config::get('params.type_name_arr');


            $already_added = Cart::where(['user_id'=>$user_id])
            ->first();

            if (!empty($already_added->id)) {
                $type_name = $type_name_arr[$already_added['type']];
                $message = $type_name.' items already added in cart.';
                return response()->json([
                    'status' => false,
                    'cart_status'=>true,
                    'message' => $message,
                    'data'=> []
                ]);
            }

            $available_params = [
                'bartender_id'=> $request->bartender_id,
                'book_date'=> $request->book_date,
                'from_time'=> $request->from_time,
                'to_time'=> $request->to_time
            ];
            $is_available = User::checkAvailability($available_params);
            if ($is_available == 0) {
                return response()->json([
                    'status' => false,
                    'cart_status'=>false,
                    'message' => 'Bartender is not available on selected time.',
                    'data'=> []
                ]);
            }

            $bartender = User::where(['id'=>$request->bartender_id])->first();
            $entity = new Cart();
            $entity->user_id = $user_id;
            $entity->bartender_id = $request->bartender_id;
            $entity->restaurant_id = $bartender->vendor_id;
            $entity->type_id = 4;
            $entity->type = 'BARTENDER';
            $entity->book_date = $request->book_date;
            $entity->from_time = $request->from_time;
            $entity->to_time = $request->to_time;
            $entity->quantity = $request->quantity;
            $entity->no_of_guest = isset($request->no_of_guest) ? $request->no_of_guest : null;
            $entity->save();

            return response()->json([
                'status' => true,
                'cart_status'=>false,
                'message' => 'Cart updated',
                'data'=> []
            ]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function getTipPercentage()
    {
        try {
            $tips = getSettings()['tip_percentage'];
            $tips = explode(',', $tips);
            return response()->json([
            'status' => true,
            'message' => 'Tips',
            'data'=> $tips
        ]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }


    public function addTip(Request $request)
    {
        try {
            $rules = [
                'order_id'=>'required|exists:orders,id',
                'restaurant_id'=>'required|exists:users,id',
                'tip_percent'=>'required|numeric',
                'tip_remark'=>'nullable',
                'card_id' => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }

            $user = JWTAuth::user();
            $user_id = $user->id;
            $stripe_customer_id = $user->stripe_customer_id;
            $order = Order::where(['id'=>$request->order_id])->first();
            $order_items = OrderItem::where(['order_id'=>$request->order_id, 'restaurant_id'=> $request->restaurant_id])
            ->orderBy('order_items.id', 'ASC')
            ->get();
            $description = "Tip for Order: ". $order->order_number;

            $driver_id = null;
            $sub_total_price = 0;
            foreach ($order_items as $i_key => $item_row) {
                $sub_total_price += $item_row->price * $item_row->quantity;
                $driver_id = $item_row->driver_id;
            }

            $tip_amount = ($sub_total_price/100) * $request->tip_percent;
            $stripePay = new StripePayment();
            $tip_multiple = $tip_amount * 100;
            $tip_multiple = (int) $tip_multiple;
            $extra_params = [
                'description'=> $description,
                'customer'=>$stripe_customer_id
            ];
            $strip_res = $stripePay->createCharge($tip_multiple, $request->card_id, $extra_params);
            if (isset($strip_res['status']) && $strip_res['status'] == 'succeeded') {
                $wallet_params = [
                'user_id'=>$driver_id,
                'order_id'=>$request->order_id,
                'transaction_type'=>'Tip',
                'type'=>'CR',
                'amount'=>$tip_amount
                ];
                Wallet::addEntryWallet($wallet_params);

                $order_item = OrderItem::where(['order_id'=>$request->order_id, 'restaurant_id'=> $request->restaurant_id])
                ->groupBy('order_id')
                ->groupBy('restaurant_id')
                ->orderBy('order_items.id', 'DESC')
                ->first();
                $order_item->tip_percent = $request->tip_percent;
                $order_item->tip_remark = $request->tip_remark;
                $order_item->tip_amount = $request->tip_amount;
                $order_item->save();

                return response()->json([
                'status' => true,
                'message' => 'Tip added.',
                'data'=> []
                ]);
            } else {
                if (isset($strip_res['failure_message'])) {
                    return response()->json(['status' => false, 'message' => $strip_res['failure_message']]);
                } else {
                    return response()->json(['status' => false, 'message' => 'Some error occurred']);
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function addReview(Request $request)
    {
        try {
            $rules = [
                'order_id'=>'required|exists:orders,id',
                'restaurant_id'=>'required',
                'restaturant_rating'=>'required|numeric',
                'restaturant_remark'=>'nullable',
                'driver_id'=>'nullable',
                'driver_rating'=>'nullable'
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            $entity = new Review();

            $entity->user_id = $user_id;
            $entity->order_id = $request->order_id;
            $entity->restaurant_id = $request->restaurant_id;
            $entity->restaturant_rating = $request->restaturant_rating;
            $entity->restaturant_remark = isset($request->restaturant_remark) ? $request->restaturant_remark : null;
            $entity->driver_id = isset($request->driver_id) ? $request->driver_id : null;
            $entity->driver_rating = isset($request->driver_rating) ? $request->driver_rating : null;
            $entity->save();

            $rest_rating = Review::where(['restaurant_id'=>$request->restaurant_id])
            ->avg('restaturant_rating');
            User::where(['id'=>$request->restaurant_id])->update([
                'rating'=>$rest_rating
            ]);
            if(!empty($request->driver_id)){
                $rating = Review::where(['driver_id'=>$request->driver_id])
                ->avg('driver_rating');
                User::where(['id'=>$request->driver_id])->update([
                    'rating'=>$rating
                ]);
            }

            return response()->json([
                'status' => true,
                'message' => 'Review added.',
                'data'=> []
            ]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function orderCreditWallet(Request $request)
    {
        try {
            $rules = [
                'order_id'=>'required|exists:orders,id',
                'restaurant_id'=>'required',
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $params = $request->all();
            $status = Order::getOrderRestaurantAmounts($params);
            return response()->json([
                'status' => true,
                'message' => 'Complete',
                'data'=> []
            ]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function refund()
    {
        try {
            $stripePay = new StripePayment();
            $strip_res = $stripePay->transfer(20, 'acct_1Hx5Fr2RXkD8yqhk');
            die;
            $customers = User::where('role', '2')
            ->whereNull('stripe_customer_id')
            ->get();
            foreach ($customers as $user) {
                if ($user->email) {
                    $stripePay = new StripePayment();
                    $strip_res = $stripePay->createCustomer($user);
                    $user->stripe_customer_id = $strip_res->id;
                    $user->save();
                }
            }
            die('com');
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }
    public function cartItemRemove(Request $request)
    {
        try {
            $rules = [
                'cart_id'=>'required|exists:carts,id',
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            $entity = new Review();

            Cart::where('id',$request->cart_id)->delete();

            return response()->json([
                'status' => true,
                'message' => 'Cart item removed.',
                'data'=> []
            ]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }
    public function orderCancel(Request $request)
    {
        try {
            $rules = [
                'order_id'=>'required|exists:orders,id',
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            $entity = new Review();

            $order = Order::where('id',$request->order_id)->with('items')->first();
            // echo $order->items->where('status','NEW')->count();
            // exit;
            if($order->items->where('status','NEW')->where('status','!=','REJECTED')->count() == $order->items->where('status','!=','REJECTED')->count()) {
                $order->status = 'CANCELLED';
                $order->save();
                DB::table('order_items')->where('order_id',$order->id)->update(['status'=>'CANCELLED']);
                $message = 'Order Cancelled.';
                $status = true;
                $this->refundOrderAmount($order);
            }
            else {
                $message = "Order can't be cancelled as one of the order item is already accepted.";
                $status = false;

            }


            return response()->json([
                'status' => $status,
                'message' => $message,
                'data'=> $order
            ]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }
    public function refundOrderAmount($order)
    {
        try {

            if($order->stripe_transaction_id) {
                $stripePay = new StripePayment();
                $strip_res = $stripePay->refund($order->stripe_transaction_id, $order->total_amount);
            }

        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }
    public function checkIngredientStock($vendor_id,$ingredient_id,$diy_id) {
        return VendorIngredient::where(['vendor_id'=>$vendor_id,'ingredient_id'=>$ingredient_id,'diy_id'=>$diy_id,'in_stock'=>'Yes'])->count();
    }
    public function updateIngredientsQty(Request $request) {
        try {
            $rules = [
                'restaurant_id'=>'required|numeric|exists:users,id',
                'ingredient_id'=>'required|numeric|integer|min:1',
                'qty'=>'required|numeric|integer|min:0',
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            $entity = new Review();
            $cart = Cart::where(['user_id'=>$user_id,'restaurant_id'=>$request->restaurant_id,'ingredient_id'=>$request->ingredient_id])->first();
            if($cart) {
            if($this->checkIngredientStock($request->restaurant_id,$request->ingredient_id,$cart->diy_id) > 0) {
                $cart->quantity = $request->qty;
                $cart->save();

                Cart::where(['quantity'=>0])->delete();

                return response()->json([
                    'status' => true,
                    'message' => 'Ingredient Quantity updated successfully.',
                    'data'=> $cart
                ]);
            }
            else {
                $cart->delete();
                return response()->json([
                    'status' => false,
                    'message' => 'Ingredient item out of stock.',
                    'data'=> []
                ]);
            }

            }
            else {
                return response()->json([
                    'status' => false,
                    'message' => 'Record not found.',
                    'data'=> []
                ]);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }
}//end class.
