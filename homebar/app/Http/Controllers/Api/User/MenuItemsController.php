<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Validator;
use \Config;
use JWTAuth;
use JWTAuthException;
use App\Lib\Uploader;
use App\Model\User;
use App\Model\Category;
use App\Model\Type;
use App\Model\MenuItem;
use App\Model\Brand;
use App\Model\Cart;

class MenuItemsController extends Controller
{
    public function getTypes()
    {
        try {
            $data = Type::get();
            return response()->json(['status' => true, 'message' => 'Types.', 'data' =>$data]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }
    public function getCategories(Request $request)
    {
        try {
            $status_arr = Config::get('params.status_arr');
            $validatorRules = [
                'type_id'=>'required|exists:types,id',
                'type'=>'required|in:PREMADE,DIY'
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $conditions = ['status'=>$status_arr['active'], 'type_id'=>$request->type_id];
            if($request->type == 'PREMADE'){
                $conditions['is_pre_made'] = 1;
            } else {
                $conditions['is_do_it_yourself'] = 1;
            }

            $category = Category::where($conditions)
            ->whereNull('parent_id')
            ->orderBy('created_at','asc')->get();

            return response()->json(['status' => true, 'message' => 'Categories.', 'data' =>$category]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function getBrands()
    {
        try {
            $status_arr = Config::get('params.status_arr');
            $data = Brand::where(['status'=>$status_arr['active']])->get();
            return response()->json(['status' => true, 'message' => 'Brands.', 'data' =>$data]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function getMenuItemsOld(Request $request)
    {
        try {
            $rules = [
                'restaurant_id'=>'nullable|exists:users,id',
                'type_id'=>'nullable|exists:types,id',
                'category_id'=>'nullable|exists:categories,id',
                'subcategory_id'=>'nullable|exists:categories,id',
                'paginate'=>'required|in:0,1'
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $data = MenuItem::select('*')->with(['menuItemImages', 'categoryData', 'subCategoryData']);
            $conditions['status'] ='1';
            if (! empty($request->restaurant_id)) {
                $conditions['user_id'] = $request->restaurant_id;
            }
            if (! empty($request->type_id)) {
                $conditions['type_id'] = $request->type_id;
            }
            if (! empty($request->category_id)) {
                $conditions['category_id'] = $request->category_id;
            }
            if (! empty($request->subcategory_id)) {
                $conditions['subcategory_id'] = $request->subcategory_id;
            }
            $data = $data->where($conditions);
            if ($request->paginate == 1) {
                $data = $data->paginate(10);
            } else {
                $data = $data->get(10);
            }
            return response()->json([
                'status' => true,
                'message' => 'Menu Items.',
                'data' =>$data
            ]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }


    public function getMenuItems(Request $request)
    {
        try {
            $rules = [
                'restaurant_id'=>'nullable|exists:users,id',
                'type_id'=>'nullable|exists:types,id',
                'category_id'=>'nullable|exists:categories,id',
                'subcategory_id'=>'nullable|exists:categories,id',
                'paginate'=>'required|in:0,1'
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            $cart_item_qty = Cart::getCartsProductQuantity($user_id);

            $data = MenuItem::select('*')
            ->with(['menuItemImages','getChilrensMenuItems', 'categoryData', 'subCategoryData'])
            ->whereNull('parent_id');
            $conditions['status'] ='1';
            if (! empty($request->restaurant_id)) {
                $conditions['user_id'] = $request->restaurant_id;
            }            if (! empty($request->type_id)) {
                $conditions['type_id'] = $request->type_id;
            }
            if (! empty($request->category_id)) {
                $conditions['category_id'] = $request->category_id;
            }
            if (! empty($request->subcategory_id)) {
                $conditions['subcategory_id'] = $request->subcategory_id;
            }
            $data = $data->where($conditions);
            if ($request->paginate == 1) {
                $data = $data->paginate(10)->toArray();
                $data = MenuItem::getPaginateFormattedData($data);
            } else {
                $data = $data->get()->toArray();
                $data = MenuItem::getFormattedData($data, $cart_item_qty);
            }

            return response()->json([
                'status' => true,
                'message' => 'Menu Items.',
                'data' =>$data
            ]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }



}//end class.
