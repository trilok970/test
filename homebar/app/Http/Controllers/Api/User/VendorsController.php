<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Validator;
use \Config;
use JWTAuth;
use JWTAuthException;
use App\Lib\Uploader;
use App\Model\User;
use App\Model\RestaurantLike;
use App\Model\Cart;
use App\Model\Category;
use App\Model\MenuItem;
use App\Model\Type;

class VendorsController extends Controller
{
    public function getRestaurants(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $validatorRules = [
                'type_id'=>'nullable|exists:types,id',
                'category_id'=>'nullable|exists:categories,id',
                'subcategory_type'=>'nullable|in:PREMADE,DOITYOURSELF',
                //'sort'=>'nullable|in:HTL,LTH,RATING,ALPHABETICAL',
                'asc_desc'=>'nullable|in:ASC,DESC',
                'brands'=>'nullable|json',
                'lat'=>'required',
                'lng'=>'required',
                'paginate'=>'required|in:0,1',
                'selectedCategory'=>'nullable|json',
                'reach_type'=>'nullable|in:DELIVERY,PICKUP',
                'search'=>'nullable'
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $lat = $request->lat;
            $lng = $request->lng;
            $request_data = $request->all();
            $status_arr = Config::get('params.status_arr');
            $role_ids = Config::get('params.role_ids');
            $distance_num = Config::get('params.distance_num');
            $conditions = [
                'status'=> $status_arr['active'],
                'role'=> $role_ids['vendor'],
                'availability'=> 1
            ];
            if(!empty($request->reach_type)){
                if($request->reach_type == 'DELIVERY'){
                    $conditions['is_pickup'] = '1';
                } else if($request->reach_type == 'DELIVERY'){
                    $conditions['is_delivery'] = '1';
                }
            }

            $settings = Config::get('settings');
            $distance_radius = $settings['restaurant_distance_radius'];

            $distance_query = "ROUND(( $distance_num * acos( cos( radians($lat) ) * cos( radians( users.latitude ))
            * cos( radians( users.longitude ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(users.latitude))  )), 1) ";

            $data = User::select('users.id','users.business_name', 'users.description', 'users.latitude', 'users.longitude','users.profile_image', 'users.cover_image', 'users.logo_image', 'users.rating',  DB::raw("$distance_query as distance"))
            ->where($conditions);

            if(!empty($request->asc_desc)) {
                $data = $data->orderBy('business_name', $request->asc_desc);
            }
            if(!empty($request->filter_type)) {
                if($request->filter_type == 'RATING'){
                    $data = $data->orderBy('users.rating', 'DESC');
                } else if($request->filter_type == 'NEARBY'){
                    $data = $data->orderBy('distance', 'ASC');
                } else if($request->filter_type == 'FAVOURITE'){
                    $data = $data->whereHas('restaurantsLikes', function ($query) use($user_id) {
                            $query->where('restaurant_likes.user_id', $user_id);
                    });
                }
            }
            $data = $data->where(DB::raw($distance_query), '<=', $distance_radius);
            if(!empty($request->type_id)){
                $data = $data->whereHas('userProducts', function ($query) use ($request_data){
                    $query->where('type_id', $request_data['type_id']);
                    if($request_data['category_id']){
                        $query->where('category_id', $request_data['category_id']);
                    }
                    if(!empty($request_data['selectedCategory'])) {
                        $selectedCategory = json_decode($request_data['selectedCategory']);
                        if(!empty($selectedCategory)) {
                            $query->whereIn('brand_id', $selectedCategory);
                        }

                    }
                })
                ->whereRaw('FIND_IN_SET('.$request->type_id.',business_type)');
            }
            if(!empty($request->search)){
                $serach_str = '%'.$request->search.'%';
                $data = $data->where(function($query) use($serach_str){
                    $query->where('business_name', 'like', $serach_str);
                });
            }
            if($request->paginate == 1){
                $data = $data->paginate(10);
            } else {
                $data = $data->get();
            }

            return response()->json(['status' => true, 'message' => 'Restaurants.', 'data' =>$data]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }


    public function getRestaurantDetail(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $validatorRules = [
                'category_id'=>'nullable|exists:categories,id',
                'type_id'=>'nullable|exists:types,id',
                'restaurant_id'=>'required|exists:users,id',
                'lat'=>'required',
                'lng'=>'required',
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $lat = $request->lat;
            $lng = $request->lng;
            $status_arr = Config::get('params.status_arr');
            $role_ids = Config::get('params.role_ids');
            $distance_num = Config::get('params.distance_num');

            $data = User::select('users.*', DB::raw("ROUND(( $distance_num * acos( cos( radians($lat) ) * cos( radians( users.latitude ))
            * cos( radians( users.longitude ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(users.latitude))  )), 1) as distance"))->where([
                'id'=>$request->restaurant_id,
            ])->first();

            $like_row = RestaurantLike::where([
                'user_id'=>$user_id,
                'restaurant_id'=>$request->restaurant_id
            ])
            ->first();
            $data->is_liked = !empty($like_row->id) ? 1 : 0;

            $category_menus = [];
            $conditons = ['status'=>'1'];
            if(!empty($request->type_id)) {
                $conditons['type_id'] = $request->type_id;
            }
            if(!empty($request->category_id)) {
                $conditons['id'] = $request->category_id;
            }
            $categories  = Category::where($conditons)->get();
            $product_ids = [];
            $cart_item_qty = Cart::getCartsProductQuantity($user_id);
            foreach($categories as $cat_key => $cat_row){
                $data_menu = MenuItem::select('*')->with(['getChilrensMenuItems', 'menuItemImages', 'subCategoryData'])
                ->where(['status'=>'1', 'user_id'=> $request->restaurant_id, 'category_id'=>$cat_row->id])
                ->whereNull('parent_id')
                ->get()
                ->toArray();
                $data_menu = MenuItem::getFormattedData($data_menu, $cart_item_qty);
                if(!empty($data_menu)) {
                    $cat_row->menu_items = $data_menu;
                    $category_menus[] = $cat_row;
                }
            }

            $data->categories = $category_menus;
            return response()->json(['status' => true, 'message' => 'Restaurant detail.', 'data' =>$data]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }



    public function likeDislikeRestaurant(Request $request)
    {
        try {
            $validatorRules = [
                'restaurant_id'=>'required|exists:users,id',
                'is_like'=>'required|in:0,1'
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            $like_row = RestaurantLike::where([
            'user_id'=>$user_id,
            'restaurant_id'=>$request->restaurant_id
            ])
            ->first();

            if ($request->is_like == 1) {
                $message = 'Favorite added.';
                if (empty($like_row->id)) {
                    $entity = new RestaurantLike();
                    $entity->user_id = $user_id;
                    $entity->restaurant_id = $request->restaurant_id;
                    $entity->save();
                }
            } else {
                $message = 'Favorite removed.';
                if (! empty($like_row->id)) {
                    $like_row->delete();
                }
            }
            return response()->json(['status'=>true, 'message'=>$message, 'data'=>[]]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function addToCart(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $validatorRules = [
                'restaurant_id'=>'required|exists:users,id',
                'menu_item_id'=>'required|exists:menu_items,id',
                'quantity'=>'required|numeric'
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            // comment for stop different category product in cart

            // $is_diy_added = Cart::where(['user_id'=>$user_id, 'type'=>'DIY'])->exists();
            // if($is_diy_added){
            //     return response()->json([
            //         'status' => false,
            //         'cart_status'=>true,
            //         'message' => 'Do It Yourself items already added in cart.',
            //         'data'=> []
            //     ]);
            // }


            $menu_item = MenuItem::where('id', $request->menu_item_id)
            ->first();
            $type_id = $menu_item->type_id;

            // comment for stop different category product in cart

            // $cart_types = Cart::where('carts.user_id', $user_id)
            // ->where('type_id', '!=', $type_id)
            // ->first();
            // if(!empty($cart_types->id)){
            //     return response()->json([
            //         'status'=>false,
            //         'cart_status'=>true,
            //         'message'=>'You have product from another category in your cart.',
            //         'data'=>[]
            //     ]);
            // }

            if($request->quantity == 0) {
                Cart::where(['user_id'=> $user_id, 'menu_item_id'=> $request->menu_item_id])->delete();
                return response()->json([
                    'status'=>true,
                    'cart_status'=>false,
                    'message'=>'Cart updated.',
                    'data'=>[]
                ]);
            }



            $entity = Cart::firstOrCreate([
                'user_id' => $user_id,
                'menu_item_id' => $request->menu_item_id,
            ]);
            $entity->type_id = $type_id;
            $entity->restaurant_id = $request->restaurant_id;
            $entity->quantity = $request->quantity;
            $entity->save();
            return response()->json([
                'status'=>true,
                'cart_status'=>false,
                'message'=>'Cart updated.',
                'data'=>[]
            ]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function getCarts(Request $request) {
        try {
            $validatorRules = [
                'lat'=>'required',
                'lng'=>'required',
                'order_type'=>'nullable|in:DELIVERY,PICKUP',
                'delivery_date'=>'nullable'
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $order_type = (!empty($request->order_type)) ? $request->order_type : 'DELIVERY';
            $user_id = JWTAuth::user()->id;
            $delivery_date = isset($request->delivery_date) ? $request->delivery_date : '';
            $ep = [
                'order_type'=>$order_type,
                'delivery_date'=>$delivery_date
            ];
            if($request->cart_type_id) {
                $cart_type_id = $request->cart_type_id;
            }
            else {
                $cart_type_id = 0;
            }
            $data = Cart::getCarts($user_id, $request->lat, $request->lng, $ep,$cart_type_id);
            return response()->json([
                'status'=>true,
                'message'=>'Cart details.',
                'data'=>$data
            ]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage().$e->getLine(), 'data'=>[]]);
        }
    }
    public function get_type_id($slug) {
        return Type::select('id')->where('slug',strtolower($slug))->first()->id ?? 0;
    }
    public function deleteCart(Request $request){
        try {

            $user_id = JWTAuth::user()->id;
            if($request->cart_type_id)
            {
                $cart_type_id = $request->cart_type_id;
            }
            else {
                $cart_type_id = 0;
            }
            $entity = Cart::where('user_id',$user_id)
            ->when($cart_type_id,function($q) use ($cart_type_id){
                $q->where('type_id',$cart_type_id);
            })
            ->delete();
            //$data = Cart::getCarts($user_id);
            return response()->json([
                'status'=>true,
                'message'=>'Item has been deleted from the cart.',
                'data'=>[]
            ]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

}//end class.
