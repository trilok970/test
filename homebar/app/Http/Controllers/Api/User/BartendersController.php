<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Validator;
use \Config;
use JWTAuth;
use JWTAuthException;
use App\Lib\Uploader;
use App\Model\User;
use App\Model\RestaurantLike;
use App\Model\Cart;
use App\Model\Category;
use App\Model\MenuItem;
use App\Model\Offer;

class BartendersController extends Controller
{
    public function getBartenders(Request $request)
    {
        try {
            $validatorRules = [
                'sort_type'=>'nullable|in:HTL,LTH,POPULARITY,RATING',
                'asc_desc'=>'nullable|in:ASC,DESC',
                'options'=>'nullable|in:INDIVIDUAL,GROUP',
                'lat'=>'required',
                'lng'=>'required',
                'paginate'=>'required|in:0,1',
                'search'=>'nullable'
                
            ];
            $validator = Validator::make($request->all(), $validatorRules);
    
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $lat = $request->lat;
            $lng = $request->lng;
            $distance_num = Config::get('params.distance_num');
            $status_arr = Config::get('params.status_arr');
            $role_ids = Config::get('params.role_ids');
            $condtions = [
                'users.status'=>$status_arr['active'],
                'users.role'=>$role_ids['bartender']
            ];

            if(!empty($request->options)) {
                if($request->options == 'INDIVIDUAL'){
                    $condtions['users.options'] = '1';
                } else if($request->options == 'GROUP'){
                    $condtions['users.options'] = '2';
                }
            }

            
            $distance_str = "ROUND(( $distance_num * acos( cos( radians($lat) ) * cos( radians( users.latitude )) 
            * cos( radians( users.longitude ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(users.latitude))  )), 1) as distance";

            $data = User::select('users.id','users.first_name', 'users.last_name', 'users.description', 'users.latitude', 'users.vendor_id', 'users.longitude','users.profile_image', 'users.rating', 'users.experience','users.bio','users.price_per_hour', 'users.price', DB::raw($distance_str.", IF(users.options='1', 'INDIVIDUAL', 'GROUP') AS options, COUNT(distinct bartender_group_members.id)+1 AS group_of, COUNT(order_items.id) AS orders_count"))
            ->leftJoin('bartender_group_members', function ($join) {
                $join->on('bartender_group_members.bartender_id', '=', 'users.id');
            })

            ->leftJoin('order_items', function ($join) {
                $join->on('order_items.bartender_id', '=', 'users.id');
            })
            ->where($condtions);

            if(!empty($request->options) && $request->options == 'GROUP') {
                $data = $data->having(DB::raw("COUNT(bartender_group_members.id)+1"), '>', 1);
            }

            if(!empty($request->search)){
                $serach_str = '%'.$request->search.'%';
                $data = $data->where(function($query) use($serach_str){
                    $query->where('business_name', 'like', $serach_str)
                        ->orWhere('first_name', 'like', $serach_str)
                        ->orWhere('last_name', 'like', $serach_str);
                });
            }

            $order_by = 1;
            if(!empty($request->asc_desc)) {
                $order_by = 0;
                $data = $data->orderBy('first_name', $request->asc_desc);
            }
            if(!empty($request->asc_desc)) {
                $order_by = 0;
                $data = $data->orderBy('first_name', $request->asc_desc);
            }
            if(!empty($request->sort_type)) {
                $order_by = 0;
                if($request->sort_type == 'RATING'){
                    $data = $data->orderBy('users.rating', 'DESC');
                } else if($request->sort_type == 'HTL') {
                    $data = $data->orderBy('users.price_per_hour', 'DESC');
                } else if($request->sort_type == 'LTH') {
                    $data = $data->orderBy('users.price_per_hour', 'ASC');
                }else if($request->sort_type == 'POPULARITY') {
                    $data = $data->orderBy('orders_count', 'DESC');
                }

            }
            if($order_by){
                $data = $data->orderBy('distance', 'ASC');
            }
            $data = $data->groupBy('users.id');
            if($request->paginate == 1){
                $data = $data->paginate(10)->toArray();
                $data['data'] = User::getFormattedBartender($data['data']);
            } else {
                $data = $data->get()->toArray();
                $data = User::getFormattedBartender($data);
            }

            return response()->json(['status' => true, 'message' => 'Restaurants.', 'data' =>$data]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }


    public function getBartenderDetail(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $validatorRules = [
                'bartender_id'=>'required|exists:users,id',
                'lat'=>'required',
                'lng'=>'required',
            ];
            $validator = Validator::make($request->all(), $validatorRules);
    
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $lat = $request->lat;
            $lng = $request->lng;
            $distance_num = Config::get('params.distance_num');
            $status_arr = Config::get('params.status_arr');
            $role_ids = Config::get('params.role_ids');

            $distance_str = "ROUND(( $distance_num * acos( cos( radians($lat) ) * cos( radians( users.latitude )) 
            * cos( radians( users.longitude ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(users.latitude))  )), 1) as distance";

            $data = User::select('users.id','users.first_name', 'users.last_name', 'users.description', 'users.cover_image', 'users.latitude', 'users.longitude','users.profile_image', 'users.rating', 'users.experience','users.bio','users.price_per_hour', 'users.vendor_id', 'users.price', DB::raw($distance_str.", IF(users.options='1', 'INDIVIDUAL', 'GROUP') AS options, COUNT(bartender_group_members.id)+1 AS group_of"))
            ->leftJoin('bartender_group_members', function ($join) {
                $join->on('bartender_group_members.bartender_id', '=', 'users.id');
            })
            ->where([
                'users.id'=>$request->bartender_id,
            ])
            ->first();
            
            $params = [
                'vendor_id'=> $data->vendor_id,
                'type_id'=> 4,
                'price'=> $data->price_per_hour
            ];
            $discounted_price = Offer::getDiscountedPrice($params);
            $data->discounted_price = $discounted_price;


            $like_row = RestaurantLike::where([
                'user_id'=>$user_id,
                'restaurant_id'=>$request->bartender_id
            ])
            ->first();
            $data->is_liked = !empty($like_row->id) ? 1 : 0;

            return response()->json(['status' => true, 'message' => 'Bartender detail.', 'data' =>$data]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    
}//end class.
