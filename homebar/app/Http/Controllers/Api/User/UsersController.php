<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use \Config;
use JWTAuth;
use JWTAuthException;
use App\Lib\Uploader;
use App\Lib\StripePayment;
use App\Model\User;
use App\Model\Country;
use App\Model\SocialAccount;
use App\Model\Notification;
use App\Model\FaqCategory;
use App\Model\ContactRequest;
use App\Model\UserSubscription;
use App\Model\Wallet;
use App\Model\Plan;

class UsersController extends Controller
{
    public function getCountries()
    {
        $all_country = Country::orderBy('name', 'asc')->get();
        return response()->json(['status' => true, 'message' => 'Listing.', 'data' => $all_country]);
    }

    public function checkSignup(Request $request)
    {
        $niceNames = [
            'phone' => 'Phone Number',
        ];
        $validatorRules = [
            'email' => 'nullable|email|max:255',
            'country_id' => 'required|numeric|digits_between:1,10|exists:countries,id',
            'phone' => 'required|numeric|digits_between:7,15',
            // 'full_name'=>'required|max:255',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'dob' => 'nullable|date_format:"Y-m-d"',
            'device_type' => 'required|in:IPHONE,ANDROID',
            'device_id' => 'required',
            'password' => 'required|max:20|min:8',

        ];
        try {
            $validator = Validator::make($request->all(), $validatorRules);
            $validator->setAttributeNames($niceNames);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            } else {
                $email = isset($request->email) ? $request->email : null;
                $user_unique_check = User::checkUserUnique($email, $request->phone);
                if ($user_unique_check['status'] == false) {
                    return response()->json($user_unique_check);
                }

                $country_data = Country::where([['id', $request->country_id]])->first();
                $country_code = isset($country_data->phonecode) ? $country_data->phonecode : '';
                $country_code_mobile = '+'.$country_code . $request->phone;

                $otp = $this->sendOtp($country_code_mobile);
                $data = ['otp' => $otp];
                return response()->json([
                    'status' => true,
                    'message' => 'User can register',
                    'data' => $data
                ]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    } // end function.

    public function signup(Request $request)
    {
        $validatorRules = [
            'email' => 'nullable|email|max:255',
            'country_id' => 'required|numeric|digits_between:1,10|exists:countries,id',
            'phone' => 'required|numeric|digits_between:7,15',
            // 'full_name'=>'required|max:255',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'dob' => 'nullable|date_format:"Y-m-d"',
            'device_type' => 'required|in:IPHONE,ANDROID',
            'device_id' => 'required',
            'otp' => 'required',
            'password' => 'required|max:20|min:8',
        ];
        try {
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            } else {
                $email = isset($request->email) ? $request->email : null;
                $user_unique_check = User::checkUserUnique($email, $request->phone);
                if ($user_unique_check['status'] == false) {
                    return response()->json($user_unique_check);
                }
                $status_arr = \Config::get('params.status_arr');
                $country_data = Country::where([['id', $request->country_id]])->first();
                $country_code = isset($country_data->phonecode) ? $country_data->phonecode : '';
                $country_code_mobile = $country_code . $request->phone;

                $user = new User();
                // $user->full_name = $request->full_name;
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->dob = isset($request->dob) ? $request->dob : null;
                $user->email = $request->email;
                $user->role = Config::get('params.role_ids.user');
                $user->status = $status_arr['active'];
                $user->phone = $request->phone;
                $user->country_id = $request->country_id;
                $user->phone_country_code = $country_code_mobile;
                $user->password = Hash::make($request->password);
                $user->save();

                $stripePay = new StripePayment();
                $strip_res = $stripePay->createCustomer($user);
                $user->stripe_customer_id = $strip_res->id;
                $user->save();

                $data = [
                    'phone' => $user->phone,
                    'country_code' => $country_code,
                    'password' => $request->password,
                    'device_id' => $request->device_id,
                    'device_type' => $request->device_type
                ];

                $response = $this->makeLogin($data);
                return response()->json($response);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    } // end function.



    public function lgoin(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'country_code' => 'required',
            'phone' => 'required',
            'device_type' => 'required|in:IPHONE,ANDROID',
            'device_id' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $response['status'] = false;
            $response['message'] = $this->validationHandle($validator->messages());
            $response['data'] = [];
            return response()->json($response);
        } else {
            $response = $this->makeLogin($data);
            return response()->json($response);
        }
    }

    protected function makeLogin($data = [])
    {
        $response = [];
        $roles_arr = Config::get('params.role_ids');
        $phone_country_code = $data['country_code'] . $data['phone'];
        $user = User::where(['phone_country_code' => $phone_country_code, 'role' => $roles_arr['user']])
            ->with(['countryData'])
            ->first();

        if (!$user) {
            $response['status'] = false;
            $response['message'] = "User does not exist.";
            $response['data'] = [];
        } else {
            if (Hash::check($data['password'], $user->password)) {
                $status_arr = \Config::get('params.status_arr');
                if ($user->status == $status_arr['inactive']) {
                    $response['status'] = false;
                    $response['message'] = "You have been marked as inactive by the administrator";
                    $response['data'] = [];
                } elseif ($user->status == $status_arr['active']) {
                    $token = JWTAuth::fromUser($user);
                    $user_id = $user->id;
                    manageDevices($user->id, $data['device_id'], $data['device_type'], 'add');
                    $user->security_token = $token;
                    $user->is_social_login = 0;

                    $user->is_have_subscription = (int) $user->is_have_subscription;



                    $response = ['status' => true, 'message' => 'Login successful.', 'data' => $user];
                }
            } else {
                $response['status'] = false;
                $response['message'] = "Password Incorrect.";
                $response['data'] = [];
            }
        }
        return $response;
    } // end function.

    public function getProfile(Request $request)
    {
        $response = [];
        try {
            $user_id = JWTAuth::user()->id;
            $user = User::where(['id' => $user_id])->with(['countryData'])->first();
            //echo $user->profile_thumb; die;
            //$user = JWTAuth::user();
            $user->is_have_subscription = (int) $user->is_have_subscription;
            $response['status'] = true;
            $response['message'] = "User profile data.";
            $response['data'] = $user;
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    } //end function.

    public function logout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'device_type' => 'required|in:IPHONE,ANDROID',
            'device_id' => 'required',
        ]);

        try {
            if ($validator->fails()) {
                $response['status'] = false;
                $response['message'] = $this->validationHandle($validator->messages());
                return response()->json($response);
            } else {
                $user_id = JWTAuth::user()->id;
                $token = JWTAuth::getToken();
                if ($token) {
                    JWTAuth::setToken($token)->invalidate();
                }
                manageDevices($user_id, $request->device_id, $request->device_type, 'delete');
                return response()->json(['status' => true, 'message' => 'Logged out successfully.', 'data' => []]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    } // end function.

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'new_password' => 'required|max:20|min:8',
        ]);
        if ($validator->fails()) {
            $error = $this->validationHandle($validator->messages());
            return response()->json(['status' => false, 'message' => $error]);
        } else {
            $user_id = JWTAuth::user()->id;
            $user = User::where('id', $user_id)->first();
            if (Hash::check($request->current_password, $user->password)) {
                $user->password = Hash::make($request->new_password);
                $user->save();
                return response()->json(['status' => true, 'message' => 'Password has been changed successfully.']);
            } else {
                return response()->json(['status' => false, 'message' => 'Current password is not correct.']);
            }
        }
    } //end function.

    public function checkPhone(Request $request)
    {
        try {
            $validatorRules = [
                'country_id' => 'required|numeric|digits_between:1,10|exists:countries,id',
                'phone' => 'required|numeric|digits_between:7,15'
            ];
            $validator = Validator::make($request->all(), $validatorRules);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            } else {
                $country_data = Country::where([['id', $request->country_id]])->first();
                $country_code = $country_data->phonecode;
                $country_code_mobile = $country_code . $request->phone;

                $user_phone_exists = User::where('phone_country_code', $country_code_mobile)
                    ->first();
                if (isset($user_phone_exists->id)) {
                    $response['status'] = false;
                    $response['message'] = 'The phone already been taken.';
                    $response['data'] = [];
                    return response()->json($response);
                }


                $phone_country_code = '+'.$request->country_code . $request->phone;
                $otp = $this->sendOtp($phone_country_code);
                $data = ['otp' => $otp];
                return response()->json([
                    'status' => true,
                    'message' => 'Otp sent',
                    'data' => $data
                ]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function editProfile(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $validator = Validator::make($request->all(), [
                // 'full_name' => 'nullable|max:255',
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'nullable|email|max:255|unique:users,email,' . $user_id,
                'profile_image' => 'nullable|mimes:jpeg,jpg,png',
                'country_id' => 'nullable|numeric|digits_between:1,10|exists:countries,id',
                'phone' => 'nullable|numeric|digits_between:7,15',
                'dob' => 'nullable|date_format:"Y-m-d"'
            ]);
            if ($validator->fails()) {
                $response['status'] = false;
                $response['message'] = $this->validationHandle($validator->messages());
                $response['data'] = [];
                return response()->json($response);
            } else {
                $country_code_mobile = '';
                $user = User::where('id', $user_id)
                    ->first();
                if (!empty($request->country_id)) {
                    $country_data = Country::where([['id', $request->country_id]])->first();
                    $country_code = isset($country_data->phonecode) ? $country_data->phonecode : '';
                    $user->country_id = $request->country_id;
                    if (empty($request->phone)) {
                        $country_code_mobile = $country_code . $user->phone;
                    } else {
                        $country_code_mobile = $country_code . $request->phone;
                        $user->phone = $request->phone;
                    }
                    $user->phone_country_code = $country_code_mobile;
                }

                // isset($request->full_name) ? $user->full_name = $request->full_name : null;
                isset($request->first_name) ? $user->first_name = $request->first_name : null;
                isset($request->last_name) ? $user->last_name = $request->last_name : null;
                isset($request->email) ? $user->email = $request->email : null;
                isset($request->dob) ? $user->dob = $request->dob : null;

                if ($request->file('profile_image') !== null) {
                    if (!empty($user->profile_image) && file_exists('public' . $user->profile_image)) {
                        unlink('public' . $user->profile_image);
                    }
                    $destinationPath = '/uploads/user/';
                    $response_data = Uploader::doUpload($request->file('profile_image'), $destinationPath);
                    if ($response_data['status'] == true) {
                        $user->profile_image = $response_data['file'];
                    }
                }

                $user->save();

                $user = User::where('id', $user_id)
                    ->with(['countryData'])
                    ->first();

                $user->is_have_subscription = (int) $user->is_have_subscription;

                return response()->json(['status' => true, 'message' => 'Profile has been updated successfully.', 'data' => $user]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function socialLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'social_type' => 'required|in:FACEBOOK',
            'social_id' => 'required',
            'email' => 'nullable|email|max:255',
            // 'full_name' => 'nullable|max:255',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'device_type' => 'required|in:IPHONE,ANDROID',
            'device_id' => 'required',
            'profile_image' => 'nullable'

        ]);
        if ($validator->fails()) {
            $error = $this->validationHandle($validator->messages());
            return response()->json(['status' => false, 'message' => $error]);
        } else {
            $status_arr = Config::get('params.status_arr');
            $roles_arr = Config::get('params.role_ids');
            $social_row = SocialAccount::where('social_type', $request->social_type)->where('social_id', $request->social_id)->first();
            if ($social_row) {
                $user = User::getUserbyId($social_row->user_id);
                if ($user->status != $status_arr['inactive']) {
                    if ($user->role == $roles_arr['user']) {
                        $user_save = 0;
                        if (empty($user->email) && !empty($request->email)) {
                            $user->email = $request->email;
                            $user_save = 1;
                        }
                        /*if (empty($user->full_name) && !empty($request->full_name)) {
                            $user->full_name = $request->full_name;
                            $user_save = 1;
                        }*/
                        if (empty($user->first_name) && !empty($request->first_name)) {
                            $user->first_name = $request->first_name;
                            $user_save = 1;
                        }
                        if (empty($user->last_name) && !empty($request->last_name)) {
                            $user->last_name = $request->last_name;
                            $user_save = 1;
                        }

                        if (empty($user->profile_image) && !empty($request->profile_image)) {
                            $destinationPath = '/uploads/user/';
                            $response_data = Uploader::saveImageFromUrl($request->profile_image, $destinationPath);
                            if ($response_data['status'] == true) {
                                $user->profile_image = $response_data['file'];
                                $user_save = 1;
                            }
                        }
                        if ($user_save) {
                            $user->save();
                        }
                        $token = JWTAuth::fromUser($user);
                        $user->security_token = $token;
                        $user->is_social_login = 1;
                        $user->is_have_subscription = (int) $user->is_have_subscription;
                        manageDevices($user->id, $request->device_id, $request->device_type, 'add');
                        return response()->json(['status' => true, 'message' => 'Social user detail.', 'data' => $user]);
                    } else {
                        return response()->json(['status' => false, 'message' => "Email is already taken.", 'data' => []]);
                    }
                } else {
                    return response()->json(['status' => false, 'message' => "Your account is inactive.", 'data' => []]);
                }
            } else {
                $save = 0;
                if (!empty($request->email)) {
                    $user = User::where([['email', $request->email]])->first();
                    if (!empty($user)) {
                        if ($user->role == $roles_arr['user']) {
                            if ($user->status == $status_arr['active']) {
                                User::manageSocialAccounts($user->id, $request->social_id, $request->social_type);

                                $token = JWTAuth::fromUser($user);
                                $user->security_token = $token;
                                $user->is_social_login = 1;
                                manageDevices($user->id, $request->device_id, $request->device_type, 'add');
                                return response()->json(['status' => true, 'message' => 'Social user detail.', 'data' => $user]);
                            } else {
                                return response()->json(['status' => false, 'message' => "Your account is inactive.", 'data' => []]);
                            }
                        } else {
                            return response()->json(['status' => false, 'message' => "Email is already taken.", 'data' => []]);
                        }
                    } else {
                        $save = 1;
                    }
                } else {
                    $save = 1;
                }

                if ($save) {
                    $user = new User();
                    // $user->full_name = isset($request->full_name) ? $request->full_name : '';
                    $user->first_name = isset($request->first_name) ? $request->first_name : '';
                    $user->last_name = isset($request->last_name) ? $request->last_name : '';
                    $user->email = isset($request->email) ? $request->email : '';
                    $user->password = null;
                    $user->role = Config::get('params.role_ids.user');
                    $user->status = Config::get('params.status_arr.active');
                    $user->token = null;
                    if (!empty($request->profile_image)) {
                        $destinationPath = '/uploads/user/';
                        $response_data = Uploader::saveImageFromUrl($request->profile_image, $destinationPath);
                        if ($response_data['status'] == true) {
                            $user->profile_image = $response_data['file'];
                            $user_save = 1;
                        }
                    }

                    $user->save();

                    User::manageSocialAccounts($user->id, $request->social_id, $request->social_type);
                    $token = JWTAuth::fromUser($user);
                    $user->security_token = $token;
                    $user->is_social_login = 1;
                    $user->is_have_subscription = (int) $user->is_have_subscription;
                    manageDevices($user->id, $request->device_id, $request->device_type, 'add');
                    return response()->json(['status' => true, 'message' => 'Social user detail.', 'data' => $user]);
                } else {
                    return response()->json(['status' => true, 'message' => 'User not found.', 'data' => []]);
                }
            }
        }
    }

    public function forgotPassword(Request $request)
    {
        try {
            $validatorRules = [
                'country_code' => 'required',
                'phone' => 'required',
                'device_type' => 'required|in:IPHONE,ANDROID',
                'device_id' => 'required',
            ];
            $validator = Validator::make($request->all(), $validatorRules);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            } else {
                $response['status'] = false;
                $response['data'] = [];
                $roles_arr = Config::get('params.role_ids');
                $status_arr = Config::get('params.status_arr');
                $phone_country_code = $request->country_code . $request->phone;
                $user = User::where(['phone_country_code' => $phone_country_code, 'role' => $roles_arr['user']])->first();


                if (!$user) {
                    $response['message'] = "User does not exist.";
                } else {
                    if ($user->status != $status_arr['active']) {
                        $response['message'] = "You have been marked as inactive by the administrator";
                    } else {
                        $otp = $this->sendOtp($phone_country_code);

                        $data = ['otp' => $otp];
                        $response['status'] = true;
                        $response['data'] = $data;
                    }
                }
                return response()->json($response);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function resetPassword(Request $request)
    {
        try {
            $validatorRules = [
                'country_code' => 'required',
                'phone' => 'required',
                'device_type' => 'required|in:IPHONE,ANDROID',
                'device_id' => 'required',
                'password' => 'required|max:20|min:8',
                'otp' => 'required'
            ];
            $validator = Validator::make($request->all(), $validatorRules);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            } else {
                $roles_arr = Config::get('params.role_ids');
                $phone_country_code = $request->country_code . $request->phone;
                $user = User::where(['phone_country_code' => $phone_country_code, 'role' => $roles_arr['user']])->first();
                $user->password = Hash::make($request->password);
                $user->save();

                return response()->json([
                    'status' => true,
                    'message' => 'Your password has been reset successfully.',
                    'data' => []
                ]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }


    public function resendOtp(Request $request)
    {
        try {
            $validatorRules = [
                'country_code' => 'required|numeric',
                'phone' => 'required|numeric|digits_between:7,15',
                'device_type' => 'required|in:IPHONE,ANDROID',
                'device_id' => 'required'
            ];
            $validator = Validator::make($request->all(), $validatorRules);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            } else {
                $phone_country_code = $request->country_code . $request->phone;
                $otp = $this->sendOtp($phone_country_code);
                $data = ['otp' => $otp];
                return response()->json([
                    'status' => true,
                    'message' => 'Otp sent',
                    'data' => $data
                ]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function notificationSetting(Request $request)
    {
        try {
            $validatorRules = [
                'is_notification_on' => 'required|in:0,1'
            ];
            $validator = Validator::make($request->all(), $validatorRules);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            } else {
                $user_id = JWTAuth::user()->id;
                $user_row = User::where(['id' => $user_id])->first();
                $user_row->is_notification_on = $request->is_notification_on;
                $user_row->save();
                $user = User::where(['id' => $user_id])->with(['countryData'])->first();
                return response()->json([
                    'status' => true,
                    'message' => 'Notification setting has been updated.',
                    'data' => $user
                ]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }


    public function getNotifications()
    {
        $user_id = JWTAuth::user()->id;
        $notifications = Notification::with([
            'orderData' => function ($query) {
                $query->select('id', 'order_number');
            },
            'orderItemData' => function ($query) {
                $query->select('id', 'driver_id');
            },
            'orderItemData.driverInfo' => function ($query) {
                $query->select('id', 'first_name', 'last_name', 'profile_image');
            }
        ])->where('user_id', $user_id)
            ->orderBy('id', 'DESC')
            ->paginate(10)
            ->toArray();

        $notifications['data'] = Notification::getNotificationFormatted($notifications['data']);
        return response()->json([
            'status' => true,
            'message' => 'Notifications.',
            'data' => $notifications
        ]);
    }

    public function getFaqCategories(Request $request)
    {
        try {
            $categories = FaqCategory::where(['status' => '1'])
                ->get();
            return response()->json(['status' => true, 'message' => 'Categories', 'data' => $categories]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function contanctUs(Request $request)
    {
        try {
            $validatorRules = [
                'faq_category_id' => 'required|exists:faq_categories,id',
                'message' => 'required'
            ];

            $customMessages = [
                'message.required' => 'The description field is required.',
            ];
            $validator = Validator::make($request->all(), $validatorRules, $customMessages);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            $entity = new ContactRequest();

            if (!empty($request->image)) {
                $destinationPath = '/uploads/contact_request/';
                $response_data = Uploader::saveImageFromUrl($request->image, $destinationPath);
                if ($response_data['status'] == true) {
                    $entity->image = $response_data['file'];
                }
            }

            $entity->user_id = $user_id;
            $entity->faq_category_id = $request->faq_category_id;
            $entity->message = $request->message;
            $entity->save();
            return response()->json(['status' => true, 'message' => 'Your query has been successfully sent to admin.', 'data' => $entity]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function addOrUpdateSubscription(Request $request)
    {
        $validatorRules = [
            'transaction_id' => 'required',
            'receipt' => 'required',
            'device_type' => 'required|in:ANDROID,IPHONE',
            'device_id' => 'required'
        ];
        $validator = Validator::make($request->all(), $validatorRules);
        if ($validator->fails()) {
            $error = $this->validationHandle($validator->messages());
            return response()->json(['status' => false, 'message' => $error]);
        } else {
            try {
                $user_id = JWTAuth::user()->id;
                $msg_type = 'updated';
                $row = UserSubscription::where('user_id', $user_id)
                    ->where('type', $request->device_type)
                    ->first();
                if (!$row) {
                    $row = new UserSubscription();
                    $msg_type = 'added';
                }
                $row->user_id = $user_id;
                $row->transaction_id = $request->transaction_id;
                $row->from_date = date('Y-m-d');
                $row->to_date = date("Y-m-d", strtotime("+1 month", strtotime(date('Y-m-d'))));
                $row->receipt = $request->receipt;
                $row->type = $request->device_type;
                $row->save();

                $user = User::where('id', $user_id)->first();
                $user->is_have_subscription = '1';
                $user->save();
                return response()->json(['status' => true, 'message' => 'Subscription ' . $msg_type . ' successfully.', 'data' => $user]);
            } catch (\Exception $e) {
                $msg = $e->getMessage();
                return response()->json(['status' => false, 'message' => $msg]);
            }
        }
    }

    public function addMoney(Request $request)
    {
        try {
            $validatorRules = [
                'token' => 'required',
                'amount' => 'required|numeric'
            ];
            $validator = Validator::make($request->all(), $validatorRules);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            $entity = new Wallet();
            $entity->user_id = $user_id;
            $entity->type = 'CR';
            $entity->amount = $request->amount;
            $entity->transaction_id = 1234;
            $entity->save();
            return response()->json(['status' => true, 'message' => 'Amount has been added successfully.', 'data' => []]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function withdrawMoney(Request $request)
    {
        try {
            $validatorRules = [
                'amount' => 'required|numeric'
            ];
            $validator = Validator::make($request->all(), $validatorRules);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;

            $wallet_amount = Wallet::select('user_id', DB::raw('SUM(amount) as total_amount'))
                ->where(['user_id' => $user_id])
                ->first();
            if ($request->amount > $wallet_amount->total_amount) {
                return response()->json(['status' => false, 'message' => 'Amount should be less than wallet balance.', 'data' => []]);
            }
            $entity = new Wallet();
            $entity->user_id = $user_id;
            $entity->type = 'DR';
            $entity->amount = -$request->amount;
            $entity->transaction_id = 1234;
            $entity->save();
            return response()->json(['status' => true, 'message' => 'Amount has been withdrawal successfully.', 'data' => []]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function transactionList(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $sum = Wallet::where(['user_id'=>$user_id])->sum('amount');

            $trns = Wallet::where(['user_id'=>$user_id])->paginate(10);
            return response()->json(['status' => true, 'message' => 'Transactions', 'data' => $trns, 'sum'=>$sum]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function getPlans(Request $request)
    {
        try {
            $plans = Plan::where(['status'=>'1'])->get();

            $premium_message = getSettings()['premium_user_message'];
            $data = ['plans'=> $plans, 'premium_user_message'=> $premium_message];

            return response()->json(['status' => true, 'message' => 'Plans', 'data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }
}//end class.
