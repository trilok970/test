<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Validator;
use \Config;
use JWTAuth;
use JWTAuthException;
use App\Lib\Uploader;
use App\Model\UserAddress;
use App\Model\Cart;

class AddressesController extends Controller
{
    public function addAddress(Request $request)
    {
        try {
            $status_arr = Config::get('params.status_arr');
            $validatorRules = [
                'address'=>'required',
                'lat'=>'required',
                'lng'=>'required',
                'zip_code'=>'nullable',
                'city'=>'nullable',
                'country'=>'nullable',
                'type'=>'nullable|in:HOME,OFFICE,OTHER',
                'sublocality'=>'nullable'
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            $added_address = UserAddress::where('user_id', $user_id)->first();
            $default_address = '0';
            if(empty($added_address->id)) {
                $default_address = '1';
            }


            $entity = new UserAddress();

            $entity->user_id = $user_id;
            $entity->is_default = $default_address;
            $entity->address = $request->address;
            $entity->lat = $request->lat;
            $entity->lng = $request->lng;
            !empty($request->zip_code) ? $entity->zip_code = $request->zip_code : null;
            !empty($request->city) ? $entity->city = $request->city: null;
            !empty($request->country) ? $entity->country = $request->country: null;
            !empty($request->type) ? $entity->type = $request->type : null;
            !empty($request->sublocality) ? $entity->sublocality = $request->sublocality : null;
            !empty($request->address_type_desc) ? $entity->address_type_desc = $request->address_type_desc : '';
            $entity->save();
            $addresses = UserAddress::getAddress($user_id);
            return response()->json(['status' => true, 'message' => 'Address has been saved successfully.', 'data' =>$addresses]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }


    public function getAddress(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $addresses = UserAddress::getAddress($user_id);
            return response()->json(['status' => true, 'message' => 'Addresses', 'data' =>$addresses]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }


    public function edit(Request $request)
    {
        try {
            $status_arr = Config::get('params.status_arr');
            $validatorRules = [
                'address_id'=>'required|exists:user_addresses,id',
                'address'=>'required',
                'lat'=>'required',
                'lng'=>'required',
                'zip_code'=>'nullable',
                'city'=>'nullable',
                'country'=>'nullable',
                'type'=>'nullable|in:HOME,OFFICE,OTHER',
                'sublocality'=>'nullable'
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;

            $entity = UserAddress::where(['id'=>$request->address_id])->first();
            $entity->address = $request->address;
            $entity->lat = $request->lat;
            $entity->lng = $request->lng;
            !empty($request->zip_code) ? $entity->zip_code = $request->zip_code : null;
            !empty($request->city) ? $entity->city = $request->city: null;
            !empty($request->country) ? $entity->country = $request->country: null;
            !empty($request->type) ? $entity->type = $request->type : null;
            !empty($request->sublocality) ? $entity->sublocality = $request->sublocality : null;
            !empty($request->address_type_desc) ? $entity->address_type_desc = $request->address_type_desc : '';

            $entity->save();
            $addresses = UserAddress::getAddress($user_id);
            return response()->json(['status' => true, 'message' => 'Address has been updated successfully.', 'data' =>$addresses]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $validatorRules = [
                'address_id'=>'required|exists:user_addresses,id'
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            UserAddress::where(['user_id'=> $user_id, 'id'=> $request->address_id])->delete();
            $addresses = UserAddress::getAddress($user_id);
            return response()->json(['status' => true, 'message' => 'Addresses deleted.', 'data' =>$addresses]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function makeDefaultAddress(Request $request){
        try {
            $validatorRules = [
                'address_id'=>'required|exists:user_addresses,id'
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            UserAddress::where(['user_id'=>$user_id, 'is_default'=>'1'])
                ->update(['is_default' => '0']);

            $address = UserAddress::where(['id'=> $request->address_id])
            ->first();
            $address->is_default = '1';
            $address->save();


            $addresses = UserAddress::getAddress($user_id);
            return response()->json(['status' => true, 'message' => 'Default address set.', 'data' =>$addresses]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function getDefaultAddressAndCartType(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $address = UserAddress::getDefaultAddress($user_id);

            /*
            $cart_type = Cart::leftJoin('menu_items', 'menu_items.id', '=', 'carts.menu_item_id')
            ->where('carts.user_id', $user_id)
            ->pluck('menu_items.type_id')
            ->first();
            */
            $data = [
                'default_address'=> $address,
                //'cart_type'=>$cart_type
            ];
            return response()->json(['status' => true, 'message' => 'Addresses', 'data' =>$data]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }


}//end class.
