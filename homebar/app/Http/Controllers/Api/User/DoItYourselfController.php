<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use Config;
use JWTAuth;
use JWTAuthException;
use App\Model\DoItYourself;
use App\Model\UserDiyLike;
use App\Model\User;
use App\Model\Offer;

class DoItYourselfController extends Controller
{

    public function getDiy(Request $request)
    {
        try {
            $status_arr = Config::get('params.status_arr');
            $validatorRules = [
                'category_id'=>'required|exists:categories,id',
                'search'=>'nullable',
                'asc_desc'=>'nullable|in:ASC,DESC',
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $conditions = [
                'status'=>$status_arr['active'],
                'category_id'=>$request->category_id
            ];

            $data = DoItYourself::where($conditions);
            $user = JWTAuth::user();

            if(!empty($request->search)){
                $serach_str = '%'.$request->search.'%';
                $data = $data->where(function($query) use($serach_str){
                    $query->where('title', 'like', $serach_str)
                        ->orWhere('description', 'like', $serach_str);
                });
            }

            if(!empty($request->filter_type)) {
                $filter_type = $request->filter_type;
                if($request->filter_type == 'RATING'){

                    // $data = $data->selectRaw("*,(select count(diy_id) from `user_diy_likes` where `do_it_yourself`.`id` = `user_diy_likes`.`diy_id` group by `diy_id`) as total_diy_fav")
                    // ->orderBy('total_diy_fav','desc');
                    $userFavIds = UserDiyLike::where(['user_id'=>$user->id])->pluck('diy_id');
                    $data = $data->whereIn('id',$userFavIds);
                }
            }
            if(!empty($request->asc_desc)) {
                $data = $data->orderBy('id', $request->asc_desc);
            }
            $data = $data->paginate(10);

            return response()->json(['status' => true, 'message' => 'DIY List', 'data' =>$data]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }


    public function getDiyDetails(Request $request)
    {
        try {
            $status_arr = Config::get('params.status_arr');
            $validatorRules = [
                'diy_id'=>'required|exists:do_it_yourself,id'
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            $conditions = [
                'status'=>$status_arr['active'],
                'id'=>$request->diy_id
            ];

            $data = DoItYourself::with(['ingredients', 'CategoryData'])
            ->where($conditions)
            ->first();

            $like_row = UserDiyLike::where([
                'user_id'=>$user_id,
                'diy_id'=>$data->id
            ])
            ->first();
            $data->is_liked = !empty($like_row->id) ? 1 : 0;

            return response()->json(['status' => true, 'message' => 'Details.', 'data' =>$data]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function likeDislikeDiy(Request $request)
    {
        try {
            $validatorRules = [
                'diy_id'=>'required|exists:do_it_yourself,id',
                'is_like'=>'required|in:0,1'
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            $like_row = UserDiyLike::where([
            'user_id'=>$user_id,
            'diy_id'=>$request->diy_id
            ])
            ->first();

            if ($request->is_like == 1) {
                $message = 'Favorite added.';
                if (empty($like_row->id)) {
                    $entity = new UserDiyLike();
                    $entity->user_id = $user_id;
                    $entity->diy_id = $request->diy_id;
                    $entity->save();
                }
            } else {
                $message = 'Favorite removed.';
                if (! empty($like_row->id)) {
                    $like_row->delete();
                }
            }
            return response()->json(['status'=>true, 'message'=>$message, 'data'=>[]]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function getStoreItem(Request $request)
    {
        try {
            $validatorRules = [
                'diy_id'=>'required|exists:do_it_yourself,id',
                'ingredients'=>'required|json',
                'lat'=>'required',
                'lng'=>'required',
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            $distance_num = '6371'; // for KM
            $ingredients = json_decode($request->ingredients, true);
            $ingredients_arr = [];
            $ingredients_attribute_selected = [];
            $lat = $request->lat;
            $lng = $request->lng;
            foreach($ingredients as $key => $i_data){
                $ingredients_arr[$i_data['ingredient_id']] = $i_data['quantity'];
                if(isset($i_data['attribute_selected'])) {
                    $ingredients_attribute_selected[$i_data['attribute_selected']] = $i_data['attribute_selected'];
                }

            }

            $ingredient_id_arr = array_keys($ingredients_arr);
            $ingredients_attribute_selected_arr = array_keys($ingredients_attribute_selected);
            $settings = Config::get('settings');
            $distance_radius = $settings['restaurant_distance_radius'];

            $distance_query = "ROUND(( $distance_num * acos( cos( radians($lat) ) * cos( radians( users.latitude ))
            * cos( radians( users.longitude ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(users.latitude))  )), 1) ";

            $vendors = User::select(['users.id', 'users.full_name', 'users.profile_image', 'users.cover_image', 'users.business_name', DB::raw("$distance_query as distance")])->with([
                'getVendorIngredients'=> function($query) use($ingredient_id_arr,$ingredients_attribute_selected_arr) {
                    $query->whereIn('ingredient_id', $ingredient_id_arr)
                    ->whereIn('qty', $ingredients_attribute_selected_arr)
                    ->where('in_stock', 'Yes');
                },
                'getVendorIngredients.ingredientDetails', 'getVendorIngredients.ingredientDetails.diy'
            ])->where(['role'=>3])
            ->where(DB::raw($distance_query), '<=', $distance_radius)
            ->whereHas('getVendorIngredients', function ($query) use($ingredient_id_arr,$ingredients_attribute_selected_arr) {
                $query->whereIn('ingredient_id', $ingredient_id_arr)
                ->whereIn('qty', $ingredients_attribute_selected_arr)
                ->where('in_stock', 'Yes');
            })
            ->orderBy('distance', 'ASC')
            ->get()
            ->toArray();
          
            //pr($vendors); die;
            $restaurants = [];
            if(!empty($vendors)) {
                $offers = Offer::getOffers();

                foreach($vendors as $key => $row){

                    $row_ingredients = $row['get_vendor_ingredients'];

                    $ings = [];
                    foreach($row_ingredients as $i_key => $i_row) {
                        $type_id = isset($i_row['ingredient_details']['diy']['type_id']) ? $i_row['ingredient_details']['diy']['type_id'] : null;
                        $row['get_vendor_ingredients'][$i_key]['discounted_price'] = Offer::getDiscountedPriceAll($offers, $row['id'], $type_id, $i_row['cost']);
                        $i_quantity = $ingredients_arr[$i_row['ingredient_id']];
                        $total_price = $i_row['cost'] * $i_quantity;

                        //($row['get_vendor_ingredients'][$i_key]); die;
                        $row['get_vendor_ingredients'][$i_key]['total_price'] = $total_price;

                    }
                    $restaurants[] = $row;
                }
            }
            // echo "<pre>";
            // print_r($restaurants);
            // exit;
            return response()->json(['status'=>true, 'message'=>'Restaurants', 'data'=>$restaurants]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }



}
