<?php

namespace App\Http\Controllers\Api\Bartender;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Validator;
use \Config;
use JWTAuth;
use JWTAuthException;
use App\Lib\Uploader;
use App\Model\Pages;

class PagesController extends Controller
{

    public function getPages(Request $request)
    {
        try {
            $status_arr = Config::get('params.status_arr');
            $validatorRules = [
                'slug'=>'required',
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $page = Pages::where(['slug'=>$request->slug])
            ->first();
            if(!empty($page->id)) {
                return response()->json(['status' => true, 'message' => 'Page.', 'data' =>$page]);
            } else {
                return response()->json(['status' => false, 'message' => 'Page not found.', 'data' =>[]]);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }



}//end class.
