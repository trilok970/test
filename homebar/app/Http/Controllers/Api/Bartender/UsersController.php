<?php

namespace App\Http\Controllers\Api\Bartender;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use \Config;
use JWTAuth;
use JWTAuthException;
use App\Lib\Uploader;
use App\Lib\StripePayment;
use App\Model\User;
use App\Model\Country;
use App\Model\SocialAccount;
use App\Model\BartendingInformation;
use App\Model\DrinkVideo;
use App\Model\Review;
use Illuminate\Support\Facades\Storage;
use App\Model\Order;
use App\Model\OrderItem;
use App\Model\Notification;
use App\Model\BartenderGroupMembers;
use Illuminate\Database\Eloquent\Builder;
use Exception;
use App\Model\FaqCategory;
use App\Model\ContactRequest;
use App\Model\Wallet;

class UsersController extends Controller
{
    public function getCountries()
    {
        $all_country = Country::orderBy('name', 'asc')->get();
        return response()->json(['status' => true, 'message' => 'Listing.', 'data' =>$all_country]);
    }

    public function checkSignup(Request $request)
    {
        $validatorRules = [
            'email' => 'nullable|email|max:255',
            'country_id' => 'required|numeric|digits_between:1,10|exists:countries,id',
            'phone' => 'required|numeric|digits_between:7,15',
            // 'full_name'=>'required|max:255',
            'first_name'=>'required|max:255',
            'last_name'=>'required|max:255',
            'dob'=>'nullable|date_format:"Y-m-d"',
            'device_type' => 'required|in:IPHONE,ANDROID',
            'device_id' => 'required',
            'password' => 'required|max:20|min:8',

        ];
        try {
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            } else {
                $email = isset($request->email) ? $request->email : null;
                $user_unique_check = User::checkUserUnique($email, $request->phone);
                if($user_unique_check['status'] == false){
                    return response()->json($user_unique_check);
                }
                $country_data = Country::where([['id', $request->country_id]])->first();
                $country_code = isset($country_data->phonecode) ? $country_data->phonecode : '';
                $country_code_mobile = $country_code.$request->phone;

                $otp = $this->sendOtp($country_code_mobile);
                $data = ['otp' => $otp];
                return response()->json([
                    'status' => true,
                    'message' => 'User can register',
                    'data' => $data
                ]);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }// end function.

    public function signup(Request $request)
    {
        $validatorRules = [
            'email' => 'nullable|email|max:255',
            'country_id' => 'required|numeric|digits_between:1,10|exists:countries,id',
            'phone' => 'required|numeric|digits_between:7,15',
            // 'full_name'=>'required|max:255',
            'first_name'=>'required|max:255',
            'last_name'=>'required|max:255',
            'dob'=>'nullable|date_format:"Y-m-d"',
            'device_type' => 'required|in:IPHONE,ANDROID',
            'device_id' => 'required',
            'otp'=>'required',
            'password' => 'required|max:20|min:8',
        ];
        try {
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            } else {
                $email = isset($request->email) ? $request->email : null;
                $user_unique_check = User::checkUserUnique($email, $request->phone);
                if($user_unique_check['status'] == false){
                    return response()->json($user_unique_check);
                }
                $status_arr = \Config::get('params.status_arr');
                $country_data = Country::where([['id', $request->country_id]])->first();
                $country_code = isset($country_data->phonecode) ? $country_data->phonecode : '';
                $country_code_mobile = $country_code.$request->phone;

                $user = new User();
                // $user->full_name = $request->full_name;
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->dob = isset($request->dob) ? $request->dob : null;
                $user->email = $request->email;
                $user->role = Config::get('params.role_ids.bartender');
                $user->status = $status_arr['active'];
                $user->phone = $request->phone;
                $user->country_id = $request->country_id;
                $user->phone_country_code = $country_code_mobile;
                $user->password = Hash::make($request->password);
                $user->options = '1';
                $user->save();

                $data = [
                    'phone'=>$user->phone,
                    'country_code'=>$country_code,
                    'password'=>$request->password,
                    'device_id'=>$request->device_id,
                    'device_type'=>$request->device_type
                ];

                $response = $this->makeLogin($data);
                return response()->json($response);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }// end function.



    public function lgoin(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'country_code' => 'required',
            'phone' => 'required',
            'device_type' => 'required|in:IPHONE,ANDROID',
            'device_id' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $response['status'] = false;
            $response['message'] = $this->validationHandle($validator->messages());
            $response['data'] = [];
            return response()->json($response);
        } else {
            $response = $this->makeLogin($data);
            return response()->json($response);
        }
    }

    protected function makeLogin($data=[])
    {
        $response = [];
        $roles_arr = Config::get('params.role_ids');
        $phone_country_code = $data['country_code'].$data['phone'];
        $user = User::where(['phone_country_code' => $phone_country_code, 'role'=>$roles_arr['bartender']])
        ->with(['countryData','bartending'])
        ->first();

        if (!$user) {
            $response['status'] = false;
            $response['message'] = "User does not exist.";
            $response['data'] = [];
        } else {
            if (Hash::check($data['password'], $user->password)) {
                $status_arr = \Config::get('params.status_arr');
                if ($user->status == $status_arr['inactive']) {
                    $response['status'] = false;
                    $response['message'] = "You have been marked as inactive by the administrator";
                    $response['data'] = [];
                } elseif ($user->status == $status_arr['active']) {
                    $token = JWTAuth::fromUser($user);
                    $user_id = $user->id;
                    manageDevices($user->id, $data['device_id'], $data['device_type'], 'add');
                    $user->security_token = $token;
                    $user->is_social_login = 0;

                    $response = ['status' => true, 'message' => 'Login successful.', 'data' => $user];
                }
            } else {
                $response['status'] = false;
                $response['message'] = "Password Incorrect.";
                $response['data'] = [];
            }
        }
        return $response;
    }// end function.

    public function getProfile(Request $request)
    {
        $response = [];
        try {
            $user_id = JWTAuth::user()->id;
            $user = User::where(['id' => $user_id])->with(['countryData'])->first();
            $response['status'] = true;
            $response['message'] = "User profile data.";
            $response['data'] = $user;
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }//end function.

    public function logout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'device_type' => 'required|in:IPHONE,ANDROID',
            'device_id' => 'required',
        ]);

        try {
            if ($validator->fails()) {
                $response['status'] = false;
                $response['message'] = $this->validationHandle($validator->messages());
                return response()->json($response);
            } else {
                $user_id = JWTAuth::user()->id;
                $token = JWTAuth::getToken();
                if ($token) {
                    JWTAuth::setToken($token)->invalidate();
                }
                manageDevices($user_id, $request->device_id, $request->device_type, 'delete');
                return response()->json(['status'=>true,'message'=>'Logged out successfully.','data'=>[]]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }// end function.

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'new_password' => 'required|max:20|min:8',
        ]);
        if ($validator->fails()) {
            $error = $this->validationHandle($validator->messages());
            return response()->json(['status'=>false,'message'=>$error]);
        } else {
            $user_id = JWTAuth::user()->id;
            $user = User::where('id', $user_id)->first();
            if (Hash::check($request->current_password, $user->password)) {
                $user->password = Hash::make($request->new_password);
                $user->save();
                return response()->json(['status'=>true,'message'=>'Password has been changed successfully.']);
            } else {
                return response()->json(['status'=>false,'message'=>'Current password is not correct.']);
            }
        }
    }//end function.

    public function checkPhone(Request $request)
    {
        try {
            $validatorRules = [
                'country_id' => 'required|numeric|digits_between:1,10|exists:countries,id',
                'phone' => 'required|numeric|digits_between:7,15'
            ];
            $validator = Validator::make($request->all(), $validatorRules);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status'=>false,'message'=>$error]);
            } else {
                $country_data = Country::where([['id', $request->country_id]])->first();
                $country_code = $country_data->phonecode;
                $country_code_mobile = $country_code.$request->phone;

                $user_phone_exists = User::where('phone_country_code', $country_code_mobile)
                ->first();
                if (isset($user_phone_exists->id)) {
                    $response['status'] = false;
                    $response['message'] = 'The phone already been taken.';
                    $response['data'] = [];
                    return response()->json($response);
                }


                $phone_country_code = $request->country_code.$request->phone;
                $otp = $this->sendOtp($phone_country_code);
                $data = ['otp' => $otp];
                return response()->json([
                    'status' => true,
                    'message' => 'Otp sent',
                    'data' => $data
                ]);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function editProfile(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $validator = Validator::make($request->all(), [
                // 'full_name' => 'nullable|max:255',
                'first_name'=>'required|max:255',
                'last_name'=>'required|max:255',
                'email' => 'nullable|email|max:255|unique:users,email,'.$user_id,
                'profile_image' => 'nullable|mimes:jpeg,jpg,png',
                'country_id' => 'nullable|numeric|digits_between:1,10|exists:countries,id',
                'phone' => 'nullable|numeric|digits_between:7,15',
                'dob'=>'nullable|date_format:"Y-m-d"',
                'experience'=>'required|numeric',
                'address' =>  'required|max:255',
                'signature_drink'=>  'required|max:255',
                'service_address_radius' =>  'required|numeric',
                'bio' =>  'required',
                'price_per_hour'  =>  'nullable|numeric',
            ]);
            if ($validator->fails()) {
                $response['status'] = false;
                $response['message'] = $this->validationHandle($validator->messages());
                $response['data'] = [];
                return response()->json($response);
            } else {
                $country_code_mobile = '';
                $user = User::where('id', $user_id)
                ->first();
                if (!empty($request->country_id)) {
                    $country_data = Country::where([['id', $request->country_id]])->first();
                    $country_code = isset($country_data->phonecode) ? $country_data->phonecode : '';
                    $user->country_id = $request->country_id;
                    if (empty($request->phone)) {
                        $country_code_mobile = $country_code.$user->phone;
                    } else {
                        $country_code_mobile = $country_code.$request->phone;
                        $user->phone = $request->phone;
                    }
                    $user->phone_country_code = $country_code_mobile;
                }

                // isset($request->full_name) ? $user->full_name = $request->full_name : null;
                isset($request->first_name) ? $user->first_name = $request->first_name : null;
                isset($request->last_name) ? $user->last_name = $request->last_name : null;
                isset($request->email) ? $user->email = $request->email : null;
                isset($request->dob) ? $user->dob = $request->dob : null;
                isset($request->experience) ? $user->experience = $request->experience : null;
                isset($request->address) ? $user->address = $request->address : null;
                isset($request->signature_drink) ? $user->signature_drink = $request->signature_drink : null;
                isset($request->service_address_radius) ? $user->service_address_radius = $request->service_address_radius : null;
                isset($request->bio) ? $user->bio = $request->bio : null;
                isset($request->price_per_hour) ? $user->price_per_hour = $request->price_per_hour : null;

                if ($request->file('profile_image') !== null) {
                    if (!empty($user->profile_image) && file_exists('public'.$user->profile_image)) {
                        unlink('public'.$user->profile_image);
                    }
                    $destinationPath = '/uploads/user/';
                    $response_data = Uploader::doUpload($request->file('profile_image'), $destinationPath);
                    if ($response_data['status'] == true) {
                        $user->profile_image = $response_data['file'];
                        $user->photo = $response_data['file'];
                    }
                }

                $user->save();

                $user = User::where('id', $user_id)
                ->with(['countryData'])
                ->first();

                return response()->json(['status' => true, 'message' => 'Profile has been updated successfully.', 'data'=>$user]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function socialLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'social_type' => 'required|in:FACEBOOK',
            'social_id' => 'required',
            'email' => 'nullable|email|max:255',
            // 'full_name' => 'nullable|max:255',
            'first_name'=>'required|max:255',
            'last_name'=>'required|max:255',
            'device_type' => 'required|in:IPHONE,ANDROID',
            'device_id' => 'required',
            'profile_image'=>'nullable'
        ]);
        if ($validator->fails()) {
            $error = $this->validationHandle($validator->messages());
            return response()->json(['status' => false, 'message' => $error]);
        } else {
            $status_arr = Config::get('params.status_arr');
            $roles_arr = Config::get('params.role_ids');
            $social_row = SocialAccount::where('social_type', $request->social_type)->where('social_id', $request->social_id)->first();
            if ($social_row) {
                $user = User::getUserbyId($social_row->user_id);
                if ($user->status != $status_arr['inactive']) {
                    if ($user->role == $roles_arr['bartender']) {
                        $user_save = 0;
                        if (empty($user->email) && !empty($request->email)) {
                            $user->email = $request->email;
                            $user_save = 1;
                        }
                        /*if (empty($user->full_name) && !empty($request->full_name)) {
                            $user->full_name = $request->full_name;
                            $user_save = 1;
                        }*/
                        if (empty($user->first_name) && !empty($request->first_name)) {
                            $user->first_name = $request->first_name;
                            $user_save = 1;
                        }

                        if (empty($user->last_name) && !empty($request->last_name)) {
                            $user->last_name = $request->last_name;
                            $user_save = 1;
                        }

                        if (empty($user->profile_image) && !empty($request->profile_image)) {
                            $destinationPath = '/uploads/user/';
                            $response_data = Uploader::saveImageFromUrl($request->profile_image, $destinationPath);
                            if ($response_data['status'] == true) {
                                $user->profile_image = $response_data['file'];
                                $user_save = 1;
                            }
                        }
                        if ($user_save) {
                            $user->save();
                        }
                        $token = JWTAuth::fromUser($user);
                        $user->security_token = $token;
                        $user->is_social_login = 1;
                        manageDevices($user->id, $request->device_id, $request->device_type, 'add');
                        return response()->json(['status' => true, 'message' => 'Social user detail.', 'data' => $user]);
                    } else {
                        return response()->json(['status' => false, 'message' => "Email is already taken.", 'data' => []]);
                    }
                } else {
                    return response()->json(['status' => false, 'message' => "Your account is inactive.", 'data' => []]);
                }
            } else {
                $save = 0;
                if (!empty($request->email)) {
                    $user = User::where([['email', $request->email]])->first();
                    if (!empty($user)) {
                        if ($user->role == $roles_arr['bartender']) {
                            if ($user->status == $status_arr['active']) {
                                User::manageSocialAccounts($user->id, $request->social_id, $request->social_type);

                                $token = JWTAuth::fromUser($user);
                                $user->security_token = $token;
                                $user->is_social_login = 1;
                                manageDevices($user->id, $request->device_id, $request->device_type, 'add');
                                return response()->json(['status' => true, 'message' => 'Social user detail.', 'data' => $user]);
                            } else {
                                return response()->json(['status' => false, 'message' => "Your account is inactive.", 'data' => []]);
                            }
                        } else {
                            return response()->json(['status' => false, 'message' => "Email is already taken.", 'data' => []]);
                        }
                    } else {
                        $save = 1;
                    }
                } else {
                    $save = 1;
                }

                if ($save) {
                    $user = new User();
                    // $user->full_name = isset($request->full_name) ? $request->full_name : '';
                    $user->first_name = isset($request->first_name) ? $request->first_name : '';
                    $user->last_name = isset($request->last_name) ? $request->last_name : '';
                    $user->email = isset($request->email) ? $request->email : '';
                    $user->password = null;
                    $user->role = Config::get('params.role_ids.bartender');
                    $user->status = Config::get('params.status_arr.active');
                    $user->token = null;
                    if (!empty($request->profile_image)) {
                        $destinationPath = '/uploads/user/';
                        $response_data = Uploader::saveImageFromUrl($request->profile_image, $destinationPath);
                        if ($response_data['status'] == true) {
                            $user->profile_image = $response_data['file'];
                            $user_save = 1;
                        }
                    }

                    $user->save();

                    User::manageSocialAccounts($user->id, $request->social_id, $request->social_type);
                    $token = JWTAuth::fromUser($user);
                    $user->security_token = $token;
                    $user->is_social_login = 1;
                    manageDevices($user->id, $request->device_id, $request->device_type, 'add');
                    return response()->json(['status' => true, 'message' => 'Social user detail.', 'data' => $user]);
                } else {
                    return response()->json(['status' => true, 'message' => 'User not found.', 'data' => []]);
                }
            }
        }
    }

    public function forgotPassword(Request $request)
    {
        try {
            $validatorRules = [
                'country_code' => 'required',
                'phone' => 'required',
                'device_type' => 'required|in:IPHONE,ANDROID',
                'device_id' => 'required',
            ];
            $validator = Validator::make($request->all(), $validatorRules);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status'=>false,'message'=>$error]);
            } else {
                $response['status'] = false;
                $response['data'] = [];
                $roles_arr = Config::get('params.role_ids');
                $status_arr = Config::get('params.status_arr');
                $phone_country_code = $request->country_code.$request->phone;
                $user = User::where(['phone_country_code' => $phone_country_code, 'role'=>$roles_arr['bartender']])->first();


                if (!$user) {
                    $response['message'] = "User does not exist.";
                } else {
                    if ($user->status != $status_arr['active']) {
                        $response['message'] = "You have been marked as inactive by the administrator";
                    } else {
                        $otp = $this->sendOtp($phone_country_code);

                        $data = ['otp' => $otp];
                        $response['status'] = true;
                        $response['data'] = $data;
                    }
                }
                return response()->json($response);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function resetPassword(Request $request)
    {
        try {
            $validatorRules = [
                'country_code' => 'required',
                'phone' => 'required',
                'device_type' => 'required|in:IPHONE,ANDROID',
                'device_id' => 'required',
                'password' => 'required|max:20|min:8',
                'otp'=> 'required'
            ];
            $validator = Validator::make($request->all(), $validatorRules);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status'=>false,'message'=>$error]);
            } else {
                $roles_arr = Config::get('params.role_ids');
                $phone_country_code = $request->country_code.$request->phone;
                $user = User::where(['phone_country_code' => $phone_country_code, 'role'=>$roles_arr['bartender']])->first();
                $user->password = Hash::make($request->password);
                $user->save();

                return response()->json([
                    'status'=>true,
                    'message'=>'Your password has been reset successfully.',
                    'data'=>[]
                ]);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }


    public function resendOtp(Request $request)
    {
        try {
            $validatorRules = [
                'country_code' => 'required|numeric',
                'phone' => 'required|numeric|digits_between:7,15',
                'device_type' => 'required|in:IPHONE,ANDROID',
                'device_id' => 'required'
            ];
            $validator = Validator::make($request->all(), $validatorRules);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status'=>false,'message'=>$error]);
            } else {
                $phone_country_code = $request->country_code.$request->phone;
                $otp = $this->sendOtp($phone_country_code);
                $data = ['otp' => $otp];
                return response()->json([
                    'status' => true,
                    'message' => 'Otp sent',
                    'data' => $data
                ]);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function updateVehicle(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $driver_vehicle = DriverVehicle::where('user_id', $user_id)->first();
            $rules = [
                'license_number' => 'required|max:50',
                'license_expiry_date'=>'required|date_format:Y-m-d',
                'license_image' =>  'nullable|image',
                'vehicle_number'=>  'required|max:50',
                'vehicle_make'  =>  'required|max:100',
                'vehicle_model' =>  'required|max:100',
                'vehicle_color' =>  'required|max:100',
                'vehicle_image' =>  'nullable|image',
                'insurance_image' =>  'nullable|image',
                'insurance_expiry_date'=>'required|date_format:Y-m-d',
                'social_security_number'=>'required|max:100'
            ];
            if (!$driver_vehicle) {
                $rules['license_image'] = 'required|image';
                $rules['vehicle_image'] = 'required|image';
                $rules['insurance_image'] = 'required|image';
            }
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $response['status'] = false;
                $response['message'] = $this->validationHandle($validator->messages());
                $response['data'] = [];
                return response()->json($response);
            } else {
                $formData = $request->except('license_image', 'vehicle_image', 'insurance_image');
                if ($request->file('license_image') !== null) {
                    $destinationPath = '/uploads/driver_vehicles/';
                    $response_data = Uploader::doUpload($request->file('license_image'), $destinationPath, 'license_image');
                    if ($response_data['status'] == true) {
                        $formData['license_image'] = $response_data['file'];
                    }
                }
                if ($request->file('vehicle_image') !== null) {
                    $destinationPath = '/uploads/driver_vehicles/';
                    $response_data = Uploader::doUpload($request->file('vehicle_image'), $destinationPath, 'vehicle_image');
                    if ($response_data['status'] == true) {
                        $formData['vehicle_image'] = $response_data['file'];
                    }
                }
                if ($request->file('insurance_image') !== null) {
                    $destinationPath = '/uploads/driver_vehicles/';
                    $response_data = Uploader::doUpload($request->file('insurance_image'), $destinationPath, 'insurance_image');
                    if ($response_data['status'] == true) {
                        $formData['insurance_image'] = $response_data['file'];
                    }
                }
                $formData['user_id'] = $user_id;
                if ($driver_vehicle) {
                    $driver_vehicle->update($formData);
                } else {
                    DriverVehicle::create($formData);
                }
                $data = DriverVehicle::where('user_id', $user_id)->first();
                return response()->json(['status' => true, 'message' => 'Vehicle information updated successfully.', 'data'=>$data]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function getVehicleInfo(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $data = DriverVehicle::where('user_id', $user_id)->first();
            return response()->json(['status' => true, 'message' => 'Vehicle information.', 'data'=>$data]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function createProfile(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $rules = [
                'profile_image' => 'nullable|image',
                'experience'=>'required|numeric',
                'address' =>  'required|max:255',
                'signature_drink'=>  'required|max:255',
                'price_per_hour'  =>  'required|numeric',
                'service_address_radius' =>  'required|numeric',
                'bio' =>  'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response['status'] = false;
                $response['message'] = $this->validationHandle($validator->messages());
                $response['data'] = [];
                return response()->json($response);
            } else {
                $user = User::find($user_id);
                $formData = $request->except('profile_image');
                if ($request->file('profile_image') !== null) {
                    $destinationPath = '/uploads/user/';
                    $response_data = Uploader::doUpload($request->file('profile_image'), $destinationPath);
                    if ($response_data['status'] == true) {
                        $formData['profile_image'] = $response_data['file'];
                    }
                }
                $user->update($formData);
                $data = User::find($user_id);
                return response()->json(['status' => true, 'message' => 'Information updated successfully.', 'data'=>$data]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function updateBartendingInformation(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $bartending_information = BartendingInformation::where('user_id', $user_id)->first();
            $rules = [
                'social_security_number' => 'required|max:100',
                'expiry_date'=>'required|date_format:Y-m-d',
                'bartending_license_image' =>  'nullable|image',
                'dl_image' =>  'nullable|image',
                'bartending_license_number'=>  'required|max:100',
                'dl_number'  =>  'required|max:50',
            ];
            if (!$bartending_information) {
                $rules['bartending_license_image'] = 'required|image';
                $rules['dl_image'] = 'required|image';
            }
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $response['status'] = false;
                $response['message'] = $this->validationHandle($validator->messages());
                $response['data'] = [];
                return response()->json($response);
            } else {
                $formData = $request->except('bartending_license_image', 'dl_image');
                if ($request->file('bartending_license_image') !== null) {
                    $destinationPath = '/uploads/bartending_images/';
                    $response_data = Uploader::doUpload($request->file('bartending_license_image'), $destinationPath, 'bartending_license_image');
                    if ($response_data['status'] == true) {
                        $formData['bartending_license_image'] = $response_data['file'];
                    }
                }
                if ($request->file('dl_image') !== null) {
                    $destinationPath = '/uploads/bartending_images/';
                    $response_data = Uploader::doUpload($request->file('dl_image'), $destinationPath, 'dl_image');
                    if ($response_data['status'] == true) {
                        $formData['dl_image'] = $response_data['file'];
                    }
                }
                $formData['user_id'] = $user_id;
                if ($bartending_information) {
                    $bartending_information->update($formData);
                } else {
                    BartendingInformation::create($formData);
                }

                $user = User::find($user_id);
                $user->bartending_license_number = $request->bartending_license_number;
                $user->bartending_license_expirations = $request->expiry_date;
                if(isset($formData['dl_image'])) {
                    $user->dl_photo = $formData['dl_image'];
                }
                if(isset($formData['bartending_license_image'])) {
                    $user->bl_photo = $formData['bartending_license_image'];
                }
                $user->dl_number  = $request->dl_number;
                $user->save();

                // $data = BartendingInformation::where('user_id',$user_id)->first();
                $data = User::where('id', $user_id)->with(['countryData','bartending'])->first();
                $token = JWTAuth::fromUser($data);
                $data->security_token = $token;
                return response()->json(['status' => true, 'message' => 'Information updated successfully.', 'data'=>$data]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function uploadDrinkVideo(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $rules = [
                'video' => 'required|mimes:mp4,mov,ogg',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response['status'] = false;
                $response['message'] = $this->validationHandle($validator->messages());
                $response['data'] = [];
                return response()->json($response);
            } else {
                $formData = [];
                if ($request->file('video') !== null) {
                    $destinationPath = '/uploads/drinking_videos/';
                    $response_data = Uploader::doUpload($request->file('video'), $destinationPath, 'video');
                    if ($response_data['status'] == true) {
                        $formData['video'] = $response_data['file'];
                    }
                }
                $formData['user_id'] = $user_id;
                $formData['status'] = 1;
                DrinkVideo::create($formData);
                $data = DrinkVideo::where('user_id', $user_id)->get();
                return response()->json(['status' => true, 'message' => 'Video uploaded successfully.', 'data'=>$data]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function getDrinkVideos(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $data = DrinkVideo::where('user_id', $user_id)->get();
            return response()->json(['status' => true, 'message' => 'Drink videos.', 'data'=>$data]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }




    public function updateSchedule(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;

            $user = User::find($user_id);
            $bartenderSchedule = $user->bartenderSchedule()->get();
            if (!empty($bartenderSchedule->toArray())) {
                $user->bartenderSchedule()->delete();
            }

            $arr = json_decode($request->dates, true);
            $bartenderSchedule = $user->bartenderSchedule()->createMany($arr);

            $response['status'] = true;
            $response['message'] = 'Schedule Created Successfully';
            $response['data'] = $bartenderSchedule;

            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }
    public function addLeaves(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $user = User::find($user_id);

            $dates = json_decode($request->dates);
            $data = [];
            $user->currentMonthLeaves()->where('date','>',date('Y-m-d'))->delete();
            foreach ($dates as $date) {
                if ($date > date('Y-m-d')) {
                    $data[] = ['date' => $date];
                }
            }
            $user->currentMonthLeaves()->createMany($data);

            $response['status'] = true;
            $response['message'] = 'Selected Dates Marked as unavailable';
            $response['data'] = $user->currentMonthLeaves()->get();
            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function deleteLeaves(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $user = User::find($user_id);

            $user->currentMonthLeaves()->whereIn('date', $request->dates)->delete();
            $response['status'] = true;
            $response['message'] = 'Leaves removed Successfully';
            $response['data'] = $user->currentMonthLeaves()->get();
            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function getLeaves(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $user = User::find($user_id);

            $response['status'] = true;
            $response['message'] = 'Current Month Leaves';
            $response['data'] = $user->currentMonthLeaves()->get();
            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function getSchedule(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;

            $user = User::find($user_id);
            $bartenderSchedule = $user->bartenderSchedule()->get();
            $response['status'] = true;
            $response['message'] = 'Bartender Schedule';
            $response['data'] = $bartenderSchedule;
            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function deleteVideo(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $rules = [
                'video_id' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response['status'] = false;
                $response['message'] = $this->validationHandle($validator->messages());
                $response['data'] = [];
            }

            $video = DrinkVideo::where(['user_id' => $user_id,'id' => $request->video_id])->first();
            if ($video) {
                if (file_exists(public_path().$video->video)) {
                    unlink(public_path().$video->video);
                }
                $video->delete();
                $response['status'] = true;
                $response['message'] = 'Video Delete Successfully';
                $response['data'] = [];
            }
            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function addGroupMember(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $rules = [
                'name' => 'required',
                'email' => 'required|email',
                'mobile' => 'required',
                'experience' => 'required',
                'signature_drink' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response['status'] = false;
                $response['message'] = $this->validationHandle($validator->messages());
                $response['data'] = [];
                return response()->json($response);
            }

            if ($request->file('photo')) {
                $destinationPath = '/uploads/bartender-group-members/';
                $response_data = Uploader::doUpload($request->file('photo'), $destinationPath);
            }
            $user = User::find($user_id);
            $fields = [
                'name' => $request->name,
                'mobile_number' => $request->mobile,
                'email' => $request->email,
                'experience' => $request->experience,
                'signature_drink' => $request->signature_drink,

            ];
            if ($request->file('photo')) {
                $fields['photo'] = $response_data['file'];
            }
            $user->bartenderGroupMembers()->create($fields);

            $response['status'] = true;
            $response['message'] = 'Group Member Added Successfully';
            $response['data'] = $user->bartenderGroupMembers()->get();

            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function listGroupMembers(Request $request)
    {
        try {
            $validatorRules = [
                'search'=>'nullable'
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            $user = User::find($user_id);

            $users = BartenderGroupMembers::where('bartender_id', $user_id);

            if (!empty($request->search)) {
                $search_key = '%'.$request->search.'%';
                $users = $users->where(function ($query) use ($search_key) {
                    $query->where('name', 'LIKE', $search_key)
                    ->orWhere('email', 'LIKE', $search_key)
                    ->orWhere('mobile_number', 'LIKE', $search_key);
                });
            }

            $users = $users->get();

            $response['status'] = true;
            $response['message'] = 'Group Member list.';
            $response['data'] = $users;

            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function deleteGroupMember(Request $request)
    {
        try {
            $rules = [
                'member_id' => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response['status'] = false;
                $response['message'] = $this->validationHandle($validator->messages());
                $response['data'] = [];
                return response()->json($response);
            }

            $user_id = JWTAuth::user()->id;
            $user = User::find($user_id);

            $user->bartenderGroupMembers()->where('id', $request->member_id)->delete();
            $response['status'] = true;
            $response['message'] = 'Member deleted successfully.';
            $response['data'] = $user->bartenderGroupMembers()->get();
            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function updateGroupMember(Request $request)
    {
        try {
            $rules = [
                'name' => 'required',
                'email' => 'required|email',
                'mobile' => 'required',
                'experience' => 'required',
                'signature_drink' => 'required',
                'member_id' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response['status'] = false;
                $response['message'] = $this->validationHandle($validator->messages());
                $response['data'] = [];
                return response()->json($response);
            }

            $user_id = JWTAuth::user()->id;
            $user = User::find($user_id);

            $member = $user->bartenderGroupMembers()->where('id', $request->member_id)->first();
            if ($member) {
                $member->name  = $request->name;
                $member->email = $request->email;
                $member->mobile_number = $request->mobile;
                $member->experience = $request->experience;
                $member->signature_drink = $request->signature_drink;
                if ($request->file('photo')) {
                    $destinationPath = '/uploads/bartender-group-members/';
                    $response_data = Uploader::doUpload($request->file('photo'), $destinationPath);
                    $member->photo = $response_data['file'];
                }
                $member->save();

                $response['status'] = true;
                $response['message'] = 'Member details updated successfully.';
                $response['data'] = $member;
            } else {
                $response['status'] = false;
                $response['message'] = 'No member found';
            }
            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function orderList(Request $request)
    {
        try {
            $validatorRules = [
                'search'=>'nullable'

            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }

            $user_id = JWTAuth::user()->id;

            $orders =  Order::with('orderItems', 'user')
            ->whereHas('orderItems', function (Builder $query) use ($user_id) {
                $query->where(['status' => 'NEW','bartender_id' => $user_id]);
            });
            if (!empty($request->search)) {
                $search_key = '%'.$request->search.'%';
                $orders = $orders->where(function ($query) use ($search_key) {
                    $query->where('order_number', 'LIKE', $search_key)
                    ->orWhere('order_type', 'LIKE', $search_key)
                    ->orWhereHas('customerInfo', function (Builder $query) use($search_key) {
                        $query->where('first_name', 'LIKE', $search_key)
                        ->orWhere('last_name', 'LIKE', $search_key)
                        ->orWhere('email', 'LIKE', $search_key)
                        ->orWhere('phone_country_code', 'LIKE', $search_key);
                    });
                });
            }
            $orders = $orders->orderBy('created_at', 'desc')
            ->paginate();
            $response['status'] = true;
            $response['message'] = 'Orders List.';
            $response['data'] = $orders;
            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function acceptOrder(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            DB::beginTransaction();
            $orderItem = OrderItem::where(['bartender_id' => $user_id,'order_id'=>$request->order_id])->firstOrFail();
            $orderItem->status = 'ACCEPTED';
            $orderItem->save();

            $order = Order::where('id', $request->order_id)->firstOrFail();
            $order->status = 'ACCEPTED';
            $order->save();

            DB::commit();
            $wallet_params = ['order_id'=>$request->order_id];
            Order::bartenderAmountCredit($wallet_params);
            $response['status'] = true;
            $response['message'] = 'Order Status updated successfully.';
            $response['data'] = $order;
            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function rejectOrder(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;

            DB::beginTransaction();
            $orderItem = OrderItem::where(['order_id'=>$request->order_id])->firstOrFail();
            $orderItem->status = 'REJECTED';
            $orderItem->save();

            $order = Order::where('id', $request->order_id)->firstOrFail();
            $order->status = 'REJECTED';
            $order->save();
            DB::commit();
            $params = [
                'order_id'=> $request->order_id
            ];
            $res = Order::refundOrderBartender($params);

            $response['status'] = true;
            $response['message'] = 'Order Status updated successfully.';
            $response['data'] = $order;
            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function acceptedOrderList(Request $request)
    {
        try {
            $validatorRules = [
                'search'=>'nullable'
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            // $orders =  Order::where(['user_id'=>$user_id,'status' => 'ACCEPTED'])->orderBy('created_at','desc')->with('user','orderItems')->paginate();
            $orders =  Order::with('orderItems', 'user')->whereHas('orderItems', function (Builder $query) use ($user_id) {
                $query->where(['status' => 'ACCEPTED','bartender_id' => $user_id]);
            });
            if (!empty($request->search)) {
                $search_key = '%'.$request->search.'%';
                $orders = $orders->where(function ($query) use ($search_key) {
                    $query->where('order_number', 'LIKE', $search_key)
                    ->orWhere('order_type', 'LIKE', $search_key)
                    ->orWhereHas('customerInfo', function (Builder $query) use($search_key) {
                        $query->where('first_name', 'LIKE', $search_key)
                        ->orWhere('last_name', 'LIKE', $search_key)
                        ->orWhere('email', 'LIKE', $search_key)
                        ->orWhere('phone_country_code', 'LIKE', $search_key);
                    });
                });
            }
            $orders = $orders->orderBy('created_at', 'desc')
            ->paginate();
            $response['status'] = true;
            $response['message'] = 'Accepted Orders list.';
            $response['data'] = $orders;
            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function orderHistory(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            // $orders = Order::where('user_id',$user_id)->where('status','!=','NEW')->orderBy('created_at','desc')->with('user','orderItems')->paginate();
            $orders =  Order::with('orderItems', 'user')->whereHas('orderItems', function (Builder $query) use ($user_id) {
                $query->where('status', '!=', 'NEW')->where('bartender_id', $user_id);
            })->orderBy('created_at', 'desc')->paginate();
            $response['status'] = true;
            $response['message'] = 'Orders History.';
            $response['data'] = $orders;
            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function orderDetail(Request $request)
    {
        try {
            $rules = [
                'order_id' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response['status'] = false;
                $response['message'] = $this->validationHandle($validator->messages());
                $response['data'] = [];
                return response()->json($response);
            }
            $user_id = JWTAuth::user()->id;
            $order = Order::where('id', $request->order_id)->with('user', 'orderItems')->whereHas('orderItems', function (Builder $query) use ($user_id) {
                $query->where('bartender_id', $user_id);
            })->first();

            $order->is_review_given = Review::checkReviewGiven($order->id, $user_id, $order->user_id);
            $response['status'] = true;
            $response['message'] = 'Order Detail.';
            $response['data'] = $order;

            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function notificationSetting(Request $request)
    {
        try {
            $validatorRules = [
                'is_notification_on' => 'required|in:0,1'
            ];
            $validator = Validator::make($request->all(), $validatorRules);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status'=>false,'message'=>$error]);
            } else {
                $user_id = JWTAuth::user()->id;
                $user_row = User::where(['id'=>$user_id])->first();
                $user_row->is_notification_on = $request->is_notification_on;
                $user_row->save();
                $user = User::where('id', $user_id)
                ->with(['countryData','bartending'])
                ->first();
                return response()->json([
                    'status' => true,
                    'message' => 'Notification setting has been updated.',
                    'data' => $user
                ]);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function getNotifications()
    {
        $user_id = JWTAuth::user()->id;
        $notifications = Notification::with(['orderData'=> function ($query) {
            $query->select('id', 'order_number');
        },
        'orderData.customerInfo'=>function ($query) {
            $query->select('id', 'first_name', 'last_name', 'profile_image');
        }
        ])->where('user_id', $user_id)
        ->orderBy('id', 'DESC')
        ->paginate(10)
        ->toArray();

        $notifications['data'] = Notification::getNotificationFormatted($notifications['data']);
        return response()->json([
            'status' => true,
            'message' => 'Notifications.',
            'data' => $notifications
        ]);
    }

    public function toggleAvailability(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $rules = [
                'status' => 'required|in:0,1',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response['status'] = false;
                $response['message'] = $this->validationHandle($validator->messages());
                $response['data'] = [];
                return response()->json($response);
            }
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status'=>false,'message'=>$error]);
            }

            $user = User::findOrFail($user_id);
            $user->availability = $request->status;
            $user->save();
            return response()->json([
                    'status' => true,
                    'message' => 'Availablity status updted.',
                    'data' => []
                ]);
        } catch (Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    

    public function getFaqCategories(Request $request) {
        try {
            $categories = FaqCategory::where(['status'=>'1'])
            ->get();
            return response()->json(['status'=>true, 'message'=>'Categories', 'data'=>$categories]);
            
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    public function contanctUs(Request $request){
        try {
            $validatorRules = [
                'faq_category_id' => 'required|exists:faq_categories,id',
                'message' => 'required'
            ];

            $customMessages = [
                'message.required' => 'The description field is required.',
            ];
            $validator = Validator::make($request->all(), $validatorRules, $customMessages);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status'=>false,'message'=>$error]);
            }
            $user_id = JWTAuth::user()->id;
            $entity = new ContactRequest();
            $entity->user_id = $user_id;
            $entity->faq_category_id = $request->faq_category_id;
            $entity->message = $request->message;
            $entity->save();
            return response()->json(['status'=>true, 'message'=>'Your query has been successfully sent to admin.', 'data'=>[]]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }

    /*
    public function myEarnings(Request $request)
    {
        try {
            $validatorRules = [
                'search'=>'nullable'
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }

            $user_id = JWTAuth::user()->id;
            $total_earning = Order::whereHas('orderItems', function (Builder $query) use ($user_id) {
                $query->where('bartender_id', $user_id);
            })
            ->sum('sub_total');

            $data = Order::with('orderItems')
            ->whereHas('orderItems', function (Builder $query) use ($user_id) {
                $query->where('bartender_id', $user_id);
            });

            if (!empty($request->search)) {
                $search_key = '%'.$request->search.'%';
                $data = $data->where(function ($query) use ($search_key) {
                    $query->where('order_number', 'LIKE', $search_key)
                    ->orWhere('order_type', 'LIKE', $search_key)
                    ->orWhereHas('customerInfo', function (Builder $query) use($search_key) {
                        $query->where('first_name', 'LIKE', $search_key)
                        ->orWhere('last_name', 'LIKE', $search_key)
                        ->orWhere('email', 'LIKE', $search_key)
                        ->orWhere('phone_country_code', 'LIKE', $search_key);
                    });
                });
            }

            $data = $data->paginate(10)
            ->toArray();

            $data['data'] = Order::getEarningsFormatted($data['data']);
            $data['total_earning'] = $total_earning;
            return response()->json([
                'status' => true,
                'message' => 'List',
                'data' => $data
            ]);
        } catch (Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }
    */

    public function getEarnings(Request $request)
    {
        try {
            $user_id = JWTAuth::user()->id;
            $current_balance = Wallet::where(['user_id'=>$user_id])->sum('amount');
            $total_earning = Wallet::where(['user_id'=>$user_id, 'type'=>'CR'])->sum('amount');
            
            $data = Wallet::with(['order.user'])
            ->where(['user_id'=>$user_id])
            ->orderBy('id', 'DESC')
            ->paginate(10)
            ->toArray();
            $data['data'] = Wallet::getDataFormatted($data['data']);
            $data['current_balance'] = $current_balance;
            $data['total_earning'] = $total_earning;
            return response()->json(['status' => true, 'message' => 'Transactions', 'data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
        }
    }

    public function withdraw(Request $request)
    {
        try {
            $rules = [
                'amount' => 'required|numeric'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status'=>false,'message'=>$error]);
            }
            $user_id = JWTAuth::user()->id;
            $stripe_connect_id = JWTAuth::user()->stripe_connect_id;
            $amount = $request->amount;
            if(empty($stripe_connect_id)){
                return response()->json(['status'=>false,'message'=>'Your Connect account is not setup.']);
            }
            $current_balance = Wallet::where(['user_id'=>$user_id])
                ->sum('amount');
            if($amount > $current_balance){
                return response()->json(['status'=>false,'message'=>'Amount should be less than or equal to current balance.']);
            }
            $amount_multi = $amount * 100;
            $stripePay = new StripePayment();
            $strip_res = $stripePay->transfer($amount_multi, $stripe_connect_id);
            if(isset($strip_res->id)){
                $wallet_params = [
                    'user_id'=>$user_id,
                    'order_id'=>null,
                    'transaction_type'=>'Withdrawal',
                    'type'=>'DR',
                    'amount'=> -$amount,
                    'transaction_id'=>$strip_res->id,
                ];
                Wallet::addEntryWallet($wallet_params);
                return response()->json(['status'=>true,'message'=>'Amount has been withdrawal successfully.']);
            } else {
                return response()->json(['status'=>false,'message'=>'Some error occurred.']);
            }
            
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage(), 'data'=>[]]);
        }
    }


}
