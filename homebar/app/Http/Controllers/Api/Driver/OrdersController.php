<?php

namespace App\Http\Controllers\Api\Driver;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Lib\Uploader;
use DB;
use Illuminate\Support\Facades\Validator;
use \Config;
use JWTAuth;
use JWTAuthException;
use App\Model\Order;
use App\Model\OrderItem;
use App\Model\UserLocation;


class OrdersController extends Controller
{

    public function getOrderDetail(Request $request)
    {
        try {
            $status_arr = Config::get('params.status_arr');
            $validatorRules = [
                'order_id'=>'required|exists:orders,id',
                //'order_item_id'=>'nullable|exists:order_items,id',
                'restaurant_id'=>'required',
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }
            $user_id = JWTAuth::user()->id;
            $driver_location = UserLocation::where(['user_id'=>$user_id])->first();
            $lat = $driver_location->lat;
            $lng = $driver_location->lng;
            if(!empty($driver_location->id)){
                $lat = $driver_location->lat;
                $lng = $driver_location->lng;
            }
            $restaurant_id = !empty($request->restaurant_id) ? $request->restaurant_id : null;
            $distance_num = Config::get('params.distance_num');

            $order = Order::with([
                'customerInfo'=>function($query){
                    $query->select('id','first_name', 'last_name', 'phone_country_code', 'phone', DB::raw("CONCAT('/public', profile_image) as profile_image"));
                },
                'orderItems'=>function($query) use($restaurant_id) {
                    if($restaurant_id){
                        $query->where('restaurant_id', $restaurant_id);
                    }
                },
                'orderItems.menuItemDetail'=>function($query){
                    $query->select('id','name', 'size', 'category_id','is_alcoholic');
                },
                'orderItems.ingredientDetail',
                'orderItems.restaurentDetail'=>function($query) use($distance_num, $lat, $lng) {
                    $query->select('id','business_name', 'cover_image', 'latitude', 'longitude', DB::raw("ROUND(( $distance_num * acos( cos( radians($lat) ) * cos( radians( users.latitude ))
            * cos( radians( users.longitude ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(users.latitude))  )), 1) as distance"));
                },
                'orderItems.menuItemDetail.categoryData',
            ])
            ->where(['id'=>$request->order_id])
            ->first()
            ->toArray();

            $order_items = $order['order_items'];
            $restaurants = $menu_items_restaurant = $rest_qty_arr = [];
            foreach($order_items as $key => $item_row){
                $restaurants[$item_row['restaurant_id']] = $item_row['restaurent_detail'];
                unset($item_row['restaurent_detail']);
                $menu_items_restaurant[$item_row['restaurant_id']][] = $item_row;
                $rest_qty_arr[$item_row['restaurant_id']][] =  $item_row['quantity'];
                //$restaurant[$item_row->restaurant_id] =
            }

            unset($order['order_items']);
            $rest_items = [];
            foreach($restaurants as $rest_id => $rest_detail) {
                $menu_items_restaurant[$rest_id];
                $rest_order_items = $menu_items_restaurant[$rest_id];
                $rest_qty = $rest_qty_arr[$rest_id];
                $rest_qty_sum = array_sum($rest_qty);
                $rest_detail['total_quantity'] = $rest_qty_sum;
                $rest_detail['order_items'] = $menu_items_restaurant[$rest_id];
                $rest_items[] = $rest_detail;
            }
            $order['restaurants'] = $rest_items;
            return response()->json([
                'status' => true,
                'message' => 'Order.',
                'data' =>$order
            ]);

        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage().$e->getLine(), 'data'=>[]]);
        }
    }

    public function getOrders(Request $request){
        try {
            $user_id = JWTAuth::user()->id;
            //$user_id = 223;
            $status_arr = Config::get('params.status_arr');

            $order_items = OrderItem::with(['order','menuItemDetail.images', 'restaurentDetail'])
            ->where(function($query) use($user_id){
                $query->whereRaw('FIND_IN_SET('.$user_id.',requested_driver_ids)')
                ->orWhere('driver_id', $user_id);
            })
            ->whereIn('status', ['ACCEPTED','IN KITCHEN','READY TO PICKUP','PICKED UP','START FOR DELIVERY', 'DELIVERED'])
            ->groupBy('order_id')
            ->groupBy('restaurant_id')
            ->orderBy('id', 'DESC')
            ->get();
            $accepted_order = null;
            if(!empty($order_items)){
                foreach($order_items as $item_key => $item_row){
                    if($item_row->driver_id == $user_id){
                        $accepted_order = $item_row;
                        break;
                    }
                }
            }
            $data = ['accepted_order'=>$accepted_order, 'all_orders'=> $order_items];
            //pr($data); die;
            return response()->json([
                'status' => true,
                'message' => 'Orders',
                'data' =>$data
            ]);

        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage().$e->getLine(), 'data'=>[]]);
        }
    }
    public function orderStatusUpdate(Request $request){
        try {
            $user_id = JWTAuth::user()->id;

            $validatorRules = [
                'order_id'=>'required|exists:orders,id',
                //'order_item_id'=>'nullable|exists:order_items,id',
                'restaurant_id'=>'required',
                'status'=>'required',
            ];
            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }

            $order = Order::where(['id'=>$request->order_id])->first();

            if(!empty($order)){
                if (!empty($request->image)) {
                    $destinationPath = '/uploads/order/';
                    $response_data = Uploader::saveImageFromUrl($request->image, $destinationPath);
                    if ($response_data['status'] == true) {
                        $order->image = $response_data['file'];
                    }
                }

                $order->status = $request->status;
                if($order->save()) {
                    OrderItem::where(['order_id'=>$request->order_id,'restaurant_id'=>$request->restaurant_id])->update(['status'=>$request->status]);
                    $status = true;
                    $order = Order::where(['id'=>$request->order_id])->first();

                    $data = $order;
                    $title = "Order ".$request->status;
                    $message = "Order status updated successfully.";

					sendNotification($request->restaurant_id,$title,$message,"Order",$order_id);

					sendNotification($order->user_id,$title,$message,"Order",$order_id);

                }
                else {
                    $status = false;
                    $data = [];
                    $message = "Order status not updated.";
                }
            }
            else {
                $status = false;
                $data = [];
                $message = "Order not found.";
            }
            //pr($data); die;
            return response()->json([
                'status' => $status,
                'message' => $message,
                'data' =>$data
            ]);

        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'message'=>$e->getMessage().$e->getLine(), 'data'=>[]]);
        }
    }
}//end class.
