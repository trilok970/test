<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\DoItYourself;
use App\Model\Type;
use App\Model\Category;
use App\Model\VendorIngredient;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use stdClass;
use App\Model\DoItYourselfIngredient;

class DoitYourselfController extends Controller {

    public function index()
    {
        $user = Auth::user();
        $params =  request()->route()->parameters;
        $type = Type::where('slug',$params['type'])->get(['id','slug'])->first();
        $categories = Category::where('type_id',$type->id)->where('parent_id','=',NULL)->get(['id','type_id','name','slug']);
        $itemsObj = DoItYourself::where('type_id',$type->id);
        $category = new Category();
        if(!empty($params['category'])) {
            $category = Category::where('slug', $params['category'])->get(['id','slug'])->first();
            $itemsObj->where('category_id',$category->id);
        }
        $items = $itemsObj->with('ingredients','category','ingredients.vendors')->get();
        return view('vendors.diy.index',compact('categories','items','type','category','user'));
    }

    public function show()
    {
        $user = Auth::user();
        $params =  request()->route()->parameters;
        $type = Type::where('slug',$params['type'])->get(['id','name','slug'])->first();
        $category = Category::where('slug', $params['category'])->get(['id','name','slug'])->first();
        $item = DoItYourself::where('slug', $params['item'])->first();

        $vendorIngredientsObj =  VendorIngredient::where('vendor_id',$user->id)->orderBy('id','desc')->get();
        $vendorIngredients = [];
        foreach ($vendorIngredientsObj as $ingredient) {
           $vendorIngredients[$ingredient->ingredient_id] = $ingredient;
        }
        // echo "<pre>";
        // print_r($vendorIngredient->toArray());
        // exit;
        return view('vendors.diy.show',compact('type','category','item','user','vendorIngredients'));
    }

    public function storeVendorItems(Request $request)
    {
        echo "<pre>";
        print_r($_POST); 
        // exit;
        try {
            $user = Auth::user();

            $index = 0;
            $arr = [];
            VendorIngredient::where(['vendor_id' => $user->id,'diy_id' => $request->diy_id])->delete();
         
            foreach ($request->ingredient_id as $ingredient) {
                // $is_available = 'is_available_'.$ingredient;
                if($request->is_available) {
                    $is_available = array_key_exists($ingredient,$request->is_available) ?? 0;
                    var_dump($is_available);
                    if($is_available === true) {
                        $row = [
                            'qty' => $request->qty[$index],
                            'cost' => $request->cost[$index],
                            'in_stock' => $request->in_stock[$index] ?? "No",
                            'diy_id' => $request->diy_id,
                            'ingredient_id' => $ingredient,
                            'vendor_id' => $user->id,
                        ];
                        // $arr[$ingredient] = $row;
                        $arr[] = $row;
                    // $user->vendorIngredients()->attach($arr);
    
                    }
                    $index++;
                }
                
            }
            VendorIngredient::insert($arr);

            // print_r($arr);

            // exit;
            $request->session()->flash('success','Details Saved Successfully');
        }catch(Exception $e) {
            $request->session()->flash('danger','Details can\'t be saved');
        }

        return redirect()->route('vendor.diy.index',[$request->type]);
    }
    public function storeVendorItemsOld(Request $request)
    {
        
        try {
            $user = Auth::user();

            $index = 0;
            $arr = [];

            foreach ($request->ingredient_id as $ingredient) {
                $is_available = 'is_available_'.$ingredient;
                if($request->$is_available === 'on') {
                    $row = [
                        'qty' => $request->qty[$index],
                        'cost' => $request->cost[$index],
                        'in_stock' => $request->in_stock[$index],
                        'diy_id' => $request->diy_id,
                    ];
                    $arr[$ingredient] = $row;
                }
                $index++;
            }
            VendorIngredient::where(['vendor_id' => $user->id,'diy_id' => $request->diy_id])->delete();
            $user->vendorIngredients()->attach($arr);
            $request->session()->flash('success','Details Saved Successfully');
        }catch(Exception $e) {
            $request->session()->flash('danger','Details can\'t be saved');
        }

        return redirect()->route('vendor.diy.index',[$request->type]);
    }
}
