<?php
namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\OrderItem;
use App\Model\Type;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use DB;

class OrderController extends Controller
{
    public function getTotalOrderCount($type_id)
    {
        $user = Auth::user();
        $orders['New'] = OrderItem::selectRaw("IFNULL(count(DISTINCT(order_id)),0) as total_count")->where('bartender_id', '=', null)->where('restaurant_id', '=', $user->id)->where('status', 'NEW')->whereHas('order', function($q) use ($type_id) {
            $q->when($type_id,function($query) use ($type_id) {
                $query->where('type_id',$type_id);
            });
        })->first()->total_count;


        $orders['In Progress'] = OrderItem::selectRaw("IFNULL(count(DISTINCT(order_id)),0) as total_count")->where('bartender_id', '=', null)->where('restaurant_id', '=', $user->id)->whereIn('status', ['ACCEPTED','IN KITCHEN','READY TO PICKUP','START FOR DELIVERY'])->whereHas('order', function($q) use ($type_id) {
            $q->when($type_id,function($query) use ($type_id) {
                $query->where('type_id',$type_id);
            });
        })->first()->total_count;


        $orders['REJECTED'] = OrderItem::selectRaw("IFNULL(count(DISTINCT(order_id)),0) as total_count")->where('bartender_id', '=', null)->where('restaurant_id', '=', $user->id)->whereIn('status', ['REJECTED','CANCELLED'])->whereHas('order', function($q) use ($type_id) {
            $q->when($type_id,function($query) use ($type_id) {
                $query->where('type_id',$type_id);
            });
        })->first()->total_count;
        $orders['COMPLETED'] = OrderItem::selectRaw("IFNULL(count(DISTINCT(order_id)),0) as total_count")->where('bartender_id', '=', null)->where('restaurant_id', '=', $user->id)->whereIn('status', ['DELIVERED','PICKED UP'])->whereHas('order', function($q) use ($type_id) {
            $q->when($type_id,function($query) use ($type_id) {
                $query->where('type_id',$type_id);
            });
        })->first()->total_count;
        return $orders;



    }
    public function index()
    {
        $user = Auth::user();
        $orderPageTitle = 'Recent Orders';
        $params =  request()->route()->parameters;
        $typeParam = !empty($params) ? $params['type'] : '';
        $type = Type::where('slug', $typeParam)->first();
        if($type) {
            $orders = Order::where('type_id', $type->id)->with('user', 'items')->whereHas('items', function (Builder $query) use ($user) {
                $query->where('restaurant_id', '=', $user->id)->where('bartender_id', '=', null)->where('status', 'NEW');
            })->orderBy('created_at', 'desc')->paginate(10);
        }
        else {
            $orders = Order::where('type_id', '!=', 4)->with('user', 'items')->whereHas('items', function (Builder $query) use ($user) {
                $query->where('restaurant_id', '=', $user->id)->where('bartender_id', '=', null)->where('status', 'NEW');
            })->orderBy('created_at', 'desc')->paginate(10);
        }


        $statusCountResult = $this->getTotalOrderCount($type->id ?? 0);
        // echo   $statusCountResult[0]->status_count;

        //echo "<pre>";
        // print_r($typeParam);
        // exit;
        // echo count($orders);exit;

        return view('vendors.orders.index', compact('orders', 'orderPageTitle', 'statusCountResult'));
    }

    public function inProgressOrders()
    {
        $orderPageTitle = 'Orders in Progress';
        $user = Auth::user();
        $params =  request()->route()->parameters;
        $typeParam = !empty($params) ? $params['type'] : '';
        $type = Type::where('slug', $typeParam)->first();
        if($type) {
            $orders = Order::where('type_id', $type->id)->with('user', 'items')->whereHas('items', function (Builder $query) use ($user) {
                $query->where('restaurant_id', '=', $user->id)->where('bartender_id', '=', null)->whereIn('status', ['ACCEPTED','IN KITCHEN','READY TO PICKUP','START FOR DELIVERY']);
            })->orderBy('created_at', 'desc')->paginate(10);
        }
        else {
            $orders = Order::where('type_id', '!=', 4)->with('user', 'items')->whereHas('items', function (Builder $query) use ($user) {
                $query->where('restaurant_id', '=', $user->id)->where('bartender_id', '=', null)->whereIn('status', ['ACCEPTED','IN KITCHEN','READY TO PICKUP','START FOR DELIVERY']);
            })->orderBy('created_at', 'desc')->paginate(10);
        }

        $statusCountResult = $this->getTotalOrderCount($type->id ?? 0);

        return view('vendors.orders.index', compact('orders', 'orderPageTitle', 'statusCountResult'));
    }

    public function completeOrders()
    {
        $orderPageTitle = 'Orders Completed';
        $user = Auth::user();
        $params =  request()->route()->parameters;
        $typeParam = !empty($params) ? $params['type'] : '';
        $type = Type::where('slug', $typeParam)->first();
        if($type) {
            $orders = Order::where('type_id', $type->id)->with('user', 'items')->whereHas('items', function (Builder $query) use ($user) {
                $query->where('restaurant_id', '=', $user->id)->where('bartender_id', '=', null)->whereIn('status', ['DELIVERED','PICKED UP']);
            })->orderBy('created_at', 'desc')->paginate(10);
        }
        else {
            $orders = Order::where('type_id', '!=', 4)->with('user', 'items')->whereHas('items', function (Builder $query) use ($user) {
                $query->where('restaurant_id', '=', $user->id)->where('bartender_id', '=', null)->whereIn('status', ['DELIVERED','PICKED UP']);
            })->orderBy('created_at', 'desc')->paginate(10);
        }

        $statusCountResult = $this->getTotalOrderCount($type->id ?? 0);
        return view('vendors.orders.index', compact('orders', 'orderPageTitle', 'statusCountResult'));
    }

    public function rejectedOrders()
    {
        $orderPageTitle = 'Orders Rejected';
        $user = Auth::user();
        $params =  request()->route()->parameters;
        $typeParam = !empty($params) ? $params['type'] : '';
        $type = Type::where('slug', $typeParam)->first();
        if($type) {
            $orders = Order::where('type_id', $type->id)->with('user', 'items')->whereHas('items', function (Builder $query) use ($user) {
                $query->where('restaurant_id', '=', $user->id)->where('bartender_id', '=', null)->whereIn('status', ['REJECTED','CANCELLED']);
            })->orderBy('created_at', 'desc')->paginate(10);
        }
        else {
            $orders = Order::where('type_id', '!=', 4)->with('user', 'items')->whereHas('items', function (Builder $query) use ($user) {
                $query->where('restaurant_id', '=', $user->id)->where('bartender_id', '=', null)->whereIn('status', ['REJECTED','CANCELLED']);
            })->orderBy('created_at', 'desc')->paginate(10);
        }

        $statusCountResult = $this->getTotalOrderCount($type->id ?? 0);


        return view('vendors.orders.index', compact('orders', 'orderPageTitle', 'statusCountResult'));
    }

    public function show($list, $id)
    {
        $userId = auth()->user()->id;
        $order = Order::where('id', $id)->with('user', 'items', 'items.menuItemDetail', 'items.menuItemDetail.images')->firstOrFail();
        $singleItem = OrderItem::where('order_id', $id)->where(['restaurant_id' => $userId])->first();
        return view('vendors.orders.show', compact('order', 'singleItem'));
    }

    public function acceptOrder(Request $request)
    {
        $userId = auth()->user()->id;
        $type =  !empty($request->type)? $request->type : 'drinks';
        $order_id = $request->order_id;
       // $barType = $request->type;
		 $date = date('Y-m-d h:i:s', time());
         if($type=='bartender')
        $affectedRows = OrderItem::where(['order_id' => $order_id,'restaurant_id' => $userId])->update(['status' => 'DELIVERED']);
        else
         $affectedRows = OrderItem::where(['order_id' => $order_id,'restaurant_id' => $userId])->update(['status' => 'ACCEPTED']);

        if ($affectedRows) {
            $orderItem = OrderItem::where(['order_id' => $order_id,'restaurant_id' => $userId])->first();
            $orderInfo=Order::where('id', $order_id)->first();
			$orderAmountInfo = $orderInfo->getItemsTotalForAll();
            $grand_total = $orderAmountInfo['grand_total'];
			if($type == 'bartender'){
				//$amount = '+'.($orderInfo->sub_total+$orderInfo->service_fee+$orderInfo->tax-$orderInfo->admin_commission);
				$amount = '+'.($grand_total);
                    DB::table('wallets')->insert(
                        ['user_id' =>$userId, 'order_id' => $order_id,'transaction_type'=>'Order','type'=>'CR','amount'=>$amount,'created_at' => $date,'updated_at'=>$date]
                    );

			}

				$title = 'Order Accepted.';
				$message = 'Order Accepted Successfully.';
				sendNotification($orderInfo->user_id,$title,$message,"Order",$order_id);


            $request->session()->flash('success', 'Order Accepted Successfully.');
            return ['status' => 1,'vendor_id' => $userId,'orderItem' => $orderItem];
        } else {
            $request->session()->flash('danger', 'Something went wrong, Try again.');
            return ['status' => 0,'msg' => 'Something went wrong. Try again.'];
        }
    }

    public function rejectOrder(Request $request)
    {
	  //  echo "dfds"; die;
        try {
            $userId = auth()->user()->id;
            $type =  !empty($request->type)? $request->type : 'drinks';
            $order_id = $request->order_id;
            $refundParm = array("order_id"=>$order_id, "restaurant_id"=>$userId);
            $affectedRows = OrderItem::where(['order_id' => $order_id,'restaurant_id' => $userId])->update(['status' => 'REJECTED']);

            if ($request->order_type == 'PREMADE' || $request->order_type == 'DIY') {
                $refund = Order::refundOrder($refundParm);
            } elseif ($request->order_type == 'BARTENDER') {
                $refund = Order::refundOrderBartender($refundParm);
            }
            if ($affectedRows) {

                $orderItem = OrderItem::where(['order_id' => $order_id,'restaurant_id' => $userId])->first();
                $request->session()->flash('success', 'Order Rejected Successfully.');
				
				$orderInfo=Order::where('id', $order_id)->first();
				$title = 'Orders Rejected.';
				$message = 'Orders Rejected Successfully.';
				sendNotification($orderInfo->user_id,$title,$message,"Order",$order_id);
		

                return ['status' => 1,'vendor_id' => $userId,'orderItem' => $orderItem];
            } else {
                $request->session()->flash('danger', 'Something went wrong, Try again.');
                return ['status' => false,'msg' => 'Something went wrong. Try again.'];
            }
        } catch (Exception $e) {
            return ['status' => 0,'msg' => $e->getMessage()];
        }
    }


    public function updateOrderStatus(Request $request, $id)
    {
        try {
            $userId = auth()->user()->id;
            $order = Order::findOrFail($id);
            // $order->status = $request->status;
            // $order->save();
			$type =  !empty($request->type)? $request->type : 'drinks';
            $date = date('Y-m-d h:i:s', time());
            $order_id = $id;


            $orderInfo = Order::where('id', $order_id)->first();
            $orderAmountInfo = $orderInfo->getItemsTotalForAll();
            $grand_total = $orderAmountInfo['grand_total'];

            if($type=='bartender')
            {
               $request->status = "DELIVERED";
               $affectedRows = OrderItem::where(['order_id' => $order_id,'restaurant_id' => $userId])->update(['status' => $request->status]);
            }
            else
            $affectedRows = OrderItem::where(['order_id' => $order_id,'restaurant_id' => $userId])->update(['status' => $request->status]);
            if ($affectedRows) {
                $orderItem = OrderItem::where(['order_id' => $order_id,'restaurant_id' => $userId])->first();

                $refundParm = array("order_id"=>$order_id, "restaurant_id"=>$userId);
                if (($orderInfo->order_type == 'PREMADE' || $orderInfo->order_type == 'DIY') && $request->status == 'REJECTED') {
                    $refund = Order::refundOrder($refundParm);
                } elseif ($orderInfo->order_type == 'BARTENDER' && $request->status == 'REJECTED') {
                    $refund = Order::refundOrderBartender($refundParm);
                }

                if ($orderInfo->reach_type == 'PICKUP' && ($request->status == 'DELIVERED' || $request->status == 'PICKED UP')) {
                   // $amount = '+'.($orderInfo->sub_total+$orderInfo->service_fee+$orderInfo->tax-$orderInfo->admin_commission);
                    $amount = '+'.($grand_total);
                    DB::table('wallets')->insert(
                        ['user_id' =>$userId, 'order_id' => $order_id,'transaction_type'=>'Order','type'=>'CR','amount'=>$amount,'created_at' => $date,'updated_at'=>$date]
                    );
                }elseif($type == 'bartender' && $request->status == 'ACCEPTED'){

				// $amount = '+'.($orderInfo->sub_total+$orderInfo->service_fee+$orderInfo->tax-$orderInfo->admin_commission);
				$amount = '+'.($grand_total);
                    DB::table('wallets')->insert(
                        ['user_id' =>$orderItem->bartender_id, 'order_id' => $order_id,'transaction_type'=>'Order','type'=>'CR','amount'=>$amount,'created_at' => $date,'updated_at'=>$date]
                    );

			}
				if($request->status=="IN KITCHEN"){
					$request->status = "BEING PREPARED";
				}
				$title = 'Order '. $request->status;
				$message = 'Order status change to '. $request->status;
				sendNotification($orderInfo->user_id,$title,$message,"Order",$order_id);

                $request->session()->flash('success', 'Order status change to '. $request->status);
                return ['status' => 1, 'msg' => 'Order status change to '. $request->status,'vendor_id' => $userId,'orderItem' => $orderItem];
            } else {
                $request->session()->flash('danger', 'Something went wrong, Try again.');
                return ['status' => 0,'msg' => 'Something went wrong. Try again.'];
            }


            return redirect()->back()->with('success', 'Status updated Successfully');
        } catch (Exception $e) {
            return ['status' => 0,'msg' => $e->getMessage()];
        }
    }

    public function updateOrderItemStatus(Request $request, $id)
    {
        $userID = auth()->user()->id;
        try {
            $orderItem = OrderItem::where(['id' => $id,'order_id' => $request->orderId])->first();
            $orderItem->status = $request->status;
            $orderItem->save();
            return ['status' => 1,'vendorId' => $userID, 'orderItem' => $orderItem, 'msg' => 'Item status changed to '.$request->status];
        } catch (Exception $e) {
            return ['status' => 0,'msg' => $e->getMessage()];
        }
    }
}
