<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Type;
use App\Model\Offer;

class OfferController extends Controller {

    public function index()
    {
        $userId = auth()->user()->id;
        $params =  request()->route()->parameters;
        $type = Type::where('slug',$params['type'])->get(['id','slug','name'])->first();
        $offer = Offer::where('type_id',$type->id)->where('user_id',$userId)->first();
        return view('vendors.offers.index',compact('type','offer'));
    }

    public function update(Request $request,Offer $offer)
    {

        try {
            $request->validate([
                'discount' => 'required|digits_between:1,3',
                'applied_from' => 'required|date|after_or_equal:today',
                'applied_to' => 'required|date|after_or_equal:applied_from'
            ]);
            $userId = auth()->user()->id;
            $offer->discount_per = $request->discount;
            $offer->applied_from = date("Y-m-d",strtotime($request->applied_from));
            $offer->applied_to = date("Y-m-d",strtotime($request->applied_to));
            $offer->user_id = $userId;
            $offer->save();
            $request->session()->flash('success','Offer Updated Successfully.');
        } catch(Exception $e) {
            $request->session()->flash('danger','Offer Updated Successfully.');
        }
        return redirect()->back();
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'discount' => 'required|digits_between:1,3',
                'applied_from' => 'required|date|after_or_equal:today',
                'applied_to' => 'required|date|after_or_equal:applied_from'
            ]);
            $userId = auth()->user()->id;
            $offer = new Offer();
            $offer->discount_per = $request->discount;
            $offer->applied_from = $request->applied_from;
            $offer->applied_to = $request->applied_to;
            $offer->user_id = $userId;
            $offer->type_id = $request->type_id;
            $offer->save();
            $request->session()->flash('success','Offer Created Successfully.');
        } catch(Exception $e) {
            $request->session()->flash('danger','Offer can\'t be Created.');
        }
        return redirect()->back();
    }
}
