<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Country;
use App\Model\Order;
use App\Model\OrderItem;
use App\Model\Type;
use App\Model\Wallet;
use App\Model\BartenderAvailability;
use App\Model\UserNotification;
use DB;
use Illuminate\Support\Facades\Input;
use Session;
use Mail;
use Illuminate\Support\Facades\Validator;
use Config;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use App\Lib\Uploader;
use App\Lib\StripePayment;
use Illuminate\Support\Facades\Hash;
use Log;

class UsersController extends Controller {


    public function signin() {
        return view('vendors.users.signin');
    }

    public function signup() {

        $types = Type::get(['id','name']);
        $country_data = Country::all();
        $countries = [];
        foreach ($country_data as $key => $value) {
            $countries[$value->id] =  $value->name ;
        }

        return view('vendors.users.signup',compact('countries','types'));
    }

    public function store(Request $request) {

        $validatorRules = [
            'business_types'=>'required|max:255',
            'business_name'=>'required|max:255',
            'first_name'=>'required|max:255|regex:/^[a-zA-Z]+$/u',
            'last_name'=>'required|max:255|regex:/^[a-zA-Z]+$/u',
            'email' => 'required|nullable|email:rfc,dns|max:255|unique:users|regex:/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/m',
            'primary_mobile_number' => 'required|numeric|digits_between:7,15|unique:users',
            'secondary_mobile_number' => 'required|numeric|digits_between:7,15|unique:users',
            'address'=>'required',
            'latitude'=>'required|numeric',
            'longitude'=>'required|numeric',
            'city'=>'required|max:255',
            'zipcode' => 'required|alpha_num|string|min:4|max:5',
            'state'=>'required|max:255',
            'country_id' => 'required|numeric|digits_between:1,10|exists:countries,id',
            'open_time'=>'required|date_format:g:i A',
            'close_time'=>'required|date_format:g:i A|after:open_time',
            'password' => 'required|confirmed|max:20|min:8',
            'password_confirmation' => 'required|max:20|min:8',
            'term_service' => 'required',
        ];
        if(empty($request->pickup) && empty($request->delivery)) {
            $validatorRules['pickup'] = 'required';
        }

        try {
            $customMessages = [
                'regex' => 'The :attribute format is invalid. Alphabets only.',
                'pickup.required' => 'Either pickup/delivery is required',
                'zipcode.min' => 'Zipcode min length is 4 characters',
                'zipcode.max' => 'Zipcode max length is 5 characters',
            ];

            $validator = Validator::make($request->all(), $validatorRules, $customMessages);

            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                $error = $this->validationHandle($validator->messages());
                return redirect()->back()->withErrors($validator)->withInput();
            } else {
                $status_arr = \Config::get('params.status_arr');
                $country_data = Country::where([['id', $request->country_id]])->first();
                $country_code = isset($country_data->phonecode) ? $country_data->phonecode : '';
                $primary_mobile_number = $country_code.$request->primary_mobile_number;
                $secondary_mobile_number = $country_code.$request->secondary_mobile_number;
                // $country_code_mobile = $country_code.$request->phone;

                $user = new User();
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->business_type = implode(",",$request->business_types);
                $user->business_name = $request->business_name;
                $user->primary_mobile_number = $primary_mobile_number;
                $user->secondary_mobile_number = $secondary_mobile_number;
                $user->email = $request->email;
                $user->address = $request->address;
                $user->latitude = $request->latitude;
                $user->longitude = $request->longitude;
                $user->city = $request->city;
                $user->zipcode = $request->zipcode;
                $user->state = $request->state;
                $user->country_id = $request->country_id;
                $user->open_time =  date("H:i:s", strtotime($request->open_time));
                $user->close_time = date("H:i:s", strtotime($request->close_time));
                $user->role = Config::get('params.role_ids.vendor');
                $user->status = $status_arr['inactive'];
                $user->password = Hash::make($request->password);
                $user->is_pickup = $request->pickup =='on' ? '1': '0';
                $user->is_delivery = $request->delivery =='on' ? '1': '0';
                $token = randomToken();
                $user->token = $token;
                $user->save();



                $fullName = $request->first_name.' '.$request->last_name;

                $template = \App\Model\EmailTemplate::where([['slug', 'send-mail-when-vendor-registered']])->first();
                $site_name = getSettings()['site_title'];
                $subject = $template->subject;
                $description = $template->description;
                $link = route('vendor.verify', ['token'=>$token]);

                $subject = str_replace(
                    ['{FULL_NAME}', '{EMAIL}', '{SITE}', '{LINK}'],
                    [$fullName, $user->email, $site_name, $link],
                    $subject
                );

                $description = str_replace(
                        ['{FULL_NAME}', '{EMAIL}', '{SITE}', '{LINK}'],
                        [$fullName, $user->email, $site_name, $link],
                        $description
                        );

                $mail_data = [
                    'email' => $user->email,
                    'subject' => $subject,
                    'content' => $description
                ];
                mailSend($mail_data);


                return redirect()->route('vendor.login')->with('success', 'Signup successfully, check email for verification link.');
            }
        } catch (\Exception $e) {
            $err = $e->getMessage() . ' : '.$e->getLine();
            // return $e->getMessage();
            return redirect()->route('vendor.signup')->with('error', $e->getMessage());
        }

    }


    public function login(Request $request)
    {
        $data = $request->all();

        $title_page = 'Login';
        if ($request->isMethod('post')) {
            try {
                $validator = Validator::make($request->all(), [
                    'email' => 'required|email:rfc,dns',
                    'password' => 'required',
                ]);

                if ($validator->passes()){

                   $email = $request->email;
                   $password = bcrypt($request->password);

                    $matchThese = ['email' => $email, 'role' => '3', 'status' => '1'];
                    $user = User::where($matchThese)->first();

                    // $user = User::where([['username', $username],['role', Config::get('params.role_ids.admin')]])->first();

                    if (!empty($user) && Hash::check($request->password, $user->password)) {
                        Session::put('AdminLoggedIn', ['user_id' => $user->id, 'userData' => $user]);
                        Session::save();
                        // if(session()->has('url.intended'))
                        // {
                        //     return redirect()->intended();
                        // }
                        return redirect()->route('vendor.orders', 'drinks');
                    } else {
                        Session::flash('error', 'Invalid email or password.');
                        return redirect()->back()->withInput()->withErrors($validator->errors());
                    }
                }
                else
                {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
            } catch (\Exception $e) {
                $msg = $e->getMessage();
                Session::flash('error', $msg);
                return redirect()->back()->withInput();
            }
        }

        return view('vendors.users.signin', compact('title_page'));
    }


    public function dashboard(Request $request)
    {
         // $id = Auth::user();
         // $id = Auth::id();
         // print_r($id); die;
        $val = Session::get('VendorLoggedIn');

        return view('vendors.users.dashboard');
    }

    public function orders(Request $request, $type)
    {

        return view('vendors.users.orders', compact('type'));

    }

    public function logout(){
        Session::forget('VendorLoggedIn');
        Session::flush();
        return redirect()->route('vendor.signin');
    }// end function.




    public function forgot(Request $request) {
        $base_url =  url('/');
        $data = $request->all();
        // $title_page = 'Login';
        if ($request->isMethod('post')) {
            // print_r($request->all()); die;

            try {
                $validator = Validator::make($data,[
                    'email' => 'required|email',
                ]);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
                } else{
                    $email = $request->email;

                    $users = User::where('email', $data['email'])->first();
                    if ($users) {
                        // generating web token.
                        // $token = JWTAuth::fromUser($users);

                        // generate random token insert in user table & send with url.
                        $hash = md5($email).time();
                        $users->token = $hash;
                        $users->save();


                        $from_mail = 'football.league.web@gmail.com';
                        $site_title = 'Homebar';

                        // Send email
                        $sdata=[];
                        $sdata['name'] = $users->full_name;
                        // $sdata['token'] = $token;
                        $sdata['token'] = $hash;    // hash will be send in the url.
                        $sdata['base_url'] = $base_url;

                        Mail::send('emails.forgotPassmail', ['data' => $sdata], function ($m) use ($data, $from_mail, $site_title, $email) {
                            $m->from($from_mail, $site_title);
                            $m->to($email, 'name')->subject('Forget Password');
                        });

                        Session::flash('success', 'Email Sent. Check your inbox.');
                        return redirect()->back()->withInput();
                    } else {
                        Session::flash('danger', 'User not exists.');
                        return redirect()->back()->withInput();
                    }
                }


            } catch (\Exception $e) {
                echo $msg = $e->getMessage() .' | '.$e->getLine();
                die;

                Session::flash('danger', $msg);
                return redirect()->back()->withInput();
            }
        }
            return view('vendors.users.forgotpassword');
    }


    public function drinkprocessing(Request $request)
    {

        $val = Session::get('VendorLoggedIn');

        return view('vendors.users.drinkprocessing');

    }

    public function drinkcompletedorder(Request $request)
    {

        $val = Session::get('VendorLoggedIn');

        return view('vendors.users.drinkcompletedorder');

    }


    public function drinkmenulist(Request $request)
    {

        $val = Session::get('VendorLoggedIn');

        return view('vendors.users.drinkmenulist');

    }


    public function myProfile(Request $request)
    {
        $user = Auth::user();
        $utypes = explode(",",$user->business_type);
        $btypes = Type::get(['id','name']);
        // return var_dump($btypes[0]->id."" === $utypes[0]);
        return view('vendors.users.myprofile', compact('user','btypes','utypes'));

    }


    public function profile(Request $request)
    {

        $user = Auth::user();

        $country_data = Country::all();
        $countries = [];
        foreach ($country_data as $key => $value) {
            $countries[$value->id] =  $value->name ;
        }
        return view('vendors.users.editprofile',compact('user','countries'));

    }

    public function profileUpdate(Request $request, $id)
    {
        $base_url =  url('/');

        $rules = [
            'business_types'=>'required|max:255',
            'business_name'=>'required|max:255',
            'first_name'=>'required|max:255|regex:/^[a-zA-Z]+$/u',
            'last_name'=>'required|max:255|regex:/^[a-zA-Z]+$/u',
            // 'email' => 'required|nullable|email:rfc,dns|max:255|unique:users,email,'.$id,
            'email' => 'required|nullable|email|max:255|unique:users,email,'.$id,
            'primary_mobile_number' => 'required|numeric|digits_between:7,15|unique:users,primary_mobile_number,'.$id,
            'secondary_mobile_number' => 'required|numeric|digits_between:7,15|unique:users,secondary_mobile_number,'.$id,
            'address'=>'required',
            'latitude'=>'required|numeric',
            'longitude'=>'required|numeric',
            'city'=>'required|max:255',
            'zipcode' => 'required|alpha_num|string|min:4|max:5',
            'state'=>'required|max:255',
            'country_id' => 'required|numeric|digits_between:1,10|exists:countries,id',
            'open_time'=>'required|date_format:g:i A',
            'close_time'=>'required|date_format:g:i A|after:open_time',

        ];
        if(empty($request->pickup) && empty($request->delivery)) {
            $rules['pickup'] = 'required';
        }
        $customMessages = [
                'regex' => 'The :attribute format is invalid. Alphabets only.',
                'after' => 'The :attribute must be a time after open time.',
                'pickup.required' => 'Either pickup/delivery is required',
            ];

        // Defining validations for input fields.
        $validator = Validator::make($request->all(),$rules, $customMessages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            // Create Listing
            $user = User::find($id);
            $status_arr = \Config::get('params.status_arr');
            $country_data = Country::where([['id', $request->country_id]])->first();
            $country_code = isset($country_data->phonecode) ? $country_data->phonecode : '';
            if (strlen($user->primary_mobile_number) > strlen($request->primary_mobile_number)) {
                $primary_mobile_number = $country_code.$request->primary_mobile_number;
            } else {
                $primary_mobile_number = $request->primary_mobile_number;
            }

            if (strlen($user->secondary_mobile_number) > strlen($request->secondary_mobile_number)) {
                $secondary_mobile_number = $country_code.$request->secondary_mobile_number;
            } else {
                $secondary_mobile_number = $request->secondary_mobile_number;
            }

            $user->business_type = implode(",",$request->business_types);
            $user->business_name = $request->input('business_name');
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->email = $request->input('email');
            $user->primary_mobile_number = $primary_mobile_number;
            $user->secondary_mobile_number = $secondary_mobile_number;
            $user->address = $request->input('address');
            $user->latitude = $request->latitude;
            $user->longitude = $request->longitude;
            $user->city = $request->input('city');
            $user->zipcode = $request->input('zipcode');
            $user->state = $request->input('state');
            $user->country_id = $request->input('country_id');
            $user->open_time = date("H:i:s", strtotime($request->input('open_time')));
            $user->close_time = date("H:i:s", strtotime($request->input('close_time')));
            $user->is_pickup = $request->pickup =='on' ? '1': '0';
            $user->is_delivery = $request->delivery =='on' ? '1': '0';
            $user->save();

            $country_data = Country::all();
            $countries = [];
            foreach ($country_data as $key => $value) {
                $countries[$value->id] =  $value->name ;
            }
            return redirect()->route('vendor.myprofile')->with('success', 'Profile details Updated.');
            // return view('vendors.users.editprofile',compact('user','countries'))->with('success', 'Profile details Updated.');
        }

    }





    public function changePassword(Request $request)
    {
        $id = Auth::id();
        $data = $request->all();
        if ($request->isMethod('post')) {

            // Defining validations for input fields.
            $validator = Validator::make($request->all(),[
                'current_password' => 'required',
                'new_password' => 'required|confirmed|min:6',
                'new_password_confirmation' => 'required|min:6',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            } else {
                $userdata = User::where('id',$id)->first();
//                echo $request->input('current_password'); die;
				//echo "<pre>"; print_r($userdata); die;
				if(!Hash::check($request->input('current_password'),$userdata->password))
				{
	                return redirect()->back()->with('error', 'Previous password is correct !');
/*
				    return Redirect::to('/admin/profile')
				        ->with('message', 'Current Password Error !')
				        ->withInput();
*/
				}

                $user = User::find($id);

                // Valid current password
                $user->password = Hash::make($request->input('new_password'));
                $user->save();

                // return redirect()->back()->with('success', 'Password updated successfully');
                return redirect()->back()->with('success', 'Password Updated.');
            }
        }
        return view('vendors.users.changepassword');

    }



    public function bartendermyearning(Request $request)
    {

		$val = Session::get('VendorLoggedIn');
		$date = date('Y-m-d h:i:s', time());
       $user = Auth::user();
        $orderPageTitle = 'My Earning';
		    $email  = 'jitendar@ninehertzindia.com';
		     $stripeLink = User::getVenderStripeCreateLink($user->email);
      //die;
        /*$todayOrders = Order::with('user','items')->whereHas('items',function(Builder $query) use ($user){
            $query->where('restaurant_id', '=',$user->id)->where('status','DELIVERED')->whereDate('updated_at', '=', date('Y-m-d'));
        })->orderBy('created_at','desc')->get();

		$orders = Order::with('user','items')->whereHas('items',function(Builder $query) use ($user){
            $query->where('restaurant_id', '=',$user->id)->where('status','DELIVERED')->whereDate('updated_at', '!=', date('Y-m-d'));
        })->orderBy('created_at','desc')->paginate(10);*/


// 		 $todayOrders = Order::with('user','items')->whereHas('walletOrder',function(Builder $query) use ($user){
//             $query->where('user_id', '=',$user->id)->whereDate('updated_at', '=', date('Y-m-d'));
//         })->orderBy('created_at','desc')->get()->toArray();
// echo "<pre>";
// print_r($todayOrders);
// exit();
       $todayOrders = Wallet::where(['user_id'=>$user->id])->whereDate('updated_at', '=', date('Y-m-d'))->with('order','user')->orderBy('id','desc')->get();

		// trilok $orders = Order::with('user','items')->whereHas('walletOrder',function(Builder $query) use ($user){
  //           $query->where('user_id', '=',$user->id)->whereDate('updated_at', '!=', date('Y-m-d'));
  //       })->orderBy('created_at','desc')->paginate(10);

        $orders = Wallet::where(['user_id'=>$user->id])->whereDate('updated_at', '!=', date('Y-m-d'))->with('order')->orderBy('id','desc')->paginate(10);

		$walletBalance = Wallet::where('user_id',$user->id)->sum('amount');
		//echo $user->id;die;

		//echo  '<pre>';
      	//print_r($user);die;
      //echo  '</pre>';


		//if (Input::get("empname") != "") {
		if($request->isMethod('post')){

		//echo	$request->get('amount');die;

			 try {
				$rules = [
					'amount' => 'required|numeric'
				];
				$validator = Validator::make($request->all(), $rules);
				if ($validator->fails()) {
					$error = $this->validationHandle($validator->messages());
					return redirect("vendor/bartender-my-earning")->with('error_msg', $error)->send();
				}
				$user_id = $user->id;
				$stripe_connect_id = $user->stripe_connect_id;
				//$stripe_connect_id = 'acct_1HvdRg2EwZYciKGA';
				//$amount = $request->amount;
				$amount = $request->get('amount');
				if(empty($stripe_connect_id)){
					return redirect("vendor/bartender-my-earning")->with('error_msg', 'Your Connect account is not setup.')->send();
				}
				$current_balance = Wallet::where(['user_id'=>$user_id])
					->sum('amount');
				if($amount > $current_balance){
					return redirect("vendor/bartender-my-earning")->with('error_msg', 'Amount should be less than or equal to current balance.')->send();
				}
				$amount_multi = $amount * 100;
				$stripePay = new StripePayment();
				$strip_res = $stripePay->transfer($amount_multi, $stripe_connect_id);
				if(isset($strip_res->id)){
					$wallet_params = [
						'user_id'=>$user_id,
						'order_id'=>null,
						'transaction_type'=>'Withdrawal',
						'type'=>'DR',
						'amount'=> -$amount,
						'transaction_id'=>$strip_res->id,
					];
					Wallet::addEntryWallet($wallet_params);
					 return redirect("vendor/bartender-my-earning")->with('success_msg', "Amount has been withdrawal successfully.")->send();
				} else {
					return redirect("vendor/bartender-my-earning")->with('error_msg', 'Some error occurred.')->send();
				}

			} catch (\Exception $e) {
				 return redirect("vendor/bartender-my-earning")->with('error_msg', $e->getMessage())->send();
			}



		}


        	//echo  '<pre>';
      	//print_r($user);die;
      //echo  '</pre>';

        return view('vendors.users.bartendermyearning',compact('stripeLink','user','todayOrders','walletBalance','orders','orderPageTitle'));

    }

    public function drinkaddmenupre(Request $request)
    {

        $val = Session::get('VendorLoggedIn');

        return view('vendors.users.drinkaddmenupre');

    }


    public function foodaddmenupre(Request $request)
    {

        $val = Session::get('VendorLoggedIn');

        return view('vendors.users.foodaddmenupre');

    }


    public function ambainceaddmenu(Request $request)
    {

        $val = Session::get('VendorLoggedIn');

        return view('vendors.users.ambainceaddmenu');

    }


    // add bartender details view page
    public function addbartenderdetails(Request $request)
    {
        $country_data = Country::all();
        $countries = [];
        foreach ($country_data as $key => $value) {
            $countries[$value->id] =  $value->name ;
        }
        $val = Session::get('VendorLoggedIn');

        return view('vendors.users.addbartenderdetails',compact('countries'));

    }


    // bartendor add video page
    public function addbartendervideos(Request $request, $id='')
    {
        $val = Session::get('VendorLoggedIn');
        return view('vendors.users.addbartendervideos',compact('id'));
    }


    public function bartendercalendarschedule(Request $request, $id='')
    {
        $weekdays = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        $val = Session::get('VendorLoggedIn');
        $user = User::find($id);
        $bartenderAvailability = BartenderAvailability::where('bartender_id',$id)->get();
        return view('vendors.users.bartendercalendarschedule', compact('id','user','bartenderAvailability','weekdays'));
    }


    public function bartenderaddmember(Request $request, $id='')
    {
        $country_data = Country::all();
        $countries = [];
        foreach ($country_data as $key => $value) {
            $countries[$value->id] =  $value->name ;
        }
        $val = Session::get('VendorLoggedIn');
        $user = User::find($id);
        return view('vendors.users.bartenderaddmember', compact('id','user','countries'));
    }


    public function profileCoverUpdate(Request $request)
    {
        $id = $request->user_id;

        $file = $request->file('cover');
        $user = User::find($id);

        // Move Uploaded File.
        if ($request->file('cover')) {
            $name = 'cover_'.time().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/user');
            $file->move($destinationPath,$name);
            $placeImage = '/uploads/user/'.$name;
        }

        $user->cover_image =  isset($placeImage) ? $placeImage : '' ;
        $user->save();
        return back()->with('success', 'Cover Updated.');
    }

    public function profileLogoUpdate(Request $request)
    {
        $id = $request->user_id;
        $file = $request->file('logo');
        $user = User::find($id);

        // Move Uploaded File.
        if ($request->file('logo')) {
            $name = 'logo_'.time().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/user');
            $file->move($destinationPath,$name);
            $placeImage = '/uploads/user/'.$name;
        }

        $user->logo_image =  isset($placeImage) ? $placeImage : null;
        $user->save();
        return redirect()->route('vendor.myprofile')->with('success', 'Logo Updated.');
    }


    public function categoryNotFound(Request $request)
    {
        return view('vendors.users.category-not-found');
    }
    public function userAvailability($availability)
    {
        $id = Auth::user()->id;
        $user = User::find($id);
        $user->availability = $availability;
        $user->save();

        return ['status' => 1,'vendor_id' => $id,'orderItem' => $user,'msg'=>'Availablility changed successfully.'];

    }
    public function notifications()
    {
	    UserNotification::where('user_id',Auth::id())->update(['read_status'=>'1']);
        return view('vendors.users.notifications');

    }



    /////////////////////////////////////////////////////////////////////////////
}
