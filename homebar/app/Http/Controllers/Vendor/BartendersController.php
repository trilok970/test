<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Country;
use DB;
use Illuminate\Support\Facades\Input;
use Session;
use Illuminate\Support\Facades\Validator;
use Config;
use Illuminate\Support\Facades\Auth;
use App\Lib\Uploader;
use Illuminate\Support\Facades\Hash;
use App\Model\DrinkVideo;
use App\Model\BartenderAvailability;
use App\Model\BartenderGroupMembers;
use App\Model\BartendingInformation;
use App\Model\Order;
use Illuminate\Database\Eloquent\Builder;
use App\Model\BartenderLeave;
use Illuminate\Support\Str;
class BartendersController extends Controller {


   public function store(Request $request)
   {
        $val = Session::get('VendorLoggedIn');
        $base_url =  url('/');
        $validatorRules = [
            'first_name'=>'required|max:255|regex:/^[a-zA-Z]+$/u',
            'last_name'=>'required|max:255|regex:/^[a-zA-Z]+$/u',
            'email' => 'required|nullable|email:rfc,dns|max:255|unique:users',
            'primary_mobile_number' => 'required|numeric|digits_between:7,15|unique:users',
            'secondary_mobile_number' => 'required|numeric|digits_between:7,15|unique:users',
            'birthday'=>'required',
            'address'=>'required',
            'dl_no'=>'required',
            'signature_drink'=>'required',
            'bio'=>'required',
            'experience'=>'required|numeric|between:0,99',
            'radius_of_service'=>'required',
            'price'=>'required|numeric',
            'bartending_license_number'=>'required',
            'bartending_license_expirations'=>'required',
            'option'=>'required',
            'password' => 'required|confirmed|min:6',
            'country_id' => 'required|numeric|digits_between:1,10|exists:countries,id',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4048',
            'dl_photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4048',
            'bl_photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4048',
        ];

        try {
            $customMessages = [
                'regex' => 'The :attribute format is invalid. Alphabets only'
            ];

            $validator = Validator::make($request->all(), $validatorRules, $customMessages);

            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                $error = $this->validationHandle($validator->messages());
                return redirect()->back()->withErrors($validator)->withInput();
            } else {
                $status_arr = \Config::get('params.status_arr');
                $country_data = Country::where([['id', $request->country_id]])->first();
                $country_code = isset($country_data->phonecode) ? $country_data->phonecode : '';
                $primary_mobile_number = $country_code.$request->primary_mobile_number;
                $secondary_mobile_number = $country_code.$request->secondary_mobile_number;


                DB::beginTransaction();
                $user = new User();
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->email = $request->email;
                $user->phone = $request->primary_mobile_number;
                $user->phone_country_code = $primary_mobile_number;
                $user->primary_mobile_number = $primary_mobile_number;
                $user->secondary_mobile_number = $secondary_mobile_number;
                $user->dob = $request->birthday;
                $user->address = $request->address;
                $user->latitude = $request->latitude;
                $user->longitude = $request->longitude;
                $user->dl_number = $request->dl_no;
                $user->signature_drink = $request->signature_drink;
                $user->bio = $request->bio;
                $user->experience = $request->experience;
                $user->service_address_radius = $request->radius_of_service;
                $user->price = $request->price;
                $user->price_per_hour = $request->price;
                $user->bartending_license_number = $request->bartending_license_number;
                $user->bartending_license_expirations = $request->bartending_license_expirations;
                $user->options = $request->option;
                $user->country_id = $request->country_id;
                $user->vendor_id = $val['vendorData']->id;
                $user->password = Hash::make($request->password);;
                $user->role = Config::get('params.role_ids.bartender');
                $user->status = $status_arr['active'];

                $file = $request->file('photo');
                $file1 = $request->file('dl_photo');
                $file2 = $request->file('bl_photo');

                // Move Uploaded File.
                if ($request->file('photo')) {
                    $name = 'bartender_'.time().'.'.$file->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/bartender');
                    $file->move($destinationPath,$name);
                    $profileImage = '/uploads/bartender/'.$name;
                }
                // Move Uploaded File.
                if ($request->file('dl_photo')) {
                    $name = 'bartender_dl_'.time().'.'.$file1->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/bartender');
                    $file1->move($destinationPath,$name);
                    $dl_photo = '/uploads/bartender/'.$name;
                }
                // Move Uploaded File.
                if ($request->file('bl_photo')) {
                    $name = 'bartender_bl_'.time().'.'.$file2->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/bartender');
                    $file2->move($destinationPath,$name);
                    $bl_photo = '/uploads/bartender/'.$name;
                }

                $user->profile_image = (isset($profileImage)) ? $profileImage : '';
                $user->dl_photo = (isset($dl_photo)) ? $dl_photo : '' ;
                $user->bl_photo = (isset($bl_photo)) ? $bl_photo : '';
                $user->save();


                $bartending = BartendingInformation::create([
                    'user_id' => $user->id,
                    'social_security_number' => $request->social_security_number,
                    'expiry_date' => $request->bartending_license_expirations,
                    'bartending_license_number' => $request->bartending_license_number,
                    'bartending_license_image' => (isset($bl_photo) ? $bl_photo : ''),
                    'dl_number' => $request->dl_no,
                    'dl_image' => (isset($dl_photo)) ? $dl_photo : '' ,
                ]);

                DB::commit();

                return redirect()->route('vendor.addbartendervideos',$user->id)->with('success', 'Registered successfully.');
            }
        } catch (\Exception $e) {
            return $e->getMessage();
            return redirect()->route('vendor.addbartenderdetails')->with('success', $e->getMessage());
        }
   }

   // for adding bartender video
   public function addbartendervideo(Request $request)
   {
       $bartender_id = $request->bartender_id;
        try {
            $rules = [
                'video.*' => 'required|mimes:mp4,mov,ogg',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                $error = $this->validationHandle($validator->messages());
                return redirect()->back()->withErrors($validator)->withInput();
            } else {

                if (isset($request->video)) {
                    if ((count($request->video)!=0) && ($request->file('video') !== null)) {
                        $files = $request->file('video');
                        if($request->file('video'))
                        {
                            $destinationPath = public_path('/uploads/drinking_videos/');
                            foreach ($files as $file) {
                                $name = 'video_'.$file->getSize().time().'.'.$file->getClientOriginalExtension();
                                $file->move($destinationPath,$name);
                                $drinkVideo = new DrinkVideo;
                                $drinkVideo->user_id = $bartender_id;
                                $drinkVideo->video = '/uploads/drinking_videos/'.$name;
                                $drinkVideo->status = 1;
                                $drinkVideo->save();
                            }
                        }
                        return redirect()->route('vendor.bartendercalendarschedule',$bartender_id)->with('success', 'Video uploaded successfully');
                    }
                } else {
                    return redirect()->route('vendor.addbartendervideos',$bartender_id)->with('danger', 'Please select atleast one video');
                }
            }
        } catch (\Exception $e) {
            return redirect()->route('vendor.addbartendervideos', $bartender_id)->with('success', $e->getMessage());
        }
   }



   public function bartenderlist(Request $request)
   {
        $val = Session::get('VendorLoggedIn');
        $bartenderlist = User::where(['role'=>'4', 'status'=>'1', 'vendor_id' => $val['vendorData']->id])->get();

        return view('vendors.users.bartenderlist', compact('bartenderlist'));
   }


    public function edit($id)
    {
        $user = User::find($id);
        $country_data = Country::all();
        $countries = [];
        foreach ($country_data as $key => $value) {
            $countries[$value->id] =  $value->name ;
        }
        if ($user) {
            return view('vendors.users.editbartenderdetails',compact('user','id','countries'));
        }
    }



    public function update(Request $request, $id)
    {
        $base_url =  url('/');
        $validatorRules = [
            'first_name'=>'required|max:255|regex:/^[a-zA-Z]+$/u',
            'last_name'=>'required|max:255|regex:/^[a-zA-Z]+$/u',
            'email' => 'required|nullable|email:rfc,dns|max:255|unique:users,email,'.$id,
            'primary_mobile_number' => 'required|numeric|digits_between:7,15|unique:users,primary_mobile_number,'.$id,
            'secondary_mobile_number' => 'required|numeric|digits_between:7,15|unique:users,secondary_mobile_number,'.$id,
            'birthday'=>'required',
            'address'=>'required',
            'dl_number'=>'required',
            'signature_drink'=>'required',
            'experience'=>'required|numeric|between:0,99',
            'service_address_radius'=>'required',
            'price'=>'required|numeric',
            'bartending_license_number'=>'required',
            'bartending_license_expirations'=>'required',
            'option'=>'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4048',
            'dl_photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4048',
            'bl_photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4048',
        ];

        try {
            $customMessages = [
                'regex' => 'The :attribute format is invalid. Alphabets only'
            ];

            $validator = Validator::make($request->all(), $validatorRules, $customMessages);

            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                $error = $this->validationHandle($validator->messages());
                return redirect()->back()->withErrors($validator)->withInput();
            } else {
                $status_arr = \Config::get('params.status_arr');
                $country_data = Country::where([['id', $request->country_id]])->first();
                $country_code = isset($country_data->phonecode) ? $country_data->phonecode : '';
                if(Str::startsWith($request->primary_mobile_number, $country_data->phonecode)) {
                    $primary_mobile_number = $request->primary_mobile_number;
                }else {
                    $primary_mobile_number = $country_code.$request->primary_mobile_number;
                }
                if(Str::startsWith($request->secondary_mobile_number, $country_data->phonecode)) {
                    $secondary_mobile_number = $request->secondary_mobile_number;
                }else {
                    $secondary_mobile_number = $country_code.$request->secondary_mobile_number;
                }
                DB::beginTransaction();
                $user = User::find($id);
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->email = $request->email;
                $user->primary_mobile_number = $primary_mobile_number;
                $user->secondary_mobile_number = $secondary_mobile_number;
                $user->dob = $request->birthday;
                $user->address = $request->address;
                $user->latitude = $request->latitude;
                $user->longitude = $request->longitude;
                $user->dl_number = $request->dl_number;
                $user->signature_drink = $request->signature_drink;
                $user->bio = $request->bio;
                $user->experience = $request->experience;
                $user->service_address_radius = $request->service_address_radius;
                $user->price = $request->price;
                $user->price_per_hour = $request->price;
                $user->bartending_license_number = $request->bartending_license_number;
                $user->bartending_license_expirations = $request->bartending_license_expirations;
                $user->options = $request->option;

                $user->role = Config::get('params.role_ids.bartender');
                $user->status = $status_arr['active'];


                $file = $request->file('photo');
                $file1 = $request->file('dl_photo');
                $file2 = $request->file('bl_photo');

                // Move Uploaded File.
                if ($request->file('photo')) {
                    $name = 'bartender_'.time().'.'.$file->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/bartender');
                    $file->move($destinationPath,$name);
                    $profileImage = '/uploads/bartender/'.$name;
                }
                // Move Uploaded File.
                if ($request->file('dl_photo')) {
                    $name = 'bartender_dl_'.time().'.'.$file1->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/bartender');
                    $file1->move($destinationPath,$name);
                    $dl_photo = '/uploads/bartender/'.$name;
                }
                // Move Uploaded File.
                if ($request->file('bl_photo')) {
                    $name = 'bartender_bl_'.time().'.'.$file2->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/bartender');
                    $file2->move($destinationPath,$name);
                    $bl_photo = '/uploads/bartender/'.$name;
                }

                $user->profile_image = (isset($profileImage)) ? $profileImage : $user->profile_image;
                $user->dl_photo = (isset($dl_photo)) ? $dl_photo : $user->dl_photo ;
                $user->bl_photo = (isset($bl_photo)) ? $bl_photo : $user->bl_photo;
                $user->save();

                $bartending = BartendingInformation::where('user_id', $user->id)->firstOrFail();

                $bartending->user_id = $user->id;
                $bartending->social_security_number = $request->social_security_number;
                $bartending->expiry_date = $request->bartending_license_expirations;
                $bartending->bartending_license_number = $request->bartending_license_number;
                $bartending->bartending_license_image = (isset($bl_photo) ? $bl_photo : '');
                $bartending->dl_number = $request->dl_no;
                $bartending->dl_image = (isset($dl_photo)) ? $dl_photo : '' ;

                $bartending->save();
                DB::commit();
                return redirect()->route('vendor.bartender.editvideo',$user->id)->with('success', 'Updated successfully.');
            }
        } catch (\Exception $e) {
            return redirect()->route('vendor.addbartenderdetails')->with('success', $e->getMessage());
        }
    }


    public function editvideo($id)
    {
        // $base_url =  url('/').'/public';
        $videodetails = DrinkVideo::where('user_id', $id)->get();
        return view('vendors.users.editbartendervideos',compact('id'))->with('videodetails', $videodetails);
    }


    public function editbartendervideo(Request $request)
   {
       $bartender_id = $request->bartender_id;
        try {
            $rules = [
                'video.*' => 'required|mimes:mp4,mov,ogg,jpg,png',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                $error = $this->validationHandle($validator->messages());
                return redirect()->back()->withErrors($validator)->withInput();
            } else {
                if($request->file('video'))
                {
                    if ((count($request->video)!=0) && ($request->file('video') !== null)) {
                        // deleting videos
                        $dvideo = DrinkVideo::where('user_id',$bartender_id)->get();
                        foreach ($dvideo as $key => $value) {
                            $path = public_path().$value->video;
                            unlink($path);
                        }

                        $drinkVideo = DrinkVideo::where('user_id',$bartender_id)->delete();

                        // Uploading new videos
                        $files = $request->file('video');

                        if($request->file('video'))
                        {
                            $destinationPath = public_path('/uploads/drinking_videos/');
                            foreach ($files as $file) {

                                $name = 'video_'.$file->getSize().time().'.'.$file->getClientOriginalExtension();
                                $file->move($destinationPath,$name);
                                $drinkVideo = new DrinkVideo;
                                $drinkVideo->user_id = $bartender_id;
                                $drinkVideo->video = '/uploads/drinking_videos/'.$name;
                                $drinkVideo->status = 1;
                                $drinkVideo->save();
                            }
                        }
                        return redirect()->route('vendor.bartendercalendarschedule',$bartender_id)->with('success', 'Action Successfull.');
                    }
                } else {
                    return redirect()->route('vendor.bartendercalendarschedule',$bartender_id)->with('success', 'Action successfull.');
                }
            }
        } catch (\Exception $e) {
            return redirect()->route('vendor.addbartendervideos', $bartender_id)->with('success', $e->getMessage());
        }
   }



    public function destroy($id)
    {
        // echo $id; die;
        $user = User::find($id);
        $user->delete();
        $drinkVideo = DrinkVideo::where('user_id', $id)->first();
        $bartendingInformation = BartendingInformation::where('user_id',$id)->first();
        if($bartendingInformation) {
            $bartendingInformation->delete();
        }
        if($drinkVideo) {
            $drinkVideo->delete();
        }
        return redirect()->route('vendor.bartenderlist')->with('success', 'Bartender removed.');
    }


    // public function updateavailability(Request $request)
    // {
    //    $id = $_POST['bartender_id'];
    //    $val = $_POST['val'];

    //    // Create Listing
    //     $user = User::find($id);
    //     $user->availability = $val;
    //     $user->save();

    //     die;
    // }



    public function bartenderscheduleadd(Request $request)
    {

        $validatorRules = [
            'weekday.0'=>'required',
            'time_from.0' => 'required',
            'time_to.0' => 'required',
            'weekday.1'=>'required',
            'time_from.1' => 'required',
            'time_to.1' => 'required',
            'weekday.2'=>'required',
            'time_from.2' => 'required',
            'time_to.2' => 'required',
            'weekday.3'=>'required',
            'time_from.3' => 'required',
            'time_to.3' => 'required',
            'weekday.4'=>'required',
            'time_from.4' => 'required',
            'time_to.4' => 'required',
            'weekday.5'=>'required',
            'time_from.5' => 'required',
            'time_to.5' => 'required',
            'weekday.6'=>'required',
            'time_from.6' => 'required',
            'time_to.6' => 'required',
        ];

        $validator = Validator::make($request->all(), $validatorRules);

        if ($validator->fails()) {
            Session::flash('error', 'Please fill all weekdays time details correctly');
            $error = $this->validationHandle($validator->messages());
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $session = Session::get('VendorLoggedIn');

        $bartender_id = $request->bartender_id;
        $weekdays = $request->weekday;
        $time_from = $request->time_from;
        $time_to = $request->time_to;
        if(!empty($time_from) && !empty($time_from[0])) {
            BartenderAvailability::where('bartender_id',$bartender_id)->delete();
        }
        foreach ($weekdays as $key => $value) {
            if ($value) {
                $bartenderAvailability = new BartenderAvailability;
                $bartenderAvailability->bartender_id = $bartender_id;
                $bartenderAvailability->vendor_id = $session['vendor_id'];
                $bartenderAvailability->weekday = $value;
                $bartenderAvailability->time_from = date('H:i',strtotime($time_from[$key]));
                $bartenderAvailability->time_to = date('H:i',strtotime($time_to[$key]));
                $bartenderAvailability->save();
            }
        }

        return redirect()->route('vendor.bartendercalendarschedule',$bartender_id)->with('success', 'Schedule added successfully.');
    }



    public function bartendermemberadd(Request $request)
    {
        $session = Session::get('VendorLoggedIn');
        $base_url =  url('/');
        $validatorRules = [
            'name'=>'required|max:255|regex:/^[a-zA-Z ]+$/u',
            'email' => 'required|nullable|email:rfc,dns|max:255|unique:bartender_group_members',
            'country_id' => 'required',
            'mobile_number' => 'required|numeric|digits_between:7,15|unique:bartender_group_members',
            'experience'=>'required|numeric|between:0.1,99',
            'signature_drink'=>'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4048',
        ];

        try {
            $customMessages = [
                'regex' => 'The :attribute format is invalid. Alphabets only.'
            ];

            $validator = Validator::make($request->all(), $validatorRules, $customMessages);

            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                $error = $this->validationHandle($validator->messages());
                return redirect()->back()->withErrors($validator)->withInput();
            } else {
                $status_arr = \Config::get('params.status_arr');
                $country_data = Country::where([['id', $request->country_id]])->first();
                $country_code = isset($country_data->phonecode) ? $country_data->phonecode : '';
                $mobile_number = $country_code.$request->mobile_number;


                $bartenderGroupMembers = new BartenderGroupMembers;
                $bartenderGroupMembers->name = $request->name;
                $bartenderGroupMembers->email = $request->email;
                $bartenderGroupMembers->mobile_number = $mobile_number;
                $bartenderGroupMembers->experience = $request->experience;
                $bartenderGroupMembers->signature_drink = $request->signature_drink;
                $bartenderGroupMembers->vendor_id = $session['vendor_id'];
                $bartenderGroupMembers->country_id = $request->country_id;
                $bartenderGroupMembers->bartender_id = $request->bartender_id;

                $file = $request->file('photo');

                // Move Uploaded File.
                if ($request->file('photo')) {
                    $name = 'bartender_'.time().'.'.$file->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/bartender');
                    $file->move($destinationPath,$name);
                    $photo = '/uploads/bartender/'.$name;
                }

                $bartenderGroupMembers->photo = (isset($photo)) ? $photo : '';

                $bartenderGroupMembers->save();
                return redirect()->route('vendor.groupmemberlist',$request->bartender_id)->with('success', 'Registered successfully.');
            }
        } catch (\Exception $e) {
            echo $e->getMessage(); die;
            return redirect()->route('vendor.addbartenderdetails')->with('success', $e->getMessage());
        }
    }



    public function groupmemberlist(Request $request,$id='')
    {

        $val = Session::get('VendorLoggedIn');
        $vendor_id = $val['vendorData']->id;
        $groupmembers = BartenderGroupMembers::where('bartender_id', $id)->orderBy('id','desc')->get();
        return view('vendors.users.groupmemberlist', compact('id','groupmembers'));
    }



    public function groupmemberedit($id,$memberId)
    {

        $country_data = Country::all();
        $countries = [];
        foreach ($country_data as $key => $value) {
            $countries[$value->id] =  $value->name ;
        }
        $user = BartenderGroupMembers::find($memberId);

        if ($user) {
            return view('vendors.users.editgroupmember',compact('countries','id'))->with('user', $user);
        }
    }



    public function groupmemberupdate(Request $request, $id='')
    {
        $base_url =  url('/');
        $validatorRules = [
            'name'=>'required|max:255|regex:/^[a-zA-Z ]+$/u',
            'country_id' => 'required',
            'email' => 'required|nullable|email:rfc,dns|max:255|unique:bartender_group_members,email,'.$id,
            'mobile_number' => 'required|numeric|digits_between:7,15|unique:bartender_group_members,mobile_number,'.$id,
            'experience'=>'required|numeric|between:0.1,99',
            'signature_drink'=>'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4048',
        ];

        try {
            $customMessages = [
                'regex' => 'The :attribute format is invalid. Alphabets only.'
            ];

            $validator = Validator::make($request->all(), $validatorRules, $customMessages);

            if ($validator->fails()) {
                Session::flash('error', 'Please correct the errors below and try again');
                $error = $this->validationHandle($validator->messages());
                return redirect()->back()->withErrors($validator)->withInput();
            } else {
                $status_arr = \Config::get('params.status_arr');
                $country_data = Country::where([['id', $request->country_id]])->first();
                $country_code = isset($country_data->phonecode) ? $country_data->phonecode : '';
                $mobile_number = $country_code.$request->mobile_number;

                $bartenderGroupMembers = BartenderGroupMembers::find($id);
                $bartenderGroupMembers->name = $request->name;
                $bartenderGroupMembers->email = $request->email;
                $bartenderGroupMembers->country_id = $request->country_id;
                $bartenderGroupMembers->mobile_number = $mobile_number;
                $bartenderGroupMembers->experience = $request->experience;
                $bartenderGroupMembers->signature_drink = $request->signature_drink;

                $file = $request->file('photo');

                // Move Uploaded File.
                if ($request->file('photo')) {
                    $name = 'bartender_'.time().'.'.$file->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/bartender');
                    $file->move($destinationPath,$name);
                    $photo = '/uploads/bartender/'.$name;
                }


                $bartenderGroupMembers->photo = (isset($photo)) ? $photo : $bartenderGroupMembers->photo;
                $bartenderGroupMembers->save();

                return redirect()->route('vendor.groupmemberlist',$bartenderGroupMembers->bartender_id)->with('success', 'Updated successfully.');
            }
        } catch (\Exception $e) {
            return redirect()->route('vendor.groupmemberlist')->with('success', $e->getMessage());
        }
    }



    public function groupmemberdestroy($id='')
    {
        $bartenderGroupMembers = BartenderGroupMembers::find($id);
        $bartenderId = $bartenderGroupMembers->bartender_id;
        $bartenderGroupMembers->delete();
        return redirect()->route('vendor.groupmemberlist',$bartenderId)->with('success', 'Member removed.');
    }


    public function bookings($id)
    {
        $orders =  Order::with('orderItems','user')->whereHas('orderItems',function(Builder $query) use ($id) {
            $query->where(['status' => 'NEW','bartender_id' => $id]);
        })->orderBy('created_at','desc')->get();

        $events = [];
        foreach($orders as $order) {
            $event = [
                'title' => $order->order_number,
                'start' => $order->orderItems?$order->orderItems->first()->book_date:'',
            ];
            array_push($events,$event);
        }
        return $events;
    }

    public function leaves($id) {
        $leaves = BartenderLeave::where('bartender_id',$id)->get();

        $events = [];
        foreach($leaves as $leave) {
            $event = [
                'title' => 'Leave',
                'start' => $leave->date
            ];

            array_push($events,$event);
        }
        return $events;
    }

    public function toggleLeave($id) {
        try {

            $leave = BartenderLeave::where(['bartender_id' => $id,'date' => request()->date])->first();
            if($leave) {
                BartenderLeave::where(['bartender_id' => $id,'date' => request()->date])->delete();
            }else{
                $leave = BartenderLeave::create([
                    'bartender_id' => $id,
                    'date' => request()->date,
                ]);
            }
            return ['result' => 1];
        }catch(Exception $e) {
            return ['result' => 0, 'error' => $e->getMessage()];
        }

    }


    public function updateUnavailability($id)
    {
        $bartenderId = $id;
        if(request()->unavailability_type     == 1) {
            $startDate = request()->unavailability_start_date;
            $endDate = request()->unavailability_end_date;

            $startTimeStamp = strtotime($startDate);
            $endTimeStamp = strtotime($endDate);

            DB::beginTransaction();
            while($startTimeStamp <= $endTimeStamp) {
                $d = date('Y-m-d',$startTimeStamp);
                $startTimeStamp = strtotime("+1 day",$startTimeStamp);

                $leave = BartenderLeave::where(['bartender_id' => $id,'date' => $d])->first();
                if($leave) {
                    BartenderLeave::where(['bartender_id' => $id,'date' => $d])->delete();
                }
                $leave = BartenderLeave::create([
                    'bartender_id' => $id,
                    'date' => $d,
                ]);
            }
            request()->session()->flash('success','Dates are marked as leave');
            DB::commit();

            return redirect()->route('vendor.bartendercalendarschedule',$id);
        }else {
            $user = User::where('id',$id)->firstOrFail();
            $user->availability = 0;
            $user->save();
            request()->session()->flash('success','Your availability is turned off');
            return redirect()->route('vendor.bartendercalendarschedule',$id);
        }
    }

    public function updateAvailability($id) {
        try {
            $user = User::where('id',$id)->firstOrFail();
            $user->availability = 1;
            $user->save();
            request()->session()->flash('success','Your availability is turned on');
            return ['result' => 1];
        } catch (Exception $e) {
            return ['result' => 0, 'error' => $e->getMessage()];
        }
    }
}
