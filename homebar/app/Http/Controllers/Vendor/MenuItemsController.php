<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use Config;
use DB;
use view;
use App\Lib\Uploader;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\MenuItem;
use App\Model\Brand;
use App\Model\Attribute;
use App\Model\MenuItemAttribute;
use App\Model\Type;
use App\Model\MenuItemImage;

class MenuItemsController extends Controller
{
    public function create($type)
    {
        try {
            $type_id = Type::where('slug', $type)->pluck('id')->first();
            $categories = Category::getCategory($type_id);
            $sub_categories = [];
            $brands = [];
            $entity = new MenuItem();
            $title_page = 'Add Menu Item';
            $i = 1;
            return view('vendors/menu_items/create', compact('title_page', 'type_id', 'type', 'categories', 'sub_categories', 'brands', 'entity', 'i'));
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('error', $msg);
            return redirect()->back();
        }
    }

    public function addVariation(Request $request)
    {
        $category_id = $request->category_id;
        $i = $request->i;

        $variation_form = (string)View::make('vendors.menu_items.create_form', compact('i'));
        $data = [
            'variation_view'=> $variation_form
        ];
        return json_encode($data);
    }

    public function getSubcategoriesBrandsAndAttributes(Request $request)
    {
        $category_id = $request->category_id;
        $key_i = $request->i;
        $sub_categories = Category::getSubCategory($category_id);

        $brands = Brand::getBrandsByCategory($category_id);
        $attributes = [];

        $data = [
            'sub_categories'=>$sub_categories,
            'brands'=>$brands
        ];
        return json_encode($data);
    }

    public function store(Request $request)
    {
        $user_id = Auth::id();
        $total_item = $request->total_var;
        $rules = [
            'type_id'=>'required|exists:types,id',
            'category_id'=>'required|exists:categories,id',
            'subcategory_id'=>'required|exists:categories,id',
            'brand_id'=>'required|exists:brands,id',
            'name'=>'required',
            'description'=>'required',
        ];
        $niceNames = [
            'type_id' => 'Type',
            'category_id' => 'Category',
            'subcategory_id' => 'Sub Category',
            'brand_id' => 'Brand'
        ];
        $category_id = $request->category_id;


        for($i=1; $i <= $total_item; $i++) {
            $rules['price_'.$i] = 'required';
            $rules['size_'.$i] = 'required';

            $niceNames['price_'.$i] = 'Price';
            $niceNames['size_'.$i] = 'Size';
        }
        $data = $request->all();

        $validator = Validator::make($data, $rules);
        $validator->setAttributeNames($niceNames);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response = [
                'status'=>
                false,
                'message'=>'Please correct the errors below and try again',
                'data'=> $errors
            ];
            return response()->json($response);
        }
        $menu_items = [];
        $menu_attribute = [];
        $curr_date = date('Y-m-d H:i:s');
        $images = $request->file('images');
        $item_images = [];
        if ($request->file('images')) {
            foreach($images as $key => $image){
                $destinationPath = '/uploads/menu_items/';
                $response_data = Uploader::doUpload($image, $destinationPath);
                if ($response_data['status'] == true) {
                    $item_images[] = $response_data['file'];
                }
            }
        }
        $curr_date_time = date('Y-m-d H:i:s');
        $menu_item_id_parent = null;
        for ($i=1; $i <= $total_item; $i++) {
            $item_id = null;
            if(!empty($data['id_'.$i])) {
                $item_id = $data['id_'.$i];
                $menu_item_entity = MenuItem::where(['id'=>$item_id])->first();
            } else {
                $menu_item_entity = new MenuItem();
            }
            $menu_item_entity->type_id = $data['type_id'];
            $menu_item_entity->category_id = $data['category_id'];
            $menu_item_entity->subcategory_id = $data['subcategory_id'];
            $menu_item_entity->parent_id = $menu_item_id_parent;
            $menu_item_entity->brand_id = $data['brand_id'];
            $menu_item_entity->name = isset($data['name']) ? $data['name'] : null;
            $menu_item_entity->description = isset($data['description']) ? $data['description'] : null;

            $menu_item_entity->price = isset($data['price_'.$i]) ? $data['price_'.$i] : null;
            $menu_item_entity->size = isset($data['size_'.$i]) ? $data['size_'.$i] : null;
            $menu_item_entity->user_id = $user_id;
            $menu_item_entity->in_stock = $data['in_stock'] ?? 0;
            $menu_item_entity->is_alcoholic = $data['is_alcoholic'] ?? 0;
            $menu_item_entity->save();
            $menu_item_id = $menu_item_entity->id;
            if(!$menu_item_id_parent){
                $menu_item_id_parent = $menu_item_id;
            }
            $image_data = [];
            foreach($item_images as $key_image => $image_name) {
                $image_data[] = [
                    'menu_item_id'=>$menu_item_id,
                    'image'=>$image_name,
                    'created_at'=>$curr_date_time,
                    'updated_at'=>$curr_date_time,
                ];
            }
            if(!empty($image_data)){
                MenuItemImage::insert($image_data);
            }

        }
        $message = 'Menu item has been saved successfully.';
        Session::flash('success', 'Menu item has been saved successfully.');
        $response = [
            'status'=> true,
            'message'=>$message,
            'data'=> []
        ];
        return response()->json($response);

    }

    public function list($type=null) {
            $drink_slug = Config::get('params.drink_slug');
            if(!$type){
                return redirect()->route('vendor.menuItem.list', $drink_slug);
            }
            $user_id = Auth::id();
            $type_row = Type::getTypeBySlug($type);
            if(empty($type_row->id)) {
                abort(404);
            }
            $type_id = $type_row->id;
            $items = MenuItem::with(['menuItemImages'])
            ->where(['type_id'=>$type_id, 'user_id'=> $user_id])
            ->whereNull('parent_id')
            ->get();


            $title_page = $type_row->name;
            return view('vendors/menu_items/list', compact('title_page', 'items', 'type'));


    }

    public function delete($id){
        $user_id = Auth::id();
        MenuItem::where(['id'=>$id, 'user_id'=> $user_id])
            ->delete();
        Session::flash('success', 'Menu Item has been deleted successfully.');
        return redirect()->back();
    }

    public function edit($type, $id) {
        try {
            $entity = MenuItem::with(['menuItemImages', 'getMenuItemAttributes','getChilrensMenuItems.getMenuItemAttributes'])
            ->where(['menu_items.id'=> $id])
            ->first();
            $type_id = Type::where('slug', $type)->pluck('id')->first();
            if(empty($entity->id) || empty($type_id)){
                abort(404);
            }
            $category_id = $entity->category_id;
            $categories = Category::getCategory($type_id);
            $sub_categories = Category::getSubCategory($category_id);
            $brands = Brand::getBrandsByCategory($category_id);
            $attributes = [];
            if ($category_id) {
                $attributes = getAttributes($category_id);
            }
            $title_page = 'Edit Menu Item';

            $childrens = $entity->getChilrensMenuItems;
            $children_count = count($childrens);
            $i = 1;
            $entity['id_1'] = $entity->id;
            $entity['price_1'] = $entity->price;
            $entity['size_1'] = $entity->size;

            if($children_count){
                foreach($childrens as $key => $child_row){
                    $count_key = $key + 2;
                    $i = $count_key;
                    $entity['id_'.$count_key] = $child_row->id;
                    $entity['price_'.$count_key] = $child_row->price;
                    $entity['size_'.$count_key] = $child_row->size;
                }
            }
            $i = $children_count + 1;
            return view('vendors/menu_items/create', compact('title_page', 'type_id', 'type', 'categories', 'sub_categories', 'brands', 'entity', 'i'));
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('error', $msg);
            return redirect()->back();
        }
    }

    public function deleteImage(Request $request){
        $image_id = $request->image_id;
        $image_row = MenuItemImage::where(['id'=>$image_id])
        ->first();
        if(!empty($image_row->id)) {
            MenuItemImage::where(['image'=>$image_row->image])
            ->delete();
        }
        return response()->json(['status'=>true]);
    }

}
