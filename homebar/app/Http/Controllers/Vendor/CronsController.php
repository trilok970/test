<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\UserLocation;
use App\Model\OrderItem;
use App\Model\User;
use App\Model\EmailTemplate;
class CronsController extends Controller
{
    public function assginDriver()
    {

        $url = env('NODE_URL').'/assign-driver';

        $from = date('Y-m-d H:i:s');
        $to = date("Y-m-d H:i:s", strtotime("+30 minutes", strtotime($from)));
        $driver_ids = UserLocation::whereHas('user', function($query){
            $query->where(['status'=>'1']);
        })
        ->pluck('user_id')
        ->toArray(); 
        
        
        if(!empty($driver_ids)) {
            $driver_ids_str = implode(',', $driver_ids);
            /*
            $order_items = OrderItem::whereIn('status', ['NEW'])
                ->where(['is_assigned'=>'0'])
                ->whereHas('order', function($query) use($from, $to) {
                    $query->where('delivery_date', '<=', $to)
                    ->where(['reach_type'=>'DELIVERY']);
                })
                ->groupBy('order_id')
                ->groupBy('restaurant_id')
                ->get();
            */
            $order_items = OrderItem::whereIn('status', ['ACCEPTED','IN KITCHEN','READY TO PICKUP',])
            ->where(['is_assigned'=>'0'])
            ->whereHas('order', function($query) use($from, $to) {
                $query->where(['reach_type'=>'DELIVERY']);
            })
            ->groupBy('order_id')
            ->groupBy('restaurant_id')
            ->get();
            foreach($order_items as $key => $row) {
                /*
                OrderItem::whereIn('status', ['NEW'])
                ->where(['is_assigned'=>'0'])
                ->whereHas('order', function($query) use($from, $to) {
                    $query->where('delivery_date', '<=', $to)
                    ->where(['reach_type'=>'DELIVERY']);
                })
                ->update(['is_assigned'=> '1', 'requested_driver_ids'=>$driver_ids_str]);
                */
                OrderItem::whereIn('status', ['ACCEPTED','IN KITCHEN','READY TO PICKUP',])
                ->where(['order_id'=>$row->order_id])
                ->where(['restaurant_id'=>$row->restaurant_id])
                ->where(['is_assigned'=>'0'])
                ->whereHas('order', function($query) use($from, $to) {
                    $query->where(['reach_type'=>'DELIVERY']);
                })
                ->update(['is_assigned'=> '1', 'requested_driver_ids'=>$driver_ids_str]);
                $data = [
                    'order_item_id'=>$row->id,
                    'driver_ids'=>$driver_ids
                ];

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $server_output = curl_exec($ch);
                curl_close ($ch);

            }
        }

        die('complete.');

    }

    public function reminderLicenseExpiry(){
        $exp_date = date('Y-m-d', strtotime("+1 day"));
        $users = User::select(['id', 'first_name', 'last_name','email', 'bartending_license_expirations'])
        ->where('bartending_license_expirations', $exp_date)
        ->whereNotNull('email')
        ->where('email','!=', '')
        ->get();
        if(!empty($users)) {
            $template = EmailTemplate::getTemplate('send-reminder-email-to update-license-expiry');
            foreach($users as $key => $row){
                $site_name = getSettings()['site_title'];
                $subject = $template->subject;
                $description = $template->description;
                $full_name = $row->first_name.' '.$row->last_name;

                $subject = str_replace(
                    ['{FULL_NAME}', '{EMAIL}', '{SITE}', '{EXPIRY_DATE}'],
                    [$full_name, $row->email, $site_name, $row->bartending_license_expirations],
                    $subject
                );

                $description = str_replace(
                    ['{FULL_NAME}', '{EMAIL}', '{SITE}', '{EXPIRY_DATE}'],
                    [$full_name, $row->email, $site_name, $row->bartending_license_expirations],
                    $description
                );

                $mail_data = [
                    'email' => $row->email,
                    'subject' => $subject,
                    'content' => $description
                ];
                mailSend($mail_data);
            }
        }
        die('complete.');

    }

}

