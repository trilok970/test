<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Session;
use Illuminate\Support\Facades\Validator;

class AdminLoginController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }
    
    public function showLoginForm() {
        if(Auth::guard('admin')->check()){
            return redirect()->route('admin.dashboard');
        }
        $title_page = 'Login';
        return view('auth.admin_login', compact('title_page'));
    }
    
    
    
    public function login(Request $request) {
        $rules = [
            'email'   => 'required|email',
            'password' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            Session::flash('error', 'Please correct the errors below and try again');
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }
        $role_ids = \Config::get('params.role_ids');
        
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password, 'role'=>$role_ids['admin']], $request->get('remember'))) {
            return redirect()->intended(route('admin.dashboard'));
        } else {
            Session::flash('error', 'Invalid username or password.');
        }
        return back()->withInput($request->only('email', 'remember'));
    }
    
    public function logout() {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }
    
    
}
