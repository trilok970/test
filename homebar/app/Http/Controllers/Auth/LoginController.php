<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Session;
use Illuminate\Support\Facades\Validator;
use App\Model\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('guest')->except('logout');
    }

    public function showLoginForm() {
        if(Auth::guard('vendor')->check()){
            return redirect()->route('vendor.dashboard');
        }
        return view('auth.login', ['url' => 'vendor']);
    }



    public function login(Request $request) {
        $email = $request->email;
        // print_r($request->all()); die;
        $rules = [
            'email'   => 'required|email',
            'password' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            Session::flash('error', 'Please correct the errors below and try again');
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }
        $role_ids = \Config::get('params.role_ids');
        // print_r($role_ids); die;
        if (Auth::guard('vendor')->attempt(['email' => $request->email, 'password' => $request->password, 'status' => '1', 'role'=>$role_ids['vendor']], $request->get('remember'))) {

            $matchThese = ['email' => $email, 'role' => '3', 'status' => '1'];
            $user = User::where($matchThese)->first();


                Session::put('VendorLoggedIn', ['vendor_id' => $user->id, 'vendorData' => $user]);
                Session::save();
                // return redirect()->route('vendor.orders', 'drinks');
                return redirect()->route('vendor.orders', 'all');



            // return redirect()->intended(route('vendor.dashboard'));
        } else {
            Session::flash('error', 'Invalid username or password or your account is not activated check email for verification.');
        }
        return back()->withInput($request->only('email', 'remember'));
    }


    public function logout() {
        Session::flush();
        Auth::guard('vendor')->logout();
        return redirect()->route('vendor.login');
    }


}
