<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\Model\User;
use Session;
use Config;
use DB;
use App\Lib\Uploader;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Lib\StripePayment;



class UsersController extends Controller {


/*    public function accountConfirmation(Request $request) {
        $data = $request->all();
        
        $users = User::where('forgot_token',$data['token'])->first();
        if ($users) {
            $users->status = 1;
            $users->forgot_token = null;
            $users->save();

            $msg = ['message_success' => 'Account activated. You can login now.'];

            return view('front.users.register_status',compact('msg'));
        } else {
            $msg = ['message_error' => 'Link Expired.'];
            return view('front.users.register_status',compact('msg'));
        }

        return view('front.users.register_status');
    }*/

/*    public function emailverify(Request $request) {
        $data = $request->all();
        
        $place = Place::where('token',$data['token'])->first();
        if ($place) {
            $place->email_verify = 1;
            $place->token = null;
            $place->save();

            $msg = ['message_success' => 'Email verified.'];

            return view('front.users.place_register_email_verify',compact('msg'));
        } else {
            $msg = ['message_error' => 'Link Expired.'];
            return view('front.users.register_status',compact('msg'));
        }

        return view('front.users.register_status');
    }*/


 /*   public function resetPassword(Request $request) {
        $data = $request->all();
        $users = User::where('forgot_token',$data['token'])->first();
        if (empty($users)) {
            return view('front/users/linkexpire');
        }
        return view('front/users/reset_password');
    }*/
    


    /*public function updateResetPassword(Request $request) {
        
        $validator = Validator::make($request->all(),[
            'new_password' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            // If validation fails. if want json response copy it from other functions.
            $response['status'] = false; 
            $response['message'] = $this->validationHandle($validator->messages());  
            $response['data'] = []; 
            
            $msg = ['status' => false, 'message_error' => $response['message']];
            return view('front.users.reset_password',compact('msg')); 
        } else {
            $data = $request->all();
            // $user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
            
            $users = User::where('forgot_token',$data['token'])->first();
            // $users = 1;
            if ($users) {
                // check if new password is same as the Previous password.
                if (Hash::check($data['new_password'], $users->password)) {
                    $msg = ['message_error' => 'New Password is already in use before. Please enter a fresh password.'];
                    return view('front.users.reset_password',compact('msg'));
                }
        
                $users->password = Hash::make($request->input('new_password'));
                $users->forgot_token = null;
                $users->save();

                $msg = ['status' => true, 'message_success' => 'Password updated successfully.'];
                return view('front.users.reset_password',compact('msg'));

            } else {
                $msg = ['status' => true, 'message_error' => 'Link Expired.'];
                return view('front.users.reset_password',compact('msg'));

            }

        }

        ###############

    }*/


    public function forgotPassword(Request $request) {
        // echo $request->token; die();
        $token =  $request->token;
        $base_url =  url('/');
        $data = $request->all(); 
        // $title_page = 'Login';
        if ($request->isMethod('post')) {
            // print_r($request->all());
            // die('post');
            
            try {

                $validation = array(
                    'password' => 'required|confirmed|min:6',
                );

                $validator = Validator::make($request->all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
                } else{
                    
                    $users = User::where('token', $token)->first();

                    if ($users) {
                        
                        $password = Hash::make($request->input('password'));    // creating password hash / encryption.
                        $users->password = $password;
                        $users->token = null;
                        $users->save();

                        Session::flash('success', 'Password updated successfully.');
                        return redirect()->back()->withInput();
                    } else {
                        Session::flash('danger', 'Link Expired.');
                        return redirect()->back()->withInput();
                    }
                }

             
            } catch (\Exception $e) {
                $msg = $e->getMessage();
                Session::flash('danger', $msg);
                return redirect()->back()->withInput();
            }
        }

        return view('front/users/forgot_password');
        // return view('front/users/forgot_password',compact('users'));
    }
    
  
    public function verify(Request $request, $token) {
        $status_arr = Config::get('params.status_arr');
        
        $user_data = User::where('token', $token)->first();
        // if(empty($user_data)){
        //     Session::flash('error', 'The request page was not found');
        //     return redirect()->route('vendor.login');
        // } else if($user_data->status == $status_arr['inactive']) {
        //     Session::flash('error', 'Your account is not active.');
        //     return redirect()->route('vendor.login');
        // }
        if ($user_data) {
            $user_data->status = "1";
            
            $user_data->token = null;
            $user_data->save();
            $msg = ['message_success' => 'Account activated. You can login now.'];
            // return view('front.users.register_status',compact('msg'));
            return redirect()->route('vendor.login')->with('success', 'Account activated successfully. You can login now.');
        } else {
            $msg = ['message_error' => 'Link Expired.'];
            return view('front.users.register_status',compact('msg'));
            // return redirect()->route('vendor.login')->with('success', 'Registered successfully.');
        }
        
        return redirect()->route('vendor.login')->with('success', 'Registered successfully.');
        // return view('front.users.register_status');
       
    }

    public function stripeRedirect(Request $request){
        $success_flag = 0;
        $message = 'Some error occurred in creating account.';
        $data = $request->all();
        if(!empty($request->code)){
            $code = $request->code;
            $stripe_secret_key = env('STRIPE_SECRET_KEY');
            
            $post = [
                'client_secret' => $stripe_secret_key,
                'code' => $code,
                'grant_type'   => 'authorization_code',
            ];
            
            
            $ch = curl_init('https://connect.stripe.com/oauth/token');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            $response = curl_exec($ch);
            curl_close($ch);
            
            if(!empty($response)){
                $response_obj = json_decode($response);
                if(!empty($response_obj->stripe_user_id)){
                    $stripePay = new StripePayment();
                    $account = $stripePay->retrieveAccount($response_obj->stripe_user_id);
                    
                    if(!empty($account->email)){
                        $account_email = $account->email;
                        $user = User::where([['email', $account_email], ['stripe_connect_id', null]])->first();
                        if(!empty($user)){
                            $user->stripe_connect_id = $response_obj->stripe_user_id;
                            $user->save();
                            $success_flag = 1;
                            $message = 'Your account has been updated successfully.';
                        }
                    }
                }
            }
        }
        if($success_flag){
            return view('front.users.thankyou',  compact('message'));
        }
        else{
            return view('front.users.fail',  compact('message'));
        }
    }

	 public function venderStripeRedirect(Request $request){
        $success_flag = 0;
        $message = 'Some error occurred in creating account.';
        $data = $request->all();
       if(!empty($request->code)){
            $code = $request->code;
            $stripe_secret_key = env('STRIPE_SECRET_KEY');
            
            $post = [
                'client_secret' => $stripe_secret_key,
                'code' => $code,
                'grant_type'   => 'authorization_code',
            ];
            
            
            $ch = curl_init('https://connect.stripe.com/oauth/token');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            $response = curl_exec($ch);
            curl_close($ch);
			
            if(!empty($response)){
                $response_obj = json_decode($response);
                if(!empty($response_obj->stripe_user_id)){
                    $stripePay = new StripePayment();
                    $account = $stripePay->retrieveAccount($response_obj->stripe_user_id);
                    
                    if(!empty($account->email)){
                        $account_email = $account->email;
                        $user = User::where([['email', $account_email], ['stripe_connect_id', null]])->first();
                        if(!empty($user)){
                            $user->stripe_connect_id = $response_obj->stripe_user_id;
                            $user->save();
                            $success_flag = 1;
                            $message = 'Your account has been updated successfully.';
                        }
                    }
                }
            }
        }  
        if($success_flag){
			return redirect("vendor/bartender-my-earning")->with('success_msg', 'Your account has been updated successfully.')->send();
            //return view('vendors.users.bartendermyearning',  compact('message'));
        }
        else{
			return redirect("vendor/bartender-my-earning")->with('error_msg', 'Something went wrong. Try again.')->send();
           // return view('vendors.users.bartendermyearning',  compact('message'));
        }
    }

    
}
