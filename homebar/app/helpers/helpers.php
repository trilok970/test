<?php
use App\Model\UserDevice;
use App\Model\User;
use App\Model\EmailTemplate;
use App\Model\Attribute;
use App\Model\UserNotification;

//use DB;

if (!function_exists('mailSend')) {
    function mailSend($data)
    {
        try {
            $site_title = Config('params.site_title');

            $from_email = Config::get('params.mail_username');
            $subject = !empty($data['subject']) ? $data['subject'] : '';
            if (is_array($data['email']) && count($data['email']) > 1) {
                foreach ($data['email'] as $email) {
                    Mail::send('emails.all_mail', ['data' => $data], function ($message) use ($data, $from_email, $subject, $site_title) {
                        $message->from($from_email, $site_title);
                        $message->to($email)
                                ->subject($subject);
                    });
                }
            } else {
                Mail::send('emails.all_mail', ['data' => $data], function ($message) use ($data, $from_email, $subject, $site_title) {
                    $message->from($from_email, $site_title);
                    $message->to($data['email'])
                            ->subject($subject);
                });
            }
            return true;
        } catch (Exception $ex) {
            dd($ex);
            return false;
        }
    }
}

if (!function_exists('getSettings')) {
    function getSettings()
    {
        return \App\Model\Setting::pluck('value', 'slug')->toArray();
    }
}

if (!function_exists('pr')) {
    function pr($data)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        die;
    }
}

if (!function_exists('randomToken')) {
    function randomToken()
    {
        $token = time() . rand(1111, 999999);
        $token = base64_encode($token);
        return $token;
    }
}

if (!function_exists('manageDevices')) {
    function manageDevices($user_id, $device_id, $device_type, $methodName)
    {
        if ($methodName == 'add') {
            UserDevice::updateOrCreate(
                ['user_id' => $user_id, 'device_id' => $device_id, 'device_type' => $device_type]
            );
        } elseif ($methodName == 'delete') {
            UserDevice::where('user_id', $user_id)
                    ->where('device_id', $device_id)
                    ->where('device_type', $device_type)
                    ->delete();
        }
        return true;
    }
}

if (!function_exists('listDateFromat')) {
    function listDateFromat($date)
    {
        return date('m-d-Y  ', strtotime($date));
    }// end function.
}
if (!function_exists('listTimeFromat')) {
    function listTimeFromat($date)
    {
        return date('h:i A', strtotime($date));
    }// end function.
}
if (!function_exists('buttonHtml')) {
    function buttonHtml($key, $link, $title='')
    {
        $array = [
            "edit" => "<span class='f-left margin-r-5'><a data-toggle='tooltip'  class='btn btn-primary btn-xs' title='Edit' href='" . $link . "'><i class='fa fas fa-edit ' aria-hidden='true'></i></a></span>",
            "view" => "<span class='f-left margin-r-5'><a data-toggle='tooltip'  class='btn btn-primary btn-xs' title='".$title."' href='" . $link . "' ><i class='fa fas fa-eye' aria-hidden='true'></i></a></span>",

            "delete" =>"<span class='f-left margin-r-5'><form style='display: inline-block;' title='Trash' action='$link' method='POST' onsubmit=' return confirmDelete(event,this);'><input type='hidden' name='_method' value='DELETE'><input type='hidden' name='_token' value='".csrf_token()."'><button type='submit' class='btn btn-danger btn-xs'><i class='fa fas fa-trash' aria-hidden='true'></i></button></form></span>",
        ];

        if (isset($array[$key])) {
            return $array[$key];
        }
        return '';
    }
}

/*
 * * Button With Html
 */
if (!function_exists('getButtons')) {
    function getButtons($array = [])
    {
        $html = '';
        foreach ($array as $arr) {
            $title = isset($arr['title']) ? $arr['title'] : '';
            $html .= buttonHtml($arr['key'], $arr['link'], $title);
        }
        return $html;
    }
}

if (!function_exists('getStatus')) {
    function getStatus($current_status, $id)
    {
        $html = '';
        switch ($current_status) {


            case '1':
                $html = '<span class="f-left margin-r-5" id = "status_' . $id . '"><a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeStatus(' . $id . ','.$current_status.')" >Active</a></span>';
                break;
            case '0':
                $html = '<span class="f-left margin-r-5" id = "status_' . $id . '"><a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Inactives" onClick="changeStatus(' . $id . ','.$current_status.')" >Inactive</a></span>';
                break;

            default:

                break;
        }

        return $html;
    }
}

if (!function_exists('getFields')) {
    function getFields($fields, $entity)
    {
        $view = '';

        foreach ($fields as $field_name => $fields_param) {
            $fields_param['name'] = $field_name;
            $view .= (string)View::make('admin.includes.text_half', [
                'entity'=>$entity,
                'fields_param'=> $fields_param
                ]);
        }

        return $view;
    }
}

if (!function_exists('getListImage')) {
    function getListImage($image_path)
    {
        $html ='';
        if (!empty($image_path)) {
            $html = '<img src="'.$image_path.'" width=50 />';
        }
        return $html;
    }
}

if (!function_exists('getTemplate')) {
    function getTemplate($slug)
    {
        $template = EmailTemplate::where(['slug'=>$slug])->first();
        return $template;
    }
}

if (!function_exists('getAttributes')) {
    function getAttributes($category_id)
    {
        $attributes = Attribute::whereHas('getAttributeCategories', function ($query) use ($category_id) {
            $query->where('category_id', $category_id);
        })
        ->pluck('name', 'id')
        ->toArray();
        return $attributes;
    }
}
if (! function_exists('strShort')) {
    function strShort($text, $limit=30)
    {
        $text_ret = '';
        if ($text) {
            if (strlen($text) >= $limit) {
                $text_ret = substr($text, 0, $limit).'...';
            } else {
                $text_ret = $text;
            }
        }
        return $text_ret;
    }
}
if (! function_exists('distance')) {
    function distance($lat1, $lon1, $lat2, $lon2, $unit='K')
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return round(($miles * 1.609344), 1);
        } elseif ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}
if (! function_exists('phoneNumberFormat')) {
    function phoneNumberFormat($number) {
    // Allow only Digits, remove all other characters.
    $number = preg_replace("/[^\d]/","",$number);
    // get number length.
    $length = strlen($number);
    // if number = 10
    // if($length == 10) {
    //     $number = preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "($1)-$2-$3", $number);
    // }else if($length == 11) {
    //     $number = substr($number, 0, 1) . '-' . substr($number, 1, 3) . '-' . substr($number, 4, 3) . '-' . substr($number, 7, 4);
    // }
    // else if($length == 12) {
    //     $number = substr($number, 0, 2) . '-' . substr($number, 1, 3) . '-' . substr($number, 4, 3) . '-' . substr($number, 7, 4);
    // }
    // $string = '123456789045645';
    $pattern = '/(\d{3})(\d{3})(\d{4})/';
    $replacement = '($1) $2-$3';
    return preg_replace($pattern, $replacement, $number);
    // return $number;
    }

if (! function_exists('sendNotification')) {

    function sendNotification($user_id,$title,$description,$type,$order_id=null)
    {
    	// $user_id=79;$title="Test notification from laravel ".date("d-m-Y h:i:s");$description="test from web";
        $tokens = [];
        $apns_ids = [];
        $responseData = [];

        //$this->notification_entry($user_id,$title,$description,$type,$order_id);

        $notification = new UserNotification;
        $notification->user_id = $user_id;
        $notification->title = $title;
        $notification->message = $description;
        $notification->type = $type;
        $notification->order_id = $order_id;
        $notification->save();


	// for Android

/*
        if ($FCMTokenData = fcmToken::where('user_id',$user_id)->where('device_id','!=',null)->where('device_type','ANDROID')->select('device_id')->get())
        {
            foreach ($FCMTokenData as $key => $value)
            {
                $tokens[] = $value->device_id;

            }

            $msg = array('type'=>$type,'title' => $title,'body'  => $description,'subtitle' => 'This is a subtitle',
                  );

            $fields = array
                    (
                        'registration_ids'  => $tokens,
                        'notification'  => $msg
                    );


            $headers = array
                    (
                        'Authorization: key=' . env('FCM_SERVER_KEY'),
                        'Content-Type: application/json'
                    );

            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch );

            if ($result === FALSE)
            {
                die('FCM Send Error: ' . curl_error($ch));
            }
            $result = json_decode($result,true);
            // echo "<pre>";
            // print_r($result);
            // exit();
            // dd($result);
            $responseData['android'] =[
                       "result" =>$result
                    ];

            curl_close( $ch );

        }

	// for IOS

        if ($FCMTokenData = fcmToken::where('user_id',$user_id)->where('device_id','!=',null)->where('device_type','IOS')->select('device_id')->get())
        {
           foreach ($FCMTokenData as $key => $value)
            {
                $apns_ids[] = $value->token;
            }

            $url = "https://fcm.googleapis.com/fcm/send";

            $serverKey = env('FCM_SERVER_KEY');
            $title = $title;
            $body = $description;
            $notification = array('type'=>$type, 'title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');
            $arrayToSend = array('registration_ids' => $apns_ids, 'notification' => $notification,'priority'=>'high');
            $json = json_encode($arrayToSend);
            $headers = array();
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Authorization: key='. $serverKey;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            //Send the request
            $result = curl_exec($ch);

            if ($result === FALSE)
            {
                die('FCM Send Error: ' . curl_error($ch));
            }
            $result = json_decode($result,true);
            $responseData['ios'] =[
                        "result" =>$result
                    ];

            //Close request
            curl_close($ch);
        }
        // echo "<pre>";
        // print_r($responseData);
        // exit();
         return $responseData;
*/

     }
	}




}
