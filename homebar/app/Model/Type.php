<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Type extends Model {

    use Sluggable;
    
    public function sluggable() {
        return ['slug'=>[
            'source'=>'name',
            'onUpdate'=>true
        ]];
    }

    public static function getTypeIdBySlug($type_slug) {
        $id = Type::where('slug', $type_slug)->pluck('id')->first();
        return $id;
    }
    public static function getTypeBySlug($type_slug) {
        $row = Type::where('slug', $type_slug)->first();
        return $row;
    }
	
	public static function typeSlug($id)
    {
        $result = Type::select('name','slug')->where('id', $id)->first();
        return $result->slug;
    }
    

}