<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model; 

class BartendingInformation extends Model {

	public $table = 'bartending_informations';
 
    public $fillable = [
        'user_id','social_security_number','bartending_license_number','expiry_date','bartending_license_image','dl_number','dl_image'
    ];
    

}