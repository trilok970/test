<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class DoItYourself extends Model {

    use Sluggable;

    public function sluggable() {
        return ['slug'=>[
            'source'=>'title',
            'onUpdate'=>true
        ]];
    }

    protected $table = "do_it_yourself";

    public function CategoryData(){
        return $this->belongsTo('App\Model\Category','category_id','id');
    }

    public function ingredients()
    {
        return $this->hasMany(DoItYourselfIngredient::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function TypeData(){
        return $this->belongsTo('App\Model\Type','type_id','id');

    }
    public function user_diy_likes()
    {
        return $this->hasMany(UserDiyLike::class,'diy_id');
    }
}
