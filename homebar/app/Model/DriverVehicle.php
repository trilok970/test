<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model; 

class DriverVehicle extends Model {
 
    public $fillable = [
        'user_id','license_number','license_expiry_date','license_image','vehicle_number','vehicle_make','vehicle_model','vehicle_color','vehicle_image','insurance_image','insurance_expiry_date','social_security_number'
    ];
    

}