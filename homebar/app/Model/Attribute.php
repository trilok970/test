<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Attribute extends Model {

    use Sluggable;
    
   public function sluggable() {
        return ['slug'=>[
            'source'=>'name',
            'onUpdate'=>true
        ]];
    }

    public function getAttributeCategories(){
        return $this->hasMany('App\Model\AttributeCategory', 'attribute_id');
         
    }
    

}