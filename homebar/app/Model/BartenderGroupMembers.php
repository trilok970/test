<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class BartenderGroupMembers extends Model {

	public $table = 'bartender_group_members';

    protected $fillable = ['name','mobile_number','email','experience','signature_drink','photo'];

}
