<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class MenuItem extends Model {

    use Sluggable;


    //protected $appends = ['discounted_price'];


    public function sluggable() {
        return ['slug'=>[
            'source'=>'name',
            'onUpdate'=>true
        ]];
    }
    /*
    public function getDiscountedPriceAttribute() {
        $offers = \Config::get('offers');
        if(!empty($this->price) && !empty($this->type_id) && isset($offers[$this->type_id]) && $offers[$this->type_id] > 0) {
            $offer_per = $offers[$this->type_id];
            $discounted_price = $this->price - (($offer_per/100) * $this->price);
            return $discounted_price;
        }
        return $this->price;
    }
    */

    public function categoryData(){
        return $this->belongsTo('App\Model\Category', 'category_id');
    }

    public function restaurantData(){
        return $this->belongsTo('App\Model\User', 'restaurant_id');
    }

    public function subCategoryData(){
        return $this->belongsTo('App\Model\Category', 'subcategory_id');
    }
    public function menuItemImages(){
        return $this->hasMany('App\Model\MenuItemImage', 'menu_item_id');
    }

    public function getChilrensMenuItems(){
        return $this->hasMany('App\Model\MenuItem', 'parent_id');
    }
    public function getMenuItemAttributes(){
        return $this->hasMany('App\Model\MenuItemAttribute', 'menu_item_id');
    }

    public static function getPaginateFormattedData($data){
        $offers = Offer::getOffers();
        $records = $data['data'];
        $return_records = [];
        foreach($records as $key => $row){
            $row_copy = $row;
            unset($row_copy['menu_item_images']);
            unset($row_copy['get_chilrens_menu_items']);
            unset($row_copy['category_data']);
            unset($row_copy['sub_category_data']);
            $row['discounted_price'] = Offer::getDiscountedPriceAll($offers, $row['user_id'], $row['type_id'], $row['price']);
            array_unshift($row['get_chilrens_menu_items'], $row_copy);
            foreach($row['get_chilrens_menu_items'] as $key_child => $row_child){
                $row['get_chilrens_menu_items'][$key_child]['discounted_price'] = Offer::getDiscountedPriceAll($offers, $row_child['user_id'], $row_child['type_id'], $row_child['price']);
            }
            $return_records[] = $row;
        }
        $data['data'] = $return_records;
        return $data;
    }

    public static function getFormattedData($data, $cart_item_qty){
        $offers = Offer::getOffers();
        $return_records = [];
        foreach($data as $key => $row){
            $row_copy = $row;
            unset($row_copy['menu_item_images']);
            unset($row_copy['get_chilrens_menu_items']);
            unset($row_copy['category_data']);
            unset($row_copy['sub_category_data']);
            $row['discounted_price'] = Offer::getDiscountedPriceAll($offers, $row['user_id'], $row['type_id'], $row['price']);
            array_unshift($row['get_chilrens_menu_items'], $row_copy);
            foreach($row['get_chilrens_menu_items'] as $menu_k => $menu_v){
                $qty = isset($cart_item_qty[$menu_v['id']]) ? $cart_item_qty[$menu_v['id']] : null;
                $row['get_chilrens_menu_items'][$menu_k]['quantity'] = $qty;
            }
            foreach($row['get_chilrens_menu_items'] as $key_child => $row_child){
                $row['get_chilrens_menu_items'][$key_child]['discounted_price'] = Offer::getDiscountedPriceAll($offers, $row_child['user_id'], $row_child['type_id'], $row_child['price']);
            }
            $return_records[] = $row;
        }
        return $return_records;
    }
    public function images()
    {
        return $this->hasMany(MenuItemImage::class);
    }

    public static function getTypeId($menu_item_id){
        $type_id = MenuItem::where(['id'=>$row['menu_item_id']])
            ->pluck('type_id')
            ->first();
        return $type_id;
    }

    public function dataFormat($data = []){

    }
}
