<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model; 

class DrinkVideo extends Model {

	public $table = 'drink_videos';
 
    public $fillable = [
        'user_id','video','status'
    ];
    

}