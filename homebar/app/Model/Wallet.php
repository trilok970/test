<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Wallet extends Model {
    
    public function order(){
        return $this->belongsTo("App\Model\Order", 'order_id', 'id');
    }
    public function user(){
        return $this->belongsTo("App\Model\User", 'user_id', 'id');
    }

    public static function addEntryWallet($params=[]){
        $wallet_entity = new Wallet();
        $wallet_entity->user_id = $params['user_id'];
        $wallet_entity->order_id = isset($params['order_id']) ? $params['order_id'] : null;
        $wallet_entity->transaction_type = isset($params['transaction_type']) ? $params['transaction_type'] : null;
        $wallet_entity->type = $params['type'];
        $wallet_entity->amount = $params['amount'];
        $wallet_entity->transaction_id = isset($params['transaction_id']) ? $params['transaction_id'] : null;
        $wallet_entity->save();
        return true;
    }

    public static function getDataFormatted($data=[]){
        $result = [];
        foreach($data as $key => $row){
            $date = date('Y-m-d', strtotime($row['created_at']));
            $result[$date][] = $row;
        }
        $formatted = [];
        foreach($result as $date => $date_result){
            $formatted[] = ['date'=> $date, 'date_result'=> $date_result];
        }
        return $formatted;
    }
    
}
