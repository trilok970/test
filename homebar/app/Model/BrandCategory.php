<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class BrandCategory extends Model {

    public static function getSelectedCategory($brand_id){
        $already_selected = BrandCategory::where('brand_id', $brand_id)
        ->pluck('category_id')
        ->toArray();
        return $already_selected;
    }

}