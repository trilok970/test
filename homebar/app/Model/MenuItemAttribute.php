<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class MenuItemAttribute extends Model {

    public static function getMenuItemAttrFormat($data) {
        $list = [];
        foreach($data as $key => $row){
            $list[$row->attribute_id] = $row->value;
        }
        return $list;
    }

}