<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
//use \Config;

class User extends Authenticatable implements JWTSubject
{
    //use Notifiable;
    protected $appends = ['profile_image_full', 'cover_image_full', 'logo_image_full', 'stripe_account_link'];

    protected $hidden = [
        'password', 'token'
    ];

    public $fillable = [
        'full_name','role','status','email','phone','phone_country_code','country_id','profile_image','dob','password','experience','address','latitude','longitude','signature_drink','price_per_hour','service_address_radius','bio','first_name','last_name'
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function countryData(){
        return $this->belongsTo('App\Model\Country','country_id','id');

    }

    public static function manageSocialAccounts($user_id, $social_id, $social_type) {
        SocialAccount::updateOrCreate(
                ['social_id' => $social_id, 'social_type' => $social_type], ['user_id' => $user_id]
        );
    }

    public static function getUserbyId($id) {
        $user = User::where('id', $id)->first();
        return $user;
    }

    public function getStripeAccountLinkAttribute() {
        if(empty($this->stripe_connect_id) && in_array($this->role, ['3','4','5'])) {
            
            $link = User::getUserStripeCreateLink($this->email);
            return $link;
        } else {
            return  null;
        }
    }

    public function getProfileImageFullAttribute() {
        if(!empty($this->profile_image)) {
            return asset($this->profile_image);
        } else {
            return  null;
        }
    }

    public function getCoverImageFullAttribute() {
        if(!empty($this->cover_image)) {
            return asset($this->cover_image);
        } else {
            return  null;
        }
    }

    public function getLogoImageFullAttribute() {
        if(!empty($this->logo_image)) {
            return asset($this->logo_image);
        } else {
            return  null;
        }
    }

    public function vehicleInfo(){
        return $this->hasOne("App\Model\DriverVehicle",'user_id');
    }

    public function bartending(){
        return $this->hasOne("App\Model\BartendingInformation",'user_id');
    }

    public function drink_videos(){
        return $this->hasMany("App\Model\DrinkVideo",'user_id');
    }

    public function userProducts(){
        return $this->hasMany("App\Model\MenuItem",'user_id');
    }

    public function vendorIngredients()
    {
        return $this->belongsToMany(DoItYourselfIngredient::class,'vendor_ingredients','vendor_id','ingredient_id');
    }

    public function getVendorIngredients()
    {
        return $this->hasMany(VendorIngredient::class, 'vendor_id');
    }


    public static function checkAvailability($params){
        $bartender_id = $params['bartender_id'];
        $book_date = $params['book_date'];
        $from_time = $params['from_time'];
        $to_time = $params['to_time'];
        $is_available = 0;
        $day_number = date('N', strtotime($book_date));
        $schedule_row = BartenderAvailability::where([
            'bartender_id'=>$bartender_id,
            'weekday'=>$day_number
        ])
        ->where('time_from', '<=', $from_time)
        ->where('time_to', '>=', $to_time)
        ->first();
        if (!empty($schedule_row->id)) {
            $leave_row = BartenderLeave::where([
                'bartender_id'=>$bartender_id,
                'date'=>$book_date
            ])
            ->first();
            if (empty($leave_row->id)) {
                $is_available = 1;
            }
        }
        return $is_available;
    }

    public function restaurantsLikes()
    {
        return $this->hasMany(RestaurantLike::class, 'restaurant_id', 'id');
    }
    
    public function bartenderSchedule()
    {
        return $this->hasMany(BartenderAvailability::class,'bartender_id');
    }


    public function currentMonthLeaves()
    {
        return $this->hasMany(BartenderLeave::class,'bartender_id')->whereMonth('date',date('m'));
    }

    public function bartenderGroupMembers()
    {
        return $this->hasMany(BartenderGroupMembers::class,'bartender_id');
    }

    public static function getFormattedBartender($data){
        $data_return = [];
        foreach($data as $key => $row){
            $params = [
                'vendor_id'=> $row['vendor_id'],
                'type_id'=> 4,
                'price'=> $row['price_per_hour']
            ];

            $discounted_price = Offer::getDiscountedPrice($params);
            $row['discounted_price'] = $discounted_price;
            $data_return[] = $row;
        }
        return $data_return;
    }

    public static function checkUserUnique($email, $phone){
        $status = true;
        $message = '';
        $user = User::where('phone', $phone);
        if($email) {
            $user = $user->orWhere('email', $email);
        }
        $user = $user->first();
        
        if(!empty($user->id)){
            $role_names = \Config::get('params.role_names');
            $role_name = isset($role_names[$user->role]) ? $role_names[$user->role] : '';
            $field_name = ($user->phone == $phone) ? 'Phone Number' : 'Email';
            $message = "$field_name is already registered as $role_name. Please use different ".strtolower($field_name)." to register.";
            $status = false;
        }
        return ['status'=> $status, 'message'=> $message];
    }

    public static function getUserStripeCreateLink($email){
        $client_id = env('STRIPE_CLIENT_ID');
        $redire_url = route('vendor.striperedirect');
        $link = "https://connect.stripe.com/express/oauth/authorize?redirect_uri=$redire_url&client_id=$client_id&stripe_user[email]=$email";
        return $link;
    }
	
	public static function getVenderStripeCreateLink($email){
        $client_id = env('STRIPE_CLIENT_ID');
        $redire_url = route('vendor.venderstriperedirect');
        $link = "https://connect.stripe.com/express/oauth/authorize?redirect_uri=$redire_url&client_id=$client_id&stripe_user[email]=$email";
        return $link;
    }
}
