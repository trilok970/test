<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Coupon extends Model {

       public static function checkCoupon($coupon_code ,$user_id){
            $response = [];
            $curr_date = date('Y-m-d');
            $coupon = Coupon::where('coupon_code', $coupon_code)
            ->first();
            
            if(empty($coupon->id)) {
                $response = ['status' => false, 'message' => 'Invalid coupon code'];
            } else if($coupon->expiry_date < $curr_date){
                $response = ['status' => false, 'message' => 'This coupon has expired'];
            } else {
                $coupon_used = Order::where(['user_id'=>$user_id, 'coupon_code'=>$coupon_code])->exists();
                if($coupon_used){
                    $response = ['status' => false, 'message' => 'You have already used this coupon'];
                } else {
                    $response = ['status' => true, 'message' => '', 'data'=>$coupon];
                }
            }
            return $response;
       }

}