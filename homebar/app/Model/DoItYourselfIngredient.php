<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class DoItYourselfIngredient extends Model {

    protected $table = "do_it_yourself_ingredients";

    public function vendors()
    {
        return $this->belongsToMany(User::class,'vendor_ingredients','ingredient_id','vendor_id');
    }

    public function diy()
    {
        return $this->belongsTo(DoItYourself::class,'do_it_yourself_id','id');
    }
    

    public static function getTypeFromIngredientId($ingredient_id) {
        $type_id = DoItYourselfIngredient::leftJoin('do_it_yourself', 'do_it_yourself_ingredients.do_it_yourself_id', '=', 'do_it_yourself.id')
        ->pluck('do_it_yourself.type_id')
        ->first();
        return $type_id;
    }
}
