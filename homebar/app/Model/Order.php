<?php

namespace App\Model;

use App\Http\Controllers\Vendor\DoitYourselfController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Lib\StripePayment;
use App\Model\OrderItem;

class Order extends Model {

    public function orderItems(){
        return $this->hasMany("App\Model\OrderItem", 'order_id');
    }

    public function customerInfo(){
        return $this->belongsTo("App\Model\User", 'user_id', 'id');
    }

    public function getTypeName(){
        return $this->belongsTo('App\Model\Type','type_id');
    }

    public static function genOrderNumber(){
        $last_order_number = Order::orderBy('id', 'DESC')
        ->pluck('order_number')
        ->first();
        if(!empty($last_order_number)){
            $order_number = $last_order_number + 1;
        } else {
            $order_number = 50000;
        }
        return $order_number;
    }



    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

  public function walletOrder()
    {
        return $this->hasMany(Wallet::class);
    }   public function ingredients()
    {
        $user =  Auth::user();
        $orderItems =  OrderItem::where('order_id',$this->id)->get();
        $ids = $orderItems->pluck('ingredient_id');
        $ingredients = VendorIngredient::with('ingredientDetails')->where('vendor_id',$user->id)->whereIn('ingredient_id',$ids)->get();
        return ['items' => $orderItems,'ingredients' => $ingredients];
    }

    public function getItemsTotal()
    {
        $user =  Auth::user();
        $items =  $this->items;
        $orderTotal = 0;
        foreach($items as $item) {
            if($item->restaurant_id === $user->id) {
                $orderTotal += $item->quantity * $item->price;
            }
        }
        return $orderTotal;
    }

    public function getItemsCount()
    {
        $user =  Auth::user();
        $items =  $this->items;
        $count = 0;
        foreach($items as $item) {
            if($item->restaurant_id === $user->id) {
                $count++;
            }
        }
        return $count;
    }

    public static function getEarningsFormatted($data=[]){
        $result = [];
        foreach($data as $key => $row){
            $date = date('Y-m-d', strtotime($row['created_at']));
            $result[$date][] = $row;
        }
        $formatted = [];
        foreach($result as $date => $date_result){
            $formatted[] = ['date'=> $date, 'date_result'=> $date_result];
        }
        return $formatted;
    }

    public function driverDetails()
    {
        $driverID = OrderItem::where(['order_id' => $this->id, 'restaurant_id' => auth()->user()->id])->pluck('driver_id');
        return User::where('id',$driverID)->first();
    }

    public static function getOrderRestaurantAmounts($params=[]){
        $order_id = $params['order_id'];
        $restaurant_id = $params['restaurant_id'];
        $order = Order::where(['id'=> $order_id])->first();
        $order_items = OrderItem::where(['order_id'=>$order_id, 'restaurant_id'=>$restaurant_id, 'is_wallet_updated'=>'0'])
        ->orderBy('order_items.id', 'ASC')
        ->get();
        if($order_items->count() == 0){
            return true;
        }
        $driver_id = null;
        $sub_total_price = $delivery_charge = $service_fee = $tax = $admin_commission = 0;
        $order_item_ids = [];
        foreach ($order_items as $i_key => $item_row) {
            $sub_total_price += $item_row->price * $item_row->quantity;
            $service_fee += $item_row->service_fee;
            $tax += $item_row->tax;
            $admin_commission += $item_row->admin_commission;
            $driver_id = $item_row->driver_id;
            $order_item_ids[] = $item_row->id;
        }

        $total_amount = ($sub_total_price + $service_fee + $tax) - $admin_commission;
        $wallet_params = [
            'user_id'=>$restaurant_id,
            'order_id'=>$order_id,
            'transaction_type'=>'Order',
            'type'=>'CR',
            'amount'=>$total_amount
        ];
        Wallet::addEntryWallet($wallet_params);

        if($order->reach_type == 'DELIVERY' && $order->delivery_charge > 0 && $driver_id){
            $rest_count = OrderItem::where(['order_id'=>$order_id])
            ->groupBy('restaurant_id')
            ->get()
            ->count();
            $delivery_charge = $order->delivery_charge / $rest_count;
            $wallet_params = [
                'user_id'=>$driver_id,
                'order_id'=>$order_id,
                'transaction_type'=>'Order',
                'type'=>'CR',
                'amount'=>$delivery_charge
            ];
            Wallet::addEntryWallet($wallet_params);
        }
        OrderItem::whereIn('id', $order_item_ids)
        ->update(['is_wallet_updated'=>'1']);
        return true;
    }

    public static function bartenderAmountCredit($params=[]){
        $order_id = $params['order_id'];
        $order = Order::where(['id'=> $order_id])->first();
        $item_row = OrderItem::where(['order_id'=>$order_id, 'is_wallet_updated'=>'0'])
        ->first();
        if(empty($item_row->id)){
            return true;
        }
        $service_fee = $tax = $admin_commission = 0;
        $sub_total_price = $item_row->price * $item_row->time_in_hours;


        if($order->service_fee > 0){
            $service_fee = $order->service_fee;
        }

        if($order->tax > 0){
            $tax = $order->tax;
        }

        if($order->admin_commission > 0){
            $tax = $order->admin_commission;
        }

        $total_amount = ($sub_total_price + $service_fee + $tax) - $admin_commission;
        $user_id = !empty($item_row->restaurant_id) ? $item_row->restaurant_id : $item_row->bartender_id;

        $wallet_params = [
            'user_id'=>$user_id,
            'order_id'=>$order_id,
            'transaction_type'=>'Order',
            'type'=>'CR',
            'amount'=>$total_amount
        ];
        Wallet::addEntryWallet($wallet_params);

        OrderItem::where('id', $item_row->id)
        ->update(['is_wallet_updated'=>'1']);
        return true;
    }

    public static function refundOrder($params=[]) {
        $order_id = $params['order_id'];
        $restaurant_id = $params['restaurant_id'];
        $order = Order::where(['id'=> $order_id])->first();
        $order_items = OrderItem::where(['order_id'=>$order_id, 'restaurant_id'=>$restaurant_id, 'is_wallet_updated'=>'0'])
        ->orderBy('order_items.id', 'ASC')
        ->get();
        if($order_items->count() == 0){
            return true;
        }
        $driver_id = null;
        $sub_total_price = $delivery_charge = $service_fee = $tax = $coupon_discount_amount = 0;
        $order_item_ids = [];
        foreach ($order_items as $i_key => $item_row) {
            $sub_total_price += $item_row->price * $item_row->quantity;
            $driver_id = $item_row->driver_id;
            $order_item_ids[] = $item_row->id;
        }

        if($order->reach_type == 'DELIVERY' && $order->delivery_charge > 0){
            $rest_count = OrderItem::where(['order_id'=>$order_id])
            ->groupBy('restaurant_id')
            ->get()
            ->count();
            $delivery_charge = $order->delivery_charge / $rest_count;
        }

        if($order->coupon_discount_amount && $order->coupon_discount_amount > 0){
            $coupon_discount_amount = ($order->coupon_discount_amount / $order->sub_total) * $sub_total_price;
        }
        $sub_total_price = $sub_total_price - $coupon_discount_amount;

        if($order->service_fee > 0){
            $service_fee = ($order->service_fee / $order->sub_total) * $sub_total_price;
        }

        $total = $sub_total_price + $service_fee + $delivery_charge;


        if($order->tax > 0){
            $tax = ($order->tax / $order->sub_total) * $sub_total_price;
        }

        $total = $total + $tax;


        $total = round($total, 2);
        $total_multiple = $total * 100;

        $stripePay = new StripePayment();

        $strip_res = $stripePay->refund($order->stripe_transaction_id, $total_multiple);
        if(isset($strip_res->id)){
            OrderItem::whereIn('id', $order_item_ids)
                ->update(['is_wallet_updated'=>'1']);
        }
        return $strip_res;
    }

    public static function refundOrderBartender($params=[]) {
        $order_id = $params['order_id'];
        $order = Order::where(['id'=> $order_id])->first();
        $order_item = OrderItem::where(['order_id'=>$order_id, 'is_wallet_updated'=>'0'])
        ->first();
        if(empty($order_item->id)){
            return true;
        }
        $total = $order->total_amount;
        $total_multiple = $total * 100;

        $stripePay = new StripePayment();

        $strip_res = $stripePay->refund($order->stripe_transaction_id, $total_multiple);
        if(isset($strip_res->id)){
            OrderItem::where('id', $order_item->id)
                ->update(['is_wallet_updated'=>'1']);
        }
        return $strip_res;
    }

    public function getItemsTotalForAll()
    {
        $user =  Auth::user();
        $items =  $this->items;
        $orderTotal = 0;
        $orderTotalTax = 0;
        $orderTotalServiceFee = 0;
        $orderTotalAdminCommission = 0;
        foreach($items as $item) {
            if($item->restaurant_id === $user->id) {
                $orderTotal += $item->quantity * $item->price;
                $orderTotalTax += $item->tax ?? 0;
                $orderTotalServiceFee += $item->service_fee ?? 0;
                $orderTotalAdminCommission += $item->admin_commission ?? 0;
            }
        }
        $grandTotal = ( $orderTotal + $orderTotalTax + $orderTotalServiceFee ) - $orderTotalAdminCommission;
        return array('sub_total'=>$orderTotal,
                      'total_tax'=>$orderTotalTax,
                      'total_service_fee'=>$orderTotalServiceFee,
                      'total_admin_commission'=>$orderTotalAdminCommission,
                      'grand_total'=>$grandTotal);
    }
    public function getImageAttribute($value) {
        if($value) {
            return asset($value);
        } else {
            return  null;
        }
    }

}

