<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ContactRequest extends Model {

    public function user(){
        return $this->belongsTo('App\Model\User','user_id','id');
    }

    public function faqCategory(){
        return $this->belongsTo('App\Model\FaqCategory','faq_category_id','id');
    }
    public function getImageAttribute($value) {
        if($value) {
            return asset($value);
        } else {
            return  null;
        }
    }
}
