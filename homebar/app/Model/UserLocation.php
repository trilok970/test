<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class UserLocation extends Model {
    public $fillable = [
        'user_id'
    ];
    

    public function user(){
        return $this->belongsTo("App\Model\User",'user_id');
    }
}