<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Brand extends Model {

    use Sluggable;
    
   public function sluggable() {
        return ['slug'=>[
            'source'=>'name',
            'onUpdate'=>true
        ]];
    }
    
    public function getBrandCategories(){
        return $this->hasMany('App\Model\BrandCategory', 'brand_id');
         
    }

    public static function getBrandsByCategory($category_id){
        $brands = Brand::whereHas('getBrandCategories', function ($query) use ($category_id) {
            $query->where('category_id', $category_id);
        })
        ->pluck('name', 'id');
        return $brands;
    }

}