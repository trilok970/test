<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Review extends Model {

    public static function checkReviewGiven($order_id, $restaurant_id, $user_id){
        $review = Review::where(['user_id'=>$user_id, 'order_id'=> $order_id, 'restaurant_id'=> $restaurant_id])->first();
        return !empty($review->id) ? 1 : 0;
    }

    public static function checkReviewGivenBartender($order_id, $bartender_id, $user_id){
        $review = Review::where(['user_id'=>$user_id, 'order_id'=> $order_id, 'bartender_id'=> $bartender_id])->first();
        return !empty($review->id) ? 1 : 0;
    }
   
}