<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class BartenderAvailability extends Model {

	public $table = 'bartender_availability';

    protected $fillable = ['weekday','time_from','time_to'];
}
