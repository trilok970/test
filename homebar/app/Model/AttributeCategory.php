<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class AttributeCategory extends Model {
    

    public static function getSelectedCategory($brand_id){
        $already_selected = AttributeCategory::where('attribute_id', $brand_id)
        ->pluck('category_id')
        ->toArray();
        return $already_selected;
    }

}