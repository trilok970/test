<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model {
    public $table = 'offers';

    public $timestamps = false;

    public static function getDiscountedPrice($params=[]){
        $vendor_id = $params['vendor_id'];
        $type_id = $params['type_id'];
        $price = $params['price'];
        $discounted_price = $price;
        
        if(!empty($vendor_id)){
            $curr_date = date('Y-m-d');

            $offer = Offer::where(['type_id'=> $type_id])
            ->where('applied_from', '<=', $curr_date)
            ->where('applied_to', '>=', $curr_date)
            ->where('user_id', $vendor_id)
            ->first();
            if(!empty($offer->id) && !empty($offer->discount_per)) {
                $offer_per = $offer->discount_per;
                $discounted_price = $price - (($offer_per/100) * $price);
                $discounted_price = round($discounted_price, 2);
                
            }
        }
        return $discounted_price;
    }

    public static function getOffers(){
        $curr_date = date('Y-m-d');
        $result = Offer::where('applied_from', '<=', $curr_date)
            ->where('applied_to', '>=', $curr_date)
            ->get();
        $offers = [];
        foreach($result as $key => $row) {
            $offers[$row->user_id][$row->type_id] = $row->discount_per;
        }
        return $offers;
    }

    public static function getDiscountedPriceAll($offers=[], $user_id='', $type_id='', $price) {
        
        if(!empty($price) && !empty($type_id) && !empty($user_id) && isset($offers[$user_id][$type_id]) && $offers[$user_id][$type_id] > 0) {
            $offer_per = $offers[$user_id][$type_id];
            $discounted_price = $price - (($offer_per/100) * $price);
            return $discounted_price;
        }
        return $price;
    }
}
