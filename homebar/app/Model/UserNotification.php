<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Auth;
class UserNotification extends Model
{
    //
    public static function getNotification(){
	    return self::where('user_id',Auth::id())->get();
    }
    
    public static function getNotificationCount(){
	    return self::where('user_id',Auth::id())->where('read_status','0')->get();
    }
}
