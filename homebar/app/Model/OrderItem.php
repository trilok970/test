<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model {

    public function menuItemDetail(){
        return $this->belongsTo("App\Model\MenuItem", 'menu_item_id');
    }
    
    public function driverDetail(){
        return $this->belongsTo("App\Model\User", 'driver_id');
    }

    public function restaurentDetail(){
        return $this->belongsTo("App\Model\User", 'restaurant_id');
    }

    public function bartender(){
        return $this->belongsTo("App\Model\User", 'bartender_id');
    }

    public function order(){
        return $this->belongsTo("App\Model\Order", 'order_id', 'id');
    }

    public function diyDetails()
    {
        return $this->belongsTo('App\Model\DoItYourself', 'diy_id');
    }

    public function ingredientDetail()
    {
        return $this->belongsTo(DoItYourselfIngredient::class,'ingredient_id');
    }

    public function driverInfo()
    {
        return $this->belongsTo(User::class,'driver_id');
    }
}
