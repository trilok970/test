<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DeliveryCharges extends Model
{
    protected $guarded = [];

    public static function getDeliveryRow($time){
        $row = DeliveryCharges::where('time_from', '<=', $time)
        ->where('time_to', '>=', $time)
        ->first();
        return $row;
    }
}
