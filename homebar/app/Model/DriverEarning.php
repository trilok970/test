<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class DriverEarning extends Model {

    public function orderData(){
        return $this->belongsTo('App\Model\Order', 'order_id', 'id');
         
    }

    public static function getDataFormatted($data=[]){
        $result = [];
        foreach($data as $key => $row){
            $date = date('Y-m-d', strtotime($row['created_at']));
            $result[$date][] = $row;
        }
        $formatted = [];
        foreach($result as $date => $date_result){
            $formatted[] = ['date'=> $date, 'date_result'=> $date_result];
        }
        return $formatted;
    }
    

}