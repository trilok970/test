<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class VendorIngredient extends Model {

    protected $table = "vendor_ingredients";
    //protected $appends = ['discounted_price'];

    /*
    public function getDiscountedPriceAttribute() {
        $offers = \Config::get('offers');
        $type_id = isset($this->ingredientDetails->diy->type_id) ? $this->ingredientDetails->diy->type_id : null;
        if(!empty($this->cost) && !empty($type_id) && isset($offers[$type_id]) && $offers[$type_id] > 0) {
            $offer_per = $offers[$type_id];
            $discounted_price = $this->cost - (($offer_per/100) * $this->cost);
            return $discounted_price;
        }
        return $this->cost;
    }
    */
    public function vendors()
    {
        return $this->belongsToMany(User::class);
    }

    public function ingredientDetails()
    {
        return $this->belongsTo(DoItYourselfIngredient::class, 'ingredient_id', 'id');
    }

    public static function getVendorIngredient($ingredient_id, $restaurant_id){
        $row = VendorIngredient::where(['vendor_id'=>$restaurant_id, 'ingredient_id'=>$ingredient_id])->first();
        return $row;
    }

}
