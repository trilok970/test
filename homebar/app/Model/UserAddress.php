<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model {

    public static function getAddress($user_id){
        $addresses = UserAddress::where(['user_id'=>$user_id])->get();
        return $addresses;
    }

    public static function getDefaultAddress($user_id){
        $address = UserAddress::where(['user_id'=>$user_id, 'is_default'=>'1'])
        ->first();
        return $address;
    }

}