<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model {

    use Sluggable;
    
    protected $appends = ['image_full'];
    
    public function sluggable() {
        return ['slug'=>[
            'source'=>'name',
            'onUpdate'=>true
        ]];
    }

    public function getImageFullAttribute() {
        if(!empty($this->image)) {
            return asset($this->image);
        } else {
            return  null;
        }
    }
    
    public function typeData(){
        return $this->belongsTo('App\Model\Type','type_id','id');
         
    }
    public function parentData(){
        return $this->belongsTo('App\Model\Category','parent_id','id');
         
    }
    public function childrenData(){
        return $this->hasMany('App\Model\Category', 'parent_id');
         
    }
    public function childrenCount(){
        return $this->hasMany('App\Model\Category', 'parent_id');
    }

    public static function getCategory($type_id=null){
        $conditions['status'] = '1';
        if ($type_id) {
            $conditions['type_id'] = $type_id;
        }
        $list = Category::where($conditions)
        ->whereNull('parent_id')
        ->pluck('name', 'id')
        ->toArray();
        return $list;
    }

    public static function getSubCategory($category_id=null){
        $conditions['status'] = '1';
        if ($category_id) {
            $conditions['parent_id'] = $category_id;
        }
        $list = Category::where($conditions)
        ->pluck('name', 'id')
        ->toArray();
        return $list;
    }

}