<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    public function orderData(){
        return $this->belongsTo('App\Model\Order', 'order_id', 'id');  
    }

    public function orderItemData(){
        return $this->belongsTo('App\Model\OrderItem', 'order_item_id', 'id');  
    }

    public static function getNotificationFormatted($data=[]){
        $result = [];
        foreach($data as $key => $row){
            $date = date('Y-m-d', strtotime($row['created_at']));
            $result[$date][] = $row;
        }
        $formatted = [];
        foreach($result as $date => $date_result){
            $formatted[] = ['date'=> $date, 'date_result'=> $date_result];
        }
        return $formatted;
    }
    
}