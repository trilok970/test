<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    public static function getTemplate($slug){
        $template = \App\Model\EmailTemplate::where([['slug', $slug]])->first();
        return $template;
                
    }
    
}
