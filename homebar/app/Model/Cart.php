<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Cart extends Model
{
    public $fillable = [
        'user_id', 'menu_item_id'
    ];

    protected $appends = ['in_stock'];

    public function getInStockAttribute() {
        if($this->menu_item_id) {
            $menu_item = MenuItem::where(['id'=>$this->menu_item_id])->first()->in_stock ?? 0;
        }
        else if($this->diy_id) {
            $menu_item = VendorIngredient::where(['vendor_id'=>$this->restaurant_id,'ingredient_id'=>$this->ingredient_id,'diy_id'=>$this->diy_id,'in_stock'=>'Yes'])->count();
            if($menu_item > 0) {
                $menu_item = 1;
            }
            else {
                $menu_item = 0;
            }
        }
        return $menu_item;

    }
    public static function getOrderMinAmount() {

        $amount = Setting::where(['slug'=>'order_min_amount'])->first()->value ?? 0;
        return $amount;

    }
    public static function getCarts($user_id, $lat, $lng, $ep=[],$cart_type_id=null)
    {
        $coupon = isset($ep['coupon']) ? $ep['coupon'] : [];
        $order_type = isset($ep['order_type']) ? $ep['order_type'] : 'DELIVERY';
        $delivery_date = isset($ep['delivery_date']) ? $ep['delivery_date'] : '';

        $carts = Cart::with(['menuItemDetails.images', 'ingredient.diy', 'restaurantDetails', 'diyDetails', 'bartender'=>function ($query) {
            $query->select('users.id', 'users.vendor_id', 'users.first_name', 'users.last_name', 'users.description', 'users.latitude', 'users.cover_image', 'users.longitude', 'users.profile_image', 'users.rating', 'users.experience', 'users.bio', 'users.price_per_hour', 'users.price', DB::raw("IF(users.options='1', 'INDIVIDUAL', 'GROUP') AS options, COUNT(bartender_group_members.id)+1 AS group_of"))->leftJoin('bartender_group_members', function ($join) {
                $join->on('bartender_group_members.bartender_id', '=', 'users.id');
            });
        }
        ])

        ->where(['user_id'=>$user_id])
        ->when($cart_type_id,function($q) use($cart_type_id) {
            $q->where('type_id',$cart_type_id);
        })
        ->get()
        ->toArray();


        $restaurants = $cart_data = $cart_final = $prices = $diy_arr = [];
        $type = '';
        $type_id = '';

        $offers = Offer::getOffers();

        foreach ($carts as $key => $row) {
            $row_copy = $row;
            if (empty($type)) {
                $type = $row['type'];
            }
            if (empty($type_id)) {
                $type_id = $row['type_id'];
            }
            unset($row_copy['restaurant_details']);
            if (! isset($restaurants[$row['restaurant_id']])) {
                $restaurants[$row['restaurant_id']] = $row['restaurant_details'];
            }

            if ($row['type'] == 'DIY') {
                $diy_arr[$row['diy_id']] = $row['diy_details'];
                unset($row_copy['diy_details']);

                $vendor_item_row = VendorIngredient::getVendorIngredient($row['ingredient_id'], $row['restaurant_id']);


                $discounted_price = Offer::getDiscountedPriceAll($offers, $row['restaurant_id'], $type_id, $vendor_item_row['cost']);
                //echo $row['id'].'-----'. $discounted_price.'---'.$vendor_item_row['cost']; die;
                if (isset($vendor_item_row['cost'])) {
                    $total_item_cost = $discounted_price * $row['quantity'];
                    $prices[] = $total_item_cost;
                }
                $row_copy['ingredient']['price'] = $vendor_item_row['cost'];
                $row_copy['ingredient']['price_total'] = $vendor_item_row['cost'] * $row['quantity'];
                $row_copy['ingredient']['discounted_price'] = $discounted_price;
                $row_copy['ingredient']['discounted_total_price'] = $total_item_cost;
                $cart_data[$row['restaurant_id']][] = $row_copy;
            } elseif ($row['type'] == 'PREMADE') {
                $discounted_price = Offer::getDiscountedPriceAll($offers, $row['restaurant_id'], $type_id, $row['menu_item_details']['price']);
                $row_copy['menu_item_details']['discounted_price'] = $discounted_price;
                $cart_data[$row['restaurant_id']][] = $row_copy;
                $prices[] = $discounted_price * $row['quantity'];
            } elseif ($row['type'] == 'BARTENDER') {
                $cart_data = $row_copy;
                $bartender = $row_copy['bartender'];
                $params_offer = [
                    'vendor_id'=> $bartender['vendor_id'],
                    'type_id'=> $row_copy['type_id'],
                    'price'=> $bartender['price_per_hour']
                ];
                $discounted_price = Offer::getDiscountedPrice($params_offer);


                $from_time = strtotime($row_copy['from_time']);
                $to_time = strtotime($row_copy['to_time']);

                $total_hours = ceil(abs($to_time - $from_time) / 3600);
                if ($bartender['options'] == 'GROUP') {
                    $total_people = $bartender['group_of'];
                    $cost_per_people = $discounted_price/$total_people;
                    $discounted_price = $cost_per_people * $row_copy['quantity'];
                }

                $discounted_price = round($discounted_price, 2);
                $cart_data['bartender']['discounted_price'] = $discounted_price;
                $total_cost = $discounted_price * $total_hours;
                $total_cost = round($total_cost, 2);
                $cart_data['bartender']['total_cost'] = $total_cost;
                $prices[] = $total_cost;
            }
        }

        $key_data = 'restaturants';
        if ($type == 'DIY') {
            // $key_data = 'diys';
            // foreach ($cart_data as $diy_id => $diy_data) {
            //     $rest_final = [];
            //     foreach ($diy_data as $rest_id => $rest_data) {
            //         $restaurant_data = isset($restaurants[$rest_id]) ? $restaurants[$rest_id] : null;
            //         $distance = distance($lat, $lng, $restaurant_data['latitude'], $restaurant_data['longitude']);
            //         $restaurant_data['distance'] = $distance;
            //         $restaurant_data['carts'] = $diy_data[$rest_id];
            //         $rest_final[] = $restaurant_data;
            //     }
            //     $diy_data = isset($diy_arr[$diy_id]) ? $diy_arr[$diy_id] : null;

            //     $diy_data['restaurants'] = $rest_final;
            //     $cart_final[] = $diy_data;
            // }
            foreach ($cart_data as $rest_id => $rest_data) {
                $restaurant_data = isset($restaurants[$rest_id]) ? $restaurants[$rest_id] : null;
                $distance = distance($lat, $lng, $restaurant_data['latitude'], $restaurant_data['longitude']);
                $restaurant_data['distance'] = $distance;
                $restaurant_data['carts'] = $cart_data[$rest_id];
                $cart_final[] = $restaurant_data;
            }
        } elseif ($type == 'PREMADE') {
            foreach ($cart_data as $rest_id => $rest_data) {
                $restaurant_data = isset($restaurants[$rest_id]) ? $restaurants[$rest_id] : null;
                $distance = distance($lat, $lng, $restaurant_data['latitude'], $restaurant_data['longitude']);
                $restaurant_data['distance'] = $distance;
                $restaurant_data['carts'] = $cart_data[$rest_id];
                $cart_final[] = $restaurant_data;
            }
        } elseif ($type == 'BARTENDER') {
            $cart_final = $cart_data;
        }


        $price_sum = array_sum($prices);
        $rest_count = count($restaurants);

        $params = [
            'price_sum'=>$price_sum,
            'restaurant_count'=> $rest_count,
            'coupon'=>$coupon,
            'order_type'=>$order_type,
            'delivery_date'=>$delivery_date
        ];
        $prices_cal = Cart::calculatePrices($params);

        $return_data = [
            $key_data=> $cart_final,
            'sub_total'=>$prices_cal['sub_total'],
            'discount'=> $prices_cal['discount'],
            'coupon_code'=> $prices_cal['coupon_code'],
            'service_fee'=>$prices_cal['service_fee'],
            'service_fee_percent'=>$prices_cal['service_fee_percent'],
            'tax'=>$prices_cal['tax'],
            'tax_percent'=>$prices_cal['tax_percent'],
            'total'=>$prices_cal['total'],
            'delivery_tip'=> 0,
            'delivery_fee'=>$prices_cal['delivery_fee'],
            'type'=>$type,
            'type_id'=>$type_id,
            'order_min_amount' => Cart::getOrderMinAmount()
        ];
        return $return_data;
    }

    public static function calculatePrices($params=[])
    {
        $settings = getSettings();
        $sub_total = round($params['price_sum'], 2);
        $rest_count = $params['restaurant_count'];
        $order_type = isset($params['order_type']) ? $params['order_type'] : 'DELIVERY';
        $delivery_date_time = isset($params['delivery_date']) ? $params['delivery_date'] : '';
        $coupon = isset($params['coupon']) ? $params['coupon'] : null;


        $high_volume_time_charge_one  = !empty($settings['high_volume_time_charges']) ? (float) $settings['high_volume_time_charges'] : 0;
        $service_fee_percent = !empty($settings['service_fee_percent']) ? (float) $settings['service_fee_percent'] : $settings['service_fee_percent'];
        $tax_percent = !empty($settings['tax_percent']) ? (float) $settings['tax_percent'] : $settings['tax_percent'];
        $commi_percent = !empty($settings['admin_commission']) ? $settings['admin_commission'] : 0;
        $service_fee = $tax = $delivery_fee = $admin_comm = 0;

        if ($order_type == 'DELIVERY' && $delivery_date_time && $rest_count) {

            $delivery_time = date('H:i:s', strtotime($delivery_date_time));
            $delivery_row = DeliveryCharges::getDeliveryRow($delivery_time);
            if (isset($delivery_row->id)) {
                $high_val_charge = ($delivery_row->is_high_volume_time) ? $high_volume_time_charge_one * $rest_count : 0;
                $delivery_fee = $delivery_row->delivery_charges * $rest_count;
                $delivery_fee = $delivery_fee + $high_val_charge;
                $delivery_fee = round($delivery_fee, 2);
            }
        }
        $coupon_discount_amt = 0;
        $coupon_code = null;
        if (isset($coupon->coupon_type)) {
            $coupon_code = $coupon->coupon_code;
            if ($coupon->coupon_type == 'Percentage') {
                $coupon_discount_amt = ($coupon->coupon_value/100) * $sub_total;
            } elseif ($coupon->coupon_type == 'Flat') {
                $coupon_discount_amt = $coupon->coupon_value;
            }
            $coupon_discount_amt = round($coupon_discount_amt, 2);
        }
        $sub_total_less_coupon = $sub_total - $coupon_discount_amt;

        if($commi_percent > 0){
            $admin_comm = ($sub_total_less_coupon / 100) * $commi_percent;
        }

        if ($service_fee_percent > 0) {
            $service_fee = ($service_fee_percent/100) * $sub_total_less_coupon;
            $service_fee = round($service_fee, 2);
        }
        $total = $sub_total_less_coupon + $service_fee + $delivery_fee;
        if ($tax_percent > 0) {
            $tax = ($tax_percent/100) * $total;
            $tax = round($tax, 2);
        }
        $total = $total + $tax;


        $res = [
            'sub_total'=> round($sub_total, 2),
            'discount'=> round($coupon_discount_amt, 2),
            'coupon_code'=> $coupon_code,
            'delivery_fee'=>round($delivery_fee, 2),
            'service_fee'=>round($service_fee, 2),
            'service_fee_percent'=>$service_fee_percent,
            'tax'=>round($tax, 2),
            'tax_percent'=>$tax_percent,
            'total'=>round($total, 2),
            'admin_commission'=> $admin_comm
        ];
        return $res;
    }

    public static function calculatePricesEachItem($params=[])
    {
        $service_fee = $tax = $admin_commission = 0;
        $sub_total_price = $params['price'] * $params['quantity'];
        if($params['service_fee'] > 0){
            $service_fee = ($params['service_fee'] / $params['sub_total']) * $sub_total_price;
        }

        if($params['tax'] > 0){
            $tax =  ($params['tax'] / $params['sub_total']) * $sub_total_price;
        }

        if($params['admin_commission'] > 0){
            $admin_commission =  ($params['admin_commission'] / $params['sub_total']) * $sub_total_price;
        }

        return [
            'service_fee'=> $service_fee,
            'tax'=> $tax,
            'admin_commission'=> $admin_commission,
        ];
    }

    public function bartender()
    {
        return $this->belongsTo('App\Model\User', 'bartender_id');
    }


    public function type()
    {
        return $this->belongsTo('App\Model\Type', 'type_id');
    }


    public function diyDetails()
    {
        return $this->belongsTo('App\Model\DoItYourself', 'diy_id');
    }

    public function menuItemDetails()
    {
        return $this->belongsTo('App\Model\MenuItem', 'menu_item_id');
    }

    public function ingredient()
    {
        return $this->belongsTo('App\Model\DoItYourselfIngredient', 'ingredient_id');
    }

    public function restaurantDetails()
    {
        return $this->belongsTo('App\Model\User', 'restaurant_id');
    }

    public static function getCartsProductQuantity($user_id)
    {
        $carts = Cart::where(['user_id'=>$user_id])
        ->pluck('quantity', 'menu_item_id')
        ->toArray();
        return $carts;
    }

    public static function deleteCart($user_id)
    {
        Cart::where(['user_id'=>$user_id])->delete();
        return true;
    }
    public static function deleteCartByTypeId($cartIds)
    {
        Cart::whereIn('id',$cartIds)->delete();
        return true;
    }
}
