<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $curr_date = date('Y-m-d');
        $site_settings = \App\Model\Setting::pluck('value', 'slug')->toArray();
        $types = \App\Model\Type::get();
        $offers = \App\Model\Offer::where('applied_from', '<=', $curr_date)
        ->where('applied_to', '>=', $curr_date)
        ->pluck('discount_per', 'type_id')
        ->toArray();
        
        //$types = \App\Model\Types::pluck('name')->toArray();
        \Config::set('settings', $site_settings);
        View::share('settings', $site_settings);
        \Config::set('types', $types);
        View::share('types', $types);

        \Config::set('offers', $offers);
        View::share('offers', $offers);
        
    }
}
