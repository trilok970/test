<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
define('SEGMENT',request()->segment(1));
define('SEGMENT2',request()->segment(2));

Route::get('/', function () {
    return view('vendors.index');
})->name('home');




/*Route::group(['prefix'=>'vendor', 'namespace' => 'vendor'], function () {
    Route::get('/signin', 'UsersController@signin')->name('vendor.signin');
    Route::post('/login', 'UsersController@login')->name('vendor.login');
    Route::get('/signup', 'UsersController@signup')->name('vendor.signup');
    Route::post('/signup','UsersController@store')->name('vendor.store');
    Route::get('/dashboard','UsersController@dashboard')->name('vendor.dashboard');

    Route::get('logout', 'UsersController@logout')->name('vendor.logout');

});
*/


// Route::get('/home', 'Auth\LoginController@showLoginForm')->name('vendor.home');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('vendor.login');
Route::post('login', 'Auth\LoginController@login')->name('vendor.login');
Route::get('logout', 'Auth\LoginController@logout')->name('vendor.logout');
Route::get('/signup', 'Vendor\UsersController@signup')->name('vendor.signup');
Route::post('/signup','Vendor\UsersController@store')->name('vendor.store');
Route::get('/assign-driver','Vendor\CronsController@assginDriver')->name('vendor.assginDriver');
Route::get('/reminder-license-expiry','Vendor\CronsController@reminderLicenseExpiry')->name('vendor.reminderLicenseExpiry');
Route::post('/tessst','Vendor\CronsController@tessst')->name('vendor.tessst');

Route::get('/user-platform-account','Front\UsersController@stripeRedirect')->name('vendor.striperedirect');
Route::get('/vender-platform-account','Front\UsersController@venderStripeRedirect')->name('vendor.venderstriperedirect');

Route::get('terms-and-conditions',function(){return view('vendors.terms-and-conditions');})->name('terms.and.conditions');
Route::get('about-us',function(){return view('vendors.about-us');})->name('about.us');
Route::get('faqs',function(){return view('vendors.faqs');})->name('faqs');
Route::get('contact-us',function(){return view('vendors.contact-us');})->name('contact.us');
Route::get('help-center',function(){return view('vendors.help-center');})->name('help.center');
Route::get('support',function(){return view('vendors.support');})->name('help.support');
Route::get('privacy-policy',function(){return view('vendors.privacy-policy');})->name('privacy.policy');
Route::get('taxes-fees',function(){return view('vendors.taxes-fees');})->name('taxes.fees');

Route::get('ios-receipt-validator',"TestController@IosReceiptValidate")->name('ios.receipt.validator');


Route::group(['prefix' => 'vendor' ,'namespace' => 'Vendor'], function () {

    //Route::get('/dashboard','UsersController@dashboard')->name('vendor.dashboard');

    Route::get('/forgot', 'UsersController@forgot')->name('vendor.forgot');
    Route::post('/forgot', 'UsersController@forgot')->name('vendor.forgot');

    Route::get('/reset/{token}', 'UsersController@reset')->name('vendor.reset');
    Route::post('/reset/{token}', 'UsersController@reset')->name('vendor.reset');
    Route::get('/app-page/{slug}', 'AppPagesController@page')->name('vendor.apppages');
});

Route::group(['prefix' => 'vendor','namespace' => 'Vendor', 'middleware'=> ['auth:vendor']], function () {
    // leftbar routes
    Route::get('/dashboard','UsersController@dashboard')->name('vendor.dashboard');
    Route::get('/drink-processing','UsersController@drinkprocessing')->name('vendor.drinkprocessing');
    Route::get('/drink-completed-order','UsersController@drinkcompletedorder')->name('vendor.drinkcompletedorder');
    Route::get('/drink-menu-list','UsersController@drinkmenulist')->name('vendor.drinkmenulist');
    Route::get('/my-profile','UsersController@myProfile')->name('vendor.myprofile');
    Route::any('/bartender-my-earning','UsersController@bartendermyearning')->name('vendor.bartendermyearning');
    // Do it yourself
    Route::get('/do-it-yourself/{type}/{category?}','DoitYourselfController@index')->name('vendor.diy.index');
    Route::get('/do-it-yourself/receipe/{type}/{category?}/{item?}','DoitYourselfController@show')->name('vendor.diy.receipe');
    Route::post('/do-it-yourself/store-vendor-items','DoitYourselfController@storeVendorItems')->name('vendor.diy.items.store');\

    // Offers by Category
    Route::get('/offers/{type}','OfferController@index')->name('vendor.offers.index');
    Route::post('/offers','OfferController@store')->name('vendor.offers.store');
    Route::put('/offers/{offer}/update','OfferController@update')->name('vendor.offers.update');
    // menubar routes
    Route::get('/drink-add-menu-pre','UsersController@drinkaddmenupre')->name('vendor.drinkaddmenupre');
    Route::get('/food-add-menu-pre','UsersController@foodaddmenupre')->name('vendor.foodaddmenupre');
    Route::get('/ambaince-add-menu','UsersController@ambainceaddmenu')->name('vendor.ambainceaddmenu');

    //bartender details
    Route::get('bartenders/add-details','UsersController@addbartenderdetails')->name('vendor.addbartenderdetails');
    Route::post('/add-bartender','BartendersController@store')->name('vendor.addbartender');

    // bartender video add routes
    Route::get('/bartenders/{id}/add-videos','UsersController@addbartendervideos')->name('vendor.addbartendervideos');
    Route::post('/add-videos','BartendersController@addbartendervideo')->name('vendor.addvideo');



    // left menu bartender list routes
    Route::get('/bartenders','BartendersController@bartenderlist')->name('vendor.bartenderlist');
    Route::get('bartenders/edit/{id}', 'BartendersController@edit')->name('vendor.bartender.edit');
    Route::post('bartender/update/{id}', 'BartendersController@update')->name('vendor.bartender.update');
    Route::get('bartender/destroy/{id}', 'BartendersController@destroy')->name('vendor.bartender.destroy');

    // bartender video edit routes
    Route::get('/bartenders/{id}/videos/edit', 'BartendersController@editvideo')->name('vendor.bartender.editvideo');
    Route::post('/edit-videos','BartendersController@editbartendervideo')->name('vendor.editvideo');

    // bartender calender schedule routes
    Route::get('/bartenders/{id}/schedule','UsersController@bartendercalendarschedule')->name('vendor.bartendercalendarschedule');
    Route::post('/bartenders/schedule/add','BartendersController@bartenderscheduleadd')->name('vendor.bartenderscheduleadd');

    // add group member routes
    Route::get('/bartenders/{id}/add-member','UsersController@bartenderaddmember')->name('vendor.bartenderaddmember');
    Route::post('/bartenders/member-add','BartendersController@bartendermemberadd')->name('vendor.bartendermemberadd');


    // group member list route
    Route::get('/bartenders/{id}/group-member-list','BartendersController@groupmemberlist')->name('vendor.groupmemberlist');
    Route::get('bartenders/{id}/group-members/{member_id}/edit', 'BartendersController@groupmemberedit')->name('vendor.groupmember.edit');
    Route::post('group-member/update/{id}', 'BartendersController@groupmemberupdate')->name('vendor.groupmember.update');
    Route::get('group-member/destroy/{id}', 'BartendersController@groupmemberdestroy')->name('vendor.groupmember.destroy');

    //Bartender leaves
    Route::post('/bartender/{id}/toggle-leave','BartendersController@toggleLeave')->name('vendor.bartender.toggle-leave');
    Route::get('/bartender/{id}/bookings','BartendersController@bookings')->name('vendor.bartender.bookings');
    Route::get('/bartender/{id}/leaves','BartendersController@leaves')->name('vendor.bartender.leaves');
    Route::post('/bartender/{id}/update-unavailability','BartendersController@updateUnavailability')->name('vendor.bartender.update-unavailability');
    Route::post('/bartender/{id}/update-availability','BartendersController@updateavailability')->name('vendor.bartender.update-availability');
    // route for update availability toggle.
    Route::post('users/status-update', 'BartendersController@updateavailability')->name('vendor.bartender.updateavailability');

    Route::post('/my-profile','UsersController@myProfile')->name('vendor.myprofile');

    Route::get('profile', 'UsersController@profile')->name('vendor.profile');
    Route::post('profile/update/{id}', 'UsersController@profileUpdate')->name('vendor.profile.update');

    Route::post('cover/update/', 'UsersController@profileCoverUpdate')->name('vendor.cover.update');
    Route::post('logo/update/', 'UsersController@profileLogoUpdate')->name('vendor.logo.update');

    Route::get('/change-password', 'UsersController@changePassword')->name('vendor.changePassword');
    Route::post('/change-password', 'UsersController@changePassword')->name('vendor.changePassword');

    Route::post('/deals/datatables', 'DealsController@datatable')->name('vendor.deals.datatables');
    Route::post('/deals/status-update', 'DealsController@statusUpdate')->name('vendor.deals.statusUpdate');
    Route::resource('/deals', 'DealsController');
    Route::post('/orders/datatables', 'OrdersController@datatable')->name('vendor.orders.datatables');
    Route::get('/confirm-order', 'OrdersController@showOrderConfirmForm')->name('vendor.orders.showOrderConfirmForm');
    Route::post('/confirm-order', 'OrdersController@orderConfirm')->name('vendor.orders.orderConfirm');

    Route::post('/confirm-order', 'OrdersController@orderConfirm')->name('vendor.orders.orderConfirm');

    Route::get('orders/recent/{type}', 'OrderController@index')->name('vendor.orders');
    Route::get('orders/progress/{type}', 'OrderController@inProgressOrders')->name('vendor.inprogress-orders');
    Route::get('orders/completed/{type}', 'OrderController@completeOrders')->name('vendor.complete-orders');
    Route::get('orders/rejected/{type}', 'OrderController@rejectedOrders')->name('vendor.rejected-orders');
    Route::get('orders/{list}/{order}/detail/{type}', 'OrderController@show')->name('vendor.order.show');
    Route::get('/menu-items/add/{type}', 'MenuItemsController@create')->name('vendor.menuItem.create');
    Route::post('/orders/accept-order','OrderController@acceptOrder')->name('vendor.order.acceptOrder');
    Route::post('/orders/reject-order','OrderController@rejectOrder')->name('vendor.order.rejectOrder');
    Route::post('/orders/{id}/update-order-status','OrderController@updateOrderStatus')->name('vendor.order.updateOrderStatus');
    Route::post('/orders/items/{id}/update-status','OrderController@updateOrderItemStatus')->name('vendor.order-items.updateOrderItemsStatus');
    Route::post('/menu-items/get-subcategories-brands-and-attributes', 'MenuItemsController@getSubcategoriesBrandsAndAttributes')->name('vendor.menuItem.getSubcategoriesBrandsAndAttributes');
    Route::post('/menu-items/add-variation', 'MenuItemsController@addVariation')->name('vendor.menuItem.addVariation');

    Route::post('/menu-items/store', 'MenuItemsController@store')->name('vendor.menuItem.store');

    Route::get('/menu-items/{type?}', 'MenuItemsController@list')->name('vendor.menuItem.list');
    Route::get('/menu-items/delete/{id}', 'MenuItemsController@delete')->name('vendor.menuItem.delete');

    Route::get('/menu-items/edit/{type}/{id}', 'MenuItemsController@edit')->name('vendor.menuItem.edit');
    Route::post('/menu-items/delete-image', 'MenuItemsController@deleteImage')->name('vendor.menuItem.deleteImage');


    Route::get('/vendor/category-not-found','UsersController@categoryNotFound')->name('vendor.category-not-found');
    Route::get('/user-availability/{availability}','UsersController@userAvailability')->name('vendor.user.status');
    Route::get('notifications','UsersController@notifications')->name('vendor.notifications');

});


Route::group(['prefix' => 'vendor','namespace' => 'Front'], function () {
    // Route::get('account-confirmation', 'UsersController@accountConfirmation')->name('front.reset-password');

    // Route::get('email-verify', 'UsersController@emailverify')->name('front.reset-password');



    // Route::get('reset-password', 'UsersController@resetPassword')->name('front.reset-password');
    // Route::post('reset-password', 'UsersController@updateResetPassword')->name('front.update-reset-password');

    Route::get('forgot-password', 'UsersController@forgotPassword')->name('front.forgot-password');
    Route::post('forgot-password', 'UsersController@forgotPassword')->name('front.forgot-password');

    Route::get('/verify/{token}', 'UsersController@verify')->name('vendor.verify');
    Route::post('/verify/{token}', 'UsersController@verify')->name('vendor.verify');



    Route::get('pages/{slug}', 'CustomPageViewController@showPages')->name('show-custom-page');;

});

/*
Route::get('/', function () {
    return redirect()->route('admin.dashboard');
});
*/

Route::get('admin/','Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::get('admin/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('admin/login','Auth\AdminLoginController@login')->name('admin.login');
Route::get('admin/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

Route::group(['prefix'=>'admin', 'namespace' => 'Admin'], function () {
    Route::get('/forgot', 'UsersController@forgot')->name('admin.forgot');
    Route::post('/forgot', 'UsersController@forgot')->name('admin.forgot');
    Route::get('/reset/{token}', 'UsersController@reset')->name('admin.reset');
    Route::post('/reset/{token}', 'UsersController@reset')->name('admin.reset');
});

Route::group(['prefix'=>'admin', 'namespace' => 'Admin', 'middleware'=> ['auth:admin']], function () {


    Route::get('/', 'UsersController@dashboard')->name('admin.dashboard');

    Route::get('drivers', 'DriversController@index')->name('admin.drivers');
    Route::post('drivers-datatable', 'DriversController@usersDataTable')->name('admin.driversDataTable');
    Route::get('drivers/view/{id}', 'DriversController@show')->name('admin.drivers.show');
    Route::delete('drivers/delete/{id}', 'DriversController@destroy')->name('admin.drivers.destroy');

    Route::get('vendors', 'VendorsController@vendors')->name('admin.vendors');
    Route::post('vendors-datatable', 'VendorsController@usersDataTable')->name('admin.vendorsDataTable');
    Route::get('vendors/view/{id}', 'VendorsController@show')->name('admin.vendors.show');
    Route::delete('vendors/delete/{id}', 'VendorsController@destroy')->name('admin.vendors.destroy');

    Route::get('bartenders', 'BartendersController@bartenders')->name('admin.bartenders');
    Route::post('bartenders-datatable', 'BartendersController@usersDataTable')->name('admin.bartendersDataTable');
    Route::get('bartenders/view/{id}', 'BartendersController@show')->name('admin.bartenders.show');
    Route::delete('bartenders/delete/{id}', 'BartendersController@destroy')->name('admin.bartenders.destroy');

    Route::get('users', 'UsersController@users')->name('admin.users');
    Route::post('users-datatable', 'UsersController@usersDataTable')->name('admin.usersDataTable');
    Route::get('users/view/{id}', 'UsersController@show')->name('admin.users.show');
    Route::delete('users/delete/{id}', 'UsersController@destroy')->name('admin.users.destroy');
    Route::post('users/status-update', 'UsersController@statusUpdate')->name('admin.users.statusUpdate');
    Route::get('/my-profile', 'UsersController@myProfile')->name('admin.myprofile');
    Route::post('/my-profile', 'UsersController@myProfile')->name('admin.myprofile');
    Route::get('/change-password', 'UsersController@changePassword')->name('admin.changePassword');
    Route::post('/change-password', 'UsersController@changePassword')->name('admin.changePassword');

    Route::get('/settings', 'SettingsController@settings')->name('admin.settings');
    Route::post('/settings', 'SettingsController@settingsUpdate')->name('admin.settings');

    Route::get('/pages', 'PagesController@index')->name('admin.pages.index');
    Route::any('pages/datatable', 'PagesController@datatable')->name('admin.pages.datatable');
    Route::any('/pages/edit/{slug}', 'PagesController@edit')->name('admin.pages.edit');
    Route::get('/pages/create', 'PagesController@create')->name('admin.pages.create');
    Route::post('/pages', 'PagesController@store')->name('admin.pages.store');

    Route::get('/templates', 'TemplatesController@index')->name('admin.templates.index');
    Route::post('templates/datatable', 'TemplatesController@datatable')->name('admin.templates.datatable');
    Route::any('/templates/edit/{slug}', 'TemplatesController@edit')->name('admin.templates.edit');

    Route::post('/types/datatables', 'CategoriesController@typesDatatable')->name('admin.types.datatables');
    Route::get('/types', 'CategoriesController@types')->name('admin.types.index');


    Route::get('/categories/add/{type_id}/{parent_id?}', 'CategoriesController@create')->name('admin.categories.add');
    Route::post('/categories/add/{type_id}/{parent_id?}', 'CategoriesController@store')->name('admin.categories.store');

    Route::get('/categories/edit/{id}', 'CategoriesController@edit')->name('admin.categories.edit');
    Route::post('/categories/edit/{id}', 'CategoriesController@update')->name('admin.categories.update');

    Route::post('/categories/datatables/{type_id}/{parent_id?}', 'CategoriesController@datatable')->name('admin.categories.datatables');
    Route::get('/categories/{type_id}/{parent_id?}', 'CategoriesController@index')->name('admin.categories.index');


    Route::delete('/categories/delete/{id}', 'CategoriesController@destroy')->name('admin.categories.destroy');


    Route::post('/brands/datatables', 'BrandsController@datatable')->name('admin.brands.datatables');
    Route::resource('/brands', 'BrandsController');

    Route::post('/attributes/datatables', 'AttributesController@datatable')->name('admin.attributes.datatables');
    Route::resource('/attributes', 'AttributesController');

    Route::post('/menu-items/datatables', 'MenuItemsController@datatable')->name('admin.menuItems.datatables');
    Route::get('/menu-items', 'MenuItemsController@index')->name('admin.menuItems.index');




    /** Do it yourself ***/

    Route::get('/do-it-yourself', 'DoItYourselfController@index')->name('admin.do_it_yourself.index');

    Route::get('/do-it-yourself/add', 'DoItYourselfController@create')->name('admin.do_it_yourself.add');
    Route::post('/do-it-yourself/add', 'DoItYourselfController@store')->name('admin.do_it_yourself.store');

    Route::post('/do-it-yourself/datatables', 'DoItYourselfController@datatable')->name('admin.do_it_yourself.datatables');

    Route::get('/do-it-yourself/view/{id}', 'DoItYourselfController@view')->name('admin.do_it_yourself.view');

    Route::get('/do-it-yourself/edit/{id}', 'DoItYourselfController@edit')->name('admin.do_it_yourself.edit');
    Route::post('/do-it-yourself/edit/{id}', 'DoItYourselfController@update')->name('admin.do_it_yourself.update');

    Route::delete('/do-it-yourself/delete/{id}', 'DoItYourselfController@destroy')->name('admin.do_it_yourself.destroy');

    Route::post('/do-it-yourself/get-categories', 'DoItYourselfController@get_categories')->name('admin.do_it_yourself.get_categories');

    /***** Coupon ******/

    Route::get('/coupons', 'CouponController@index')->name('admin.coupons.index');
    Route::get('/coupons/add', 'CouponController@create')->name('admin.coupons.add');
    Route::post('/coupons/add', 'CouponController@store')->name('admin.coupons.store');
    Route::post('/coupons/datatables', 'CouponController@datatable')->name('admin.coupons.datatables');
    Route::get('/coupons/view/{id}', 'CouponController@view')->name('admin.coupons.view');
    Route::get('/coupons/edit/{id}', 'CouponController@edit')->name('admin.coupons.edit');
    Route::post('/coupons/edit/{id}', 'CouponController@update')->name('admin.coupons.update');
    Route::delete('/coupons/delete/{id}', 'CouponController@destroy')->name('admin.coupons.destroy');


    /** Orders */

    Route::get('/orders','OrderController@index')->name('admin.orders.index');
    Route::post('/orders/datatables','OrderController@datatables')->name('admin.orders.datatable');
    Route::get('/order/{order}','OrderController@show')->name('admin.orders.show');

    // Delivery Charges

    Route::get('/delivery-charges','DeliveryChargesController@index')->name('admin.delivery-charges.index');
    Route::post('/delivery-charges','DeliveryChargesController@store')->name('admin.delivery-charges.store');

    Route::post('/faq-categories/datatables', 'FaqCategoriesController@datatable')->name('admin.faq-categories.datatables');
    Route::resource('/faq-categories', 'FaqCategoriesController');

    Route::get('/contact-requests','ContactRequestsController@index')->name('admin.contactRequests.index');
    Route::post('contact-requests/datatable', 'ContactRequestsController@datatable')->name('admin.contactRequests.datatable');
    Route::get('/contact-requests/view/{id}', 'ContactRequestsController@view')->name('admin.contactRequests.view');
    Route::any('/contact-requests/delete/{id}', 'ContactRequestsController@destroy')->name('admin.contactRequests.delete');

    Route::post('/plans/datatables', 'PlansController@datatable')->name('admin.plans.datatables');
    Route::resource('/plans', 'PlansController');

});
