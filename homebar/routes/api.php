<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization, X-CSRF-Token');
header('Access-Control-Allow-Credentials: true');

use Illuminate\Http\Request;

// here your routes



/*
header ("Access-Control-Allow-Origin: *");
header ("Access-Control-Expose-Headers: Content-Length, X-JSON");
header ("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
header ("Access-Control-Allow-Headers: *");
*/

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// For User Route
Route::group(['namespace' => 'Api\User'], function () {
    Route::post('get-countries', 'UsersController@getCountries');
    Route::post('check-signup', 'UsersController@checkSignup');
    Route::post('signup', 'UsersController@signup');
    Route::post('login', 'UsersController@lgoin');
    Route::post('social-login', 'UsersController@socialLogin');
    Route::post('forgot-password', 'UsersController@forgotPassword');
    Route::post('reset-password', 'UsersController@resetPassword');
    Route::post('resend-otp', 'UsersController@resendOtp');
    Route::post('check-phone', 'UsersController@checkPhone');

    Route::post('get-pages', 'PagesController@getPages');
    Route::post('order-credit-wallet', 'OrdersController@orderCreditWallet');



});

Route::group(['middleware' => 'jwt.auth', 'namespace' => 'Api\User'], function () {
    Route::post('set-password', 'UsersController@setPassword');
    Route::post('get-profile', 'UsersController@getProfile');
    Route::post('logout', 'UsersController@logout');
    Route::post('change-password', 'UsersController@changePassword');
    Route::post('edit-profile', 'UsersController@editProfile');
    Route::post('check-phone', 'UsersController@checkPhone');
    Route::post('notification-setting', 'UsersController@notificationSetting');

    Route::post('get-types', 'MenuItemsController@getTypes');
    Route::post('get-categories', 'MenuItemsController@getCategories');
    Route::post('get-brands', 'MenuItemsController@getBrands');
    Route::post('get-menu-items', 'MenuItemsController@getMenuItems');

    Route::post('get-restaurants', 'VendorsController@getRestaurants');
    Route::post('get-restaurant-detail', 'VendorsController@getRestaurantDetail');
    Route::post('like-dislike-restaurant', 'VendorsController@likeDislikeRestaurant');
    Route::post('add-to-cart', 'VendorsController@addToCart');
    Route::post('get-carts', 'VendorsController@getCarts');
    Route::post('delete-cart', 'VendorsController@deleteCart');

    Route::post('confirm-order', 'OrdersController@confirmOrder');

    Route::post('get-bartenders', 'BartendersController@getBartenders');
    Route::post('get-bartender-detail', 'BartendersController@getBartenderDetail');

    Route::post('add-address', 'AddressesController@addAddress');
    Route::post('get-address', 'AddressesController@getAddress');
    Route::post('edit-address', 'AddressesController@edit');
    Route::post('delete-address', 'AddressesController@delete');
    Route::post('make-default-address', 'AddressesController@makeDefaultAddress');
    Route::post('get-default-address-and-cart-type', 'AddressesController@getDefaultAddressAndCartType');

    Route::post('get-diy', 'DoItYourselfController@getDiy');
    Route::post('get-diy-details', 'DoItYourselfController@getDiyDetails');

    Route::post('like-dislike-diy', 'DoItYourselfController@likeDislikeDiy');
    Route::post('get-store-item', 'DoItYourselfController@getStoreItem');
    Route::post('add-to-cart-diy', 'OrdersController@addToCartDiy');

    Route::post('apply-coupon-code', 'OrdersController@applyCouponCode');

    Route::post('get-orders', 'OrdersController@getOrders');
    Route::post('order-detail', 'OrdersController@orderDetail');

    Route::post('get-cart-badge', 'OrdersController@getCartBadge');
    Route::post('add-to-cart-bartender', 'OrdersController@addToCartBartender');

    Route::post('get-notifications', 'UsersController@getNotifications');
    Route::post('get-faq-categories', 'UsersController@getFaqCategories');
    Route::post('contanct-us', 'UsersController@contanctUs');
    Route::post('add-or-update-subscription', 'UsersController@addOrUpdateSubscription');
    Route::post('add-money', 'UsersController@addMoney');
    Route::post('withdraw-money', 'UsersController@withdrawMoney');
    Route::post('transaction-list', 'UsersController@transactionList');
    Route::post('get-plans', 'UsersController@getPlans');
    Route::post('get-tip-percentage', 'OrdersController@getTipPercentage');
    Route::post('add-tip', 'OrdersController@addTip');
    Route::post('add-review', 'OrdersController@addReview');
    Route::post('refund', 'OrdersController@refund');
    Route::post('cart-item-remove', 'OrdersController@cartItemRemove');
    Route::post('order-cancel', 'OrdersController@orderCancel');
    Route::post('update-ingredients-qty', 'OrdersController@updateIngredientsQty');

    // Inapp purchase apis
    Route::post('purchase-plan','InAppPurchanseController@purchasePlan');
    Route::post('purchase-restore','InAppPurchanseController@purchaseRestore');


});

/*=========== */
// For Driver
Route::group(['namespace' => 'Api\Driver','prefix'=>'driver'], function () {
    Route::post('get-countries', 'UsersController@getCountries');
    Route::post('check-signup', 'UsersController@checkSignup');
    Route::post('signup', 'UsersController@signup');
    Route::post('login', 'UsersController@lgoin');
    Route::post('social-login', 'UsersController@socialLogin');
    Route::post('forgot-password', 'UsersController@forgotPassword');
    Route::post('reset-password', 'UsersController@resetPassword');
    Route::post('resend-otp', 'UsersController@resendOtp');
    Route::post('check-phone', 'UsersController@checkPhone');
    Route::post('get-pages', 'PagesController@getPages');
});
Route::group(['middleware' => 'jwt.auth', 'namespace' => 'Api\Driver','prefix'=>'driver'], function () {
    Route::post('set-password', 'UsersController@setPassword');
    Route::post('get-profile', 'UsersController@getProfile');
    Route::post('logout', 'UsersController@logout');
    Route::post('change-password', 'UsersController@changePassword');
    Route::post('edit-profile', 'UsersController@editProfile');
    Route::post('check-phone', 'UsersController@checkPhone');
    Route::post('update-vehicle', 'UsersController@updateVehicle');
    Route::post('get-vehicle-info', 'UsersController@getVehicleInfo');
    Route::post('notification-setting', 'UsersController@notificationSetting');
    Route::post('get-order-detail', 'OrdersController@getOrderDetail');
    Route::post('get-notifications', 'UsersController@getNotifications');
    Route::post('location-update', 'UsersController@locationUpdate');
    Route::post('get-orders', 'OrdersController@getOrders');
    Route::post('get-faq-categories', 'UsersController@getFaqCategories');
    Route::post('contanct-us', 'UsersController@contanctUs');
    Route::post('get-earnings', 'UsersController@getEarnings');
    Route::post('withdraw', 'UsersController@withdraw');
    Route::post('order-status-update', 'OrdersController@orderStatusUpdate');

});


/*=========== */
// For Bartender
Route::group(['namespace' => 'Api\Bartender','prefix'=>'bartender'], function () {
    Route::post('get-countries', 'UsersController@getCountries');
    Route::post('check-signup', 'UsersController@checkSignup');
    Route::post('signup', 'UsersController@signup');
    Route::post('login', 'UsersController@lgoin');
    Route::post('social-login', 'UsersController@socialLogin');
    Route::post('forgot-password', 'UsersController@forgotPassword');
    Route::post('reset-password', 'UsersController@resetPassword');
    Route::post('resend-otp', 'UsersController@resendOtp');
    Route::post('check-phone', 'UsersController@checkPhone');
    Route::post('get-pages', 'PagesController@getPages');
});
Route::group(['middleware' => 'jwt.auth', 'namespace' => 'Api\Bartender','prefix'=>'bartender'], function () {
    Route::post('set-password', 'UsersController@setPassword');
    Route::post('get-profile', 'UsersController@getProfile');
    Route::post('logout', 'UsersController@logout');
    Route::post('change-password', 'UsersController@changePassword');
    Route::post('edit-profile', 'UsersController@editProfile');
    Route::post('check-phone', 'UsersController@checkPhone');
    Route::post('create-profile', 'UsersController@createProfile');
    Route::post('update-bartending-information', 'UsersController@updateBartendingInformation');
    Route::post('upload-drink-video', 'UsersController@uploadDrinkVideo');
    Route::post('get-drink-videos', 'UsersController@getDrinkVideos');
    Route::post('add-leaves','UsersController@addLeaves');
    Route::post('get-leaves','UsersController@getLeaves');
    Route::post('delete-leaves','UsersController@deleteLeaves');
    Route::post('delete-video','UsersController@deleteVideo');
    Route::post('update-schedule','UsersController@updateSchedule');
    Route::post('get-schedule','UsersController@getSchedule');
    Route::post('add-group-member','UsersController@addGroupMember');
    Route::post('group-members','UsersController@listGroupMembers');
    Route::post('delete-group-member','UsersController@deleteGroupMember');
    Route::post('update-group-member','UsersController@updateGroupMember');
    Route::post('order-list','UsersController@orderList'); // only new orders
    Route::post('order-history','UsersController@orderHistory'); // All orders except new
    Route::post('accepted-order-list','UsersController@acceptedOrderList');
    Route::post('accept-order','UsersController@acceptOrder');
    Route::post('reject-order','UsersController@rejectOrder');
    Route::post('order-detail','UsersController@orderDetail');
    Route::post('notification-setting', 'UsersController@notificationSetting');
    Route::post('get-notifications', 'UsersController@getNotifications');
    Route::post('toggle-availability','UsersController@toggleAvailability');
    Route::post('get-faq-categories', 'UsersController@getFaqCategories');
    Route::post('contanct-us', 'UsersController@contanctUs');
    //Route::post('my-earnings','UsersController@myEarnings');
    Route::post('get-earnings', 'UsersController@getEarnings');
    Route::post('withdraw', 'UsersController@withdraw');
});
