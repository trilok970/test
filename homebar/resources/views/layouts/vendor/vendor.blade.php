<!DOCTYPE html>
<html lang="en">

@include('vendors.includes.head')

<body style="background: #f4f6f8;">
    @include('vendors.includes.topnavbar')

    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    @include('vendors.includes.leftbar')

                </div>
                <div class="col-lg-9 col-md-8 right">

                   
                    @include('vendors.includes.menubar')
                    
                    @yield('content')

                    


                    
                </div>
            </div>
        </div>

    </div>


    @include('vendors.includes.footer')

    <div class="site-loader" id="site-loader" style='display:none;'>
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
    </div>

    @include('vendors.includes.scripts')

    <script src="{{asset('assets/admin/notify.min.js')}}"></script>
        <?php
        $msg_type = (session()->has('success') ? 'success' : ( session()->has('error') ? 'error' : ( session()->has('warning') ? 'warning' : '')));
        $message = ''; 
        if($msg_type) {
            $message = session()->get($msg_type);
        }
        ?>
        <script>
            var msg_type = "<?= $msg_type ?>";
            var message = "<?= $message ?>";
            if(msg_type) {
                $.notify(message, msg_type);
            }
        </script>
</body>

</html>