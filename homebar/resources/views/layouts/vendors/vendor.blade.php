<!DOCTYPE html>
<html lang="en">

@include('vendors.includes.head')

<body style="background: #f4f6f8;">
    @include('vendors.includes.topnavbar')

    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <?php
                        $url_params = request()->route()->parameters;
                        $type = isset($url_params['type']) ? $url_params['type'] : '';
                        $btypes = explode(",",auth()->user()->business_type);
                    ?>
                    <div class="row btns dash-btn dash-btn-mobile">
                        <?php
                        foreach ($types as $key => $value) {
                            $active = ($type == $value->slug)? 'active' : '';
                        ?>
                        @if(in_array($value->id,$btypes))
                        <div class="col-lg-3 col-sm-3 col-6 mb-sm-0 mb-4">
                            <a class="<?= $active ?>" href="<?= route('vendor.orders', $value->slug) ?>"><?= $value->name; ?></a>
                        </div>
                        @else
                        <div class="col-lg-3 col-sm-3 col-6 mb-sm-0 mb-4">
                            <a class="<?= $active ?>" href="<?= route('vendor.category-not-found') ?>"><?= $value->name; ?></a>
                        </div>
                        @endif
                        <?php
                        }
                        ?>
                    </div>

                    @include('vendors.includes.leftbar')

                </div>
                <div class="col-lg-9 col-md-8 right right-block-sec">

                    @include('vendors.includes.menubar')

                    @yield('content')

                </div>
            </div>
        </div>

    </div>


    @include('vendors.includes.footer')

    <div class="site-loader" id="site-loader" style='display:none;'>
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
    </div>

    @include('vendors.includes.scripts')

    <script src="{{asset('assets/admin/notify.min.js')}}"></script>
        <?php
        $msg_type = (session()->has('success') ? 'success' : ( session()->has('error') ? 'error' : ( session()->has('warning') ? 'warning' : '')));
        $message = '';
        if($msg_type) {
            $message = session()->get($msg_type);
        }
        ?>
        <script>
            var msg_type = "<?= $msg_type ?>";
            var message = "<?= $message ?>";
            if(msg_type) {
                $.notify(message, msg_type);
            }
        </script>

        <script type="text/javascript">
            $(document).ready(function(){
                $('.left-side-btn').click(function(e){
                    e.stopPropagation();
                     $('.mobile-left-side').toggleClass('show-menu');
                });
                $('.mobile-left-side').click(function(e){
                    e.stopPropagation();
                });
                $('body,html').click(function(e){
                       $('.mobile-left-side').removeClass('show-menu');
                });
            });
        </script>



        @stack('scripts')
</body>
</html>
