@extends('layouts.admin.admin')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Information</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">



                <div class="box-body box-profile">

                    <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <tbody>

                            <tr>
                                <th>Name:</th>
                                <td><?= isset($entity->user->first_name) ? $entity->user->first_name.' '. $entity->user->last_name : '' ?></td>
                            </tr>

                            <tr>
                                <th>Email:</th>
                                <td><?= isset($entity->user->email) ? $entity->user->email : '' ?></td>
                            </tr>

                            <tr>
                                <th>Phone:</th>
                                <td><?= isset($entity->user->phone) ? '+'.$entity->user->countryData->phonecode .' '.$entity->user->phone : '' ?></td>
                            </tr>

                            <tr>
                                <th>FAQ Category:</th>
                                <td><?= isset($entity->faqCategory->name) ? $entity->faqCategory->name : '' ?></td>
                            </tr>

                            <tr>
                                <th>Description:</th>
                                <td><?= $entity->message ?></td>
                            </tr>
                            @if($entity->image)
                             <tr>
                                <th>Image:</th>
                                <td><a href="{{ $entity->image }}" target="_blank" ><img src="{{ $entity->image }}" width="50" /></a></td>
                            </tr>
                            @endif


                        </tbody>
                    </table>
<!-- /.row -->
</section>
<!-- /.content -->
@endsection


