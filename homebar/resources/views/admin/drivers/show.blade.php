@extends('layouts.admin.admin')
@section('content')
<?php
$file_save_path = Config::get('params.file_save_path');
$admin_default_image = Config::get('params.admin_default_image');
$file_save_path = Config::get('params.file_save_path');

$profile_pic = !empty($entity->profile_image_full) ? $entity->profile_image_full: asset($admin_default_image);
$license_image = !empty($entity->vehicleInfo->license_image) ? $entity->vehicleInfo->license_image: asset($admin_default_image);
$vehicle_image = !empty($entity->vehicleInfo->vehicle_image) ? $entity->vehicleInfo->vehicle_image: asset($admin_default_image);
$insurance_image = !empty($entity->vehicleInfo->insurance_image) ? $entity->vehicleInfo->insurance_image: asset($admin_default_image);
$role_names = Config::get('params.role_names')
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Information</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">



                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{ asset($profile_pic) }}" alt="User profile picture">

                    <h3 class="profile-username text-center">@if(!empty($entity->full_name)) {{ucwords($entity->full_name)}} @else {{ucfirst($entity->first_name).' '.ucfirst($entity->last_name)}} @endif</h3>

                    <p class="text-muted text-center"><?= $entity->designation ?></p>
                    <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <tbody>
                            {{-- <tr>
                                <th>Address:</th>
                                <td>{{ $entity->address }}</td>
                                <th>City:</th>
                                <td>{{ $entity->city }}</td>
                            </tr>

                            <tr>
                                <th>State:</th>
                                <td>{{ $entity->state }}</td>
                                <th>Zipcode:</th>
                                <td>{{ $entity->zipcode }}</td>
                            </tr> --}}

                           <!--  <tr>
                                <th>Primary Mobile Number:</th>
                                <td>{{ phoneNumberFormat($entity->primary_mobile_number) }}</td>
                                <th>Secondary Mobile Number:</th>
                                <td>{{ phoneNumberFormat($entity->secondary_mobile_number) }}</td>
                            </tr> -->
                             <tr>
                                <th>Phone Number:</th>
                                <td>{{ phoneNumberFormat($entity->phone_country_code) }}</td>
                                <!-- <th>Secondary Mobile Number:</th>
                                <td>{{ phoneNumberFormat($entity->secondary_mobile_number) }}</td> -->
                            </tr>

                            {{-- <tr>
                                <th>Open Time:</th>
                                <td>{{ $entity->open_time }}</td>
                                <th>Close Time:</th>
                                <td>{{ $entity->close_time }}</td>
                            </tr> --}}

                            <tr>
                                <th>Email:</th>
                                <td>{{ $entity->email }}</td>
                                <th>Role:</th>
                                <td>{{ isset($role_names[$entity->role]) ? $role_names[$entity->role] : '' }}</td>
                            </tr>

                            <tr>
                                <th>Created:</th>
                                <td> {{ listDateFromat($entity->created_at) }} </td>
                                <th>Last Updated:</th>
                                <td> {{ listDateFromat($entity->updated_at) }} </td>
                            </tr>

                            <tr>
                                <th>Status</th>
                                <td> <?= getStatus($entity->status,$entity->id) ?> </td>
                                 @if($entity->status==0)
                                <th class="reason_for_deactivation">Reason For Deactivation</th>
                                <td class="reason_for_deactivation" id="reason_for_deactivation"> {{ $entity->reason_for_deactivation }} </td>
                                @endif
                            </tr>
                        </tbody>
                    </table>
            <div class="box-header with-border">
            <h3 class="box-title"><b>Vehicle Information</b></h3>
        </div>
                     <table class="table table-bordered table-hover">
                        <tbody>
                            <tr>
                                <th>License Number:</th>
                                <td>{{ $entity->vehicleInfo->license_number }}</td>
                                <th>License Expiry Date:</th>
                                <td>{{ listDateFromat($entity->vehicleInfo->license_expiry_date) }}</td>
                            </tr>

                            <tr>
                                <th>License Image:</th>
                                <td>
                                    <a data-toggle="tooltip" title="Download License Image" href="{{ asset($entity->vehicleInfo->license_image) }}" target="_blank">
                                        <img src="{{ asset($entity->vehicleInfo->license_image) }}" width="100px">
                                    </a>
                                </td>
                                <th>Vehicle Number:</th>
                                <td>{{ $entity->vehicleInfo->vehicle_number }}</td>
                            </tr>


                             <tr>
                                <th>Vehicle Make:</th>
                                <td>{{ $entity->vehicleInfo->vehicle_make }}</td>
                                <th>Vehicle Model:</th>
                                <td>{{ $entity->vehicleInfo->vehicle_model }}</td>
                            </tr>

                            <tr>
                                <th>Vehicle Color:</th>
                                <td>{{ $entity->vehicleInfo->vehicle_color }}</td>
                                <th>Vehicle Image:</th>
                                <td>
                                    <a data-toggle="tooltip" title="Download Vehicle Image" href="{{ asset($entity->vehicleInfo->vehicle_image) }}" target="_blank">
                                        <img src="{{ asset($entity->vehicleInfo->vehicle_image) }}" width="100px">
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <th>Insurance Image:</th>
                                <td>
                                    <a data-toggle="tooltip" title="Download Insurance Image" href="{{ asset($entity->vehicleInfo->insurance_image) }}" target="_blank">
                                        <img src="{{ asset($entity->vehicleInfo->insurance_image) }}" width="100px">
                                    </a>
                                </td>
                                <th>Insurance Expiry Date:</th>
                                <td>{{ listDateFromat($entity->vehicleInfo->insurance_expiry_date)  }}</td>
                            </tr>
                            <tr>
                                <th>Social Security Number:</th>
                                <td>{{ $entity->vehicleInfo->social_security_number }}</td>

                            </tr>

                        </tbody>
                    </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
<!-- /.row -->
</section>
<!-- /.content -->
@endsection
@section('uniquepagescript')
<script>
    $(".icon_info").tooltip();

  function changeStatus(id,status) {
        var newStatus = 'activate';

        if(status == 1){
            var newStatus = 'deactivate';
        }
    swal({
    title: "Do you really want to "+newStatus+" this driver?",
            type: "warning",
            showCancelButton: "No",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
    text: "Write something here:",
  type: "input",
  showCancelButton: true,
  closeOnConfirm: false,
  animation: "slide-from-top",
  inputPlaceholder: "Write something"
    },
    function(inputValue){

        if (inputValue === false) return false;

        if (inputValue === "") {
            swal.showInputError("You need to write something!");
            return false
        }
        if(inputValue)
        {
            jQuery.ajax({
            url: '{{route('admin.users.statusUpdate')}}',
                type: 'POST',
                data:{id:id,reason_for_deactivation:inputValue},
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                $("#status_" + id).html(response);
                if(status == 1){
                    $(".reason_for_deactivation").show();
                    $("#reason_for_deactivation").html(inputValue);
                }
                else
                    $(".reason_for_deactivation").hide();

                }
        });
            swal("Nice!", "You wrote: " + inputValue, "success");
        }

    }
    );
}

</script>




@endsection
