<?php
// $fields = Config::get('lang.plans');
// $fields_html = getFields($fields, $entity);
$status_name_arr = Config::get('params.status_name_arr');
?>
@extends('layouts.admin.admin')
@section('content')

<?php

if(empty($entity->id)) {
    $action_route = ['plans.store'];
    $method = 'POST';
} else {
    $action_route = ['plans.update', [$entity->id]];
    $method = 'PATCH';
}
$plan_types = Config::get('params.plan_types');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Information</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">

                {!! Form::model($entity, ['route' => $action_route, 'method' => $method, 'class'=>'form-horizontal validate', 'enctype'=>'multipart/form-data']) !!}
                {{ csrf_field() }}
                <div class="form-group ">

                    <div class="col-md-6 <?= $errors->has('name') ? 'has-error' : '' ?>">
                        <label class="control-label" for="name">Name</label>
                        <?= Form::text('name', null, ['placeholder'=>'Name','class'=>'form-control', 'required'=>true])?>
                        <span class="help-block"><?= $errors->has('name') ? $errors->first('name') : '' ?></span>
                    </div>

                    <div class="col-md-6 <?= $errors->has('status') ? 'has-error' : '' ?>">
                        <label class="control-label" for="status">Status</label>
                        <?= Form::select('status', $status_name_arr, null, ['placeholder'=>'Select Status','class'=>'form-control', 'required'=>true])?>
                        <span class="help-block"><?= $errors->has('status') ? $errors->first('status') : '' ?></span>

                    </div><div class="col-md-6 <?= $errors->has('type') ? 'has-error' : '' ?>">
                        <label class="control-label" for="type">Type</label>
                        <?= Form::select('type', $plan_types, null, ['placeholder'=>'Select Plan','class'=>'form-control', 'required'=>true])?>
                        <span class="help-block"><?= $errors->has('type') ? $errors->first('status') : '' ?></span>
                    </div>

                    <div class="col-md-6 <?= $errors->has('price') ? 'has-error' : '' ?>">
                        <label class="control-label" for="name">Price</label>
                        <?= Form::number('price', null, ['placeholder'=>'Price','class'=>'form-control', 'required'=>true, 'step'=>".01"])?>
                        <span class="help-block"><?= $errors->has('price') ? $errors->first('price') : '' ?></span>
                    </div>

                    
                </div>

                <div class="text-right">
                    <a href="{!! route('plans.index') !!}" class="btn btn-default"> Cancel </a>
                    <?= Form::submit('Submit', ['class' => 'btn btn-primary ']) ?>

                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>






    <!-- /.row -->
</section>
<!-- /.content -->
@endsection



@section('uniquepagestyle')
{{-- <link rel="stylesheet" href="{{asset('assets/admin/bootstrap-multiselect.css')}}" /> --}}
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<style>
.note_info {
	font-size: 10px;
	color: red;
}

.col-md-6 {
	min-height: 72px;
}


.btn-group {
	display: block;
	width: 100% !important;
	text-align: left !important;
	margin-top: 2px;
}

.select2-container--default .select2-selection--multiple .select2-selection__choice {
    color: #020202;
}
/* .multiselect.dropdown-toggle.btn.btn-default {
	text-align: left;
} */
</style>
@endsection

@section('uniquepagescript')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function(){
        $('.select_check').select2({
            'placeholder':'Select Brand'
        });
    });
</script>
{{-- <script src="{{asset('assets/admin/bootstrap-multiselect.js')}}"></script> --}}
{{-- <script>
$('.select_check').multiselect({
  nonSelectedText: 'Select',
  enableFiltering: false,
  //enableCaseInsensitiveFiltering: true,
  buttonWidth:'100%'
 });
</script> --}}
@endsection
