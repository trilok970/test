@extends('layouts.admin.admin')
@section('content')
<?php
$file_save_path = Config::get('params.file_save_path');
$admin_default_image = Config::get('params.admin_default_image');
$file_save_path = Config::get('params.file_save_path');

$profile_pic = !empty($entity->profile_image_full) ? $entity->profile_image_full: asset($admin_default_image);
$role_names = Config::get('params.role_names')
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Information</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">



                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{ asset($profile_pic) }}" alt="User profile picture">

                    <h3 class="profile-username text-center"><?= $entity->full_name ?></h3>

                    <p class="text-muted text-center"><?= $entity->designation ?></p>
                    <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <tbody>

                            <tr>
                                <th>First Name:</th>
                                <td>{{ $entity->first_name }}</td>
                                <th>Last Name:</th>
                                <td>{{ $entity->last_name }}</td>
                            </tr>

                            <tr>
                                <th>Email:</th>
                                <td>{{ $entity->email }}</td>
                                <th>Role:</th>
                                <td>{{ isset($role_names[$entity->role]) ? $role_names[$entity->role] : '' }}</td>
                            </tr>
                            <tr>
                                <th>Phone No.:</th>
                                <td>{{ phoneNumberFormat($entity->phone) }}</td>
                                <th>DOB:</th>
                                <td>{{ listDateFromat($entity->dob) }}</td>
                            </tr>

                            <tr>
                                <th>Created:</th>
                                <td> {{ listDateFromat($entity->created_at) }} </td>
                                <th>Updated:</th>
                                <td> {{ listDateFromat($entity->updated_at) }} </td>
                            </tr>

                            <tr>
                                <th>Status:</th>
                                <td> <?= getStatus($entity->status,$entity->id) ?> </td>
                                 @if($entity->status==0)
                                <th class="reason_for_deactivation">Reason For Deactivation</th>
                                <td class="reason_for_deactivation" id="reason_for_deactivation"> {{ $entity->reason_for_deactivation }} </td>
                                @endif
                            </tr>
                        </tbody>
                    </table>
<div class="box-header with-border">
            <h3 class="box-title"><b>Customer Orders</b></h3>
        </div>
                    <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                      <th>ID</th>
                      <th>Order#</th>
                      <th>User</th>
                      <th>Total Amount</th>
                      <th>Order Type</th>
                      <th>Delivery Type</th>
                      <th>Delivery Date</th>
                      <th>Created</th>
                      <th>Status</th>
                      <th>Action</th>
                  </tr>
                  </thead>

                </table>

                    </div>
                </div>

            </div>
        </div>
    </div>
<!-- /.row -->
</section>
<!-- /.content -->
@endsection





@section('uniquepagestyle')
<link rel="stylesheet" href="{{asset('assets/admin/dataTables.bootstrap.min.css')}}">
@endsection
@section('uniquepagescript')
<script>
    $(".icon_info").tooltip();

   function changeStatus(id,status) {
        var newStatus = 'activate';

        if(status == 1){
            var newStatus = 'deactivate';
        }
    swal({
    title: "Do you really want to "+newStatus+" this customer?",
            type: "warning",
            showCancelButton: "No",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
    text: "Write something here:",
  type: "input",
  showCancelButton: true,
  closeOnConfirm: false,
  animation: "slide-from-top",
  inputPlaceholder: "Write something"
    },
    function(inputValue){

        if (inputValue === false) return false;

        if (inputValue === "") {
            swal.showInputError("You need to write something!");
            return false
        }
        if(inputValue)
        {
            jQuery.ajax({
            url: '{{route('admin.users.statusUpdate')}}',
                type: 'POST',
                data:{id:id,reason_for_deactivation:inputValue},
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                $("#status_" + id).html(response);
                if(status == 1){
                    $(".reason_for_deactivation").show();
                    $("#reason_for_deactivation").html(inputValue);
                }
                else
                    $(".reason_for_deactivation").hide();

                }
        });
            swal("Nice!", "You wrote: " + inputValue, "success");
        }

    }
    );
}

</script>
<script src="{{asset('assets/admin/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/admin/js/dataTables.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        var user_id = "{{$entity->id}}";
        var table = $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        pageLength: getAdminPageLength(),
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('admin.orders.datatable') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}",user_id:user_id}
        },
        columns: [
        { data: 'id', name: 'id', orderable:true, "visible": true },
        { data: 'order_number',name:'order_number',orderable:true},
        { data: 'user',name:'user',orderable:true},
        { data: 'total_amount', name: 'total_amount', orderable:true },
        { data: 'order_type', name: 'order_type', orderable:true },
        { data: 'reach_type', name: 'reach_type', orderable:true  },
        { data: 'delivery_date', name: 'delivery_date', orderable:true },
        { data: 'created_at', name: 'created_at', orderable:true },
        { data: 'status', name: 'status', orderable:false},
        { data: 'action', name: 'action', orderable:false},
        ],
});
});
</script>
@endsection
