@extends('layouts.admin.admin')
@section('content')
<?php
$file_save_path = Config::get('params.file_save_path');
$admin_default_image = Config::get('params.admin_default_image');
$file_save_path = Config::get('params.file_save_path');

$profile_pic = !empty($entity->profile_image_full) ? $entity->profile_image_full: asset($admin_default_image);
$role_names = Config::get('params.role_names')
?>



<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Information</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{ asset($profile_pic) }}" alt="User profile picture">

                    <h3 class="profile-username text-center">@if(!empty($entity->full_name)){{ucwords($entity->full_name)}}@else{{ucfirst($entity->first_name).' '.ucfirst($entity->last_name)}}@endif</h3>

                    <p class="text-muted text-center"><?= $entity->designation ?></p>
                    <div class="table-responsive">

                        <table class="table table-bordered table-hover">
                            <tbody>
                                <tr>
                                    <th>Phone:</th>
                                    <td>{{ phoneNumberFormat($entity->phone) }}</td>
                                    <th>Years of Experience:</th>
                                    <td>{{ $entity->experience }}</td>
                                </tr>

                                <tr>
                                    <th>Address:</th>
                                    <td>{{ $entity->address }}</td>
                                <th>Signature Drink:</th>
                                <td>{{ $entity->signature_drink }}</td>
                            </tr>

                            <tr>
                                <th>Price Per Hour:</th>
                                <td>{{ $entity->price_per_hour }}</td>
                                <th>Service Address Radius:</th>
                                <td>{{ $entity->service_address_radius }}</td>
                            </tr>

                            <tr>
                                <th>Role:</th>
                                <td>{{ isset($role_names[$entity->role]) ? $role_names[$entity->role] : '' }}</td>
                            </tr>

                            <tr>
                                <th>Created:</th>
                                <td> {{ listDateFromat($entity->created_at) }} </td>
                                <th>Last Updated:</th>
                                <td> {{ listDateFromat($entity->updated_at) }} </td>
                            </tr>
                            <tr>
                                <th>Bio:</th>
                                <td colspan="3">{{ $entity->bio }}</td>

                            </tr>


                            <tr>
                                <th>Availability</th>
                                <td>{{$entity->availability?"Available":'Not Available'}}</td>
                                <th>Email:</th>
                                <td> {{ ($entity->email) }} </td>
                            </tr>
                             <tr>
                                <th>Status</th>
                                <td> <?= getStatus($entity->status,$entity->id) ?> </td>

                                 @if($entity->status==0)
                                <th class="reason_for_deactivation">Reason For Deactivation</th>
                                <td class="reason_for_deactivation" id="reason_for_deactivation"> {{ $entity->reason_for_deactivation }} </td>
                                @endif
                            </tr>
                            <tr>
                                <th>Rating</th>
                                <td> <?= $entity->rating ?> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>


    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Documents</h3>
        </div>
         <div class="box-body">
            <div class="col-md-12">

                        <div class="col-md-3">
                            <label>Bartender DL/ID</label>
                        </div>

                        <div class="col-md-3">
                            <label>
                                @if($entity->dl_photo)
                                <a data-toggle="tooltip" title="Download DL/ID" href="{{ asset($entity->dl_photo) }}" target="_blank">
                                    <img src="{{asset($entity->dl_photo)}}" width="100px">
                                </a>
                                @endif
                            </label>
                        </div>
                        <div class="col-md-3">
                            <label> BARTENDING LICENSE</label>
                        </div>

                        <div class="col-md-3">
                            <label>
                                @if($entity->bl_photo)
                                    <a data-toggle="tooltip" title="BARTENDING LICENSE" href="{{ asset($entity->bl_photo) }}" target="_blank">
                                        <img src="{{asset($entity->bl_photo)}}" width="100px">
                                    </a>
                                @endif
                            </label>
                        </div>
            </div>
        </div>
    </div>


    @if(count($drinkVideos)>0)
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Videos</h3>
        </div>
         <div class="box-body">
            <div class="col-md-12">
                    @foreach ($drinkVideos as $key => $value)
                        <div class="col-md-4">
                            <iframe src="{{asset($value->video)}}" frameborder="0" width="320px"></iframe>
                        </div>
                    @endforeach
            </div>
        </div>
    </div>
    @endif


    @if(count($bartenderAvailability)>0)
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Bartender Availability</h3>
        </div>
         <div class="box-body">
            <div class="col-md-12">
                <table class="table table-bordered table-hover">
                    @foreach ($bartenderAvailability as $key => $value)
                        <tr>
                            <td>
                                @if ($value->weekday==1) {{"Sunday"}}
                                @elseif ($value->weekday==2) {{"Monday"}}
                                @elseif ($value->weekday==3) {{"Tuesday"}}
                                @elseif ($value->weekday==4) {{"Wednesday"}}
                                @elseif ($value->weekday==5) {{"Thursday"}}
                                @elseif ($value->weekday==6) {{"Friday"}};
                                @else {{"Saturday"}}
                                @endif
                            </td>
                            <td>{{$value->time_from}}</td>
                            <td>{{$value->time_to}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
   @endif

   @if(count($bartenderLeaves) > 0)
   <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Bartender Leaves</h3>
        </div>
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>S.No.</th>
                    <th>Date</th>
                    <th>Created</th>
                </tr>
            </thead>
            @foreach ($bartenderLeaves as $leave)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$leave->date}}</td>
                <td>{{$leave->created_at}}</td>
            </tr>
            @endforeach
        </table>
   </div>
   @endif

    @if(count($bartenderGroupMembers)>0)
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Group Members</h3>
        </div>

         <div class="box-body">
            <div class="col-md-12">
                <table class="table table-bordered table-hover">
                    @foreach ($bartenderGroupMembers as $key => $value)
                       <tr>
                            <th>
                                Name
                            </th>
                            <td>
                                <?php echo $value->name; ?>
                            </td>
                            <th>
                                Mobile Number
                            </th>
                            <td>
                                <?php echo $value->mobile_number; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Email
                            </th>
                            <td>
                                <?php echo $value->email; ?>
                            </td>
                            <th>
                                Experience
                            </th>
                            <td>
                                <?php echo $value->experience; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Signature Drink
                            </th>
                            <td>
                                <?php echo $value->signature_drink; ?>
                            </td>
                            <th>
                                Photo
                            </th>
                            <td>
                                <img src="<?php echo $value->photo; ?>"  height="150">
                            </td>
                        </tr>
                    @endforeach
                    </table>
            </div>
        </div>
    </div>
    @endif


<!-- /.row -->
</section>
<!-- /.content -->
@endsection
@section('uniquepagescript')
<script>
    $(".icon_info").tooltip();

    function changeStatus(id,status) {
        var newStatus = 'activate';

        if(status == 1){
            var newStatus = 'deactivate';
        }
    swal({
    title: "Do you really want to "+newStatus+" this bartender?",
            type: "warning",
            showCancelButton: "No",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
    text: "Write something here:",
  type: "input",
  showCancelButton: true,
  closeOnConfirm: false,
  animation: "slide-from-top",
  inputPlaceholder: "Write something"
    },
    function(inputValue){

        if (inputValue === false) return false;

        if (inputValue === "") {
            swal.showInputError("You need to write something!");
            return false
        }
        if(inputValue)
        {
            jQuery.ajax({
            url: '{{route('admin.users.statusUpdate')}}',
                type: 'POST',
                data:{id:id,reason_for_deactivation:inputValue},
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                $("#status_" + id).html(response);
                if(status == 1){
                    $(".reason_for_deactivation").show();
                    $("#reason_for_deactivation").html(inputValue);
                }
                else
                    $(".reason_for_deactivation").hide();

                }
        });
            swal("Nice!", "You wrote: " + inputValue, "success");
        }

    }
    );
}

</script>




@endsection
