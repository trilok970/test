@extends('layouts.admin.admin')
@section('content')


<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Manage Delivery Charges</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <form action="" method="post">
                            @csrf
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Time From</th>
                                        <th>Time To</th>
                                        <th>Delivery Charges</th>
                                        <th>Is High Volume Time?</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($deliveryCharges as $row)
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <input type="time" class="form-control" name="time_from[]" value="{{$row->time_from}}" readonly>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <input type="time" class="form-control" name="time_to[]" value="{{$row->time_to}}" readonly>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <input type="text" class="form-control" name="delivery_charges[]" value="{{$row->delivery_charges}}">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <select name="is_high_volume[]" class="form-control">
                                                        <option value="0" @if(!$row->is_high_volume_time) {{'selected'}} @endif>No</option>
                                                        <option value="1" @if($row->is_high_volume_time) {{'selected'}} @endif>Yes</option>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="form-group">
                                <label for="high_volume_price">High Volume Price</label>
                            <input type="text" class="form-control" name="high_volume_price" id="high_volume_price" value="@if($setting){{$setting->value}}@else{{'0'}}@endif">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->


        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection
