@extends('layouts.admin.admin')
@section('content')

<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <!-- <h3 class="box-title">Information</h3> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">
                <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tbody>
                        <tr>
                            <th>Title:</th>
                            <td>{{ $entity->coupon_title }}</td>
                        </tr>

                        <tr>
                            <th>Coupon Type:</th>
                            <td>{{ $entity->coupon_type }}</td>
                        </tr>

                        <tr>
                            <th>Coupon:</th>
                            <td>{{ $entity->coupon_code }}</td>
                        </tr>

                        <tr>
                            <th>Coupon Value:</th>
                            <td>{{ $entity->coupon_value }}</td>
                        </tr>

                        <tr>
                            <th>Expiry Date:</th>
                            <td>{{ $entity->expiry_date }}</td>
                        </tr>


                        <tr>
                            <th>Created:</th>
                            <td> {{ listDateFromat($entity->created_at) }} </td>

                        </tr>


                        <tr>
                            <th>Status</th>
                            <td> <?= getStatus($entity->status,$entity->id) ?> </td>
                        </tr>

                    </tbody>

                </table>
                </div>

            </div>
        </div>
    </div>

</section>

@endsection
