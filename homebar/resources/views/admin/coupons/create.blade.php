<?php
$status_name_arr = Config::get('params.status_name_arr');
?>

@extends('layouts.admin.admin')
@section('content')

<?php

//dd($entity);

if(empty($entity)) {
    $action_route = ['admin.coupons.store'];
    $method = 'POST';
    $current_cat = null;
} else {
    $action_route = ['admin.coupons.update', [$entity->id]];
    $method = 'POST';
    $current_cat_arr = [];
    $current_cat = [];
}
?>

<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <!-- <h3 class="box-title">Information</h3> -->
        </div>
        <div class="box-body">
            <div class="col-md-12">
                {!! Form::model($entity, ['route' => $action_route, 'method' => $method, 'class'=>'form-horizontal validate', 'enctype'=>'multipart/form-data']) !!}
                {{ csrf_field() }}
                <div class="form-group ">

                    <div class="col-md-12 <?= $errors->has('coupon_title') ? 'has-error' : '' ?>">
                        <label class="control-label" for="coupon_title">Coupon Title</label>
                        <?= Form::text('coupon_title', null, ['placeholder'=>'Title','class'=>'form-control', 'required'=>true])?>
                        <span class="help-block"><?= $errors->has('coupon_title') ? $errors->first('coupon_title') : '' ?></span>
                    </div>

                    <div class="col-md-12 <?= $errors->has('coupon_type') ? 'has-error' : '' ?> type_div">
                        <label class="control-label" for="coupon_type">Coupon Type</label>
                        <?= Form::select('coupon_type', $coupon_type, null, ['placeholder'=>'Select type','class'=>'form-control', 'required'=>true])?>
                        <span class="help-block"><?= $errors->has('coupon_type') ? $errors->first('coupon_type') : '' ?></span>
                    </div>

                    <div class="col-md-12 <?= $errors->has('coupon_code') ? 'has-error' : '' ?>">
                        <label class="control-label" for="coupon_code">Coupon Code</label>
                        <?= Form::text('coupon_code', null, ['placeholder'=>'Coupon Code','class'=>'form-control', 'required'=>true])?>
                        <span class="help-block"><?= $errors->has('coupon_code') ? $errors->first('coupon_code') : '' ?></span>
                    </div>

                    <div class="col-md-12 <?= $errors->has('coupon_value') ? 'has-error' : '' ?>">
                        <label class="control-label" for="coupon_value">Coupon Value</label>
                        <?= Form::text('coupon_value', null, ['placeholder'=>'Coupon Value','class'=>'form-control', 'required'=>true])?>
                        <span class="help-block"><?= $errors->has('coupon_value') ? $errors->first('coupon_value') : '' ?></span>
                    </div>                

                    <div class="col-md-12 <?= $errors->has('expiry_date') ? 'has-error' : '' ?>">
                        <label class="control-label" for="expiry_date">Expiry Date</label>
                        <?= Form::text('expiry_date', null, ['placeholder'=>'Expiry Date','class'=>'form-control datepicker', 'required'=>true, 'autocomplete ' => 'off'])?>
                        <span class="help-block"><?= $errors->has('expiry_date') ? $errors->first('expiry_date') : '' ?></span>
                    </div>



                    <div class="text-right">
                        <a href="{!! route('admin.coupons.index') !!}" class="btn btn-default"> Cancel </a>
                        <?= Form::submit('Submit', ['class' => 'btn btn-primary ']) ?>
                    </div>

                    {{ Form::close() }}

            </div>
        </div>
    </div>

</section>

@endsection

@section('uniquepagescript')

<link rel="stylesheet" href="{{asset('public/assets/admin/bootstrap-datepicker.min.css')}}">
<script src="{{asset('public/assets/admin/bootstrap-datepicker.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker').datepicker({format:'yyyy-mm-dd'});
    });
</script>
@endsection