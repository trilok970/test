@extends('layouts.admin.admin')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Information</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">

                {!! Form::model($entity, ['route' => ['admin.pages.edit', $entity->slug], 'method' => 'POST', 'class'=>'form-horizontal validate', 'enctype'=>'multipart/form-data']) !!}
                {{ csrf_field() }}


                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label" for="title">Title<span class="required">*</span>
                        </label>
                        <div class="">
                            {!! Form::text('title', null, ['class'=>'form-control','placeholder'=>'Title', 'required'=>true, 'max'=>255]) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label" for="description">Description<span class="required">*</span>
                        </label>
                        <div class="">
                                @if($entity->slug == 'terms')
                                {!! Form::textArea('description', null, ['class'=>'form-control','id'=>'description','placeholder'=>'Description', 'required'=>true,'rows'=>'3',"onkeyup"=>"countChar(this)"]) !!}
                                <span id="charNum" style="color:red;" class="error">@if(strlen($entity->description) > 0) @php echo $count = 30 - strlen($entity->description); @endphp @else 30 @endif  </span>
                                <span class="error" style="color:red;">Character Left</span>
                                @else
                                {!! Form::textArea('description', null, ['class'=>'form-control','id'=>'description-editor','placeholder'=>'Description', 'required'=>true]) !!}
                            @endif

                        </div>
                    </div>
                </div>

                <div class="text-right">
                    <a href="{!! route('admin.pages.index') !!}" class="btn btn-default"> Cancel </a>
                    <?= Form::submit('Update', ['class' => 'btn btn-primary ']) ?>

                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>






    <!-- /.row -->
</section>
<!-- /.content -->
@endsection
@section('uniquepagescript')
<script src="{{ asset('public/assets/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    	function countChar(val) {
        var len = val.value.length;
        if (len >= 30) {
          val.value = val.value.substring(0, 30);
        } else {
          $('#charNum').text(30 - len);
        }
      }
    $(".icon_info").tooltip();
    CKEDITOR.replace( 'description-editor' );
</script>
@endsection
