@extends('layouts.admin.admin')
@section('content')

<?php
$role_ids = Config::get('params.role_ids');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
              <table id="dataTable" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Last Modified</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>

              </table>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('uniquepagestyle')
<link rel="stylesheet" href="{{asset('assets/admin/dataTables.bootstrap.min.css')}}">
@endsection

@section('uniquepagescript')
<script src="{{asset('assets/admin/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/admin/js/dataTables.bootstrap4.min.js')}}"></script>

<script>

function changeStatus(id,status) {
        var newStatus = 'activate';

        if(status == 1){
            var newStatus = 'deactivate';
        }
    swal({
    title: "Do you really want to "+newStatus+" this vendor?",
            type: "warning",
            showCancelButton: "No",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
    text: "Write something here:",
  type: "input",
  showCancelButton: true,
  closeOnConfirm: false,
  animation: "slide-from-top",
  inputPlaceholder: "Write something"
    },
    function(inputValue){

        if (inputValue === false) return false;

        if (inputValue === "") {
            swal.showInputError("You need to write something!");
            return false
        }
        if(inputValue)
        {
            jQuery.ajax({
            url: '{{route('admin.users.statusUpdate')}}',
                type: 'POST',
                data:{id:id,reason_for_deactivation:inputValue},
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                $("#status_" + id).html(response);
                if(status == 1){
                    $(".reason_for_deactivation").show();
                    $("#reason_for_deactivation").html(inputValue);
                }
                else
                    $(".reason_for_deactivation").hide();

                }
        });
            swal("Nice!", "You wrote: " + inputValue, "success");
        }

    }
    );
}
$(function() {
var table = $('#dataTable').DataTable({
processing: true,
        serverSide: true,
        pageLength: getAdminPageLength(),
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('admin.vendorsDataTable') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}"}
        },
        columns: [
        { data: 'id', name: 'id', orderable:true, "visible": false },
        // { data: 'full_name', name: 'full_name', orderable:true },
        { data: 'first_name', name: 'first_name', orderable:true },
        { data: 'last_name', name: 'last_name', orderable:true },
        { data: 'email', name: 'email', orderable:true  },
        { data: 'created_at', name: 'created_at', orderable:true },
        { data: 'status', name: 'status', orderable:false},
        { data: 'action', name: 'action', orderable:false},
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search"
        },
});
});
</script>

@endsection
