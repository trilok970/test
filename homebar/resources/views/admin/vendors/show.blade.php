@extends('layouts.admin.admin')
@section('content')
<?php
$file_save_path = Config::get('params.file_save_path');
$admin_default_image = Config::get('params.admin_default_image');
$file_save_path = Config::get('params.file_save_path');

$profile_pic = !empty($entity->profile_image_full) ? $entity->profile_image_full: asset($admin_default_image);
$role_names = Config::get('params.role_names')
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Information</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">



                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{ asset($profile_pic) }}" alt="User profile picture">

                    <h3 class="profile-username text-center"><?= $entity->full_name ?></h3>

                    <p class="text-muted text-center"><?= $entity->designation ?></p>
                    <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <tbody>
                            <tr>
                                <th>Business Name:</th>
                                <td>{{ $entity->business_name }}</td>
                                <th>Business Type:</th>
                                <td>{{ $btypes }}</td>
                            </tr>

                            <tr>
                                <th>Address:</th>
                                <td>{{ $entity->address }}</td>
                                <th>City:</th>
                                <td>{{ $entity->city }}</td>
                            </tr>

                            <tr>
                                <th>State:</th>
                                <td>{{ $entity->state }}</td>
                                <th>Zipcode:</th>
                                <td>{{ $entity->zipcode }}</td>
                            </tr>

                            <tr>
                                <th>Primary Mobile Number:</th>
                                <td>{{ phoneNumberFormat($entity->primary_mobile_number) }}</td>
                                <th>Secondary Mobile Number:</th>
                                <td>{{ phoneNumberFormat($entity->secondary_mobile_number) }}</td>
                            </tr>

                            <tr>
                                <th>Open Time:</th>
                                <td>{{ listTimeFromat($entity->open_time) }}</td>
                                <th>Close Time:</th>
                                <td>{{ listTimeFromat($entity->close_time) }}</td>
                            </tr>

                            <tr>
                                <th>Email:</th>
                                <td>{{ $entity->email }}</td>
                                <th>Role:</th>
                                <td>{{ isset($role_names[$entity->role]) ? $role_names[$entity->role] : '' }}</td>
                            </tr>

                            <tr>
                                <th>Created:</th>
                                <td> {{ listDateFromat($entity->created_at) }} </td>
                                <th>Last Updated:</th>
                                <td> {{ listDateFromat($entity->updated_at) }} </td>
                            </tr>

                            <tr>
                                <th>Status</th>
                                <td> <?= getStatus($entity->status,$entity->id) ?> </td>
                                 @if($entity->status==0)
                                <th class="reason_for_deactivation">Reason For Deactivation</th>
                                <td class="reason_for_deactivation" id="reason_for_deactivation"> {{ $entity->reason_for_deactivation }} </td>
                                @endif
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <?php if(count($bartenders)>0) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Associated bartenders</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">


                <?php foreach ($bartenders as $key => $value) { ?>
                <div class="col-md-6">

                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="<?php echo $value->photo; ?>" alt="User profile picture">


                        <h3 class="profile-username text-center"><?= $value->first_name.' '.$value->last_name ?></h3>


                        <table class="table table-bordered table-hover">
                            <tbody>
                                <tr>
                                    <th>Primary Mobile Number:</th>
                                    <td>{{ $value->primary_mobile_number }}</td>
                                    <th>Secondary Mobile Number :</th>
                                    <td>{{ $value->secondary_mobile_number }}</td>
                                </tr>

                                <tr>
                                    <th>Address:</th>
                                    <td>{{ $value->address }}</td>
                                    <th>Email:</th>
                                    <td>{{ $value->email }}</td>
                                </tr>

                                <tr>
                                    <th>Date of Birth:</th>
                                    <td>{{ $value->dob }}</td>
                                    <th>Experience (years):</th>
                                    <td>{{ $value->experience }}</td>
                                </tr>

                                <tr>
                                    <th>Signature Drink:</th>
                                    <td>{{ $value->signature_drink }}</td>
                                    <th>Price Per Hour:</th>
                                    <td>{{ $value->price_per_hour }}</td>
                                </tr>

                                <tr>
                                    <th>RADIUS OF PROVIDES THE SERVICE ( in Miles ):</th>
                                    <td>{{ $value->service_address_radius }}</td>
                                    <th>Bio:</th>
                                    <td>{{ $value->bio }}</td>
                                </tr>

                                <tr>
                                    <th>DL Number:</th>
                                    <td>{{ $value->dl_number }}</td>
                                    <th>PRICE ( per Hour for one bartender ):</th>
                                    <td>{{ $value->price }}</td>
                                </tr>

                                <tr>
                                    <th>Bartending License Number:</th>
                                    <td> {{ $value->bartending_license_number }} </td>
                                    <th>Bartending License Expirations:</th>
                                    <td> {{ $value->bartending_license_expirations }} </td>
                                </tr>


                            </tbody>
                        </table>
                    </div>

                </div>
                <?php } ?>
        </div>
    </div>
    <?php } ?>


<!-- /.row -->
</section>
<!-- /.content -->
@endsection
@section('uniquepagescript')
<script>
    $(".icon_info").tooltip();

    function changeStatus(id,status) {
        var newStatus = 'activate';

        if(status == 1){
            var newStatus = 'deactivate';
        }
    swal({
    title: "Do you really want to "+newStatus+" this vendor?",
            type: "warning",
            showCancelButton: "No",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
    text: "Write something here:",
  type: "input",
  showCancelButton: true,
  closeOnConfirm: false,
  animation: "slide-from-top",
  inputPlaceholder: "Write something"
    },
    function(inputValue){

        if (inputValue === false) return false;

        if (inputValue === "") {
            swal.showInputError("You need to write something!");
            return false
        }
        if(inputValue)
        {
            jQuery.ajax({
            url: '{{route('admin.users.statusUpdate')}}',
                type: 'POST',
                data:{id:id,reason_for_deactivation:inputValue},
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                $("#status_" + id).html(response);
                if(status == 1){
                    $(".reason_for_deactivation").show();
                    $("#reason_for_deactivation").html(inputValue);
                }
                else
                    $(".reason_for_deactivation").hide();

                }
        });
            swal("Nice!", "You wrote: " + inputValue, "success");
        }

    }
    );
}

</script>




@endsection
