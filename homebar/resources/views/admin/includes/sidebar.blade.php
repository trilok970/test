<?php
$user = auth()->user();
$route = Route::currentRouteAction();
$explode_route = explode('\\', $route);
$controller_ation = end($explode_route);
$controller_ation_ex = explode('@', $controller_ation);
$controller = isset($controller_ation_ex[0]) ? $controller_ation_ex[0] : '';
$action = isset($controller_ation_ex[1]) ? $controller_ation_ex[1] : '';
$user_full_name = $user->full_name;
$file_save_path = Config::get('params.file_save_path');
$admin_default_image = Config::get('params.admin_default_image');
$profile_pic = !empty($user->profile_image_full) ? $user->profile_image_full : asset($admin_default_image);
$role_ids = Config::get('params.role_ids');
$types = DB::table('types')->where('name','!=','BARTENDING')->get();
?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="@if(isset($profile)){{ asset($profile_pic)}}@else{{asset('assets/admin/images/logo.png')}}@endif" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?= $user_full_name ?></p>

            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <?php
            $active = ($controller == 'UsersController' && $action == 'dashboard') ? 'active' : '';
            ?>
            <li class="<?= $active ?>"><a href="<?= route('admin.dashboard') ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>


            <?php
            $active = ($controller == 'UsersController' && in_array($action, ['users', 'show', 'create', 'edit' ])) ? 'active' : '';
            ?>
            <li class="treeview <?= $active ?>">
                <a href="#"><i class="fa fa-users"></i><span> Customers</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php ?>
                    <li class="<?= $title_page == 'Customers' ? 'active' : '' ?>"><a href="<?= route('admin.users') ?>"><i class="fa fa-circle-o"></i> List </a></li>


                </ul>
            </li>



            <?php
            $active = ($controller == 'VendorsController' && in_array($action, ['index', 'create', 'edit' ])) ? 'active' : '';
            ?>
            <li class="treeview <?= $active ?>">
                <a href="#"><i class="fa fa-users"></i><span> Vendors</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <?php
                $active = ($controller == 'VendorsController' && in_array($action, ['index'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('admin.vendors') ?>"><i class="fa fa-circle-o"></i> List </a></li>



                </ul>
            </li>

            <?php
            $active = ($controller == 'OrderController' && in_array($action, ['index','show'])) ? 'active' : '';
            ?>
            <li class="treeview <?= $active ?>">
                <a href="#"><i class="fa fa-users"></i><span> Orders</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <?php
                $active = ($controller == 'Ordercontroller' && in_array($action, ['index'])) ? 'active' : '';
                ?>
                <li class="<?= $active ?>"><a href="<?= route('admin.orders.index') ?>"><i class="fa fa-circle-o"></i> All Orders </a></li>
                @foreach($types as $type)
                <li class="<?= $active ?>"><a href="{{url(SEGMENT.'/orders?type_id='.$type->id)}}"><i class="fa fa-circle-o"></i> {{$type->name}} </a></li>
                @endforeach

                </ul>
            </li>
             <?php
            $active = ($controller == 'BartendersController' && in_array($action, ['index', 'create', 'edit' ])) ? 'active' : '';
            ?>
            <li class="treeview <?= $active ?>">
                <a href="#"><i class="fa fa-users"></i><span> Bartenders</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <?php
                $active = ($controller == 'BartendersController' && in_array($action, ['index'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('admin.bartenders') ?>"><i class="fa fa-circle-o"></i> List </a></li>



                </ul>
            </li>

            <?php
            $active = ($controller == 'DriversController' && in_array($action, ['index', 'create', 'edit' ])) ? 'active' : '';
            ?>
            <li class="treeview <?= $active ?>">
                <a href="#"><i class="fa fa-users"></i><span> Drivers</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <?php
                $active = ($controller == 'DriversController' && in_array($action, ['index'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('admin.drivers') ?>"><i class="fa fa-circle-o"></i> List </a></li>



                </ul>
            </li>

            <?php /* ?>

            <?php
            $active = ($controller == 'CategoriesController' && in_array($action, ['index', 'create', 'edit' ])) ? 'active' : '';
            ?>
            <li class="treeview <?= $active ?>">
                <a href="#"><i class="fa fa-users"></i><span> Categories</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <?php
                $active = ($controller == 'CategoriesController' && in_array($action, ['index'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('categories.index') ?>"><i class="fa fa-circle-o"></i> List </a></li>
                    <?php
                $active = ($controller == 'CategoriesController' && in_array($action, ['create'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('categories.create') ?>"><i class="fa fa-circle-o"></i> Add </a></li>


                </ul>
            </li>


            <?php */ ?>


            <?php /* ?>


            <?php
            $active = ($controller == 'EquipmentsController' && in_array($action, ['index', 'create', 'edit' ])) ? 'active' : '';
            ?>
            <li class="treeview <?= $active ?>">
                <a href="#"><i class="fa fa-users"></i><span> Equipments</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <?php
                $active = ($controller == 'EquipmentsController' && in_array($action, ['index'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('equipments.index') ?>"><i class="fa fa-circle-o"></i> List </a></li>
                    <?php
                $active = ($controller == 'EquipmentsController' && in_array($action, ['create'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('equipments.create') ?>"><i class="fa fa-circle-o"></i> Add </a></li>


                </ul>
            </li>

            <?php
            $active = ($controller == 'MusclesController' && in_array($action, ['index', 'create', 'edit' ])) ? 'active' : '';
            ?>
            <li class="treeview <?= $active ?>">
                <a href="#"><i class="fa fa-users"></i><span> Muscles</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <?php
                $active = ($controller == 'MusclesController' && in_array($action, ['index'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('muscles.index') ?>"><i class="fa fa-circle-o"></i> List </a></li>
                    <?php
                $active = ($controller == 'MusclesController' && in_array($action, ['create'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('muscles.create') ?>"><i class="fa fa-circle-o"></i> Add </a></li>


                </ul>
            </li>
            <?php
            $active = ($controller == 'InjuriesController' && in_array($action, ['index', 'create', 'edit' ])) ? 'active' : '';
            ?>
            <li class="treeview <?= $active ?>">
                <a href="#"><i class="fa fa-users"></i><span> Injuries</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <?php
                $active = ($controller == 'InjuriesController' && in_array($action, ['index'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('injuries.index') ?>"><i class="fa fa-circle-o"></i> List </a></li>
                    <?php
                $active = ($controller == 'InjuriesController' && in_array($action, ['create'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('injuries.create') ?>"><i class="fa fa-circle-o"></i> Add </a></li>


                </ul>
            </li>

            <?php
            $active = ($controller == 'ExcersiceController' && in_array($action, ['index', 'create', 'edit' ])) ? 'active' : '';
            ?>
            <li class="treeview <?= $active ?>">
                <a href="#"><i class="fa fa-users"></i><span> Excercises</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <?php
                $active = ($controller == 'ExercisesController' && in_array($action, ['index'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('exercises.index') ?>"><i class="fa fa-circle-o"></i> List </a></li>
                    <?php
                $active = ($controller == 'ExercisesController' && in_array($action, ['create'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('exercises.create') ?>"><i class="fa fa-circle-o"></i> Add </a></li>


                </ul>
            </li>

            <?php
            $active = ($controller == 'QuantityUnitsController' && in_array($action, ['index', 'create', 'edit' ])) ? 'active' : '';
            ?>
            <li class="treeview <?= $active ?>">
                <a href="#"><i class="fa fa-users"></i><span> Quantity Units</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <?php
                $active = ($controller == 'QuantityUnitsController' && in_array($action, ['index'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('quantity-units.index') ?>"><i class="fa fa-circle-o"></i> List </a></li>
                    <?php
                $active = ($controller == 'QuantityUnitsController' && in_array($action, ['create'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('quantity-units.create') ?>"><i class="fa fa-circle-o"></i> Add </a></li>


                </ul>
            </li>

            <?php
            $active = ($controller == 'IngredientsController' && in_array($action, ['index', 'create', 'edit' ])) ? 'active' : '';
            ?>
            <li class="treeview <?= $active ?>">
                <a href="#"><i class="fa fa-users"></i><span> Ingredients</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <?php
                $active = ($controller == 'IngredientsController' && in_array($action, ['index'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('ingredients.index') ?>"><i class="fa fa-circle-o"></i> List </a></li>
                    <?php
                $active = ($controller == 'IngredientsController' && in_array($action, ['create'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('ingredients.create') ?>"><i class="fa fa-circle-o"></i> Add </a></li>


                </ul>
            </li>

            <?php
            $active = ($controller == 'MealsController' && in_array($action, ['index', 'create', 'edit' ])) ? 'active' : '';
            ?>
            <li class="treeview <?= $active ?>">
                <a href="#"><i class="fa fa-users"></i><span> Meals</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <?php
                $active = ($controller == 'MealsController' && in_array($action, ['index'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('meals.index') ?>"><i class="fa fa-circle-o"></i> List </a></li>
                    <?php
                $active = ($controller == 'MealsController' && in_array($action, ['create'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= route('meals.create') ?>"><i class="fa fa-circle-o"></i> Add </a></li>


                </ul>
            </li>
            <?php
            $active = ($controller == 'AttributesController' && in_array($action, ['index', 'edit'])) ? 'active' : '';
            ?>
            <li class="<?= $active ?>"><a href="{!! route('attributes.index') !!}"><i class="fa fa-file-text"></i> <span>Attributes</span></a></li>

            <?php */ ?>

            <?php
            $active = ($controller == 'CategoriesController' && in_array($action, ['index', 'edit'])) ? 'active' : '';
            ?>
            <li class="<?= $active ?>"><a href="{!! route('admin.types.index') !!}"><i class="fa fa-file-text"></i> <span>Categories</span></a></li>

            <?php
            $active = ($controller == 'FaqCategoriesController' && in_array($action, ['index', 'edit'])) ? 'active' : '';
            ?>
            <li class="<?= $active ?>"><a href="{!! route('faq-categories.index') !!}"><i class="fa fa-file-text"></i> <span>FAQ</span></a></li>

            <?php
            $active = ($controller == 'ContactRequestsController' && in_array($action, ['index', 'vie'])) ? 'active' : '';
            ?>
            <li class="<?= $active ?>"><a href="{!! route('admin.contactRequests.index') !!}"><i class="fa fa-file-text"></i> <span>Contact Requests</span></a></li>

            <?php
            $active = ($controller == 'DoItYourselfController' && in_array($action, ['index', 'edit'])) ? 'active' : '';
            ?>


            <li class="treeview <?= $active ?>">
                <a href="#"><i class="fa fa-file-text"></i><span> Do It Yourself</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <?php
                $active = ($controller == 'DoItYourselfController' && in_array($action, ['index'])) ? 'active' : '';
                ?>
                    <li class="<?= $active ?>"><a href="<?= url(SEGMENT.'/do-it-yourself?type_id=1') ?>"><i class="fa fa-circle-o"></i> Drink </a></li>
                    <li class="<?= $active ?>"><a href="<?= url(SEGMENT.'/do-it-yourself?type_id=2') ?>"><i class="fa fa-circle-o"></i> Food </a></li>
                </ul>
            </li>


            <?php
            $active = ($controller == 'BrandsController' && in_array($action, ['index', 'edit'])) ? 'active' : '';
            ?>
            <li class="<?= $active ?>"><a href="{!! route('brands.index') !!}"><i class="fa fa-file-text"></i> <span>Brands</span></a></li>

            {{-- // Attribute Tab --}}

            <?php
            $active = ($controller == 'CouponController' && in_array($action, ['index', 'edit'])) ? 'active' : '';
            ?>
            <li class="<?= $active ?>"><a href="{!! route('admin.coupons.index') !!}"><i class="fa fa-file-text"></i> <span>Coupons</span></a></li>
            <?php
            $active = ($controller == 'PlansController' && in_array($action, ['index', 'edit'])) ? 'active' : '';
            ?>
            <li class="<?= $active ?>"><a href="{!! route('plans.index') !!}"><i class="fa fa-file-text"></i> <span>Plans</span></a></li>

            <?php
            $active = ($controller == 'PagesController' && in_array($action, ['index', 'edit'])) ? 'active' : '';
            ?>
            <li class="<?= $active ?>"><a href="{!! route('admin.pages.index') !!}"><i class="fa fa-file-text"></i> <span>App Pages</span></a></li>


            <?php
            $active = ($controller == 'TemplatesController') ? 'active' : '';
            ?>
            <li class="<?= $active ?>"><a href="{!! route('admin.templates.index') !!}"><i class="fa fa-envelope-o"></i> <span>Email Templates</span></a></li>

            <?php
            $active = ($controller == 'DeliveryChargesController' && in_array($action, ['index'])) ? 'active' : '';
            ?>
            <li class="<?= $active ?>"><a href="{!! route('admin.delivery-charges.index') !!}"><i class="fa fas fa-truck"></i> <span>Delivery Charges</span></a></li>

            <?php
            $active = ($controller == 'SettingsController' && in_array($action, ['settings'])) ? 'active' : '';
            ?>
            <li class="<?= $active ?>"><a href="{!! route('admin.settings') !!}"><i class="fa fas fa-cog"></i> <span>Settings</span></a></li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>


