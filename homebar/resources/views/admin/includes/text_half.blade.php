<?php 
$lang_arr = Config::get('params.lang_arr');

foreach($lang_arr as $lang_key => $lang_data){ 
  
$lang_postfix = $lang_data['postfix'];
$field_name = $fields_param['name'];
$field_name_inc = $field_name.$lang_postfix;
$title = $fields_param['title'].' '. $lang_data['key'];

$options = ['class'=>'form-control','placeholder'=>$title, 'max'=>255];


if($fields_param['required']){
    $options['required'] = 'required';
}
if($lang_data['dir']){
    $options['dir'] = $lang_data['dir'];
}
$type = 'text';
if($fields_param['type']== 'text'){
    $options['max'] = 255;
} else {
    $options['rows'] = 5;
    $type = 'textArea';
}
if(old($field_name_inc) == null){
    $value = isset($entity[$field_name_inc]) ? $entity[$field_name_inc] : '';
} else {
    $value = old($field_name_inc);
}
?>
    
    <div class="<?= $fields_param['class'] ?> <?= $errors->has($field_name_inc) ? 'has-error' : '' ?>">
        <label class="control-label" for="title"><?= $title ?><?= ($fields_param['required']) ? '<span class="required">*</span>': null ?>
        </label>
        <div class="">
            {!! Form::$type($field_name_inc, $value, $options) !!}
            <span class="help-block"><?= $errors->has($field_name_inc) ? $errors->first($field_name_inc) : '' ?></span>
        </div>
    </div>
    

<?php } ?>