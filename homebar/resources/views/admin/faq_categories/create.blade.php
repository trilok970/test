<?php
$status_name_arr = Config::get('params.status_name_arr');
?>
@extends('layouts.admin.admin')
@section('content')

<?php

if(empty($entity->id)) {
    $action_route = ['faq-categories.store'];
    $method = 'POST';
} else {
    $action_route = ['faq-categories.update', $entity->id];
    $method = 'PATCH';
}

?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Information</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">

                {!! Form::model($entity, ['route' => $action_route, 'method' => $method, 'class'=>'form-horizontal validate', 'enctype'=>'multipart/form-data']) !!}
                {{ csrf_field() }}
                
                <div class="col-md-12 <?= $errors->has('name') ? 'has-error' : '' ?>">
                        <label class="control-label" for="name">Name</label>
                        <?= Form::text('name', null, ['placeholder'=>'Name','class'=>'form-control', 'required'=>true])?>
                        <span class="help-block"><?= $errors->has('name') ? $errors->first('name') : '' ?></span>
                    </div>
                
                
                    <div class="col-md-12 <?= $errors->has('status') ? 'has-error' : '' ?>">
                        <label class="control-label" for="status">Status</label>
                        <?= Form::select('status', $status_name_arr, null, ['placeholder'=>'Select Status','class'=>'form-control', 'required'=>true])?>
                        <span class="help-block"><?= $errors->has('status') ? $errors->first('status') : '' ?></span>
                    </div>
                
                
                <div class="text-right">
                    <a href="{!! route('faq-categories.index') !!}" class="btn btn-default"> Cancel </a>
                    <?= Form::submit('Submit', ['class' => 'btn btn-primary ']) ?>

                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>






    <!-- /.row -->
</section>
<!-- /.content -->
@endsection
