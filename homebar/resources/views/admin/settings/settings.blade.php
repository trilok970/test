@extends('layouts.admin.admin')
@section('content')


<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Information</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">
                {!! Form::open(['url' => route('admin.settings'),'class'=>'form-horizontal validate', 'enctype'=>'multipart/form-data']) !!}
                {{ csrf_field() }}
                <?php $i = 0;
                foreach($site_settings as $key => $setting) {
                    $form_div = 0;
                    if($i==0 || $i%2 == 0) {

                ?>
                    <div class="form-group">
                <?php } ?>

                        <div class="col-md-6">
                            <label class="control-label" for="<?= $setting->slug ?>"><?= $setting->name ?><span class="required">*</span>
                            </label>
                            <div class="">
                                <?php if($setting->type == 'TEXT') { ?>
                                {!! Form::text($setting->slug, $setting->value, ['class'=>'form-control', 'placeholder'=>$setting->name,'required'=>true]) !!}
                                <?php } else if($setting->type == 'NUMBER') { ?>
                                {!! Form::number($setting->slug, $setting->value, ['class'=>'form-control', 'placeholder'=>$setting->name,'required'=>true,'step' => '0.01','min'=>0,'pattern'=> '^\d*(\.\d{0,2})?$']) !!}
                                <?php } else if($setting->type == 'FILE') { ?>
                                    {!! Form::file($setting->slug); !!}
                                    <?php
                                    if (!empty($setting->value)) {
                                        ?>
                                        <br><br>
                                        <img src="<?= asset($setting->value) ?>" style="max-width: 200px;" />
                                        <?php
                                    }
                                    ?>
                                <?php }?>
                            </div>
                        </div>
                        <?php

                        if($i%2 != 0) {
                        ?>
                    </div>
                    <?php } ?>
                <?php
                    $i++;
                }
                ?>



                <div class="form-group"> </div>

                <div class="text-right">
                    <a href="{!! route('admin.dashboard') !!}" class="btn btn-default"> Cancel </a>
                    <?= Form::submit('Update', ['class' => 'btn btn-primary ']) ?>

                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>






    <!-- /.row -->
</section>
<!-- /.content -->
@endsection
