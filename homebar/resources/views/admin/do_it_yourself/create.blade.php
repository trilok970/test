<?php
// $fields = Config::get('lang.categories');
// $fields_html = getFields($fields, $entity);
$status_name_arr = Config::get('params.status_name_arr');
$ingredientCountStart = 1;
$total_ingredients = @isset($ingredients)?count($ingredients):0;
if(!$total_ingredients) $total_ingredients =1;
?>
@extends('layouts.admin.admin')
@section('content')

<style type="text/css">
    .checkox_input.checkox_input {width: 26px;}
</style>

<?php

if(empty($entity)) {
    $action_route = ['admin.do_it_yourself.store'];
    $method = 'POST';
    $current_cat = null;
    $type_id = null;
} else {
    $action_route = ['admin.do_it_yourself.update', [$entity->id]];
    $method = 'POST';

    $current_cat_arr = $entity->CategoryData->toArray();
    $current_cat = $current_cat_arr['id'];
    $type_id = $entity->type_id;
}

//echo $parent_id.'zz';

?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <!-- <h3 class="box-title">Information</h3> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">

                {!! Form::model($entity, ['route' => $action_route, 'method' => $method, 'class'=>'form-horizontal validate', 'enctype'=>'multipart/form-data']) !!}
                {{ csrf_field() }}
                <div class="form-group ">
                    <div class="col-md-12 <?= $errors->has('type') ? 'has-error' : '' ?> type_div">
                        <label class="control-label" for="type">Type</label>
                        {{-- Form::select('type', $type, $type_id, ['placeholder'=>'Select type','class'=>'form-control', 'required'=>true]) --}}
                        <select name="type" id="type" class="form-control">
                            <option value="Select Type">Select Type</option>
                            @foreach ($type as $id => $name)
                                <option value="{{$id}}" @if(isset($entity) && $entity->type_id == $id){{'selected'}}@endif>{{$name}}</option>
                            @endforeach
                        </select>
                        <span class="help-block"><?= $errors->has('type') ? $errors->first('type') : '' ?></span>
                    </div>

                    <div class="col-md-12 <?= $errors->has('category') ? 'has-error' : '' ?>">
                        <label class="control-label" for="category">Category</label>
                        <select name="category" id="category" class="form-control">
                            <option value="">Select Category</option>
                            @foreach ($categories as $id => $name)
                                <option value="{{$id}}" @if($entity->category_id == $id){{'selected'}}@endif>{{$name}}</option>
                            @endforeach
                        </select>
                        <span class="help-block"><?= $errors->has('category') ? $errors->first('category') : '' ?></span>
                    </div>

                    <div class="col-md-12 <?= $errors->has('title') ? 'has-error' : '' ?>">
                        <label class="control-label" for="title">Title</label>
                        <?= Form::text('title', null, ['placeholder'=>'Title','class'=>'form-control', 'required'=>true])?>
                        <span class="help-block"><?= $errors->has('title') ? $errors->first('title') : '' ?></span>
                    </div>

                    <div class="col-md-12 <?= $errors->has('description') ? 'has-error' : '' ?>">
                        <label class="control-label" for="description">Description</label>
                        <?= Form::textarea('description', null, ['placeholder'=>'Description','class'=>'form-control', 'required'=>true])?>
                        <span class="help-block"><?= $errors->has('description') ? $errors->first('description') : '' ?></span>
                    </div>

                    <div class="col-md-12 <?= $errors->has('youtube_url') ? 'has-error' : '' ?>">
                        <label class="control-label" for="youtube_url">Youtube URL</label>
                        <?= Form::text('youtube_url', null, ['placeholder'=>'Youtube URL','class'=>'form-control'])?>
                        <span class="help-block"><?= $errors->has('youtube_url') ? $errors->first('youtube_url') : '' ?></span>
                    </div>

                    <div class="col-md-12 <?= $errors->has('prep_time') ? 'has-error' : '' ?>">
                        <label class="control-label" for="prep_time">Prepration Time</label>
                        <?= Form::text('prep_time', null, ['placeholder'=>'Prepration Time','class'=>'form-control'])?>
                        <span class="help-block"><?= $errors->has('prep_time') ? $errors->first('prep_time') : '' ?></span>
                    </div>

                    <div class="col-md-12 <?= $errors->has('servings') ? 'has-error' : '' ?>">
                        <label class="control-label" for="servings">Number of Servings</label>
                        <?= Form::number('servings', null, ['placeholder'=>'Number of Servings','class'=>'form-control'])?>
                        <span class="help-block"><?= $errors->has('servings') ? $errors->first('servings') : '' ?></span>
                    </div>

                    <div class="col-md-12 <?= $errors->has('image') ? 'has-error' : '' ?>">
                        <label class="control-label" for="image">Image</label>
                        {!! Form::file('image',null,null, array('id'=>'image','accept'=>'image/*')); !!}
                        <span class="help-block"><?= $errors->has('image') ? $errors->first('image') : '' ?></span>
                        <?php
                        if (!empty($entity->image)) {
                            ?>
                            <img src="{{ asset($entity->image) }}" style="max-width: 200px;" />
                            <?php
                        }
                        ?>
                    </div>
                    <br/>


                    @if(isset($ingredients) && count($ingredients))
                        <h4>Ingredients</h4>
                        @foreach($ingredients as $val)
                        <div class="col-md-12">

                            <div class="col-md-6">
                                <label class="control-label" for="ingredient_name_1">Ingredient Name</label>
                                <?= Form::text('ingredient_name_'.$ingredientCountStart, $val->item_name, ['placeholder'=>'Ingredient Name','class'=>'form-control'])?>
                            </div>

                            <div class="col-md-5">




                                <label class="control-label" for="ingredient_quantity_1">Ingredient Qunatity</label>
                                <?= Form::text('ingredient_quantity_'.$ingredientCountStart, $val->item_quantity, ['placeholder'=>'Quantity','class'=>'form-control'])?>
                            </div>
                            <input type="hidden" name="ingredient_{{$ingredientCountStart}}" value="{{$val->id}}">
                        </div>
                        <?php $ingredientCountStart++; ?>
                        @endforeach
                    @else
                        @if(old('ingredient_name_1'))
                            <h4>Ingredients</h4>
                            @for($i = 1; $i <= 100; $i++)
                                @if (old('ingredient_name_'.$i))
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <label class="control-label" for="ingredient_name_1">Ingredient Name</label>
                                            <?= Form::text('ingredient_name_'.$i, old('ingredient_name_'.$i), ['placeholder'=>'Ingredient Name','class'=>'form-control']) ?>
                                        </div>

                                        <div class="col-md-5">
                                            <label class="control-label" for="ingredient_quantity_1">Ingredient Qunatity</label>
                                            <?=Form::text('ingredient_quantity_'.$i, old('ingredient_quantity_'.$i), ['placeholder'=>'Quantity','class'=>'form-control tags-input']) ?>
                                        </div>

                                        <input type="hidden" name="ingredient_{{$i}}" value="{{old('ingredient_'.$i)}}">

                                    </div>
                                @else
                                    @break;
                                @endif
                            @endfor
                        @else
                        <div class="col-md-12">
                            <h4>Ingredients</h4>
                            <div class="col-md-3">
                                <label class="control-label" for="ingredient_name_1">Ingredient Name</label>
                                <?= Form::text('ingredient_name_1', null, ['placeholder'=>'Ingredient Name','class'=>'form-control'])?>
                            </div>

                            <div class="col-md-5">
                                <label class="control-label" for="ingredient_quantity_1">Ingredient Qunatity</label>
                                <?= Form::text('ingredient_quantity_1', null, ['placeholder'=>'Quantity','class'=>'form-control tags-input-1'])?>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label" for="ingredient_quantity_1">Ingredient Value</label>
                                <?= Form::text('ingredient_value_1', null, ['placeholder'=>'Value','class'=>'form-control'])?>
                            </div>

                        </div>
                        @endif
                    @endif


                    <div class="append_me">
                    </div>

                    <div class="col-md-12">
                        <a href="#" class="add_more" style="margin: 10px 0 0 30px; display: block; color: #000; font-weight: bold; font-style: italic;" data-count="{{$total_ingredients}}">Add More</a>
                    </div>

                </div>

                <div class="text-right">
                    <a href="{!! route('admin.do_it_yourself.index') !!}" class="btn btn-default"> Cancel </a>
                    <?= Form::submit('Submit', ['class' => 'btn btn-primary ']) ?>

                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>

</section>

@endsection

@section('uniquepagescript')
<style type="text/css">
    .bootstrap-tagsinput{
        width: 100%;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" integrity="sha512-xmGTNt20S0t62wHLmQec2DauG9T+owP9e6VU8GigI0anN7OXLip9i7IwEhelasml2osdxX71XcYm6BQunTQeQg==" crossorigin="anonymous" />
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js" integrity="sha512-9UR1ynHntZdqHnwXKTaOm1s6V9fExqejKvg5XMawEMToW4sSw+3jtLrYfZPijvnwnnE8Uol1O9BcAskoxgec+g==" crossorigin="anonymous"></script>
<script type="text/javascript">
   $(document).ready(function(){        
      var tagInputEle = $('.tags-input-1');
      tagInputEle.tagsinput();


    });
    $(document).ready(function(){
        $('.add_more').click(function(){
            var data_count = $(this).attr('data-count');
            console.log(data_count);

            data_count = parseInt(data_count);

            data_count++;

            var append_me = '<div class="col-md-12"><div class="col-md-3"> <label class="control-label" for="ingredient_name_'+data_count+'">Ingredient Name</label> <input type="text" name="ingredient_name_'+data_count+'" placeholder="Ingredient Name" class="form-control" /> </div><div class="col-md-5"> <label class="control-label" for="ingredient_quantity_'+data_count+'">Ingredient Qunatity</label><input type="text" name="ingredient_quantity_'+data_count+'" placeholder="Quantity" class="form-control tags-input-'+data_count+'" /></div><div class="col-md-3"> <label class="control-label" for="ingredient_quantity_'+data_count+'">Ingredient Value</label><input type="text" name="ingredient_value_'+data_count+'" placeholder="Value" class="form-control" /></div><div class="col-md-1" style="margin-top: 30px"><p><a href="#" class="btn btn-info delete-btn"><i class="fa fa-trash"></i></a></p></div></div><br/>';
    

            $('.append_me').append(append_me);

              var tagInputEles = $('.tags-input-'+data_count);
              tagInputEles.tagsinput();
            $(this).attr('data-count',data_count);


            return false;

        });

        $('.type_div select').change(function(){
            var type = $(this).find('option:selected').val();
            console.log(type);

            jQuery.ajax({
                url: '{{route('admin.do_it_yourself.get_categories')}}',
                type: 'POST',
                data:{type_id:type},
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    //$("#status_" + id).html(response);
                    console.log(response);
                    $('select[name=category]').html(response);
                }
            });

        });
    });

    $(document).on('click','.delete-btn',function(evt){
        evt.preventDefault();
        $(this).closest('.col-md-12').remove();
    });
</script>
@endsection
