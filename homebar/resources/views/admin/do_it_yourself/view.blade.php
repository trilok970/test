<?php
$cat = $entity->categoryData->toArray();

if(count($cat))
$category_name = $cat['name'];
else $category_name = '';
?>
@extends('layouts.admin.admin')
@section('content')

<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <!-- <h3 class="box-title">Information</h3> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">
                <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tbody>
                        <tr>
                            <th>Title:</th>
                            <td>{{ $entity->title }}</td>
                        </tr>

                        <tr>
                            <th>Category:</th>
                            <td>{{ $category_name }}</td>
                        </tr>

                        <tr>
                            <th>Description:</th>
                            <td>{{ $entity->description }}</td>
                        </tr>

                        <tr>
                            <th>Youtube URL:</th>
                            <td>{{ $entity->youtube_url }}</td>
                        </tr>

                        <tr>
                            <th>Prepration Time:</th>
                            <td>{{ $entity->prep_time }}</td>
                        </tr>

                        <tr>
                            <th>Number of Servings :</th>
                            <td>{{ $entity->servings }}</td>
                        </tr>

                        <tr>
                            <th>Image:</th>
                            <td><?php echo getListImage(asset($entity->image)); ?></td>
                        </tr>

                        <tr>
                            <th>Created:</th>
                            <td> {{ listDateFromat($entity->created_at) }} </td>

                        </tr>


                        <tr>
                            <th>Status</th>
                            <td> <?= getStatus($entity->status,$entity->id) ?> </td>
                        </tr>

                    </tbody>

                </table>
                </div>

                @if(count($Ingredient))

                <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td><h3>Ingredients</h3></td>
                        </tr>

                        @foreach($Ingredient as $val)

                        <tr>
                            <td>{{ $val->item_name.' ( '.$val->item_quantity.' )' }}</td>
                        </tr>

                        @endforeach

                    </tbody>

                </table>
                </div>
                @endif

            </div>
        </div>
    </div>

</section>

@endsection
