@extends('layouts.admin.admin')
@section('content')


<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">List</h3>
                    <a href="<?= route('admin.categories.add', [$type_id, $parent_id]) ?>" class="btn btn-primary getExcel" style="float: right;">
                        <i class="fa fa-plus"></i> Add New
                    </a>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> ID </th>
                                <th> Name </th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Registered Vendors</th>

                                @if(!$parent_id)
                                <th>Do it Yourself</th>
                                <th>Pre Made</th>
                                @endif

                                <th>Last Modified</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                    </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->


        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection

<?php
$dt_url = route('admin.categories.datatables', [$type_id, $parent_id]);
?>

@section('uniquepagestyle')
<link rel="stylesheet" href="{{asset('assets/admin/dataTables.bootstrap.min.css')}}">
@endsection

@section('uniquepagescript')
<script src="{{asset('assets/admin/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/admin/js/dataTables.bootstrap4.min.js')}}"></script>

@if(!$parent_id)
<script>

$(function () {
    var table = $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        pageLength: getAdminPageLength(),
        order: [[0, "desc"]],
        "ajax": {
            "url": '{!! $dt_url !!}',
            "dataType": "json",
            "type": "POST",
            "data": {_token: "{{csrf_token()}}"}
        },
        columns: [
            { data: 'id', name: 'id', orderable:true, "visible": false },
            {data: 'name', name: 'name', orderable: true},
            {data: 'image', name: 'image', orderable: false},
            {data: 'status', name: 'status', orderable: true},
            {data: 'children_count', name: 'children_count', orderable: false},
            {data: 'is_do_it_yourself', name: 'is_do_it_yourself', orderable: false},
            {data: 'is_pre_made', name: 'is_pre_made', orderable: false},
            {data: 'created_at', name: 'created_at', orderable: true},
            {data: 'action', name: 'action', orderable: false}
        ],
        "columnDefs": [
            {"searchable": false, "targets": 0},
            {className: 'text-center', targets: [1]},
        ]
        , language: {
            searchPlaceholder: "Search"
        }
    });
});
</script>
@endif


@if($parent_id)
<script>

$(function () {
    var table = $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        pageLength: getAdminPageLength(),
        order: [[0, "desc"]],
        "ajax": {
            "url": '{!! $dt_url !!}',
            "dataType": "json",
            "type": "POST",
            "data": {_token: "{{csrf_token()}}"}
        },
        columns: [
            { data: 'id', name: 'id', orderable:true, "visible": false },
            {data: 'name', name: 'name', orderable: true},
            {data: 'image', name: 'image', orderable: false},
            {data: 'status', name: 'status', orderable: true},
            {data: 'children_count', name: 'children_count', orderable: false},
            //{data: 'is_do_it_yourself', name: 'is_do_it_yourself', orderable: false},
            //{data: 'is_pre_made', name: 'is_pre_made', orderable: false},
            {data: 'created_at', name: 'created_at', orderable: true},
            {data: 'action', name: 'action', orderable: false}
        ],
        "columnDefs": [
            {"searchable": false, "targets": 0},
            {className: 'text-center', targets: [1]},
        ]
        , language: {
            searchPlaceholder: "Search"
        }
    });
});
</script>
@endif



@endsection
