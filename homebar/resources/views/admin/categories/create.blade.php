<?php
// $fields = Config::get('lang.categories');
// $fields_html = getFields($fields, $entity);
$status_name_arr = Config::get('params.status_name_arr');
?>
@extends('layouts.admin.admin')
@section('content')

<style type="text/css">
    .checkox_input.checkox_input {width: 26px;}
</style>

<?php

if(empty($entity->id)) {
    $action_route = ['admin.categories.store', [$type_id, $parent_id]];
    $method = 'POST';
} else {
    $action_route = ['admin.categories.update', [$entity->id]];
    $method = 'POST';
}

//echo $parent_id.'zz';

?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Information</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">

                {!! Form::model($entity, ['route' => $action_route, 'method' => $method, 'class'=>'form-horizontal validate', 'enctype'=>'multipart/form-data']) !!}
                {{ csrf_field() }}
                <div class="form-group ">
                    
                    <div class="col-md-12 <?= $errors->has('name') ? 'has-error' : '' ?>">
                        <label class="control-label" for="name">Name</label>
                        <?= Form::text('name', null, ['placeholder'=>'Name','class'=>'form-control', 'required'=>true])?>
                        <span class="help-block"><?= $errors->has('name') ? $errors->first('name') : '' ?></span>
                    </div>

                    @if(!$parent_id)

                    <div class="col-md-12 <?= $errors->has('is_pre_made') ? 'has-error' : '' ?>">                       

                        <label class="control-label" for="is_pre_made">Pre Made</label>
                        
                        <?php if(!$entity->id) $pre_made ='checked'; else $pre_made = null; ?>

                        {{ Form::checkbox('is_pre_made',null,$pre_made, array('id'=>'is_pre_made','class'=>'checkox_input')) }}
                        
                        <span class="help-block"><?= $errors->has('is_pre_made') ? $errors->first('is_pre_made') : '' ?></span>
                    </div>
                    
                    <div class="col-md-12 <?= $errors->has('is_do_it_yourself') ? 'has-error' : '' ?>">
                        
                        <label class="control-label" for="is_do_it_yourself">Do it yourself</label>

                        {{ Form::checkbox('is_do_it_yourself',null,null, array('id'=>'is_do_it_yourself','class'=>'checkox_input')) }}
                        
                        <span class="help-block"><?= $errors->has('is_do_it_yourself') ? $errors->first('is_do_it_yourself') : '' ?></span>
                    </div>

                    
                    @endif


                    <div class="col-md-12 <?= $errors->has('status') ? 'has-error' : '' ?>">
                        <label class="control-label" for="status">Status</label>
                        <?= Form::select('status', $status_name_arr, null, ['placeholder'=>'Select Status','class'=>'form-control', 'required'=>true])?>
                        <span class="help-block"><?= $errors->has('status') ? $errors->first('status') : '' ?></span>
                    </div>

                    <div class="col-md-12 <?= $errors->has('image') ? 'has-error' : '' ?>">
                        <label class="control-label" for="image">Image</label>
                        {!! Form::file('image',null,null, array('id'=>'image','accept'=>'image/*')); !!}
                        <span class="help-block"><?= $errors->has('image') ? $errors->first('image') : '' ?></span>
                        <?php
                        if (!empty($entity->image_full)) {
                            ?>
                            <img src="{{ $entity->image_full }}" style="max-width: 200px;" />
                            <?php
                        }
                        ?>
                    </div>
                </div>
                
                
                
                
                
                <div class="text-right">
                    <a href="{!! route('admin.categories.index', [$type_id, $parent_id]) !!}" class="btn btn-default"> Cancel </a>
                    <?= Form::submit('Submit', ['class' => 'btn btn-primary ']) ?>

                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>






    <!-- /.row -->
</section>
<!-- /.content -->
@endsection
