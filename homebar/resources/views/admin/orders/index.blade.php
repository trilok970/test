@extends('layouts.admin.admin')
@section('content')
<?php
$role_ids = Config::get('params.role_ids');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
          </div>
             <!-- /.box-header -->
             <div class="box-body">
            <div class="row">
            {{-- @if($type_id==0) --}}
            @foreach($statusCountResult as $key=>$value)

              <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-{{$colors[$loop->index]}} p-3">
                    <div class="inner">
                        <h3>{{$key}}</h3>

                        <p><b> {{$value}}</b> </p>
                    </div>
                    <div class="icon">
                        <i class="fa fas fa-{{$icons[$loop->index]}}"></i>
                    </div>
                    <br>
                </div>
              </div>

              @endforeach
              {{-- @endif --}}
              </div>
                <div class="table-responsive">
                <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                      <th>ID</th>
                      <th>Order#</th>
                      <th>User</th>
                      <th>Total Amount</th>
                      <th>Order Type</th>
                      <th>Delivery Type</th>
                      <th>Delivery Date</th>
                      <th>Last Modified</th>
                      <th>Status</th>
                      <th>Action</th>
                  </tr>
                  </thead>

                </table>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->


          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
@endsection


@section('uniquepagestyle')
<link rel="stylesheet" href="{{asset('assets/admin/dataTables.bootstrap.min.css')}}">
@endsection
@section('uniquepagescript')
<script src="{{asset('assets/admin/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/admin/js/dataTables.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        var type_id = "{{$type_id}}";
        var table = $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        pageLength: getAdminPageLength(),
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('admin.orders.datatable') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}",type_id:type_id}
        },
        columns: [
        { data: 'id', name: 'id', orderable:true, "visible": true },
        { data: 'order_number',name:'order_number',orderable:true},
        { data: 'user',name:'user',orderable:false},
        { data: 'total_amount', name: 'total_amount', orderable:true },
        { data: 'order_type', name: 'order_type', orderable:true },
        { data: 'reach_type', name: 'reach_type', orderable:true  },
        { data: 'delivery_date', name: 'delivery_date', orderable:true },
        { data: 'created_at', name: 'created_at', orderable:true },
        { data: 'status', name: 'status', orderable:false},
        { data: 'action', name: 'action', orderable:false},
        ],
});
});
</script>
@endsection
