@extends('layouts.admin.admin')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title_page ?>
    </h1>
    @include('admin.includes.breadcumb')
</section>
<section class="content">
    <div class="row">
      <div class="col-lg-8">
        <div class="box">
          <div class="box-header">Order Detail</div>
             <!-- /.box-header -->
             <div class="box-body">
                <div class="table-responsive">
                <table class="table table-bordered table-hover">
                  <tr>
                      <th>ID</th><td>{{$order->id}}</td>
                      <th>Order#</th><td>{{$order->order_number}}</td>
                  </tr>
                  <tr>
                      <th>Address</th>
                      <td colspan="3">{{$order->address}}</td>
                  </tr>
                  <tr>
                      <th>Latitude</th>
                      <td>{{$order->address_lat}}</td>
                      <th>Longitude</th>
                      <td>{{$order->address_lng}}</td>
                  </tr>
                  <tr>
                      <th>Order Type</th><td>{{$order->order_type}}</td>
                      <th>Delivery Type</th><td>{{$order->reach_type}}</td>
                  </tr>
                  <tr>
                    <th>Delivery Date</th><td>{{$order->delivery_date}}</td>
                    <th>Created</th><td>{{date("m/d/Y",strtotime($order->created_at))}}</td>
                  </tr>
                    <tr>
                      <th>Status</th><td>{{$order->status}}</td>
                      @if($order->image)
                      <th>Image</th><td><a href="{{ $order->image }}" target="_blank" ><img src="{{ $order->image }}" width="80" /></a></td>
                      @endif
                    </tr>
                  </tr>
                </table>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
          <div class="col-lg-4">
            <div class="box">
                <div class="box-header">
                  Customer Detail
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>ID</th>
                            <td>{{$order->user->id}}</td>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <td>{{$order->user->first_name.' '.$order->user->last_name}}</td>
                        </tr>
                        <tr>
                            <th>Mobile</th>
                            <td>{{phoneNumberFormat($order->user->phone)}}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{$order->user->email}}</td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td>{{$order->user->address}}</td>
                        </tr>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-header">Order Items</div>
                    <div class="box-body">
                        <div class="table-responsive">
                        @if($order->items)
                        <table class="table table-bordered table-striped table-hover">


                            @foreach ($order->items->where('menu_item_id','>',0)  as $item)
                            @if($loop->iteration == 1)
                            @if($item->menu_item_id)
                            <tr>

                                <th>Item ID</th>
                                <th>Item Name</th>
                                <th>Restaurant</th>
                                <th>Price</th>
                                <th>Qty</th>
                                <th>Tax</th>
                                <th>Service Fee</th>
                                <th>Admin Commission</th>
                                <th>Special Instruction</th>
                            </tr>
                            @endif
                            @endif
                                @if($item->menu_item_id)
                                <tr>
                                    <td>@if($item->menuItemDetail){{$item->menuItemDetail->id}}@endif</td>
                                    <td>@if($item->menuItemDetail){{$item->menuItemDetail->name}}@endif</td>
                                    <td>{{$item->restaurentDetail->first_name}} {{$item->restaurentDetail->last_name}}</td>
                                    <td>${{$item->price}}</td>
                                    <td>{{$item->quantity}}</td>
                                    <td>{{$item->tax}}</td>
                                    <td>{{$item->service_fee}}</td>
                                    <td>{{$item->admin_commission}}</td>
                                    <td>{{$item->special_instructions}}</td>
                                </tr>
                                @endif
                            @endforeach
                        </table>



                        <table class="table table-bordered table-striped table-hover">


                            @foreach ($order->items->where('bartender_id','>',0) as $item)
                            @if($loop->iteration == 1)
                            @if($item->bartender_id)

                            <tr>
                                <th>Bartender ID</th>
                                <th>Bartender Name</th>
                                <th>Book Date</th>
                                <th>From Time</th>
                                <th>To Time</th>
                                <th>Time In Hours</th>
                                <th>No Of Guests</th>

                                <th>Price</th>
                                <th>Qty</th>
                                <th>Tax</th>
                                <th>Service Fee</th>
                                <th>Admin Commission</th>
                                <th>Special Instruction</th>
                            </tr>
                            @endif
                            @endif
                            @if($item->bartender_id)

                                <tr>
                                    <td>{{ $item->bartender_id }}</td>
                                    <td>{{$item->bartender->first_name }} {{$item->bartender->last_name }}</td>
                                    <td>{{listDateFromat($item->book_date)}} </td>
                                    <td>{{listTimeFromat($item->from_time)}} </td>
                                    <td>{{listTimeFromat($item->to_time)}} </td>
                                    <td>{{$item->time_in_hours}} </td>
                                    <td>{{$item->no_of_guests}} </td>


                                    <td>${{$item->price}}</td>
                                    <td>{{$item->quantity}}</td>
                                    <td>{{$item->tax}}</td>
                                    <td>{{$item->service_fee}}</td>
                                    <td>{{$item->admin_commission}}</td>
                                    <td>{{$item->special_instructions}}</td>
                                </tr>
                                @endif
                            @endforeach
                            </table>



                            <table class="table table-bordered table-striped table-hover">


                            @foreach ($order->items->where('diy_id','>',0) as $item_diy)
                            @if($loop->iteration == 1 && $item_diy->diy_id)


                            <tr>

                                <th>DIY ID</th>
                                <th>DIY Name</th>
                                <th>Restaurant</th>
                                <th>Ingradient</th>
                                <th>Price</th>
                                <th>Qty</th>
                                <th>Tax</th>
                                <th>Service Fee</th>
                                <th>Admin Commission</th>
                                <th>Special Instruction</th>
                            </tr>
                            @endif
                            @if($item_diy->diy_id)

                                <tr>
                                    <td>{{ $item_diy->diy_id }}</td>
                                    <td>{{$item_diy->diyDetails->title }}</td>
                                    <td>{{$item_diy->restaurentDetail->first_name}} {{$item_diy->restaurentDetail->last_name}} </td>
                                    <td>{{$item_diy->ingredientDetail->item_name}} </td>


                                    <td>${{$item_diy->price}}</td>
                                    <td>{{$item_diy->quantity}}</td>
                                    <td>{{$item_diy->tax}}</td>
                                    <td>{{$item_diy->service_fee}}</td>
                                    <td>{{$item_diy->admin_commission}}</td>
                                    <td>{{$item_diy->special_instructions}}</td>
                                </tr>
                                @endif
                            @endforeach
                            </table>


                        @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-header">Payment Details</div>
                    <div class="box-body">
                        <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped">
                            <tr>
                                <th>Order Total</th>
                                <th>${{$order->sub_total}}</th>
                            </tr>
                            <tr>
                                <th>Delivery Charges</th>
                                <th>${{$order->delivery_charge}}</th>
                            </tr>
                            <tr>
                                <th>Service Charges</th>
                                <th>${{$order->service_fee}}</th>
                            </tr>
                            <tr>
                                <th>Tax</th>
                                <th>${{$order->tax}}</th>
                            </tr>
                            <tr>
                                <th>Coupon Code</th>
                                <td>@if(isset($order->coupon_code)){{$order->coupon_code}}@else{{'N/A'}}@endif</td>
                            </tr>
                            <tr>
                                <th>Coupon Discount</th>
                                <th>${{$order->coupon_discount_amount}}</th>
                            </tr>
                            <tr>
                                <th>Total Amount</th>
                                <th>${{$order->total_amount}}</th>
                            </tr>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </section>
      <!-- /.content -->
@endsection
