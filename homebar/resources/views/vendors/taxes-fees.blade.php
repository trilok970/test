<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="{{asset('assets/front/images/favicon.jpg')}}" type="image/jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Taxes Fees</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!-- Custom -->
    <link rel="stylesheet" href="{{asset('assets/front/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/swiper.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/responsive-index.css')}}">

</head>

<body id="page-top">
    <!-- Navigation-->





    <div class="container padding-top">
          <div class="row black-area">
              <div class="col-12">
                @php
                $cms = DB::table('pages')->where('slug','taxes-fees')->first();
                @endphp
                <h2  class="h2style text-center"> {{ ucfirst($cms->title) }}</h2>
                <div class="col-12 text-area">

                    {!! $cms->description !!}
                </div>
              </div>





          </div>
      </div>







    <!--    go top-->
    <a href="#" id="gotop" data-aos="fade-in" style="display: inline-block;"> <i class="fa fa-angle-up"> </i> </a>

    <!-- Scripts -->
    <script src="{{asset('assets/front/js/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('assets/front/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
    <!-- aos animation -->
    <script src="{{asset('assets/front/js/aos.js')}}"></script>
    <!-- Owl Carousel -->
    <script src="{{asset('assets/front/js/owl.carousel.min.js')}}"></script>

    <script src="{{asset('assets/front/js/swiper.min.js')}}"></script>
    <script src="{{asset('assets/front/js/who.js')}}"></script>
    <script src="{{asset('assets/front/js/custom.js')}}"></script>

    <script src="{{asset('assets/front/js/bootstrap-select.js')}}"></script>
    <script>
        $(window).scroll(function() {
            if ($(this).scrollTop() > 50) {
                $('#dynamic').addClass('newClass');
            } else {
                $('#dynamic').removeClass('newClass');
            }
        });
    </script>

</body>


</html>
