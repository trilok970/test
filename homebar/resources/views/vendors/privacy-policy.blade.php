<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="{{asset('assets/front/images/favicon.jpg')}}" type="image/jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Privacy Policy</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!-- Custom -->
    <link rel="stylesheet" href="{{asset('assets/front/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/swiper.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/responsive-index.css')}}">

</head>

<body id="page-top">
    <!-- Navigation-->
    <div class="row fixed-top" id="dynamic">
        <div class="container-fluid  ">
            <div class="row mt-3">
                <div class="col-lg-6 col-5 ">
                    <img src="{{asset('assets/front/images/logo.png')}}">
                </div>
                @if (!Session::has('VendorLoggedIn'))
                <div class="col-lg-6 col-7 text-right">
                    <a href="{!! route('vendor.login') !!}" class="btn signin"> Sign in </a>
                    <a href="{!! route('vendor.signup') !!}" class="btn signup ml-2"> Sign Up </a>
                </div>
                @else
                <div class="col-lg-6 col-7 text-right">
                    <a href="{!! route('vendor.orders','drinks') !!}" class="new-btn"> Dashboard</a>
                </div>
                @endif
            </div>
        </div>
    </div>

    <div class="privacy-banner">

    </div>


    <div class="container padding-top privacy-policy">
          <div class="row black-area">
              <div class="col-12">
                <h2 class="h2style ">PRIVACY POLICY</h2>
              </div>
              <div class="col-12 text-area">
                  <p><b>HOMEBAR APP</b> (“we” or “us” or “our”) respects the privacy of our users (“user” or “you”). This Privacy Policy explains how we collect, use, disclose, and safeguard your information when you visit our mobile application (the “Application”). This notice applies to all users of our application, website, features, or other services anywhere in the world, unless covered by a separate privacy notice. Please read this Privacy Policy carefully.</p>
                  <p>IF YOU DO NOT AGREE WITH THE TERMS OF THIS POLICY, PLEASE DO NOT ACCESS THE APPLICATION.</p>
                  <p><b>Scope:</b> This notice applies to users of HomeBar App’s services anywhere in the world, including use of the application, website, features or other services.</p>
                  <p>We reserve the right to make changes to this Privacy Policy at any time for any reason. We will alert you about any changes by updating the “Last updated” date of this Privacy Policy. You are encouraged to periodically review this Privacy Policy to stay informed of updates. You will be deemed to have been made aware of, will subject to, and will be deemed to have accepted the changed in any revised Privacy Policy by your continued use of the Application after the date such revised Privacy Policy posted. </p>
                  <p>This notice also governs HomeBar App’s other collections of personal data in connection with HomeBar App’s services. Our data practices are subject to applicable laws in the places in which we operate. </p>
                  <p>This Privacy Policy does not apply to the third-party online/mobile store from which you install the Application or make payments, including any in-game virtual items, which may also collect and use data about you. We are not responsible for any data collected by any such third party. </p>
                  <h2 class="h2style ">COLLECTION OF YOUR INFORMATION</h2>
                  <h3>Data we collect:</h3>
                  <p><ul>
                  <li>Data provided by users to HomeBar App, such as during account creation;</li>
                  <li>Data created during use of our services, such as location, app usage, and device data;</li>
                  <li>Data from other sources, such as HomeBar App partners and third parties.</li>
                  </ul>
                  </p>
                  <p>We may collect information about you in a variety of ways. The information we may collect via the Application depends on the content and materials you use, and includes: </p>
                  <h3>Personal Data</h3>
                  <p>Demographic and other personally identifiable information (such as your name and email address) that you voluntarily give to us when choosing to participate in various activities related to the Application, such as chat, posting messages in comment sections or in our forms, liking posts, sending feedback, and responding to surveys. If you choose to share data about yourself via your profile, online chat, or other interactive areas of the Application, please be advised that all data you disclose in these areas is public and your data will be accessible to anyone who accesses the Application. </p>
                  <h3>Derivative Data</h3>
                  <p>Information our surveys automatically collect when you access the Application, such as your native actions that are integral to the Application, include liking, re-blogging, or replying to a post, as well as other interactions with the Application and other users via server log files. </p>
                  <h3>How we use personal data</h3>
                  <p>HomeBar App collects and uses data to enable reliable and convenient products and services. We also use the data we collect:</p>
                  <ul>
                    <li> To enhance the safety and security of our users and services;</li>
                    <li> For customer support;</li>
                    <li> For research and development;</li>
                    <li> To enable communications between users;</li>
                    <li> To send marketing and non-marketing communication to users;</li>
                    <li> In connection with legal proceedings.</li>
                    <p>HomeBar App does not sell or share user personal data with third parties for their direct marketing, except with users’ consent. </p>
                    <p>HomeBar App uses the data it collects for purposes including: </p>

                    <li> Providing services and features
                        <ul>
                        <li> HomeBar App uses the data we collect to provide, personalize, maintain, and improve our products and services.</li>
                        </ul>
                    </li>
                    <li>Safety and Security
                        <ul>
                            <li> We use personal data to help maintain the safety, security, and integrity of our services and users.</li>
                        </ul>
                    </li>
                    <li>Customer Support
                        <ul>
                            <li>HomeBar App uses the information we collect to provide customer support to direct questions to the appropriate customer support person, investigate and address user concerns, and monitor and improve our customer service support responses and processes.</li>
                        </ul>
                    </li>
                    <li>Research and Development
                        <ul>
                            <li>We may use the data we collect for testing, research, analysis, product development, and machine learning to improve the user experience.</li>
                        </ul>
                    </li>
                    <li>Enabling Communications and Marketing
                        <ul>
                            <li>HomeBar App may use the data we collect to market our services to our users. This includes sending users communications about services, features, promotions, sweepstakes, studies, surveys, news, updates, and events.</li>
                        </ul>
                    </li>
                  <br>

                  <p>We may also send communications to our users about products and services offered by partners. We may use the data we collect to personalize the marketing communications (including advertisements) that we send, including based on user location, past use of services, and user preferences and settings.</p>
                  <li>Non-marketing communications
                        <ul>
                            <li>We may use the data we collect to generate and provide users with receipts, inform them of changes to our terms, services or policies; or send other communications that are not for the purpose of marketing the services or products of HomeBar App or its partners.</li>
                        </ul>
                  </li>
                  <li>Legal proceedings and requirements
                        <ul>
                            <li>We may use the personal data we collect to investigate or address claims or disputes relating to the use of services, or as otherwise allowed by applicable laws, or as requested by regulators, government entities, and official inquiries.</li>
                        </ul>
                  </li>
                 </ul>
                 <br>

                  <h3>Cookies and third-party technologies</h3>
                  <h3>HomeBar App and its partners use cookies and other identification technologies on our application, website, emails and online advertisements for purposes described in this notice.</h3>
                  <p>Cookies are small text files that are stored on browsers or devices by websites, applications, online media, and advertisements. HomeBar App uses cookies and similar technologies for purposes such as:</p>
                  <ul>
                    <li>Authenticating users</li>
                    <li>Remembering user preferences and settings</li>
                    <li>Determining the popularity of content</li>
                    <li>Delivering and measuring the effectiveness of advertising campaigns</li>
                    <li>Authenticating users</li>
                    <li>Analyzing site traffic and trends, and generally understanding the online behaviors and interests of people who interact with our services</li>
                   <p>We may also allow others to provide audience measurement and analytics services for us, to serve advertisements on our behalf across the internet, and to track and report on the performance of those advertisements. These entities may use cookies, web beacons, SDKs, and other technologies to identify the devices used by visitors to our website, as well as when they visit other online sites and services.</p>

                  </ul>
                  <h3>Data sharing and disclosure</h3>
                  <p>Some of our products, services, and features require that we share data with other users or at a user’s request. We may also share data with our affiliates, subsidiaries, and partners, for legal reasons or in connection with claims or disputes.</p>
                  <p>HomeBar App may share the data we collect:</p>
                  <ul>
                    <li>With other users</li>
                    <li>At the user’s request</li>
                    <li>With the general public</li>
                    <li>With the account owner</li>
                    <li>With subsidiaries and affiliates</li>
                    <li>With service providers and business partners</li>
                    <li>For legal reasons or in the event of a dispute</li>
                    <li>With consent</li>
                  </ul>
                  <br>
                  <h3>Data retention and deletion</h3>
                  <p>HomeBar App retains user data for as long as necessary for the purposes described above.</p>
                  <p>Users may request deletion of their accounts at any time. We may retain user data after a deletion request due to a legal regulatory requirements or for the reasons stated in this policy.</p>
                  <h3>Grounds for processing</h3>
                  <p>We only collect and use personal data where we have lawful grounds to do so. These include processing user personal data to provide requested services and features, for the purposes of legitimate interests or those of other parties, to fulfill our legal obligations, or based on consent.</p>
                  <h3>Choice and Transparency</h3>
                  <p>HomeBar App enables users to access and control the data that we collect, including through:</p>
                  <ul>
                    <li>In-app settings</li>
                    <li>Device permissions</li>
                    <li>In-app ratings pages</li>
                    <li>Marketing opt-outs</li>
                  </ul>
                  <p>HomeBar App also enables users to request access to or copies of their data, changes or updates to their accounts, deletion of their accounts, or that HomeBar App restrict its processing of user personal data.</p>
                 <h3>Updates to this Notice</h3>
                  <p>We may occasionally update this notice. If we make significant changes, we will notify users in advance of the changes through the application or through other means, such as email. We encourage users to periodically review this notice for the latest information on our privacy practices.</p>
                  <p>After such notice, use of our services y users will be understood as consent to the updates to the extent permitted by law.</p>
              </div>
















          </div>
      </div>


    <footer class="footer py-4 ">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 text-lg-left f-links">

                <a class="mr-3" href="#"> About Us</a>
                    <a class="mr-3" href="#"> Faq’s </a>
                    <a class="mr-3" href="#"> Contact Us</a>
                    <a class="mr-3" href="#"> Help Center </a>
                    <a class="mr-3" href="#"> Support </a>
                    <a class="mr-3" href="{{ url('privacy-policy') }}"> Privacy Policy </a>
                    <a class="mr-3" href="{{ url('terms-and-conditions') }}"> Terms And Conditions </a>

                </div>

                <div class="col-lg-6 my-3 my-lg-0 text-lg-right social">
                    <span>Connect with us</span>
                    <a class="btn  btn-social " href="#!"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a class="btn btn-social " href="#!"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
                    <a class="btn  btn-social " href="#!"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                </div>

            </div>
        </div>
        <div class="col-lg-12 text-lg-center f-bdr mt-5 copy">© Homebar.com 2020. All Rights Reserved.</div>
    </footer>




    <!--    go top-->
    <a href="#" id="gotop" data-aos="fade-in" style="display: inline-block;"> <i class="fa fa-angle-up"> </i> </a>

    <!-- Scripts -->
    <script src="{{asset('assets/front/js/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('assets/front/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
    <!-- aos animation -->
    <script src="{{asset('assets/front/js/aos.js')}}"></script>
    <!-- Owl Carousel -->
    <script src="{{asset('assets/front/js/owl.carousel.min.js')}}"></script>

    <script src="{{asset('assets/front/js/swiper.min.js')}}"></script>
    <script src="{{asset('assets/front/js/who.js')}}"></script>
    <script src="{{asset('assets/front/js/custom.js')}}"></script>

    <script src="{{asset('assets/front/js/bootstrap-select.js')}}"></script>
    <script>
        $(window).scroll(function() {
            if ($(this).scrollTop() > 50) {
                $('#dynamic').addClass('newClass');
            } else {
                $('#dynamic').removeClass('newClass');
            }
        });
    </script>

</body>

</html>
