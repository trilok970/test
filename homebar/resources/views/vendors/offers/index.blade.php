@extends('layouts.vendors.vendor')
@section('title','Vendor Offers | Homebar')
@section('content')

<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 mb-3 ">
        <h2>Discount Offers (For {{$type->name}})</h2>
        <div class="navbar-light left-side-btn ml-auto mr-3">
            <button class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>
        </div>
    </div>
</div>
<div class="row bgw diy-username">
    <div class="col">
        <form action="@if($offer){{route('vendor.offers.update',$offer->id)}}@else{{route('vendor.offers.store')}}@endif" method="POST">
            @if($offer)
                @method('put')
            @endif
            @csrf
            <input type="hidden" name="type_id" value="{{$type->id}}">
            <div class="form-group">
                <label for="discount">Discount%</label>
                <input type="number" name="discount" id="discount" class="form-control @error('discount') is-invalid @enderror" placeholder="Discount Percent" value="@if(old('discount')){{old('discount')}}@elseif($offer){{$offer->discount_per}}@endif" min="0" max="100">
                @error('discount')
                    <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="applied_from">Apply From</label>
                <input type="text" name="applied_from" id="applied_from" class="form-control date @error('applied_from') is-invalid @enderror" placeholder="dd-mm-yyyy" value="@if(old('applied_from')){{old('applied_from')}}@elseif($offer){{ date('d-m-Y',strtotime($offer->applied_from))}}@endif">
                @error('applied_from')
                    <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="applied_to">Apply To</label>
                <input type="text" name="applied_to" id="applied_to" class="form-control date @error('applied_to') is-invalid @enderror" placeholder="dd-mm-yyyy" value="@if(old('applied_to')){{old('applied_to')}}@elseif($offer){{ date('d-m-Y',strtotime($offer->applied_to)) }}@endif">
                @error('applied_to')
                    <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <button type="submit" class="tablebtn">Apply Discount</button>
            </div>
        </form>
    </div>
</div>
@endsection

@push('styles')
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/datepicker.min.css" integrity="sha512-YdYyWQf8AS4WSB0WWdc3FbQ3Ypdm0QCWD2k4hgfqbQbRCJBEgX0iAegkl2S1Evma5ImaVXLBeUkIlP6hQ1eYKQ==" crossorigin="anonymous" /> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />
<style>
    .input-container input {
    border: none;
    box-sizing: border-box;
    outline: 0;
    padding: .75rem;
    position: relative;
    width: 100%;
}

/* input[type="date"]::-webkit-calendar-picker-indicator {
    background: transparent;
    bottom: 0;
    color: transparent;
    cursor: pointer;
    height: auto;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    width: auto;
} */
</style>
@endpush
@push('scripts')
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/datepicker.min.js" integrity="sha512-RCgrAvvoLpP7KVgTkTctrUdv7C6t7Un3p1iaoPr1++3pybCyCsCZZN7QEHMZTcJTmcJ7jzexTO+eFpHk4OCFAg==" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>
  <script>
    $(document).ready(function() {
    //     $("#applied_from").datepicker({dateFormat:'dd/mm/yyyy',autoclose: true});
    //     $("#applied_to").datepicker({dateFormat:'dd/mm/yyyy'}).on('change', function(){
    //     $('.datepicker').hide();
    // });

    $('#applied_from').datepicker({
        startDate: new Date(),
        format:   {
        /*
         * Say our UI should display a week ahead,
         * but textbox should store the actual date.
         * This is useful if we need UI to select local dates,
         * but store in UTC
         */
        toDisplay: function (date, format, language) {
            var d = new Date(date);
            d.setDate(d.getDate());
            return moment(d).format("DD-MM-YYYY");
        },
        toValue: function (date, format, language) {
            var d = new Date(date);
            d.setDate(d.getDate());
            return new Date(d);
        }
    },
        autoclose: true
    });

    $('#applied_to').datepicker({
        startDate: new Date(),
        format:   {
        /*
         * Say our UI should display a week ahead,
         * but textbox should store the actual date.
         * This is useful if we need UI to select local dates,
         * but store in UTC
         */
        toDisplay: function (date, format, language) {
            var d = new Date(date);
            d.setDate(d.getDate());
            return moment(d).format("DD-MM-YYYY");
        },
        toValue: function (date, format, language) {
            var d = new Date(date);
            d.setDate(d.getDate());
            return new Date(d);
        }
    }
    }).on('change', function () {
        $('.datepicker').hide();
    });

    });
</script>
@endpush
