@extends('layouts.vendors.vendor')
@section('title','Do it yourself | Homebar')
@section('content')
<div class="row pag-head ">
    <div class="col-lg-12 sub-btns col-sm-12 col-12">

    </div>
</div>

<div class="row  mix-cocktail">
    <div class="col-lg-6 pr0">
        <img class="img-fluid cocktail-img" width="100%" src="{{asset($item->image)}}">
    </div>

    <div class="col-lg-6 pl0">
        <div class="cocktailright">
            <h3>{{$item->title}}</h3>
            <p class="sprints">{{$type->name}} &nbsp; - {{$category->name}} </p>
            <p>{{$item->description}}
            </p>

            <div class="media mt-3">
                <img class="mr-2 " src="{{asset('images/video-icon.jpg')}}" alt="Generic placeholder image">
                <div class="media-body align-self-center">
                    <h5 class="mt-0">Video URL</h5>
                    <p><a href="{{$item->youtube_url}}" target="_blank">{{$item->youtube_url}}</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <h2 class="ingredients">Ingredients </h2>
        <form action="{{route('vendor.diy.items.store')}}" method="post">
            <input type="hidden" name="type" value="{{$type->slug}}">
            <input type="hidden" name="diy_id" value="{{$item->id}}">
        <div class="table-responsive">
            @csrf
            <table class="table border1px">
                <thead>
                    <tr>
                        <th style="width: 20%;" scope="col">ITEM</th>
                        <th scope="col">QUANTITY</th>
                        <th scope="col">IN STOCK</th>
                        <th scope="col">COST($)</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    
                    function findIngredient($ingredient_id,$user,$item){
                  
                   return $vendorIngredient =  DB::table('vendor_ingredients')->where(['vendor_id'=>$user->id,'diy_id'=>$item->id,'ingredient_id'=>$ingredient_id])->skip(1)->limit(100)->get();

                    }
                    @endphp

                    @foreach ($item->ingredients as $ingredient)

                    
                        @php
                            $ingredientVendors = $ingredient->vendors->pluck('id')->toArray();
                            
                            $currentIngredient = array_key_exists($ingredient->id,$vendorIngredients)? $vendorIngredients[$ingredient->id]:'';
        //                 if($ingredientVendors) {
        //                     echo "<pre>";
        //                     echo $currentIngredient;
        // print_r($ingredientVendors);
        // exit;
        //                 }
                           
                        @endphp
                        <tr id="tr_{{$ingredient->id}}">
                        <input type="hidden" name="ingredient_id[]" value="{{$ingredient->id}}">

                            <th scope="row">{{$ingredient->item_name}}</th>
                            <td >
                                @php
                                 $itemqty = explode(",",$ingredient->item_quantity);
                                @endphp
                                <select class="form-control" name="qty[]" id="qty-{{$loop->iteration}}">
                                @foreach($itemqty as $val)
                                    <option @if(!empty($currentIngredient)) @if(strcmp(strtolower(trim($val)),strtolower(trim($currentIngredient->qty))) === 0) {{'selected'}} @endif @endif>{{$val}}</option>
                                @endforeach
                                </select>
<!--                                 <input type="text" name="qty[]" class="form-control" id="qty-{{$loop->iteration}}" placeholder="Enter QTY" value="{{$ingredient->item_quantity}}" readonly>
 -->                            </td>
                            <td>
                                {{-- <select name="in_stock[]" class="selectpicker customselect form-control in_stock" id="in_stock-{{$loop->iteration}}" data-container="body" data-live-search="true" title="No" data-hide-disabled="true"> --}}
                                    <select name="in_stock[]" class=" customselect form-control in_stock" id="in_stock-{{$loop->iteration}}" data-container="body" data-live-search="true" title="No" data-hide-disabled="true">
                                    <option value="Yes" @if(!empty($currentIngredient) && $currentIngredient->in_stock =='Yes') {{'selected'}} @endif>Yes</option>
                                    <option value="No" @if(empty($currentIngredient)){{'selected'}}@endif @if(!empty($currentIngredient) && $currentIngredient->in_stock =='No') {{'selected'}} @endif>No</option>
                                </select>
                            </td>
                            <td>
                            <input name="cost[]" type="number" class="form-control price" id="cost-{{$loop->iteration}}" placeholder="Price" min="1" max="99999" value="@if(!empty($currentIngredient) && $currentIngredient->cost){{$currentIngredient->cost}}@endif">
                            </td>
                            <td> <button id="{{$ingredient->id}}" class="ingredient_add btn btn-primary btn-sm"><i class="fa fa-plus"></i></button></td>
                            <td>
                                <label class="checked">

                            <input type="checkbox" class="availablity" name="is_available[{{$ingredient->id}}]" id="is-available-{{$loop->iteration}}" @if(in_array($user->id,$ingredientVendors)) {{"checked"}}@else{{'disabled'}} @endif>
                                <span class="checkmark" title="Enter Price to select"></span>
                              </label>
                            </td>
                        </tr>
                        <tbody class="tbody_{{$ingredient->id}}">
                            @if(findIngredient($ingredient->id,$user,$item))
                            @foreach (findIngredient($ingredient->id,$user,$item) as $ingredient1)
                            @php
                            $ingredientVendors = $ingredient->vendors->pluck('id')->toArray();
                            $currentIngredient = array_key_exists($ingredient->id,$vendorIngredients)? $vendorIngredients[$ingredient->id]:'';
                            @endphp
                            <tr id="trnew_{{$ingredient->id}}">
                            <input type="hidden" name="ingredient_id[]" value="{{$ingredient->id}}">

                                <th scope="row">{{$ingredient->item_name}}</th>
                                <td >
                                    @php
                                    $itemqty = explode(",",$ingredient->item_quantity);
                                    @endphp
                                    <select class="form-control" name="qty[]" id="qty-{{$loop->iteration}}">
                                    @foreach($itemqty as $val)
                                <option  @if(strcmp(strtolower(trim($val)),strtolower(trim($ingredient1->qty))) === 0) {{'selected'}} @endif > {{ $val }} </option>
                                    @endforeach
                                    </select>
    <!--                                 <input type="text" name="qty[]" class="form-control" id="qty-{{$loop->iteration}}" placeholder="Enter QTY" value="{{$ingredient1->qty}}" readonly>
    -->                            </td>
                                <td>
                                    {{-- <select name="in_stock[]" class="selectpicker customselect form-control in_stock" id="in_stock-{{$loop->iteration}}" data-container="body" data-live-search="true" title="No" data-hide-disabled="true"> --}}
                                    <select name="in_stock[]" class=" customselect form-control in_stock" id="in_stock-{{$loop->iteration}}" data-container="body" data-live-search="true" title="No" data-hide-disabled="true">
                                        <option value="Yes"  {{ ($ingredient1->in_stock== 'Yes') ? 'selected': "" }} >Yes</option>
                                        <option value="No" {{ ($ingredient1->in_stock== 'No') ? 'selected': "" }}>No</option>
                                    </select>
                                </td>
                                <td>
                                <input name="cost[]" type="number" class="form-control price" id="cost-{{$loop->iteration}}" placeholder="Price" min="1" max="99999" value="{{$ingredient1->cost}}">
                                </td>
                                <td> <button id="{{$ingredient->id}}" class="ingredient_minus btn btn-danger btn-sm"><i class="fa fa-minus"></i></button></td>
                                <td>
                                    <label class="checked">

                                <input type="checkbox" class="availablity" name="is_available[{{$ingredient->id}}]" id="is-available-{{$loop->iteration}}" @if(in_array($user->id,$ingredientVendors)) {{"checked"}}@else{{'disabled'}} @endif>
                                    <span class="checkmark" title="Enter Price to select"></span>
                                </label>
                                </td>
                            </tr>
                               
                        
                            
                           @endforeach
                           @endif
                        </tbody>
                    @endforeach
                </tbody>
            </table>
        </div>

        <button type="submit" class="tablebtn">update</button>
        </form>
    </div>
</div>
@endsection
@push('scripts')

<script>
    
    $(document).ready(function(){
        $('.price').change(function(evt){
            var price = $(this).val();
            if(price > 0) {
                $(this).closest('tr').find('.availablity').removeAttr('disabled');
            }else {
                $(this).closest('tr').find('.availablity').attr('disabled','disabled')
            }
        });
        $(document).on('click','.ingredient_add',function(evt){
            evt.preventDefault();
            console.log("add");
            var id = $(this).attr('id');
            var divClone = $("#tr_"+id).clone(true);
            $(divClone).find(".ingredient_add").removeClass("ingredient_add").addClass("ingredient_minus_main");
            $(divClone).find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
            $(divClone).find(".btn-primary").removeClass("btn-primary").addClass("btn-danger");
            
            

            $(".tbody_"+id).append(divClone);
        });
        $(document).on('click','.ingredient_minus_main',function(evt){
            evt.preventDefault();
            var id = $(this).attr('id');
            console.log("id "+ id);

            console.log($(this).parent("tr").html());
            $(this).closest("tr").remove();
        });
        $('.ingredient_minus').click(function(evt){
            evt.preventDefault();
            var id = $(this).attr('id');
            $("#trnew_"+id).remove();
        });
    });
</script>
@endpush
