@extends('layouts.vendors.vendor')
@section('title','Do it Yourself | Homebar')
@section('content')

<div class="row pag-head ">
    <div class="col-lg-12 sub-btns col-sm-12 col-12">
        <a href="{{route('vendor.diy.index',[$type->slug])}}" @if(!$category->id)class="active"@endif>All</a>
        @foreach ($categories as $cat)
            <a href="{{route('vendor.diy.index',[$type->slug,$cat->slug])}}" class="{{$cat->id == $category->id?'active':''}}">{{$cat->name}}</a>
        @endforeach
    </div>
</div>

@forelse ($items as $item)
    @php
        $isIngredientAvailable = false;
    @endphp
    @foreach ($item->ingredients as $ingredient)
        @foreach ($ingredient->vendors as $vendor)
            @if($vendor->id === $user->id)
                @php
                    $isIngredientAvailable = true;
                    break;
                @endphp
            @endif
        @endforeach
    @endforeach
    <div class="row bgw diy-username">
    <div class=" col-lg-6 col-sm-6 col-10 ">
        <div class="media proimg">
            <span class="mr-3"><img  src="{{asset($item->image)}}"></span>
            <div class="media-body">
                <!-- if($item && $type && $item->cateogry) -->
                   <h5 class="mt-0"><a href="{{route('vendor.diy.receipe',[$type->slug,$item->category->slug ?? '' ,$item->slug])}}">{{$item->title}}</a></h5>
                <!-- endif -->
                @if($item)
                    <p class="totalitem">Ingredients: <span>{{$item->ingredients->pluck('item_name')->join(", ")}}</span></p>
                @endif
            </div>
        </div>

    </div>

    <div class=" txtr  col-lg-6 col-sm-6 col-2 unstyled centered">
        @if($isIngredientAvailable)
        <label class="checked">
            <input type="checkbox" @if($isIngredientAvailable)checked="checked"@endif disabled>
            <span class="checkmark"></span>
        </label>
        @endif
    </div>
</div>
@empty
    <div class="row bgw">
        <div class=" col-lg-6 col-sm-6 col-10 ">
            <p class="mt-3">No Item found in this category.</p>
        </div>
    </div>
@endforelse

@endsection
