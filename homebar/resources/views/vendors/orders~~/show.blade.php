@extends('layouts.vendors.vendor')
@section('title','Order Detail | Homebar')
@section('content')

<div class="row pag-head">
    <div class="flex  justify-content-start col-lg-6 col-sm-12 col-12 ">
        <div class="backbtn"><a href="{{route('vendor.orders',request()->segment(6))}}"><i class="fa fa-angle-left" aria-hidden="true"></i></a></div>
        <h2>Order Details</h2>
        <div class="navbar-light left-side-btn ml-auto mr-3">
            <button class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>
        </div>
    </div>
    <div class="flex containtstart-mob  justify-content-end col-lg-6 col-sm-12 col-12 ">
    </div>
    <div class="col-md-12">
        <hr class="hrline">
    </div>
</div>

<div class="row  orderid">
    <div class="col-lg-12">
        <div class="bg-w pdd15">
            <div class="row">
                <div class="justi    col-lg-8 col-sm-7 col-12 ">
                    <p class="myid pt-3">Order ID : {{$order->order_number}} | {{date('M d, Y, H:i A',strtotime($order->created_at))}} @if($order->reach_type !== 'PICKUP') | Delivery Date:
						   @if($order->order_type !== 'BARTENDER')
								@if(!empty($order->delivery_date))
									{{date('M d, Y, H:i A',strtotime($order->delivery_date))}}
								@endif
							@else
								@if($order->orderItems())
								{{date('M d, Y',strtotime($order->orderItems()->first()->book_date)).' From '.date('H:i A',strtotime($order->orderItems()->first()->from_time)).' to '.date('H:i A',strtotime($order->orderItems()->first()->to_time))}}
								@else
									{{'N/A'}}
								@endif
							@endif
						@endif
                    </p>
                </div>

                @if($order->order_type !== 'BARTENDER')
                <div class="  txtr col-lg-4 col-sm-5 col-12 ">
                    <button class="deliverbtn">{{$order->reach_type}} </button>
                </div>
                @endif
                <hr>
            </div>
        </div>
    </div>
</div>
<div class="row  username">
    <div class="col-lg-12">
        <div class="bg-w pdd15">
            <div class="row">
                <div class=" col-lg-6 col-sm-6 col-12 ">
                    <div class="media proimg">
                        <span class="mr-3"><img  src="@if(!empty($order->user->logo_image)){{asset($order->user->logo_image)}} @else{{asset('images/default_user.png')}}@endif"></span>
                        <div class="media-body">
                            <h5 class="mt-0 mt-1">{{$order->user->first_name.' '.$order->user->last_name}}</h5>
                            <p class="totalitem mb-2">{{$order->user->primary_mobile_number}}</p>
                        </div>
                    </div>

                </div>

                <div class=" txtr  col-lg-6 col-sm-6 col-12 ">
                    <div class="acc">{{$singleItem->status}}</div>
                    <p class="packed">Updated at : {{date('M d, y H:i A',strtotime($order->updated_at))}}</p>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table order-dtail-table border1px">
                <thead>
                    <tr>
                        <th style="width: 40%;" scope="col"> PRODUCT</th>
                        @if($order->order_type === 'PREMADE')
                        <th scope="col">SIZE</th>
                        @endif
                        <th scope="col">QTY</th>
                        <th scope="col">PRICE</th>
                        @if($order->order_type === 'BARTENDER')
                            <th scope="col">TIME (IN HOURS)</th>
                        @endif
                        <th scope="col">TOTAL</th>
                        @if($order->order_type === 'PREMADE')
                        <th scope="col">SPECIAL INSTRUCTIONS</th>
                        @endif
                        {{-- <th scope="col">STATUS</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @php
                        $order_total = 0;
                        $order_total_tax = 0;
                        $order_total_service_fee = 0;
                        $order_total_admin_commission = 0;
                    @endphp
                    @if($order->order_type === 'PREMADE')
                        @foreach ($order->items as $item)
                        @if($item->restaurant_id ==auth()->user()->id)
                        <tr>
                            <th scope="row">
                                <div class="media tbl-img">
                                    <span class="align-self-center mr-3"><img src="@if($item->menuItemDetail && $item->menuItemDetail->images->first()){{asset($item->menuItemDetail->images->first()->image)}}@endif"></span>
                                    <div class="media-body">
                                        <h5 class="mt-2">@if($item->menuItemDetail){{$item->menuItemDetail->name}}@endif</h5>
                                        <p class="totalitem">{{$item->description}}</p>
                                    </div>
                                </div>

                            </th>
                            <td><p>@if($item->menuItemDetail){{$item->menuItemDetail->size}}@endif</p></td>
                            <td>
                                <p>{{$item->quantity}}</p>
                            </td>
                            <td>
                                <p>${{number_format($item->price,2,'.',',')}}</p>
                            </td>
                            <td>
                                <p>${{number_format($item->quantity * $item->price,2,'.',',')}}</p>
                            </td>
                            <td>
                                <p>{{$item->special_instructions}}</p>
                            </td>

                            {{-- <td>
                                <p>
                                    <select name="order_items_status" class="order_items_status" data-order-id="{{$item->order_id}}" data-url={{route('vendor.order-items.updateOrderItemsStatus',$item->id)}}>
                                        <option value="NEW" @if($item->status==='NEW'){{'selected'}}@endif>NEW</option>
                                        <option value="ACCEPTED" @if($item->status==='ACCEPTED'){{'selected'}}@endif>ACCEPTED</option>
                                        <option value="REJECTED" @if($item->status==='REJECTED'){{'selected'}}@endif>REJECTED</option>
                                        <option value="CANCELLED" @if($item->status==='CANCELLED'){{'selected'}}@endif>CANCELLED</option>
                                        <option value="IN KITCHEN" @if($item->status==='IN KITCHEN'){{'selected'}}@endif>IN KITCHEN</option>
                                        @if($order->reach_type == 'DELIVERY')
                                            <option value="START FOR DELIVERY" @if($item->status==='START FOR DELIVERY'){{'selected'}}@endif>START FOR DELIVERY</option>
                                            <option value="DELIVERED" @if($item->status==='DELIVERED'){{'selected'}}@endif>DELIVERED</option>
                                        @endif
                                        @if($order->reach_type == 'PICKUP')
                                            <option value="READY TO PICKUP" @if($item->status==='READY TO PICKUP'){{'selected'}}@endif>READY TO PICKUP</option>
                                            <option value="PICKUPED UP">PICKED UP</option>
                                        @endif
                                    </select>
                                </p>
                            </td> --}}

                        </tr>
                        @php
                            $order_total += ($item->quantity * $item->price);
                            $order_total_tax += $item->tax;
                            $order_total_service_fee += $item->service_fee;
                            $order_total_admin_commission += $item->admin_commission;
                        @endphp
                        @endif
                        @endforeach
                    @else
                        @php
                            $order_total = 0;
                        @endphp
                        @if($order->order_type === 'BARTENDER')
                            @foreach ($order->items as $item)
                                @if(!empty($item))
                                <tr>
                                    <th scope="row">
                                        <div class="media tbl-img">
                                            <div class="media-body">
                                                <h5 class="mt-0">Bartender</h5>
                                            </div>
                                        </div>
                                    </th>
                                    <td>
                                        <p>{{$item->quantity}}</p>
                                    </td>
                                    <td>
                                        <p>${{number_format($item->price,2,'.',',')}}</p>
                                    </td>
                                    <td>
                                        <p>{{$item->time_in_hours}}</p>
                                    </td>
                                    <td>
                                        <p>${{number_format($item->price * $item->time_in_hours,2,'.',',')}}</p>
                                    </td>
                                </tr>
                                @php
                                    $order_total += ($item->price * $item->time_in_hours);
                                    $order_total_tax += $item->tax;
                                    $order_total_service_fee += $item->service_fee;
                                    $order_total_admin_commission += $item->admin_commission;
                                @endphp
                                @endif
                            @endforeach
                        @else
                            @foreach ($order->items as $item)
                                @if(!empty($item) && !empty($item->ingredientDetail))
                                    @if($item->restaurant_id ==auth()->user()->id)
                                    <tr>
                                        <th scope="row">
                                            <div class="media tbl-img">
                                                <div class="media-body">
                                                    <h5 class="mt-0">{{$item->ingredientDetail->item_name}}</h5>
                                                </div>
                                            </div>
                                        </th>
                                        <td>
                                            <p>{{$item->quantity}}</p>
                                        </td>
                                        <td>
                                            <p>${{number_format($item->price,2,'.',',')}}</p>
                                        </td>
                                        <td>
                                            <p>${{number_format($item->quantity * $item->price,2,'.',',')}}</p>
                                        </td>
                                    </tr>
                                    @php
                                        $order_total += ($item->quantity * $item->price);
                                        $order_total_tax += $item->tax;
                                        $order_total_service_fee += $item->service_fee;
                                        $order_total_admin_commission += $item->admin_commission;
                                    @endphp
                                    @endif
                            @endif
                            @endforeach
                        @endif
                    @endif
                </tbody>
            </table>

        </div>
    </div>
</div>

@if(!empty($order->special_instruction))
<div class="row mt-3">
    <div class="col">
        <div class="bg-w pdd15">
            <div class="row">
                <div class="col">
                    <p class="pt-3">Special Instructions</p>
                    <p>{{$order->special_instruction}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@if(($singleItem->status !== 'DELIVERED' && $singleItem->status !== 'REJECTED' && $singleItem->status !== 'PICKED UP') && $order->order_type !== 'BARTENDER')
<div class="row mt-3">
    <div class="col">
        <div class="bg-w pdd15 status_section">
            <div class="row">
                <div class="col">
                    <p class="pt-3 select_status">Update Order Status</p>
                    <form action="{{route('vendor.order.updateOrderStatus',$order->id)}}" method="POST" class="form-inline" id="update-order-status">
                        @csrf
                        <div class="form-group">
                            <select name="status" id="status" class="form-control mr-3 @error('status') is-invalid @enderror">
                                <option value="">Select Status</option>
                            @if($singleItem->status === 'NEW')
                                <option value="ACCEPTED" @if($singleItem->status === 'ACCEPTED') {{"selected"}}@endif>Accept</option>
                                <option value="REJECTED" @if($singleItem->status === 'REJECTED') {{"selected"}}@endif>Reject</option>
                            @else
                                <option value="IN KITCHEN" @if($singleItem->status === 'IN KITCHEN') {{"selected"}}@endif>In Kitchen</option>
                                <option value="READY TO PICKUP" @if($singleItem->status === 'READY TO PICKUP') {{"selected"}}@endif>READY TO PICKUP</option>
                                @if($order->reach_type === 'PICKUP')
                                    <option value="PICKED UP" @if($singleItem->status === 'PICKED UP') {{"selected"}}@endif>PICKED UP</option>
                                @else if($order->reach_type === 'DELIVERY')
                                    <!-- <option value="DELIVERED" @if($singleItem->status === 'DELIVERED') {{"selected"}}@endif>DELIVERED</option> -->
                                @endif
                            @endif
                            </select>
                            @error('status')
                                <div class="invalid-feedback">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" name="sub" class="new-btn">Update Status</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@elseif($singleItem->status == 'NEW' && $order->order_type == 'BARTENDER')

<div class="row mt-3">
    <div class="col">
        <div class="bg-w pdd15 status_section">
            <div class="row">
                <div class="col">
                    <p class="pt-3 select_status">Update Order Status</p>
                    <form action="{{route('vendor.order.updateOrderStatus',$order->id)}}" method="POST" class="form-inline" id="update-order-status">
                        @csrf
						<input type="hidden" name="type" value="bartender">
                        <div class="form-group">
                            <select name="status" id="status" class="form-control mr-3 @error('status') is-invalid @enderror">
                                <option value="">Select Status</option>
                                <option value="ACCEPTED" @if($singleItem->status === 'ACCEPTED') {{"selected"}}@endif>Accept</option>
                                <option value="REJECTED" @if($singleItem->status === 'REJECTED') {{"selected"}}@endif>Reject</option>

                            </select>
                            @error('status')
                                <div class="invalid-feedback">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" name="sub" class="new-btn">Update Status</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endif
<div class="row mt-3">
    <div class="col-lg-6">
        <div class="row ">
            @if($order->reach_type === 'DELIVERY')
             @if($order->driverDetails())
            <div class="col-lg-12 ">
                <div class="bg-w driver">
                    <h3>Assigned Driver</h3>
                    <div class="row">
                        <div class="col d-flex">
                            <div class="media tbl-img">
                                <span class=" mr-2"><img src="@if($order->driverDetails()->profile_image) {{asset($order->driverDetails()->profile_image)}} @else{{asset('images/default_user.png')}}@endif"></span>
                                <div class="media-body">
                                    <h5 class="mt-0">{{$order->driverDetails()->first_name.' '.$order->driverDetails()->last_name}}</h5>
                                    <p class="totalitem">{{$order->driverDetails()->phone_country_code}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="col align-self-end">
                            <p class="vehicle-no">Vehicle Number :<span>{{$order->driverDetails()->vehicleInfo->vehicle_number}}</span> </p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @endif
            <div class="col-lg-12 visa">
                <div class="bg-w driver">
                    <div class="row">
                        <div class="col  tbl-img">
                            <p class="totalitem mt-0">Order Paid By </p>
                            <h5 class="mt-0">VISA Credit Card</h5>

                        </div>

                        <div class="col text-right align-self-end">
                            <img class="ml-auto" src="{{asset('assets/front/images/visa.png')}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6  ">
    @php
            $orderAmountInfo = $order->getItemsTotalForAll();
            $sub_total = $orderAmountInfo['sub_total'];
            $total_tax = $orderAmountInfo['total_tax'];
            $total_service_fee = $orderAmountInfo['total_service_fee'];
            $total_admin_commission = $orderAmountInfo['total_admin_commission'];
            $grand_total = $orderAmountInfo['grand_total'];
            @endphp
        <div class="table-responsive">
            <table class="table order-dtail-table border1px @if($order->reach_type === 'PICKUP'){{"mt-3"}}@endif">

                <tbody class="totalorder">
                    <tr>
                        <td scope="row">
                            <p>Subtotal</p>
                        </td>
                        <td>


                            <p>${{number_format($sub_total,2,'.',',')}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row">
                            <p>Tax</p>
                        </td>
                        <td>


                            <p>+ ${{number_format($total_tax,2,'.',',')}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row">
                            <p>Service Charge</p>
                        </td>
                        <td>
                            <p>+ ${{number_format($total_service_fee,2,'.',',')}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row">
                            <p>Admin Commission</p>
                        </td>
                        <td>
                            <p>- ${{number_format($total_admin_commission,2,'.',',')}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row">
                            <p><b>Total</b></p>
                        </td>
                        <td>
                        @php  $total_amount = $grand_total;   @endphp

                            <p><b>${{number_format($total_amount,2,'.',',')}}</b></p>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.1/socket.io.js" integrity="sha512-AcZyhRP/tbAEsXCCGlziPun5iFvcSUpEz2jKkx0blkYKbxU81F+iq8FURwPn1sYFeksJ+sDDrI5XujsqSobWdQ==" crossorigin="anonymous"></script>
<script>
    // $(document).on('change','.order_items_status',function() {
    //     if(confirm("Are you sure to confirm the status?")) {
    //         var orderID = $(this).data('order-id');
    //         var status = $(this).val();
    //         var url = $(this).data('url');

    //         $.post(url,{orderId: orderID,status:status},function(out){

    //            if(out.status === 1) {
    //                 var socket = io("http://testing.demo2server.com:5059", {query: "user_id="+out.vendorId});
    //                 var status_update_request =
    //                 {
    //                     "order_id": orderID,
    //                     "restaurant_id": out.orderItem.restaurant_id,
    //                     "status": status,
    //                     "driver_id":out.orderItem.driver_id,
    //                 };
    //                 socket.emit('vendor_status_update', status_update_request);
    //             }
    //             alert(out.msg);
    //         });
    //     }
    // });

    $(document).on('submit','#update-order-status',function(evt) {
        evt.preventDefault();

        var url = $(this).attr('action');
        var postData = $(this).serialize();
        var selected_status = $('#status').val();
        var status = $("#status").val().toLowerCase();
        if(status)
            status = "Do you want to change status of this order to "+status+"?";
        else
            status = "Do you want to change status of this order?";
        swal({
        title: "Are you sure?",
        text: status,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, change it!",
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {

        $("#status").attr('disabled',true);
             $.post(url,postData,function(out) {
            if(out.status == 1) {
                // alert(out.msg);
                // swal ( "Success" ,  out.msg+"!" ,  "success" );

                var socket = io("http://testing.demo2server.com:5059", {query: "user_id="+out.orderItem.restaurant_id});
                var status_update_request =
                {
                    "order_id": out.orderItem.order_id,
                    "restaurant_id": out.orderItem.restaurant_id,
                    "status": selected_status,
                    "driver_id":out.orderItem.driver_id,
                };

                socket.emit('vendor_status_update', status_update_request, (res)=>{
                     window.location.reload();
                   // var segment = "{{request()->segment(6)}}";
                   // location.href="{{url('vendor/orders/recent/')}}"+'/'+segment;
                });

            } else {
                swal ( "Opps" ,  out.msg+"!" ,  "error" );

                // alert(out.msg);
            }
        });
            swal("Success!", out.msg, "success");
        } else {
            swal("Cancelled", "Your order status is not changed :)", "error");
        }
    }
);





    });
</script>
@endpush
