@extends('layouts.vendors.vendor')
@section('title','Vendor Orders | Homebar')
@section('content')
@php
    $url_params = request()->route()->parameters;
    $type = isset($url_params['type']) ? $url_params['type'] : '';
@endphp
<div class="row pag-head">



    <div class="col-12 col-12 px-0 ye px-0">
    <div class="row orders-row">
         @foreach($statusCountResult as $key=>$value)
            <div class="col-lg-3 col-sm-6 col-6 mt-4">
                <div class="orders-col">
                    <p class="myid">{{$key}} Order</p>
                    <p class="myid">{{$value}} </p>
                </div>
            </div>
            @endforeach
   </div>
   </div>

    <div class="flex col-lg-12 col-sm-12 col-12  pdL-l0 pdr-0">
        <h2>{{$orderPageTitle}}</h2>
        <div class="navbar-light left-side-btn ml-auto mr-3">
            <button class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>
        </div>



        <!-- ./col -->

        <!-- <a href="drink-add-menu-pre.html" class="nowbtn pr-5 pl-5"> ADD MENU </a> -->
    </div>
</div>
@forelse ($orders as $order)
<a href="{{route('vendor.order.show',[request()->segment(3),$order->id,$type])}}">
    <div class="row bgw orderid">
        <div class="justi    col-lg-8 col-sm-7 col-8 ">
            <p class="myid">Order Number : {{$order->order_number}} <em>|</em> {{date('M d, Y, H:i A',strtotime($order->created_at))}} @if($order->reach_type !== 'PICKUP') | Delivery Date:
					@if($order->order_type !== 'BARTENDER')
						@if(!empty($order->delivery_date))
							{{date('M d, Y, H:i A',strtotime($order->delivery_date))}}
						@endif
					@else
						@if($order->orderItems())
						{{date('M d, Y',strtotime($order->orderItems()->first()->book_date)).' From '.date('H:i A',strtotime($order->orderItems()->first()->from_time)).' to '.date('H:i A',strtotime($order->orderItems()->first()->to_time))}}
						@else
							{{'N/A'}}
						@endif
					@endif
				@endif
            </p>
        </div>
        @if($order->order_type !== 'BARTENDER')
            <div class="  txtr col-lg-4 col-sm-5 col-4 ">
                <button class="deliverbtn">{{$order->reach_type}} </button>
            </div>
        @endif
        <hr>
    </div>
</a>
    <div class="row bgw username">
        <div class=" col-lg-6 col-sm-6 col-12 ">
            <a href="{{route('vendor.order.show',[request()->segment(3),$order->id,$type])}}">
            <div class="media proimg">
                <span class="mr-3"><img  src="@if(!empty($order->user->logo_image)){{asset($order->user->logo_image)}} @else{{asset('images/default_user.png')}}@endif"></span>
                <div class="media-body">
                    <h5 class="mt-0">@if($order->user){{$order->user->first_name.' '.$order->user->last_name}}@endif</h5>
                    {{-- <p class="totalitem">Number of Items : <b>@if($order->order_type==='DIY'){{count($order->ingredients())}}  @else {{$order->getItemsCount()}} @endif</b> <em>|</em> --}}
                    <p class="totalitem">Number of Items : <b> {{$order->getItemsCount()}}</b> <em>|</em>

                    Total Price: <b>
                        @php
                        $orderAmountInfo = $order->getItemsTotalForAll();
                        $grand_total = $orderAmountInfo['grand_total'];
                        @endphp
                        @if($order->order_type==='BARTENDER')
                        @php
                       // $total_amount = (($order->sub_total + $order->tax + $order->service_fee) - $order->admin_commission);
                        $total_amount = $grand_total;   @endphp

                         {{'$'.$total_amount}}
                        @else
                        @php
                       //$total_amount = (($order->getItemsTotal() + $order->tax + $order->service_fee) - $order->admin_commission);
                        $total_amount = $grand_total;

                        @endphp

                        ${{number_format($total_amount,2,'.',',')}}  @endif</b></p>
                </div>
            </div>
            </a>
        </div>

        <div class=" txtr  col-lg-6 col-sm-6 col-12 ">
            @if($order->items()->where('restaurant_id',auth()->user()->id)->first()->status === 'NEW')
                <button class="acceptbtn accept-btn">Accept </button>
                <button class="nowbtn reject-btn">Decline</button>
                <form action="{{route('vendor.order.acceptOrder')}}" method="POST" class="accept-form" style="display: none">
                    @csrf
                    <input type="hidden" class="order_id" name="order_id" value="{{$order->id}}">
                    <input type="hidden" name="type" value="{{$type}}">
                </form>

                <form action="{{route('vendor.order.rejectOrder')}}" method="POST" class="reject-form" style="display: none">
                    @csrf
                    <input type="hidden" name="order_id" value="{{$order->id}}">
                    <input type="hidden" name="order_type" value="{{$order->order_type}}">
                    <input type="hidden" name="type" value="{{$type}}">
                </form>
            @else
                <?php /*?><span class="@if(Request::is('vendor/orders/progress/*') || Request::is('vendor/orders/completed/*')){{'text-success'}} @else{{'text-danger'}}@endif">{{$order->items()->where('restaurant_id',auth()->user()->id)->first()->status}}</span><?php */?>
            @endif
        </div>
    </div>

@empty
    <div class="row bgw mt-3">
        <div class="col pt-3">
            <p>No Order Found.</p>
        </div>
    </div>
@endforelse
<br>
{{$orders->links()}}
@endsection
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.1/socket.io.js" integrity="sha512-AcZyhRP/tbAEsXCCGlziPun5iFvcSUpEz2jKkx0blkYKbxU81F+iq8FURwPn1sYFeksJ+sDDrI5XujsqSobWdQ==" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $(".accept-btn").click(function(evt){
                evt.preventDefault();
                var url = $(this).siblings('form.accept-form').attr('action');
                var postData = $(this).siblings('form.accept-form').serialize();




                 swal({
        title: "Are you sure?",
        text: "Do you want to accept this order?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, accept it!",
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {

        $.post(url,postData,function(out){
                    if(out.status) {
                        var socket = io("http://testing.demo2server.com:5059", {query: "user_id="+out.vendor_id});
                        var status_update_request =
                        {
                            "order_id": out.orderItem.order_id,
                            "restaurant_id": out.vendor_id,
                            "status": out.orderItem.status,
                            "driver_id":out.orderItem.driver_id,
                        };
                        socket.emit('vendor_status_update', status_update_request);
                        window.location.reload();
                    }else {
                swal ( "Opps" ,  out.msg+"!" ,  "error" );
                        // alert(out.msg);
                    }
                });

            swal("Success!", out.msg, "success");
        } else {
            swal("Cancelled", "Your order status is not changed :)", "error");
        }
    }
);


            });

            $(".reject-btn").click(function(evt){
                evt.preventDefault();
                var url = $(this).siblings('form.reject-form').attr('action');


                var postData = $(this).siblings('form.reject-form').serialize();

                  swal({
        title: "Are you sure?",
        text: "Do you want to reject this order?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, reject it!",
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {

         $.post(url,postData,function(out){
                    console.log(out);
                    if(out.status) {
                        window.location.reload();
                    } else {
                        alert(out.msg);
                    }
                });

            swal("Success!", out.msg, "success");
        } else {
            swal("Cancelled", "Your order status is not changed :)", "error");
        }
    }
);



				//console.log(postData);

            });
        });

    </script>
@endpush
