<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="{{asset('assets/front/images/favicon.jpg')}}" type="image/jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Terms and Conditions</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!-- Custom -->
    <link rel="stylesheet" href="{{asset('assets/front/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/swiper.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/responsive-index.css')}}">

</head>

<body id="page-top">
    <!-- Navigation-->
    <div class="row fixed-top" id="dynamic">
        <div class="container-fluid  ">
            <div class="row mt-3">
                <div class="col-lg-6 col-5 ">
                    <img src="{{asset('assets/front/images/logo.png')}}">
                </div>
                @if (!Session::has('VendorLoggedIn'))
                <div class="col-lg-6 col-7 text-right">
                    <a href="{!! route('vendor.login') !!}" class="btn signin"> Sign in </a>
                    <a href="{!! route('vendor.signup') !!}" class="btn signup ml-2"> Sign Up </a>
                </div>
                @else
                <div class="col-lg-6 col-7 text-right">
                    <a href="{!! route('vendor.orders','drinks') !!}" class="new-btn"> Dashboard</a>
                </div>
                @endif
            </div>
        </div>
    </div>

    <div class="terms-banner">

    </div>


    <div class="container padding-top">
          <div class="row black-area">
              <div class="col-12">
                <h2  class="h2style text-center">TERMS & CONDITIONS</h2>
                <h6 class="text-center">PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY</h6>
              </div>
              <div class="col-12 text-area">
                 <h3>AGREEMENT TO TERMS</h3>
                  <p>These Terms and Conditions constitute a legally binding agreement made between User, whether personally or on behalf of an entity(“you”) and <b>HOMEBAR APP</b> (“we”, “us” or “our”), concerning your access to and use of the <b>HomeBar App</b> and HomeBar.com website as any other media form, media channel, mobile website or mobile application related, linked, or otherwise connected thereto (collectively, the “site”). User agrees that by accessing the Site, you have read, understood, and agree to be bound by all Terms and Conditions as set forth in here within.  Further, by continuing access of app, the User agrees and certifies that they are, at minimum, 21 years of age or older and are fully able and competent to understand and agree to the terms, conditions, obligations, affirmations, representation, and warranties as set forth in this Agreement.</p>
                  <p>IF YOU DO NOT AGREE WITH TERMS and CONDITIONS, THEN YOU ARE EXPRESSLY PROHIBITED FROM USING THE SITE AND MUST DISCONTINUE USE IMMEDIATELY.</p>
                  <p>Supplemental terms and conditions or documents that may be posted on the Site from time to time are hereby expressly incorporated herein by reference.  We reserve the right, in our sole discretion, to make changes or modifications to these Terms and Conditions at any time and for any reason. We will alert you about any changes by updating the “Last updated” date of these Terms and Conditions.  User’s may waive any right to receive specific notice of such changes; however, it is ultimately the User’s responsibility to periodically review these Terms and Conditions to stay informed of updates, even while accepting said alerts.  You will be subject to, and will be deemed to have been made aware of and to have accepted, the revised Terms and Conditions by your continued use of the Site after date in which such revised Terms are posted.  The User may be required to install certain upgrades or updates to the software in order to continue access or use the Services, or portions thereof (including upgrades or updates designed to correct issues with the Services). Any updates or upgrades provided to you by us under the term and conditions shall be considered part of the Services.</p>
                  <p>The information provided on the Site is not intended for distribution to or use by any person or entity in any jurisdiction, state or country where such distribution or use would be contrary to law or regulation or which would subject us to any registration requirement within such jurisdiction or country. Accordingly, those persons who choose to access the Site from other locations do so on their own initiative and are solely responsible for compliance with local laws, if and to the extent local laws are acceptable.</p>
                  <h3>1: ELIGIBILITY</h3>
                  <p>The Site in intended for users who are at least 21 years old. Persons under the age of 21 are not permitted to register for the Site.</p>
                  <h3>2: Contractual Relationship</h3>
                  <p>These Terms do not supersede or otherwise impact the enforceability of any agreements you may have with <b>HomeBar App</b> or its subsidiaries. To the extent any agreement you may have with <b>HomeBar App</b> regarding its services conflicts with these Terms, those agreements (and not these Terms) will prevail.</p>
                  <p><b>HomeBar App</b> may immediately terminate these Terms or any services with respect to you, or cease offering or deny access to the Services or any portion thereof, at any time for any reason.</p>
                  <p>Supplemental Terms may apply to certain Services, such as policies for a particular event, program, activity, or promotion, and supplemental terms will be disclosed to you. Supplemental terms are in addition to, and shall be deemed a part of, the Terms for purposes of the applicable service(s).</p>
                  <p><b>HomeBar App</b> may amend the Terms periodically. Amendments will be effective upon posting of updated Terms at this location or in the amended policies or supplemental terms on the applicable Service. Your continued access to use the services after such posting confirms your consent to be bound by the Terms, as amended.</p>
                  <h3>3: Arbitration Agreement</h3>
                  <p>By agreeing to these Terms, you agree that you are required to resolve any claim that you may have on an individual basis in arbitration, as set forth in this Agreement. This will preclude you from bringing any class, collective, or representative action against <b>HomeBar App</b>.</p>
                  <h3>Agreement to Binding Arbitration</h3>
                  <p>You agree that any dispute, claim or controversy that arises related to these Terms, or your access, will be settled by binding arbitration between you and <b>HomeBar App</b>, and not in a court of law. You acknowledge and agree that you and <b>HomeBar App</b> are each waiving the right to a trial by jury or to participate as a plaintiff or class member in any purported class action or representative proceeding.</p>
                  <h3>4: The Services</h3>
                  <p>The Services comprise mobile applications and related services, which enable users to arrange and schedule Home Bar services and related services including logistics, and/or delivery of services and/or purchase certain goods, including with third party providers.  It is the intent of the <b>HomeBar App</b> to clarify that it is neither a producer, seller or merchant of food or beverages but only facilitates orderings, sales and purchase of food, beverages and other supplies against partnering vendors.</p>
                  <h3>License</h3>
                  <p>Subject to your compliance with these Terms, <b>HomeBar App</b> grants you a limited, non-exclusive, non-sublicensable, revocable, non-transferrable license to: (i) access and use the Application on your personal device solely in connection with your use of the Services; and (ii) access and use of any content, information or related materials that may be made available through the Services, in each case solely for your personal, noncommercial use. Any rights not expressly granted herein are reserved by <b>HomeBar App</b> and <b>HomeBar App</b>’s licensors.</p>

                  <h3>Restrictions:</h3>
                  <p>A. Remove any copyright, trademark or other proprietary notices from any portion of the Services;</p>
                  <p>B. Reproduce, modify, prepare derivative works, distribute, license, lease, sell, resell, transfer, publicly display, publicly perform, transmit, stream, broadcast or otherwise exploit the Services, except as expressly permitted;</p>
                  <p>C. Decompile, reverse engineer, or disassemble the Services except as may be permitted;</p>
                  <p>D. Link to, mirror or frame any portion of the Services;</p>
                  <p>E. Cause or launch any programs for the purpose of mining data, burdening or hindering operation or functionality any aspect of the Services;</p>
                  <p>F. Attempt to gain unauthorized access to or impair any aspect of the Services or its related systems or networks.</p>
                  <h3>Provision of the Services</h3>
                  <p>You acknowledge that portions of the Services may be made available under such brands or request options by or in connection with subsidiaries and affiliates or independent Third Party Providers.</p>
                  <h3>5: Access and Use of the Services</h3>
                  <h3>User Accounts</h3>
                  <p>In order to use most aspects of the Services, you must register for, and maintain, an active personal user Services account (“Account”). You must be at least 21 years of age to obtain an account, unless a specific service permits otherwise. Account registration requires you to provide certain personal information, such as your name, address, mobile phone number and age, and at least one valid payment method. You agree to maintain accurate, complete and up to date Account information. You are responsible for all activity that occurs under your Account, and you agree to maintain the secrecy of your username and password at all times. You may only possess one account.</p>
                  <h3>User Requirements and Conduct</h3>
                  <p>This Service is not available for use by persons under the age of 21. You may not authorize third parties to use your Account, and you may not allow persons under the age of 21 to receive services unless they are accompanied by you. You cannot assign or transfer your Account to any other person or entity. You agree to comply with all applicable laws.</p>
                  <p>You agree to be contacted by <b>HomeBar App</b> by telephone or text message at any of the phone numbers provided by you or on your behalf in connection with your account, including for marketing purposes. <b>HomeBar App</b> may permit you to submit, upload, publish, or make available textual, audio and/or visual content and information, including commentary or feedback related to the Services.</p>
                  <h3>User Cancellation</h3>
                  <p>The User can delete his/her account at any time according to instructions as provided within the app or by contacting HBA directly via email through the “contact us” option within the app.  The HBA may also terminate User’s use of the Services and deny access to the Services in HBA’s sole discretion for any reason or no reason, including User’s: (i) violation of terms and conditions of this Agreement (ii) lack of use of the Services. The User must agree that any termination of User’s access to the Services may be effected without prior notice and acknowledge and agree that HBA may immediately deactivate or delete the account and all related information and/or bar any further access to User’s account or the Services. Further, the User must agree that HBA shall not be liable to the User or any third party for the discontinuation or termination of User’s access to the Services.</p>
                  <h3>6: Payment</h3>
                  <p>You understand that use of the Services may result in charges to you for the services or goods you receive (“Charges”). All charges and payments will be enabled by <b>HomeBar App</b> using the preferred payment method designated in your Account. <b>HomeBar App</b> reserves the right to establish, remove and/or revise Charges for any or all services or goods obtained through the use of the Services at any time. You are responsible for the cost of repair for damage to, or necessary cleaning of, equipment and property resulting from the use of the Services under your Account in excess of normal “wear and tear” damages and necessary cleaning (“Repair or Cleaning”).</p>
                  <h3>7: Disclaimers; Limitation of Liability; Indemnity</h3>
                  <h3>Disclaimer</h3>
                  <p>The services are provided “as is” and “as available.” <b>HomeBar App</b> disclaims all representations and warranties, express, implied, or statutory, not expressly set out in these terms, including the implied warranties of merchantability, fitness for a particular purpose and non-infringement. In addition, no representation, warranty, or guarantee regarding the reliability, timeliness, quality, suitability, or availability of the services or goods requested through the use of the services, or that the services will be uninterrupted or error-free is made. <b>HomeBar App</b> does not guarantee the quality, suitability, safety or ability of third party providers. You agree that the entire risk arising out of your use of the services and any service, or good requested in connection therewith, remains solely with you, to the maximum extent permitted under applicable law.</p>
                  <h3>Limitation of liability</h3>
                  <p>The <b>HomeBar App</b> shall not be liable for any actions or omissions committed by Partnering Vendors, including deficiency in service, wrong delivery or order, quality of food, time taken to prepare or deliver the order, etc. </p>
                  <p>The <b>HomeBar App</b> bears no responsibility for any violation of Federal, State or local governing laws, licenses, rules and/or regulations applicable to Partnering Vendors, including, but not limited to violations of the Food and Drug Administration (FDA), the Food Safety and Inspection Service (FSIS), the Center for Disease Control and Prevention (CDC), the State Department of Public Health, and the State Department of Agriculture.</p>
                  <p>The <b>HomeBar App</b> shall in no manner be liable for any in-person interactions with representatives or staff of the Partnering Vendors with members or for the member’s experience with Partnering Vendors, including instances in which Partnering Vendors may abruptly temporarily or permanently discontinue services.  The <b>HomeBar App</b> will, however, take care of claims/liabilities only arising out of offers/membership plans as promoted and advertised solely through the app.</p>
                  <p><b>HomeBar App</b> shall not be responsible for indirect, incidental, special, exemplary, punitive, or consequential damages, including lost profits, lost data, personal injury, or property damage related to, in connection with, or otherwise resulting from any Use of the services, regardless of the negligence (Either active, affirmative, sole, or concurrent) of <b>HomeBar App</b>, even if <b>HomeBar App</b> has been advised of the possibility of such damages.</p>
                  <p>The limitations and disclaimer in this section do not purport to limit liability or alter your rights as a consumer that cannot be excluded under applicable law.</p>
                  <h3>Indemnity</h3>
                  <p>You agree to indemnify and hold <b>HomeBar App</b> and its affiliates and their officers, directors, employees, and agents harmless from any and all claims, demands, losses, liabilities, and expenses (including attorney’s fees), arising out of or in connection with your use of the Service, or services or goods obtained through your use of the Services; your breach or violation of any of these Terms; HomeBarApp’s use of your User Content; or your violation of the rights of any third party, including Third Party Providers.</p>
                  <h3>8: OTHER PROVISIONS</h3>
                  <h3>Choice of Law</h3>
                  <p>These Terms are governed by and construed in accordance with the laws of the State of California, USA without giving any effect to any conflict of law principles, except as may be otherwise provided in the Arbitration Agreement above or in supplemental terms applicable to your region.</p>
                  <h3>Claims of Copyright Infringement</h3>
                  <p>Any claims of copyright infringement should be sent to <b>HomeBar App</b>’s designated agent. Please visit the webpage for the designated address and additional information.</p>
                  <h3>Notice</h3>
                  <p><b>HomeBar App</b> may give notice by means of a general notice on the Services, electronic mail to your email address in your Account, telephone or text messages to any phone number provided in connection with your Account, or by written communication sent by first class mail or pre-paid post to any address connected with your Account.</p>
                  <h3>General</h3>
                  <p>You may not assign these Terms without prior written approval. <b>HomeBar App</b> may assign these Terms without your consent to a subsidiary or affiliate, an acquirer of equity, business or assets, or a successor by merger. Any purported assignment in violation of this section shall be void. No joint venture, partnership, employment, or agency relationship exists between you, <b>HomeBar App</b>, or any Third Party Provider as a result of this Agreement or use of the Services.</p>
              </div>




          </div>
      </div>


    <footer class="footer py-4 ">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 text-lg-left f-links">

                <a class="mr-3" href="#"> About Us</a>
                    <a class="mr-3" href="#"> Faq’s </a>
                    <a class="mr-3" href="#"> Contact Us</a>
                    <a class="mr-3" href="#"> Help Center </a>
                    <a class="mr-3" href="#"> Support </a>
                    <a class="mr-3" href="{{ url('privacy-policy') }}"> Privacy Policy </a>
                    <a class="mr-3" href="{{ url('terms-and-conditions') }}"> Terms And Conditions </a>

                </div>

                <div class="col-lg-6 my-3 my-lg-0 text-lg-right social">
                    <span>Connect with us</span>
                    <a class="btn  btn-social " href="#!"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a class="btn btn-social " href="#!"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
                    <a class="btn  btn-social " href="#!"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                </div>

            </div>
        </div>
        <div class="col-lg-12 text-lg-center f-bdr mt-5 copy">© Homebar.com 2020. All Rights Reserved.</div>
    </footer>




    <!--    go top-->
    <a href="#" id="gotop" data-aos="fade-in" style="display: inline-block;"> <i class="fa fa-angle-up"> </i> </a>

    <!-- Scripts -->
    <script src="{{asset('assets/front/js/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('assets/front/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
    <!-- aos animation -->
    <script src="{{asset('assets/front/js/aos.js')}}"></script>
    <!-- Owl Carousel -->
    <script src="{{asset('assets/front/js/owl.carousel.min.js')}}"></script>

    <script src="{{asset('assets/front/js/swiper.min.js')}}"></script>
    <script src="{{asset('assets/front/js/who.js')}}"></script>
    <script src="{{asset('assets/front/js/custom.js')}}"></script>

    <script src="{{asset('assets/front/js/bootstrap-select.js')}}"></script>
    <script>
        $(window).scroll(function() {
            if ($(this).scrollTop() > 50) {
                $('#dynamic').addClass('newClass');
            } else {
                $('#dynamic').removeClass('newClass');
            }
        });
    </script>

</body>


</html>
