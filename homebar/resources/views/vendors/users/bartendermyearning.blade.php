@extends('layouts.vendors.vendor')
@section('title','My Earning | Homebar')
@section('content')


<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 mb-3 ">
        <h2>My Earnings</h2>
		<div class="row">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session('success_msg'))
        <div class="alert alert-success">
            {{ session('success_msg') }}
        </div>
        @endif
        @if (session('error_msg'))
        <div class="alert alert-danger">
            {{ session('error_msg') }}
        </div>
        @endif

    </div>

        <div class="navbar-light left-side-btn ml-auto mr-3">
            <button class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>
        </div>
    </div>
</div>


<div class="row my-earning">
    <div class="col-lg-12">
        <div class="totalearning">


            <div class="earning-rs">total earnings</div>
            <div class="earning-rs text-center">
			    @if($user->stripe_connect_id == '')
				<a href="{{$stripeLink}}" class="btn btn-primary withdraw-btn" target="_blank">Click Here For Stripe Account</a><br>
				<i class="withdraw-info">( <i class="fa fa-info-circle" aria-hidden="true"></i> First you need to setup your stripe account for withdraw )</i>
				@else
				<a href="#" class="btn btn-primary withdraw-btn" data-toggle="modal" id="addForm" data-target="#addWithdraw"><i class="fa fa-usd"></i> Make Withdraw</a>
			    @endif
			</div>
            <div class="earning-rs">${{number_format($walletBalance,2,'.',',')}}</div>
        </div>
    </div>
    <div class="col-lg-12">
		@if(count($todayOrders)>0)
        <h4>Today Transactions</h4>
		@endif
	@forelse ($todayOrders as $todayOrder)
		@if($todayOrder->order_id!=null)
		@if($todayOrder->order->status == 'NEW')
		   @php
			$todaStatus = 'recent'
			@endphp
		@elseif($todayOrder->order->status == 'ACCEPTED' || $todayOrder->order->status == 'REJECTED' || $todayOrder->order->status == 'IN KITCHEN' || $todayOrder->order->status == 'READY TO PICKUP' || $todayOrder->order->status == 'START FOR DELIVERY')
		 @php
			$todaStatus = 'progress'
			@endphp

		@elseif($todayOrder->order->status == 'DELIVERED')
		@php
			$todaStatus = 'completed'
			@endphp
		@elseif($todayOrder->order->status == 'REJECTED' || $order->order->status == 'CANCELLED')
		@php
			$todaStatus = 'rejected'
			@endphp
		@endif
		@php
		   $type  = App\Model\Type::typeSlug($todayOrder->order->type_id)
		@endphp

		@php
		 $amount =  ($todayOrder->order->sub_total+$todayOrder->order->service_fee+$todayOrder->order->tax)
		@endphp
		@endif
        <div class="earningdiv">
			<a href="@if($todayOrder->order_id!=null) {{route('vendor.order.show',[$todaStatus,$todayOrder->order->id,$type])}} @endif">
            <div class="media proimg">

                <span class="mr-3">
					<img  src="@if(!empty($todayOrder->user->logo_image)){{asset($todayOrder->user->logo_image)}} @else{{asset('images/default_user.png')}}@endif"></span>
                <div class="media-body">
                    <h5 class="mt-0"> @if($todayOrder->order_id!=null)
                    	Order ID : {{$todayOrder->order->order_number}}
                    @else

                    @endif
                     </h5>
                    <p class="totalitem"> <span class="timespan">{{date('M d, Y, h:i A',strtotime($todayOrder->created_at))}}</span></p>

                </div>

            </div>
			</a>

            <div class="earning-rs">{{$todayOrder->transaction_type}}</div>
            <div class="earning-rs">${{number_format($todayOrder->amount,2,'.',',')}}</div>

        </div>

	@empty
    <!--<div class="row bgw mt-3">
        <div class="col pt-3">
			<h4>Today Orders</h4>
            <p>No Order Found.</p>
        </div>
    </div>-->
  @endforelse
    </div>
</div>


<div class="row my-earning">

    <div class="col-lg-12">
        @if(count($orders)>0)
        <h4>All Transactions</h4>
		@endif
	@forelse ($orders as $ordernew)
	@if($ordernew->order_id!=null)
		@if($ordernew->order->status == 'NEW')
		   @php
			$status = 'recent'
			@endphp
		@elseif($ordernew->order->status == 'ACCEPTED' || $ordernew->order->status == 'REJECTED' || $ordernew->order->status == 'IN KITCHEN' || $ordernew->order->status == 'READY TO PICKUP' || $ordernew->order->status == 'START FOR DELIVERY')
		 @php
			$status = 'progress'
			@endphp

		@elseif($ordernew->order->status == 'DELIVERED')
		@php
			$status = 'completed'
			@endphp
		@elseif($ordernew->order->status == 'REJECTED' || $ordernew->order->status == 'CANCELLED')
		@php
			$status = 'rejected'
			@endphp
		@endif
		@php
		   $type1  = App\Model\Type::typeSlug($ordernew->order->type_id)
		@endphp
		@php
		 $amount =  ($ordernew->order->sub_total+$ordernew->order->service_fee+$ordernew->order->tax)
		@endphp
		@endif
        <div class="earningdiv">
			<a href="@if($ordernew->order_id!=null) {{route('vendor.order.show',[$status,$ordernew->order->id,$type1])}} @endif">
				<div class="media proimg">
					<span class="mr-3"><img  src="@if(!empty($ordernew->user->logo_image)){{asset($ordernew->user->logo_image)}} @else{{asset('images/default_user.png')}}@endif"></span>
					<div class="media-body">
						<h5 class="mt-0">
							@if($ordernew->order_id!=null)
                    	Order ID : {{$ordernew->order->order_number}}
                    @else

                    @endif
						 </h5>
						<p class="totalitem"> <span class="timespan">{{date('M d, Y, h:i A',strtotime($ordernew->created_at))}}</span></p>
					</div>
				</div>
			</a>
            <div class="earning-rs">{{$ordernew->transaction_type}}</div>
             <div class="earning-rs">${{number_format($ordernew->amount,2,'.',',')}}</div>

        </div>
	@empty
    <!--<div class="row bgw mt-3">
        <div class="col pt-3">
            <p>No Order Found.</p>
        </div>
    </div>-->
  @endforelse
		<br>
{{$orders->links()}}
    </div>
</div>


<!-- Modal -->
<div id="addWithdraw" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Add Amount</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
        <form action="" method="post" name="DptForm" id="dptForm" >
               {{ csrf_field() }}

			<input type="hidden" name="withdrawRequest" value="withdrawRequest">

      <div class="modal-body">
        <div class="form-group">
            <input class="form-control" name="amount" id="amount" placeholder="Enter Amount" type="number">
        </div>
      </div>
      <div class="modal-footer">
          <input type="submit" class="btn btn-primary withdraw-btn" value="Withdraw"></div>

      </div>
        </form>
    </div>

  </div>

@endsection
