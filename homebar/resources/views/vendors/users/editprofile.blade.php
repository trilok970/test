@extends('layouts.vendors.vendor')
@section('title','My Profile')

@push('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endpush
@section('content')

<div class="container">

    <div class="row">

        <div class="container signupfroms">

        @if (session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        <div class="row pag-head mt-0">
            <div class="flex col-lg-12 col-sm-12 col-12  pdL-l0 pdr-0">
                <div class="navbar-light left-side-btn ml-auto mr-3">
                    <button class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>
                </div>
                <!-- <a href="drink-add-menu-pre.html" class="nowbtn pr-5 pl-5"> ADD MENU </a> -->
            </div>
        </div>
            {{ Form::model($user, ['route' => ['vendor.profile.update', $user->id], 'method' => 'post', 'files'=>'true']) }}


            <div class="row">
                <div class="form-group col-md-6">
                    <label for="inputBussinessType">Business Type</label>
                    <select name="business_types[]" id="inputBussinessType" class="form-control" multiple="multiple">
                        @foreach ($types as $type)
                            <option value="{{$type->id}}" @if(old('business_types')!=null && in_array($type->id,old('business_types'))){{'selected'}}@elseif($user->business_type!= null && in_array($type->id,explode(',',$user->business_type))){{'selected'}}@endif>{{$type->name}}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('business_types', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Business Name</label>
                    <?= Form::text('business_name',  old('business_name') , ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Business Name', 'id' => 'inputBussinessName']) ?>
                    {!! $errors->first('business_name', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="first_name">First Name</label>
                    <?= Form::text('first_name', old('first_name'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter First Name', 'id' => 'inputfirst_name']) ?>
                    {!! $errors->first('first_name', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
                <div class="form-group col-md-3">
                    <label for="last_name">Last Name</label>
                    <?= Form::text('last_name', old('last_name'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Last Name', 'id' => 'inputlast_name']) ?>
                    {!! $errors->first('last_name', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Email Address</label>
                    <?= Form::email('email', old('email'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Email', 'id' => 'inputEmail']) ?>
                    {!! $errors->first('email', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
            </div>


            <div class="row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Primary Mobile Number</label>
                    <?= Form::text('primary_mobile_number', old('primary_mobile_number'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Primary Mobile Number', 'id' => 'inputPrimaryMobileNumber', 'min' => 0]) ?>
                    {!! $errors->first('primary_mobile_number', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Secondary Mobile Number</label>
                    <?= Form::text('secondary_mobile_number', old('secondary_mobile_number'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Secondary Mobile Number', 'id' => 'inputSecondaryMobileNumber', 'min'=> 0]) ?>
                    {!! $errors->first('secondary_mobile_number', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12">
                    <h4>Business Location</h4>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Address</label>


                    <?= Form::textarea('address', old('address'), ['class' => 'form-control sightextarea', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Address and choose from google map hint', 'id' => 'autocomplete', 'rows' => 2, 'cols' => 40]) ?>
                    {!! $errors->first('address', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}

                </div>
                <div class="form-group col-md-6">
                    <div class="row">
                        <div class="form-group col-md-7">
                            <label for="inputCity">City</label>
                            <?= Form::text('city', old('city'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter City', 'id' => 'inputCity']) ?>
                            {!! $errors->first('city', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-5">
                            <label for="inputZipCode">Zip Code</label>
                            <?= Form::text('zipcode', old('zipcode'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Zip code', 'id' => 'inputZipCode', 'maxlength' => 6]) ?>
                            {!! $errors->first('zipcode', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                        </div>



                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="row">
                        <div class="form-group col-md-7">
                            <label for="latitude">Latitude</label>
                            <?= Form::text('latitude', old('latitude'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Auto fill latitude', 'id' => 'latitude','readonly']) ?>
                            {!! $errors->first('latitude', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-5">
                            <label for="longitude">Longitude</label>
                            <?= Form::text('longitude', old('longitude'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Auto fill longitude', 'id' => 'longitude','readonly']) ?>
                            {!! $errors->first('longitude', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}

                        </div>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="row">
                        <div class="form-group col-md-7">
                            <label for="inputState">State</label>
                            <?= Form::text('state', old('state'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter State', 'id' => 'inputState']) ?>
                            {!! $errors->first('state', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-5">
                            <label for="country">Country</label>
                            <?= Form::select('country_id', $countries, old('country_id') ,['class' => 'selectpicker customselect form-control', 'id' => 'country', 'data-container' => 'body', 'data-live-search' => 'true', 'title' => 'Select Country', 'data-hide-disabled' => 'true' ]); ?>
                            {!! $errors->first('country_id', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12 ">
                    <h4 class="mt-0">Hours of Availability</h4>
                </div>

                <div class="form-group col-md-3 clock">
                    <label for="inputOpenTime">Open Time</label>
                    <?= Form::text('open_time', old('open_time'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Open Time', 'id' => 'inputOpenTime']) ?>

                    <i class="fa fa-clock-o " aria-hidden="true"></i>
                    {!! $errors->first('open_time', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
                <div class="form-group col-md-3 clock">
                    <label for="inputCloseTime">Close Time</label>
                    <?= Form::text('close_time', old('close_time'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Close Time', 'id' => 'inputCloseTime']) ?>
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    {!! $errors->first('close_time', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>

                <div class="form-group col-md-3 p-t-4">
                    <div class="form-check">
                        <input class="form-check-input gridCheck" type="checkbox" id="is_pickup" name="pickup" @if($user->is_pickup){{'checked'}}@endif>
                        <label class="form-check-label" for="is_pickup">
                        Pickup Available
                        </label>
                    </div>
                    {!! $errors->first('pickup', '<p class="help-block" style="color: red;  font-weight: bold; width:100%;text-align: left;">:message</p>') !!}
                </div>
                <div class="form-group col-md-3 p-t-4">
                    <div class="form-check">
                        <input class="form-check-input gridCheck" type="checkbox" id="is_delivery" name="delivery" @if($user->is_delivery){{'checked'}}@endif>
                        <label class="form-check-label" for="is_delivery">
                        Delivery Available
                        </label>
                    </div>
                    {!! $errors->first('delivery', '<p class="help-block" style="color: red; float: left; font-weight: bold; width:100%;text-align: left;">:message</p>') !!}
                </div>
            </div>


            <?= Form::submit('Update', ['class' => 'btn sighn-up']) ?>


        {{ Form::close() }}
    </div>
    </div>

</div>
@endsection

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{asset('assets/front/js/moment.min.js')}}"></script>
<script src="{{asset('assets/front/js/mdtimepicker.min.js')}}"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNVV0WGqduJq4EsX8_Y_s8L-hiZrHmrj4&libraries=places&callback=initialize"
        async defer></script>

<script type="text/javascript">
$(document).ready(function(){
    $('#inputOpenTime').mdtimepicker();
    $('#inputCloseTime').mdtimepicker();
});
</script>

<script>
    $(document).ready(function(){
        $("#inputBussinessType").select2({
            placeholder:'Select business type(s)'
        });
    });
</script>
<script type="text/javascript">
  //google map
  $("#autocomplete").change(function(){
    $("#latitude").val('');
    $("#longitude").val('');
  });

  function initialize()
  {
      var input = document.getElementById('autocomplete');
      var options = {};
      var autocomplete = new google.maps.places.Autocomplete(input, options);
      google.maps.event.addListener(autocomplete, 'place_changed', function () {
          var place = autocomplete.getPlace();
          var lat = place.geometry.location.lat();
          var lng = place.geometry.location.lng();
          $("#latitude").val(lat);
          $("#longitude").val(lng);
      });
  }

  google.maps.event.addDomListener(window, 'load', initialize);

</script>
@endpush


