@extends('layouts.vendors.vendor')
@section('title','Edit Bartender Detail')
@section('content')

<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 ">
        <h2>Edit Bartender</h2>
        <div class="navbar-light left-side-btn ml-auto mr-3">
            <button class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class=" col-lg-3 col-sm-4 col-12 pr0">
        @include('vendors.includes.bartendermenu')
    </div>
    <div class=" col-lg-9 col-sm-8 col-12 pl0 addmuenu-right ">

        @if(isset($user))
            {{ Form::model($user, ['route' => ['vendor.bartender.update', $user->id], 'method' => 'post', 'files'=>'true']) }}
        @else
            {!! Form::open() !!}
        @endif

            <div class="form-group">
                <label for="first_name">First Name</label>
                <?= Form::text('first_name', old('first_name'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter First Name', 'id' => 'inputfirst_name']) ?>
                {!! $errors->first('first_name', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="last_name">Last Name</label>
                <?= Form::text('last_name', old('last_name'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Last Name', 'id' => 'inputlast_name']) ?>
                {!! $errors->first('last_name', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="inputEmail">E-mail Address</label>
                    <?= Form::email('email', old('email'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Email', 'id' => 'inputEmail']) ?>
                    {!! $errors->first('email', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="inputPrimaryMobileNumber">Primary Mobile Number</label>
                <?= Form::text('primary_mobile_number', old('primary_mobile_number'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Primary Mobile Number', 'id' => 'inputPrimaryMobileNumber', 'min' => 0]) ?>
                {!! $errors->first('primary_mobile_number', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="inputSecondaryMobileNumber">Secondary Mobile Number</label>
                <?= Form::text('secondary_mobile_number', old('secondary_mobile_number'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Secondary Mobile Number', 'id' => 'inputSecondaryMobileNumber', 'min'=> 0]) ?>
                {!! $errors->first('secondary_mobile_number', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                  <label>DATE OF BIRTH:</label>

                  <div class="input-group">
                    <input type="text" class="form-control " name="birthday" id="reservation" placeholder="Enter Date of Birth" value="<?php if(isset($user->dob)) { echo $user->dob; } ?>">
                  </div>
            </div>
            <br>


            <div class="form-group">
                <label for="autocomplete">Address</label>
                <?= Form::textarea('address', old('address'), ['class' => 'form-control sightextarea', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Address', 'id' => 'autocomplete', 'rows' => 4, 'cols' => 40]) ?>
                {!! $errors->first('address', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
                {!! Form::hidden('latitude', null, ['class'=>'form-control', 'id'=>'latitude']) !!}
                {!! Form::hidden('longitude', null, ['class'=>'form-control', 'id'=>'longitude']) !!}
            </div>
            <br>
            <div class="form-group">
                <label for="dl_number">Driver’s License/ID Number</label>
                <?= Form::text('dl_number', old('dl_number'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Driving License', 'id' => 'dl_number', 'min'=> 0]) ?>
                {!! $errors->first('dl_number', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="signature_drink">signature drink</label>
                <?= Form::text('signature_drink', old('signature_drink'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'signature drink', 'id' => 'signature_drink']) ?>
                {!! $errors->first('signature_drink', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="bio">BIO</label>
                <?= Form::textarea('bio', old('bio'), ['class' => 'form-control sightextarea', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Bio', 'id' => 'bio', 'rows' => 4, 'cols' => 40]) ?>
                {!! $errors->first('bio', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>

            <div class="form-group">
                <label for="experience"> Years of experience </label>
                <?= Form::number('experience', old('experience'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Years of experience', 'id' => 'experience', 'min'=>0]) ?>
                {!! $errors->first('experience', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>

            <div class="form-group">
                <label for="service_address_radius">radius of provides the service <small> ( in Miles ) </small></label>
                <?= Form::number('service_address_radius', old('service_address_radius'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Radius of service', 'id' => 'service_address_radius', 'min'=>1]) ?>
                {!! $errors->first('service_address_radius', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="price">Price <small> ( per Hour for one bartender ) </small></label>
                <?= Form::text('price', old('price'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Price ( per Hour for one bartender )', 'id' => 'price']) ?>
                {!! $errors->first('price', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="bartending_license_number">Bartending License Number <small>( optional)</small> </label>

                <?= Form::text('bartending_license_number', old('bartending_license_number'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Bartending License Number', 'id' => 'bartending_license_number']) ?>
                {!! $errors->first('bartending_license_number', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="bartending_license_expirations">Bartending License Expirations Date  <small>( optional)</small></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="bartending_license_expirations" id="bartending_license_expirations" placeholder="Bartending License Expirations Date" value="<?php if(isset($user->bartending_license_expirations)) { echo $user->bartending_license_expirations; } ?>">
                </div>
                  {!! $errors->first('bartending_license_expirations', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>


            <br>


            <label for="inputPassword4">select option</label>
            <div class="custom-redio">
                <p>
                    <input type="radio" id="test1" name="option" value="1"  <?php if($user->options==1) { echo 'checked'; } ?> >
                    <label for="test1">Individual</label>
                </p>
                <p>
                    <input type="radio" id="test2" name="option" value="2" <?php if($user->options==2) { echo 'checked'; } ?>>
                    <label for="test2">Group</label>
                </p>

            </div>
            <br>

            <div class="form-row">
                <div class="form-group col- pr-3">
                    <label for="inputPassword4">photo</label>
                    <div class="box">
                        <input type="file" name="photo" id="photo" class="inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple />
                        <label for="photo"><figure> <img width="40" src="{{asset('assets/front/images/user-add.svg')}}"></figure> </label>
                    </div>
                    <img id="photo-preview" src="#" alt="" width="80px"/>
                    @if(!empty($user->profile_image))
                        <br><img src="{{asset($user->profile_image)}}" width="100px">
                    @endif
                </div>

                <div class="form-group col- pr-3">
                    <label for="inputPassword4">Add Photo DL/ID</label>
                    <div class="box">
                        <input type="file" name="dl_photo" id="dl_photo" class="inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple />
                        <label for="dl_photo"><figure> <img width="40" src="{{asset('assets/front/images/dl.svg')}}"></figure> </label>
                    </div>
                    <img id="dl-photo-preview" src="#" alt="" width="80px"/>
                    @if(!empty($user->dl_photo))
                        <br><img src="{{asset($user->dl_photo)}}" width="100px">
                    @endif
                </div>

                <div class="form-group col- ">
                    <label for="inputPassword4">Add Bartending License </label>
                    <div class="box">
                        <input type="file" name="bl_photo" id="bl_photo" class="inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple />
                        <label for="bl_photo"><figure> <img width="40" src="{{asset('assets/front/images/dl.svg')}}"></figure> </label>
                    </div>
                    <img id="bl-photo-preview" src="#" alt="" width="80px"/>
                    @if(!empty($user->bl_photo))
                        <br><img src="{{asset($user->bl_photo)}}" width="100px">
                    @endif
                </div>

            </div>
            {!! $errors->first('photo', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            {!! $errors->first('dl_photo', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            {!! $errors->first('bl_photo', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            <br>

            <div class="form-group">
                <label for="country">Country</label>
                <select name="country_id" id="country_id" class="selectpicker customselect form-control" data-container="body" data-live-search="true" title="Select Country" data-hide-disabled="true">
                    @foreach ($countries as $key => $name)
                        <option value="{{$key}}" @if($key == $user->country_id) {{'selected="selected"'}} @endif>{{$name}}</option>
                    @endforeach
                </select>
                {!! $errors->first('country_id', '<p class="help-block" style="color: red; float: left;">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="social_security_number">Social Security Number(SSN)</label>
                <input type="text" name="social_security_number" id="social_security_number" class="form-control" id="social_security_number" placeholder="Social Security Number" value="{{$user->bartending->social_security_number}}">
                {!! $errors->first('social_security_number', '<p class="help-block" style="color: red; float: left;">:message</p>') !!}
            </div>
            <br>

            <button class="add mt-4">continue</button>

        {{ Form::close() }}

    </div>

</div>

@endsection

@push('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNVV0WGqduJq4EsX8_Y_s8L-hiZrHmrj4&libraries=places&callback=initialize"
async defer></script>
<script type="text/javascript">
    //google map
    $("#autocomplete").change(function(){
      $("#latitude").val('');
      $("#longitude").val('');
    });

    function initialize()
    {
        var input = document.getElementById('autocomplete');
        var options = {};
        var autocomplete = new google.maps.places.Autocomplete(input, options);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
            $("#latitude").val(lat);
            $("#longitude").val(lng);
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);

  </script>
  {{-- show the preview of images --}}
<script>
    function readURL(input,preview_id) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#'+preview_id).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    $("#photo").change(function() {
      readURL(this,'photo-preview');
    });
    $("#dl_photo").change(function() {
      readURL(this,'dl-photo-preview');
    });

    $("#bl_photo").change(function() {
      readURL(this,'bl-photo-preview');
    });
    </script>
@endpush

