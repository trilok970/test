@extends('layouts.vendors.vendor')
@section('title','Bartender Schedule')
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('content')
<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 ">
        <h2>Add Schedule Time</h2>
        <div class="navbar-light left-side-btn ml-auto mr-3">
            <button class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class=" col-lg-3 col-sm-4 col-12 pr0">
        @include('vendors.includes.bartendermenu')
    </div>

    <div class=" col-lg-9 col-sm-8 col-12 pl0  claledra-page ">
        {!! Form::open(['route' => 'vendor.bartenderscheduleadd', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'bartendor-add-video','enctype' => 'multipart/form-data']) !!}
            <h5 class="calendarheading mt-3">Schedule calendar</h5>
            <div id="calendar" data-url="{{route('vendor.bartender.bookings',$id)}}" data-leaveurl="{{route('vendor.bartender.leaves',$id)}}" data-addleaveurl={{route('vendor.bartender.toggle-leave',$id)}}></div>

            <div class="swithbtn">
                <div class="clockicon"><i class="fa fa-clock-o" aria-hidden="true"></i> turn off availability</div>
                <label class="switch">
                    <input type="checkbox" id="toggleAvailiblity" @if(isset($user->availability) && ($user->availability==1)){{'checked'}}@endif data-url={{route('vendor.bartender.update-availability',$id)}}>
                    <span class="slider"></span>
                  </label>
            </div>

            <input type="hidden" name="bartender_id" value="<?php echo $id; ?>" id="bartender_id">

            <div class="d-t-head">Regular Times</div>
                @if (count($bartenderAvailability)>0)
                    @foreach ($bartenderAvailability as $key => $value)
                    <div class="form-row day-selct">
                        <div class="form-group col-lg-4 col-md-12 col-sm-12">
                            <input type="text" class="form-control disabled" value="{{$weekdays[$loop->index]}}" readonly>
                            <input type="hidden" name="weekday[]" value="{{$loop->iteration}}">
                        </div>
                        <div class="form-group col-lg-4 col-md-12 col-sm-12">
                            <input type="text" placeholder="Time From "  name="time_from[]" class="form-control timeFrom" id="inputZip" value="@if($value->time_from) {{$value->time_from}} @else {{old('time_from')[$loop->index]}} @endif">
                        </div>
                        <div class="form-group col-lg-4 col-md-12 col-sm-12">
                            <input type="text" placeholder="Time To" name="time_to[]" class="form-control timeTo" id="inputZip" value="<?php echo $value->time_to; ?>">
                        </div>
                    </div>
                    @endforeach
                @else
                <div class="form-row day-selct">
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" class="form-control disabled" value="Sunday" readonly>
                        <input type="hidden" name="weekday[]" value="1">
                    </div>
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" placeholder="Time From " name="time_from[]" class="form-control timeFrom" id="inputZip" readonly value="@if(!empty(old('time_from'))){{old('time_from')[0]}}@endif">
                    </div>
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" placeholder="Time To" name="time_to[]" class="form-control timeTo" id="inputZip" readonly value="@if(!empty(old('time_to'))){{old('time_to')[0]}}@endif">
                    </div>
                </div>
                <div class="form-row day-selct">
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" class="form-control disabled" value="Monday" readonly>
                        <input type="hidden" name="weekday[]" value="2">
                    </div>
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" placeholder="Time From " name="time_from[]"  class="form-control timeFrom" id="inputZip" value="@if(!empty(old('time_from'))){{old('time_from')[1]}}@endif">

                    </div>
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">

                        <input type="text" placeholder="Time To" name="time_to[]" class="form-control timeTo" id="inputZip" value="@if(!empty(old('time_to'))){{old('time_to')[1]}}@endif">
                    </div>
                </div>
                <div class="form-row day-selct">
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" class="form-control disabled" value="Tuesday" readonly>
                        <input type="hidden" name="weekday[]" value="3">
                    </div>
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" placeholder="Time From " name="time_from[]"  class="form-control timeFrom" id="inputZip" value="@if(!empty(old('time_from'))){{old('time_from')[2]}}@endif">

                    </div>
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">

                        <input type="text" placeholder="Time To" name="time_to[]" class="form-control timeTo" id="inputZip" value="@if(!empty(old('time_to'))){{old('time_to')[1]}}@endif">
                    </div>
                </div>
                <div class="form-row day-selct">
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" class="form-control disabled" value="Wednesday" readonly>
                        <input type="hidden" name="weekday[]" value="4">
                    </div>
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" placeholder="Time From " name="time_from[]"  class="form-control timeFrom" id="inputZip" value="@if(!empty(old('time_from'))){{old('time_from')[3]}}@endif">

                    </div>
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" placeholder="Time To" name="time_to[]" class="form-control timeTo" id="inputZip" value="@if(!empty(old('time_to'))){{old('time_to')[3]}}@endif">
                    </div>
                </div>
                <div class="form-row day-selct">
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" class="form-control disabled" value="Thursday" readonly>
                        <input type="hidden" name="weekday[]" value="5">
                    </div>
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" placeholder="Time From " name="time_from[]"  class="form-control timeFrom" id="inputZip" value="@if(!empty(old('time_from'))){{old('time_from')[4]}}@endif">

                    </div>
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">

                        <input type="text" placeholder="Time To" name="time_to[]" class="form-control timeTo" id="inputZip" value="@if(!empty(old('time_from'))){{old('time_to')[4]}}@endif">
                    </div>
                </div>
                <div class="form-row day-selct">
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" class="form-control disabled" value="Friday" readonly>
                        <input type="hidden" name="weekday[]" value="6">
                    </div>
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" placeholder="Time From " name="time_from[]"  class="form-control timeFrom" id="timeFrom" value="@if(!empty(old('time_from'))){{old('time_from')[5]}}@endif">

                    </div>
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">

                        <input type="text" placeholder="Time To" name="time_to[]" class="form-control timeTo" id="timeTo" value="@if(!empty(old('time_to'))){{old('time_to')[5]}}@endif">
                    </div>
                </div>
                <div class="form-row day-selct">
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" class="form-control disabled" value="Saturday" readonly>
                        <input type="hidden" name="weekday[]" value="7">
                    </div>
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">
                        <input type="text" placeholder="Time From " name="time_from[]"  class="form-control timeFrom" id="timeFrom" value="@if(!empty(old('time_from'))){{old('time_from')[6]}}@endif">

                    </div>
                    <div class="form-group col-lg-4 col-md-12 col-sm-12">

                        <input type="text" placeholder="Time To" name="time_to[]" class="form-control timeTo" id="timeTo" value="@if(!empty(old('time_to'))){{old('time_from')[6]}}@endif">
                    </div>
                </div>
            @endif
            <button class="add mt-4">submit</button>
            {{ Form::close() }}
    </div>

</div>
<!-- Modal -->
<div class="modal fade" id="availiblityModel" tabindex="-1" role="dialog" aria-labelledby="availiblityModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="availiblityModalLabel">Update Availability</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form method="post" action="{{route('vendor.bartender.update-unavailability',$id)}}" id="update-unavailability-form">
                @csrf
                <div class="form-group">
                    <label for="unavailability_type">Select Type</label>
                    <select name="unavailability_type" id="unavailability_type" class="form-control" required>
                        <option value="">Select Unavailability Type</option>
                        <option value="1">Specific Date Range</option>
                        <option value="2">From Now Onwords</option>
                    </select>
                </div>
                <div class="d-none" id="unavailability-date-range-block">
                    <div class="form-group">
                        <label for="unavailability_start_date">Start Date</label>
                        <input type="text" name="unavailability_start_date" id="unavailability_start_date" class="form-control" placeholder="Start Date">
                    </div>
                    <div class="form-group">
                        <label for="unavailability_end_date">End Date</label>
                        <input type="text" name="unavailability_end_date" id="unavailability_end_date" class="form-control" placeholder="End Date">
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn new-btn">Update Availability</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.3.2/main.min.css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush


@push('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.3.2/main.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var eventURL = $("#calendar").data('url');
        var leaveURL = $("#calendar").data('leaveurl');
        var addLeaveURL = $("#calendar").data('addleaveurl');
        var calendar = new FullCalendar.Calendar(calendarEl, {
          defaultView: 'agendaWeek',
          eventSources: [{
                url: eventURL,
                type: 'GET',
                error: function() {
                    alert('There was an error while fetching events.');
                },
                color:'green',
                textcolor:'white',
            },
            {
                url: leaveURL,
                type: 'GET',
                error: function() {
                    alert('There was an error while fetching events.');
                },
                color:'red',
                textcolor:'white',
            }],
        });
        calendar.render();

        calendar.on('dateClick', function(info) {
            var date = info.dateStr;
            if(confirm("Are you sure to mark leave or remove leave?")){
                $.post(addLeaveURL,{date:date},function(out){
                    if(out.result == 1) {
                        history.go(0); // reload the document
                    }else {
                        alert(out.error);
                    }
                });
            }
        });
      });

        $("#toggleAvailiblity").change(function(){
            if(!$(this).prop('checked')) {
                $("#availiblityModel").modal('show');
            }else {
                if(confirm("Are you sure, you want to turn on your availability")) {
                    var url = $(this).data('url');
                    $.post(url,function(out){
                        if(out.result == 1) {
                            history.go(0); // reload the document
                        }else {
                            alert(out.error);
                        }
                    });
                }
            }
        });
        $(function() {
            $( "#unavailability_start_date,#unavailability_end_date" ).datepicker({dateFormat:'yy-mm-dd',minDate:0});
        });
        $(document).on('change','#unavailability_type',function(out){
            if($(this).val() == 1) {
                $("#unavailability-date-range-block").removeClass('d-none');
            }else {
                $("#unavailability-date-range-block").addClass('d-none');
            }
        });


    </script>
@endpush

