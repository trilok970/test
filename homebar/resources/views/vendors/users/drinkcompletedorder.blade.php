@extends('layouts.vendors.vendor')

@section('title','Completed Orders | Homebar')


@section('content')

<div class="row pag-head">

    <div class="flex col-lg-12 col-sm-12 col-12 ">
        <h2>Completed Orders</h2>
        
        <!-- <button class="nowbtn">Add Menu </button> -->
    </div>

</div>


<a href="#">
    <div class="row  orderid">
        <div class="col-lg-12">
            <div class="bg-w pdd15">
                <div class="row">
                    <div class="justi    col-lg-6 col-sm-7 col-12 ">
                        <p class="myid">Order ID : H321654987 | May 01, 2020, 05:30 PM</p>

                    </div>

                    <div class="  txtr col-lg-6 col-sm-5 col-12 ">
                        <button class="deliverbtn">Delivery </button>

                    </div>
                    <hr>
                </div>
            </div>

        </div>


    </div>
    <div class="row  username">
        <div class="col-lg-12">
            <div class="bg-w pdd15">
                <div class="row">

                    <div class=" col-lg-6 col-sm-7 col-12 ">
                        <div class="media proimg">
                            <span class="mr-3"><img  src="{{asset('assets/front/images/pro.jpg')}}"></span>
                            <div class="media-body">
                                <h5 class="mt-0">Atkinson Allen</h5>
                                <p class="totalitem">Total Items: <b>05</b> | Total Price: <b>$230</b></p>
                            </div>
                        </div>

                    </div>

                    <div class=" txtr  col-lg-6 col-sm-5 col-12 ">
                        <div class="acc">Delivered</div>
                        <p class="packed">Driver:<span class="bld">Daniel Quiroz, May 1, 2020 at 7:30 PM</span> </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</a>
<a href="#">
    <div class="row  orderid">
        <div class="col-lg-12">
            <div class="bg-w pdd15">
                <div class="row">
                    <div class="justi    col-lg-6 col-sm-7 col-12 ">
                        <p class="myid">Order ID : H321654987 | May 01, 2020, 05:30 PM</p>

                    </div>

                    <div class="  txtr col-lg-6 col-sm-5 col-12 ">
                        <button class="deliverbtn">Delivery </button>

                    </div>
                    <hr>
                </div>
            </div>

        </div>


    </div>
    <div class="row  username">
        <div class="col-lg-12">
            <div class="bg-w pdd15">
                <div class="row">

                    <div class=" col-lg-6 col-sm-7 col-12 ">
                        <div class="media proimg">
                            <span class="mr-3"><img  src="{{asset('assets/front/images/pro.jpg')}}"></span>
                            <div class="media-body">
                                <h5 class="mt-0">Atkinson Allen</h5>
                                <p class="totalitem">Total Items: <b>05</b> | Total Price: <b>$230</b></p>
                            </div>
                        </div>

                    </div>

                    <div class=" txtr  col-lg-6 col-sm-5 col-12 ">
                        <div class="acc">Delivered</div>
                        <p class="packed">Driver:<span class="bld">Daniel Quiroz, May 1, 2020 at 7:30 PM</span> </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</a>

<a href="#">
    <div class="row  orderid">
        <div class="col-lg-12">
            <div class="bg-w pdd15">
                <div class="row">
                    <div class="justi    col-lg-6 col-sm-7 col-12 ">
                        <p class="myid">Order ID : H321654987 | May 01, 2020, 05:30 PM</p>

                    </div>

                    <div class="  txtr col-lg-6 col-sm-5 col-12 ">
                        <button class="deliverbtn">Delivery </button>

                    </div>
                    <hr>
                </div>
            </div>

        </div>


    </div>
    <div class="row  username">
        <div class="col-lg-12">
            <div class="bg-w pdd15">
                <div class="row">

                    <div class=" col-lg-6 col-sm-7 col-12 ">
                        <div class="media proimg">
                            <span class="mr-3"><img  src="{{asset('assets/front/images/pro.jpg')}}"></span>
                            <div class="media-body">
                                <h5 class="mt-0">Atkinson Allen</h5>
                                <p class="totalitem">Total Items: <b>05</b> | Total Price: <b>$230</b></p>
                            </div>
                        </div>

                    </div>

                    <div class=" txtr  col-lg-6 col-sm-5 col-12 ">
                        <div class="acc">Delivered</div>
                        <p class="packed">Driver:<span class="bld">Daniel Quiroz, May 1, 2020 at 7:30 PM</span> </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</a>


<a href="#">
    <div class="row  orderid">
        <div class="col-lg-12">
            <div class="bg-w pdd15">
                <div class="row">
                    <div class="justi    col-lg-6 col-sm-7 col-12 ">
                        <p class="myid">Order ID : H321654987 | May 01, 2020, 05:30 PM</p>

                    </div>

                    <div class="  txtr col-lg-6 col-sm-5 col-12 ">
                        <button class="deliverbtn">Delivery </button>

                    </div>
                    <hr>
                </div>
            </div>

        </div>


    </div>
    <div class="row  username">
        <div class="col-lg-12">
            <div class="bg-w pdd15">
                <div class="row">

                    <div class=" col-lg-6 col-sm-7 col-12 ">
                        <div class="media proimg">
                            <span class="mr-3"><img  src="{{asset('assets/front/images/pro.jpg')}}"></span>
                            <div class="media-body">
                                <h5 class="mt-0">Atkinson Allen</h5>
                                <p class="totalitem">Total Items: <b>05</b> | Total Price: <b>$230</b></p>
                            </div>
                        </div>

                    </div>

                    <div class=" txtr  col-lg-6 col-sm-5 col-12 ">
                        <div class="acc">Delivered</div>
                        <p class="packed">Driver: <span class="bld">Terence  May 1, 2020 at 7:30 PM</span> </p>

                    </div>

                </div>
            </div>
        </div>
    </div>
</a>
<a href="#">

    <div class="row  orderid">
        <div class="col-lg-12">
            <div class="bg-w pdd15">
                <div class="row">
                    <div class="justi    col-lg-6 col-sm-7 col-12 ">
                        <p class="myid">Order ID : H321654987 | May 01, 2020, 05:30 PM</p>

                    </div>

                    <div class="  txtr col-lg-6 col-sm-5 col-12 ">
                        <button class="deliverbtn">Delivery </button>

                    </div>
                    <hr>
                </div>
            </div>

        </div>


    </div>
    <div class="row  username">
        <div class="col-lg-12">
            <div class="bg-w pdd15">
                <div class="row">

                    <div class=" col-lg-6 col-sm-7 col-12 ">
                        <div class="media proimg">
                            <span class="mr-3"><img  src="{{asset('assets/front/images/pro.jpg')}}"></span>
                            <div class="media-body">
                                <h5 class="mt-0">Atkinson Allen</h5>
                                <p class="totalitem">Total Items: <b>05</b> | Total Price: <b>$230</b></p>
                            </div>
                        </div>

                    </div>

                    <div class=" txtr  col-lg-6 col-sm-5 col-12 ">
                        <div class="acc">Delivered</div>
                        <p class="packed">Driver: <span class="bld">Terence  May 1, 2020 at 7:30 PM</span> </p>
                    </div>

                </div>
            </div>
        </div>
    </div>


</a>

@endsection
