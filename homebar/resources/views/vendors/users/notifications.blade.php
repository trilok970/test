@extends('layouts.vendors.vendor')
@section('title','Notifications | Homebar')
@section('content')


<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 mb-3 ">
        <h2>Notifications</h2>
		<div class="row">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session('success_msg'))
        <div class="alert alert-success">
            {{ session('success_msg') }}
        </div>
        @endif
        @if (session('error_msg'))
        <div class="alert alert-danger">
            {{ session('error_msg') }}
        </div>
        @endif

    </div>

        <div class="navbar-light left-side-btn ml-auto mr-3">
            <button class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>
        </div>
    </div>
</div>


<div class="row my-earning">
    <div class="col-lg-12">
        <div class="totalearning">


            <div class="earning-rs">total notifications</div>
            <div class="earning-rs text-center">

			</div>
            <div class="earning-rs">{{count(App\Model\UserNotification::getNotification())}}</div>
        </div>
    </div>
    <div class="col-lg-12">

        <h4>Notifications</h4>
        @php $user = Auth::user(); @endphp
        @if(count(App\Model\UserNotification::getNotification()) > 0)
		@foreach(App\Model\UserNotification::getNotification() as $val)        
        <div class="earningdiv">
			<a href="#">
            <div class="media proimg">

                <span class="mr-3">
					<img  src="@if(!empty($user->profile_image)){{asset($user->profile_image)}} @else{{asset('images/default_user.png')}}@endif"></span>
                <div class="media-body">
                    <h5 class="mt-0">
                    	Title : {{$val->message}}

                     </h5>
                    <p class="totalitem"> <span class="timespan">{{date('M d, Y, H:i A',strtotime($val->created_at))}}</span></p>

                </div>

            </div>
			</a>

            <div class="earning-rs"></div>
            <div class="earning-rs">{{$val->title}}</div>

        </div>
        @endforeach
        @else
        <div class="earningdiv">
	        <center><h4>Notification not found.</h4></center>
        </div>        
        @endif



  <!-- pagination -->
    </div>
</div>






@endsection
