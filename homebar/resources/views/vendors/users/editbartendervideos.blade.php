@extends('layouts.vendors.vendor')
@section('title','Bartender Edit Video')
@section('content')

<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 ">
        <h2>Edit Video</h2>
        <div class="navbar-light left-side-btn ml-auto mr-3">
            <button class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class=" col-lg-3 col-sm-4 col-12 pr0">

        @include('vendors.includes.bartendermenu')

    </div>

    <div class=" col-lg-9 col-sm-8 col-12 pl0 ">
        {!! Form::open(['route' => 'vendor.editvideo', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'bartendor-edit-video','enctype' => 'multipart/form-data']) !!}
        <div class="add-video">
            <div class="text-add">ADD VIDEOS</div>
            <div class="row ">
            <div class="col-lg-12 col-sm-12 col-12" style="color: red; float: left; padding-bottom: 2%;">
                @if($errors->any())
                    {!! implode('', $errors->all('<div>:message</div>')) !!}
                @endif
            </div>

                <div class=" col-lg-6 col-sm-6 col-6 ">
                    <div class="video">
                        <input type="file" name="video[]" id="video" class="inputfile inputfile-4" multiple="" />
                        <label for="video"><figure> <img width="60" src="{{asset('assets/front/images/add-video.svg')}}"></figure> </label>
                    </div>

                </div>

                {!! $errors->first('video', '<p class="help-block" style="color: red; float: left;">:message</p>') !!}
                <input type="hidden" name="bartender_id" id="bartender_id" value="<?php echo $id; ?>">

                <?php
                    if (count($videodetails)!=0) {
                        foreach ($videodetails as $key => $value) {
                ?>
                <div class=" col-lg-6 col-sm-6 col-6 ">

                    <div class="video ">
                        <iframe style="border-radius: 4px; width: 100%; height: 100%;" src="{{asset($value->video)}}">
                        </iframe>
                        <i class="fa fa-times-circle " aria-hidden="true"></i>
                    </div>

                </div>
                <?php
                        }
                    }
                ?>

            </div>
            <button class="add mt-4">continue</button>
            {{ Form::close() }}
        </div>
    </div>

</div>

@endsection
