@extends('layouts.vendors.vendor')
@section('title','Vendor Dashboard | Category Not found')
@section('content')
<div class="container">
    <div class="row pag-head">
        <div class="flex col-lg-12 col-sm-12 col-12 ">
            <div class="navbar-light left-side-btn ml-auto mr-3">
                <button class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="profile-page w-100">
            <div class="row mt30 bg-w pt-4 pb-4 pdd15">
                <div class="col">
                    <h3 class="text-center">Category Not Added in your business type.</h3>
                    <p class="text-muted text-center">To Activate the business type, Edit your profile.</p>
                    <p class="text-center"><a href="{{route('vendor.profile')}}" class="new-btn">Edit Profile</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
