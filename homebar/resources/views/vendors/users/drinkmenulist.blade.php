@extends('layouts.vendors.vendor')
@section('content')

<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 ">
        <h2>Drink Menu</h2>

    </div>
</div>

<div class="row list-page mt-3">
    <div class="col-12 col-md-6 col-lg-4">
        <div class="card">
            <img class="card-img-top" src="{{asset('assets/front/images/king.jpg')}}" alt="Card image cap">
            <div class="card-body">
                <h4 class="card-title"><a href="#" title="View Product">Kingfisher Bear</a></h4>
                <p class="card-text">Mix of gin and curry leaves and cucu...</p>
                <h5>$20</h5>
                <div class="row border-top pt-1">
                    <div class="col">
                        <a href="#" class="listbtn edit"><!-- <img width="12" src="{{asset('assets/front/images/edit.svg')}}"> --> Edit</a>
                    </div>
                    <div class="col">
                        <a href="#" class="listbtn delete"><!-- <img width="10" src="{{asset('assets/front/images/close.svg')}}"> --> Delete</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 col-lg-4">
        <div class="card">
            <img class="card-img-top" src="{{asset('assets/front/images/d2.jpg')}}" alt="Card image cap">
            <div class="card-body">
                <h4 class="card-title"><a href="#" title="View Product">Budweiser Bear</a></h4>
                <p class="card-text">Mix of gin and curry leaves and cucu...</p>
                <h5>$20</h5>
                <div class="row border-top pt-1">
                    <div class="col">
                        <a href="#" class="listbtn edit"><!-- <img width="12" src="{{asset('assets/front/images/edit.svg')}}"> --> Edit</a>
                    </div>
                    <div class="col">
                        <a href="#" class="listbtn delete"><!-- <img width="10" src="{{asset('assets/front/images/close.svg')}}"> --> Delete</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 col-lg-4">
        <div class="card">
            <img class="card-img-top" src="{{asset('assets/front/images/d3.jpg')}}" alt="Card image cap">
            <div class="card-body">
                <h4 class="card-title"><a href="#" title="View Product">Heineken Bear</a></h4>
                <p class="card-text">Mix of gin and curry leaves and cucu...</p>
                <h5>$20</h5>
                <div class="row border-top pt-1">
                    <div class="col">
                        <a href="#" class="listbtn edit"><!-- <img width="12" src="{{asset('assets/front/images/edit.svg')}}"> --> Edit</a>
                    </div>
                    <div class="col">
                        <a href="#" class="listbtn delete"><!-- <img width="10" src="{{asset('assets/front/images/close.svg')}}"> --> Delete</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 col-lg-4">
        <div class="card">
            <img class="card-img-top" src="{{asset('assets/front/images/d4.jpg')}}" alt="Card image cap">
            <div class="card-body">
                <h4 class="card-title"><a href="#" title="View Product">100 Pipers Whisky</a></h4>
                <p class="card-text">Mix of gin and curry leaves and cucu...</p>
                <h5>$20</h5>
                <div class="row border-top pt-1">
                    <div class="col">
                        <a href="#" class="listbtn edit"><!-- <img width="12" src="{{asset('assets/front/images/edit.svg')}}"> --> Edit</a>
                    </div>
                    <div class="col">
                        <a href="#" class="listbtn delete"><!-- <img width="10" src="{{asset('assets/front/images/close.svg')}}"> --> Delete</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 col-lg-4">
        <div class="card">
            <img class="card-img-top" src="{{asset('assets/front/images/d5.jpg')}}" alt="Card image cap">
            <div class="card-body">
                <h4 class="card-title"><a href="#" title="View Product">Chivas Regal Whisky</a></h4>
                <p class="card-text">Mix of gin and curry leaves and cucu...</p>
                <h5>$20</h5>
                <div class="row border-top pt-1">
                    <div class="col">
                        <a href="#" class="listbtn edit"><!-- <img width="12" src="{{asset('assets/front/images/edit.svg')}}"> --> Edit</a>
                    </div>
                    <div class="col">
                        <a href="#" class="listbtn delete"><!-- <img width="10" src="{{asset('assets/front/images/close.svg')}}"> --> Delete</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 col-lg-4">
        <div class="card">
            <img class="card-img-top" src="{{asset('assets/front/images/d6.jpg')}}" alt="Card image cap">
            <div class="card-body">
                <h4 class="card-title"><a href="#" title="View Product">Ballentine Whisky</a></h4>
                <p class="card-text">Mix of gin and curry leaves and cucu...</p>
                <h5>$20</h5>
                <div class="row border-top pt-1">
                    <div class="col">
                        <a href="#" class="listbtn edit"><!-- <img width="12" src="{{asset('assets/front/images/edit.svg')}}"> --> Edit</a>
                    </div>
                    <div class="col">
                        <a href="#" class="listbtn delete"><!-- <img width="10" src="{{asset('assets/front/images/close.svg')}}"> --> Delete</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 col-md-6 col-lg-4">
        <div class="card">
            <img class="card-img-top" src="{{asset('assets/front/images/d7.jpg')}}" alt="Card image cap">
            <div class="card-body">
                <h4 class="card-title"><a href="#" title="View Product">Absolut - Blue Kurrant</a></h4>
                <p class="card-text">Mix of gin and curry leaves and cucu...</p>
                <h5>$20</h5>
                <div class="row border-top pt-1">
                    <div class="col">
                        <a href="#" class="listbtn edit"><!-- <img width="12" src="{{asset('assets/front/images/edit.svg')}}"> --> Edit</a>
                    </div>
                    <div class="col">
                        <a href="#" class="listbtn delete"><!-- <img width="10" src="{{asset('assets/front/images/close.svg')}}"> --> Delete</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-12 col-md-6 col-lg-4">
        <div class="card">
            <img class="card-img-top" src="{{asset('assets/front/images/d8.jpg')}}" alt="Card image cap">
            <div class="card-body">
                <h4 class="card-title"><a href="#" title="View Product">Simrnoff - Orange Citrus</a></h4>
                <p class="card-text">Mix of gin and curry leaves and cucu...</p>
                <h5>$20</h5>
                <div class="row border-top pt-1">
                    <div class="col">
                        <a href="#" class="listbtn edit"><!-- <img width="12" src="{{asset('assets/front/images/edit.svg')}}"> --> Edit</a>
                    </div>
                    <div class="col">
                        <a href="#" class="listbtn delete"><!-- <img width="10" src="{{asset('assets/front/images/close.svg')}}"> --> Delete</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 col-md-6 col-lg-4">
        <div class="card">
            <img class="card-img-top" src="{{asset('assets/front/images/d9.jpg')}}" alt="Card image cap">
            <div class="card-body">
                <h4 class="card-title"><a href="#" title="View Product">Grey Goose</a></h4>
                <p class="card-text">Mix of gin and curry leaves and cucu...</p>
                <h5>$20</h5>
                <div class="row border-top pt-1">
                    <div class="col">
                        <a href="#" class="listbtn edit"><!-- <img width="12" src="{{asset('assets/front/images/edit.svg')}}"> --> Edit</a>
                    </div>
                    <div class="col">
                        <a href="#" class="listbtn delete"><!-- <img width="10" src="{{asset('assets/front/images/close.svg')}}"> --> Delete</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
