@extends('layouts.vendors.vendor')
@section('title','Vendor Dashboard | Homebar')

@section('content')

<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12  pdL-l0 pdr-0">
        <h2>Recent Orders</h2>
        <a href="#" class="nowbtn pr-5 pl-5"> ADD MENU </a>
        <!-- <a href="drink-add-menu-pre.html" class="nowbtn pr-5 pl-5"> ADD MENU </a> -->
    </div>
</div>
<a href="#">

    <div class="row bgw orderid">
        <div class="justi    col-lg-6 col-sm-7 col-8 ">
            <p class="myid">Order Number : H321654987 <em>|</em> May 01, 2020, 05:30 PM</p>

        </div>

        <div class="  txtr col-lg-6 col-sm-5 col-4 ">
            <button class="deliverbtn">Delivery </button>

        </div>
        <hr>
    </div>
    <div class="row bgw username">
        <div class=" col-lg-6 col-sm-6 col-12 ">
            <div class="media proimg">
                <span class="mr-3"><img  src="{{asset('assets/front/images/pro.jpg')}}"></span>
                <div class="media-body">
                    <h5 class="mt-0">Atkinson Allen</h5>
                    <p class="totalitem">Number of Items : <b>05</b> <em>|</em> Total Price: <b>$230</b></p>
                </div>
            </div>

        </div>

        <div class=" txtr  col-lg-6 col-sm-6 col-12 ">
            <button class="acceptbtn">Accept </button>
            <button class="nowbtn">Decline</button>
        </div>
    </div>
</a>
<a href="#">

    <div class="row bgw orderid">
        <div class="justi    col-lg-6 col-sm-7 col-12 ">
            <p class="myid">Order Number : H321654987 <em>|</em> May 01, 2020, 05:30 PM</p>

        </div>

        <div class="  txtr col-lg-6 col-sm-5 col-12 ">
            <button class="deliverbtn">Delivery </button>

        </div>
        <hr>
    </div>
    <div class="row bgw username">
        <div class=" col-lg-6 col-sm-6 col-12 ">
            <div class="media proimg">
                <span class="mr-3"><img  src="{{asset('assets/front/images/pro.jpg')}}"></span>
                <div class="media-body">
                    <h5 class="mt-0">Atkinson Allen</h5>
                    <p class="totalitem">Number of Items : <b>05</b> <em>|</em> Total Price: <b>$230</b></p>
                </div>
            </div>

        </div>

        <div class=" txtr  col-lg-6 col-sm-6 col-12 ">
            <button class="acceptbtn">Accept </button>
            <button class="nowbtn">Decline</button>
        </div>
    </div>
</a>
<a href="#">
    <div class="row bgw orderid">
        <div class="justi    col-lg-6 col-sm-7 col-12 ">
            <p class="myid">Order Number : H321654987 <em>|</em> May 01, 2020, 05:30 PM</p>

        </div>

        <div class="  txtr col-lg-6 col-sm-5 col-12 ">
            <button class="deliverbtn">Delivery </button>

        </div>
        <hr>
    </div>
    <div class="row bgw username">
        <div class=" col-lg-6 col-sm-6 col-12 ">
            <div class="media proimg">
                <span class="mr-3"><img  src="{{asset('assets/front/images/pro.jpg')}}"></span>
                <div class="media-body">
                    <h5 class="mt-0">Atkinson Allen</h5>
                    <p class="totalitem">Number of Items : <b>05</b> <em>|</em> Total Price: <b>$230</b></p>
                </div>
            </div>

        </div>

        <div class=" txtr  col-lg-6 col-sm-6 col-12 ">
            <button class="acceptbtn">Accept </button>
            <button class="nowbtn">Decline</button>
        </div>
    </div>
</a>
<a href="#">

    <div class="row bgw orderid">
        <div class="justi    col-lg-6 col-sm-7 col-12 ">
            <p class="myid">Order Number : H321654987 <em>|</em> May 01, 2020, 05:30 PM</p>

        </div>

        <div class="  txtr col-lg-6 col-sm-5 col-12 ">
            <button class="deliverbtn">Delivery </button>

        </div>
        <hr>
    </div>
    <div class="row bgw username">
        <div class=" col-lg-6 col-sm-6 col-12 ">
            <div class="media proimg">
                <span class="mr-3"><img  src="{{asset('assets/front/images/pro.jpg')}}"></span>
                <div class="media-body">
                    <h5 class="mt-0">Atkinson Allen</h5>
                    <p class="totalitem">Number of Items : <b>05</b> <em>|</em> Total Price: <b>$230</b></p>
                </div>
            </div>

        </div>

        <div class=" txtr  col-lg-6 col-sm-6 col-12 ">
            <button class="acceptbtn">Accept </button>
            <button class="nowbtn">Decline</button>
        </div>
    </div>

</a>
<a href="#">

    <div class="row bgw orderid">
        <div class="justi    col-lg-6 col-sm-7 col-12 ">
            <p class="myid">Order Number : H321654987 <em>|</em> May 01, 2020, 05:30 PM</p>

        </div>

        <div class="  txtr col-lg-6 col-sm-5 col-12 ">
            <button class="deliverbtn">Delivery </button>

        </div>
        <hr>
    </div>
    <div class="row bgw username">
        <div class=" col-lg-6 col-sm-6 col-12 ">
            <div class="media proimg">
                <span class="mr-3"><img  src="{{asset('assets/front/images/pro.jpg')}}"></span>
                <div class="media-body">
                    <h5 class="mt-0">Atkinson Allen</h5>
                    <p class="totalitem">Number of Items : <b>05</b> <em>|</em> Total Price: <b>$230</b></p>
                </div>
            </div>

        </div>

        <div class=" txtr  col-lg-6 col-sm-6 col-12 ">
            <button class="acceptbtn">Accept </button>
            <button class="nowbtn">Decline</button>
        </div>
    </div>

</a>
<a href="#">

    <div class="row bgw orderid">
        <div class="justi    col-lg-6 col-sm-7 col-12 ">
            <p class="myid">Order Number : H321654987 <em>|</em> May 01, 2020, 05:30 PM</p>

        </div>

        <div class="  txtr col-lg-6 col-sm-5 col-12 ">
            <button class="deliverbtn">Delivery </button>

        </div>
        <hr>
    </div>
    <div class="row bgw username">
        <div class=" col-lg-6 col-sm-6 col-12 ">
            <div class="media proimg">
                <span class="mr-3"><img  src="{{asset('assets/front/images/pro.jpg')}}"></span>
                <div class="media-body">
                    <h5 class="mt-0">Atkinson Allen</h5>
                    <p class="totalitem">Number of Items : <b>05</b> <em>|</em> Total Price: <b>$230</b></p>
                </div>
            </div>

        </div>

        <div class=" txtr  col-lg-6 col-sm-6 col-12 ">
            <button class="acceptbtn">Accept </button>
            <button class="nowbtn">Decline</button>
        </div>
    </div>
</a>

@endsection
