@extends('layouts.vendors.vendor')
@section('title','Add Group Member | Homebar')
@section('content')

<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 ">
        <h2>Group Member List</h2>
        <div class="navbar-light left-side-btn ml-auto mr-3">
            <button class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class=" col-lg-3 col-sm-4 col-12 pr0">
        @include('vendors.includes.bartendermenu')
    </div>
    <div class=" col-lg-9 col-sm-8 col-12 pl0 addmuenu-right ">
    <div class="container">
        <div class=" row  pt-3 bartender-list">
            <?php foreach ($groupmembers as $key => $value) { ?>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-img-top">
                        <img src="@if(empty($value->photo)){{asset('assets/front/images/pro1.jpg')}}@else{{asset($value->photo)}}@endif">
                    </div>

                    <div class="card-body">
                        <h5 class="card-title"><?= $value->name; ?></h5>
                        <p class="card-text">Call : <?= $value->mobile_number; ?></p>
                    </div>
                    <ul class="list-group list-group-flush ">
                        <li class="list-group-item">
                            Experience
                            <p><?= $value->experience; ?> Years</p>
                        </li>
                        <li class="list-group-item">
                            Signature Drink
                            <p><?= $value->signature_drink; ?></p>

                        </li>
                        <li class="list-group-item">
                            Email
                            <p><?= $value->email; ?></p>
                        </li>
                    </ul>
                    <div class="card-bottom">
                        <a href="{{ route('vendor.groupmember.edit', [$id ,$value->id]) }}" class="card-link"><!-- <img src="{{asset('assets/front/images/edit.svg')}}"> --> Edit</a>
                        <a href="{{ route('vendor.groupmember.destroy', $value->id) }}" class="card-link" onclick="return confirm('Are you sure you want to delete this member?');" ><!-- <img src="{{asset('assets/front/images/close.svg')}}"> -->Delete</a>
                    </div>
                </div>

            </div>
            <?php } ?>
        </div>
    </div>
    </div>
</div>

@endsection
