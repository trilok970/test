@extends('layouts.vendors.vendor')
@section('content')


<div class="row pag-head">
                        <div class="flex col-lg-12 col-sm-12 col-12 ">
                            <h2>Add Product</h2>
                        </div>
                    </div>


                    <div class="row mt-3">
                        <div class=" col-lg-3 col-sm-4 col-12 pr0">
                             @include('vendors.includes.ambaincemenu')

                        </div>

                        <div class=" col-lg-9 col-sm-8 col-12 pl0 addmuenu-right ">
                            <form class="mt-3">
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">categories </label>

                                    <select class="form-control selectpicker" placeholer="Spirit" id="exampleFormControlSelect1">
                                    <option>Decoration</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                  </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleFormControlSelect2">NaMe</label>
                                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Enter Name">
                                </div>

                                <div class="form-group">
                                    <label for="exampleFormControlSelect2">Price</label>
                                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Enter Price">
                                </div>


                                <div class="custom-redio smal">
                                    <p>
                                        <input type="radio" id="test1" name="radio-group" checked>
                                        <label for="test1">Per Dozons</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="test2" name="radio-group">
                                        <label for="test2">Per packet</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="test3" name="radio-group">
                                        <label for="test3">Per Piece</label>
                                    </p>
                                </div>

                                <label for="inputPassword4">Image</label>
                                <div class="box">
                                    <input type="file" name="file-5[]" id="file-5" class="inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple />
                                    <label for="file-5"><figure> <img src="{{asset('assets/front/images/img-icon.svg')}}"></figure> </label>
                                </div>
                                <button class="add mt-4">ADD PRODUCT</button>

                            </form>

                        </div>

                    </div>


@endsection
