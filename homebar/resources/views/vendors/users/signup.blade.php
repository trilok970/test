<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="{{asset('assets/front/images/favicon.jpg')}}" type="image/jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vendor Registration | Homebar</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- Custom -->
    <link href="{{asset('assets/front/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/front/css/responsive.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/front/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/front/css/mdtimepicker.min.css')}}" rel="stylesheet" type="text/css">
    <link href='//fonts.googleapis.com/css?family=Montserrat:thin,extra-light,light,100,200,300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap-select.css')}}">
    <style>

.eyebtn {
    position: absolute;
    bottom: 27px;
    right: 7px;
    background: white;
    padding: 7px;
    cursor: pointer;
    font-size: 14px;
    color: #787878;
}


    </style>
</head>

<body style="background: #ffffff;">


    <div class="row fixed-top" id="dynamic">
        <div class="container-fluid  ">
            <div class="row mt-3">
                <div class="col-lg-6 col-5 ">
                    <a href="{!! route('home') !!}"><img src="{{asset('assets/front/images/logo.png')}}"></a>
                </div>
                <?php
                if (!Session::has('VendorLoggedIn')) {
                  ?>
                <div class="col-lg-6 col-7 text-right">
                    <a href="{!! route('vendor.login') !!}" class="btn signin"> Sign in </a>
                    <a href="{!! route('vendor.signup') !!}" class="btn signup ml-2"> Sign Up </a>
                </div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="banner signupbh mt-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <h3 class="text-center rgtext">Registration</h3>
                </div>

            </div>
        </div>
    </div>




    <div class="container signupfroms">
        <div class="row ">
            <div class=" col-md-12">
                <h2>Sign Up</h2>
                <p>Complete the below fields</p>
            </div>
        </div>
        <!-- <form> -->
        {!! Form::open(['route' => 'vendor.store', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'signup-form','enctype' => 'multipart/form-data']) !!}
            <div class="row">
                <div class="form-group col-md-6">
                    <div class="section-height">
                    <label for="inputBussinessType">Business Type</label>
                    <select name="business_types[]" id="inputBussinessType" class="form-control" multiple="multiple">
                        @foreach ($types as $type)
                            <option value="{{$type->id}}" @if(old('business_types')!= null && in_array($type->id,old('business_types'))){{'selected'}}@endif>{{$type->name}}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('business_types', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                   </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="section-height">
                    <label for="inputPassword4">Business Name</label>
                    <?= Form::text('business_name',  old('business_name') , ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Business Name', 'id' => 'inputBussinessName']) ?>
                    {!! $errors->first('business_name', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <div class="section-height">
                    <label for="first_name">First Name</label>
                    <?= Form::text('first_name', old('first_name'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter First Name', 'id' => 'inputfirst_name']) ?>
                    {!! $errors->first('first_name', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                   </div>
                </div>
                <div class="form-group col-md-3">
                    <div class="section-height">
                    <label for="last_name">Last Name</label>
                    <?= Form::text('last_name', old('last_name'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Last Name', 'id' => 'inputlast_name']) ?>
                    {!! $errors->first('last_name', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="section-height">
                    <label for="inputEmail4">Email Address</label>
                    <?= Form::email('email', old('email'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Email', 'id' => 'inputEmail']) ?>
                    {!! $errors->first('email', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="form-group col-md-6">
                    <div class="section-height">
                    <label for="inputEmail4">Primary Mobile Number</label>
                    <?= Form::text('primary_mobile_number', old('primary_mobile_number'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Primary Mobile Number', 'id' => 'inputPrimaryMobileNumber', 'min' => 0]) ?>
                    {!! $errors->first('primary_mobile_number', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="section-height">
                    <label for="inputEmail4">Secondary Mobile Number</label>
                    <?= Form::text('secondary_mobile_number', old('secondary_mobile_number'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Secondary Mobile Number', 'id' => 'inputSecondaryMobileNumber', 'min'=> 0]) ?>
                    {!! $errors->first('secondary_mobile_number', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12">
                    <h4>Business Location</h4>
                </div>
                <div class="form-group col-md-6">
                    <div class="section-height address">
                    <label for="address">Address</label>
                    <?= Form::textarea('address', old('address'), ['class' => 'form-control sightextarea', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Address and choose from google map hint', 'id' => 'autocomplete', 'rows' => 4, 'cols' => 40]) ?>
                    {!! $errors->first('address', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}


                </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="row">
                        <div class="form-group col-md-7">
                            <div class="section-height">
                            <label for="inputCity">City</label>
                            <?= Form::text('city', old('city'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter City', 'id' => 'inputCity']) ?>
                            {!! $errors->first('city', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group col-md-5">
                            <div class="section-height">
                            <label for="inputZipCode">Zip Code</label>
                            <?= Form::text('zipcode', old('zipcode'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Zip code', 'id' => 'inputZipCode', 'maxlength' => 6]) ?>
                            {!! $errors->first('zipcode', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                            </div>
                        </div>


                    </div>
                </div>
                <!-- {!! Form::hidden('latitude', null, ['class'=>'form-control', 'id'=>'latitude']) !!}
                {!! Form::hidden('longitude', null, ['class'=>'form-control', 'id'=>'longitude']) !!} -->
                <div class="form-group col-md-6">
                    <div class="row">
                    <div class="form-group col-md-6">
                        <div class="section-height">
                            <label for="latitude">Latitude</label>
                            <?= Form::text('latitude', old('latitude'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Auto fill latitude', 'id' => 'latitude', 'readonly']) ?>
                            {!! $errors->first('latitude', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                            </div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="section-height">
                            <label for="longitude">Longitude</label>
                            <?= Form::text('longitude', old('longitude'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Auto fill longitude', 'id' => 'longitude', 'readonly']) ?>
                            {!! $errors->first('longitude', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                            </div>
                    </div>
                    </div>

                </div>
                <div class="form-group col-md-6">
                    <div class="row">
                    <div class="form-group col-md-6">
                        <div class="section-height">
                            <label for="inputState">State</label>
                            <?= Form::text('state', old('state'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter State', 'id' => 'inputState']) ?>
                            {!! $errors->first('state', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                            </div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="section-height">
                            <label for="country">Country</label>
                            <?= Form::select('country_id', $countries, old('country_id') ,['class' => 'selectpicker customselect form-control', 'id' => 'country', 'data-container' => 'body', 'data-live-search' => 'true', 'title' => 'Select Country', 'data-hide-disabled' => 'true' ]); ?>
                            {!! $errors->first('country_id', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                            </div>
                    </div>
                    </div>

                </div>

            </div>

            <div class="row">
                <div class="form-group col-md-12 ">
                    <h4 class="mt-0">Hours of Availability</h4>
                </div>

                <div class="form-group col-md-3 clock">
                    <div class="section-height">
                    <label for="inputOpenTime">Open Time</label>
                    <?= Form::text('open_time', old('open_time'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Open Time', 'id' => 'inputOpenTime']) ?>

                    <i class="fa fa-clock-o " aria-hidden="true"></i>
                    {!! $errors->first('open_time', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group col-md-3 clock">
                    <div class="section-height">
                    <label for="inputCloseTime">Close Time</label>
                    <?= Form::text('close_time', old('close_time'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Close Time', 'id' => 'inputCloseTime']) ?>
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    {!! $errors->first('close_time', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group col-md-3 d-flex p-t-4 flex-wrap">
                    <div class="section-height check-box">
                    <div class="form-check">
                        <input class="form-check-input gridCheck" type="checkbox" id="is_pickup" name="pickup" @if(old('pickup')){{'checked'}}@endif>
                        <label class="form-check-label" for="is_pickup">
                        Pickup Available
                        </label>
                    </div>
                    {!! $errors->first('pickup', '<p class="help-block" style="color: red;  font-weight: bold; width:100%;text-align: left;">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group col-md-3 clock d-flex p-t-4 flex-wrap">
                    <div class="section-height">
                    <div class="form-check">
                        <input class="form-check-input gridCheck" type="checkbox" id="is_delivery" name="delivery" @if(old('delivery')){{'checked'}}@endif>
                        <label class="form-check-label" for="is_delivery">
                        Delivery Available
                        </label>
                    </div>
                    {!! $errors->first('delivery', '<p class="help-block" style="color: red; float: left; font-weight: bold; width:100%;text-align: left;">:message</p>') !!}
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="form-group col-md-6">
                    <div class="section-height position-relative">
                    <label for="inputPassword">Password</label>
                    <?= Form::password('password', ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Password', 'id' => 'password']) ?>
                    {!! $errors->first('password', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                        <span id_key="password" class="eyebtn fa fa-eye-slash">
                            <!-- <i class="fa fa-eye" aria-hidden="true"></i> -->
                            <!-- <i class="fa fa-eye-slash" aria-hidden="true"></i> -->
                        </span>

                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="section-height">
                    <label for="inputConfirmPassword">Confirm Password</label>
                    <?= Form::password('password_confirmation', ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Confirm Password', 'id' => 'password_confirmation']) ?>
                    {!! $errors->first('password_confirmation', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}

                      <span id_key="password_confirmation" class="eyebtn fa fa-eye-slash">
                            <!-- <i class="fa fa-eye" aria-hidden="true"></i> -->
                            <!-- <i class="fa fa-eye-slash" aria-hidden="true"></i> -->
                        </span>
                    </div>
                </div>
            </div>

            <div class="form-group form-check d-flex pl-0 flex-wrap">

                <input class="styled-checkbox gridCheck" id="styled-checkbox-1" name="term_service" type="checkbox" value="1" @if(old('term_service')){{'checked'}}@endif>

                <label class="form-check-label ml-2" for="gridCheck">
                        I have read and agree to the <a href="#"><span>Terms of Service.</span></a>
                </label>
                {!! $errors->first('term_service', '<br><p class="help-block" style="color: red; text-align:left; font-weight: bold; width: 100%;">:message</p>') !!}

            </div>


                <?= Form::submit('Sign Up', ['class' => 'btn sighn-up']) ?>

            <div class="already">Already have an account? <a href="{!! route('vendor.login') !!}"> Sign in </a> </div>
        <!-- </form> -->
        {{ Form::close() }}
    </div>
    <!--    go top-->

     @include('vendors.includes.footer')

    @include('vendors.includes.scripts')
<script src="{{asset('assets/front/js/moment.min.js')}}"></script>
<script src="{{asset('assets/front/js/mdtimepicker.min.js')}}"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNVV0WGqduJq4EsX8_Y_s8L-hiZrHmrj4&libraries=places&callback=initialize"
        async defer></script>

<script type="text/javascript">
$(document).ready(function(){
    $('#inputOpenTime').mdtimepicker();
    $('#inputCloseTime').mdtimepicker();
});
</script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script>
    $(document).ready(function(){
        $("#inputBussinessType").select2({
            placeholder:'Select business type(s)'
        });
    });
</script>
<script type="text/javascript">
  //google map
  $("#autocomplete").change(function(){
    $("#latitude").val('');
    $("#longitude").val('');
  });

  function initialize()
  {
      var input = document.getElementById('autocomplete');
      var options = {};
      var autocomplete = new google.maps.places.Autocomplete(input, options);
      google.maps.event.addListener(autocomplete, 'place_changed', function () {
          var place = autocomplete.getPlace();
          var lat = place.geometry.location.lat();
          var lng = place.geometry.location.lng();
          $("#latitude").val(lat);
          $("#longitude").val(lng);
      });
  }

  google.maps.event.addDomListener(window, 'load', initialize);

</script>

<!-- <script type="text/javascript">
    $(function () {
        $('#inputOpenTime').datetimepicker({
            format: 'LT'
        });
    });
</script> -->

<script src="{{asset('assets/admin/notify.min.js')}}"></script>
        <?php
        $msg_type = (session()->has('success') ? 'success' : ( session()->has('error') ? 'error' : ( session()->has('warning') ? 'warning' : '')));
        $message = '';
        if($msg_type) {
            $message = session()->get($msg_type);
        }
        ?>
        <script>
            var msg_type = "<?= $msg_type ?>";
            var message = "<?= $message ?>";
            if(msg_type) {
                $.notify(message, msg_type);
            }
        </script>
         <script type="text/javascript">
             $(".eyebtn").click(function(){
                // $(".fa-eye").toggleClass("fa-eye-slash");
                $(this).toggleClass("fa-eye fa-eye-slash");
                var id = $(this).attr('id_key');
                var input = $("#"+id).attr("type");
                // console.log("type >> "+input);

                if (input == "password") {
                    $("#"+id).attr("type", "text");
                } else {
                    $("#"+id).attr("type", "password");
                }
              });
        </script>


</body>

</html>
