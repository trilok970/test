@extends('layouts.vendors.vendor')

@section('content')

<div class="container">

    <div class="row">

        <div class="container signupfroms">
 
        @if (session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <div class="row pag-head mt-0">
            <div class="flex col-lg-12 col-sm-12 col-12  pdL-l0 pdr-0">
                <div class="navbar-light left-side-btn ml-auto mr-3">
                    <button class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>
                </div>
                <!-- <a href="drink-add-menu-pre.html" class="nowbtn pr-5 pl-5"> ADD MENU </a> -->
            </div>
        </div>

    {!! Form::open(['route' => ['vendor.changePassword'], 'method' => 'post', 'files'=>'true']) !!}

    <div class="row">
        <div class="col-sm-4">
            <label for="inputEmail">Current Password :</label>
            <?=  Form::password('current_password', ['class' => 'form-control', 'placeholder' => 'Current Password', 'id' => 'inputEmail']); ?>
            {!! $errors->first('current_password', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
        </div>

        <div class="col-sm-4">
            <label for="inputEmail">New Password :</label>
            <?=  Form::password('new_password', ['class' => 'form-control', 'placeholder' => 'New Password', 'id' => 'inputEmail']); ?>
            {!! $errors->first('new_password', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
        </div>

        <div class="col-sm-4">
            <label for="inputName">Confirm New Password :</label>
            <?=  Form::password('new_password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm New Password', 'id' => 'inputEmail']); ?>
            {!! $errors->first('new_password_confirmation', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
        </div>

        <div class="col-lg-12" style="margin: 1% 0;"></div>

        <div class="col-sm-2">
            <?= Form::submit('Update', ['class' => 'btn sighn-up']) ?>
        </div>

    </div>
    <!-- .row -->

    {{ Form::close() }}


    </div>
    </div>

</div>
@endsection

