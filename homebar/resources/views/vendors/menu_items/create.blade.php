@extends('layouts.vendors.vendor')
@section('content')

<?php
$id = '';
if(empty($entity->id)) {
    $action_route = ['vendor.menuItem.store', [$type_id]];
    $method = 'POST';
} else {
    $id = $entity->id;
    $action_route = ['admin.categories.update', [$entity->id]];
    $method = 'POST';
}
?>
<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 ">
        <h2><?= $title_page ?></h2>
        <div class="navbar-light left-side-btn ml-auto mr-3">
            <button class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>
        </div>
    </div>
</div>


<div class="row mt-3">
    <div class=" col-lg-3 col-sm-4 col-12 pr0">

        @include('vendors.includes.drinkmenu')

    </div>
    <?php

    ?>

    <div class=" col-lg-9 col-sm-8 col-12 pl0 addmuenu-right ">
        {!! Form::model($entity, ['route' => $action_route, 'method' => $method, 'class'=>'mt-3', 'enctype'=>'multipart/form-data', 'id'=>'menu_form']) !!}
        {{ csrf_field() }}
            <div class="form-group">
                <label for="category_id">Category </label>
                <?= Form::select("category_id", $categories, null, ['placeholder'=>'Category','class'=>'form-control selectpicker', 'required'=>true, 'id'=>'category_select'])?>
                <span class='error-field' id='err_category_id'></span>
            </div>

            <div class="form-group">
                <label for="subcategory_id">Sub Category</label>
                <?= Form::select('subcategory_id', $sub_categories, null, ['placeholder'=>'Sub category','class'=>'form-control selectpicker', 'required'=>true, 'id'=>'subcategory_select'])?>
                <span class='error-field' id='err_subcategory_id'></span>
            </div>
            <div class="form-group">
                <label for="brand_id">Brand</label>
                <?= Form::select('brand_id', $brands, null, ['placeholder'=>'Brand','class'=>'form-control selectpicker', 'required'=>true, 'id'=>'brand_select'])?>
                <span class='error-field' id='err_brand_id'></span>
            </div>

            <div class="form-group">
                <label for="name">Name</label>
                <?= Form::text("name", null, ['placeholder'=>'Enter Name','class'=>'form-control'])?>
                <span class='error-field' id="err_name"></span>
            </div>

            <div class="form-group">
                <label for="description">Description </label>
                <?= Form::textArea("description", null, ['placeholder'=>'Description','class'=>'form-control', 'rows'=>3])?>
                <span class='error-field' id="err_description"></span>
            </div>
            <div class="form-group">
                <label for="brand_id">In Stock</label>
                <?= Form::select('in_stock', [1=>"In Stock",0=>"Out Of Stock"], null, ['class'=>'form-control selectpicker', 'required'=>true, 'id'=>'in_stock'])?>
                <span class='error-field' id='err_brand_id'></span>
            </div>
            <div class="form-group">
                <label for="is_alcoholic">Is Alcoholic</label>
                <?= Form::select('is_alcoholic', [1=>"Yes",0=>"No"], null, ['class'=>'form-control selectpicker', 'required'=>true, 'id'=>'is_alcoholic'])?>
                <span class='error-field' id='err_is_alcoholic'></span>
            </div>
            <label for="inputPassword4">Image</label>
            <div class="box">
                <input type="file" name="images[]" id="file-5" class="inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple />
                <label for="file-5"><figure> <img src="{{asset('assets/front/images/img-icon.svg')}}"></figure> </label>
                <span class='error-field' id='err_image'></span>
            </div>
            <div class="imageall-box">
            <?php
            foreach($entity->menuItemImages as $image_key => $image_row) {
            ?>
                <div class="box image-box">
                    <a href="javascript:void(0);" class="image_delete" row_id="<?= $image_row->id ?>">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                    <img src="<?= asset($image_row->image) ?>">
                </div>
            <?php } ?>
            </div>

            <div id='multi_form_secs'>
                <?php for($j=1; $j<=$i; $j++){
                ?>
                @include('vendors.menu_items.create_form', ['i'=>$j])
                <?php } ?>
            </div>
            <button class="add mt-4" id='btn_add_variation' type='button'>ADD VARIATION</button>
            <?= Form::hidden('entity_id', $id, ['id'=>'entity_id'])?>
            <?= Form::hidden('total_var', $i, ['id'=>'total_var'])?>
            <?= Form::hidden('type_id', $type_id, ['id'=>'err_type_id'])?>
            <?= Form::submit('ADD TO MENU', ['class' => 'mb-4 new-btn']) ?>

            {{ Form::close() }}

    </div>

</div>

@endsection

@section('uniquepagescript')
<script>
function validateImage(){
    is_valid = 1;
    for (var i = 0; i < $("#file-5").get(0).files.length; ++i) {
        var file1=$("#file-5").get(0).files[i].name;

        if(file1){
            var ext = file1.split('.').pop().toLowerCase();
            if($.inArray(ext,['jpg','jpeg','png'])===-1){
                $('#err_image').text('Only jpg, jpeg, png images are allowd.')
                is_valid = 0;
            }
        }
    }
    return is_valid;
}

$('.image_delete').click(function(){
    var image_id = $(this).attr('row_id');
    obj = $(this);
    jQuery.ajax({
        url: "<?= route('vendor.menuItem.deleteImage') ?>",
        type: 'POST',
        data:{
            image_id:image_id
        },
        dataType:'JSON',
        headers: {
        'X-CSRF-Token': '{{ csrf_token() }}'
        },
        beforeSend: function(){
            $('#site-loader').show();
        },
        success: function (data) {
            obj.parent().slideUp('slow');
            $('#site-loader').hide();
        }
    });
});

$('#category_select').change(function() {
    category_id = $(this).val();
    $("#subcategory_select option").remove();
    $("#subcategory_select").prepend("<option value=''>Sub Category</option>");
    $('#subcategory_select').selectpicker('refresh');

    $("#brand_select option").remove();
    $("#brand_select").prepend("<option value=''>Brand</option>");
    $('#brand_select').selectpicker('refresh');
    var total_var = $('#total_var').val();

    if(category_id){
        jQuery.ajax({
            url: "<?= route('vendor.menuItem.getSubcategoriesBrandsAndAttributes') ?>",
            type: 'POST',
            data:{
                category_id:category_id,
                i:total_var
            },
            dataType:'JSON',
            headers: {
            'X-CSRF-Token': '{{ csrf_token() }}'
            },
            beforeSend: function(){
                $('#site-loader').show();
            },
            success: function (data) {
                var toAppend = '';
                $.each(data['sub_categories'], function (i, o) {
                    toAppend += '<option value=' + i + '>' + o + '</option>';
                });

                $('#subcategory_select').append(toAppend);
                $('#subcategory_select').selectpicker('refresh');

                toAppend = '';
                $.each(data['brands'], function (i, o) {
                    toAppend += '<option value=' + i + '>' + o + '</option>';
                });

                $('#brand_select').append(toAppend);
                $('#brand_select').selectpicker('refresh');
                $('#site-loader').hide();
            }
        });
    }
});

$('#btn_add_variation').click(function() {

    var total_var = $('#total_var').val();
    total_var = parseInt(total_var);
    var new_total_var = total_var + 1;
    var category_id = $('#category_select').val();
    $('#total_var').val(new_total_var);

    jQuery.ajax({
        url: "<?= route('vendor.menuItem.addVariation') ?>",
        type: 'POST',
        data:{
            i:new_total_var,
            category_id: category_id
        },
        dataType:'JSON',
        headers: {
            'X-CSRF-Token': '{{ csrf_token() }}'
        },
        beforeSend: function(){
            $('#site-loader').show();
        },
        success: function (data) {
            $('#multi_form_secs').append(data['variation_view']);
            $('#site-loader').hide();
        }

    });

});

$('#menu_form').submit(function(e) {
    e.preventDefault();
    $('.error-field').html('');
    is_valid = validateImage();
    if(is_valid == 0){
        notifyAlert('Please correct the errors below and try again', 'error');
        return false;
    }

    var form = $(this);
    var form = $('#menu_form');
    form_data = form.serialize();
    var form_files = new FormData(this);
    //form_files.append("image", document.getElementById('file-5').files[0]);
    console.log(form_data);

    var total_var = $('#total_var').val();


    jQuery.ajax({
        url: "<?= route('vendor.menuItem.store') ?>",
        type: 'POST',
        data:form_files,
        dataType:'JSON',
        processData: false,
        contentType: false,
        beforeSend: function(){
            $('#site-loader').show();
        },
        headers: {
            'X-CSRF-Token': '{{ csrf_token() }}'
        },
        success: function (response) {
            if(response['status'] == false){
                notifyAlert(response['message'], 'error');
                $.each(response['data'], function( key, value ) {
                    $('#err_'+key).text(value[0]);
                });
            } else {
                window.location.href = "<?= route('vendor.menuItem.list', $type) ?>";
            }
            $('#site-loader').hide();
        }

    });

});

</script>
@endsection
