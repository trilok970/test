<div class='multi-form-sec' id='multi_form_sec_1'>
    
    <?= Form::hidden("id_$i", null, ['id'=>"id_$i"])?>
    
    <div class="form-row attr-sec">
        <div class="form-group col-lg-4 col-md-4 col-sm-12">
            <label for="price">Price</label>
            <?= Form::number("price_$i", null, ['placeholder'=>'Enter Price','class'=>'form-control', 'id'=>"price_$i", 'step'=>".01"])?>
            <span class='error-field' id="err_price_<?= $i ?>"></span>
        </div>

        <div class="form-group col-lg-4 col-md-4 col-sm-12">
            <label for="size">Size</label>
            <?= Form::text("size_$i", null, ['placeholder'=>'Enter Size','class'=>'form-control', 'id'=>"size_$i"])?>
            <span class='error-field' id="err_size_<?= $i ?>"></span>
        </div>
    </div>

    

</div>
