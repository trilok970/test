<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="{{asset('assets/front/images/favicon.jpg')}}" type="image/jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Homebar | Contact Us</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- Custom -->
    <link rel="stylesheet" href="{{asset('assets/front/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/swiper.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/responsive-index.css')}}">

</head>

<body id="page-top">
    <!-- Navigation-->
    <div class="row fixed-top" id="dynamic">
        <div class="container-fluid  ">
            <div class="row mt-3">
                <div class="col-lg-6 col-5 ">
                    <img src="{{asset('assets/front/images/logo.png')}}">
                </div>
                @if (!Session::has('VendorLoggedIn'))
                <div class="col-lg-6 col-7 text-right">
                    <a href="{!! route('vendor.login') !!}" class="btn signin"> Sign in </a>
                    <a href="{!! route('vendor.signup') !!}" class="btn signup ml-2"> Sign Up </a>
                </div>
                @else
                <div class="col-lg-6 col-7 text-right">
                    <a href="{!! route('vendor.orders','drinks') !!}" class="new-btn"> Dashboard</a>
                </div>
                @endif
            </div>
        </div>
    </div>




   <div class="col-md-12" style="margin-top: 104px;margin-bottom: 100px;"> 
<h1>   Contact Us</h1>
</div>

   



   


   


   

    

    <footer class="footer py-4 ">
        <div class="container">
            <div class="row align-items-center ptf">
                <div class="col-lg-6 text-lg-left f-links">

                    <a class="mr-3" href="{{ url('about-us') }}"> About Us</a>
                    <a class="mr-3" href="{{ url('faqs') }}"> Faq’s </a>
                    <a class="mr-3" href="{{ url('contact-us') }}"> Contact Us</a>
                    <a class="mr-3" href="{{ url('help-center') }}"> Help Center </a>
                    <a class="mr-3" href="{{ url('support') }}"> Support </a>
                    <a class="mr-3" href="{{ url('privacy-policy') }}"> Privacy Policy </a>
                    <a class="mr-3" href="{{ url('terms-and-conditions') }}"> Terms And Conditions </a>

                </div>

                <div class="col-lg-6 my-3 my-lg-0 text-lg-right social">
                    <span>Connect with us</span>
                    <a class="btn  btn-social " href="#!"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a class="btn btn-social " href="#!"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
                    <a class="btn  btn-social " href="#!"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                </div>

            </div>
        </div>
        <div class="col-lg-12 text-lg-center f-bdr mt-5 copy">© Homebar.com 2020. All Rights Reserved.</div>
    </footer>




    <!--    go top-->
    <a href="#" id="gotop" data-aos="fade-in" style="display: inline-block;"> <i class="fa fa-angle-up"> </i> </a>

    <!-- Scripts -->
    <script src="{{asset('assets/front/js/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('assets/front/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
    <!-- aos animation -->
    <script src="{{asset('assets/front/js/aos.js')}}"></script>
    <!-- Owl Carousel -->
    <script src="{{asset('assets/front/js/owl.carousel.min.js')}}"></script>

    <script src="{{asset('assets/front/js/swiper.min.js')}}"></script>
    <script src="{{asset('assets/front/js/who.js')}}"></script>
    <script src="{{asset('assets/front/js/custom.js')}}"></script>

    <script src="{{asset('assets/front/js/bootstrap-select.js')}}"></script>
    <script>
        $(window).scroll(function() {
            if ($(this).scrollTop() > 50) {
                $('#dynamic').addClass('newClass');
            } else {
                $('#dynamic').removeClass('newClass');
            }
        });
    </script>

</body>

</html>
