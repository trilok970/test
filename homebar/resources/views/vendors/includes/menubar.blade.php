<?php
    $url_params = request()->route()->parameters;
    $type = isset($url_params['type']) ? $url_params['type'] : '';
    $btypes = explode(",",auth()->user()->business_type);

	$route = Route::currentRouteAction();
	$explode_route = explode('\\', $route);
	$controller_ation = end($explode_route);
	$controller_ation_ex = explode('@', $controller_ation);

	$urltype = request()->segment(3);
//	echo $urltype; die;
	$urlmenutype = request()->segment(2);
	if(isset($urltype) && $urltype=="progress"){
		$routename = "inprogress-orders";
	}
	else if(isset($urltype) && $urltype=="completed"){
		$routename = "complete-orders";
	}
	else if(isset($urltype) && $urltype=="rejected"){
		$routename = "rejected-orders";
	}
	else if(isset($urlmenutype) && $urlmenutype=="menu-items"){
		$routename = "menuItem.list";
	}
	else if(isset($urlmenutype) && $urlmenutype=="offers"){
		$routename = "offers.index";
	}

	else{
		$routename = "orders";
	}


?>
<div class="row btns dash-btn dash-btn-desktop">
    @php  $active = ($type == 'all')? 'active' : ''; @endphp

     @if(isset($urlmenutype) && $urlmenutype=="offers")

      @else
      	<?php $routename = "orders"; ?>
	    <div class="col-lg-3 col-sm-3 col-1">
	        <a class="{{ $active }}" href="<?=route('vendor.'.$routename, 'all')?>">All</a>
	    </div>

      @endif
    <?php
    foreach ($types as $key => $value) {
		if($controller_ation_ex[1] == 'bartendermyearning'){

//			 if($key == 0){ $active = 'active';  }else{ $active = ''; }
		}else{
			 $active = ($type == $value->slug)? 'active' : '';
		}


    ?>

    @if(in_array($value->id,$btypes))
    @if($value->id != 4)
    <div class="col-lg-3 col-sm-3 col-12">
        <a class="<?= $active ?>" href="<?= route('vendor.'.$routename, $value->slug) ?>"><?= $value->name; ?></a>
    </div>
    @endif
    @else
    <div class="col-lg-3 col-sm-3 col-12">
        <a class="<?= $active ?>" href="<?= route('vendor.category-not-found') ?>"><?= $value->name; ?></a>
    </div>
    @endif
    <?php
    }
    ?>
</div>
