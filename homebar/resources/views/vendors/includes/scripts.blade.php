    <!-- Scripts -->
    <script src="{{asset('assets/front/js/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('assets/front/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
    <!-- aos animation -->
    <script src="{{asset('assets/front/js/aos.js')}}"></script>
    <!-- Owl Carousel -->
    <script src="{{asset('assets/front/js/owl.carousel.min.js')}}"></script>

    <script src="{{asset('assets/front/js/swiper.min.js')}}"></script>
    <script src="{{asset('assets/front/js/who.js')}}"></script>
    <script src="{{asset('assets/front/js/custom.js')}}"></script>

    <script src="{{asset('assets/front/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('assets/front/js/moment-with-locales.min.js')}}"></script>
    {{-- <script src="{{asset('assets/front/js/calendar.js')}}"></script> --}}
    <script src="{{asset('assets/front/js/daterangepicker.js')}}"></script>

    <script src="{{asset('assets/admin/sweetalert.min.js')}}"></script>
    <script>
        $(window).scroll(function() {
            if ($(this).scrollTop() > 50) {
                $('#dynamic').addClass('newClass');
            } else {
                $('#dynamic').removeClass('newClass');
            }
        });
    </script>

    <script>
        // Sticky navbar
        // =========================
        $(document).ready(function() {
            // Custom function which toggles between sticky class (is-sticky)
            var stickyToggle = function(sticky, stickyWrapper, scrollElement) {
                var stickyHeight = sticky.outerHeight();
                var stickyTop = stickyWrapper.offset().top;
                if (scrollElement.scrollTop() >= stickyTop) {
                    stickyWrapper.height(stickyHeight);
                    sticky.addClass("is-sticky");
                } else {
                    sticky.removeClass("is-sticky");
                    stickyWrapper.height('auto');
                }
            };

            // Find all data-toggle="sticky-onscroll" elements
            $('[data-toggle="sticky-onscroll"]').each(function() {
                var sticky = $(this);
                var stickyWrapper = $('<div>').addClass('sticky-wrapper'); // insert hidden element to maintain actual top offset on page
                sticky.before(stickyWrapper);
                sticky.addClass('sticky');

                // Scroll & resize events
                $(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function() {
                    stickyToggle(sticky, stickyWrapper, $(this));
                });

                // On page load
                stickyToggle(sticky, stickyWrapper, $(window));
            });
        });
    </script>

    <script src="{{asset('assets/front/js/moment.min.js')}}"></script>
<script src="{{asset('assets/front/js/mdtimepicker.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('#inputOpenTime').mdtimepicker();
    $('#inputCloseTime').mdtimepicker();
});
</script>
<script>
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>

<?php /*
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNVV0WGqduJq4EsX8_Y_s8L-hiZrHmrj4&libraries=places&callback=initialize"
        async defer></script>

<script type="text/javascript">
  //google map
  $("#autocomplete2").change(function(){
    $("#latitude").val('');
    $("#longitude").val('');
  });

  function initialize()
  {
      var input = document.getElementById('autocomplete2');
      var options = {};
      var autocomplete2 = new google.maps.places.Autocomplete(input, options);
      google.maps.event.addListener(autocomplete2, 'place_changed', function () {
          var place = autocomplete.getPlace();
          var lat = place.geometry.location.lat();
          var lng = place.geometry.location.lng();
          $("#latitude").val(lat);
          $("#longitude").val(lng);
      });
  }

  google.maps.event.addDomListener(window, 'load', initialize);

</script>
*/ ?>

<!-- daterangepicker for dob on adding bartender -->
<script>
$(function() {
  $('input[name="birthday"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1940,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'YYYY-MM-DD'
    }
  });
});
</script>
<script>
$(function() {
  $('input[name="bartending_license_expirations"]').daterangepicker({
    // autoUpdateInput: false,
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1990,
    maxYear: 2100,
    locale: {
        format: 'YYYY-MM-DD'
    }
    // maxYear: parseInt(moment().format('YYYY'),11)
  }, function(chosen_date) {
      $('input[name="bartending_license_expirations"]').val(chosen_date.format('YYYY-MM-DD'));
    });
});

function notifyAlert(message, msg_type) {
    $.notify(message, msg_type);
}

function confirmDelete(event, obj) {
    event.preventDefault();
    var link = $(obj).attr('href');
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this record!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    },
    function () {
        window.location.href = link;
    });
}
</script>

@yield('uniquepagescript')


<script type="text/javascript">
    $(document).ready(function(){
        $('.timeFrom').mdtimepicker();
        $('.timeTo').mdtimepicker();
    });
</script>
