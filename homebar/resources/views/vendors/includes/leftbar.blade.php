
<style type="text/css">
input[type="file"] {
    display: none;
}
.custom-file-upload2 {
    border: none;
    cursor: pointer;
}
</style>
<?php
$route = Route::currentRouteAction();
$explode_route = explode('\\', $route);
$controller_ation = end($explode_route);
$controller_ation_ex = explode('@', $controller_ation);
$controller = isset($controller_ation_ex[0]) ? $controller_ation_ex[0] : '';
$action = isset($controller_ation_ex[1]) ? $controller_ation_ex[1] : '';
$url_params = request()->route()->parameters;
  $type = isset($url_params['type']) ? $url_params['type'] : '';
$user = Auth::user();
?>
<div class="left-side mobile-left-side">
    {!! Form::open(['route'=>'vendor.logo.update','id'=>'logoUploadForm','files'=>true]) !!}
    <div class="info"><span>CHANGE PROFILE</span>
        <label for="file2" class="custom-file-upload2 ">
             <img src="{{asset('assets/front/images/dots.svg')}}" title="Update Logo">
        </label>
        <input id="file2" type="file" name="logo" onchange="logoupdate();" />
    </div>

    <div class="left-logo">
        <?php if(!empty($user->logo_image)) { ?>
            <img src="<?=  URL::to('/'). $user->logo_image; ?>">
        <?php } else { ?>
            <img src="{{asset('assets/front/images/left-logo.png')}}">
        <?php } ?>
        @if(Auth::user()->availability == 1)
        <button availability="0" url="{{ url('vendor/user-availability/0') }}"   class="nowbtn available" >Available Now <i class="fa fa-check-circle" aria-hidden="true"></i>
        </button>
        @else
        <button availability="1" url="{{ url('vendor/user-availability/1') }}"  class="nowbtn available" style="background:#e62234;" >Unavailable Now <i class="fa fa-check-circle" aria-hidden="true"></i>
        </button>
        @endif
    </div>
    <input type="hidden" name="user_id" id="user_id" value="<?= $user->id; ?>">
    {{ Form::close() }}

    <p class="ptext pb-2 mt-3">
        <b><?= $user->first_name; ?> <?= $user->last_name; ?></b>
    </p>
    <p class="ptext ">
        <span><?= $user->email; ?></span>
    </p>

    <p class="ptext pt-4">
        <span>Hours of Availability</span>
    </p>
    <p class="ptext pt-2">
        <b><?= date("g:i a", strtotime($user->open_time)); ?> - <?= date("g:i a", strtotime($user->close_time)); ?></b>
    </p>

    <ul>
        <li><a <?php if(Request::is('vendor/my-profile')) { echo 'class="active"'; } ?> href="<?= route('vendor.myprofile') ?>">My Profile</a></li>

        <li><a <?php if(Request::is('vendor/orders/recent/*')) { echo 'class="active"'; } ?> href="<?= route('vendor.orders',request()->route()->parameters && isset(request()->route()->parameters['type'])?request()->route()->parameters['type']:'drinks') ?>">New Orders</a></li>

        <li><a <?php if(Request::is('vendor/orders/progress/*')) { echo 'class="active"'; } ?> href="<?= route('vendor.inprogress-orders',request()->route()->parameters && isset(request()->route()->parameters['type'])?request()->route()->parameters['type']:'drinks') ?>">
			@if($type == 'bartender')
				Order In Accepted
			@else
				In Progress Orders
			@endif
			</a></li>

        <li><a <?php if(Request::is('vendor/orders/rejected/*')) { echo 'class="active"'; } ?> href="<?= route('vendor.rejected-orders',request()->route()->parameters && isset(request()->route()->parameters['type'])?request()->route()->parameters['type']:'drinks') ?>">Rejected/Cancelled Orders</a></li>

        <li><a <?php if(Request::is('vendor/orders/completed/*')) { echo 'class="active"'; } ?> href="<?= route('vendor.complete-orders',request()->route()->parameters && isset(request()->route()->parameters['type'])?request()->route()->parameters['type']:'drinks') ?>">Completed Orders</a></li>

        <?php
            $active = ($controller == 'MenuItemsController') ? 'active' : '';
        ?>
        @if(Request::is('*/drinks') || Request::is('*/food') || Request::is('*/ambaince'))
            <li><a class="<?= $active ?>" href="<?= route('vendor.menuItem.list', $type) ?>">Menu Listing</a></li>
        @endif
        @php
            $btypes = explode(",",auth()->user()->business_type);
        @endphp

        @if(Request::is('*/bartender') || Request::is('*/bartenders*'))
            <li><a <?php if(Request::is('bartenders') || Request::is('*/bartenders*')) { echo 'class="active"'; } ?> href="<?= route('vendor.bartenderlist') ?>">Bartender List</a></li>
        @endif
        {{-- <li><a @if(Request::is('vendor/bartender-calendar-schedule')) {{'class="active"'}} @endif href="{{route('vendor.bartendercalendarschedule')}}">My Schedule</a></li> --}}

        <li><a <?php if(Request::is('vendor/change-password')) { echo 'class="active"'; } ?> href="<?= route('vendor.changePassword') ?>">Change Password</a></li>

        <li><a <?php if(Request::is('vendor/bartender-my-earning')) { echo 'class="active"'; } ?> href="<?= route('vendor.bartendermyearning') ?>">My Earnings</a></li>



        @if(Request::is('*/drinks') || Request::is('*/food'))
            <li><a <?php if(Request::is('vendor/do-it-yourself/*')) { echo 'class="active"'; } ?> href="<?= route('vendor.diy.index',request()->route()->parameters && isset(request()->route()->parameters['type'])?request()->route()->parameters['type']:'drinks') ?>">Do it Yourself</a></li>
        @endif
        <li><a <?php if(Request::is('vendor/offers/*')) { echo 'class="active"'; } ?> href="<?= route('vendor.offers.index','drinks') ?>">Discount Offers</a></li>

        <li><a class="logout"  href="<?= route('vendor.logout') ?>" >Log out</a></li>
    </ul>
</div>
<script type="text/javascript">
   function logoupdate() {
        document.getElementById("logoUploadForm").submit();
   }
</script>

@push('scripts')
    <script>
        $(document).ready(function(){
            $(".available").click(function(evt){
                evt.preventDefault();
                var url = $(this).attr('url');
                var postData = $(this).attr('availability');




                 swal({
        title: "Are you sure?",
        text: "You will be able to change availability of this vendor!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, change it!",
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {

        $.get(url,postData,function(out){
                    if(out.status==1) {
                        swal("Success!", out.msg, "success");
                        setTimeout(function(){
                            window.location.reload();
                        },2000)
                    }else {
                swal ( "Opps" ,  out.msg+"!" ,  "error" );
                        // alert(out.msg);
                    }
                });

            swal("Success!", out.msg, "success");
        } else {
            swal("Cancelled", "Your availability is not changed :)", "error");
        }
    }
);


            });

            $(".logout").click(function(evt){
                evt.preventDefault();
                var url = $(this).attr('href');
                var postData = $(this).attr('availability');

                 swal({
        title: "Are you sure?",
        text: "You want to log out",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes!",
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {
            swal("Success!", "you are logged out :)", "success");
            location.href = url;
        } else {
            swal("Cancelled", "you are logged in :)", "error");
        }
    }
);


            });


        });

    </script>
@endpush
