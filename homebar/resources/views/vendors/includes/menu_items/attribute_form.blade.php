
<?php
foreach($attributes as $attr_id => $attr_name){
    $value = isset($attr_values[$attr_id]) ? $attr_values[$attr_id]: ''; 
    
?>
    <div class="form-group col-lg-4 col-md-4 col-sm-12">
        <label for="inputEmail4"><?= $attr_name ?></label>
        <?= Form::text("attr_".$attr_id."_$i", $value, ['placeholder'=>$attr_name,'class'=>'form-control'])?>
    </div>
<?php
}
?>

