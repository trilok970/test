<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="{{asset('assets/front/images/favicon.jpg')}}" type="image/jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- Custom -->

    <link href="{{asset('assets/front/css/responsive.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/front/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/front/css/mdtimepicker.min.css')}}" rel="stylesheet" type="text/css">

    {{-- <link href="{{asset('assets/front/css/calendar.css')}}" rel="stylesheet" type="text/css"> --}}
    <link href="{{asset('assets/front/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/front/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/front/css/daterangepicker.css')}}" rel="stylesheet" type="text/css">

    <link href="{{asset('assets/front/css/css/responsive.css')}}" rel="stylesheet" type="text/css">
    @stack('styles')
    <link href='//fonts.googleapis.com/css?family=Montserrat:thin,extra-light,light,100,200,300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('assets/admin/sweetalert.css')}}">
    <style>
        .btn-outline-light {
            color: #000;
        }

        .p-2.border.align-items-center {
            background: #fff!important;
        }

        span.text-uppercase {
            color: #000;
        }

        .bg-dark {
            background: #fff!important;
            color: #000;
        }

        .flex-fill {
            color: #000;
            font-size: 12px;
        }

        .btn.btn-rounded-success {
            background: #f7f0eb;
            border-radius: 5px;
            flex: 1 1 auto !important;
        }

        .fa-angle-left {
            color: #bbbbbb;
            font-size: 25px;
        }

        .fa-angle-right {
            color: #bbbbbb;
            font-size: 25px;
        }

        #calendar .border {
            border: 2px solid #fff!important;
            border-right: 4px solid #fff !important;
        }
    </style>
</head>
