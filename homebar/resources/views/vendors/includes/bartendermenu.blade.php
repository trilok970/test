<ul class="sub-menu-left pt-3">
    <p class="slecttype">Select Type </p>
    <li>
    <a <?php if(Request::is('vendor/bartenders/add-details')) { echo 'class="active"'; } ?> <?php if(Request::is('vendor/bartenders/edit/*')) { echo 'class="active"'; } ?> href="@if(isset($id)){{route('vendor.bartender.edit',$id)}} @else {{route('vendor.addbartenderdetails')}} @endif">bartender details</a>
    </li>
    <li>
        <a <?php if(Request::is('vendor/bartenders/videos/*') || Request::is('vendor/bartenders/*/add-videos') || Request::is('vendor/bartenders/*/videos/edit')) { echo 'class="active"'; } ?> <?php if(Request::is('vendor/bartender-video/edit/*')) { echo 'class="active"'; } ?>  href="@if(isset($id)) {{route('vendor.bartender.editvideo',$id)}} @else {{'javascript:void(0)'}}@endif">Videos</a>
    </li>
    <li>
        <a <?php if(Request::is('vendor/bartenders/schedule/*')||Request::is('vendor/bartenders/*/schedule')) { echo 'class="active"'; } ?> href="@if(isset($id)){{route('vendor.bartendercalendarschedule',$id)}} @else {{'javascript:void(0)' }}@endif"> schedule time</a>
    </li>
    @php
    if(isset($id))
        $user = App\Model\User::find($id)??'';
    @endphp
    @if(isset($user) && $user->options == 2)
    <li>
        <a <?php if(Request::is('vendor/bartenders/member-add')||Request::is('vendor/bartenders/*/add-member')) { echo 'class="active"'; } ?>  <?php if(Request::is('vendor/group-member/edit/*')) { echo 'class="active"'; } ?> href="@if(isset($id)){{route('vendor.bartenderaddmember',$id)}}@else{{'javascript:void(0)'}}@endif">Add group member</a>
    </li>
    <li>
        <a class="@if(Request::is('vendor/bartenders/*/group-member-list') || Request::is('vendor/bartenders/*/group-members/*')){{'active'}}@endif" href="{{route('vendor.groupmemberlist',$id)}}">Group member List</a>
    </li>
    @endif
</ul>
