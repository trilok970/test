@extends('layouts.vendor.vendor')
@section('content')
<div class="row pag-head ">
    <div class="col-lg-12 sub-btns col-sm-12 col-12">
        {{-- <a class="active" href="#">MIxed Drinks</a>
        <a href="#">Non-Alcoholic Drinks</a>
        <a href="#">Smoothies</a> --}}
    </div>
</div>

<div class="row  mix-cocktail">
    <div class="col-lg-6 pr0">
        <img class="img-fluid cocktail-img" width="100%" src="{{asset($item->image)}}">
    </div>

    <div class="col-lg-6 pl0">
        <div class="cocktailright">
            <h3>{{$item->title}}</h3>
            <p class="sprints">{{$type->name}} &nbsp; - {{$category->name}} </p>
            <p>{{$item->description}}
            </p>

            <div class="media mt-3">
                <img class="mr-2 " src="{{asset('images/video-icon.jpg')}}" alt="Generic placeholder image">
                <div class="media-body align-self-center">
                    <h5 class="mt-0">Video URL</h5>
                    <p><a href="{{$item->youtube_url}}" target="_blank">{{$item->youtube_url}}</a></p>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="row">
    <div class="col-lg-12">
        <h2 class="ingredients">Ingredients </h2>
        <div class="table-responsive">
            <table class="table border1px">
                <thead>
                    <tr>
                        <th style="width: 40%;" scope="col">ITEM</th>
                        <th scope="col">QUANTITY</th>
                        <th scope="col">IN STOCK</th>
                        <th scope="col">COST</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($item->ingredients as $ingredient)
                        <tr>
                            <th scope="row">{{$ingredient->item_name}}</th>
                            <td>
                                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Enter QTY">
                            </td>
                            <td>
                                <select class="selectpicker customselect form-control" id="number" data-container="body" data-live-search="true" title="yes" data-hide-disabled="true">
                                    <option>Yes</option>
                                    <option>No</option>
                                </select>
                            </td>
                            <td>
                                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                            </td>
                            <td>
                                <label class="checked">
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                              </label>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>

        <button class="tablebtn">update</button>
    </div>
</div>




</div>
</div>
</div>

@endsection
