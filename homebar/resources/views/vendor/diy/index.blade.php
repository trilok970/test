@extends('layouts.vendor.vendor')
@section('content')

<div class="row pag-head ">
    <div class="col-lg-12 sub-btns col-sm-12 col-12">
        @foreach ($categories as $cat)
            <a href="{{route('vendor.diy.index',[$type->slug,$cat->slug])}}" class="{{$cat->id == $category->id?'active':''}}">{{$cat->name}}</a>
        @endforeach
    </div>
</div>

@forelse ($items as $item)
    <div class="row bgw diy-username">
    <div class=" col-lg-6 col-sm-6 col-10 ">
        <div class="media proimg">
            <span class="mr-3"><img  src="{{asset($item->image)}}"></span>
            <div class="media-body">
                <h5 class="mt-0"><a href="{{route('vendor.diy.receipe',[$type->slug,$item->category->slug,$item->slug])}}">{{$item->title}}</a></h5>
                <p class="totalitem">Ingredients: <span>{{$item->ingredients->pluck('item_name')->join(", ")}}</span></p>
            </div>
        </div>

    </div>

    <div class=" txtr  col-lg-6 col-sm-6 col-2 unstyled centered">
        <label class="checked">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
        </label>
    </div>
</div>
@empty
    <div class="row bgw">
        <div class=" col-lg-6 col-sm-6 col-10 ">
            <p class="mt-3">No Item found in this category.</p>
        </div>
    </div>
@endforelse

@endsection
