@extends('layouts.vendor.vendor')
@section('content')

<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 ">
        <h2>Add Videos</h2>
    </div>
</div>


<div class="row mt-3">
    <div class=" col-lg-3 col-sm-4 col-12 pr0">

        @include('vendor.includes.bartendermenu')
        
    </div>

    <div class=" col-lg-9 col-sm-8 col-12 pl0 ">
        {!! Form::open(['route' => 'vendor.addvideo', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'bartendor-add-video','enctype' => 'multipart/form-data']) !!}
        <div class="add-video">
            <div class="text-add">ADD VIDEOS</div>
            <div class="row ">
                <div class="col-lg-12 col-sm-12 col-12" style="color: red; float: left; padding-bottom: 2%;">
                @if($errors->any())
                    {!! implode('', $errors->all('<div>:message</div>')) !!}
                @endif
            </div>
                <div class=" col-lg-6 col-sm-6 col-6 ">
                    <div class="video">
                        <input type="file" name="video[]" id="video" class="inputfile inputfile-4" multiple="" />
                        <label for="video"><figure> <img width="60" src="{{asset('assets/front/images/add-video.svg')}}"></figure> </label>
                    </div>
                    {!! $errors->first('video', '<p class="help-block" style="color: red; float: left;">:message</p>') !!}
                </div>

                <input type="hidden" name="bartender_id" id="bartender_id" value="<?php echo $id; ?>">

              
            </div>
            <button class="add mt-4">continue</button>
            {{ Form::close() }}
        </div>
    </div>

</div>

@endsection
