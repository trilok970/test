@extends('layouts.vendor.vendor')
@section('content')


<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 mb-3 ">
        <h2>My Earnings</h2>
    </div>
</div>


<div class="row my-earning">
    <div class="col-lg-12">
        <div class="totalearning">
            <div class="earning-rs">total earnings</div>
            <div class="earning-rs">$2000</div>
        </div>
    </div>
    <div class="col-lg-12">
        <h4>Today</h4>
        <div class="earningdiv">
            <div class="media proimg">
                <span class="mr-3"><img  src="{{asset('assets/front/images/pro1.jpg')}}"></span>
                <div class="media-body">
                    <h5 class="mt-0">Order Id : H321654987 </h5>
                    <p class="totalitem"> <span class="timespan">05:00 PM - 6:00 PM</span></p>
                </div>
            </div>

            <div class="earning-rs">$2000</div>

        </div>
        <div class="earningdiv">
            <div class="media proimg">
                <span class="mr-3"><img  src="{{asset('assets/front/images/pro1.jpg')}}"></span>
                <div class="media-body">
                    <h5 class="mt-0">Order Id : H321654987 </h5>
                    <p class="totalitem"> <span class="timespan">05:00 PM - 6:00 PM</span></p>
                </div>
            </div>

            <div class="earning-rs">$2000</div>

        </div>

        <div class="earningdiv">
            <div class="media proimg">
                <span class="mr-3"><img  src="{{asset('assets/front/images/pro1.jpg')}}"></span>
                <div class="media-body">
                    <h5 class="mt-0">Order Id : H321654987 </h5>
                    <p class="totalitem"> <span class="timespan">05:00 PM - 6:00 PM</span></p>
                </div>
            </div>

            <div class="earning-rs">$2000</div>

        </div>
    </div>
</div>


<div class="row my-earning">

    <div class="col-lg-12">
        <h4>4 May 2020</h4>
        <div class="earningdiv">
            <div class="media proimg">
                <span class="mr-3"><img  src="{{asset('assets/front/images/pro1.jpg')}}"></span>
                <div class="media-body">
                    <h5 class="mt-0">Order Id : H321654987 </h5>
                    <p class="totalitem"> <span class="timespan">05:00 PM - 6:00 PM</span></p>
                </div>
            </div>

            <div class="earning-rs">$2000</div>

        </div>
        <div class="earningdiv">
            <div class="media proimg">
                <span class="mr-3"><img  src="{{asset('assets/front/images/pro1.jpg')}}"></span>
                <div class="media-body">
                    <h5 class="mt-0">Order Id : H321654987 </h5>
                    <p class="totalitem"> <span class="timespan">05:00 PM - 6:00 PM</span></p>
                </div>
            </div>

            <div class="earning-rs">$2000</div>

        </div>

        <div class="earningdiv">
            <div class="media proimg">
                <span class="mr-3"><img  src="{{asset('assets/front/images/pro1.jpg')}}"></span>
                <div class="media-body">
                    <h5 class="mt-0">Order Id : H321654987 </h5>
                    <p class="totalitem"> <span class="timespan">05:00 PM - 6:00 PM</span></p>
                </div>
            </div>

            <div class="earning-rs">$2000</div>

        </div>
    </div>
</div>

@endsection
