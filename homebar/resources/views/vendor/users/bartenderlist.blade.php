@extends('layouts.vendor.vendor')
@section('content')

<div class="row mt-12">
    

    <div class="row pag-head">
        <div class="flex   col-lg-12 col-sm-12 col-12 ">

            <h2>Bartender List</h2>
            <a class="sendmsg " href="<?= route('vendor.addbartenderdetails') ?>" style="float: right;">Add bartender</a>
        </div>

        <!-- <div class="col-md-12">
            <hr class="hrline">
        </div> -->
    </div>
    <div class="col-md-12">
            <hr class="hrline">
    </div>
    <div class="container">
        <div class="row  pt-3 bartender-list">
            <?php foreach ($bartenderlist as $key => $value) { ?>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-img-top">
                        <?php if($value->photo) { ?> 
                        <img src="<?= $value->photo; ?>" alt="...">
                        <?php } else { ?> 
                        <img src="{{asset('assets/front/images/pro1.jpg')}}" alt="...">
                        <?php } ?>
                    </div>

                    <div class="card-body">
                        <h5 class="card-title"><?= $value->first_name .' '.$value->last_name; ?></h5>
                        <p class="card-text">Call : <?= $value->primary_mobile_number; ?></p>
                    </div>
                    <ul class="list-group list-group-flush ">
                        <li class="list-group-item">
                            Experience
                            <p><?= $value->experience; ?> Years</p>
                        </li>
                        <li class="list-group-item">
                            Signature Drink
                            <p><?= $value->signature_drink; ?></p>

                        </li>
                        <li class="list-group-item">
                            Email
                            <p><?= $value->email; ?></p>
                        </li>
                    </ul>
                    <div class="card-bottom">
                        <a href="{{ route('vendor.bartender.edit', $value->id) }}" class="card-link"><!-- <img src="{{asset('assets/front/images/edit.svg')}}"> --> Edit</a>
                        <a href="{{ route('vendor.bartender.destroy', $value->id) }}" class="card-link" onclick="return confirm('Are you sure you want to delete this member?');" ><!-- <img src="{{asset('assets/front/images/close.svg')}}"> -->Delete</a>
                    </div>
                </div>

            </div>
            <?php } ?>


        </div>
    </div>
</div>

@endsection
