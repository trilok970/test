@extends('layouts.vendor.vendor')
@section('content')

<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 ">
        <h2>Edit Group Member</h2>
    </div>
</div>


<div class="row mt-3">
    <div class=" col-lg-3 col-sm-4 col-12 pr0">

        @include('vendor.includes.bartendermenu')
        
    </div>
<?php

    // echo "<pre>";
    // print_r($user->options);
    // die;

?>

    <div class=" col-lg-9 col-sm-8 col-12 pl0 addmuenu-right ">
        
            @if(isset($user))
                {{ Form::model($user, ['route' => ['vendor.groupmember.update', $user->id], 'method' => 'post', 'files'=>'true']) }}
            @else
                {!! Form::open() !!}
            @endif


           <div class="form-group">
                <label for="name">Name</label>
                <?= Form::text('name', old('name'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter First Name', 'id' => 'inputname']) ?>
                {!! $errors->first('name', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div> 
            <br>
            <div class="form-group">
                <label for="country">Country</label>
                <?= Form::select('country_id', $countries, old('country_id') ,['class' => 'selectpicker customselect form-control', 'id' => 'country', 'data-container' => 'body', 'data-live-search' => 'true', 'title' => 'Select Country', 'data-hide-disabled' => 'true' ]); ?>
                {!! $errors->first('country_id', '<p class="help-block" style="color: red; float: left;">:message</p>') !!}
                
            </div>
            <br>
            <div class="form-group">
                <label for="inputMobileNumber">Mobile Number</label>
                <?= Form::text('mobile_number', old('mobile_number'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Mobile Number', 'id' => 'inputMobileNumber', 'min' => 0]) ?>
                {!! $errors->first('mobile_number', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="inputEmail">E-mail Address</label>
                    <?= Form::email('email', old('email'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Email', 'id' => 'inputEmail']) ?>
                    {!! $errors->first('email', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>

            <div class="form-group">
                <label for="experience"> Years of experience </label>
                <?= Form::text('experience', old('experience'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Years of experience', 'id' => 'experience']) ?>
                {!! $errors->first('experience', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>

            <div class="form-group">
                <label for="signature_drink">signature drink</label>
                <?= Form::text('signature_drink', old('signature_drink'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Signature drink', 'id' => 'signature_drink']) ?>
                {!! $errors->first('signature_drink', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>

            <input type="hidden" name="group_member_id" id="group_member_id" value="<?php echo $id; ?>">

            <div class="form-group col- pr-3">
                    <label for="inputPassword4">photo</label>
                    <div class="box">
                        <input type="file" name="photo" id="photo" class="inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple />
                        <label for="photo"><figure> <img width="40" src="{{asset('assets/front/images/user-add.svg')}}"></figure> </label>
                    </div>
                </div>


            
            <button class="add mt-4">Update Member</button>

        {{ Form::close() }}

    </div>
</div>

@endsection
