@extends('layouts.vendor.vendor')
@section('content')
<style type="text/css">
input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
</style>
<div class="container">
    
    <div class="row">
 
        <div class="profile-page">
   
            <div class="row pag-head">

                <div class="flex col-lg-12 col-sm-12 col-12 ">
                    <h2>My Profile</h2>
                    <a href="{!! route('vendor.profile') !!}" class="nowbtn">EDIT PROFILE </a>
                </div>

            </div>

            <div class="row ">
                <div class="col-lg-12">
                    {!! Form::open(['route'=>'vendor.cover.update','id'=>'coverUploadForm','files'=>true]) !!}
                    <div class="profileimg">
                        <?php if(!empty($user->cover_image)) { ?>
                            <img src="<?= URL::to('/').$user->cover_image; ?>">
                        <?php } else { ?>
                            <img  src="{{asset('assets/front/images/profile-img.jpg')}}" >
                        <?php } ?>

                        <label for="file" class="custom-file-upload nowbtn">
                             Upload Cover
                        </label>
                        <input id="file" type="file" name="cover" onchange="coverupdate();" />

                        <input type="hidden" name="user_id" id="user_id" value="<?= $user->id; ?>">

                        <div class="media profillogo">
                            <span class="align-self-center mr-2"> 
                                <?php if(!empty($user->logo_image)) { ?>
                                    <img src="<?= URL::to('/').$user->logo_image; ?>">
                                <?php } else { ?>
                                    <img src="{{asset('assets/front/images/left-logo.png')}}">
                                <?php } ?>
                            </span>
                            <div class="media-body align-self-center ">
                                <h5 class="mt-0"><?= $user->business_name; ?></h5>
                                <p>Store Manager : <?= $user->first_name; ?> <?= $user->last_name; ?> </p>


                            </div>
                        </div>

                    </div>
                    {{ Form::close() }}
                </div>
            </div>

            <div class="row mt30  orderid">
                <div class="col-lg-8 col-md-7 col-sm-12">
                    <div class="bg-w pt-4 pb-4 pdd15">
                        <div class="row">
                            <div class=" col-lg-3 col-sm-12 col-12 ">
                                <p class="email">Email Address</p>
                            </div>
                            <div class=" col-lg-9 col-sm-12 col-12 ">
                                <p class="myid"><?= $user->email; ?></p>
                            </div>
                            <hr>
                            <div class=" col-lg-3 col-sm-12 col-12 ">
                                <p class="email">Mobile Number</p>
                            </div>
                            <div class=" col-lg-9 col-sm-12 col-12 ">
                                <p class="myid"> <span class="pr-4">+<?= $user->primary_mobile_number; ?>,</span> +<?= $user->secondary_mobile_number; ?> </p>
                            </div>
                            <hr>
                            <div class=" col-lg-3 col-sm-12 col-12 ">
                                <p class="email">Address</p>
                            </div>
                            <div class=" col-lg-9 col-sm-12 col-12 ">
                                <p class="myid"><?= $user->address; ?>, <?= $user->city; ?>, <?= $user->state; ?>, <?= $user->zipcode; ?></p>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 col-md-5 col-sm-12">

                    <div class="rating">
                        <div class="media">
                            <img width="34" class="mr-2" src="{{asset('assets/front/images/star.svg')}}">
                            <div class="media-body">
                                <p class="mb-0">Total Reviews</p>
                                <h5 class="mt-0">4.0</h5>
                            </div>
                        </div>
                    </div>

                    <div class="offi-time">
                        <h5>Hours of Availablity</h5>
                        <p>Open Time : <?= $user->open_time; ?> <br>Closing Time : <?= $user->close_time; ?></p>
                    </div>
                </div>

            </div>


            <div class="row btns btnsbotton pdd12px">
                <div class="col-12 selectmenu ">
                    <h3 class="text-left mlh3">SELECTED MENUS</h3>
                </div>
                <div class="col-lg-3 col-sm-3 col-12 ">
                    <a href="<?= route('vendor.drinkaddmenupre') ?>">DRINKS</a>
                </div>
                <div class="col-lg-3 col-sm-3 col-12">
                    <a href="<?= route('vendor.foodaddmenupre') ?>">APPETIZER / FOOD</a>
                </div>
                <div class="col-lg-3 col-sm-3 col-12">
                    <a href="<?= route('vendor.ambainceaddmenu') ?>">AMBAINCE</a>
                </div>

                <div class="col-lg-3 col-sm-3 col-12">
                    <a class=" active1" href="<?= route('vendor.addbartenderdetails') ?>">BARTENDING</a>
                </div>
            </div>

        </div>
    </div>

</div>
@endsection
<script type="text/javascript">
   function coverupdate() {
        document.getElementById("coverUploadForm").submit();
   }
</script>