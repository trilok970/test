@extends('layouts.vendor.vendor')
@section('content')


<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 ">
        <h2>Add Menu Item</h2>
    </div>
</div>


<div class="row mt-3">
    <div class=" col-lg-3 col-sm-4 col-12 pr0">
        @include('vendor.includes.foodmenu')
        
    </div>

    <div class=" col-lg-9 col-sm-8 col-12 pl0 addmuenu-right ">
        <form class="mt-3">
            <div class="form-group">
                <label for="exampleFormControlInput1">categories </label>

                <select class="form-control selectpicker" placeholer="Spirit" id="exampleFormControlSelect1">
                <option>Fast Food</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </select>
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Sub categories</label>
                <select class="form-control selectpicker" placeholer="Spirit" id="exampleFormControlSelect1">
                <option>Pizzas </option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </select>
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect2">NaMe</label>
                <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Enter Name">
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1" placeholder="Enter Description ">description </label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="2"></textarea>

            </div>

            <div class="form-row">
                <div class="form-group col-lg-4 col-md-4 col-sm-12">
                    <label for="inputEmail4">Price</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Price">
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-12">
                    <label for="inputPassword4">tax <span>( in Percentage )</span></label>
                    <input type="text" class="form-control" id="inputPassword4" placeholder="Enter Tax">
                </div>

                <div class="form-group col-lg-4 col-md-4 col-sm-12">
                    <label for="inputPassword4">Additional charge</label>
                    <input type="text" class="form-control" id="inputPassword4" placeholder="Enter Amount">
                </div>
            </div>
            <div class="form-group  ">
                <label class="w-100" for="inputPassword4">Select option</label>
                <div class="form-check  form-check-inline">
                    <label class="checked checked2">
                        <input type="checkbox" >
                        <span class="checkmark"></span>
                      </label>
                    <label class="form-check-label" for="inlineCheckbox1">Pickup</label>

                </div>
                <div class="form-check form-check-inline">
                    <label class="checked checked2">
                        <input type="checkbox" checked="false">
                        <span class="checkmark"></span>
                      </label>
                    <label class="form-check-label" for="inlineCheckbox2">Delivery</label>
                </div>
            </div>

            <label for="inputPassword4">Image</label>
            <div class="box">
                <input type="file" name="file-5[]" id="file-5" class="inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple />
                <label for="file-5"><figure> <img src="{{asset('assets/front/images/img-icon.svg')}}"></figure> </label>
            </div>
            <button class="add mt-4">ADD Menu</button>

        </form>

    </div>

</div>

@endsection
