@extends('layouts.vendor.vendor')
@section('content')

<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 ">
        <h2>Add Bartender</h2>
    </div>
</div>


<div class="row mt-3">
    <div class=" col-lg-3 col-sm-4 col-12 pr0">

        @include('vendor.includes.bartendermenu')
        
    </div>

    <div class=" col-lg-9 col-sm-8 col-12 pl0 addmuenu-right ">
        {!! Form::open(['route' => 'vendor.addbartender', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'bartendor-add-form','enctype' => 'multipart/form-data']) !!}
            <div class="form-group">
                <label for="first_name">First Name</label>
                <?= Form::text('first_name', old('first_name'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter First Name', 'id' => 'inputfirst_name']) ?>
                {!! $errors->first('first_name', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>           
            <br>
            <div class="form-group">
                <label for="last_name">Last Name</label>
                <?= Form::text('last_name', old('last_name'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Last Name', 'id' => 'inputlast_name']) ?>
                {!! $errors->first('last_name', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="inputEmail">E-mail Address</label>
                    <?= Form::email('email', old('email'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Email', 'id' => 'inputEmail']) ?>
                    {!! $errors->first('email', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="inputPrimaryMobileNumber">Primary Mobile Number</label>
                <?= Form::text('primary_mobile_number', old('primary_mobile_number'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Primary Mobile Number', 'id' => 'inputPrimaryMobileNumber', 'min' => 0]) ?>
                {!! $errors->first('primary_mobile_number', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="inputSecondaryMobileNumber">Secondary Mobile Number</label>
                <?= Form::text('secondary_mobile_number', old('secondary_mobile_number'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Secondary Mobile Number', 'id' => 'inputSecondaryMobileNumber', 'min'=> 0]) ?>
                {!! $errors->first('secondary_mobile_number', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                  <label>DATE OF BIRTH:</label>

                  <div class="input-group">
                    <input type="text" class="form-control " name="birthday" id="reservation" placeholder="Enter Date of Birth">
                  </div>
            </div>
            <br>


            <div class="form-group">
                <label for="autocomplete">Address</label>
                <?= Form::textarea('address', old('address'), ['class' => 'form-control sightextarea', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Address', 'id' => 'autocomplete', 'rows' => 4, 'cols' => 40]) ?>
                {!! $errors->first('address', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="dl_no">Driver’s License/ID Number</label>
                <?= Form::text('dl_no', old('dl_no'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Driving License', 'id' => 'dl_no', 'min'=> 0]) ?>
                {!! $errors->first('dl_no', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="signature_drink">signature drink</label>
                <?= Form::text('signature_drink', old('signature_drink'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'signature drink', 'id' => 'signature_drink']) ?>
                {!! $errors->first('signature_drink', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="bio">BIO</label>
                <?= Form::textarea('bio', old('bio'), ['class' => 'form-control sightextarea', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Bio', 'id' => 'bio', 'rows' => 4, 'cols' => 40]) ?>
                {!! $errors->first('bio', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>

            <div class="form-group">
                <label for="experience"> Years of experience </label>
                <?= Form::text('experience', old('experience'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Years of experience', 'id' => 'experience']) ?>
                {!! $errors->first('experience', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>

            <div class="form-group">
                <label for="radius_of_service">radius of provides the service <small> ( in Miles ) </small></label>
                <?= Form::text('radius_of_service', old('radius_of_service'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Radius of service', 'id' => 'radius_of_service']) ?>
                {!! $errors->first('radius_of_service', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="price">Price <small> ( per Hour for one bartender ) </small></label>
                <?= Form::text('price', old('price'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Price ( per Hour for one bartender )', 'id' => 'price']) ?>
                {!! $errors->first('price', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="bartending_license_number">Bartending License Number <small>( optional)</small> </label>
        
                <?= Form::text('bartending_license_number', old('bartending_license_number'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Bartending License Number', 'id' => 'bartending_license_number']) ?>
                {!! $errors->first('bartending_license_number', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="bartending_license_expirations">Bartending License Expirations Date  <small>( optional)</small></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="bartending_license_expirations" id="bartending_license_expirations" placeholder="Bartending License Expirations Date" autocomplete="off">
                    
                  </div>
                  {!! $errors->first('bartending_license_expirations', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>


            <label for="inputPassword4">select option</label>
            <div class="custom-redio">
                <p>
                    <input type="radio" id="test1" name="option" value="1" checked>
                    <label for="test1">Individual</label>
                </p>
                <p>
                    <input type="radio" id="test2" name="option" value="2">
                    <label for="test2">Group</label>
                </p>

            </div>
            <br>

            <div class="form-row">
                <div class="form-group col- pr-3">
                    <label for="inputPassword4">photo</label>
                    <div class="box">
                        <input type="file" name="photo" id="photo" class="inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple />
                        <label for="photo"><figure> <img width="40" src="{{asset('assets/front/images/user-add.svg')}}"></figure> </label>
                    </div>
                </div>
                <!-- http://localhost/homebar/vendor/add-bartender-videos/31 -->
                <div class="form-group col- pr-3">
                    <label for="inputPassword4">Add Photo DL/ID</label>
                    <div class="box">
                        <input type="file" name="dl_photo" id="dl_photo" class="inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple />
                        <label for="dl_photo"><figure> <img width="40" src="{{asset('assets/front/images/dl.svg')}}"></figure> </label>
                    </div>
                </div>
                <div class="form-group col- ">
                    <label for="inputPassword4">Add Bartending License </label>
                    <div class="box">
                        <input type="file" name="bl_photo" id="bl_photo" class="inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple />
                        <label for="bl_photo"><figure> <img width="40" src="{{asset('assets/front/images/dl.svg')}}"></figure> </label>
                    </div>
                </div>
                
            </div>
            {!! $errors->first('photo', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            {!! $errors->first('dl_photo', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            {!! $errors->first('bl_photo', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            
            <br>
            <br>
            <div class="form-group">
                <label for="country">Country</label>
                <?= Form::select('country_id', $countries, old('country_id') ,['class' => 'selectpicker customselect form-control', 'id' => 'country', 'data-container' => 'body', 'data-live-search' => 'true', 'title' => 'Select Country', 'data-hide-disabled' => 'true' ]); ?>
                {!! $errors->first('country_id', '<p class="help-block" style="color: red; float: left;">:message</p>') !!}
                
            </div>
            <br>
            <div class="form-group">
                <label for="password">Password</label>
              <?= Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'id' => 'password']); ?>
              {!! $errors->first('password', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>
            <div class="form-group">
                <label for="password_confirmation">Confirm Password</label>
              <?= Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Retype Password', 'id' => 'password_confirmation']); ?>
              {!! $errors->first('password_confirmation', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>


            <button class="add mt-4">continue</button>

        {{ Form::close() }}

    </div>

</div>

@endsection
