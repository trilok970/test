@extends('layouts.vendor.vendor')

@section('content')

<div class="container">
    
    <div class="row">
 
        <div class="container signupfroms">

        @if (session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif


            {{ Form::model($user, ['route' => ['vendor.profile.update', $user->id], 'method' => 'post', 'files'=>'true']) }}

        
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="inputBussinessType">Business Type</label>
                    <?= Form::text('business_type',  old('business_type') , ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Business Type', 'id' => 'inputBussinessType']) ?>
                    {!! $errors->first('business_type', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Business Name</label>
                    <?= Form::text('business_name',  old('business_name') , ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Business Name', 'id' => 'inputBussinessName']) ?>
                    {!! $errors->first('business_name', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="first_name">First Name</label>
                    <?= Form::text('first_name', old('first_name'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter First Name', 'id' => 'inputfirst_name']) ?>
                    {!! $errors->first('first_name', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
                <div class="form-group col-md-3">
                    <label for="last_name">Last Name</label>
                    <?= Form::text('last_name', old('last_name'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Last Name', 'id' => 'inputlast_name']) ?>
                    {!! $errors->first('last_name', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Email Address</label>
                    <?= Form::email('email', old('email'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Email', 'id' => 'inputEmail']) ?>
                    {!! $errors->first('email', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
            </div>


            <div class="row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Primary Mobile Number</label>
                    <?= Form::text('primary_mobile_number', old('primary_mobile_number'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Primary Mobile Number', 'id' => 'inputPrimaryMobileNumber', 'min' => 0]) ?>
                    {!! $errors->first('primary_mobile_number', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Secondary Mobile Number</label>
                    <?= Form::text('secondary_mobile_number', old('secondary_mobile_number'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Secondary Mobile Number', 'id' => 'inputSecondaryMobileNumber', 'min'=> 0]) ?>
                    {!! $errors->first('secondary_mobile_number', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12">
                    <h4>Business Location</h4>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Address</label>
                    <?= Form::textarea('address', old('address'), ['class' => 'form-control sightextarea', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Address', 'id' => 'autocomplete2', 'rows' => 4, 'cols' => 40]) ?>
                    {!! $errors->first('address', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                    {!! Form::hidden('latitude', null, ['class'=>'form-control', 'id'=>'latitude']) !!}
                    {!! Form::hidden('longitude', null, ['class'=>'form-control', 'id'=>'longitude']) !!}
                </div>
                <div class="form-group col-md-6">
                    <div class="row">
                        <div class="form-group col-md-7">
                            <label for="inputCity">City</label>
                            <?= Form::text('city', old('city'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter City', 'id' => 'inputCity']) ?>
                            {!! $errors->first('city', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-5">
                            <label for="inputZipCode">Zip Code</label>
                            <?= Form::number('zipcode', old('zipcode'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Zip code', 'id' => 'inputZipCode', 'min'=> 0]) ?>
                            {!! $errors->first('zipcode', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-7">
                            <label for="inputState">State</label>
                            <?= Form::text('state', old('state'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter State', 'id' => 'inputState']) ?>
                            {!! $errors->first('state', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-5">
                            <label for="country">Country</label>
                            <?= Form::select('country_id', $countries, old('country_id') ,['class' => 'selectpicker customselect form-control', 'id' => 'country', 'data-container' => 'body', 'data-live-search' => 'true', 'title' => 'Select Country', 'data-hide-disabled' => 'true' ]); ?>
                            {!! $errors->first('country_id', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                            
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12 ">
                    <h4 class="mt-0">Hours of Availability</h4>
                </div>

                <div class="form-group col-md-3 clock">
                    <label for="inputOpenTime">Open Time</label>
                    <?= Form::text('open_time', old('open_time'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Open Time', 'id' => 'inputOpenTime']) ?>
                    
                    <i class="fa fa-clock-o " aria-hidden="true"></i>
                    {!! $errors->first('open_time', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
                <div class="form-group col-md-3 clock">
                    <label for="inputCloseTime">Close Time</label>
                    <?= Form::text('close_time', old('close_time'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Close Time', 'id' => 'inputCloseTime']) ?>
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    {!! $errors->first('close_time', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
                </div>
            </div>


            <?= Form::submit('Update', ['class' => 'btn sighn-up']) ?>
            
           
        {{ Form::close() }}
    </div>
    </div>

</div>
@endsection


