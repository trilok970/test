@extends('layouts.vendor.vendor')
@section('content')

<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 ">
        <h2>Add Schedule Time</h2>
    </div>
</div>
<?php
  // echo "<pre>";
  // print_r($user->availability);
  // die();
?>

<div class="row mt-3">
    <div class=" col-lg-3 col-sm-4 col-12 pr0">

        @include('vendor.includes.bartendermenu')
        
    </div>

    <div class=" col-lg-9 col-sm-8 col-12 pl0  claledra-page ">
        {!! Form::open(['route' => 'vendor.bartenderscheduleadd', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'bartendor-add-video','enctype' => 'multipart/form-data']) !!}
            <h5 class="calendarheading">schedule calendar</h5>
            <div id="calendar"></div>

            <div class="swithbtn">
                <div class="clockicon"><i class="fa fa-clock-o" aria-hidden="true"></i> turn off availability</div>
                <label class="switch">
                    <input type="checkbox" id="toggle" onchange="updateAvailability();" <?php if(isset($user->availability) && ($user->availability==1)) { echo 'checked=""'; } ?>  >
                    <span class="slider"></span>
                  </label>
            </div>

            <input type="hidden" name="bartender_id" value="<?php echo $id; ?>" id="bartender_id">

            <div class="d-t-head">Regular Times</div>

            <?php
                // echo "<pre>";
                // print_r($bartenderAvailability);
                // die();
                ?>

                <?php

                if (count($bartenderAvailability)>0) {
                    foreach ($bartenderAvailability as $key => $value) {
                ?>
                    <div class="form-row day-selct">
                        <div class="form-group col-lg-4 col-md-12 col-sm-12">
                            <select class="selectpicker customselect form-control" id="number" data-container="body" data-live-search="true" title="Select day" data-hide-disabled="true" disabled="">
                                <option value="<?php echo $value->weekday; ?>" selected>
                                    <?php
                                        if ($value->weekday==1) {
                                            echo 'Sunday';
                                        } elseif ($value->weekday==2) {
                                            echo 'Monday';
                                        } elseif ($value->weekday==3) {
                                            echo 'Tuesday';
                                        } elseif ($value->weekday==4) {
                                            echo 'Wednesday';
                                        } elseif ($value->weekday==5) {
                                            echo 'Thursday';
                                        } elseif ($value->weekday==6) {
                                            echo 'Friday';
                                        } else {
                                            echo 'Saturday';
                                        }
                                        
                                    ?>
                                </option>
                           </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-12 col-sm-12">
                            <input type="text" placeholder="Time From "  class="form-control " id="inputZip" value="<?php echo $value->time_from; ?>" disabled="">

                        </div>
                        <div class="form-group col-lg-4 col-md-12 col-sm-12">

                            <input type="text" placeholder="Time To" class="form-control " id="inputZip" value="<?php echo $value->time_to; ?>" disabled="">
                        </div>
                    </div>
                <?php   
                    }
                } 
                

            ?>

            <div class="form-row day-selct">
                <div class="form-group col-lg-4 col-md-12 col-sm-12">
                    <select class="selectpicker customselect form-control" id="number" data-container="body" data-live-search="true" title="Select day" data-hide-disabled="true" name="weekday[]">
                        <option value="1">Sunday</option>
                        <option value="2">Monday</option>
                        <option value="3">Tuesday</option>
                        <option value="4">Wednesday</option>
                        <option value="5">Thursday</option>
                        <option value="6">Friday</option>
                        <option value="7">Saturday</option>
                   </select>
                </div>
                <div class="form-group col-lg-4 col-md-12 col-sm-12">
                    <input type="text" placeholder="Time From " name="time_from[]" class="form-control timeFrom" id="inputZip">

                </div>
                <div class="form-group col-lg-4 col-md-12 col-sm-12">

                    <input type="text" placeholder="Time To" name="time_to[]" class="form-control timeTo" id="inputZip">
                </div>
            </div>
            <div class="form-row day-selct">
                <div class="form-group col-lg-4 col-md-12 col-sm-12">
                    <select class="selectpicker customselect form-control" id="number" data-container="body" data-live-search="true" title="Select day" data-hide-disabled="true" name="weekday[]">
                       <option value="1">Sunday</option>
                        <option value="2">Monday</option>
                        <option value="3">Tuesday</option>
                        <option value="4">Wednesday</option>
                        <option value="5">Thursday</option>
                        <option value="6">Friday</option>
                        <option value="7">Saturday</option>
                      
                   </select>
                </div>
                <div class="form-group col-lg-4 col-md-12 col-sm-12">
                    <input type="text" placeholder="Time From " name="time_from[]"  class="form-control timeFrom" id="inputZip">

                </div>
                <div class="form-group col-lg-4 col-md-12 col-sm-12">

                    <input type="text" placeholder="Time To" name="time_to[]" class="form-control timeTo" id="inputZip">
                </div>
            </div>
            <div class="form-row day-selct">
                <div class="form-group col-lg-4 col-md-12 col-sm-12">
                    <select class="selectpicker customselect form-control" id="number" data-container="body" data-live-search="true" title="Select day" data-hide-disabled="true"  name="weekday[]">
                       <option value="1">Sunday</option>
                        <option value="2">Monday</option>
                        <option value="3">Tuesday</option>
                        <option value="4">Wednesday</option>
                        <option value="5">Thursday</option>
                        <option value="6">Friday</option>
                        <option value="7">Saturday</option>
                      
                   </select>
                </div>
                <div class="form-group col-lg-4 col-md-12 col-sm-12">
                    <input type="text" placeholder="Time From " name="time_from[]"  class="form-control timeFrom" id="inputZip">

                </div>
                <div class="form-group col-lg-4 col-md-12 col-sm-12">

                    <input type="text" placeholder="Time To" name="time_to[]" class="form-control timeTo" id="inputZip">
                </div>
            </div>
            <div class="form-row day-selct">
                <div class="form-group col-lg-4 col-md-12 col-sm-12">
                    <select class="selectpicker customselect form-control" id="number" data-container="body" data-live-search="true" title="Select day" data-hide-disabled="true"  name="weekday[]">
                       <option value="1">Sunday</option>
                        <option value="2">Monday</option>
                        <option value="3">Tuesday</option>
                        <option value="4">Wednesday</option>
                        <option value="5">Thursday</option>
                        <option value="6">Friday</option>
                        <option value="7">Saturday</option>
                      
                   </select>
                </div>
                <div class="form-group col-lg-4 col-md-12 col-sm-12">
                    <input type="text" placeholder="Time From " name="time_from[]"  class="form-control timeFrom" id="inputZip">

                </div>
                <div class="form-group col-lg-4 col-md-12 col-sm-12">

                    <input type="text" placeholder="Time To" name="time_to[]" class="form-control timeTo" id="inputZip">
                </div>
            </div>
            <div class="form-row day-selct">
                <div class="form-group col-lg-4 col-md-12 col-sm-12">
                    <select class="selectpicker customselect form-control" id="number" data-container="body" data-live-search="true" title="Select day" data-hide-disabled="true" name="weekday[]">
                       <option value="1">Sunday</option>
                        <option value="2">Monday</option>
                        <option value="3">Tuesday</option>
                        <option value="4">Wednesday</option>
                        <option value="5">Thursday</option>
                        <option value="6">Friday</option>
                        <option value="7">Saturday</option>
                      
                   </select>
                </div>
                <div class="form-group col-lg-4 col-md-12 col-sm-12">
                    <input type="text" placeholder="Time From " name="time_from[]"  class="form-control timeFrom" id="inputZip">

                </div>
                <div class="form-group col-lg-4 col-md-12 col-sm-12">

                    <input type="text" placeholder="Time To" name="time_to[]" class="form-control timeTo" id="inputZip">
                </div>
            </div>
            <div class="form-row day-selct">
                <div class="form-group col-lg-4 col-md-12 col-sm-12">
                    <select class="selectpicker customselect form-control" id="number" data-container="body" data-live-search="true" title="Select day" data-hide-disabled="true"  name="weekday[]">
                       <option value="1">Sunday</option>
                        <option value="2">Monday</option>
                        <option value="3">Tuesday</option>
                        <option value="4">Wednesday</option>
                        <option value="5">Thursday</option>
                        <option value="6">Friday</option>
                        <option value="7">Saturday</option>
                      
                   </select>
                </div>
                <div class="form-group col-lg-4 col-md-12 col-sm-12">
                    <input type="text" placeholder="Time From " name="time_from[]"  class="form-control timeFrom" id="timeFrom">

                </div>
                <div class="form-group col-lg-4 col-md-12 col-sm-12">

                    <input type="text" placeholder="Time To" name="time_to[]" class="form-control timeTo" id="timeTo">
                </div>
            </div>
            <div class="form-row day-selct">
                <div class="form-group col-lg-4 col-md-12 col-sm-12">
                    <select class="selectpicker customselect form-control" id="number" data-container="body" data-live-search="true" title="Select day" data-hide-disabled="true" name="weekday[]">
                       <option value="1">Sunday</option>
                        <option value="2">Monday</option>
                        <option value="3">Tuesday</option>
                        <option value="4">Wednesday</option>
                        <option value="5">Thursday</option>
                        <option value="6">Friday</option>
                        <option value="7">Saturday</option>
                      
                   </select>
                </div>
                <div class="form-group col-lg-4 col-md-12 col-sm-12">
                    <input type="text" placeholder="Time From " name="time_from[]"  class="form-control timeFrom" id="timeFrom">

                </div>
                <div class="form-group col-lg-4 col-md-12 col-sm-12">

                    <input type="text" placeholder="Time To" name="time_to[]" class="form-control timeTo" id="timeTo">
                </div>
            </div>



            <!-- <div class="date-time">
                <span>18 May 2020</span>
                <span>   03 : 00 PM - 05 : 00 PM</span>
            </div> -->
            <button class="add mt-4">submit</button>

        {{ Form::close() }}

    </div>

</div>

@endsection


<script type="text/javascript">
  function updateAvailability() {
      var bartender_id = $('#bartender_id').val();
      if (bartender_id) {
        if($("#toggle").prop("checked") == true){
           var val = 1;
        } else {
           var val = 0;
        }
      
        jQuery.ajax({
          url: '{{route('vendor.bartender.updateavailability')}}',
          type: 'POST',
          data: {
          "_token": "{{ csrf_token() }}",
          "val": val,
          "bartender_id": bartender_id
          },
          success: function (response) {
            // alert(response);
            // $("#status_" + id).html(response);
          }
        }); 
      }
        
  }
</script>

