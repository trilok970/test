@extends('layouts.vendor.vendor')

@section('content')

<div class="container">
    
    <div class="row">
 
        <div class="container signupfroms">

        @if (session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif


    {!! Form::open(['route' => ['vendor.changePassword'], 'method' => 'post', 'files'=>'true']) !!}

    <div class="row">

        <div class="col-sm-4">
            <label for="inputEmail">Password :</label>
            <?=  Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'id' => 'inputEmail']); ?>
            {!! $errors->first('password', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
        </div>
        
        <div class="col-sm-4">
            <label for="inputName">Confirm Password :</label>
            <?=  Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm Password', 'id' => 'inputEmail']); ?>
            {!! $errors->first('password_confirmation', '<p class="help-block" style="color: red; float: left; font-weight: bold;">:message</p>') !!}
        </div>

        <div class="col-lg-12" style="margin: 1% 0;"></div>

        <div class="col-sm-2">
            <?= Form::submit('Update', ['class' => 'btn sighn-up']) ?>
        </div>

    </div>
    <!-- .row -->

    {{ Form::close() }}

            
    </div>
    </div>

</div>
@endsection

