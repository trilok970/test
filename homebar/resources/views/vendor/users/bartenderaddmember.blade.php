@extends('layouts.vendor.vendor')
@section('content')

<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 ">
        <h2>Add Bartender</h2>
    </div>
</div>


<div class="row mt-3">
    <div class=" col-lg-3 col-sm-4 col-12 pr0">

        @include('vendor.includes.bartendermenu')

        
    </div>

    <div class=" col-lg-9 col-sm-8 col-12 pl0 addmuenu-right ">
        {!! Form::open(['route' => 'vendor.bartendermemberadd', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'bartendor-member-add','enctype' => 'multipart/form-data']) !!}
           <div class="form-group">
                <label for="name">Name</label>
                <?= Form::text('name', old('name'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter First Name', 'id' => 'inputname']) ?>
                {!! $errors->first('name', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div> 
            <br>

            <div class="form-group">
                <label for="country">Country</label>
                <?= Form::select('country_id', $countries, old('country_id') ,['class' => 'selectpicker customselect form-control', 'id' => 'country', 'data-container' => 'body', 'data-live-search' => 'true', 'title' => 'Select Country', 'data-hide-disabled' => 'true' ]); ?>
                {!! $errors->first('country_id', '<p class="help-block" style="color: red; float: left;">:message</p>') !!}
                
            </div>
            <br>

            <div class="form-group">
                <label for="inputMobileNumber">Mobile Number</label>
                <?= Form::text('mobile_number', old('mobile_number'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Mobile Number', 'id' => 'inputMobileNumber', 'min' => 0]) ?>
                {!! $errors->first('mobile_number', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>

            <div class="form-group">
                <label for="inputEmail">E-mail Address</label>
                    <?= Form::email('email', old('email'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Email', 'id' => 'inputEmail']) ?>
                    {!! $errors->first('email', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>

            <div class="form-group">
                <label for="experience"> Years of experience </label>
                <?= Form::text('experience', old('experience'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Years of experience', 'id' => 'experience']) ?>
                {!! $errors->first('experience', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>

            <div class="form-group">
                <label for="signature_drink">signature drink</label>
                <?= Form::text('signature_drink', old('signature_drink'), ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Signature drink', 'id' => 'signature_drink']) ?>
                {!! $errors->first('signature_drink', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
            </div>
            <br>

            <input type="hidden" name="bartender_id" id="bartender_id" value="<?php echo $id; ?>">

            <div class="form-group col- pr-3">
                    <label for="inputPassword4">photo</label>
                    <div class="box">
                        <input type="file" name="photo" id="photo" class="inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple />
                        <label for="photo"><figure> <img width="40" src="{{asset('assets/front/images/user-add.svg')}}"></figure> </label>
                    </div>
                    {!! $errors->first('photo', '<p class="help-block" style="color: red; float: left; ">:message</p>') !!}
                </div>


            
            <button class="add mt-4">Add Member</button>

        {{ Form::close() }}

    </div>

</div>

@endsection
