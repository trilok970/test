<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="{{asset('assets/front/images/favicon.jpg')}}" type="image/jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> </title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!-- Custom -->
    <link rel="stylesheet" href="{{asset('assets/front/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/swiper.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/responsive-index.css')}}">

</head>

<body id="page-top">
    <!-- Navigation-->
    <div class="row fixed-top" id="dynamic">
        <div class="container-fluid  ">
            <div class="row mt-3">
                <div class="col-lg-6 col-5 ">
                    <img src="{{asset('assets/front/images/logo.png')}}">
                </div>
                <?php 
                if (!Session::has('VendorLoggedIn')) {
                  ?>
                
                <div class="col-lg-6 col-7 text-right">
                    <a href="{!! route('vendor.login') !!}" class="btn signin"> Sign in </a>
                    <a href="{!! route('vendor.signup') !!}" class="btn signup ml-2"> Sign Up </a>
                </div>
                <?php 
                } 
                ?>
            </div>
        </div>
    </div>




    <div class="sldier">

        <div class="overlay"></div>

        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">

            <!-- <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class=""></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1" class=""></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2" class="active"></li>
            </ol> -->
            <div class="carousel-inner">
                <div class="carousel-item">
                    <img class="d-block w-100" data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide" alt="First slide [800x400]" src="{{asset('assets/front/images/slider1.jpg')}}" data-holder-rendered="true">
                    <div class="carousel-caption ">

                        <h5>Take your Business Online Today</h5>
                        <h2>Register Bar shop owner</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable Cooking at its layout. </p>
                        <a href="#" class="sliderbtn">Download APP<i class="fa fa-angle-right" aria-hidden="true"></i> </a>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" data-src="holder.js/800x400?auto=yes&amp;bg=666&amp;fg=444&amp;text=Second slide" alt="Second slide [800x400]" src="{{asset('assets/front/images/bartend.jpeg')}}" data-holder-rendered="true">
                    <div class="carousel-caption  ">
                        <h5>Take your Business Online Today</h5>
                        <h2>Register Bar shop owner</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable Cooking at its layout. </p>
                        <a href="#" class="sliderbtn">Download APP<i class="fa fa-angle-right" aria-hidden="true"></i> </a>
                    </div>
                </div>
                <div class="carousel-item active">
                    <img class="d-block w-100" data-src="holder.js/800x400?auto=yes&amp;bg=555&amp;fg=333&amp;text=Third slide" alt="Third slide [800x400]" src="{{asset('assets/front/images/slider3.jpg')}}" data-holder-rendered="true">
                    <div class="carousel-caption ">
                        <h5>Take your Business Online Today</h5>
                        <h2>Register Bar shop owner</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable Cooking at its layout. </p>
                        <a href="#" class="sliderbtn">Download APP<i class="fa fa-angle-right" aria-hidden="true"></i> </a>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <section class="section1">
        <div class="container">
            <div class="row top-galary">
                <div class="col-md-6 left">
                    <p></p>
                    <p class="gold">Become a vendor to sell </p>
                    <h2>Established fact that a reader</h2>
                    <ul class="be-bendor">
                        <li> Appetizers</li>
                        <li> Pre-Made Drinks</li>
                        <li>Food recipe ingredients</li>
                        <li>Alcoholic and Non-alcoholic Drink ingredients </li>
                    </ul>
                    <a href="{!! route('vendor.signup') !!}" class="btn signup ml-2">
                    <button class="mangerbtn"> Register Now <img src="{{asset('assets/front/images/arrow.svg')}}"> </button>
                    </a>
                </div>
                <div class="col-md-6 right">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-4">
                            <img src="{{asset('assets/front/images/h1.jpg')}}">
                        </div>

                        <div class="col-md-4 col-sm-4 col-4">
                            <img src="{{asset('assets/front/images/h2.jpg')}}">
                        </div>

                        <div class="col-md-4 col-sm-4 col-4">
                            <img src="{{asset('assets/front/images/h3.jpg')}}">
                        </div>

                        <div class="col-md-4 col-sm-4 col-4">
                            <img src="{{asset('assets/front/images/h4.jpg')}}">
                        </div>

                        <div class="col-md-4 col-sm-4 col-4">
                            <img src="{{asset('assets/front/images/h5.jpg')}}">
                        </div>

                        <div class="col-md-4 col-sm-4 col-4">
                            <img src="{{asset('assets/front/images/h6.jpg')}}">
                        </div>

                    </div>
                </div>
            </div>
    </section>



    <section class="section2">
        <div class="container">
            <div class="row top-galary">

                <div class="col-md-6 right">
                    <img class="imgmob" src="{{asset('assets/front/images/mob.jpg')}}">
                </div>
                <div class="col-md-6 left">
                    <p class="gold">HEADING FOR USER</p>
                    <h2>Established fact that a reader</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
                    <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. </p>
                    <h6>Download on the</h6>
                    <button class="mangerbtn"> <i class="fa fa-apple" aria-hidden="true"></i> Apple Store  </button>
                    <button class="mangerbtn ml-3"> <img src="{{asset('assets/front/images/play-store.png')}}"> Play Store  </button>
                </div>
            </div>
    </section>


    <section class="section3">
        <div class="container">
            <div class="row top-galary">
                <div class="col-md-6 left">
                    <p class="gold">Register as a Bartender</p>
                    <h2>Established fact that a reader</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout</p>
                    <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. </p>
                    <h6>Download App for Become a Bartender</h6>
                    <button class="mangerbtn"> <i class="fa fa-apple" aria-hidden="true"></i> Apple Store  </button>
                    <button class="mangerbtn ml-3"> <img src="{{asset('assets/front/images/play-store.png')}}"> Play Store  </button>
                </div>
                <div class="col-md-5 right">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-6">
                            <div class="col-md-12 col-sm-12 col-12 mb50 ">
                                <img src="{{asset('assets/front/images/1.jpg')}}">
                            </div>

                            <div class="col-md-12 col-sm-12 col-12 mb50 mb-0">
                                <img src="{{asset('assets/front/images/bartend.jpeg')}}">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-6 df">
                            <img src="{{asset('assets/front/images/3.jpg')}}">
                        </div>
                    </div>
                </div>
            </div>
    </section>


    <section class="section4">
        <div class="container">
            <div class="row top-galary">
                <div class="col-md-6 left mt-5">
                    <p class="gold">Register as a Driver</p>
                    <h2>Established fact that a reader</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout</p>
                    <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. </p>
                    <h6>Download App for Become a Driver</h6>
                    <button class="mangerbtn"> <i class="fa fa-apple" aria-hidden="true"></i> Apple Store  </button>
                    <button class="mangerbtn ml-3"> <img src="{{asset('assets/front/images/play-store.png')}}"> Play Store  </button>
                </div>
                <div class="col-md-5 col-sm-6 right">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-6">
                            <div class="col-md-12 col-sm-12 col-12 mb50 ">
                                <img src="{{asset('assets/front/images/driver_red.jpeg')}}">
                            </div>

                            <div class="col-md-12 col-sm-12 col-12 mb50 mb-0">
                                <img src="{{asset('assets/front/images/ddd.jpeg')}}">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-6 df">
                            <img src="{{asset('assets/front/images/driver_blue.jpeg')}}">
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <section class="section5">
        <div class="container-fluid pl-0">
            <div class="row top-galary align-items-start">
                <!-- <div class="row top-galary"></div> -->
                <div class="col-md-6 right">
                    <p class="amb">AMBIANCE</p>
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-4">
                            <img src="{{asset('assets/front/images/b11.jpg')}}">
                        </div>

                        <div class="col-md-4 col-sm-4 col-4">
                            <img src="{{asset('assets/front/images/b22.jpg')}}">
                        </div>

                        <div class="col-md-4 col-sm-4 col-4">
                            <img src="{{asset('assets/front/images/b33.jpg')}}">
                        </div>

                        <div class="col-md-4 col-sm-4 col-4">
                            <img src="{{asset('assets/front/images/b44.jpg')}}">
                        </div>

                        <div class="col-md-4 col-sm-4 col-4">
                            <img src="{{asset('assets/front/images/b55.jpg')}}">
                        </div>

                        <div class="col-md-4 col-sm-4 col-4">
                            <img src="{{asset('assets/front/images/b66.jpg')}}">
                        </div>

                    </div>
                </div>
                <div class="col-md-6 mt65px left">
                    <p class="gold"> Register to Sell Ambiance and Decorations</p>
                    <h2>Established fact</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.

                    </p>
                    <p>
                        The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                    <a href="{!! route('vendor.signup') !!}" class="btn signup ml-2"><button class="mangerbtn"> Become a Vendor  <img src="{{asset('assets/front/images/arrow.svg')}}"> </button></a>
                </div>
            </div>
    </section>
    
    <footer class="footer py-4 ">
        <div class="container">
            <div class="row align-items-center ptf">
                <div class="col-lg-6 text-lg-left f-links">

                    <a class="mr-3" href="#!">About</a>
                    <a class="mr-3" href="#!"> Faq’s </a>
                    <a class="mr-3" href="#!"> Contact </a>
                    <a class="mr-3" href="#!"> Help Center </a>
                    <a class="mr-3" href="#!"> Support </a>
                    <a class="mr-3" href="#!"> Privacy Policy </a>

                </div>

                <div class="col-lg-6 my-3 my-lg-0 text-lg-right social">
                    <span>Connect with us</span>
                    <a class="btn  btn-social " href="#!"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a class="btn btn-social " href="#!"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
                    <a class="btn  btn-social " href="#!"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                </div>

            </div>
        </div>
        <div class="col-lg-12 text-lg-center f-bdr mt-5 copy">© Homebar.com 2020. All Rights Reserved.</div>
    </footer>




    <!--    go top-->
    <a href="#" id="gotop" data-aos="fade-in" style="display: inline-block;"> <i class="fa fa-angle-up"> </i> </a>

    <!-- Scripts -->
    <script src="{{asset('assets/front/js/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('assets/front/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
    <!-- aos animation -->
    <script src="{{asset('assets/front/js/aos.js')}}"></script>
    <!-- Owl Carousel -->
    <script src="{{asset('assets/front/js/owl.carousel.min.js')}}"></script>

    <script src="{{asset('assets/front/js/swiper.min.js')}}"></script>
    <script src="{{asset('assets/front/js/who.js')}}"></script>
    <script src="{{asset('assets/front/js/custom.js')}}"></script>

    <script src="{{asset('assets/front/js/bootstrap-select.js')}}"></script>
    <script>
        $(window).scroll(function() {
            if ($(this).scrollTop() > 50) {
                $('#dynamic').addClass('newClass');
            } else {
                $('#dynamic').removeClass('newClass');
            }
        });
    </script>

</body>

</html>