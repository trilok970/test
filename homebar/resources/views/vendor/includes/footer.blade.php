<footer class="footer finnr-pages py-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 text-lg-left f-links">

                    <a class="mr-3" href="#!">About</a>
                    <a class="mr-3" href="#!"> Faq’s </a>
                    <a class="mr-3" href="#!"> Contact </a>
                    <a class="mr-3" href="#!"> Help Center </a>
                    <a class="mr-3" href="#!"> Support </a>
                    <a class="mr-3" href="#!"> Privacy Policy </a>

                </div>

                <div class="col-lg-6 my-3 my-lg-0 text-lg-right social">
                    <span>Connect with us</span>
                    <a class="btn  btn-social " href="#!"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a class="btn btn-social " href="#!"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
                    <a class="btn  btn-social " href="#!"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                </div>

            </div>
        </div>

        <div class="col-lg-12 text-lg-center  copy">© Homebar.com 2020. All Rights Reserved.</div>
    </footer>
    <!--    go top-->
    <a href="#" id="gotop" data-aos="fade-in" style="display: inline-block;"> <i class="fa fa-angle-up"> </i> </a>