
<style type="text/css">
input[type="file"] {
    display: none;
}
.custom-file-upload2 {
    border: none;
    cursor: pointer;
}
</style>
<?php
$route = Route::currentRouteAction();
$explode_route = explode('\\', $route);
$controller_ation = end($explode_route);
$controller_ation_ex = explode('@', $controller_ation);
$controller = isset($controller_ation_ex[0]) ? $controller_ation_ex[0] : '';
$action = isset($controller_ation_ex[1]) ? $controller_ation_ex[1] : '';
$url_params = request()->route()->parameters;
$type = isset($url_params['type']) ? $url_params['type'] : '';
$user = Auth::user();
?>
<div class="left-side">
    {!! Form::open(['route'=>'vendor.logo.update','id'=>'logoUploadForm','files'=>true]) !!}
    <div class="info"><span>INFO</span>
        <label for="file2" class="custom-file-upload2 ">
             <img src="{{asset('assets/front/images/dots.svg')}}" title="Update Logo">
        </label>
        <input id="file2" type="file" name="logo" onchange="logoupdate();" />
    </div>

    <div class="left-logo">
        <?php if(!empty($user->logo_image)) { ?>
            <img src="<?=  URL::to('/'). $user->logo_image; ?>">
        <?php } else { ?>
            <img src="{{asset('assets/front/images/left-logo.png')}}">
        <?php } ?>
        <button class="nowbtn" disabled>Available Now <i class="fa fa-check-circle" aria-hidden="true"></i>
        </button>
    </div>
    <input type="hidden" name="user_id" id="user_id" value="<?= $user->id; ?>">
    {{ Form::close() }}

    <p class="ptext pb-2 mt-3">
        <b><?= $user->first_name; ?> <?= $user->last_name; ?></b>
    </p>
    <p class="ptext ">
        <span><?= $user->email; ?></span>
    </p>

    <p class="ptext pt-4">
        <span>Hours of Availability</span>
    </p>
    <p class="ptext pt-2">
        <b><?= date("g:i a", strtotime($user->open_time)); ?> - <?= date("g:i a", strtotime($user->close_time)); ?></b>
    </p>

    <ul>
        <li><a <?php if(Request::is('vendor/dashboard')) { echo 'class="active"'; } ?> href="<?= route('vendor.dashboard') ?>">Recent Orders</a></li>

        <li><a <?php if(Request::is('vendor/drink-processing')) { echo 'class="active"'; } ?> href="<?= route('vendor.drinkprocessing') ?>">Orders in Progress</a></li>

        <li><a <?php if(Request::is('vendor/drink-completed-order')) { echo 'class="active"'; } ?> href="<?= route('vendor.drinkcompletedorder') ?>">Complete Orders</a></li>

        <?php
            $active = ($controller == 'MenuItemsController') ? 'active' : '';
        ?>
        <li><a class="<?= $active ?>" href="<?= route('vendor.menuItem.list', $type) ?>">Menu Listing</a></li>

        <li><a <?php if(Request::is('vendor/my-profile')) { echo 'class="active"'; } ?> href="<?= route('vendor.myprofile') ?>">My Profile</a></li>

        <li><a <?php if(Request::is('vendor/bartender-list')) { echo 'class="active"'; } ?> href="<?= route('vendor.bartenderlist') ?>">Bartender List</a></li>

        <li><a <?php if(Request::is('vendor/bartender-calendar-schedule')) { echo 'class="active"'; } ?> href="<?= route('vendor.bartendercalendarschedule') ?>">My Schedule</a></li>

        <li><a <?php if(Request::is('vendor/change-password')) { echo 'class="active"'; } ?> href="<?= route('vendor.changePassword') ?>">Change Password</a></li>

        <li><a <?php if(Request::is('vendor/bartender-my-earning')) { echo 'class="active"'; } ?> href="<?= route('vendor.bartendermyearning') ?>">My Earnings</a></li>

        <li><a <?php if(Request::is('vendor/do-it-yourself/*')) { echo 'class="active"'; } ?> href="<?= route('vendor.diy.index',request()->route()->parameters?request()->route()->parameters['type']:'') ?>">Do it Yourself</a></li>

        <li><a href="<?= route('vendor.logout') ?>" >Log out</a></li>
    </ul>
</div>
<script type="text/javascript">
   function logoupdate() {
        document.getElementById("logoUploadForm").submit();
   }
</script>
