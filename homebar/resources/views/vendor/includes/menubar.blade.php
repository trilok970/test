<?php
    $url_params = request()->route()->parameters;
    $type = isset($url_params['type']) ? $url_params['type'] : '';
?>
<div class="row btns dash-btn">
    <?php

    foreach ($types as $key => $value) {
        $active = ($type == $value->slug)? 'active' : '';
    ?>
    <div class="col-lg-3 col-sm-3 col-12">
        <a class="<?= $active ?>" href="<?= route('vendor.orders', $value->slug) ?>"><?= $value->name; ?></a>
    </div>
    <?php
    }

    ?>

    <?php /* ?>
    <div class="col-lg-3 col-sm-3 col-12 ">
        <a <?php if(Request::is('vendor/drink-add-menu-pre')) { echo 'class="active"'; } ?> href="<?= route('vendor.drinkaddmenupre') ?>">Drinks</a>
    </div>
    <div class="col-lg-3 col-sm-3 col-12">
        <a <?php if(Request::is('vendor/food-add-menu-pre')) { echo 'class="active"'; } ?> href="<?= route('vendor.foodaddmenupre') ?>">APPETIZER / FOOD</a>
    </div>

    <div class="col-lg-3 col-sm-3 col-12">
        <a <?php if(Request::is('vendor/ambaince-add-menu')) { echo 'class="active"'; } ?> href="<?= route('vendor.ambainceaddmenu') ?>">AMBAINCE</a>
    </div>
    <div class="col-lg-3 col-sm-3 col-12 pdr-0">
        <a <?php if(Request::is('vendor/add-bartender-details')) { echo 'class="active"'; } ?> href="<?= route('vendor.addbartenderdetails') ?>">BARTENDING</a>
    </div>
    <?php */?>
</div>
