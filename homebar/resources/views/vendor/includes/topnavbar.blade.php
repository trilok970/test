<div class="container-fluid nav-bg">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="<?= route('vendor.dashboard') ?>"><img src="{{asset('assets/front/images/logo.png')}}"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav m-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">ABOUT US</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"> FAQ’S</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"> CONTACT US</a>
                    </li>

                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <p class="bel"><img src="{{asset('assets/front/images/bell.svg')}}">
                        <span class="noti">4</span>
                    </p>
                    <i><img class="i-img" src="{{asset('assets/front/images/i.png')}}"></i>

                </form>
            </div>
        </nav>
    </div>
