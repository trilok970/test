
@extends('layouts.vendor.vendor')
@section('content')

<div class="row pag-head">
    <div class="flex col-lg-12 col-sm-12 col-12 ">
        <h2><?= $title_page ?></h2>
        <a href="<?= route('vendor.menuItem.create', $type) ?>" class="nowbtn pr-5 pl-5"> ADD MENU </a>

    </div>
</div>

<div class="row list-page mt-3">
    <?php
    $default_image = Config::get('params.default_menu_item_image');
    $currency_sign = Config::get('params.currency_sign');
    foreach ($items as $key => $item) {
        $image = isset($item->menuItemImages[0]) ? asset($item->menuItemImages[0]->image) : $default_image;
    ?>
        <div class="col-12 col-md-6 col-lg-4">
            <div class="card">
                <img class="card-img-top" src="<?= $image ?>" alt="Card image cap">
                <div class="card-body">
                    <h4 class="card-title"><a href="#" title="View Product"><?= $item->name ?></a></h4>
                    <p class="card-text"><?= strShort($item->description) ?></p>
                    <h5><?= $currency_sign.$item->price ?></h5>
                    <div class="row border-top pt-1">
                        <div class="col">
                            <a href="<?= route('vendor.menuItem.edit', [$type, $item->id]) ?>" class="listbtn edit"><!-- <img width="12" src="{{asset('assets/front/images/edit.svg')}}"> --> Edit</a>
                        </div>
                        <div class="col">
                            <a href="<?= route('vendor.menuItem.delete', $item->id) ?>" class="listbtn delete" onclick="confirmDelete(event, this)"><!-- <img width="10" src="{{asset('assets/front/images/close.svg')}}"> --> Delete</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>
@endsection
