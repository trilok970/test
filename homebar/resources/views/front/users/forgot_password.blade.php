<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="{{asset('assets/front/images/favicon.jpg')}}" type="image/jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> </title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- Custom -->
    <link href="{{asset('assets/front/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/front/css/responsive.css')}}" rel="stylesheet" type="text/css">

    <link href='//fonts.googleapis.com/css?family=Montserrat:thin,extra-light,light,100,200,300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap-select.css')}}">
</head>

<body style="background: #ffffff;">
    <div class="container-fluid nav-bg navbg-2 ">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="{!! route('home') !!}"><img src="{{asset('assets/front/images/logo.png')}}"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav m-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">ABOUT US</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"> FAQ’S</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"> CONTACT US</a>
                    </li>

                </ul>
                <form class="form-inline my-2 my-lg-0 signupbtns">
                    <!-- <a href="sign-in-other.html" class="button-line" type="button" class="btn btn-primary">Sign in</a>
                    <a href="sign-up.html" class="button-fill" type="button" class="btn btn-secondary">Sign Up</a> -->
                    <a href="{!! route('vendor.login') !!}" class="button-line "> Sign in </a>
                    <a href="{!! route('vendor.signup') !!}" class="button-fill"> Sign Up </a>
                </form>
            </div>
        </nav>
    </div>

    <div class="banner signupbh">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <h3 class="text-center">Change Your Password</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container">


         {!! Form::open() !!}
            <div class="row justify-content-center text-center">

                <div class="form-group col-md-8 col-12 ">
                    <div class="forgotpass">
                            @if(Session::has('danger'))
                               <div class="alert alert-danger text-center alert-dismissible">
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                  <strong>Opps!</strong> {{ Session::get('danger') }}
                               </div>   
                            @endif

                            @if(Session::has('success'))
                               <div class="alert alert-success text-center alert-dismissible">
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                  {{ Session::get('success') }}
                               </div>   
                            @endif

                          @if ($errors->any())
                            <div class="alert absolte_alert alert-danger alert-dismissible" onClick= "this.remove();">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <p class="enter">Change your password here</p>
                        <div class="mailtext">Password</div>

                        <?= Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'id' => 'inputPassword','required'=>true]); ?>
                        <br>
                        <input type="hidden" name="token" value="<?php echo $_GET['token']; ?>" />

                        <div class="mailtext">Confirm Password</div>
                        <?= Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm Password', 'id' => 'inputPassword','required'=>true]); ?>

                        <br>
                            <?= Form::submit('Update', ['class' => 'forgot-submit']) ?>
                    </div>
                </div>
            </div>

        {{ Form::close() }}
    </div>

    @include('vendors.includes.footer')

    @include('vendors.includes.scripts')

</body>

</html>





