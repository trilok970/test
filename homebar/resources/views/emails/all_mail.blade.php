@extends('layouts.emails.email')

@section('content')
<div style="margin: 20px; line-height:1.5;">	
{!! $data['content'] !!}
</div>

@endsection