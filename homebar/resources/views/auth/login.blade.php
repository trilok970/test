
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="{{asset('assets/front/images/favicon.jpg')}}" type="image/jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vendor Login | Homebar</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- Custom -->
    <link href="{{asset('assets/front/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/front/css/responsive.css')}}" rel="stylesheet" type="text/css">
    <link href='//fonts.googleapis.com/css?family=Montserrat:thin,extra-light,light,100,200,300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap-select.css')}}">
</head>

<body style="background: #ffffff;">
    <section class="login-block">
        <div class="container login-container" style="max-width: 900px; background: #fff;">



            <div class="row">

                <div class="col-md-6 col-sm-12 col-12 login-sec">

                    <div class="container">

                    </div>

                    <div>
                        <div class="maxwidht-300">
                            <img width="145" src="{{asset('assets/front/images/login-logo.png')}}">
                            <h2 class="">Welcome to HomeBar Delivery</h2>
                            {!! Form::open(['route' => 'vendor.login', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'login-form','enctype' => 'multipart/form-data']) !!}
                                <div class="form-group">

                                    <?= Form::email('email', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Email', 'id' => 'inputEmail']) ?>
                                    {!! $errors->first('email', '<p class="help-block" style="color: red; float: left;">:message</p>') !!}
                                </div>
                                <div class="form-group position-relative">
                                    <?= Form::password('password', ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Password', 'id' => 'password']) ?>
                                    {!! $errors->first('password', '<p class="help-block" style="color: red; float: left;">:message</p>') !!}
                                    <span id_key="password" class="eyebtn fa fa-eye-slash">
                                        <!-- <i class="fa fa-eye-slash eye" aria-hidden="true"></i> -->
                                        <!-- <i class="fa fa-eye" aria-hidden="true"></i> -->
                                    </span>
                                </div>


                                <div class="form-check p-0 ">
                                    <p class="forgetlink"> <a href="{!! route('vendor.forgot') !!}">Forgot Password? </a></p>
                                    <?= Form::submit('Sign In', ['class' => 'btn btn-login']) ?>
                                </div>
                                <div class="copy-text">Don’t have an account? <a href="{!! route('vendor.signup') !!}"> Sign Up </a> </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-12 banner-sec">
                    <div class="row">
                        <div class="col-6 p-0"><img class="img-fluid" src="{{asset('assets/front/images/login1.jpg')}}"> </div>
                        <div class="col-6 p-0"> <img class="img-fluid" src="{{asset('assets/front/images/login2.jpg')}}"></div>
                        <div class="col-6 p-0"> <img class="img-fluid" src="{{asset('assets/front/images/login3.jpg')}}"></div>
                        <div class="col-6 p-0"><img class="img-fluid" src="{{asset('assets/front/images/login4.jpg')}}"> </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!--    go top-->
    <a href="#" id="gotop" data-aos="fade-in" style="display: inline-block;"> <i class="fa fa-angle-up"> </i> </a>

    <!-- Scripts -->
    <script src="{{asset('assets/front/js/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('assets/front/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>

    <!-- aos animation -->
    <script src="{{asset('assets/front/js/aos.js')}}"></script>
    <!-- Owl Carousel -->
    <script src="{{asset('assets/front/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/front/js/custom.js')}}"></script>
    <script src="{{asset('assets/front/js/bootstrap-select.js')}}"></script>

    <script src="{{asset('assets/admin/notify.min.js')}}"></script>
        <?php
        $msg_type = (session()->has('success') ? 'success' : ( session()->has('error') ? 'error' : ( session()->has('warning') ? 'warning' : '')));
        $message = '';
        if($msg_type) {
            $message = session()->get($msg_type);
        }
        ?>
        <script>
            var msg_type = "<?= $msg_type ?>";
            var message = "<?= $message ?>";
            if(msg_type) {
                $.notify(message, msg_type);
            }
        </script>

        <script type="text/javascript">
             $(".eyebtn").click(function(){
                // $(".fa-eye").toggleClass("fa-eye-slash");
                $(this).toggleClass("fa-eye fa-eye-slash");
                var id = $(this).attr('id_key');
                var input = $("#"+id).attr("type");
                // console.log("type >> "+input);

                if (input == "password") {
                    $("#"+id).attr("type", "text");
                } else {
                    $("#"+id).attr("type", "password");
                }
              });
        </script>

</body>

</html>
