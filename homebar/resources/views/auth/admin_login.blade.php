@extends('layouts.admin.login')
@section('content')
<?php
$site_title = $settings['site_title'];
$site_logo = $settings['site_logo'];
?>
<style type="text/css">
    .position-relative{
        position: relative;
    }
    .eyebtn {
        position: absolute;
    bottom: 7px;
    right: 7px;
    background: white;
    padding: 3px;
    cursor: pointer;
    font-size: 14px;
    color: #787878;
}
</style>
<div class="login-box">
    <div class="login-logo">
        <a href="javascript:void(0);"><img src='<?= asset($site_logo) ?>' style="max-width:200px;"></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        {!! Form::open() !!}
        <div class="form-group <?= $errors->has('email') ? 'has-error' : '' ?>">
            <?= Form::text('email', null, ['class' => 'form-control', 'maxlength' => '255', 'autofocus' => 'autofocus', 'placeholder' => 'Email', 'id' => 'inputEmail', 'required' => true]) ?>
            <span class="help-block"><?= $errors->has('email') ? $errors->first('email') : '' ?></span>
        </div>
        <div class="form-group position-relative <?= $errors->has('password') ? 'has-error' : '' ?>">
            <?= Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'id' => 'password', 'required' => true]); ?>
            <span class="help-block"><?= $errors->has('password') ? $errors->first('password') : '' ?></span>

            <span id_key="password" class="eyebtn fa fa-eye-slash">
              <!-- <i class="fa fa-eye" aria-hidden="true"></i> -->
              <!-- <i class="fa fa-eye-slash" aria-hidden="true"></i> -->
             </span>
        </div>

        <div class="form-group row">
            <div class="col-md-6 offset-md-4">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
        </div>
        <div class="row">

            <!-- /.col -->
            <div class="col-xs-4">
                <?= Form::submit('Log In', ['class' => 'btn btn-primary btn-block btn-flat']) ?>
            </div>
            <!-- /.col -->
        </div>
        {{ Form::close() }}


        <!-- /.social-auth-links -->

        <a href="<?= route('admin.forgot') ?>">I forgot my password</a><br>
    </div>
    <!-- /.login-box-body -->
</div>

@push('scripts')
<script type="text/javascript">
             $(".eyebtn").click(function(){
                // $(".fa-eye").toggleClass("fa-eye-slash");
                $(this).toggleClass("fa-eye fa-eye-slash");
                var id = $(this).attr('id_key');
                var input = $("#"+id).attr("type");
                // console.log("type >> "+input);

                if (input == "password") {
                    $("#"+id).attr("type", "text");
                } else {
                    $("#"+id).attr("type", "password");
                }
              });
        </script>

@endpush
@endsection
