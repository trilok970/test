<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" type="image/x-icon" href="{{url('front/images/favicon.ico')}}">
<!-- Style Css -->
<link rel="stylesheet" type="text/css" href="{{url('front/css/style.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/libs/select2/dist/css/select2.min.css')}}">
<title>Castillo Engineering @yield('title')</title>
<style>
.text-white
{
    color:#fff;
}
.error{
    color:red;
}

</style>
@stack('styles')
</head>

<body>

