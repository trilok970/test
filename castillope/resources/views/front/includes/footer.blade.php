<footer>
    <div class="copyright">
      <p class="text-center">&copy; Copyright Castillo Engineeering.com 2020</p>
    </div>
  </footer>
</div>









<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

  <!-- Scripts -->
  <script src="{{url('front/js/jquery.min.js')}}"></script>
  <!-- Bootstrap -->
  <script src="{{url('front/js/popper.min.js')}}"></script>
  <script src="{{url('front/js/bootstrap.min.js')}}"></script>

  <!-- Owl Carousel -->
  <script src="{{url('front/js/owl.carousel.min.js')}}"></script>
  <script src="{{url('front/js/custom.js')}}"></script>
  <script src="{{asset('assets/admin/js/loadingoverlay.min.js')}}"></script>
  <script src="{{asset('assets/admin/assets/libs/select2/dist/js/select2.min.js')}}"></script>
  <script src="{{asset('front/assets/bootbox/bootbox.min.js')}}"></script>

  <script>
    $(document)
        .ajaxStart(function () {
            // $('#AjaxLoader').show();
            $("body").LoadingOverlay("show");
        })
        .ajaxStop(function () {
            $("body").LoadingOverlay("hide");
            // $('#AjaxLoader').hide();
        });

        $.ajaxSetup({
                headers: {
                    'Access-Control-Allow-Headers': '*',
                    'Accept': 'xml'
                }
            });
</script>

<script>
    $(function() {
    $(".alert-success").fadeOut(5000);
    $(".alert-danger").fadeOut(5000);
    });

    $(document).ready(function(){
        $('.custom-select2').select2({
        // closeOnSelect: false
        tags: true
      });

    $('[data-toggle="tooltip"]').tooltip();

            // For quantity validation
     $(".numeric_feild").on("focus",function(event)
     {
        id=$(this).attr('id');
        var text = document.getElementById(id);
        text.onkeypress = text.onpaste = checkInput;
     });
    function checkInput(e)
    {
    var e = e || event;
    var char = e.type == 'keypress'
    ? String.fromCharCode(e.keyCode || e.which)
    : (e.clipboardData || window.clipboardData).getData('Text');
    if (/[^\d]/gi.test(char)) {
    return false;
    }
    }

    //For discount validation
    $(".numeric_feild_discount").keypress(function(event){
        return isNumber(event, this);
    });

     function isNumber(evt, element)
     {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (
                (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
                (charCode < 48 || charCode > 57))
                return false;

                return true;
      }
});
</script>
  @stack('scripts')

</body>

</html>
