<header id="myHeader">
    <nav class="container-fluid navbar navbar-expand-xl navbar-light align-items-stretch">
      <div class="navbar-brand"><a class="d-inline-block" href="{{url('/')}}"><img class="logo" src="{{url('front/images/header-logo.png')}}" /></a></div>

      <div class="header-inner d-flex">
        <div class="ml-auto order-xl-3 d-inline-flex align-items-center">
        @if(Auth::check())
          {{-- <form>
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text icon-search"></div>
              </div>
              <input type="text" class="form-control" id="" placeholder="Search Here">
            </div>
          </form> --}}
          @endif
          {{-- <a href="#"><span class="icon-right icon-bell"></span></a> --}}
          <div class="btn-group">
          @if(!Auth::check())
            <div class="buttons d-flex align-items-center">
              <a href="{{url('login')}}" class="btn {{ Request::segment(1)=='login' ? 'active':''}}">Login</a>
              <a href="{{url('signup')}}" class="btn {{ Request::segment(1)=='signup' ? 'active':''}}">Sign Up</a>
            </div>
          @endif
          @if(Auth::check())
            <div class="profile-btn dropdown-toggle d-flex align-items-center" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span><img src="{{url('front/images/profile-pic.jpg')}}" alt="" /></span>
            </div>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                <a href="#" class="dropdown-item" type="button">My Account</a>
                <a href="#" class="dropdown-item" type="button">Settings</a>
                <a href="{{url('logout')}}" class="dropdown-item" type="button">Logout</a>
            </div>
          @endif

          </div>
          <button class="navbar-toggler ml-3" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
        @if(Auth::check())
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav" id="menu-center">
              <li class="nav-item {{ request()->segment(1) == 'recent-projects' ? 'active':'' }}"><a href="{{url('recent-projects')}}">Recent Projects</a></li>
              <li class="nav-item {{ request()->segment(1) == 'create-new-project' ? 'active':'' }}"><a href="{{url('create-new-project')}}">Create New Project </a></li>
              <li class="nav-item {{ request()->segment(1) == 'block-design' ? 'active':'' }}"><a href="{{url('block-design-project')}}">Block Design </a></li>
              <li class="nav-item {{ request()->segment(1) == 'structural' ? 'active':'' }}"><a href="{{url('structural')}}">Structural </a></li>
              <li class="nav-item {{ request()->segment(1) == 'electrical' ? 'active':'' }}"><a href="{{url('electrical')}}">Electrical</a></li>
              <li class="nav-item {{ request()->segment(1) == 'modules' ? 'active':'' }}"><a href="{{url('modules')}}">PV Modules</a></li>
              <li class="nav-item {{ request()->segment(1) == 'inverters' ? 'active':'' }}"><a href="{{url('inverters')}}">Inverters</a></li>
              <li class="nav-item {{ request()->segment(1) == 'ahj' ? 'active':'' }}"><a href="{{url('ahj')}}">AHJ</a></li>
          </ul>
        </div>
        @endif

      </div>
    </nav>
  </header>

