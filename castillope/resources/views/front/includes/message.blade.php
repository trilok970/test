@if(session('error_message'))
    <p class="alert alert-danger">{{session('error_message')}}</p>
@endif
@if(session('message'))
    <p class="alert alert-success">{{session('message')}}</p>
@endif
