<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, viewport-fit=cover"/>
<meta name="description" content="Drag a link to reconnect it. Nodes have custom Adornments for selection, resizing, and rotating.  The Palette includes links."/>
<link rel="stylesheet" href="{{ asset('front/gostyle.css') }}"/>
<!-- Copyright 1998-2021 by Northwoods Software Corporation. -->
<title>Draggable Link</title>
<script src="{{ asset('front/js/go.js') }}"></script>

<script id="code">
    function init() {
      var $ = go.GraphObject.make;

      myDiagram =
        $(go.Diagram, "myDiagramDiv",
            {
              "undoManager.isEnabled": true,
              "ModelChanged": function(e) {     // just for demonstration purposes,
                if (e.isTransactionFinished) {  // show the model data in the page's TextArea
                  document.getElementById("mySavedModel").textContent = e.model.toJson();
                }
              }
            });

      function makePortTemplate(input) {
        var p =
          $(go.Panel,
            {
              margin: new go.Margin(2, 0),
              toolTip:
                $("ToolTip",
                  $(go.TextBlock,
                    { margin: 3 },
                    new go.Binding("text", "id"))
                )
            },
            new go.Binding("portId", "id"),
            $(go.Shape, { width: 8, height: 8 },
              new go.Binding("fill"))
          );
        if (input) {
          p.toLinkable = true;
          p.toSpot = go.Spot.Left;
        } else {
          p.fromLinkable = true;
          p.fromSpot = go.Spot.Right;
        }
        return p;
      }

      myDiagram.nodeTemplate =
        $(go.Node, "Table",
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          $(go.Panel, "Auto",
            { column: 1, stretch: go.GraphObject.Vertical, minSize: new go.Size(30, 30) },
            $(go.Shape, { fill: "white" },
              new go.Binding("fill", "color")),
            $(go.TextBlock,
              { margin: 8, editable: true },
              new go.Binding("text").makeTwoWay())
          ),
          $(go.Panel, "Vertical",
            { column: 0, itemTemplate: makePortTemplate(true) },
            new go.Binding("itemArray", "inputs")
          ),
          $(go.Panel, "Vertical",
            { column: 2, itemTemplate: makePortTemplate(false) },
            new go.Binding("itemArray", "outputs")
          )
        );

      myDiagram.linkTemplate =
        $(go.Link,
          {
            routing: go.Link.AvoidsNodes, corner: 6,
            relinkableFrom: true, relinkableTo: true,
            reshapable: true, resegmentable: true
          },
          $(go.Shape),
          $(go.Shape, { toArrow: "OpenTriangle" })
        );

      myDiagram.model = $(go.GraphLinksModel, {
        linkFromPortIdProperty: "fromPort",
        linkToPortIdProperty: "toPort",
        nodeDataArray: [
          {
            key: 1, text: "Alpha", color: "lightblue", loc: "0 0",
            inputs: [{ id: "A" }, { id: "B" }],
            outputs: [{ id: "W" }, { id: "X" }, { id: "Y" }, { id: "Z" }]
          },
          {
            key: 2, text: "Beta", color: "orange", loc: "150 0",
            inputs: [{ id: "A" }, { id: "B" }, { id: "C" }, { id: "D" }],
            outputs: [{ id: "W" }, { id: "X" }, { id: "Y" }, { id: "Z" }]
          }
        ],
        linkDataArray: [
        ]
        });
    }

    function findNextPortId(node) {
      let i = 0;
      const defaultport = node.port;
      while (node.findPort("P" + i) !== defaultport) i++;
      return "P" + i;
    }

    function addPort(input) {
      myDiagram.commit(diag => {
        diag.selection.each(n => {
          if (!(n instanceof go.Node)) return;
          const m = diag.model;
          m.addArrayItem(input ? n.data.inputs : n.data.outputs, { id: findNextPortId(n) });
        })
      });
    }

    function removePort(input) {
      myDiagram.commit(diag => {
        diag.selection.each(n => {
          if (!(n instanceof go.Node)) return;
          const m = diag.model;
          const arr = input ? n.data.inputs : n.data.outputs;
          if (arr.length > 0) m.removeArrayItem(arr, arr.length-1);
        })
      });
    }
    </script>
</head>
<body onload="init()">
    <div id="myDiagramDiv" style="border: solid 1px black; width:100%; height:400px"></div>
    <button onclick="addPort(true)">Add Input</button>
    <button onclick="addPort(false)">Add Output</button>
    <button onclick="removePort(true)">Remove Input</button>
    <button onclick="removePort(false)">Remove Output</button>
    <textarea id="mySavedModel" style="width:100%;height:250px"></textarea>
  </body>
</html>
