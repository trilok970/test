@extends('front.layouts.app')
@section('title',$data['page_title'])
@section('content')
@push('styles')
<link rel="stylesheet" href="{{ asset('front/css/board.css') }}" />
@endpush
<div class="body-section bg-gray structural-body-sec  h-auto">

    <!-- Recent Projects section -->
    <section class="recent-projects-sec structural-sec el_form bords_page">
        <div class="d-flex structural-sec-inner">
            <div class="structural-col w-100">
                <!-- Inner Banner -->
                <section class="inner-bnr board_page"></section>
                <div class="container-fluid">
                    <div class="title-sec">
                        <div class="row m-0">
                            {{-- <div class="col-lg-4 col-md-6 mb-md-0 mb-5 p-0 bords_cate">

                                <div class="structural-details-col  electrical_page_top  bg-transparent">
                                    <div class="row" id="drag-items">
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b1.png')}}" alt="" id="pv_module"   draggble="true">
                                                    <h4 >PV MODULE</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b2.png')}}" alt="" id="micro_inverter"  draggble="true">
                                                    <h4>MICRO INVERTER</h4>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b3.png')}}" alt="" id="solar_load_center"  draggble="true">
                                                    <h4>SOLAR LOAD CENTER</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box active"  >
                                                    <img src="{{ asset('front/images/b4.png')}}" alt="" id="non_fused_disconnect"  draggble="true">
                                                    <h4>NON FUSED DISCONNECT</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b5.png')}}" alt="" id="tesla_gateway"  draggble="true">
                                                    <h4>TESLA GATEWAY</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b6.png')}}" alt="" id="fused_disconnect"  draggble="true">
                                                    <h4>FUSED DISCONNECT</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b7.png')}}" alt="" id="solardeck"  draggble="true">
                                                    <h4>SOLADECK</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b8.png')}}" alt="" id="battery"  draggble="true">
                                                    <h4>BATTERY</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b9.png')}}" alt="" id="meter"  draggble="true">
                                                    <h4>METER</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b10.png')}}" alt="" id="monitoring"  draggble="true">
                                                    <h4>MONITORING</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b11.png')}}" alt="" id="ats"  draggble="true">
                                                    <h4>ATS</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b12.png')}}" alt="" id="junction_box"  draggble="true">
                                                    <h4>JUNCTION BOX</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b13.png')}}" alt="" id="optimizer"  draggble="true">
                                                    <h4>OPTIMIZR</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b14.png')}}" alt="" id="transformer"  draggble="true">
                                                    <h4>TRANSFORMER</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b15.png')}}" alt="" id="dc_inverter"  draggble="true">
                                                    <h4>DC INVERTER</h4>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div> --}}

                            {{-- <div class="col-lg-8 col-md-6">
                                <div class="inventer_properties_sec board_circuit mt-0">
                                    <h2 class="c_text">Baord</h2>
                                    <div  class="structural-details-col" >

                                        <div id="container"></div>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="col-lg-12 col-md-12 mb-md-0 mb-5 p-0 bords_cate">

                                <div id="sample">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="b_left">
                                                <span class="d"></span>
                                                <div id="palette"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="b_left">
                                                <span class="d d2"></span>
                                                <div id="myDiagramDiv" style="flex-grow: 1; height: 450px; "></div>
                                                <span style="display: inline-block; vertical-align: top; width:150px">
                                                    <div id="myInspector">
                                                    </div>
                                                </span>
                                            </div>

                                            <div class="b_form" id="myForm">
                                                <form action="/action_page.php" class="form-container">
                                                    <div class="form-group">
                                                        <label for="input_port"><b>Input</b></label>
                                                        <!-- <input type="text" placeholder="Enter Input Port" name="input_port" id="input_port"> -->
                                                        <select id="input_port" onchange="addInput(true,this.value)"></select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="output_port"><b>Output</b></label>
                                                        <select id="output_port" onchange="addInput(false,this.value)"></select>
                                                        <!-- <input type="text" placeholder="Enter Output Port" name="output_port" id="output_port" > -->
                                                    </div>
                                                </form>
                                                <button class="btn btn2" onclick="generateImages()">Pdf Download</button>
                                                <textarea id="mySavedModel" style="width:100%;height:200px;display:none;">


                                                { "class": "go.GraphLinksModel",
                                                "copiesArrays": true,
                                                "copiesArrayObjects": true,
                                                "linkFromPortIdProperty": "fromPort",
                                                "linkToPortIdProperty": "toPort",
                                                "nodeDataArray": [

                                                ],
                                                "linkDataArray": [

                                                ]}

                                                </textarea>
                                            </div>


                                        </div>

                                    </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>


    </div>
</div>



@push('scripts')


<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{ asset('front/js/nine.js') }}"></script>
<script src="{{ asset('front/js/nine-figures.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script>
    function openForm() {
        document.getElementById("myForm").style.display = "block";
        $(".overlay").show();
    }
    function closeForm() {
        document.getElementById("myForm").style.display = "none";
        $(".overlay").hide();
    }
    function addInput(input,val) {
        removeAllPort(input);
        for(var i =1; i <= val; i++) {
            addPort(input);
        }
    }
</script>
<script id="code">
    function  generateImages() {
        save();
        load();
        var diagram = document.getElementById('myDiagramDiv');
        width = 900;
        height = 700;
        if (isNaN(width)) width = 100;
        if (isNaN(height)) height = 100;
        // Give a minimum size of 50x50
        width = Math.max(width, 50);
        height = Math.max(height, 50);
        var imgDiv = document.getElementById('myImages');
        console.log(imgDiv)
        // imgDiv.innerHTML = ‘’; // clear out the old images, if any
        var db = this.myDiagram.documentBounds.copy();
        var boundswidth = db.width;
        var boundsheight = db.height;
        var imgWidth = width;
        var imgHeight = height;
        var p = db.position.copy();
        //making images
        for (var i = 0; i< boundsheight; i += imgHeight) {
            var img;
            for (var j = 0; j < boundswidth; j += imgWidth) {
                img= this.myDiagram.makeImage({
                    scale: 1,
                    type: "image/jpeg",
                    background: "white",
                    position: new go.Point(p.x + j, p.y + i),
                    size: new go.Size(imgWidth, imgHeight)
                });
            }
        }
        var doc = new jsPDF();
        doc.addImage(img.src, 'JPEG', 15, 40, 240, 180);
        //if you need more page use addPage();
        // doc.addPage();
        doc.save('diagram.pdf');
    }

    var red = "orangered";  // 0 or false
    var green = "forestgreen";  // 1 or true

    function init() {
      var $ = go.GraphObject.make; 
      myDiagram =
        $(go.Diagram, "myDiagramDiv",
        {
            "draggingTool.isGridSnapEnabled": true, 
            "undoManager.isEnabled": true
        });

        
        myDiagram.addDiagramListener("Modified", function(e) {
            var button = document.getElementById("saveModel");
            if (button) button.disabled = !myDiagram.isModified;
            var idx = document.title.indexOf("*");
            if (myDiagram.isModified) {
              if (idx < 0) document.title += "*";
            }else{
                if (idx >= 0) document.title = document.title.substr(0, idx);
            }
        });

      var palette = new go.Palette("palette");
      myDiagram.linkTemplate =
      $(go.Link,
      {
        routing: go.Link.AvoidsNodes,
        curve: go.Link.JumpOver,
        corner: 3,
        relinkableFrom: true, relinkableTo: true,
            selectionAdorned: false, // Links are not adorned when selected so that their color remains visible.
            shadowOffset: new go.Point(0, 0), shadowBlur: 5, shadowColor: "blue",
        },
        new go.Binding("isShadowed", "isSelected").ofObject(),
        $(go.Shape,
            { name: "SHAPE", strokeWidth: 2, stroke: red }));

      // node template helpers
      var sharedToolTip =
      $("ToolTip",
          { "Border.figure": "RoundedRectangle" },
          $(go.TextBlock, { margin: 2 },
            new go.Binding("text", "", function(d) { return d.category; })));

      // define some common property settings
        function nodeStyle() {
            return [new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            new go.Binding("isShadowed", "isSelected").ofObject(),
            {
              selectionAdorned: false,
              shadowOffset: new go.Point(0, 0),
              shadowBlur: 15,
              shadowColor: "blue",
              toolTip: sharedToolTip
          }];
        }

         function shapeStyle() {
            return {
              name: "NODESHAPE",
              fill: "lightgray",
              stroke: "darkslategray",
              desiredSize: new go.Size(40, 40),
              strokeWidth: 2
          };
         }

         function portStyle(input) {
             return {
               desiredSize: new go.Size(6, 6),
               fill: "black",
               fromSpot: go.Spot.Right,
               fromLinkable: !input,
               toSpot: go.Spot.Left,
               toLinkable: input,
               toMaxLinks: 1,
               cursor: "pointer"
           };
         }

         function makePortTemplate(input) {
             var p =
             $(go.Panel,
             {
               margin: new go.Margin(2, 0),
               toolTip:
               $("ToolTip",
                   $(go.TextBlock,
                     { margin: 3 },
                     new go.Binding("text", "id"))
                   )
           },
           new go.Binding("portId", "id"),
           $(go.Shape, { width: 6, height: 6 },
               new go.Binding("fill"))
           );
             if (input) {
               p.toLinkable = true;
               p.toSpot = go.Spot.Left;
               p.margin = new go.Margin(5, 90, 0, 0);
           } else {
               p.fromLinkable = true;
               p.fromSpot = go.Spot.Right;

           }
           return p;
         }



        var pvModuleTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
          $(go.Panel, "Table",
            $(go.Panel, "Table",
                new go.Binding("itemArray", "inputPorts"),
                {
                    margin: new go.Margin(4, 0),
                    alignment: go.Spot.Left,
                    stretch: go.GraphObject.Vertical,
                    rowSizing: go.RowColumnDefinition.ProportionalExtra,
                    itemTemplate:
                    $(go.Panel, "TableRow",
                        $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                        )
                }
                ),
            $(go.Panel, "Vertical",
                { column: 0, itemTemplate: makePortTemplate(true) },
                new go.Binding("itemArray", "inputs")
                ),
            $(go.Panel, "Vertical",
                { column: 1, itemTemplate: makePortTemplate(false) },
                new go.Binding("itemArray", "outputs")
                ),
            $(go.Picture,
                { margin: 10,width: 80, height: 100, background: "white" },
                new go.Binding("source")),

            ),

          );

        // For micro invertor
        var microInverterTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
          $(go.Panel, "Table",
            $(go.Panel, "Table",
                new go.Binding("itemArray", "inputPorts"),
                {
                    margin: new go.Margin(4, 0),
                    alignment: go.Spot.Left,
                    stretch: go.GraphObject.Vertical,
                    rowSizing: go.RowColumnDefinition.ProportionalExtra,
                    itemTemplate:
                    $(go.Panel, "TableRow",
                        $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                        )
                }
                ),
            $(go.Panel, "Vertical",
                { column: 0, itemTemplate: makePortTemplate(true) },
                new go.Binding("itemArray", "inputs")
                ),
            $(go.Panel, "Vertical",
                { column: 1, itemTemplate: makePortTemplate(false) },
                new go.Binding("itemArray", "outputs")
                ),
            $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
            /*$(go.TextBlock,
                "Micro Invertor",
                { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),*/
            ),
          );
        // Solar load center template
        var solarLoadCenterTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
          $(go.Panel, "Table",
            $(go.Panel, "Table",
                new go.Binding("itemArray", "inputPorts"),
                {
                    margin: new go.Margin(4, 0),
                    alignment: go.Spot.Left,
                    stretch: go.GraphObject.Vertical,
                    rowSizing: go.RowColumnDefinition.ProportionalExtra,
                    itemTemplate:
                    $(go.Panel, "TableRow",
                        $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                        )
                }
                ),
            $(go.Panel, "Vertical",
                { column: 0, itemTemplate: makePortTemplate(true) },
                new go.Binding("itemArray", "inputs")
                ),
            $(go.Panel, "Vertical",
                { column: 1, itemTemplate: makePortTemplate(false) },
                new go.Binding("itemArray", "outputs")
                ),
            $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
            /*$(go.TextBlock,
                "Solar Load Center",
                { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),*/
            ),
          );

       // For Non fused disconnect Template
       var nonFusedDisconnectTemplate =
       $(go.Node, "Spot", nodeStyle(),
          { background: "#605641" },
          $(go.Panel, "Table",
            $(go.Panel, "Table",
                new go.Binding("itemArray", "inputPorts"),
                {
                    margin: new go.Margin(4, 0),
                    alignment: go.Spot.Left,
                    stretch: go.GraphObject.Vertical,
                    rowSizing: go.RowColumnDefinition.ProportionalExtra,
                    itemTemplate:
                    $(go.Panel, "TableRow",
                        $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                        )
                }
                ),
            $(go.Panel, "Vertical",
                { column: 0, itemTemplate: makePortTemplate(true) },
                new go.Binding("itemArray", "inputs")
                ),
            $(go.Panel, "Vertical",
                { column: 1, itemTemplate: makePortTemplate(false) },
                new go.Binding("itemArray", "outputs")
                ),
            $(go.Picture,
                { margin: 10, width: 80, height: 100},
                new go.Binding("source")),
            /*$(go.TextBlock,
                "Non fused Disconnect",
                { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),*/
            ),
          );
        // For tesla gateway
        var teslaGatewayTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
          $(go.Panel, "Table",
            $(go.Panel, "Table",
                new go.Binding("itemArray", "inputPorts"),
                {
                    margin: new go.Margin(4, 0),
                    alignment: go.Spot.Left,
                    stretch: go.GraphObject.Vertical,
                    rowSizing: go.RowColumnDefinition.ProportionalExtra,
                    itemTemplate:
                    $(go.Panel, "TableRow",
                        $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                        )
                }
                ),
            $(go.Panel, "Vertical",
                { column: 0, itemTemplate: makePortTemplate(true) },
                new go.Binding("itemArray", "inputs")
                ),
            $(go.Panel, "Vertical",
                { column: 1, itemTemplate: makePortTemplate(false) },
                new go.Binding("itemArray", "outputs")
                ),
            $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
            /*$(go.TextBlock,
                "Tesla Gateway",
                { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),*/
            ),
          );

        // For fused gateway
        var fusedDisconnectTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
          $(go.Panel, "Table",
            $(go.Panel, "Table",
                new go.Binding("itemArray", "inputPorts"),
                {
                    margin: new go.Margin(4, 0),
                    alignment: go.Spot.Left,
                    stretch: go.GraphObject.Vertical,
                    rowSizing: go.RowColumnDefinition.ProportionalExtra,
                    itemTemplate:
                    $(go.Panel, "TableRow",
                        $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                        )
                }
                ),
            $(go.Panel, "Vertical",
                { column: 0, itemTemplate: makePortTemplate(true) },
                new go.Binding("itemArray", "inputs")
                ),
            $(go.Panel, "Vertical",
                { column: 1, itemTemplate: makePortTemplate(false) },
                new go.Binding("itemArray", "outputs")
                ),
            $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
            /*$(go.TextBlock,
                "Fused Disconnect",
                { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),*/
            ),

          );
    // For Solardeck Template
    var soladeckTemplate =
    $(go.Node, "Spot", nodeStyle(),
      { background: "#fff" },
      $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                    )
            }
            ),
        $(go.Panel, "Vertical",
            { column: 0, itemTemplate: makePortTemplate(true) },
            new go.Binding("itemArray", "inputs")
            ),
        $(go.Panel, "Vertical",
            { column: 1, itemTemplate: makePortTemplate(false) },
            new go.Binding("itemArray", "outputs")
            ),
        $(go.Picture,
            { margin: 10, width: 80, height: 100, background: "white" },
            new go.Binding("source")),
        /*$(go.TextBlock,
            "Soladeck",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
            new go.Binding("text", "name")),*/
        ),

      );
    // For battery
    var batteryTemplate =
    $(go.Node, "Spot", nodeStyle(),
      { background: "#fff" },
      $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                    )
            }
            ),
        $(go.Panel, "Vertical",
            { column: 0, itemTemplate: makePortTemplate(true) },
            new go.Binding("itemArray", "inputs")
            ),
        $(go.Panel, "Vertical",
            { column: 1, itemTemplate: makePortTemplate(false) },
            new go.Binding("itemArray", "outputs")
            ),
        $(go.Picture,
            { margin: 10, width: 80, height: 100, background: "white" },
            new go.Binding("source")),
        /*$(go.TextBlock,
            "Battery",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
            new go.Binding("text", "name")),*/
        ),

      );
     // For meter template
     var meterTemplate =
     $(go.Node, "Spot", nodeStyle(),
      { background: "#fff" },
      $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                    )
            }
            ),
        $(go.Panel, "Vertical",
            { column: 0, itemTemplate: makePortTemplate(true) },
            new go.Binding("itemArray", "inputs")
            ),
        $(go.Panel, "Vertical",
            { column: 1, itemTemplate: makePortTemplate(false) },
            new go.Binding("itemArray", "outputs")
            ),
        $(go.Picture,
            { margin: 10, width: 80, height: 100, background: "white" },
            new go.Binding("source")),
        /*$(go.TextBlock,
            "Meter",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
            new go.Binding("text", "name")),*/
        ),

      );
    // For monitoring template
    var monitoringTemplate =
    $(go.Node, "Spot", nodeStyle(),
      { background: "#fff" },
      $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                    )
            }
            ),
        $(go.Panel, "Vertical",
            { column: 0, itemTemplate: makePortTemplate(true) },
            new go.Binding("itemArray", "inputs")
            ),
        $(go.Panel, "Vertical",
            { column: 1, itemTemplate: makePortTemplate(false) },
            new go.Binding("itemArray", "outputs")
            ),
        $(go.Picture,
            { margin: 10, width: 80, height: 100, background: "white" },
            new go.Binding("source")),
        /*$(go.TextBlock,
            "Monitoring",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
            new go.Binding("text", "name")),*/
        ),

      );
        // Ats template
        var atsTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
          $(go.Panel, "Table",
            $(go.Panel, "Table",
                new go.Binding("itemArray", "inputPorts"),
                {
                    margin: new go.Margin(4, 0),
                    alignment: go.Spot.Left,
                    stretch: go.GraphObject.Vertical,
                    rowSizing: go.RowColumnDefinition.ProportionalExtra,
                    itemTemplate:
                    $(go.Panel, "TableRow",
                        $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                        )
                }
                ),
            $(go.Panel, "Vertical",
                { column: 0, itemTemplate: makePortTemplate(true) },
                new go.Binding("itemArray", "inputs")
                ),
            $(go.Panel, "Vertical",
                { column: 1, itemTemplate: makePortTemplate(false) },
                new go.Binding("itemArray", "outputs")
                ),
            $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
            /*$(go.TextBlock,
                "ATS",
                { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),*/
            ),

          );
        // JUnctionbox template
        var junctionBoxTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
          $(go.Panel, "Table",
            $(go.Panel, "Table",
                new go.Binding("itemArray", "inputPorts"),
                {
                    margin: new go.Margin(4, 0),
                    alignment: go.Spot.Left,
                    stretch: go.GraphObject.Vertical,
                    rowSizing: go.RowColumnDefinition.ProportionalExtra,
                    itemTemplate:
                    $(go.Panel, "TableRow",
                        $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                        )
                }
                ),
            $(go.Panel, "Vertical",
                { column: 0, itemTemplate: makePortTemplate(true) },
                new go.Binding("itemArray", "inputs")
                ),
            $(go.Panel, "Vertical",
                { column: 1, itemTemplate: makePortTemplate(false) },
                new go.Binding("itemArray", "outputs")
                ),
            $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
            /*$(go.TextBlock,
                "Junction Box Template",
                { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),*/
            ),

          );
        // Optimizer template
        var optimizrTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
          $(go.Panel, "Table",
            $(go.Panel, "Table",
                new go.Binding("itemArray", "inputPorts"),
                {
                    margin: new go.Margin(4, 0),
                    alignment: go.Spot.Left,
                    stretch: go.GraphObject.Vertical,
                    rowSizing: go.RowColumnDefinition.ProportionalExtra,
                    itemTemplate:
                    $(go.Panel, "TableRow",
                        $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                        )
                }
                ),
            $(go.Panel, "Vertical",
                { column: 0, itemTemplate: makePortTemplate(true) },
                new go.Binding("itemArray", "inputs")
                ),
            $(go.Panel, "Vertical",
                { column: 1, itemTemplate: makePortTemplate(false) },
                new go.Binding("itemArray", "outputs")
                ),
            $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
            /*$(go.TextBlock,
                "Optimizer",
                { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),*/
            ),

          );

        // Transformer template
        var transformerTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
          $(go.Panel, "Table",
            $(go.Panel, "Table",
                new go.Binding("itemArray", "inputPorts"),
                {
                    margin: new go.Margin(4, 0),
                    alignment: go.Spot.Left,
                    stretch: go.GraphObject.Vertical,
                    rowSizing: go.RowColumnDefinition.ProportionalExtra,
                    itemTemplate:
                    $(go.Panel, "TableRow",
                        $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                        )
                }
                ),
            $(go.Panel, "Vertical",
                { column: 0, itemTemplate: makePortTemplate(true) },
                new go.Binding("itemArray", "inputs")
                ),
            $(go.Panel, "Vertical",
                { column: 1, itemTemplate: makePortTemplate(false) },
                new go.Binding("itemArray", "outputs")
                ),
            $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
            /*$(go.TextBlock,
                "Transformer",
                { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),*/
            ),

          );
      // Dc inverter template
      var dcInverterTemplate =
      $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
          $(go.Panel, "Table",
            $(go.Panel, "Table",
                new go.Binding("itemArray", "inputPorts"),
                {
                    margin: new go.Margin(4, 0),
                    alignment: go.Spot.Left,
                    stretch: go.GraphObject.Vertical,
                    rowSizing: go.RowColumnDefinition.ProportionalExtra,
                    itemTemplate:
                    $(go.Panel, "TableRow",
                        $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                        )
                }
                ),
            $(go.Panel, "Vertical",
                { column: 0, itemTemplate: makePortTemplate(true) },
                new go.Binding("itemArray", "inputs")
                ),
            $(go.Panel, "Vertical",
                { column: 1, itemTemplate: makePortTemplate(false) },
                new go.Binding("itemArray", "outputs")
                ),
            $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
            /*$(go.TextBlock,
                "Dc Inverter",
                { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),*/
            ),

          );


      var blankTemplate =
      $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
          $(go.Picture,
            { width: 80, height: 60, background: "white" },
            new go.Binding("source")));


          // add the templates created above to myDiagram and palette
          myDiagram.nodeTemplateMap.add("blank", blankTemplate);
          myDiagram.nodeTemplateMap.add("pvModule", pvModuleTemplate);
          myDiagram.nodeTemplateMap.add("microInverter", microInverterTemplate);
          myDiagram.nodeTemplateMap.add("solarLoadCenter", solarLoadCenterTemplate);
          myDiagram.nodeTemplateMap.add("soladeck", soladeckTemplate);
          myDiagram.nodeTemplateMap.add("nonFusedDisconnect", nonFusedDisconnectTemplate);
          myDiagram.nodeTemplateMap.add("teslaGateway", teslaGatewayTemplate);
          myDiagram.nodeTemplateMap.add("fusedDisconnect", fusedDisconnectTemplate);
          myDiagram.nodeTemplateMap.add("battery", batteryTemplate);
          myDiagram.nodeTemplateMap.add("meter", meterTemplate);
          myDiagram.nodeTemplateMap.add("monitoring", monitoringTemplate);
          myDiagram.nodeTemplateMap.add("ats", atsTemplate);
          myDiagram.nodeTemplateMap.add("junctionBox", junctionBoxTemplate);
          myDiagram.nodeTemplateMap.add("optimizr", optimizrTemplate);
          myDiagram.nodeTemplateMap.add("transformer", transformerTemplate);
          myDiagram.nodeTemplateMap.add("dcInverter", dcInverterTemplate);


      // share the template map with the Palette
      palette.nodeTemplateMap = myDiagram.nodeTemplateMap;

      palette.model.nodeDataArray = [
        { category: "blank", source: "{{ asset('front/images/blank.png')}}" },
        { category: "blank",  source: "{{ asset('front/images/blank.png')}}"},
        { category: "blank", source: "{{ asset('front/images/blank.png')}}"},
        { category: "pvModule", source: "{{ asset('front/images/b1.png')}}", inputs: [],
        outputs: [{ id: "W" }]  },
        { category: "microInverter", source: "{{ asset('front/images/b2.png')}}", inputs: [{ id: "A" }],outputs: [{ id: "W" }]},

        { category: "nonFusedDisconnect", source: "{{ asset('front/images/b4.png')}}", inputs: [{ id: "A" }],outputs: [{ id: "W" }] },

        { category: "fusedDisconnect", source: "{{ asset('front/images/b6.png')}}", inputs: [{ id: "A" }],outputs: [{ id: "W" }]},
        { category: "soladeck", source: "{{ asset('front/images/b7.png')}}", inputs: [{ id: "A" }],outputs: [{ id: "W" }]},
        { category: "battery", source: "{{ asset('front/images/b8.png')}}", inputs: [],outputs: [{ id: "W" }] },
        { category: "meter", source: "{{ asset('front/images/b9.png')}}", inputs: [{ id: "A" }],outputs: [{ id: "W" }] },
        { category: "monitoring", source: "{{ asset('front/images/b10.png')}}", inputs: [],outputs: [{ id: "W" }] },
        { category: "ats", source: "{{ asset('front/images/b11.png')}}", inputs: [{ id: "A" }],outputs: [{ id: "W" }] },
        { category: "junctionBox", source: "{{ asset('front/images/b12.png')}}", inputs: [{ id: "A" }],outputs: [{ id: "W" }] },
        { category: "optimizr", source: "{{ asset('front/images/b13.png')}}", inputs: [{ id: "A" }],outputs: [{ id: "W" }] },
        { category: "transformer", source: "{{ asset('front/images/b14.png')}}", inputs: [{ id: "A" }],outputs: [{ id: "W" }] },
        { category: "solarLoadCenter", source: "{{ asset('front/images/b3.png')}}", inputs: [{ id: "A" }],outputs: [{ id: "W" }] },
        { category: "teslaGateway", source: "{{ asset('front/images/b5.png')}}" ,inputs: [{ id: "A" }],outputs: [{ id: "W" }] },
        { category: "dcInverter", source: "{{ asset('front/images/b15.png')}}", inputs: [{ id: "A" }],outputs: [{ id: "W" }] },
        ];


        $(function() {
            myDiagram.mouseDrop = function(e) {
                //openForm();
                myDiagram.commit(diag => {
                    diag.selection.each(n => {
                        if (!(n instanceof go.Node)) return;
                        const m = diag.model;

                        if(n.data.category=="pvModule"){
                            dynamicPortDataAppend(0,1);
                        }else if(n.data.category=="microInverter"){
                            dynamicPortDataAppend(2,1);
                        }else if(n.data.category=="nonFusedDisconnect"){
                            dynamicPortDataAppend(1,1);
                        }else if(n.data.category=="fusedDisconnect"){
                            dynamicPortDataAppend(1,1);
                        }else if(n.data.category=="soladeck"){
                            dynamicPortDataAppend(4,4);
                        }else if(n.data.category=="battery"){
                            dynamicPortDataAppend(0,1);
                        }else if(n.data.category=="meter"){
                            dynamicPortDataAppend(1,1);
                        }else if(n.data.category=="monitoring"){
                            dynamicPortDataAppend(0,1);
                        }else if(n.data.category=="ats"){
                            dynamicPortDataAppend(2,1);
                        }else if(n.data.category=="junctionBox"){
                            dynamicPortDataAppend(4,4);
                        }else if(n.data.category=="optimizr"){
                            dynamicPortDataAppend(2,1);
                        }else if(n.data.category=="transformer"){
                            dynamicPortDataAppend(1,1);
                        }else if(n.data.category=="solarLoadCenter"){
                            dynamicPortDataAppend(10,1);
                        }else if(n.data.category=="teslaGateway"){
                            dynamicPortDataAppend(20,3);
                        }else if(n.data.category=="dcInverter"){
                            dynamicPortDataAppend(20,1);
                        }
                  });
                });
            }
        });

        myDiagram.addDiagramListener("ObjectSingleClicked",function(e){
            myDiagram.commit(diag => {
                diag.selection.each(n => {
                    if (!(n instanceof go.Node)) return;
                    const m = diag.model;

                    if(n.data.category=="pvModule"){
                        dynamicPortDataAppend(0,1);
                    }else if(n.data.category=="microInverter"){
                        dynamicPortDataAppend(2,1);
                    }else if(n.data.category=="nonFusedDisconnect"){
                        dynamicPortDataAppend(1,1);
                    }else if(n.data.category=="fusedDisconnect"){
                        dynamicPortDataAppend(1,1);
                    }else if(n.data.category=="soladeck"){
                        dynamicPortDataAppend(4,4);
                    }else if(n.data.category=="battery"){
                        dynamicPortDataAppend(0,1);
                    }else if(n.data.category=="meter"){
                        dynamicPortDataAppend(1,1);
                    }else if(n.data.category=="monitoring"){
                        dynamicPortDataAppend(0,1);
                    }else if(n.data.category=="ats"){
                        dynamicPortDataAppend(2,1);
                    }else if(n.data.category=="junctionBox"){
                        dynamicPortDataAppend(4,4);
                    }else if(n.data.category=="optimizr"){
                        dynamicPortDataAppend(2,1);
                    }else if(n.data.category=="transformer"){
                        dynamicPortDataAppend(1,1);
                    }else if(n.data.category=="solarLoadCenter"){
                        dynamicPortDataAppend(10,1);
                    }else if(n.data.category=="teslaGateway"){
                        dynamicPortDataAppend(20,3);
                    }else if(n.data.category=="dcInverter"){
                        dynamicPortDataAppend(20,1);
                    }
                });
            });
        });

      load();
      loop();

    function dynamicPortDataAppend(inputsData,outputsData){

        var select = document.getElementById('input_port');
        if(inputsData>0){
            var values = [];
            for(i=1;i<=inputsData;i++){
                values.push(i);
            }
            console.log(values);
            select.innerHTML="";
            for (const val of values)
            {
                var option = document.createElement("option");
                option.value = val;
                option.text = val;
                select.appendChild(option);
            }
        }else{
            select.innerHTML="";
        }

        var outputPorts = document.getElementById('output_port');
        if(outputsData>0){
            console.log("outputPorts.html",outputPorts);
            var valuesout = [];
            outputPorts.innerHTML="";
            for(i=1;i<=outputsData;i++){
                valuesout.push(i);
            }
            console.log("valuesout",valuesout);
            for (const valout of valuesout)
            {
                var optionout = document.createElement("option");
                optionout.value = valout;
                optionout.text = valout;
                outputPorts.appendChild(optionout);
            }
        }else{
            outputPorts.innerHTML="";
        }

    }


  }

  function findNextPortId(node) {
      let i = 0;
      const defaultport = node.port;
      while (node.findPort("P" + i) !== defaultport) i++;
      return "P" + i;
  }

  function addPort(input) {
      myDiagram.commit(diag => {
        diag.selection.each(n => {
            console.log("n >> "+JSON.stringify(n.data));
            if (!(n instanceof go.Node)) return;
            const m = diag.model;
            m.addArrayItem(input ? n.data.inputs : n.data.outputs, { id: findNextPortId(n) });
        })
    });
  }

  function removePort(input) {
      myDiagram.commit(diag => {
        diag.selection.each(n => {
          if (!(n instanceof go.Node)) return;
          const m = diag.model;
          const arr = input ? n.data.inputs : n.data.outputs;
          if (arr.length > 0) m.removeArrayItem(arr, arr.length-1);
      })
    });
  }
  function removeAllPort(input) {
      myDiagram.commit(diag => {
        diag.selection.each(n => {
          if (!(n instanceof go.Node)) return;
          const m = diag.model;
          const arr = input ? n.data.inputs : n.data.outputs;
          var length = arr.length;
          for(var i=1; i <= length; i++) {
            if (arr.length > 0) m.removeArrayItem(arr, arr.length-1);
          }
      })
    });
  }

    // update the diagram every 250 milliseconds
    function loop() {
      setTimeout(function() { updateStates(); loop(); }, 250);
  }

    // update the value and appearance of each node according to its type and input values
    function updateStates() {
      var oldskip = myDiagram.skipsUndoManager;
      myDiagram.skipsUndoManager = true;
      // do all "input" nodes first
      myDiagram.nodes.each(function(node) {
        if (node.category === "input") {
          doInput(node);
      }
  });
      // now we can do all other kinds of nodes
      myDiagram.nodes.each(function(node) {
        switch (node.category) {
          case "output": doOutput(node); break;
          case "input": break;  
      }
  });
      myDiagram.nodes.each(function(node) {
        switch (node.name) {
          case "Don Meow": doAnd(node); break;
      }
  });
      myDiagram.skipsUndoManager = oldskip;
  }

    // helper predicate
    function linkIsTrue(link) {  // assume the given Link has a Shape named "SHAPE"
    return link.findObject("SHAPE").stroke === green;
}

    // helper function for propagating results
    function setOutputLinks(node, color) {
      node.findLinksOutOf().each(function(link) { link.findObject("SHAPE").stroke = color; });
  }

    function doInput(node) {
      // the output is just the node's Shape.fill
      setOutputLinks(node, node.findObject("NODESHAPE").fill);
    }

    function save() {
      document.getElementById("mySavedModel").value = myDiagram.model.toJson();
      myDiagram.isModified = false;
  }
  function load() {
      myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
  }


  window.addEventListener('DOMContentLoaded', init);

</script>


@endpush
@endsection
