@extends('front.layouts.app')
@section('title',$data['page_title'])
@section('content')
@push('styles')
<style>
    .drag_div  {
      width: 100%;
      height: 100%;
      padding: 10px;
      border: 1px solid #aaaaaa;
    }
    canvas{border:1px solid red;}
    #exportedImgs{border:1px solid green; padding:15px; width:300px; height:70px;}
    #toolbar{
        width:350px;
        height:35px;
        border:solid 1px blue;
    }
    .bords_page .structural-col .structural-details-col .item .board_box img {
        width:50px !important;
        height:50px !important;
    }
    #container {
        background-color: rgba(0, 0, 0, 0.1);
        border:1px solid #000;
        min-height: 500px;
      }
</style>
@endpush
<div class="body-section bg-gray structural-body-sec  h-auto">

    <!-- Recent Projects section -->
    <section class="recent-projects-sec structural-sec el_form bords_page">
        <div class="d-flex structural-sec-inner">


            <div class="structural-col w-100">
                <!-- Inner Banner -->
                <section class="inner-bnr"></section>
                <div class="container-fluid">
                    <div class="title-sec">
                        <div class="row m-0">
                            <div class="col-lg-4 col-md-6 mb-md-0 mb-5 p-0 bords_cate">

                                <div class="structural-details-col  electrical_page_top  bg-transparent">
                                    <div class="row" id="drag-items">
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                        <img src="{{ asset('front/images/b1.png')}}" alt="" id="pv_module"   draggble="true">
                                                        <h4 >PV MODULE</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b2.png')}}" alt="" id="micro_inverter"  draggble="true">
                                                    <h4>MICRO INVERTER</h4>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b3.png')}}" alt="" id="solar_load_center"  draggble="true">
                                                    <h4>SOLAR LOAD CENTER</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box active"  >
                                                    <img src="{{ asset('front/images/b4.png')}}" alt="" id="non_fused_disconnect"  draggble="true">
                                                    <h4>NON FUSED DISCONNECT</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b5.png')}}" alt="" id="tesla_gateway"  draggble="true">
                                                    <h4>TESLA GATEWAY</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b6.png')}}" alt="" id="fused_disconnect"  draggble="true">
                                                    <h4>FUSED DISCONNECT</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b7.png')}}" alt="" id="solardeck"  draggble="true">
                                                    <h4>SOLADECK</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b8.png')}}" alt="" id="battery"  draggble="true">
                                                    <h4>BATTERY</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b9.png')}}" alt="" id="meter"  draggble="true">
                                                    <h4>METER</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b10.png')}}" alt="" id="monitoring"  draggble="true">
                                                    <h4>MONITORING</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b11.png')}}" alt="" id="ats"  draggble="true">
                                                    <h4>ATS</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b12.png')}}" alt="" id="junction_box"  draggble="true">
                                                    <h4>JUNCTION BOX</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b13.png')}}" alt="" id="optimizer"  draggble="true">
                                                    <h4>OPTIMIZR</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b14.png')}}" alt="" id="transformer"  draggble="true">
                                                    <h4>TRANSFORMER</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b15.png')}}" alt="" id="dc_inverter"  draggble="true">
                                                    <h4>DC INVERTER</h4>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <!--****************************** Inverter Properties**************************************** -->
                            <div class="col-lg-8 col-md-6">
                                <div class="inventer_properties_sec board_circuit mt-0">
                                    <h2 class="c_text">Images Diagram</h2>
                                    <div  class="structural-details-col" >
                                                {{-- <div class="board_circuit_img">
                                                    <img src="{{ asset('front/images/circuit.png')}}" alt="">
                                                </div> --}}
                                {{-- <center>
                                    <canvas id="grid" width="610" height="610" style="border:1px solid #00ff00"></canvas>
                                </center> --}}
                                <div id="container"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>


        </div>
</div>


@push('scripts')

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/konva@8.1.2/konva.min.js"></script>

<script>


$(window).scroll(function(){
if ($(this).scrollTop() > 150) {
   $('.c_text').addClass('new_text');
} else {
   $('.c_text').removeClass('new_text');
}
});

function handleDragStart(e) {
    this.style.opacity = '0.4';
}
function handleDragEnd(e) {
    this.style.opacity = '1';
}
let items = document.querySelectorAll('.row img');
items.forEach(function(item) {
    item.addEventListener('dragstart',handleDragStart, false);
    item.addEventListener('dragend',handleDragEnd, false);
})

// var cnv = document.getElementById("grid");
// var ctx = cnv.getContext("2d");

// for(var i = 5; i <= 605; i = i+6) {
//     // verticale lines
//     ctx.moveTo(i, 5);
//     ctx.lineTo(i, 605);

//     //horizontal line
//     ctx.moveTo(5, i);
//     ctx.lineTo(605,i);

//     ctx.strokeStyle = "#f0f0f0";
//     ctx.stroke();
// }
// ctx.beginPath();
// for(var i = 5; i <= 605; i = i+30) {
//     // verticale lines
//     ctx.moveTo(i, 5);
//     ctx.lineTo(i, 605);

//     //horizontal line
//     ctx.moveTo(5, i);
//     ctx.lineTo(605,i);

//     ctx.strokeStyle = "#c0c0c0";
//     ctx.stroke();
// }


// function allowDrop(ev) {
//     ev.preventDefault();
// }
// function drag(ev) {
//     ev.dataTransfer.setData("this",ev.target.id);
// }
// function drop(ev) {
//     ev.preventDefault();
//     let data = ev.dataTransfer.getData("this");
//     ev.target.appendChild(document.getElementById(data));
// }

var width = window.innerWidth;
      var height = window.innerHeight;

      var stage = new Konva.Stage({
        container: 'container',
        width: width,
        height: height,
      });
      var layer = new Konva.Layer();
      stage.add(layer);

      // what is url of dragging element?
      var itemURL = '';
      document
        .getElementById('drag-items')
        .addEventListener('dragstart', function (e) {
          itemURL = e.target.src;
        });

      var con = stage.container();
      con.addEventListener('dragover', function (e) {
        e.preventDefault(); // !important
      });

      con.addEventListener('drop', function (e) {
        e.preventDefault();
        // now we need to find pointer position
        // we can't use stage.getPointerPosition() here, because that event
        // is not registered by Konva.Stage
        // we can register it manually:
        stage.setPointersPositions(e);

        Konva.Image.fromURL(itemURL, function (image) {
          layer.add(image);

          image.position(stage.getPointerPosition());
          image.draggable(true);
        });
      });


$(document).ready(function(){

    });
</script>

@endpush
@endsection
