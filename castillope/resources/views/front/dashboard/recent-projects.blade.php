@extends('front.layouts.app')
@section('title','Recent Projects')
@section('content')
@push('styles')
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<style>
.report-type {
    border-color: #A80D2C;
    color: #A80D2C !important;
}

</style>
@endpush
<div class="body-section body-login">

<!-- Inner Banner -->
<section class="inner-bnr"></section>

<!-- Recent Projects section -->
<section class="recent-projects-sec">
  <div class="container-fluid">
    <div class="d-flex align-items-center title-sec">
      <h5>Recent Projects</h5>
      <a href="#" class="btn btn-white ml-auto create-project" id="create-project">Create New Project</a>
    </div>
    <div id="accordion" class="accordian-collapse">
    @include('front.includes.message')

      @foreach($projects as $project)
      <div class="card">
        <div class="card-header">
          <h5 class="d-flex flex-wrap align-items-center">
          	<div class="col-md-6 col-md-5">
          		<span class="owner-name"> Owner Name: </span> {{$project->owner_name}}
          	</div>
          	<div class="col-lg-6 col-md-7 d-flex justify-content-md-end btn-area">
              @if($project->type == 'structural' || $project->type == 'full')
          	    <a class="btn btn-success brn-sm"  href="{{url('structural/'.$project->id)}}" data-toggle="tooltip" title="Edit Structure Report"><i class="far fa-building"></i></a>
              @endif
              @if($project->type == 'electrical' || $project->type == 'full')
                <a class="ml-3 btn btn-warning brn-sm" href="{{url('electrical/'.$project->id)}}" id="myBtn" data-toggle="tooltip" title="Edit Electrical Report"><i class="far fa-lightbulb"></i></a>
                <a class="ml-3 btn btn-warning brn-sm" href="{{route('block.design.reports',[$project->id])}}" id="" data-toggle="tooltip" title="Block Design Report"><i class="far fa-eye"></i></a>
              @endif
              @if($project->type == 'block_design')
              <a class="ml-3 btn btn-warning brn-sm" href="{{route('block.design.reports',[$project->id])}}" id="" data-toggle="tooltip" title="Block Design Report"><i class="far fa-eye"></i></a>
              @endif
                <a class="edit-project ml-3 btn edit-btn" href="{{url('project/'.$project->id.'/edit')}}" data-toggle="tooltip" title="Edit Project Info"><i class="fas fa-edit"></i></a>

                <a class="delete-project ml-3 btn  edit-btn" href="{{url('project/'.$project->id.'/delete')}}" data-toggle="tooltip" title="Delete Project Info"><i class="fas fa-trash-alt"></i></a>
              <a class="print-project ml-3 btn  edit-btn" target="_blank()" href="{{url('project-detail-pdf/'.$project->id)}}" data-toggle="tooltip" title="Print"><i class="fas fa-print"></i></a>
             <!--  <button class="ml-auto" id="myBtn"><span class="icon icon-accordian-add morelink"></span></button> -->
          	</div>

          </h5>

          <div class="col-12 edit-ssection">
	          <div class="more">
                  <p >Report Type : <span class="report-type" >{{ucwords(str_replace("_"," ",$project->type))}}</span></p>
                  <br>
	            <p>
                    <b>Address :</b> {{$project->address}}
                    <b class="ml-2">| State :</b> {{$project->state}}
                    @if($project->type == 'full' || $project->type == 'structural')
                    <b class="ml-2">| Ult Wind Speed :</b> {{$project->ult_wind_speed}}
                    <b class="ml-2">| Ground Snow Load :</b> {{$project->ground_snow_load}}
                    @endif
                    <b class="ml-2">| AHJ :</b> {{$project->ahj}}
                </p>
	          </div>
          </div>


        </div>
      </div>
      @endforeach
      {{ $projects->appends(request()->query())}}







    </div>
  </div>
</section>
</div>
<!-- Start Project Modal -->
<div class="modal fade startprojectmodal" id="startprojectmodal" tabindex="-1" aria-labelledby="startprojectmodalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span class="icon-accordian-add"></span>
      </button>
      <h2>New Project</h2>
      <input id="create-project-url" type="hidden" value="{{url('project')}}" />
      <form action="{{url('project')}}" method="post" id="create-project-form">
      @csrf
        <input type="hidden" name="_method" id="_method" value="" />
        <div class="row">
          <div class="col-md-6">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Owner Name" name="owner_name" id="owner_name">
            </div>

          </div>
          <div class="col-md-6">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Address" name="address" id="address">
            </div>
          </div>
          <div class="col-md-6">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="State" name="state" id="state">
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="AHJ" name="ahj" id="ahj">
            </div>
          </div>
          <div class="col-md-6">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Ult Wind Speed" name="ult_wind_speed" id="ult_wind_speed">
            </div>
          </div>
          <div class="col-md-6">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Ground Snow Load" name="ground_snow_load" id="ground_snow_load">
            </div>
          </div>
          <div class="col-md-6">
            <div class="additional-info d-flex align-items-center">
              <p>For Structural Loads</p>
              <a target="_blank" class="ml-auto" href="https://hazards.atcouncil.org/" data-toggle="tooltip" title="atchazards">
              <img class="ml-auto" src="{{url('front/images/atc-logo-blue.png')}}" width="40" /></a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="additional-info d-flex align-items-center">
              <p>For Design Temperature</p>
               <a target="_blank" class="ml-auto" href="http://www.solarabcs.org" data-toggle="tooltip" title="solarabcs">
              <img class="ml-auto" src="{{url('front/images/Banner_760x150.jpg')}}" width="40" /></a>

            </div>
          </div>
        </div>

        <div class="row btn-groups-row create-project-div" >
          <div class="col-sm-4 mb-sm-0 mb-3">
            <a href="{{url('structural')}}" id="structural_report" class="btn btn-tab btn-red create-project-submit">Create Structural Report</a>
          </div>
          <div class="col-sm-4 mb-sm-0 mb-3">
            <a href="{{url('electrical')}}" id="electrical_report" href="#" class="btn btn-tab btn-gray create-project-submit">Create Electrical Report</a>
          </div>
          <div class="col-sm-4 mb-sm-0 mb-3">
            <a href="{{url('structural')}}" id="full_report" href="#" class="btn btn-tab btn-black create-project-submit">Create Full Report</a>
          </div>
        </div>
        <div class="row btn-groups-row update-project-div" >
        <div class="col-sm-4 mb-sm-0 mb-3"></div>
        <div class="col-sm-4 mb-sm-0 mb-3" >
            <a  href="#" id="update-project-report" class="btn btn-tab btn-red create-project-submit">Update Project Report</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

@push('scripts')
<script>
// $(".submit").click(function(){
// alert("check");
// });
</script>

<script src="{{asset('front/js/projects/event.js')}}"></script>
@endpush
@endsection
