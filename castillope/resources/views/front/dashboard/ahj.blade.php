@extends('front.layouts.app')
@section('title',$data['page_title'])
@section('content')

<div class="body-section body-login">

<!-- Inner Banner -->
<section class="inner-bnr"></section>

<!-- Recent Projects section -->
<section class="recent-projects-sec">
  <div class="container-fluid">
    <div class="d-flex align-items-center title-sec">
      <h5>{{ $data['page_title'] }}</h5>

    </div>
    <div id="accordion" class="accordian-collapse">
    @include('front.includes.message')
      @if($ahjs)
      @foreach($ahjs as $key => $value)
      <div class="card">
        <div class="card-header">
            <h5 class="d-flex align-items-center">
                <span class="owner-name"> AHJ Name: </span>&nbsp;{{$value->ahj}} <button class="ml-auto" id="myBtn"><span class="icon icon-accordian-add morelink"></span></button>
            </h5>

          <div class="col-12 edit-ssection">
	          <div class="more">
	            <p>
                    <b>State:</b> {{$value->state}}
                    <b class="ml-2">| Fire Setback:</b> {{ $value->fire_setback == 1 ? "Yes":"No" }}
                    <b class="ml-2">| Note:</b> {{$value->note}}
                </p>
	          </div>
          </div>


        </div>
      </div>
      @endforeach

      {{ $ahjs->appends(request()->query())}}

      @endif







    </div>
  </div>
</section>
</div>


@push('scripts')
<script>
// $(".submit").click(function(){
// alert("check");
// });
</script>

@endpush
@endsection
