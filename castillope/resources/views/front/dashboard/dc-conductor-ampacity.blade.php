@inject('formula', 'App\Http\Controllers\Front\ElectricalStringController')
@extends('front.layouts.app')
@section('title', $type." Conductor Ampacity Calculation")
@section('content')
@push('styles')
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" /> --}}
<style>

.new-popup .modal-dialog {
    top: 15px !important;
    transform: translateY(0px) !important;
}
</style>
@endpush
@php
function getTempratureCorrection($temp) {
    $factor = 0;
    $tempCorrection = DB::table('temprature_corrections')
                    ->select('factor')
                    ->whereRaw("(( min_val >= $temp AND max_val <= $temp ) || (max_val >= $temp AND min_val <= $temp))")
                    ->first();
    if($tempCorrection) {
        $factor = $tempCorrection->factor;
    }
    return $factor;

}
function getinverterTypeIds($ids) {
    return DB::table('inverters')
                    ->select('inverter_type_id')
                    ->whereIn("id",$ids)
                    ->pluck('inverter_type_id')->toArray();


}
function getStringDuplicateNo($electrical_id,$string_id_duplicate) {
    return DB::table('electrical_strings')
                    ->where(['electrical_id'=>$electrical_id,'string_id_duplicate'=>$string_id_duplicate])
                    ->count();
}
@endphp<div class="body-section bg-gray structural-body-sec">

<!-- Inner Banner -->
<!-- <section class="inner-bnr"></section> -->

 <!-- Recent Projects section -->
 <section class="recent-projects-sec structural-sec el_form">
    <div class="d-flex structural-sec-inner">
        <div class="sidebar">
            <div class="sidebar-inner">
                <button id="close-btn" class="d-lg-none d-block">X</button>
                <h5>Please fill the details first </h5>
                <form action="#">
                    <div class="input-box">
                        <div class="form-group">
                            <div class=" form-control" style="width:100%;">
                                {{-- <select>
                                    <option value="0">Dc Conductor Ampacity Calculations</option>
                                    <option value="1">AC Conductor Ampacity Calculations</option>
                                    <option value="2">DC Conductor Ampacity Calculations</option>
                                    <option value="3">Battery Conductor Ampacity Calculations</option>
                                </select> --}}

                                @php
                                    $inverterIds = getinverterTypeIds($electrical->ElectricalString->pluck('inverter_id'));
                                    // echo "<pre>";
                                    //     print_r($inverterIds);
                                    //     if(!in_array(3,$inverterIds))
                                    //     echo "yes";
                                    //     exit;
                                @endphp
                                @if(in_array(2,$inverterIds) || in_array(3,$inverterIds))
                                <p><a href="{{ route('conductor.ampacity.calculations',[$electrical->id,'DC']) }}"> Dc Conductor Ampacity Calculations</a></p>

                                @endif
                                <p> <a href="{{ route('conductor.ampacity.calculations',[$electrical->id,'AC']) }}"> Ac Conductor Ampacity Calculations</a> </p>
                                @if($electrical->battery == 'Yes')
                                <p> <a href="{{ route('conductor.ampacity.calculations',[$electrical->id,'Battery']) }}">Battery Conductor Ampacity Calculations</a><p>
                                @endif
                            </div>
                        </div>

                    </div>

                    {{-- <div class="btn-row"><button type="button"  class="btn btn-red w-100" >Submit</button></div> --}}


                </form>
            </div>
        </div>

        <div class="structural-col">
            <!-- Inner Banner -->
            <section class="inner-bnr"></section>
            <div class="container-fluid">
                <div class="title-sec">
                    <div class="navbar-light left-side-btn mb-4">
                        <button class="navbar-toggler d-lg-none d-block">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-md-0 mb-5">
                            <h5 class="text-uppercase">{{ $type }} Conductor Ampacity Calculations</h5>

                            <div class="structural-details-col  electrical_page_top elec2">
                                <!-- Accordian Section -->

                                <div id="accordion" class="accordian-collapse structural-collapse">

                                    @if($electrical_strings)
                                    <form id="create-electrical-string-form" class="create-electrical-string-form new_g" action="{{url('electrical-string/'.$electrical_strings[0]->id)}}" method="post">
                                    @csrf
                                    @method("PUT")
                                    <input type="hidden" name="electrical_formula" id="electrical_formula" value="{{url('electrical-formula')}}" />
                                    <input type="hidden" name="ambient_temp" id="ambient_temp" value="{{$electrical_strings[0]->electrical->ambient_temp}}" />
                                    <input type="hidden" name="electrical_id" id="electrical_id" value="{{$electrical_strings[0]->electrical_id}}" />
                                    <input type="hidden" name="url_type" id="url_type" value="{{$type}}" />
                                    @php
                                    function nextLargestValue(array $arr, $val)
                                    {
                                        $last = null;
                                        foreach ($arr as $key => $value) {
                                            if($value > $val) {
                                                return $value;
                                            }
                                            $last = $value;
                                        }
                                        return $last;
                                    }
                                    @endphp
                                    @php $j = 1;$i = 1; 
                                    $i_new = 1;
                                    $j_new = 0.1;
                                    @endphp
                                    @foreach($electrical_strings->where('type','string') as $electrical_string)

                                    @if(strtolower($electrical_string->inverter->model_no) == 'enphase iq 7')
                                        @php
                                            $current = $electrical_string->size * 1.25;
                                        @endphp
                                    @elseif(strtolower($electrical_string->inverter->model_no) == 'enphase iq 7 plus')
                                        @php
                                            $current = $electrical_string->size * 1.25 * 1.21;
                                        @endphp
                                    @else
                                        @php
                                            $current = ($electrical_string->module->isc * $electrical_string->inverter->InverterType->value);
                                        @endphp
                                    @endif


                                    @if($electrical_string->conduit_derate == 0)
                                        @php
                                           // $electrical_string->conduit_derate = $formula->conduitDerate(2,$electrical_strings->where('type','string')->count(), 1);
                                        @endphp
                                    @endif
                                    @if($electrical_string->temp_derate == 0)
                                        @php
                                            //$electrical_string->temp_derate = $formula->tempDerate(0,$electrical_strings[0]->electrical->ambient_temp);
                                        @endphp
                                    @endif


                                    <div class="card {{ strtolower($electrical_string->inverter->InverterType->name) }} ">
                                        <div class="card-header" id="heading{{ $loop->iteration }}" data-toggle="collapse"
                                            data-target="#collapse{{ $loop->iteration }}" aria-expanded="true"
                                            aria-controls="collapse{{ $loop->iteration }}">
                                            <h5 class="d-flex align-items-center mb-0">
                                                <span style="color: #A80D2C;" class="string_id"> String ID {{ $electrical_string->id }}</span> : <span  class="string_id_no">&nbsp; #@if($electrical_string->string_id_duplicate==0){{ $i_new }}@else @if($i_new > 0){{ $i_new + $j_new - 1 }}@else{{ $i_new + $j_new }} @endif @endif {{ $electrical_string->inverter->manufacturer->title }} {{ $electrical_string->inverter->InverterType->name }} </span>
                                                    <span class="string_checkbox">&nbsp;<input style="display: none;" name="string_checkbox_val[{{ $electrical_string->id }}]"  id="{{ $electrical_string->id }}"  data-string-id="{{ $electrical_string->id }}"  class="string_checkbox_val @if(strtolower($electrical_string->inverter->InverterType->name) != 'generac') @if($electrical->CombinerString && in_array($electrical_string->string_checkbox_val,$electrical->CombinerString->pluck('electrical_string_id')->toArray())) selected @else notSelectedCombiner @endif @endif @if(strtolower($electrical_string->inverter->InverterType->name) == 'generac') @if($electrical->OptimizerString && in_array($electrical_string->string_checkbox_val,$electrical->OptimizerString->pluck('electrical_string_id')->toArray())) selected @else notSelectedOptimizer @endif @endif " type="checkbox" value="{{ $current }}"  /></span>
                                                    <span class="icon icon-accordian-2-add ml-auto"></span>
                                                </h5>
                                        </div>

                                        <div id="collapse{{ $loop->iteration }}" class="collapse @if($loop->iteration ==1) show @endif cus_acro"
                                            aria-labelledby="heading{{ $loop->iteration }}" data-parent="#accordion">
                                            <div class="card-body">

                                                    <input type="hidden" name="id[]" class="id" value="{{ $electrical_string->id }}" />
                                                    <input type="hidden" name="type[]" class="type" value="{{ $electrical_string->type }}" />
                                                    <input type="hidden" name="module_id[]" class="module_id" value="{{ $electrical_string->module_id}}" />
                                                    <input type="hidden" name="inverter_id[]" class="inverter_id" value="{{ $electrical_string->inverter_id }}" />
                                                    <input type="hidden" name="size[]" class="size" value="{{ $electrical_string->size }}" />
                                                    <div class="row roof-container">
                                                        <div class="col-md-12 mb-md-0 mb-5">
                                                            <div class="row">

                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="from_val">From </label>
                                                                            <select class="form-control custom-select2 from_val" name="from_val[]" style="width:100%;">
                                                                                <option value="">Select From</option>
                                                                                <option value="module" {{ $electrical_string->from_val=='module' ? "selected":""}}>Module</option>
                                                                                <option value="inverter" @if($type == 'AC' && ($electrical_string->from_val == '' || $electrical_string->from_val == null)) @if(strtolower($electrical_string->inverter->InverterType->name) == 'string inverter') selected @endif @else {{ $electrical_string->from_val=='inverter' ? "selected":""}} @endif>Inverter</option>
                                                                                <option value="pv_meter" {{ $electrical_string->from_val=='pv_meter' ? "selected":""}}>Pv Meter</option>
                                                                                <option value="ac_disconnect" {{ $electrical_string->from_val=='ac_disconnect' ? "selected":""}}>Ac Disconnect</option>
                                                                                <option value="poi" {{ $electrical_string->from_val=='pv_meter' ? "selected":""}}>POI</option>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="to_val">To</label>
                                                                            <select class="form-control custom-select2 to_val" name="to_val[]" style="width:100%;">
                                                                                <option value="">Select To</option>
                                                                                <option value="module" {{ $electrical_string->to_val=='module' ? "selected":""}}>Module</option>
                                                                                <option value="inverter" {{ $electrical_string->to_val=='inverter' ? "selected":""}}>Inverter</option>
                                                                                <option value="pv_meter" {{ $electrical_string->to_val=='pv_meter' ? "selected":""}}>Pv Meter</option>
                                                                                <option value="ac_disconnect"  @if($type == 'AC' && ($electrical_string->to_val == '' || $electrical_string->to_val == null)) @if(strtolower($electrical_string->inverter->InverterType->name) == 'string inverter') selected @endif @else {{ $electrical_string->to_val=='ac_disconnect' ? "selected":""}} @endif >Ac Disconnect</option>
                                                                                <option value="combiner"  {{ $electrical_string->to_val=='combiner' ? "selected":""}} >Combiner</option>
                                                                            </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="phase">Phase</label>
                                                                        <select class="form-control custom-select2 phase max-length-cal" name="phase[]" style="width:100%;">
                                                                            <option value="">Select phase</option>
                                                                            @if($type == 'DC')
                                                                            <option value="dc" @if($electrical_string->phase == '' || $electrical_string->phase == null) @if(strtolower($electrical_string->inverter->InverterType->name) == 'string inverter') selected @endif @else {{ $electrical_string->phase=='dc' ? "selected":""}} @endif>DC</option>
                                                                            @else
                                                                            <option value="dc"  {{ $electrical_string->phase=='dc' ? "selected":""}} >DC</option>
                                                                            @endif
                                                                            <option value="single_phase" {{ $electrical_string->phase=='single_phase' ? "selected":""}}>Single Phase</option>
                                                                            <option value="three_phase" {{ $electrical_string->phase=='three_phase' ? "selected":""}}>Three Phase</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="inverter_id">Voltage </label>
                                                                        @if($type == 'DC' && strtolower($electrical_string->inverter->InverterType->name) == 'string inverter' && strtolower($electrical_string->inverter->manufacturer->title) == 'solaredge')
                                                                        <input  type="text" class="form-control input-style2 voltage voltage_dc_solaredge_string_inverter max-length-cal" name="voltage[]"  value="@if($electrical_string->voltage_dc_solaredge_string_inverter == 0){{ $electrical_string->inverter->maximum_input_voltage }}@else{{ $electrical_string->voltage_dc_solaredge_string_inverter }}@endif">
                                                                        @endif

                                                                        @if($type == 'AC' && strtolower($electrical_string->inverter->InverterType->name) != 'microinverter')
                                                                        <input  type="text" class="form-control input-style2 voltage voltage_ac_string_inverter max-length-cal" name="voltage[]"  value="@if($electrical_string->voltage_ac_string_inverter == 0){{ $electrical_string->inverter->maximum_output_voltage }}@else{{ $electrical_string->voltage_ac_string_inverter }}@endif">
                                                                        @endif

                                                                        @if($type == 'DC' && strtolower($electrical_string->inverter->InverterType->name) == 'string inverter' && strtolower($electrical_string->inverter->manufacturer->title) != 'solaredge')
                                                                        @php
                                                                            $temp_correction = getTempratureCorrection($electrical_string->electrical->minimum_temp);
                                                                            if($electrical_string->voltage_dc_string_inverter == 0) {
                                                                                $voltage_dc_string_inverter = ($electrical_string->module->voc * $temp_correction * $electrical_string->size);
                                                                            }
                                                                            else {
                                                                                $voltage_dc_string_inverter = $electrical_string->voltage_dc_string_inverter;
                                                                            }
                                                                        @endphp
                                                                        <input  type="text" class="form-control input-style2  voltage voltage_dc_string_inverter max-length-cal" name="voltage[]"  value="{{ $voltage_dc_string_inverter }}">
                                                                        @if($electrical_string->warning_message_voltage_dc_string_inverter == null)
                                                                        @php
                                                                            $warning_message_voltage_dc_string_inverter = $electrical_string->warning_message_voltage_dc_string_inverter;
                                                                        @endphp
                                                                        @else
                                                                        @php
                                                                            $warning_message_voltage_dc_string_inverter = '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Voltage can not be over 600.';
                                                                        @endphp
                                                                        @endif

                                                                        <span class="warning_message_voltage_dc_string_inverter text-warning" @if($voltage_dc_string_inverter > 600) style="display:block;" @else  style="display:none;" @endif>{!! $warning_message_voltage_dc_string_inverter !!}</span>
                                                                        <input type="hidden" name="warning_message_voltage_dc_string_inverter[]" class="warning_message_voltage_dc_string_inverter" value="{{ $warning_message_voltage_dc_string_inverter }}" />

                                                                        @endif

                                                                        @if($type == 'DC' && strtolower($electrical_string->inverter->InverterType->name) == 'generac')
                                                                        @php
                                                                            $temp_correction = getTempratureCorrection($electrical_string->electrical->minimum_temp);
                                                                            if($electrical_string->voltage_dc_generac_inverter == 0) {
                                                                                $voltage_dc_generac_inverter = ($electrical_string->module->voc * $temp_correction * $electrical_string->size);
                                                                            }
                                                                            else {
                                                                                $voltage_dc_generac_inverter = $electrical_string->voltage_dc_generac_inverter;
                                                                            }

                                                                        @endphp
                                                                        <input  type="text" class="form-control input-style2 voltage voltage_dc_generac_inverter max-length-cal" name="voltage[]"  value="{{ $voltage_dc_generac_inverter }}">
                                                                        @if($electrical_string->warning_message_voltage_dc_generac_inverter == null)
                                                                        @php
                                                                            $warning_message_voltage_dc_generac_inverter = $electrical_string->warning_message_voltage_dc_generac_inverter;
                                                                        @endphp
                                                                        @else
                                                                        @php
                                                                            $warning_message_voltage_dc_generac_inverter = '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Voltage can not be over 420.';
                                                                        @endphp
                                                                        @endif

                                                                        <span class="warning_message_voltage_dc_generac_inverter text-warning" @if($voltage_dc_generac_inverter > 420) style="display:block;" @else style="display:none;" @endif>{!! $warning_message_voltage_dc_generac_inverter !!}</span>
                                                                        <input type="hidden" name="warning_message_voltage_dc_generac_inverter[]" class="warning_message_voltage_dc_generac_inverter" value="{{ $warning_message_voltage_dc_generac_inverter }}" />

                                                                        @endif



                                                                        @if(strtolower($electrical_string->inverter->InverterType->name) == 'microinverter')
                                                                        <select class="form-control custom-select2 voltage max-length-cal" name="voltage[]" style="width:100%;">
                                                                            <option value="">Select Voltage</option>
                                                                            <option value="240" @if($electrical_string->phase == '' || $electrical_string->phase == null) @if(strtolower($electrical_string->inverter->InverterType->name) == 'microinverter') selected @endif @else {{ $electrical_string->voltage=='240' ? "selected":""}} @endif>240</option>
                                                                            <option value="208" {{ $electrical_string->voltage=='208' ? "selected":""}}>208</option>
                                                                            <option value="480" {{ $electrical_string->voltage=='480' ? "selected":""}}>480</option>
                                                                            <option value="600" {{ $electrical_string->voltage=='600' ? "selected":""}}>600</option>
                                                                            <option value="1000" {{ $electrical_string->voltage=='1000' ? "selected":""}}>1000</option>
                                                                            <option value="1500" {{ $electrical_string->voltage=='1500' ? "selected":""}}>1500</option>

                                                                        </select>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="current" class="current-label">
                                                                            @if($type == 'DC' || $type == 'AC')
                                                                            Current (O/P Current x 1.25)
                                                                            @else
                                                                            Current
                                                                            @endif
                                                                        </label>
                                                                        @if($type == 'DC' && strtolower($electrical_string->inverter->InverterType->name) == 'string inverter' && strtolower($electrical_string->inverter->manufacturer->title) == 'solaredge')
                                                                        <select class="form-control custom-select2 current current_dc_solaredge_string_inverter max-length-cal" name="current[]" style="width:100%;">
                                                                            <option value="">Select Voltage</option>
                                                                            <option value="15" @if($electrical_string->current_dc_solaredge_string_inverter == '' || $electrical_string->current_dc_solaredge_string_inverter == null)  selected  @else {{ $electrical_string->current_dc_solaredge_string_inverter=='15' ? "selected":""}} @endif>15</option>
                                                                            <option value="18" {{ $electrical_string->current_dc_solaredge_string_inverter=='18' ? "selected":""}}>18</option>
                                                                        </select>
                                                                        @endif

                                                                        @if($type == 'AC' && strtolower($electrical_string->inverter->InverterType->name) != 'microinverter')
                                                                        <input  type="text" class="form-control input-style2 current current_ac_string_inverter max-length-cal" name="current[]"  value="@if($electrical_string->current_ac_string_inverter == null){{ $electrical_string->inverter->maximum_output_current * 1.25 }}@else{{ $electrical_string->current_ac_string_inverter }}@endif">
                                                                        @endif

                                                                        @if($type == 'DC' && strtolower($electrical_string->inverter->InverterType->name) == 'string inverter' && strtolower($electrical_string->inverter->manufacturer->title) != 'solaredge')
                                                                        @php

                                                                            if($electrical_string->current_dc_string_inverter == 0) {
                                                                                $current_dc_string_inverter = ($electrical_string->module->isc * 1.56);
                                                                            }
                                                                            else {
                                                                                $current_dc_string_inverter = $electrical_string->current_dc_string_inverter;
                                                                            }
                                                                        @endphp
                                                                        <input  type="text" class="form-control input-style2  current current_dc_string_inverter max-length-cal" name="current[]"  value="{{ $current_dc_string_inverter }}">

                                                                        @endif

                                                                        @if($type == 'DC' && strtolower($electrical_string->inverter->InverterType->name) == 'generac')
                                                                        @php

                                                                            if($electrical_string->current_dc_generac_inverter == 0) {
                                                                                $current_dc_generac_inverter = 8;
                                                                            }
                                                                            else {
                                                                                $current_dc_generac_inverter = $electrical_string->current_dc_generac_inverter;
                                                                            }

                                                                        @endphp
                                                                        <input  type="text" class="form-control input-style2 current current_dc_generac_inverter max-length-cal" name="current[]"  value="{{ $current_dc_generac_inverter }}">

                                                                        @endif



                                                                        @if(strtolower($electrical_string->inverter->InverterType->name) == 'microinverter')
                                                                        <input readonly type="text" class="form-control input-style2 current" name="current[]"  value="{{ $current }}">
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="terminal_ampacity">Terminal Ampacity Rating</label>
                                                                            <select class="form-control custom-select2 terminal_ampacity terminal-rating-cal" name="terminal_ampacity[]" style="width:100%;">
                                                                                <option value="75" {{ $electrical_string->terminal_ampacity==75 ? "selected":""}}>75</option>
                                                                                <option value="90" {{ $electrical_string->terminal_ampacity==90 ? "selected":""}}>90</option>
                                                                            </select>
                                                                            <span class="warning_message_terminal_ampacity text-warning">{!! $electrical_string->warning_message_terminal_ampacity !!}</span>
                                                                            <input type="hidden" name="warning_message_terminal_ampacity[]" class="warning_message_terminal_ampacity" value="{{ $electrical_string->warning_message_terminal_ampacity }}" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="wire_ampacity">Wire Ampacity Rating</label>
                                                                            <select class="form-control custom-select2 wire_ampacity conductor-ampacity-cal" name="wire_ampacity[]" style="width:100%;">
                                                                                <option value="75" {{ $electrical_string->wire_ampacity==75 ? "selected":""}}>75</option>
                                                                                <option value="90" @if(!$electrical_string->wire_ampacity) selected @endif {{ $electrical_string->wire_ampacity==90 ? "selected":""}}>90</option>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="no_of_conductors">No. of Conductors</label>
                                                                            <select class="form-control custom-select2 no_of_conductors conduit-derate-cal" name="no_of_conductors[]" style="width:100%;">
                                                                                @if($type == 'DC' && $electrical_string->string_id_duplicate == 0 && $electrical_string->no_of_conductors == 0)
                                                                                    <option value="2"  selected >2</option>
                                                                                @elseif($type == 'AC' && strtolower($electrical_string->inverter->InverterType->name) == 'string inverter' && $electrical_string->no_of_conductors == 0)
                                                                                    <option value="3"  selected >3</option>
                                                                                @elseif($type == 'AC' && strtolower($electrical_string->inverter->InverterType->name) == 'microinverter' && $electrical_string->no_of_conductors == 0)
                                                                                    <option value="2"  selected >2</option>
                                                                                @else
                                                                                <option value="2" {{ $electrical_string->no_of_conductors==2 ? "selected":""}}>2</option>
                                                                                <option value="3" {{ $electrical_string->no_of_conductors==3 ? "selected":""}}>3</option>
                                                                                <option value="4" {{ $electrical_string->no_of_conductors==4 ? "selected":""}}>4</option>
                                                                                @endif
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="sets_of_wire">Sets of Wires </label>
                                                                            <select class="form-control custom-select2 sets_of_wire conduit-derate-cal" name="sets_of_wire[]" style="width:100%;">
                                                                                @for($i=1; $i<=15; $i++)
                                                                                @if($type == 'DC' && $electrical_string->string_id_duplicate == 0 && $electrical_string->sets_of_wire == 0)
                                                                                <option value="{{ $i }}"  {{ 1 == $i ? "selected":""}} >{{ $i }}</option>
                                                                                @elseif($type == 'DC' && $electrical_string->string_id_duplicate > 0 && $electrical_string->sets_of_wire == 0)
                                                                                @php  $stringDuplicateNo = getStringDuplicateNo($electrical->id,$electrical_string->string_id_duplicate) + 1; @endphp
                                                                                <option value="{{ $i }}"  {{ $stringDuplicateNo == $i ? "selected":""}} >{{ $i }}</option>
                                                                                @elseif($type == 'AC' && strtolower($electrical_string->inverter->InverterType->name) == 'microinverter' && $electrical_string->sets_of_wire == 0)
                                                                                <option value="{{ $i }}"  {{ 1 == $i ? "selected":""}} >{{ $i }}</option>
                                                                                @else
                                                                                <option value="{{ $i }}" @if($electrical_string->sets_of_wire == 0 && $electrical_strings->where('type','string')->count() == $i) selected  @else {{ $electrical_string->sets_of_wire==$i ? "selected":""}} @endif>{{ $i }}</option>
                                                                                @endif
                                                                                @endfor
                                                                            </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="no_of_conduits">No. of Conduits</label>
                                                                        <select class="form-control custom-select2 no_of_conduits conduit-derate-cal" name="no_of_conduits[]" style="width:100%;">
                                                                            @for($i=1; $i<=10; $i++)
                                                                                @if($type == 'DC' && $electrical_string->string_id_duplicate == 0 && $electrical_string->no_of_conduits == 0)
                                                                                <option value="{{ $i }}" {{ 1==$i ? "selected":""}}>{{ $i }}</option>
                                                                                @elseif($type == 'AC' && strtolower($electrical_string->inverter->InverterType->name) == 'microinverter' && $electrical_string->no_of_conduits == 0)
                                                                                <option value="{{ $i }}" {{ 1==$i ? "selected":""}}>{{ $i }}</option>
                                                                                @else
                                                                                <option value="{{ $i }}" {{ $electrical_string->no_of_conduits==$i ? "selected":""}}>{{ $i }}</option>
                                                                                @endif
                                                                            @endfor
                                                                        </select>

                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="conduit_location">Conduit Location</label>

                                                                            <select  class="form-control custom-select2 conduit_location temp-derate-cal conductor-ampacity-cal" name="conduit_location[]" style="width:100%;">
                                                                                <option value="">Select Conduit Location</option>
                                                                                @foreach ($conduit_locations as $key => $value)
                                                                                <option value="{{ $value }}" {{ $electrical_string->conduit_location==$value ? "selected":""}}>{{ $key }}</option>
                                                                                @endforeach
                                                                                {{-- <option value="Free Air">Free Air</option>
                                                                                <option value="Inside Building">Inside Building</option>
                                                                                <option value="Exterior Building">Exterior Building</option>
                                                                                <option value="36">36</option>
                                                                                <option value="12.1">12.1</option>
                                                                                <option value="12">12</option>
                                                                                <option value="3.51">3.51</option>
                                                                                <option value="3.5">3.5</option>
                                                                                <option value="0.5">0.5</option>
                                                                                <option value="0">0</option> --}}
                                                                            </select>

                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="conductor_material">Conductor Material</label>
                                                                            <select  class="form-control custom-select2 conductor_material terminal-rating-cal conductor-ampacity-cal ground-wire-cal max-length-cal" name="conductor_material[]" style="width:100%;">
                                                                                <option value="Cu" {{ $electrical_string->conductor_material=='Cu' ? "selected":""}}>Cu</option>
                                                                                <option value="Al" {{ $electrical_string->conductor_material=="Al" ? "selected":""}}>Al</option>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="conduit_derate">Conduit Derate</label>
                                                                        <input readonly type="text" class="form-control input-style2 conduit_derate derated-ampacity-cal" name="conduit_derate[]"  value="{{ $electrical_string->conduit_derate  }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="temp_derate">Temp Derate</label>
                                                                        <input readonly type="text" class="form-control input-style2 temp_derate derated-ampacity-cal" name="temp_derate[]"  value="{{ $electrical_string->temp_derate  }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="wire_size">Wire Size</label>
                                                                            <select class="form-control custom-select2 wire_size terminal-rating-cal corrected-ground-wire-cal max-length-cal" name="wire_size[]" style="width:100%;">
                                                                                @foreach ($awjs as $key1 => $value1)
                                                                                @if($electrical_string->wire_size == 0)
                                                                                    <option value="{{ $value1 }}" {{ 10 == $value1 ? "selected":""}}>{{ $key1 }}</option>
                                                                                @else
                                                                                    <option value="{{ $value1 }}" {{ $electrical_string->wire_size == $value1 ? "selected":""}}>{{ $key1 }}</option>
                                                                                @endif
                                                                                @endforeach
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="corrected_wire_size">Corrected Wire Size</label>
                                                                            <select class="form-control custom-select2 corrected_wire_size conductor-ampacity-cal corrected-ground-wire-cal" name="corrected_wire_size[]" style="width:100%;">
                                                                                @foreach ($awjs as $key1 => $value1)
                                                                                @if($electrical_string->corrected_wire_size == 0)
                                                                                <option value="{{ $value1 }}" {{ 10 == $value1 ? "selected":""}}>{{ $key1 }}</option>
                                                                                @else
                                                                                <option value="{{ $value1 }}" {{ $electrical_string->corrected_wire_size == $value1 ? "selected":""}}>{{ $key1 }}</option>
                                                                                @endif
                                                                                @endforeach
                                                                            </select>
                                                                        <span class="warning_message text-warning">{!! $electrical_string->warning_message !!}</span>
                                                                    <input type="hidden" name="warning_message[]" class="warning_message" value="{{ $electrical_string->warning_message }}" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="conductor_ampacity">Conductor Ampacity</label>
                                                                        @php

                                                                            $ampacity_dropdown_data = $formula->ampacityRating($electrical_string->conduit_location,$electrical_string->corrected_wire_size, strtolower($electrical_string->conductor_material),$electrical_string->terminal_ampacity);
                                                                            $ampacity_dropdown = $ampacity_dropdown_data['ampacity_rating_dropdown_array'];
                                                                            // echo "<pre>";
                                                                            // print_r($ampacity_dropdown);exit;
                                                                        @endphp
                                                                            <select class="form-control custom-select2 conductor_ampacity derated-ampacity-cal" name="conductor_ampacity[]" style="width:100%;">
                                                                                <option value="">Select Conductor Ampacity</option>
                                                                                @if($ampacity_dropdown)
                                                                                @foreach($ampacity_dropdown as $val)
                                                                                    <option value="{{ $val }}" {{ $electrical_string->conductor_ampacity == $val ? "selected":""}}>{{ $val }}</option>
                                                                                @endforeach
                                                                                @endif
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="derated_ampacity">Derated Ampacity</label>
                                                                        <input readonly type="text" class="form-control input-style2 derated_ampacity" name="derated_ampacity[]"  value="{{ $electrical_string->derated_ampacity  }}">
                                                                        <span class="warning_message_ter text-warning">{!! $electrical_string->warning_message_ter !!}</span>
                                                                        <span class="warning_message_ocpd text-warning">{!! $electrical_string->warning_message_ocpd !!}</span>
                                                                        <input type="hidden" name="warning_message_ter[]" class="warning_message_ter" value="{{ $electrical_string->warning_message_ter }}" />
                                                                        <input type="hidden" name="warning_message_ocpd[]" class="warning_message_ocpd" value="{{ $electrical_string->warning_message_ocpd }}" />

                                                                    </div>
                                                                </div>
                                                                @php
                                                                $ocpd_next_value = nextLargestValue($ocpd_rating,$current);
                                                                @endphp
                                                               <div class="col-lg-2 col-md-2 col-sm-2">
                                                                   <div class="form-group">
                                                                       <label for="ocpd">OCPD</label>
                                                                           <select class="form-control custom-select2 ocpd ground-wire-cal" name="ocpd[]" style="width:100%;">
                                                                            <option value="0" >Select Ocpd</option>
                                                                            @foreach ($ocpd_rating as  $ocpd)
                                                                            @if($type == 'DC')
                                                                            <option value="{{ $ocpd }}" @if($electrical_string->ocpd) {{ $electrical_string->ocpd == $ocpd ? "selected":""}} @endif>{{ $ocpd }}</option>
                                                                            @elseif($type == 'AC')
                                                                            <option value="{{ $ocpd }}" @if($electrical_string->ocpd_ac) {{ $electrical_string->ocpd_ac == $ocpd ? "selected":""}} @endif>{{ $ocpd }}</option>
                                                                            @endif
                                                                               @endforeach
                                                                           </select>
                                                                   </div>
                                                               </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="ground_wire">Ground Wire</label>
                                                                                @php
                                                                                    $ground_wire_datas = $formula->ground_wire();
                                                                                    $value2 = $ground_wire_datas['value2'];
                                                                                    $value3 = $ground_wire_datas['value3'];
                                                                                @endphp
                                                                                @if(strtolower($electrical_string->conductor_material) == 'cu')
                                                                                    @php $ground_wire_dropdown = $value2; @endphp
                                                                                @elseif(strtolower($electrical_string->conductor_material) == 'al')
                                                                                    @php $ground_wire_dropdown = $value3; @endphp
                                                                                @endif
                                                                            <select class="form-control custom-select2 ground_wire corrected-ground-wire-cal" name="ground_wire[]" style="width:100%;">
                                                                                <option value="">Select Ground Wire</option>

                                                                                @if(isset($ground_wire_dropdown))
                                                                                @foreach($ground_wire_dropdown as $val)
                                                                                    @if($electrical_string->ground_wire == 0)
                                                                                    <option value="{{ $val }}" {{ 10 == $val ? "selected":""}}>{{ $val }}</option>
                                                                                    @else
                                                                                    @if($val === 1 || $val === 2 || $val === 3 || $val === 4 || $val === 6 || $val === 8 || $val === 10 || $val === 12 || $val === 14)
                                                                                    <option value="{{ $val }}" {{ $electrical_string->ground_wire == $val ? "selected":""}}>#{{ $val }}</option>
                                                                                    @else
                                                                                    <option value="{{ $val }}" {{ $electrical_string->ground_wire == $val ? "selected":""}}>{{ $val }}</option>
                                                                                    @endif
                                                                                    @endif
                                                                                @endforeach
                                                                                @endif
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="corrected_ground_wire">Corrected Ground Wire</label>
                                                                        <input readonly type="text" class="form-control input-style2 corrected_ground_wire" name="corrected_ground_wire[]"  value=" @if($electrical_string->corrected_ground_wire==0)10 @else {{ $electrical_string->corrected_ground_wire  }} @endif">
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="conductor_type">Conductor Type</label>
                                                                            <select class="form-control custom-select2 conductor_type" name="conductor_type[]" style="width:100%;">
                                                                                @foreach ($conductors as $conductor)
                                                                                <option value="{{ $conductor }}" >{{ $conductor }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="vd">Vd%</label>
                                                                            <select class="form-control custom-select2 vd max-length-cal" name="vd[]" style="width:100%;">
                                                                                <option value="2" {{ $electrical_string->conductor_type==2 ? "selected":""}}>2</option>
                                                                                <option value="3" {{ $electrical_string->conductor_type==3 ? "selected":""}}>3</option>
                                                                                <option value="4" {{ $electrical_string->conductor_type==4 ? "selected":""}}>4</option>
                                                                                <option value="5" {{ $electrical_string->conductor_type==5 ? "selected":""}}>5</option>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="max_length">Max Length</label>
                                                                        <input type="text" class="form-control input-style2 max_length" name="max_length[]"  value="{{ $electrical_string->max_length }}">

                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12 combiner-string-button-div" style="display: none;">
                                                                    <div class="form-group"><button class="btn new_btn btn-red w-20 btn-sm text-center  add-combiner-string-to-current " > Select String Values </button></div>
                                                                </div>
                                                                <div class="col-lg-12 optimizer-string-button-div" style="display: none;">
                                                                    <div class="form-group"><button class="btn new_btn btn-red w-20 btn-sm text-center  add-optimizer-string-to-current " > Select String Values </button></div>
                                                                </div>


                                                            </div>
                                                        </div>

                                                    </div>


                                            </div>
                                        </div>
                                    </div>
                                    @php
                                        $j++;
                                        $i++;
                                    @endphp
                                    @if($electrical_string->string_id_duplicate==0)
                                    @php
                                      $i_new++;
                                      $j_new = 0.1;
                                    @endphp
                                  @else
                                    @php
                                      $j_new = $j_new + 0.1;  
                                    @endphp
                                  @endif
                                    @endforeach
                                    <div id="combiner_div">
                                        @php
                                            $k = 100001;
                                        @endphp
                                    @foreach($electrical_strings->where('type','combiner') as $electrical_string)


                                        @php
                                            $current = $electrical_string->current;
                                        @endphp

                                        <div class="card">
                                        <div class="card-header" id="heading{{ $k }}" data-toggle="collapse"
                                            data-target="#collapse{{ $k }}" aria-expanded="true"
                                            aria-controls="collapse{{ $k }}">
                                            <h5 class="d-flex align-items-center mb-0">
                                                <span style="color: #A80D2C;" class="string_id"> AC</span> : <span  class="string_id_no">&nbsp; #{{ $loop->iteration }}</span>
                                                    <span class="string_checkbox">&nbsp;<input style="display: none;" name="string_checkbox_val[{{ $electrical_string->id }}]" id="{{ $electrical_string->id }}" data-string-id="{{ $electrical_string->id }}" class="string_checkbox_val @if(strtolower($electrical_string->inverter->InverterType->name) != 'generac') @if($electrical->CombinerString && in_array($electrical_string->string_checkbox_val,$electrical->CombinerString->pluck('electrical_string_id')->toArray())) selected @else notSelectedCombiner @endif @endif @if(strtolower($electrical_string->inverter->InverterType->name) == 'generac') @if($electrical->OptimizerString && in_array($electrical_string->string_checkbox_val,$electrical->OptimizerString->pluck('electrical_string_id')->toArray())) selected @else notSelectedOptimizer @endif @endif " type="checkbox" value="{{ $current }}"  /></span>
                                                    <span class="icon icon-accordian-2-add ml-auto"></span>
                                                    <span style="cursor:pointer;" class="combiner-edit text-primary" id="{{ $electrical_string->id }}"><i class="fas fa-edit"></i></span>
                                            </h5>
                                        </div>

                                        <div id="collapse{{ $k }}" class="collapse  cus_acro"
                                            aria-labelledby="heading{{ $k }}" data-parent="#accordion">
                                            <div class="card-body">

                                                    <input type="hidden" name="id[]" class="id" value="{{ $electrical_string->id }}" />
                                                    <input type="hidden" name="type[]" class="type" value="{{ $electrical_string->type }}" />
                                                    <input type="hidden" name="module_id[]" class="module_id" value="{{ $electrical_string->module_id}}" />
                                                    <input type="hidden" name="inverter_id[]" class="inverter_id" value="{{ $electrical_string->inverter_id }}" />
                                                    <input type="hidden" name="size[]" class="size" value="{{ $electrical_string->size }}" />
                                                    <div class="row roof-container">
                                                        <div class="col-md-12 mb-md-0 mb-5">
                                                            <div class="row">

                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="from_val">From </label>
                                                                            <select class="form-control custom-select2 from_val" name="from_val[]" style="width:100%;">
                                                                                <option value="">Select From</option>
                                                                                <option value="module" {{ $electrical_string->from_val=='module' ? "selected":""}}>Module</option>
                                                                                <option value="inverter" {{ $electrical_string->from_val=='inverter' ? "selected":""}}>Inverter</option>
                                                                                <option value="pv_meter" {{ $electrical_string->from_val=='pv_meter' ? "selected":""}}>Pv Meter</option>
                                                                                <option value="ac_disconnect" {{ $electrical_string->from_val=='ac_disconnect' ? "selected":""}}>Ac Disconnect</option>
                                                                                <option value="poi" {{ $electrical_string->from_val=='poi' ? "selected":""}}>POI</option>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="to_val">To</label>
                                                                            <select class="form-control custom-select2 to_val" name="to_val[]" style="width:100%;">
                                                                                <option value="">Select To</option>
                                                                                <option value="module" {{ $electrical_string->to_val=='module' ? "selected":""}}>Module</option>
                                                                                <option value="inverter" {{ $electrical_string->to_val=='inverter' ? "selected":""}}>Inverter</option>
                                                                                <option value="pv_meter" {{ $electrical_string->to_val=='pv_meter' ? "selected":""}}>Pv Meter</option>
                                                                                <option value="ac_disconnect" {{ $electrical_string->to_val=='ac_disconnect' ? "selected":""}}>Ac Disconnect</option>
                                                                                @if($electrical_string->type == 'combiner')
                                                                                <option value="combiner" {{ $electrical_string->to_val=='combiner' ? "selected":""}}>Combiner</option>
                                                                                @endif

                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="phase">Phase</label>
                                                                        <select class="form-control custom-select2 phase max-length-cal" name="phase[]" style="width:100%;">
                                                                            <option value="">Select phase</option>
                                                                            <option value="dc" @if($electrical_string->phase == '' || $electrical_string->phase == null) @if(strtolower($electrical_string->inverter->InverterType->name) == 'string inverter') selected @endif @else {{ $electrical_string->phase=='dc' ? "selected":""}} @endif>DC</option>
                                                                            <option value="single_phase" {{ $electrical_string->phase=='single_phase' ? "selected":""}}>Single Phase</option>
                                                                            <option value="three_phase" {{ $electrical_string->phase=='three_phase' ? "selected":""}}>Three Phase</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="inverter_id">Voltage</label>
                                                                        <select class="form-control custom-select2 voltage max-length-cal" name="voltage[]" style="width:100%;">
                                                                            <option value="">Select Voltage</option>
                                                                            <option value="240" @if($electrical_string->phase == '' || $electrical_string->phase == null) @if(strtolower($electrical_string->inverter->InverterType->name) == 'microinverter') selected @endif @else {{ $electrical_string->voltage=='240' ? "selected":""}} @endif>240</option>
                                                                            <option value="208" {{ $electrical_string->voltage=='208' ? "selected":""}}>208</option>
                                                                            <option value="480" {{ $electrical_string->voltage=='480' ? "selected":""}}>480</option>
                                                                            <option value="600" {{ $electrical_string->voltage=='600' ? "selected":""}}>600</option>
                                                                            <option value="1000" {{ $electrical_string->voltage=='1000' ? "selected":""}}>1000</option>
                                                                            <option value="1500" {{ $electrical_string->voltage=='1500' ? "selected":""}}>1500</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="current">Current</label>


                                                                        <input readonly type="text" class="form-control input-style2 current" name="current[]"  value="{{ $current }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="terminal_ampacity">Terminal Ampacity Rating</label>
                                                                            <select class="form-control custom-select2 terminal_ampacity terminal-rating-cal" name="terminal_ampacity[]" style="width:100%;">
                                                                                <option value="75" {{ $electrical_string->terminal_ampacity==75 ? "selected":""}}>75</option>
                                                                                <option value="90" {{ $electrical_string->terminal_ampacity==90 ? "selected":""}}>90</option>
                                                                            </select>
                                                                            <span class="warning_message_terminal_ampacity text-warning">{!! $electrical_string->warning_message_terminal_ampacity !!}</span>
                                                                            <input type="hidden" name="warning_message_terminal_ampacity[]" class="warning_message_terminal_ampacity" value="{{ $electrical_string->warning_message_terminal_ampacity }}" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="wire_ampacity">Wire Ampacity Rating</label>
                                                                            <select class="form-control custom-select2 wire_ampacity conductor-ampacity-cal" name="wire_ampacity[]" style="width:100%;">
                                                                                <option value="75" {{ $electrical_string->wire_ampacity==75 ? "selected":""}}>75</option>
                                                                                <option value="90" @if(!$electrical_string->wire_ampacity) selected @endif {{ $electrical_string->wire_ampacity==90 ? "selected":""}}>90</option>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="no_of_conductors">No. of Conductors</label>
                                                                            <select class="form-control custom-select2 no_of_conductors conduit-derate-cal" name="no_of_conductors[]" style="width:100%;">
                                                                                <option value="2" {{ $electrical_string->no_of_conductors==2 ? "selected":""}}>2</option>
                                                                                <option value="3" {{ $electrical_string->no_of_conductors==3 ? "selected":""}}>3</option>
                                                                                <option value="4" {{ $electrical_string->no_of_conductors==4 ? "selected":""}}>4</option>

                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="sets_of_wire">Sets of Wires</label>
                                                                            <select class="form-control custom-select2 sets_of_wire conduit-derate-cal" name="sets_of_wire[]" style="width:100%;">
                                                                                @for($i=1; $i<=15; $i++)
                                                                                <option value="{{ $i }}" @if($electrical_string->sets_of_wire == 0 && $electrical_strings->where('type','combiner')->count() == $i) selected  @else {{ $electrical_string->sets_of_wire==$i ? "selected":""}} @endif>{{ $i }}</option>
                                                                                @endfor
                                                                            </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="no_of_conduits">No. of Conduits</label>
                                                                        <select class="form-control custom-select2 no_of_conduits conduit-derate-cal" name="no_of_conduits[]" style="width:100%;">
                                                                            @for($i=1; $i<=10; $i++)
                                                                                <option value="{{ $i }}" {{ $electrical_string->sets_of_wire==$i ? "selected":""}}>{{ $i }}</option>
                                                                                @endfor
                                                                        </select>

                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="conduit_location">Conduit Location</label>

                                                                            <select  class="form-control custom-select2 conduit_location temp-derate-cal conductor-ampacity-cal" name="conduit_location[]" style="width:100%;">
                                                                                <option value="">Select Conduit Location</option>
                                                                                @foreach ($conduit_locations as $key => $value)
                                                                                <option value="{{ $value }}" {{ $electrical_string->conduit_location==$value ? "selected":""}}>{{ $key }}</option>
                                                                                @endforeach
                                                                                {{-- <option value="Free Air">Free Air</option>
                                                                                <option value="Inside Building">Inside Building</option>
                                                                                <option value="Exterior Building">Exterior Building</option>
                                                                                <option value="36">36</option>
                                                                                <option value="12.1">12.1</option>
                                                                                <option value="12">12</option>
                                                                                <option value="3.51">3.51</option>
                                                                                <option value="3.5">3.5</option>
                                                                                <option value="0.5">0.5</option>
                                                                                <option value="0">0</option> --}}
                                                                            </select>

                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="conductor_material">Conductor Material</label>
                                                                            <select  class="form-control custom-select2 conductor_material terminal-rating-cal conductor-ampacity-cal ground-wire-cal max-length-cal" name="conductor_material[]" style="width:100%;">
                                                                                <option value="Cu" {{ $electrical_string->conductor_material=='Cu' ? "selected":""}}>Cu</option>
                                                                                <option value="Al" {{ $electrical_string->conductor_material=="Al" ? "selected":""}}>Al</option>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="conduit_derate">Conduit Derate</label>
                                                                        <input type="text" class="form-control input-style2 conduit_derate derated-ampacity-cal" name="conduit_derate[]"  value="{{ $electrical_string->conduit_derate  }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="temp_derate">Temp Derate</label>
                                                                        <input type="text" class="form-control input-style2 temp_derate derated-ampacity-cal" name="temp_derate[]"  value="{{ $electrical_string->temp_derate  }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="wire_size">Wire Size</label>
                                                                            <select class="form-control custom-select2 wire_size terminal-rating-cal corrected-ground-wire-cal max-length-cal" name="wire_size[]" style="width:100%;">
                                                                                @foreach ($awjs as $key1 => $value1)
                                                                                <option value="{{ $value1 }}" {{ $electrical_string->wire_size == $value1 ? "selected":""}}>{{ $key1 }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="corrected_wire_size">Corrected Wire Size</label>
                                                                            <select class="form-control custom-select2 corrected_wire_size conductor-ampacity-cal corrected-ground-wire-cal" name="corrected_wire_size[]" style="width:100%;">
                                                                                @foreach ($awjs as $key1 => $value1)
                                                                                <option value="{{ $value1 }}" {{ $electrical_string->corrected_wire_size == $value1 ? "selected":""}}>{{ $key1 }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        <span class="warning_message text-warning">{!! $electrical_string->warning_message !!}</span>
                                                                    <input type="hidden" name="warning_message[]" class="warning_message" value="{{ $electrical_string->warning_message }}" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="conductor_ampacity">Conductor Ampacity</label>
                                                                        @php

                                                                            $ampacity_dropdown_data = $formula->ampacityRating($electrical_string->conduit_location,$electrical_string->corrected_wire_size, strtolower($electrical_string->conductor_material),$electrical_string->terminal_ampacity);
                                                                            $ampacity_dropdown = $ampacity_dropdown_data['ampacity_rating_dropdown_array'];
                                                                            // echo "<pre>";
                                                                            // print_r($ampacity_dropdown);exit;
                                                                        @endphp
                                                                            <select class="form-control custom-select2 conductor_ampacity derated-ampacity-cal" name="conductor_ampacity[]" style="width:100%;">
                                                                                <option value="">Select Conductor Ampacity</option>
                                                                                @if($ampacity_dropdown)
                                                                                @foreach($ampacity_dropdown as $val)
                                                                                    <option value="{{ $val }}" {{ $electrical_string->conductor_ampacity == $val ? "selected":""}}>{{ $val }}</option>
                                                                                @endforeach
                                                                                @endif
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="derated_ampacity">Derated Ampacity</label>
                                                                        <input type="text" class="form-control input-style2 derated_ampacity" name="derated_ampacity[]"  value="{{ $electrical_string->derated_ampacity  }}">
                                                                        <span class="warning_message_ter text-warning">{!! $electrical_string->warning_message_ter !!}</span>
                                                                        <span class="warning_message_ocpd text-warning">{!! $electrical_string->warning_message_ocpd !!}</span>
                                                                        <input type="hidden" name="warning_message_ter[]" class="warning_message_ter" value="{{ $electrical_string->warning_message_ter }}" />
                                                                        <input type="hidden" name="warning_message_ocpd[]" class="warning_message_ocpd" value="{{ $electrical_string->warning_message_ocpd }}" />

                                                                    </div>
                                                                </div>
                                                                @php
                                                                $ocpd_next_value = nextLargestValue($ocpd_rating,$current);
                                                                @endphp
                                                               <div class="col-lg-2 col-md-2 col-sm-2">
                                                                   <div class="form-group">
                                                                       <label for="ocpd">OCPD</label>
                                                                           <select class="form-control custom-select2 ocpd ground-wire-cal" name="ocpd[]" style="width:100%;">
                                                                               @foreach ($ocpd_rating as  $ocpd)
                                                                               <option value="{{ $ocpd }}" @if($electrical_string->ocpd) {{ $electrical_string->ocpd == $ocpd ? "selected":""}} @else {{ $ocpd == $ocpd_next_value ? "selected":""}} @endif>{{ $ocpd }}</option>
                                                                               @endforeach
                                                                           </select>
                                                                   </div>
                                                               </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="ground_wire">Ground Wire</label>
                                                                                @php
                                                                                    $ground_wire_datas = $formula->ground_wire();
                                                                                    $value2 = $ground_wire_datas['value2'];
                                                                                    $value3 = $ground_wire_datas['value3'];
                                                                                @endphp
                                                                                @if(strtolower($electrical_string->conductor_material) == 'cu')
                                                                                    @php $ground_wire_dropdown = $value2; @endphp
                                                                                @elseif(strtolower($electrical_string->conductor_material) == 'al')
                                                                                    @php $ground_wire_dropdown = $value3; @endphp
                                                                                @endif
                                                                            <select class="form-control custom-select2 ground_wire corrected-ground-wire-cal" name="ground_wire[]" style="width:100%;">
                                                                                <option value="">Select Ground Wire</option>

                                                                                @if(isset($ground_wire_dropdown))
                                                                                @foreach($ground_wire_dropdown as $val)
                                                                                    @if($val === 1 || $val === 2 || $val === 3 || $val === 4 || $val === 6 || $val === 8 || $val === 10 || $val === 12 || $val === 14)
                                                                                    <option value="{{ $val }}" {{ $electrical_string->ground_wire == $val ? "selected":""}}>#{{ $val }}</option>
                                                                                    @else
                                                                                    <option value="{{ $val }}" {{ $electrical_string->ground_wire == $val ? "selected":""}}>{{ $val }}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                                @endif
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="corrected_ground_wire">Corrected Ground Wire</label>
                                                                        <input readonly type="text" class="form-control input-style2 corrected_ground_wire" name="corrected_ground_wire[]"  value="{{ $electrical_string->corrected_ground_wire  }}">
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="conductor_type">Conductor Type</label>
                                                                            <select class="form-control custom-select2 conductor_type" name="conductor_type[]" style="width:100%;">
                                                                                @foreach ($conductors as $conductor)
                                                                                <option value="{{ $conductor }}" >{{ $conductor }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="vd">Vd%</label>
                                                                            <select class="form-control custom-select2 vd max-length-cal" name="vd[]" style="width:100%;">
                                                                                <option value="2" {{ $electrical_string->conductor_type==2 ? "selected":""}}>2</option>
                                                                                <option value="3" {{ $electrical_string->conductor_type==3 ? "selected":""}}>3</option>
                                                                                <option value="4" {{ $electrical_string->conductor_type==4 ? "selected":""}}>4</option>
                                                                                <option value="5" {{ $electrical_string->conductor_type==5 ? "selected":""}}>5</option>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="max_length">Max Length</label>
                                                                        <input type="text" class="form-control input-style2 max_length" name="max_length[]"  value="{{ $electrical_string->max_length }}">

                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12 combiner-string-button-div" style="display: none;">
                                                                    <div class="form-group"><button class="btn new_btn btn-red w-20 btn-sm text-center  add-combiner-string-to-current " > Select String Values </button></div>
                                                                </div>



                                                            </div>
                                                        </div>

                                                    </div>


                                            </div>
                                        </div>
                                    </div>
                                    @php
                                        $k++;
                                    @endphp
                                    @endforeach
                                    </div>
                                   
                                    <div id="optimizer_div">
                                    @php
                                        $m = 500001;
                                    @endphp
                                    @foreach($electrical_strings->where('type','optimizer') as $electrical_string)
                                    
                                    
                                        @php
                                            $current = $electrical_string->current;
                                        @endphp
                                    
                                        <div class="card">
                                        <div class="card-header" id="heading{{ $m }}" data-toggle="collapse"
                                            data-target="#collapse{{ $m }}" aria-expanded="true"
                                            aria-controls="collapse{{ $m }}">
                                            <h5 class="d-flex align-items-center mb-0">
                                                <span style="color: #A80D2C;" class="string_id"> Optimizer</span> : <span  class="string_id_no">&nbsp; #{{ $loop->iteration }}</span>
                                                    <span class="string_checkbox">&nbsp;<input style="display: none;" name="string_checkbox_val[{{ $electrical_string->id }}]" id="{{ $electrical_string->id }}" data-string-id="{{ $electrical_string->id }}" class="string_checkbox_val @if(strtolower($electrical_string->inverter->InverterType->name) != 'generac') @if($electrical->CombinerString && in_array($electrical_string->string_checkbox_val,$electrical->CombinerString->pluck('electrical_string_id')->toArray())) selected @else notSelectedCombiner @endif @endif @if(strtolower($electrical_string->inverter->InverterType->name) == 'generac') @if($electrical->OptimizerString && in_array($electrical_string->string_checkbox_val,$electrical->OptimizerString->pluck('electrical_string_id')->toArray())) selected @else notSelectedOptimizer @endif @endif " type="checkbox" value="{{ $current }}"  /></span>
                                                    <span class="icon icon-accordian-2-add ml-auto"></span>
                                                    <span style="cursor:pointer;" class="optimizer-edit text-primary" id="{{ $electrical_string->id }}"><i class="fas fa-edit"></i></span>
                                            </h5>
                                        </div>
                                    
                                        <div id="collapse{{ $m }}" class="collapse  cus_acro"
                                            aria-labelledby="heading{{ $m }}" data-parent="#accordion">
                                            <div class="card-body">
                                    
                                                    <input type="hidden" name="id[]" class="id" value="{{ $electrical_string->id }}" />
                                                    <input type="hidden" name="type[]" class="type" value="{{ $electrical_string->type }}" />
                                                    <input type="hidden" name="module_id[]" class="module_id" value="{{ $electrical_string->module_id}}" />
                                                    <input type="hidden" name="inverter_id[]" class="inverter_id" value="{{ $electrical_string->inverter_id }}" />
                                                    <input type="hidden" name="size[]" class="size" value="{{ $electrical_string->size }}" />
                                                    <div class="row roof-container">
                                                        <div class="col-md-12 mb-md-0 mb-5">
                                                            <div class="row">
                                    
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="from_val">From </label>
                                                                            <select class="form-control custom-select2 from_val" name="from_val[]" style="width:100%;">
                                                                                <option value="">Select From</option>
                                                                                <option value="module" {{ $electrical_string->from_val=='module' ? "selected":""}}>Module</option>
                                                                                <option value="inverter" {{ $electrical_string->from_val=='inverter' ? "selected":""}}>Inverter</option>
                                                                                <option value="pv_meter" {{ $electrical_string->from_val=='pv_meter' ? "selected":""}}>Pv Meter</option>
                                                                                <option value="ac_disconnect" {{ $electrical_string->from_val=='ac_disconnect' ? "selected":""}}>Ac Disconnect</option>
                                                                                <option value="poi" {{ $electrical_string->from_val=='poi' ? "selected":""}}>POI</option>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="to_val">To</label>
                                                                            <select class="form-control custom-select2 to_val" name="to_val[]" style="width:100%;">
                                                                                <option value="">Select To</option>
                                                                                <option value="module" {{ $electrical_string->to_val=='module' ? "selected":""}}>Module</option>
                                                                                <option value="inverter" {{ $electrical_string->to_val=='inverter' ? "selected":""}}>Inverter</option>
                                                                                <option value="pv_meter" {{ $electrical_string->to_val=='pv_meter' ? "selected":""}}>Pv Meter</option>
                                                                                <option value="ac_disconnect" {{ $electrical_string->to_val=='ac_disconnect' ? "selected":""}}>Ac Disconnect</option>
                                                                                <option value="combiner" {{ $electrical_string->to_val=='combiner' ? "selected":""}}>Combiner</option>
                                                                               
                                    
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="phase">Phase</label>
                                                                        <select class="form-control custom-select2 phase max-length-cal" name="phase[]" style="width:100%;">
                                                                            <option value="">Select phase</option>
                                                                            <option value="dc" @if($electrical_string->phase == '' || $electrical_string->phase == null) @if(strtolower($electrical_string->inverter->InverterType->name) == 'string inverter') selected @endif @else {{ $electrical_string->phase=='dc' ? "selected":""}} @endif>DC</option>
                                                                            <option value="single_phase" {{ $electrical_string->phase=='single_phase' ? "selected":""}}>Single Phase</option>
                                                                            <option value="three_phase" {{ $electrical_string->phase=='three_phase' ? "selected":""}}>Three Phase</option>
                                    
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="inverter_id">Voltage</label>
                                                                       
                                                                        @if($type == 'DC' && strtolower($electrical_string->inverter->InverterType->name) == 'generac')
                                                                        @php
                                                                            $temp_correction = getTempratureCorrection($electrical_string->electrical->minimum_temp);
                                                                            if($electrical_string->voltage_dc_generac_inverter == 0) {
                                                                                $voltage_dc_generac_inverter = ($electrical_string->module->voc * $temp_correction * $electrical_string->size);
                                                                            }
                                                                            else {
                                                                                $voltage_dc_generac_inverter = $electrical_string->voltage_dc_generac_inverter;
                                                                            }

                                                                        @endphp
                                                                        <input  type="text" class="form-control input-style2 voltage voltage_dc_generac_inverter max-length-cal" name="voltage[]"  value="{{ $voltage_dc_generac_inverter }}">
                                                                        @if($electrical_string->warning_message_voltage_dc_generac_inverter == null)
                                                                        @php
                                                                            $warning_message_voltage_dc_generac_inverter = $electrical_string->warning_message_voltage_dc_generac_inverter;
                                                                        @endphp
                                                                        @else
                                                                        @php
                                                                            $warning_message_voltage_dc_generac_inverter = '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Voltage can not be over 420.';
                                                                        @endphp
                                                                        @endif

                                                                        <span class="warning_message_voltage_dc_generac_inverter text-warning" @if($voltage_dc_generac_inverter > 420) style="display:block;" @else style="display:none;" @endif>{!! $warning_message_voltage_dc_generac_inverter !!}</span>
                                                                        <input type="hidden" name="warning_message_voltage_dc_generac_inverter[]" class="warning_message_voltage_dc_generac_inverter" value="{{ $warning_message_voltage_dc_generac_inverter }}" />

                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="current">Current</label>
                                    
                                    
                                                                        <input readonly type="text" class="form-control input-style2 current" name="current[]"  value="{{ $current }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="terminal_ampacity">Terminal Ampacity Rating</label>
                                                                            <select class="form-control custom-select2 terminal_ampacity terminal-rating-cal" name="terminal_ampacity[]" style="width:100%;">
                                                                                <option value="75" {{ $electrical_string->terminal_ampacity==75 ? "selected":""}}>75</option>
                                                                                <option value="90" {{ $electrical_string->terminal_ampacity==90 ? "selected":""}}>90</option>
                                                                            </select>
                                                                            <span class="warning_message_terminal_ampacity text-warning">{!! $electrical_string->warning_message_terminal_ampacity !!}</span>
                                                                            <input type="hidden" name="warning_message_terminal_ampacity[]" class="warning_message_terminal_ampacity" value="{{ $electrical_string->warning_message_terminal_ampacity }}" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="wire_ampacity">Wire Ampacity Rating</label>
                                                                            <select class="form-control custom-select2 wire_ampacity conductor-ampacity-cal" name="wire_ampacity[]" style="width:100%;">
                                                                                <option value="75" {{ $electrical_string->wire_ampacity==75 ? "selected":""}}>75</option>
                                                                                <option value="90" @if(!$electrical_string->wire_ampacity) selected @endif {{ $electrical_string->wire_ampacity==90 ? "selected":""}}>90</option>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="no_of_conductors">No. of Conductors</label>
                                                                            <select class="form-control custom-select2 no_of_conductors conduit-derate-cal" name="no_of_conductors[]" style="width:100%;">
                                                                                <option value="2" {{ $electrical_string->no_of_conductors==2 ? "selected":""}}>2</option>
                                                                                <option value="3" {{ $electrical_string->no_of_conductors==3 ? "selected":""}}>3</option>
                                                                                <option value="4" {{ $electrical_string->no_of_conductors==4 ? "selected":""}}>4</option>
                                    
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="sets_of_wire">Sets of Wires</label>
                                                                            <select class="form-control custom-select2 sets_of_wire conduit-derate-cal" name="sets_of_wire[]" style="width:100%;">
                                                                                @for($i=1; $i<=15; $i++)
                                                                                <option value="{{ $i }}" @if($electrical_string->sets_of_wire == 0 && $electrical_strings->where('type','optimizer')->count() == $i) selected  @else {{ $electrical_string->sets_of_wire==$i ? "selected":""}} @endif>{{ $i }}</option>
                                                                                @endfor
                                                                            </select>
                                                                    </div>
                                                                </div>
                                    
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="no_of_conduits">No. of Conduits</label>
                                                                        <select class="form-control custom-select2 no_of_conduits conduit-derate-cal" name="no_of_conduits[]" style="width:100%;">
                                                                            @for($i=1; $i<=10; $i++)
                                                                                <option value="{{ $i }}" {{ $electrical_string->sets_of_wire==$i ? "selected":""}}>{{ $i }}</option>
                                                                                @endfor
                                                                        </select>
                                    
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="conduit_location">Conduit Location</label>
                                    
                                                                            <select  class="form-control custom-select2 conduit_location temp-derate-cal conductor-ampacity-cal" name="conduit_location[]" style="width:100%;">
                                                                                <option value="">Select Conduit Location</option>
                                                                                @foreach ($conduit_locations as $key => $value)
                                                                                <option value="{{ $value }}" {{ $electrical_string->conduit_location==$value ? "selected":""}}>{{ $key }}</option>
                                                                                @endforeach
                                                                                {{-- <option value="Free Air">Free Air</option>
                                                                                <option value="Inside Building">Inside Building</option>
                                                                                <option value="Exterior Building">Exterior Building</option>
                                                                                <option value="36">36</option>
                                                                                <option value="12.1">12.1</option>
                                                                                <option value="12">12</option>
                                                                                <option value="3.51">3.51</option>
                                                                                <option value="3.5">3.5</option>
                                                                                <option value="0.5">0.5</option>
                                                                                <option value="0">0</option> --}}
                                                                            </select>
                                    
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="conductor_material">Conductor Material</label>
                                                                            <select  class="form-control custom-select2 conductor_material terminal-rating-cal conductor-ampacity-cal ground-wire-cal max-length-cal" name="conductor_material[]" style="width:100%;">
                                                                                <option value="Cu" {{ $electrical_string->conductor_material=='Cu' ? "selected":""}}>Cu</option>
                                                                                <option value="Al" {{ $electrical_string->conductor_material=="Al" ? "selected":""}}>Al</option>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="conduit_derate">Conduit Derate</label>
                                                                        <input type="text" class="form-control input-style2 conduit_derate derated-ampacity-cal" name="conduit_derate[]"  value="{{ $electrical_string->conduit_derate  }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="temp_derate">Temp Derate</label>
                                                                        <input type="text" class="form-control input-style2 temp_derate derated-ampacity-cal" name="temp_derate[]"  value="{{ $electrical_string->temp_derate  }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="wire_size">Wire Size</label>
                                                                            <select class="form-control custom-select2 wire_size terminal-rating-cal corrected-ground-wire-cal max-length-cal" name="wire_size[]" style="width:100%;">
                                                                                @foreach ($awjs as $key1 => $value1)
                                                                                <option value="{{ $value1 }}" {{ $electrical_string->wire_size == $value1 ? "selected":""}}>{{ $key1 }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="corrected_wire_size">Corrected Wire Size</label>
                                                                            <select class="form-control custom-select2 corrected_wire_size conductor-ampacity-cal corrected-ground-wire-cal" name="corrected_wire_size[]" style="width:100%;">
                                                                                @foreach ($awjs as $key1 => $value1)
                                                                                <option value="{{ $value1 }}" {{ $electrical_string->corrected_wire_size == $value1 ? "selected":""}}>{{ $key1 }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        <span class="warning_message text-warning">{!! $electrical_string->warning_message !!}</span>
                                                                    <input type="hidden" name="warning_message[]" class="warning_message" value="{{ $electrical_string->warning_message }}" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="conductor_ampacity">Conductor Ampacity</label>
                                                                        @php
                                    
                                                                            $ampacity_dropdown_data = $formula->ampacityRating($electrical_string->conduit_location,$electrical_string->corrected_wire_size, strtolower($electrical_string->conductor_material),$electrical_string->terminal_ampacity);
                                                                            $ampacity_dropdown = $ampacity_dropdown_data['ampacity_rating_dropdown_array'];
                                                                            // echo "<pre>";
                                                                            // print_r($ampacity_dropdown);exit;
                                                                        @endphp
                                                                            <select class="form-control custom-select2 conductor_ampacity derated-ampacity-cal" name="conductor_ampacity[]" style="width:100%;">
                                                                                <option value="">Select Conductor Ampacity</option>
                                                                                @if($ampacity_dropdown)
                                                                                @foreach($ampacity_dropdown as $val)
                                                                                    <option value="{{ $val }}" {{ $electrical_string->conductor_ampacity == $val ? "selected":""}}>{{ $val }}</option>
                                                                                @endforeach
                                                                                @endif
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="derated_ampacity">Derated Ampacity</label>
                                                                        <input type="text" class="form-control input-style2 derated_ampacity" name="derated_ampacity[]"  value="{{ $electrical_string->derated_ampacity  }}">
                                                                        <span class="warning_message_ter text-warning">{!! $electrical_string->warning_message_ter !!}</span>
                                                                        <span class="warning_message_ocpd text-warning">{!! $electrical_string->warning_message_ocpd !!}</span>
                                                                        <input type="hidden" name="warning_message_ter[]" class="warning_message_ter" value="{{ $electrical_string->warning_message_ter }}" />
                                                                        <input type="hidden" name="warning_message_ocpd[]" class="warning_message_ocpd" value="{{ $electrical_string->warning_message_ocpd }}" />
                                    
                                                                    </div>
                                                                </div>
                                                                @php
                                                                $ocpd_next_value = nextLargestValue($ocpd_rating,$current);
                                                                @endphp
                                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                                                <div class="form-group">
                                                                    <label for="ocpd">OCPD</label>
                                                                        <select class="form-control custom-select2 ocpd ground-wire-cal" name="ocpd[]" style="width:100%;">
                                                                            @foreach ($ocpd_rating as  $ocpd)
                                                                            <option value="{{ $ocpd }}" @if($electrical_string->ocpd) {{ $electrical_string->ocpd == $ocpd ? "selected":""}} @else {{ $ocpd == $ocpd_next_value ? "selected":""}} @endif>{{ $ocpd }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                </div>
                                                            </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="ground_wire">Ground Wire</label>
                                                                                @php
                                                                                    $ground_wire_datas = $formula->ground_wire();
                                                                                    $value2 = $ground_wire_datas['value2'];
                                                                                    $value3 = $ground_wire_datas['value3'];
                                                                                @endphp
                                                                                @if(strtolower($electrical_string->conductor_material) == 'cu')
                                                                                    @php $ground_wire_dropdown = $value2; @endphp
                                                                                @elseif(strtolower($electrical_string->conductor_material) == 'al')
                                                                                    @php $ground_wire_dropdown = $value3; @endphp
                                                                                @endif
                                                                            <select class="form-control custom-select2 ground_wire corrected-ground-wire-cal" name="ground_wire[]" style="width:100%;">
                                                                                <option value="">Select Ground Wire</option>
                                    
                                                                                @if(isset($ground_wire_dropdown))
                                                                                @foreach($ground_wire_dropdown as $val)
                                                                                    @if($val === 1 || $val === 2 || $val === 3 || $val === 4 || $val === 6 || $val === 8 || $val === 10 || $val === 12 || $val === 14)
                                                                                    <option value="{{ $val }}" {{ $electrical_string->ground_wire == $val ? "selected":""}}>#{{ $val }}</option>
                                                                                    @else
                                                                                    <option value="{{ $val }}" {{ $electrical_string->ground_wire == $val ? "selected":""}}>{{ $val }}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                                @endif
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="corrected_ground_wire">Corrected Ground Wire</label>
                                                                        <input readonly type="text" class="form-control input-style2 corrected_ground_wire" name="corrected_ground_wire[]"  value="{{ $electrical_string->corrected_ground_wire  }}">
                                                                    </div>
                                                                </div>
                                    
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="conductor_type">Conductor Type</label>
                                                                            <select class="form-control custom-select2 conductor_type" name="conductor_type[]" style="width:100%;">
                                                                                @foreach ($conductors as $conductor)
                                                                                <option value="{{ $conductor }}" >{{ $conductor }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="vd">Vd%</label>
                                                                            <select class="form-control custom-select2 vd max-length-cal" name="vd[]" style="width:100%;">
                                                                                <option value="2" {{ $electrical_string->conductor_type==2 ? "selected":""}}>2</option>
                                                                                <option value="3" {{ $electrical_string->conductor_type==3 ? "selected":""}}>3</option>
                                                                                <option value="4" {{ $electrical_string->conductor_type==4 ? "selected":""}}>4</option>
                                                                                <option value="5" {{ $electrical_string->conductor_type==5 ? "selected":""}}>5</option>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="max_length">Max Length</label>
                                                                        <input type="text" class="form-control input-style2 max_length" name="max_length[]"  value="{{ $electrical_string->max_length }}">
                                    
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12 optimizer-string-button-div" style="display: none;">
                                                                    <div class="form-group"><button class="btn new_btn btn-red w-20 btn-sm text-center  add-optimizer-string-to-current " > Select String Values </button></div>
                                                                </div>
                                    
                                    
                                    
                                                            </div>
                                                        </div>
                                    
                                                    </div>
                                    
                                    
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                        $m++;
                                    @endphp
                                    @endforeach
                                    
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                          <div class="col-lg-12 mt-2 text-lg text-center text-danger combiner-current-div mb-3">

                                          </div>
                                          @if($type != 'AC')
                                          @if(in_array(3,$inverterIds)) 
                                          <div class="col-lg-4 mt-2 text-lg text-center">
                                            <button id=""  class="btn new_btn btn-red w-50 text-center add-optimizer @if(count($electrical_strings) ==  0) d-none @endif " > Add Optimizer </button>
                                          </div>
                                          @endif
                                          <div class="col-lg-4 mt-2 text-lg text-center">
                                            <button id=""  class="btn new_btn btn-red w-50 text-center add-combiner  @if(count($electrical_strings) ==  0) d-none @endif" > Add Combiner </button>
                                          </div>
                                          <div class="col-lg-4 mt-2 text-lg text-center">
                                            <button id="create-string-info" type="submit" class="btn new_btn btn-red w-50 text-center create-string-info @if(count($electrical_strings) ==  0) d-none @endif" > Save </button>
                                          </div>
                                          @else 
                                          <div class="col-lg-3 mt-2 text-lg text-center">
                                            <button id=""  class="btn new_btn btn-red w-50 text-center add-combiner  @if(count($electrical_strings) ==  0) d-none @endif" > Add Combiner </button>
                                          </div>
                                          <div class="col-lg-3 mt-2 text-lg text-center">
                                            <button id="create-string-info" type="submit" class="btn new_btn btn-red w-50 text-center create-string-info @if(count($electrical_strings) ==  0) d-none @endif" > Save </button>
                                          </div>
                                          <div class="col-lg-3 mt-2 text-lg text-center">
                                            <a id="create-string-info" type="submit" class="btn new_btn btn-red w-50 text-center"  href="{{route('block.design',[$electrical_strings[0]->electrical->project_id])}}"> Block Design </a>
                                          </div>
                                          <div class="col-lg-3 mt-2 text-lg text-center">
                                            <button type="submit" class="btn new_btn btn-red w-50 text-center" > Print Pdf </button>
                                          </div>
                                          @endif
                                          
                                        </div>
                                    </div>
                                    <input type="hidden" id="combiner_id" name="combiner_id" class="combiner-id" value="0" />
                                    <input type="hidden" id="optimizer_id" name="optimizer_id" class="optimizer-id" value="0" />
                                    <input type="hidden" id="add_type" name="add_type" class="add_type" value="0" />

                                    </form>
                                    @endif
                                    <input type="hidden" id="combiner_current_values" class="combiner-current-values" value="0" />
                                    <input type="hidden" id="combiner_set_of_wire_values" class="combiner-set-of-wire-values" value="0" />
                                    <input type="hidden" id="optimizer_current_values" class="optimizer-current-values" value="0" />
                                    <input type="hidden" id="optimizer_set_of_wire_values" class="optimizer-set-of-wire-values" value="0" />
                                    {{-- <div class="card">
                                        <div class="card-header collapsed" id="headingTwo"
                                            data-toggle="collapse" data-target="#collapseTwo"
                                            aria-expanded="true" aria-controls="collapseTwo">
                                            <h5 class="d-flex align-items-center mb-0">String ID : 2 <span
                                                    class="icon icon-accordian-2-add ml-auto"></span></h5>
                                        </div>

                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                            data-parent="#accordion">
                                            <div class="card-body">
                                                <p>lorem ipsum</p>
                                            </div>
                                        </div>
                                    </div> --}}

                                    {{-- <div class="card">
                                        <div class="card-header collapsed" id="headingThree"
                                            data-toggle="collapse" data-target="#collapseThree"
                                            aria-expanded="true" aria-controls="collapseThree">
                                            <h5 class="d-flex align-items-center mb-0">String ID : 3 <span
                                                    class="icon icon-accordian-2-add ml-auto"></span></h5>
                                        </div>

                                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                            data-parent="#accordion">
                                            <div class="card-body">
                                                <p>lorem ipsum</p>
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>










            </div>


        </div>
    </div>
</section>

</div>
@push('scripts')
<script src="{{asset('front/js/electrical_string/event.js')}}"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script> --}}
<script>

</script>

@endpush
@endsection
