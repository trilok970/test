@extends('front.layouts.app')
@section('title',$data['page_title'])
@section('content')

<div class="body-section body-login">

<!-- Inner Banner -->
<section class="inner-bnr"></section>

<!-- Recent Projects section -->
<section class="recent-projects-sec">
  <div class="container-fluid">
    <div class="d-flex align-items-center title-sec">
      <h5>{{ $data['page_title'] }}</h5>

    </div>
    <div id="accordion" class="accordian-collapse">
    @include('front.includes.message')
      @if($inverters)
      @foreach($inverters as $key => $value)
      <div class="card">
        <div class="card-header">
            <h5 class="d-flex align-items-center">
                <span class="owner-name"> Inverter Name: </span>&nbsp;{{$value->model_no}} <button class="ml-auto" id="myBtn"><span class="icon icon-accordian-add morelink"></span></button>
            </h5>

          <div class="col-12 edit-ssection">
	          <div class="more">
	            <p>
                    <b>Manufacturer:</b> {{$value->manufacturer->title ?? ""}}
                    <b class="ml-2">| Maximum Input Power (W):</b> {{$value->maximum_input_power}}
                    <b class="ml-2">| Maximum Input Voltage (V):</b> {{$value->maximum_input_voltage}}
                    <b class="ml-2">| Maximum Input Current (I)	:</b> {{$value->maximum_input_current}}
                    <b class="ml-2">| Maximum Output Power (W)	:</b> {{$value->maximum_output_power}}
                    <b class="ml-2">| Maximum Output Voltage (V):</b> {{$value->maximum_output_voltage}}
                    <b class="ml-2">| Maximum Output Current (I):</b> {{$value->maximum_output_current}}
                    <b class="ml-2">| Operating Voltage Range (V):</b> {{$value->operating_voltage_range}}
                    <b class="ml-2">| MPPT Voltage Range (V):</b> {{$value->mppt_voltage_range}}
                    <b class="ml-2">| Number of MPPTs:</b> {{$value->no_of_mppts}}
                    <b class="ml-2">| Number of Inputs/MPPT:</b> {{$value->no_of_inputs_mppt}}
                    <b class="ml-2">| Maximum Modules Per String:</b> {{$value->maximum_modules_per_string}}
                </p>
	          </div>
          </div>


        </div>
      </div>
      @endforeach

      {{ $inverters->appends(request()->query())}}

      @endif







    </div>
  </div>
</section>
</div>


@push('scripts')
<script>
// $(".submit").click(function(){
// alert("check");
// });
</script>

@endpush
@endsection
