@extends('front.layouts.app')
@section('title',$data['page_title'])
@section('content')
@push('styles')
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<style>


</style>
@endpush
<div class="body-section body-login">

<!-- Inner Banner -->
<section class="inner-bnr"></section>

<!-- Recent Projects section -->
<section class="recent-projects-sec  structural-project-report">
  <div class="container-fluid">
    <div class="d-flex align-items-center title-sec">
      <h5>{{ $data['page_title'] }}</h5>

    </div>
    <div id="accordion" class="accordian-collapse">
    @include('front.includes.message')


      <div class="card">
        <form  action="{{url('project')}}" method="post" id="create-project-form">
            @csrf
              <input type="hidden" name="_method" id="_method" value="" />
              <div class="row">
                <div class="col-md-6">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Owner Name" name="owner_name" id="owner_name">
                  </div>

                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Address" name="address" id="address">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="State" name="state" id="state">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="AHJ" name="ahj" id="ahj">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Ult Wind Speed" name="ult_wind_speed" id="ult_wind_speed">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Ground Snow Load" name="ground_snow_load" id="ground_snow_load">
                  </div>
                </div>
                

                <div class="col-md-6">
                  <div class="additional-info d-flex align-items-center">
                    <p>For Structural Loads</p>
                    <a target="_blank" class="ml-auto" href="https://hazards.atcouncil.org/" data-toggle="tooltip" title="atchazards">
                    <img class="ml-auto" src="{{url('front/images/atc-logo-blue.png')}}" width="40" /></a>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="additional-info d-flex align-items-center">
                    <p>For Design Temperature</p>
                     <a target="_blank" class="ml-auto" href="http://www.solarabcs.org" data-toggle="tooltip" title="solarabcs">
                    <img class="ml-auto" src="{{url('front/images/Banner_760x150.jpg')}}" width="40" /></a>

                  </div>
                </div>
              </div>

              <div class="row btn-groups-row create-project-div" >
                <div class="col-sm-4 mb-sm-0 mb-3">
                    <a href="{{url('structural')}}" id="structural_report" class="btn btn-tab btn-red create-project-submit">Create Structural Report</a>
                </div>
                <div class="col-sm-4 mb-sm-0 mb-3 text-center">
                    <a href="{{url('electrical')}}" id="electrical_report" href="#" class="btn btn-tab btn-gray create-project-submit">Create Electrical Report</a>
                </div>
                <div class="col-sm-4 mb-sm-0 mb-3 text-lg-right">
                    <a href="{{url('structural')}}" id="full_report" href="#" class="btn btn-tab btn-black create-project-submit">Create Full Report</a>
                </div>

              </div>

            </form>
      </div>









    </div>
  </div>
</section>
</div>


@push('scripts')
<script>
// $(".submit").click(function(){
// alert("check");
// });
</script>

<script src="{{asset('front/js/projects/event.js')}}"></script>
@endpush
@endsection
