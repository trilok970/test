@extends('front.layouts.app')
@section('title','Login')
@section('content')

@push('styles')
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<style>

.new-popup .modal-dialog {
    top: 15px !important;
    transform: translateY(0px) !important;
}
</style>
@endpush

<div class="body-section bg-gray structural-body-sec">

<!-- Inner Banner -->
<!-- <section class="inner-bnr"></section> -->

 <!-- Recent Projects section -->
 <section class="recent-projects-sec structural-sec">
    <div class="d-flex structural-sec-inner">
      <div class="sidebar">
        <div class="sidebar-inner  site-info-feilds">
          <button id="close-btn" class="d-lg-none d-block">X</button>
          {{-- <h5>Please fill the details first </h5> --}}
          <h5 class="position-relative siteModal" style="cursor:pointer;" >Please fill the details first    <i class="fa fa-info-circle infor-btn"  aria-hidden="true"></i> </h5>
            @include('front.includes.message')
            <div class="message"></div>
          <form id="create-site-form" class="create-site-form" action="@if($site){{url('site/'.$site->id)}}@else {{url('site')}} @endif" method="post">
          @csrf
          @if($site)
          @method("PUT")
          @endif

          <input type="hidden" name="velocity_pressure" id="velocity_pressure" value="0" />
          <input type="hidden" name="project_id" id="project_id" value="{{$project_id}}" />
          <input type="hidden" name="roof_no" id="roof_no" value="{{count($roofinfos)}}" />
          <input type="hidden" name="site_formula" id="site_formula" value="{{url('site-formula')}}" />
            <div class="input-box">

              <div class="form-group height-sec">
                  <label for="">Inverter Type</label>
                  <div class="select-area form-control" style="width:100%;">
                      <select name="inverter_type" id="inverter_type" class="inverter_type">
                      <option value="">Select Inverter Type</option>
                      <option value="Microinverter" @if($site) {{$site->inverter_type=="Microinverter" ? "selected":""}} @endif>Microinverter</option>
                      <option value="String Inverter" @if($site) {{$site->inverter_type=="String Inverter" ? "selected":""}} @endif>String Inverter</option>
                      <option value="Generac" @if($site) {{$site->inverter_type=="Generac" ? "selected":""}} @endif>Generac</option>
                      <option value="Ac Coupled Inverter" @if($site) {{$site->inverter_type=="Ac Coupled Inverter" ? "selected":""}} @endif>Ac Coupled Inverter</option>
                      </select>
                  </div>
              </div>
              <div class="form-group height-sec">
                <label for="">Module Manufacturer</label>
                <div class="select-area form-control" style="width:100%;">
                    <select name="manufacturer_id" id="manufacturer_id" class="manufacturer_id">
                    <option value="">Select Module Manufacturer</option>
                    @foreach($manufacturers as $manufacturer)
                    <option value="{{ $manufacturer->id }}" @if($site) {{$site->manufacturer_id==$manufacturer->id ? "selected":""}} @endif>{{ $manufacturer->title }}</option>
                    @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group height-sec">
                <label for="">Module Model</label>
                <div class="select-area form-control" style="width:100%;">
                    <select name="module_id" id="module_id" class="module_id">
                    <option value="">Select Module Model</option>
                    @if($modules)
                    @foreach($modules as $module)
                    <option value="{{ $module->id }}" @if($site) {{$site->module_id==$module->id ? "selected":""}} @endif>{{ $module->title }}</option>
                    @endforeach
                    @endif
                    </select>
                </div>
            </div>
            <div class="form-group height-sec">
                <label for="">Inverter Manufacturer</label>
                <div class="select-area form-control" style="width:100%;">
                    <select name="inverter_manufacturer_id" id="inverter_manufacturer_id" class="inverter_manufacturer_id">
                    <option value="">Select Inverter Manufacturer</option>
                    @foreach($manufacturers as $manufacturer)
                    <option value="{{ $manufacturer->id }}" @if($site) {{$site->inverter_manufacturer_id==$manufacturer->id ? "selected":""}} @endif>{{ $manufacturer->title }}</option>
                    @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group height-sec">
                <label for="">Inverter Model</label>
                <div class="select-area form-control" style="width:100%;">
                    <select name="inverter_module_id" id="inverter_module_id" class="inverter_module_id">
                    <option value="">Select Inverter Model</option>
                    @if($modules)
                    @foreach($inverter_modules as $module)
                    <option value="{{ $module->id }}" @if($site) {{$site->inverter_module_id==$module->id ? "selected":""}} @endif>{{ $module->title }}</option>
                    @endforeach
                    @endif
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="">Number of Inverters</label>
                <input type="text" class="form-control no_of_inverters" name="no_of_inverters" id="no_of_inverters"  value="@if($site) {{ $site->no_of_inverters }} @endif">
              </div>
              <div class="cu_radio">
                <label for="">Mixture of Inverters</label>
                <div class="form-group">
                  <input type="radio" name="mixture_of_inverters"  value="yes" @if($site) {{$site->mixture_of_inverters=='yes' ? "checked":""}} @endif>
                  <span>Yes</span>
                  <input type="radio" name="mixture_of_inverters"  value="no" @if($site) {{$site->mixture_of_inverters=='no' ? "checked":""}} @endif>
                  <span>No</span>
                </div>
              </div>
              <div class="form-group">
                <label for="">Number of diff. Inverters</label>
                <input type="text" class="form-control no_of_diff_inverters" name="no_of_diff_inverters" id="no_of_diff_inverters"  value="@if($site) {{ $site->no_of_diff_inverters }} @endif">
              </div>
              {{-- <div class="form-group">
                <label for="">Number of diff. Inverters</label>
                <input type="text" class="form-control no_of_diff_inverters" name="no_of_diff_inverters" id="no_of_diff_inverters"  value="@if($site) {{ $site->no_of_diff_inverters }} @endif">
              </div> --}}




              <input @if(count($roofinfos) > 0) readonly @endif type="hidden" class="form-control numeric_feild_discount no_of_strings" placeholder="" name="no_of_strings" id="no_of_strings" value="{{$site->no_of_strings ?? ''}}">


            </div>

            <div class="btn-row"><button type="submit" class="btn btn-red w-100 create-site-submit create-site-button" id="create-site-button">@if($site) Update @else Submit @endif</button></div>

          </form>
        </div>
      </div>

      <div class="structural-col">
        <!-- Inner Banner -->
        <section class="inner-bnr"></section>
        <div class="container-fluid">
          <div class="title-sec">
            <div class="navbar-light left-side-btn mb-4">
              <button class="navbar-toggler d-lg-none d-block">
                <span class="navbar-toggler-icon"></span>
              </button>
            </div>
            <div class="row">
              <div class="col-md-12 mb-md-0 mb-5">
                <h5 class="text-uppercase">Module Properties <a href="{{ url('electrical1/'.$project_id) }}">Electrical 1</a></h5>

                <div class="structural-details-col  electrical_page_top">
                  <div class="row w-100 cur_border">
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel el_fist">
                        <h4>Voc</h4>
                        <p>#N/A</p>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel">
                        <h4>Vmpp</h4>
                        <p>#N/A</p>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel">
                        <h4>TC Voc</h4>
                        <p>#N/A</p>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel">
                        <h4>Isc</h4>
                        <p>#N/A</p>
                      </div>
                    </div>
                  </div>
                  <div class="row w-100">
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel el_fist">
                        <h4>Imp</h4>
                        <p>#N/A</p>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel">
                        <h4>Pmp</h4>
                        <p>#N/A</p>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel">
                        <h4>TC Vmp</h4>
                        <p>#N/A</p>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel">
                        <h4>NOCT</h4>
                        <p>#N/A</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!--****************************** Inverter Properties**************************************** -->
          <div class="inventer_properties_sec">
            <h2>Inverter Properties</h2>
            <div class="structural-details-col">

              <div class="row">
                <div class="col-lg-12">
                  <div class="invertey_table_list">
                    <table class="table table-responsive">
                      <tbody>
                        <tr>
                          <td>No. of strings</td>
                          <td>Nos</td>
                        </tr>
                        <tr>
                          <td>Output Voltage</td>
                          <td>Vac</td>
                        </tr>
                        <tr>
                          <td>Max Input dc Voltage</td>
                          <td>48 Vdc</td>
                        </tr>
                        <tr>
                          <td>Operating Range</td>
                          <td>27 - 39 Vdc</td>
                        </tr>
                        <tr>
                          <td>MPPT Voltage Range</td>
                          <td>27 - 39 Vdc</td>
                        </tr>
                        <tr>
                          <td>Start Voltage</td>
                          <td>22 Vdc</td>
                        </tr>
                        <tr>
                          <td>Max Input Power</td>
                          <td>300Wdc</td>
                        </tr>
                        <tr>
                          <td>Max AC Current</td>
                          <td>VA</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

              </div>
            </div>
          </div>


          <!--****************************** Inverter Properties strings**************************************** -->
          <div class="inventer_properties_sec stings_no">
            <h2>Number of Strings</h2>
            <div class="structural-details-col">

              <div class="row">
                <div class="col-lg-12">
                  <div class="invertey_table_list">

                    <table class="table table-responsive">
                      <thead>
                        <th>String ID</th>
                        <th>Module</th>
                        <th>Inverter</th>
                        <th>String Size</th>
                      </thead>
                      <tbody>
                        <tr>
                          <td>#1</td>
                          <td>AC12</td>
                          <td>Microinverter</td>
                          <td>305</td>
                        </tr>
                        <tr>
                          <td>#1</td>
                          <td>AC12</td>
                          <td>Microinverter</td>
                          <td>305</td>
                        </tr>
                        <tr>
                          <td>#1</td>
                          <td>AC12</td>
                          <td>Microinverter</td>
                          <td>305</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

              </div>
            </div>
          </div>





        </div>


      </div>
    </div>

    <!-- Button trigger modal -->



  </section>
</div>
@push('scripts')
<script src="{{asset('front/js/sites/event.js')}}"></script>
<script src="{{asset('front/js/roofinfo/event.js')}}"></script>
<script>

</script>

@endpush
@endsection
