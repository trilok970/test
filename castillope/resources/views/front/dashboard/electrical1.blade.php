@extends('front.layouts.app')
@section('title','Login')
@section('content')

@push('styles')
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<style>

.new-popup .modal-dialog {
    top: 15px !important;
    transform: translateY(0px) !important;
}
</style>
@endpush

<div class="body-section bg-gray structural-body-sec">

<!-- Inner Banner -->
<!-- <section class="inner-bnr"></section> -->

 <!-- Recent Projects section -->
 <section class="recent-projects-sec structural-sec el_form">
    <div class="d-flex structural-sec-inner">
        <div class="sidebar">
            <div class="sidebar-inner">
                <button id="close-btn" class="d-lg-none d-block">X</button>
                <h5>Please fill the details first </h5>
                <form action="#">
                    <div class="input-box">
                        <div class="form-group">
                            <div class="custom-select form-control" style="width:100%;">
                                <select>
                                    <option value="0">Dc Conductor Ampacity Calculations</option>
                                    <option value="1">AC Conductor Ampacity Calculations</option>
                                    <option value="2">DC Conductor Ampacity Calculations</option>
                                    <option value="3">Battery Conductor Ampacity Calculations</option>
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="btn-row"><button type="button"  class="btn btn-red w-100" >Submit</button></div>


                </form>
            </div>
        </div>

        <div class="structural-col">
            <!-- Inner Banner -->
            <section class="inner-bnr"></section>
            <div class="container-fluid">
                <div class="title-sec">
                    <div class="navbar-light left-side-btn mb-4">
                        <button class="navbar-toggler d-lg-none d-block">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-md-0 mb-5">
                            <h5 class="text-uppercase">DC Conductor Ampacity Calculations</h5>

                            <div class="structural-details-col  electrical_page_top elec2">
                                <!-- Accordian Section -->
                                <div id="accordion" class="accordian-collapse structural-collapse">
                                    <div class="card">
                                        <div class="card-header" id="headingOne" data-toggle="collapse"
                                            data-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            <h5 class="d-flex align-items-center mb-0"><span
                                                    style="color: #A80D2C;"> String ID</span> : AC270p/605 <span
                                                    class="icon icon-accordian-2-add ml-auto"></span></h5>
                                        </div>

                                        <div id="collapseOne" class="collapse show cus_acro"
                                            aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body">
                                                <form>
                                                    <div class="row">
                                                        <div class="col-md-12 mb-md-0 mb-5">
                                                            <div class="row">
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="mlength">Module</label>
                                                                        <input type="text" class="form-control"
                                                                            id="mlength" placeholder="78">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="pvmodule">From </label>
                                                                        <div class="custom-select form-control"
                                                                            style="width:100%;">
                                                                            <select>
                                                                                <option value="0">2</option>
                                                                                <option value="1">2</option>
                                                                                <option value="2">2</option>
                                                                                <option value="3">2</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="morientation">To</label>
                                                                        <div class="custom-select form-control"
                                                                            style="width:100%;">
                                                                            <select>
                                                                                <option value="0">2</option>
                                                                                <option value="1">2</option>
                                                                                <option value="2">2</option>
                                                                                <option value="3">2</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="mlength">Terminal Ampacity</label>
                                                                        <input type="text" class="form-control"
                                                                            id="mlength" placeholder="75">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="mlength">Inverter</label>
                                                                        <input type="text" class="form-control"
                                                                            id="mlength" placeholder="75">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="mlength">Current</label>
                                                                        <input type="text" class="form-control"
                                                                            id="mlength" placeholder="Free Air">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="mlength">Sets of Wires</label>
                                                                        <input type="text" class="form-control"
                                                                            id="mlength" placeholder="2">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="mlength">Wire Ampacity</label>
                                                                        <input type="text" class="form-control"
                                                                            id="mlength" placeholder="90">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="morientation">No. of Conductors</label>
                                                                        <div class="custom-select form-control"
                                                                            style="width:100%;">
                                                                            <select>
                                                                                <option value="0">2</option>
                                                                                <option value="1">2</option>
                                                                                <option value="2">2</option>
                                                                                <option value="3">2</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="morientation">Conductor Material</label>
                                                                        <div class="custom-select form-control"
                                                                            style="width:100%;">
                                                                            <select>
                                                                                <option value="0">CU</option>
                                                                                <option value="1">CU</option>
                                                                                <option value="2">CU</option>
                                                                                <option value="3">CU</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="morientation">Size</label>
                                                                        <div class="custom-select form-control"
                                                                            style="width:100%;">
                                                                            <select>
                                                                                <option value="0">1</option>
                                                                                <option value="1">1</option>
                                                                                <option value="2">1</option>
                                                                                <option value="3">1</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="morientation">No. of Conduits</label>
                                                                        <div class="custom-select form-control"
                                                                            style="width:100%;">
                                                                            <select>
                                                                                <option value="0">2</option>
                                                                                <option value="1">2</option>
                                                                                <option value="2">2</option>
                                                                                <option value="3">2</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="morientation">Conduit Location</label>
                                                                        <div class="custom-select form-control"
                                                                            style="width:100%;">
                                                                            <select>
                                                                                <option value="0">Free Air</option>
                                                                                <option value="1">Free Air</option>
                                                                                <option value="2">Free Air</option>
                                                                                <option value="3">Free Air</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="morientation">Wire Size</label>
                                                                        <div class="custom-select form-control"
                                                                            style="width:100%;">
                                                                            <select>
                                                                                <option value="0">78</option>
                                                                                <option value="1">78</option>
                                                                                <option value="2">78</option>
                                                                                <option value="3">78</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="mlength">Conduit Derate</label>
                                                                        <input type="text" class="form-control"
                                                                            id="mlength" placeholder="1">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="mlength">Temp Derate</label>
                                                                        <input type="text" class="form-control"
                                                                            id="mlength" placeholder="2">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="morientation">Derated Ampacity</label>
                                                                        <div class="custom-select form-control"
                                                                            style="width:100%;">
                                                                            <select>
                                                                                <option value="0">78</option>
                                                                                <option value="1">78</option>
                                                                                <option value="2">78</option>
                                                                                <option value="3">78</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="morientation">CorretedWire Size</label>
                                                                        <div class="custom-select form-control"
                                                                            style="width:100%;">
                                                                            <select>
                                                                                <option value="0">90</option>
                                                                                <option value="1">90</option>
                                                                                <option value="2">90</option>
                                                                                <option value="3">90</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="mlength">Conductor Ampacity</label>
                                                                        <input type="text" class="form-control"
                                                                            id="mlength" placeholder="CU">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="mlength">OCPD</label>
                                                                        <input type="text" class="form-control"
                                                                            id="mlength" placeholder="78">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="morientation">Ground Wire</label>
                                                                        <div class="custom-select form-control"
                                                                            style="width:100%;">
                                                                            <select>
                                                                                <option value="0">2</option>
                                                                                <option value="1">2</option>
                                                                                <option value="2">2</option>
                                                                                <option value="3">2</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="mlength">Corrected Ground Wire</label>
                                                                        <input type="text" class="form-control"
                                                                            id="mlength" placeholder="1">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="morientation">Max length</label>
                                                                        <div class="custom-select form-control"
                                                                            style="width:100%;">
                                                                            <select>
                                                                                <option value="0">78</option>
                                                                                <option value="1">78</option>
                                                                                <option value="2">78</option>
                                                                                <option value="3">78</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="morientation">Conductor Type</label>
                                                                        <div class="custom-select form-control"
                                                                            style="width:100%;">
                                                                            <select>
                                                                                <option value="0">1</option>
                                                                                <option value="1">1</option>
                                                                                <option value="2">1</option>
                                                                                <option value="3">1</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="morientation">VD%</label>
                                                                        <div class="custom-select form-control"
                                                                            style="width:100%;">
                                                                            <select>
                                                                                <option value="0">1</option>
                                                                                <option value="1">1</option>
                                                                                <option value="2">1</option>
                                                                                <option value="3">1</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-10 col-md-10 col-sm-10">
                                                                    <div class="btn-row btn_dr"><button type="button"  class="btn btn-red" >Submit</button></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="card-header collapsed" id="headingTwo"
                                            data-toggle="collapse" data-target="#collapseTwo"
                                            aria-expanded="true" aria-controls="collapseTwo">
                                            <h5 class="d-flex align-items-center mb-0">String ID : 2 <span
                                                    class="icon icon-accordian-2-add ml-auto"></span></h5>
                                        </div>

                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                            data-parent="#accordion">
                                            <div class="card-body">
                                                <p>lorem ipsum</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="card-header collapsed" id="headingThree"
                                            data-toggle="collapse" data-target="#collapseThree"
                                            aria-expanded="true" aria-controls="collapseThree">
                                            <h5 class="d-flex align-items-center mb-0">String ID : 3 <span
                                                    class="icon icon-accordian-2-add ml-auto"></span></h5>
                                        </div>

                                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                            data-parent="#accordion">
                                            <div class="card-body">
                                                <p>lorem ipsum</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>










            </div>


        </div>
    </div>
</section>

</div>
@push('scripts')
<script src="{{asset('front/js/sites/event.js')}}"></script>
<script src="{{asset('front/js/roofinfo/event.js')}}"></script>
<script>

</script>

@endpush
@endsection
