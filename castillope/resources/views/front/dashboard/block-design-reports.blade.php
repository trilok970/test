@extends('front.layouts.app')
@section('title',$data['page_title'])
@section('content')
@push('styles')
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<style>
.report-type {
    border-color: #A80D2C;
    color: #A80D2C !important;
}

</style>
@endpush
<div class="body-section body-login">

<!-- Inner Banner -->
<section class="inner-bnr"></section>

<!-- Recent Projects section -->
<section class="recent-projects-sec">
  <div class="container-fluid">
    <div class="d-flex align-items-center title-sec">
      <h5>{{ $data['page_title']}}</h5>
    </div>
    <div id="accordion" class="accordian-collapse">
    @include('front.includes.message')
    
      
      <div class="card">
        <div class="card-header">
          <h5 class="d-flex flex-wrap align-items-center">
          	<div class="col-md-6 col-md-5">
          		<span class="owner-name"> Owner Name: </span> {{$data['project']->owner_name}}
          	</div>
          	<div class="col-lg-6 col-md-7 d-flex justify-content-md-end btn-area">
              
              
             
                
          	</div>

          </h5>

          <div class="col-12 edit-ssection">
	          <div class="more">
                  <p >Report Type : <span class="report-type" >{{ucwords(str_replace("_"," ",$data['project']->type))}}</span></p>
                  <br>
	              <p>
                    <b>Address :</b> {{$data['project']->address}}
                    <b class="ml-2">| State :</b> {{$data['project']->state}}
                    <b class="ml-2">| AHJ :</b> {{$data['project']->ahj}}
                </p>
	          </div>
          </div>
        </div>
        
      </div>
      <div class="row">
        <div class="col-md-12">
          @if(count($data['blockDesigns']) > 0)
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th class="text-center">View</th>
                    <th class="text-center">Edit</th>
                  </tr>
                </thead>
                <tbody>
                  
                  @foreach($data['blockDesigns'] as $blockDesign)

                  <tr>
                    <td>{{ date('M,d Y',strtotime($blockDesign->created_at))}}</td>
                    <td class="text-center">
                        <a target="_blank" class="btn btn-warning brn-sm" href="{{$blockDesign->file}}" id="" data-toggle="tooltip" title="Block Design Report"><i class="far fa-eye"></i></a>
                    </td>
                    <td class="text-center">
                      <a target="_blank" class="btn btn-success brn-sm" href="{{ route('block.design',$blockDesign->project_id) }}" id="" data-toggle="tooltip" title="Block Design Report"><i class="far fa-pen"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              <table>
            </div>
            @else 
          <h4>Data not found.</h4>
       @endif
      </div>
    </div>
      {{ $data['blockDesigns']->appends(request()->query())}}

     





    </div>
  </div>
</section>
</div>


@push('scripts')
<script>
// $(".submit").click(function(){
// alert("check");
// });
</script>

<script src="{{asset('front/js/projects/event.js')}}"></script>
@endpush
@endsection
