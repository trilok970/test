@extends('front.layouts.app')
@section('title',$data['page_title'])
@section('content')

<div class="body-section body-login">

<!-- Inner Banner -->
<section class="inner-bnr"></section>

<!-- Recent Projects section -->
<section class="recent-projects-sec">
  <div class="container-fluid">
    <div class="d-flex align-items-center title-sec">
      <h5>{{ $data['page_title'] }}</h5>

    </div>
    <div id="accordion" class="accordian-collapse">
    @include('front.includes.message')
      @if($modules)
      @foreach($modules as $key => $value)
      <div class="card">
        <div class="card-header">
            <h5 class="d-flex align-items-center">
                <span class="owner-name"> Model Name: </span>&nbsp;{{$value->module_model}} <button class="ml-auto" id="myBtn"><span class="icon icon-accordian-add morelink"></span></button>
            </h5>
          {{-- <h5 class="d-flex flex-wrap align-items-center">
          	<div class="col-md-6 col-md-5">
          		<span class="owner-name"> Model Name: </span> {{$value->module_model}}
          	</div>
           <div class="col-lg-6 col-md-7 d-flex justify-content-md-end btn-area">

              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
               <button class="ml-auto" id="myBtn"><span class="icon icon-accordian-add morelink"></span></button>

          	</div>

          </h5> --}}

          <div class="col-12 edit-ssection">
	          <div class="more">
	            <p>
                    <b>Manufacturer:</b> {{$value->manufacturer->title ?? ""}}
                    <b class="ml-2">| Pmp:</b> {{$value->pmp}}
                    <b class="ml-2">| Voc:</b> {{$value->voc}}
                    <b class="ml-2">| Vmpp:</b> {{$value->vmpp}}
                    <b class="ml-2">| Tvoc:</b> {{$value->tvoc}}
                    <b class="ml-2">| Isc:</b> {{$value->isc}}
                    <b class="ml-2">| Imp:</b> {{$value->imp}}
                    <b class="ml-2">| Tcpm:</b> {{$value->tcpm}}
                    <b class="ml-2">| Module Lenght:</b> {{$value->module_lenght}}
                    <b class="ml-2">| Module Width:</b> {{$value->module_width}}
                    <b class="ml-2">| Allowed Pressure Short Side:</b> {{$value->allowed_pressure_short_side}}
                    <b class="ml-2">| Allowed Pressure Long Side:</b> {{$value->allowed_pressure_long_side}}
                </p>
	          </div>
          </div>


        </div>
      </div>
      @endforeach

      {{ $modules->appends(request()->query())}}

      @endif







    </div>
  </div>
</section>
</div>


@push('scripts')
<script>
// $(".submit").click(function(){
// alert("check");
// });
</script>

@endpush
@endsection
