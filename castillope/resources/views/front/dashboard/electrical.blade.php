@extends('front.layouts.app')
@section('title',"Electrical Calculations")
@section('content')

@push('styles')
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<style>

.new-popup .modal-dialog {
    top: 15px !important;
    transform: translateY(0px) !important;
}
</style>
@endpush

<div class="body-section bg-gray structural-body-sec">
<input type="hidden" name="electrical_formula" id="electrical_formula" value="{{url('electrical-formula')}}" />

<!-- Inner Banner -->
<!-- <section class="inner-bnr"></section> -->

 <!-- Recent Projects section -->
 <section class="recent-projects-sec structural-sec el_form">
    <div class="d-flex structural-sec-inner">
      <div class="sidebar">
        <div class="sidebar-inner electrical-info-feilds">
          <button id="close-btn" class="d-lg-none d-block">X</button>
          <h5>Please fill the details first </h5>
          <form id="create-electrical-form" class="create-electrical-form" action="{{url('electrical')}}" method="post">
            @csrf


            <input type="hidden" name="project_id" id="project_id" value="{{$project_id}}" />
            <input type="hidden" class="id" name="id" id="id" value="@if($electrical) {{ $electrical->id }} @else 0 @endif" />
            <input type="hidden" name="site_formula" id="site_formula" value="{{url('site-formula')}}" />
            <div class="input-box">
                <div class="form-group height-sec">
                    <label for="">Inverter Type</label>
                    <div class="select-area form-control" >
                        <select name="inverter_type_id" id="inverter_type_id" class="inverter_type_id select2"  >
                        <option value="">Select Inverter Type</option>
                        {{-- <option value="Microinverter" @if($electrical) {{$electrical->inverter_type=="Microinverter" ? "selected":""}} @endif>Microinverter</option>
                        <option value="String Inverter" @if($electrical) {{$electrical->inverter_type=="String Inverter" ? "selected":""}} @endif>String Inverter</option>
                        <option value="Generac" @if($electrical) {{$electrical->inverter_type=="Generac" ? "selected":""}} @endif>Generac</option>
                        <option value="Ac Coupled Inverter" @if($electrical) {{$electrical->inverter_type=="Ac Coupled Inverter" ? "selected":""}} @endif>Ac Coupled Inverter</option> --}}
                        @foreach($inverter_types as $inverter_type)
                        <option @if(!empty($electrical)){{$electrical->inverter_type_id==$inverter_type->id?'selected':''}}@endif value="{{$inverter_type->id}}">{{$inverter_type->name}}</option>
                       @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group height-sec">
                  <label for="">Module Manufacturer</label>
                  <div class="select-area form-control" >
                      <select name="manufacturer_id" id="manufacturer_id" class="manufacturer_id select2"  url="{{url('module-manufacturer')}}">
                      <option value="">Select Module Manufacturer</option>
                      @foreach($manufacturers as $manufacturer)
                      <option value="{{ $manufacturer->id }}" @if($electrical) {{$electrical->manufacturer_id==$manufacturer->id ? "selected":""}} @endif>{{ $manufacturer->title }}</option>
                      @endforeach
                      </select>
                  </div>
              </div>
              <div class="form-group height-sec">
                  <label for="">Module Model</label>
                  <div class="select-area form-control" >
                      <select name="module_id" id="module_id" class="module_id select2">
                      <option value="">Select Module Model</option>
                      @if($modules)
                      @foreach($modules as $module)
                      <option value="{{ $module->id }}" @if($electrical) {{$electrical->module_id==$module->id ? "selected":""}} @endif>{{ $module->module_model }}</option>
                      @endforeach
                      @endif
                      </select>
                  </div>
              </div>
              <div class="form-group">
                <label for="">Mixture of Inverters</label>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input mixture_of_inverters" type="radio" name="mixture_of_inverters" id="mixture_of_inverters1" value="Yes" @if($electrical) {{$electrical->mixture_of_inverters=='Yes' ? "checked":""}} @endif>
                    <label class="form-check-label" for="mixture_of_inverters1" style="margin-top:10px; ">Yes</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input mixture_of_inverters" type="radio" name="mixture_of_inverters" id="mixture_of_inverters2" value="No" @if($electrical) {{$electrical->mixture_of_inverters=='No' ? "checked":""}} @endif>
                    <label class="form-check-label" for="mixture_of_inverters2" style="margin-top:10px; ">No</label>
                  </div>

              </div>
              <div class="form-group no_of_inverters_div " style="display: @if($electrical && $electrical->mixture_of_inverters == 'No') none; @else block;@endif"  >
                <label for="">Number of Inverters</label>
                <input type="text" class="form-control no_of_inverters numeric_feild" name="no_of_inverters" id="no_of_inverters"  value="@if($electrical){{$electrical->no_of_inverters}}@endif">
              </div>
              <div class="form-group height-sec no_of_inverter">
                  <label for="">Inverter Manufacturer</label>
                  <div class="select-area form-control" >
                      <select  style="width:100%;" name="inverter_manufacturer_id" id="inverter_manufacturer_id" class="inverter_manufacturer_id select2"  url="{{url('manufacturer-inverter')}}" >
                      <option value="">Select Inverter Manufacturer</option>
                      @foreach($inverter_manufacturers as $inv_manufacturer)
                      <option value="{{ $inv_manufacturer->id }}" @if($electrical) {{$electrical->inverter_manufacturer_id==$inv_manufacturer->id ? "selected":""}} @endif>{{ $inv_manufacturer->title }}</option>
                      @endforeach
                      </select>
                  </div>
              </div>
              <div class="form-group height-sec no_of_inverter">
                  <label for="">Inverter Model</label>
                  <div class="select-area form-control">
                      <select style="width:100%;" name="inverter_model_id" id="inverter_model_id" class="inverter_model_id select2">
                      <option value="">Select Inverter Model</option>
                      @if($inverter_modules)
                      @foreach($inverter_modules as $inverter)
                      <option value="{{ $inverter->id }}" @if($electrical) {{$electrical->inverter_model_id==$inverter->id ? "selected":""}} @endif>{{ $inverter->model_no }}</option>
                      @endforeach
                      @endif
                      </select>
                  </div>
              </div>
              <div class="form-group display-flex no_of_diff_inverter ">
                <label for="">Number of diff. Inverters</label>
                <div class="input-group">
                  <input type="text" class="form-control  no_of_diff_inverters numeric_feild" placeholder="" name="no_of_diff_inverters" id="site_roof_slope" value="{{$electrical->no_of_diff_inverters ?? ''}}" min="2">
                  <div class="input-group-append">
                      <button class="btn-red w-100 btn-sm differentModal" data-toggle="tooltip" title="Add Different Inverters" style="cursor: pointer;"><i class="fa fa-plus-square"></i></button>
                  </div>
              </div>
              </div>



                <input  type="hidden" class="form-control numeric_feild_discount no_of_strings" placeholder="" name="no_of_strings" id="no_of_strings" value="{{$electrical->no_of_strings ?? ''}}">

              {{-- <div class="form-group">
                <div class="custom-select form-control" style="width:100%;">
                  <select>
                    <option value="0">Number of Strings</option>
                    <option value="1">Number of Strings</option>
                    <option value="2">Number of Strings</option>
                    <option value="3">Number of Strings</option>
                  </select>
                </div>
              </div> --}}
              <div class="form-group">
                    <label for="">Battery</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input battery" type="radio" name="battery" id="inlineRadio1" value="Yes" @if($electrical) {{$electrical->battery=='Yes' ? "checked":""}} @endif>
                        <label class="form-check-label" for="inlineRadio1" style="margin-top:10px; ">Yes</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input battery" type="radio" name="battery" id="inlineRadio2" value="No" @if($electrical) {{$electrical->battery=='No' ? "checked":""}} @endif>
                        <label class="form-check-label" for="inlineRadio2" style="margin-top:10px; ">No</label>
                      </div>
              </div>
              <div class="form-group number_div">
                <label for="">Number</label>
                <input type="text" class="form-control number numeric_feild"  name="number" id="number" value="{{$electrical->number ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Ambient Temp</label>
                <input type="text" class="form-control ambient_temp numeric_feild_discount"  name="ambient_temp" id="ambient_temp" value="{{$electrical->ambient_temp ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">2% Minimum Temp</label>
                <input type="text" class="form-control minimum_temp numeric_feild_discount" placeholder="" name="minimum_temp" id="minimum_temp" value="{{$electrical->minimum_temp ?? ''}}">
              </div>
            </div>
            <div class="model-data-row d-none" >

            </div>
            <div class="btn-row"><button type="submit" id="exammodel" class="btn btn-red w-100  create-electrical-submit create-electrical-button" >Submit</button></div>


          </form>
        </div>
      </div>

      <div class="structural-col">
        <!-- Inner Banner -->
        <section class="inner-bnr"></section>
        <div class="container-fluid">
          <div class="title-sec">
            <div class="navbar-light left-side-btn mb-4">
              <button class="navbar-toggler d-lg-none d-block">
                <span class="navbar-toggler-icon"></span>
              </button>
            </div>
            <div class="row">
              <div class="col-md-12 mb-md-0 mb-5">
                <h5 class="text-uppercase">Module Properties </h5>

                <div class="structural-details-col  electrical_page_top">
                  <div class="row w-100 cur_border">
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel el_fist">
                        <h4>Voc</h4>
                        <p class="voc">@if($module) {{ $module->voc }} @endif</p>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel">
                        <h4>Vmpp</h4>
                        <p class="vmpp">@if($module) {{ $module->vmpp }} @else #N/A @endif</p>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel">
                        <h4>Tvoc</h4>
                        <p class="tvoc">@if($module) {{ $module->tvoc }} @else #N/A @endif</p>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel">
                        <h4>Isc</h4>
                        <p class="isc">@if($module) {{ $module->isc }} @else #N/A @endif</p>
                      </div>
                    </div>
                  </div>
                  <div class="row w-100">
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel el_fist">
                        <h4>Imp</h4>
                        <p class="imp">@if($module) {{ $module->imp }} @else #N/A @endif</p>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel">
                        <h4>Pmp</h4>
                        <p class="pmp">@if($module) {{ $module->pmp }} @else #N/A @endif</p>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel">
                        <h4>Tcpm</h4>
                        <p class="tcpm">@if($module) {{ $module->tcpm }} @else #N/A @endif</p>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <div class="details-panel">
                        <h4>NOCT</h4>
                        <p class="noct">#N/A</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!--****************************** Inverter Properties For 1**************************************** -->
         @if($electrical )

          <div class="inventer_properties_sec no_of_inverter">
            <h2>Inverter Properties</h2>
            <div class="structural-details-col">

              <div class="row">
                <div class="col-lg-12">
                  <div class="invertey_table_list">
                    <table class="table table-responsive">
                      <tbody>
                        <tr>
                            <td>Model No</td>
                            <td>{{ $electrical->inverter_model->model_no ?? "" }}</td>
                          </tr>
                       <tr>
                        <tr>
                          <td>Modules Per String</td>
                          <td>{{ $electrical->inverter_model->maximum_modules_per_string ?? "" }}</td>
                        </tr>
                        <tr>
                          <td>Maximum Input Power</td>
                          <td>{{ $electrical->inverter_model->maximum_input_power ?? "" }} (W)</td>
                        </tr>
                        <tr>
                          <td>Maximum Input Voltage</td>
                          <td>{{ $electrical->inverter_model->maximum_input_voltage ?? "" }} (V)</td>
                        </tr>
                        <tr>
                          <td>Maximum Input Current</td>
                          <td>{{ $electrical->inverter_model->maximum_input_current ?? "" }} (I)</td>
                        </tr>
                        <tr>
                          <td>Maximum Output Power</td>
                          <td>{{ $electrical->inverter_model->maximum_output_power ?? "" }} (W)</td>
                        </tr>
                        <tr>
                          <td>Maximum Output Voltage</td>
                          <td>{{ $electrical->inverter_model->maximum_output_voltage ?? "" }} (W)</td>
                        </tr>
                        <tr>
                          <td>Maximum Output Current</td>
                          <td>{{ $electrical->inverter_model->maximum_output_current ?? "" }} (I)</td>
                        </tr>
                        <tr>
                          <td>Operating Voltage Range</td>
                          <td>{{ $electrical->inverter_model->operating_voltage_range ?? "" }} (V)</td>
                        </tr>
                        <tr>
                            <td>Mppt Voltage Range</td>
                            <td>{{ $electrical->inverter_model->mppt_voltage_range ?? "" }} (V)</td>
                        </tr>
                        <tr>
                            <td>No Of Mppts</td>
                            <td>{{ $electrical->inverter_model->no_of_mppts ?? "" }}</td>
                        </tr>
                        <tr>
                            <td>No Of Inputs Mppt</td>
                            <td>{{ $electrical->inverter_model->no_of_inputs_mppt ?? "" }}</td>
                        </tr>

                      </tbody>
                    </table>
                  </div>
                </div>

              </div>
            </div>
          </div>
           <!--****************************** Inverter Properties For Many**************************************** -->
           <div class="structural-details-col  electrical_page_top elec2  no_of_diff_inverter">
            <!-- Accordian Section -->

            <div id="accordion" class="accordian-collapse structural-collapse ">

                @foreach($electrical->DifferentInverter as $inverter)
                <div class="card">
                    <div class="card-header collapsed" id="heading{{ $loop->iteration }}"
                        data-toggle="collapse" data-target="#collapse{{ $loop->iteration }}"
                        aria-expanded="true" aria-controls="collapse{{ $loop->iteration }}">
                        <h5 class="d-flex align-items-center mb-0">Inverter Properties {{ $loop->iteration }} <span
                                class="icon icon-accordian-2-add ml-auto"></span></h5>
                    </div>

                    <div id="collapse{{ $loop->iteration }}" class="collapse @if($loop->iteration ==1) show @endif" aria-labelledby="heading{{ $loop->iteration }}"
                        data-parent="#accordion">
                        <div class="card-body">

         <div class="inventer_properties_sec" style="margin: 0%;">
           <div class="structural-details-col">

             <div class="row">
               <div class="col-lg-12">
                 <div class="invertey_table_list">
                   <table class="table table-responsive">
                     <tbody>
                        <tr>
                            <td>Model No</td>
                            <td>{{ $inverter->inverter->model_no }}</td>
                          </tr>
                       <tr>
                         <td>Modules Per String</td>
                         <td>{{ $inverter->inverter->maximum_modules_per_string }}</td>
                       </tr>
                       <tr>
                         <td>Maximum Input Power</td>
                         <td>{{ $inverter->inverter->maximum_input_power }} (W)</td>
                       </tr>
                       <tr>
                         <td>Maximum Input Voltage</td>
                         <td>{{ $inverter->inverter->maximum_input_voltage }} (V)</td>
                       </tr>
                       <tr>
                         <td>Maximum Input Current</td>
                         <td>{{ $inverter->inverter->maximum_input_current }} (I)</td>
                       </tr>
                       <tr>
                         <td>Maximum Output Power</td>
                         <td>{{ $inverter->inverter->maximum_output_power }} (W)</td>
                       </tr>
                       <tr>
                         <td>Maximum Output Voltage</td>
                         <td>{{ $inverter->inverter->maximum_output_voltage }} (W)</td>
                       </tr>
                       <tr>
                         <td>Maximum Output Current</td>
                         <td>{{ $inverter->inverter->maximum_output_current }} (I)</td>
                       </tr>
                       <tr>
                         <td>Operating Voltage Range</td>
                         <td>{{ $inverter->inverter->operating_voltage_range }} (V)</td>
                       </tr>
                       <tr>
                           <td>Mppt Voltage Range</td>
                           <td>{{ $inverter->inverter->mppt_voltage_range }} (V)</td>
                       </tr>
                       <tr>
                           <td>No Of Mppts</td>
                           <td>{{ $inverter->inverter->no_of_mppts }}</td>
                       </tr>
                       <tr>
                           <td>No Of Inputs Mppt</td>
                           <td>{{ $inverter->inverter->no_of_inputs_mppt }}</td>
                       </tr>

                     </tbody>
                   </table>
                 </div>
               </div>

             </div>
           </div>
         </div>
                        </div>
                    </div>
                </div>
                @endforeach

                 {{-- <div class="card">
                    <div class="card-header collapsed" id="headingThree"
                        data-toggle="collapse" data-target="#collapseThree"
                        aria-expanded="true" aria-controls="collapseThree">
                        <h5 class="d-flex align-items-center mb-0">String ID : 3 <span
                                class="icon icon-accordian-2-add ml-auto"></span></h5>
                    </div>

                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                        data-parent="#accordion">
                        <div class="card-body">
                            <p>lorem ipsum</p>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
         @endif


          <!--**************************** Inverter Properties strings********************************** -->
          @if($electrical)
          <div class="inventer_properties_sec stings_no string-no-div">
            <div class="row">
              <div class="col-lg-6">
              <h2>Number of Strings </h2>
              </div>
              <div class="col-lg-6">
                {{-- <button id="add-roof" type="button" class="btn btn-red w-50 text-center add-string pull-right"> ADD STRINGING </button> --}}
              </div>
            </div>

            <div class="structural-details-col">

              <div class="row">
                <div class="col-lg-12">
                  <div class="invertey_table_list">
                    <form id="create-electrical-string-form" class="create-electrical-string-form" action="{{url('electrical-string')}}" method="post">
                        @csrf
                    <table class="table table-responsive">
                      <thead>
                        <th>String ID</th>
                        <th>Module</th>
                        <th>Inverter</th>
                        <th>String Size</th>
                      </thead>
                      <tbody>
                        <tr class="string_row d-none" id="string_row">
                            <input type="hidden" name="string_id[]" class="string_id" value="0" />
                            <input type="hidden" name="string_id_duplicate[]" class="string_id_duplicate" value="0" />
                            <td class="string_no">#1</td>
                            <td>
                                <div class="form-group">
                                    <div class="select-area form-control" style="width:100%;">
                                    <select style="width:100%;" name="module_id[]"  class="module_id">
                                    <option value="">Select Module</option>
                                    @foreach($string_modules as $module)
                                    <option value="{{ $module->id }}" >{{ $module->module_model }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class="select-area form-control" style="width:100%;">
                                        <select style="width:100%;" name="inverter_id[]"  class="inverter_id">
                                        <option value="">Select Inverter</option>
                                        @foreach($string_inverters as $inverter)
                                        <option value="{{ $inverter->id }}" >{{ $inverter->model_no }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                  <input type="text" name="size[]" class="size numeric_feild_discount">
                                  <span class="string_row_delete remove-string">
                                    <span class="icon ml-auto icon-accordian-2-minus"></span>
                                  </span>
                                </div>
                                <span class="add-string-duplicate" maxDuplicateString="0" randomString="0" style="display: none;">
                                  <button class="btn btn-sm btn-red" >
                                  <span class="icon ml-auto icon-accordian-2-add">Add Duplicate String</span>
                                  </button>
                                </span>
                                <span class="text-danger size-warning-message"></span>
                                <input type="hidden" name="size_warning_message[]" class="size-warning-message" />

                            </td>
                        </tr>

                      </tbody>

                        <input type="hidden" name="electrical_id" id="electrical_id" value="{{ $electrical->id }}" />
                        <tbody class="new_row" id="new_row">
                        @if(count($electrical_strings) > 0)
                        @php
                        function generateRandomString($length = 10) {
                            return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
                        }

                     
                            $i = 1;
                            $j = 0.1;
                            $randomString = 0; 
                        @endphp
                        @foreach($electrical_strings as $electrical_string)
                        <tr class="@if($electrical_string->string_id_duplicate==0)string_row @else string_row_duplicate @endif" id="string_{{ $loop->iteration }}">
                        <input type="hidden" name="string_id[]" class="string_id" value="{{ $electrical_string->id }}" />
                        <input type="hidden" name="string_id_duplicate[]" class="string_id_duplicate" value="{{ $electrical_string->string_id_duplicate }}" />

                            <td class="@if($electrical_string->string_id_duplicate==0)string_no @else string_no_duplicate {{$randomString}} @endif">#@if($electrical_string->string_id_duplicate==0){{ $i }} @else @if($i > 0){{ $i + $j -1 }} @else{{ $i + $j }} @endif @endif</td>
                            <td>
                                <div class="form-group">
                                    <div class="select-area form-control" style="width:100%;">
                                    <select style="width:100%;" name="module_id[]"  class="module_id">
                                    <option value="">Select Module</option>
                                    @foreach($string_modules as $module)
                                    <option value="{{ $module->id }}"  {{$electrical_string->module_id==$module->id ? "selected":""}}>{{ $module->module_model }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class="select-area form-control" style="width:100%;">
                                        <select style="width:100%;" name="inverter_id[]"  class="inverter_id sizeInverter">
                                        <option value="">Select Inverter</option>
                                        @foreach($string_inverters as $inverter)
                                        <option value="{{ $inverter->id }}"  {{$electrical_string->inverter_id==$inverter->id ? "selected":""}}>{{ $inverter->model_no }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                  <input type="text" name="size[]" class="size numeric_feild_discount" value="{{ $electrical_string->size }}">
                                  <span class="string_row_delete remove-string">
                                    <span class="icon ml-auto icon-accordian-2-minus"></span>
                                  </span>
                        @if($electrical_string->string_id_duplicate==0)
                        @php
                            $randomString = generateRandomString();
                        @endphp
                        @endif
                        {{-- {{ strtolower($electrical_string->inverter->InverterType->name) }} {{ $electrical_string->string_id_duplicate }} --}}
                        {{-- @if((strtolower($electrical_string->inverter->InverterType->name) == 'string inverter') && ($electrical_string->string_id_duplicate == 0)) --}}
                                  <span class="add-string-duplicate" maxDuplicateString="{{ $electrical_string->inverter->no_of_mppts * $electrical_string->inverter->no_of_inputs_mppt}}" randomString="{{$randomString}}" string_id_duplicate="{{$electrical_string->string_id_duplicate}}" @if((strtolower($electrical_string->inverter->InverterType->name) == 'string inverter') && ($electrical_string->string_id_duplicate == 0)) @else style="display:none;" @endif >
                                    <button class="btn btn-sm btn-red">
                                    <span class="icon ml-auto icon-accordian-2-add">Add Duplicate String</span>
                                    </button>
                                  </span>
                        {{-- @endif --}}
                                </div>
                                <span class="text-danger size-warning-message">{{ $electrical_string->size_warning_message }}</span>
                                <input type="hidden" name="size_warning_message[]" class="size-warning-message" value="{{ $electrical_string->size_warning_message }}" />

                            </td>
                        </tr>
                        @if($electrical_string->string_id_duplicate==0)
                          @php
                            $i++;
                            $j = 0.1;
                          @endphp
                        @else
                          @php
                            $j = $j + 0.1;  
                            
                          @endphp

                        @endif
                        @endforeach
                        @else

                        <tr class="string_row" id="string_1">
                        <input type="hidden" name="string_id[]" class="string_id" value="0" />
                            <td class="string_no">#1</td>
                            <td>
                                <div class="form-group">
                                    <div class="select-area form-control" style="width:100%;">
                                    <select style="width:100%;" name="module_id[]"  class="module_id">
                                    <option value="">Select Module</option>
                                    @foreach($string_modules as $module)
                                    <option value="{{ $module->id }}" >{{ $module->module_model }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class="select-area form-control" style="width:100%;">
                                        <select style="width:100%;" name="inverter_id[]"  class="inverter_id sizeInverter">
                                        <option value="">Select Inverter</option>
                                        @foreach($string_inverters as $inverter)
                                        <option value="{{ $inverter->id }}" >{{ $inverter->model_no }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                  <input type="text" name="size[]" class="size numeric_feild_discount" >
                                  <span class="string_row_delete remove-string">
                                    <span class="icon ml-auto icon-accordian-2-minus"></span>
                                  </span>
                                  <span class="add-string-duplicate" maxDuplicateString="0" randomString="0" style="display: none;">
                                    <button class="btn btn-sm btn-red" >
                                    <span class="icon ml-auto icon-accordian-2-add">Add Duplicate String</span>
                                    </button>
                                  </span>
                                </div>
                                <span class="text-danger size-warning-message"></span>
                                <input type="hidden" name="size_warning_message[]" class="size-warning-message" />

                            </td>
                        </tr>
                        @endif

                       </tbody>

                    </table>
                </form>

                  </div>
                  {{-- <div class="container"> --}}
                    <div class="row">
                      <div class="col-md-12  message alert alert-danger" style="display: none;"></div>
                        @if($electrical->project->type == 'full')
                        <div class="col-lg-3 text-lg-left mt-2  text-center form-group">
                            <a class="btn btn-red brn-sm " href="{{url('structural/'.$electrical->project->id)}}" id="myBtn" data-toggle="tooltip" title="Structural Report">Structural Report</a>
                        </div>
                        @endif
                      <div class="col-lg-4 mt-2 text-lg-right text-center">
                        <button id="create-string-info" type="submit" class="btn btn-red w-50 text-center create-string-info "> SAVE & NEXT</button>
                      </div>

                      <div class="col-lg-5 mt-2  text-center">
                          <button id="add-string" type="button" class="btn btn-red w-50 text-center add-string "> ADD STRINGING </button>
                      </div>

                    </div>
                {{-- </div> --}}

                </div>
              </div>

            </div>
          </div>
          @endif




        </div>


      </div>
    </div>
  </section>
</div>
@if($electrical)
<input type="hidden" name="electrical_url" id="electrical_url" value="{{ route('conductor.ampacity.calculations',[$electrical->id ?? 0,'DC']) }}" />
@endif
<!--**************************** Modal ***************************************-->
<div class="modal model_string fade" id="diff_inv_model" tabindex="-1" role="dialog"
aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
<div class="modal-dialog modal-dialog-centered" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-cnter" id="exampleModalLongTitle">Different Inverters</h5>
      {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span class="close_btn" aria-hidden="true">&times;</span>
      </button> --}}
    </div>
    <div class="modal-body">
      <div class="row align-items-center diff-inverter-row d-none" id="diff-inverter-row">
        <div class="col-lg-5 col-md-5 col-5">
          <div class="stings_model_list">
            {{-- <div class="custom-select form-control" style="width:100%;">
              <select>
                <option value="0">Number of Strings</option>
                <option value="1">Number of Strings</option>
                <option value="2">Number of Strings</option>
                <option value="3">Number of Strings</option>
              </select>
            </div> --}}
            <div class="form-group">
                <label for="">Inverter Manufacturer</label>
                <div class="select-area form-control" style="width:100%;">
                    <select style="width:100%;" name="inverter_manufacturer_id[]"  class="inverter_manufacturer_id"  url="{{url('manufacturer-inverter')}}">
                    <option value="">Select Inverter Manufacturer</option>
                    @foreach($inverter_manufacturers as $inv_manufacturer)
                    <option value="{{ $inv_manufacturer->id }}" >{{ $inv_manufacturer->title }}</option>
                    @endforeach
                    </select>
                </div>
            </div>
          </div>
        </div>
        <div class="col-lg-5 col-md-5 col-5">
          <div class="stings_model_list">
            {{-- <div class="custom-select form-control" style="width:100%;">
              <select>
                <option value="0">Inverter Model</option>
                <option value="1">Inverter Model</option>
                <option value="2">Inverter Model</option>
                <option value="3">Inverter Model</option>
              </select>
            </div> --}}
            <div class="form-group height-sec">
                <label for="">Inverter Model</label>
                <div class="select-area form-control" style="width:100%;">
                    <select style="width:100%;" name="inverter_id[]"  class="inverter_id">
                        <option value="">Select Inverter Model</option>
                        {{-- @if($modules)
                        @foreach($modules as $module)
                        <option value="{{ $module->id }}" @if($electrical) {{$electrical->module_id==$module->id ? "selected":""}} @endif>{{ $module->module_model }}</option>
                        @endforeach
                        @endif --}}
                        </select>
                </div>
            </div>

          </div>
        </div>
        <div class="col-lg-2 col-md-2 col-2">
          <div class="string_row_delete">
            <span class="icon ml-auto icon-accordian-2-minus remove-inverter"></span>
          </div>
         </div>
      </div>
      <form id="create-diff-inverter-form" class="create-diff-inverter-form" action="{{url('different-inverter')}}" method="post">
        @csrf
      <div class="diff-inverter-row-new" >
@if($electrical)
        @foreach($electrical->DifferentInverter as $inverter)
        <div class="row align-items-center diff-inverter-row" id="diff-inverter-{{ $loop->iteration }}">
            <div class="col-lg-5 col-md-5 col-5">
              <div class="stings_model_list">

                <div class="form-group">
                    <label for="">Inverter Manufacturer </label>
                    <div class="select-area form-control" style="width:100%;">
                        <select style="width:100%;" name="inverter_manufacturer_id[]"  class="inverter_manufacturer_id"  url="{{url('manufacturer-inverter')}}">
                        <option value="">Select Inverter Manufacturer</option>
                        @foreach($inverter_manufacturers as $inv_manufacturer)
                        <option value="{{ $inv_manufacturer->id }}"  {{$inverter->inverter_manufacturer_id==$inv_manufacturer->id ? "selected":""}}>{{ $inv_manufacturer->title }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-lg-5 col-md-5 col-5">
              <div class="stings_model_list">

                <div class="form-group height-sec">
                    <label for="">Inverter Model</label>
                    <div class="select-area form-control" style="width:100%;">
                        <select style="width:100%;" name="inverter_id[]"  class="inverter_id">
                            <option value="">Select Inverter Model</option>

        @php $diff_modal_inverters  = DB::table('inverters')->where('manufacturer_id',$inverter->inverter_manufacturer_id)->orderBy('id','desc')->get(); @endphp
                            @foreach($diff_modal_inverters as $module)
                            <option value="{{ $module->id }}"  {{$inverter->inverter_id==$module->id ? "selected":""}} >{{ $module->model_no }}</option>
                            @endforeach

                            </select>
                    </div>
                </div>

              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-2">
              <div class="string_row_delete">
                <span class="icon ml-auto icon-accordian-2-minus remove-inverter"></span>
              </div>
             </div>
          </div>

        @endforeach
        @endif


      </div>
      </form>

    </div>
    <div class="modal-footer">
      <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-primary">Save changes</button> -->

     <button type="button"  class="btn btn-red create-diff-inverter-button" >Close</button>
     <button type="button" class=" btn btn-gray create-diff-inverter-button">Save changes</button>
    </div>
  </div>
</div>
</div>


@push('scripts')
<script src="{{asset('front/js/electrical/event.js')}}"></script>
<script>
$(document).ready(function(){

});

</script>

@endpush
@endsection
