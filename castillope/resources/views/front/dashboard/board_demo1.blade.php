@extends('front.layouts.app')
@section('title',$data['page_title'])
@section('content')
@push('styles')
<style>
    .drag_div  {
      width: 100%;
      height: 100%;
      padding: 10px;
      border: 1px solid #aaaaaa;
    }
  /*  canvas{border:1px solid red;}*/
    #exportedImgs{border:1px solid green; padding:15px; width:300px; height:70px;}
    #toolbar{
        width:350px;
        height:35px;
        border:solid 1px blue;
    }
    .bords_page .structural-col .structural-details-col .item .board_box img {
        width:50px !important;
        height:50px !important;
    }
    #container {
        background-color: rgba(0, 0, 0, 0.1);
        border:1px solid #000;
        min-height: 500px;
      }
    .overlay{
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        display: none;
        background: rgba(0, 0, 0, 0.8);
        z-index: 1000;
    }
    /* Button used to open the contact form - fixed at the bottom of the page */
    .open-button {
      background-color: #555;
      color: white;
      padding: 16px 20px;
      border: none;
      cursor: pointer;
      opacity: 0.8;
      position: fixed;
      bottom: 23px;
      right: 28px;
      width: 280px;


    }

    /* The popup form - hidden by default */
    .form-popup {
      display: none;
      position: fixed;
    left: 50%;
    top: 50%;

    transform: translate(-50%, -50%);
    z-index: 1001;}

    /* Add styles to the form container */
    .form-container {
      max-width: 330px;
      padding: 20px;
    border-radius: 10px ;
      background-color: white;
    }

    .form-popup  h5{
        margin-bottom: 20px;
        font-weight: bold;
        text-align: center;
    }

    /* Full-width input fields */
    .form-container input[type=text], .form-container input[type=password] {
      width: 100%;
      padding: 15px;
      margin: 5px 0 22px 0;
      border: none;
      background: #f1f1f1;
    }

    /* When the inputs get focus, do something */
    .form-container input[type=text]:focus, .form-container input[type=password]:focus {
      background-color: #ddd;
      outline: none;
    }

    /* Set a style for the submit/login button */
    .form-container .btn {
      background-color: #04AA6D;
      color: white;
      padding: 16px 20px;
      border: none;
      cursor: pointer;
      width: 100%;
      margin-bottom:10px;
      opacity: 0.8;
    }

    /* Add a red background color to the cancel button */
    .form-container .cancel {
      background-color: #a80d2c;
      padding: 12px 20px;
    }

    /* Add some hover effects to buttons */
    .form-container .btn:hover, .open-button:hover {
      opacity: 1;
    }
    /*.board_page{
        position: relative;
        z-index: 5;
    }*/
    #palette {
       width: 100% !important;
    box-shadow: 0 4px 10px rgb(120 120 119 / 15%);
    background: white;
    height: 450px;
}
#myDiagramDiv {
    background-color: #FFFFFF;
    -webkit-box-shadow: 0 4px 10px rgb(120 120 119 / 15%);
    box-shadow: 0 4px 10px rgb(120 120 119 / 15%);
}
.b_left {
    position: relative;
}
span.d {
    position: relative;
    background: white;
    width: 95%;
    height: 70px;
    display: block;
    z-index: 4;
    position: absolute;
}
.b_btn {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 20px;
}
.b_btn .btn{
        margin: 0 15px 0 0;
}
.b_btn .btn1 {
    background: #a80d2c !important;
    color: white !important;

}
.b_btn .btn2 {
    background: transparent !important;
    color: #a80d2c !important;

}
.inspector{
    margin-left: 30px;
    background-color: white !important;
        box-shadow: 0 4px 10px rgb(120 120 119 / 15%);
    margin-top: 15px;
}
.inspector input, select {
    background-color: white !important;
    color: black !important;
    font: bold 12px helvetica, sans-serif;
    border: 0px;
    padding: 2px;
    border: solid 1px #ddd !important;
    padding: 10px !important;
    border-radius: 5px;
}
.inspector  select {
    width: 100%;
}
.inspector td, th {
    padding: 2px;
    color: #a80d2c;
    padding: 7px !important;
}
span.d.d2 {
    width: 180px;
}
</style>
<link rel="stylesheet" href="{{ asset('front/css/DataInspector.css') }}" />

@endpush
<div class="body-section bg-gray structural-body-sec  h-auto">

    <!-- Recent Projects section -->
    <section class="recent-projects-sec structural-sec el_form bords_page">
        <div class="d-flex structural-sec-inner">


            <div class="structural-col w-100">
                <!-- Inner Banner -->
                <section class="inner-bnr board_page"></section>
                <div class="container-fluid">
                    <div class="title-sec">
                        <div class="row m-0">
                            {{-- <div class="col-lg-4 col-md-6 mb-md-0 mb-5 p-0 bords_cate">

                                <div class="structural-details-col  electrical_page_top  bg-transparent">
                                    <div class="row" id="drag-items">
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                        <img src="{{ asset('front/images/b1.png')}}" alt="" id="pv_module"   draggble="true">
                                                        <h4 >PV MODULE</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b2.png')}}" alt="" id="micro_inverter"  draggble="true">
                                                    <h4>MICRO INVERTER</h4>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b3.png')}}" alt="" id="solar_load_center"  draggble="true">
                                                    <h4>SOLAR LOAD CENTER</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box active"  >
                                                    <img src="{{ asset('front/images/b4.png')}}" alt="" id="non_fused_disconnect"  draggble="true">
                                                    <h4>NON FUSED DISCONNECT</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b5.png')}}" alt="" id="tesla_gateway"  draggble="true">
                                                    <h4>TESLA GATEWAY</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b6.png')}}" alt="" id="fused_disconnect"  draggble="true">
                                                    <h4>FUSED DISCONNECT</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b7.png')}}" alt="" id="solardeck"  draggble="true">
                                                    <h4>SOLADECK</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b8.png')}}" alt="" id="battery"  draggble="true">
                                                    <h4>BATTERY</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b9.png')}}" alt="" id="meter"  draggble="true">
                                                    <h4>METER</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b10.png')}}" alt="" id="monitoring"  draggble="true">
                                                    <h4>MONITORING</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b11.png')}}" alt="" id="ats"  draggble="true">
                                                    <h4>ATS</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b12.png')}}" alt="" id="junction_box"  draggble="true">
                                                    <h4>JUNCTION BOX</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b13.png')}}" alt="" id="optimizer"  draggble="true">
                                                    <h4>OPTIMIZR</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b14.png')}}" alt="" id="transformer"  draggble="true">
                                                    <h4>TRANSFORMER</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="item">
                                                <div class="board_box"  >
                                                    <img src="{{ asset('front/images/b15.png')}}" alt="" id="dc_inverter"  draggble="true">
                                                    <h4>DC INVERTER</h4>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div> --}}

                            {{-- <div class="col-lg-8 col-md-6">
                                <div class="inventer_properties_sec board_circuit mt-0">
                                    <h2 class="c_text">Images Diagram</h2>
                                    <div  class="structural-details-col" >

                                    <div id="container"></div>
                                    </div>
                                </div>
                            </div> --}}
                             <div class="col-lg-12 col-md-12 mb-md-0 mb-5 p-0 bords_cate">

                                <div id="sample">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="b_left">
                                            <span class="d"></span>
                                            <div id="palette"></div>
                                        </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="b_left">
                                            <span class="d d2"></span>
                                             <div id="myDiagramDiv" style="flex-grow: 1; height: 450px; "></div>
                                               <span style="display: inline-block; vertical-align: top; width:150px">
                                        <div id="myInspector">
                                        </div>
                                      </span>
                                  </div>
                                        </div>



                                    </div>

                                     <div class="b_btn">
                                      <!-- <div>
                                        <button id="saveModel" onclick="save()">Save</button>
                                        <button onclick="load()">Load</button>
                                        Diagram Model saved in JSON format:
                                      </div>
                                      Add port to selected nodes:
                                      <button onclick="addPort('top')">Top</button>
                                      <button onclick="addPort('bottom')">Bottom</button>
                                      <button onclick="addPort('left')">Left</button>
                                      <button onclick="addPort('right')">Right</button> -->
                                      <button onclick="addPort(true)">Add Input</button>
                                        <button onclick="addPort(false)">Add Output</button>
                                        <button onclick="removePort(true)">Remove Input</button>
                                        <button onclick="removePort(false)">Remove Output</button>
                                      <button class="btn btn1" onclick="addPort('top')">Inputs</button>

                                      <button class="btn btn2" onclick="generateImages()">Pdf Download</button>
                                      <textarea id="mySavedModel" style="width:100%;height:200px;display:none;">


                                        { "class": "go.GraphLinksModel",
                                            "copiesArrays": true,
                                            "copiesArrayObjects": true,
                                            "linkFromPortIdProperty": "fromPort",
                                            "linkToPortIdProperty": "toPort",
                                            "nodeDataArray": [

                                            ],
                                            "linkDataArray": [

                                            ]}

                                          </textarea>
                                    </div>
                                  </div>
                             </div>

                        </div>
                    </div>

                </div>

            </div>


        </div>
</div>

<div class="form-popup" id="myForm">
  <form action="/action_page.php" class="form-container">
    <h5>Diagram Add Input And Output Port</h5>

    <label for="input_port"><b>Input</b></label>
    <input type="text" placeholder="Enter Input Port" name="input_port" required>

    <label for="output_port"><b>Output</b></label>
    <input type="text" placeholder="Enter Output Port" name="output_port" required>

    <button type="button" class="btn cancel" onclick="closeForm()">Submit</button>
  </form>
</div>
<div class="overlay"></div>

@push('scripts')

<script>

function openForm() {
  document.getElementById("myForm").style.display = "block";
  $(".overlay").show();
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
  $(".overlay").hide();
}
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{ asset('front/js/go.js') }}"></script>
<script src="{{ asset('front/js/Figures.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script src="{{ asset('front/js/DataInspector.js') }}"></script>
<script id="code">
function  generateImages() {
    save();
    load();
    var diagram = document.getElementById('myDiagramDiv');

    width = 900;
    height = 700;
    if (isNaN(width)) width = 100;
    if (isNaN(height)) height = 100;
    // Give a minimum size of 50x50
    width = Math.max(width, 50);
    height = Math.max(height, 50);
    var imgDiv = document.getElementById('myImages');
    console.log(imgDiv)
    // imgDiv.innerHTML = ‘’; // clear out the old images, if any
    var db = this.myDiagram.documentBounds.copy();
    var boundswidth = db.width;
    var boundsheight = db.height;
    var imgWidth = width;
    var imgHeight = height;
    var p = db.position.copy();
    //making images
    for (var i = 0; i< boundsheight; i += imgHeight) {
        var img;
        for (var j = 0; j < boundswidth; j += imgWidth) {
            img= this.myDiagram.makeImage({
            scale: 1,
            type: "image/jpeg",
            background: "white",
            position: new go.Point(p.x + j, p.y + i),
            size: new go.Size(imgWidth, imgHeight)
            });
        }
    }
    var doc = new jsPDF();
    doc.addImage(img.src, 'JPEG', 15, 40, 240, 180);
    //if you need more page use addPage();
    // doc.addPage();
    doc.save('diagram.pdf');
    }
    var red = "orangered";  // 0 or false
    var green = "forestgreen";  // 1 or true

    function init() {
      var $ = go.GraphObject.make;  // for conciseness in defining templates

      myDiagram =
        $(go.Diagram, "myDiagramDiv",  // create a new Diagram in the HTML DIV element "myDiagramDiv"
          {
            "draggingTool.isGridSnapEnabled": true,  // dragged nodes will snap to a grid of 10x10 cells
            "undoManager.isEnabled": true
          });

      // when the document is modified, add a "*" to the title and enable the "Save" button
      myDiagram.addDiagramListener("Modified", function(e) {
        var button = document.getElementById("saveModel");
        if (button) button.disabled = !myDiagram.isModified;
        var idx = document.title.indexOf("*");
        if (myDiagram.isModified) {
          if (idx < 0) document.title += "*";
        } else {
          if (idx >= 0) document.title = document.title.substr(0, idx);
        }
      });

      var palette = new go.Palette("palette");  // create a new Palette in the HTML DIV element "palette"

      // creates relinkable Links that will avoid crossing Nodes when possible and will jump over other Links in their paths
      myDiagram.linkTemplate =
        $(go.Link,
          {
            routing: go.Link.AvoidsNodes,
            curve: go.Link.JumpOver,
            corner: 3,
            relinkableFrom: true, relinkableTo: true,
            selectionAdorned: false, // Links are not adorned when selected so that their color remains visible.
            shadowOffset: new go.Point(0, 0), shadowBlur: 5, shadowColor: "blue",
          },
          new go.Binding("isShadowed", "isSelected").ofObject(),
          $(go.Shape,
            { name: "SHAPE", strokeWidth: 2, stroke: red }));

      // node template helpers
      var sharedToolTip =
        $("ToolTip",
          { "Border.figure": "RoundedRectangle" },
          $(go.TextBlock, { margin: 2 },
            new go.Binding("text", "", function(d) { return d.category; })));

      // define some common property settings
      function nodeStyle() {
        return [new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        new go.Binding("isShadowed", "isSelected").ofObject(),
        {
          selectionAdorned: false,
          shadowOffset: new go.Point(0, 0),
          shadowBlur: 15,
          shadowColor: "blue",
          toolTip: sharedToolTip
        }];
      }

      function shapeStyle() {
        return {
          name: "NODESHAPE",
          fill: "lightgray",
          stroke: "darkslategray",
          desiredSize: new go.Size(40, 40),
          strokeWidth: 2
        };
      }

      function portStyle(input) {
        return {
          desiredSize: new go.Size(6, 6),
          fill: "black",
          fromSpot: go.Spot.Right,
          fromLinkable: !input,
          toSpot: go.Spot.Left,
          toLinkable: input,
          toMaxLinks: 1,
          cursor: "pointer"
        };
      }



      function makePortTemplate(input) {
        var p =
          $(go.Panel,
            {
              margin: new go.Margin(2, 0),
              toolTip:
                $("ToolTip",
                  $(go.TextBlock,
                    { margin: 3 },
                    new go.Binding("text", "id"))
                )
            },
            new go.Binding("portId", "id"),
            $(go.Shape, { width: 6, height: 6 },
              new go.Binding("fill"))
          );
        if (input) {
          p.toLinkable = true;
          p.toSpot = go.Spot.Left;
          p.margin = new go.Margin(5, 90, 0, 0);
        } else {
          p.fromLinkable = true;
          p.fromSpot = go.Spot.Right;

        }
        return p;
      }



    var pvModuleTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
        $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                )
            }
        ),
        $(go.Panel, "Vertical",
            { column: 0, itemTemplate: makePortTemplate(true) },
            new go.Binding("itemArray", "inputs")
          ),
          $(go.Panel, "Vertical",
            { column: 1, itemTemplate: makePortTemplate(false) },
            new go.Binding("itemArray", "outputs")
          ),
        $(go.Picture,
            { margin: 10, background: "white" },
                new go.Binding("source")),

        ),

        );

        // For micro invertor
        var microInverterTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
        $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                )
            }
        ),
        $(go.Picture,
                { margin: 10, width: 50, height: 50, background: "white" },
                new go.Binding("source")),
        $(go.TextBlock,
            "Micro Invertor",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),
        ),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out", alignment: new go.Spot(1, 0.45) })
        );
        // Solar load center template
        var solarLoadCenterTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
        $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                )
            }
        ),
        $(go.Picture,
                { margin: 10, width: 80, height: 200, background: "white" },
                new go.Binding("source")),
        $(go.TextBlock,
            "Solar Load Center",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),
        ),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out", alignment: new go.Spot(1, 0.45) })
        );

       // For Non fused disconnect Template
       var nonFusedDisconnectTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
        $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                )
            }
        ),
        $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "#ccc" },
                new go.Binding("source")),
        $(go.TextBlock,
            "Non fused Disconnect",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),
        ),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out", alignment: new go.Spot(1, 0.45) })
        );
        // For tesla gateway
        var teslaGatewayTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
        $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                )
            }
        ),
        $(go.Picture,
                { margin: 10, width: 80, height: 200, background: "white" },
                new go.Binding("source")),
        $(go.TextBlock,
            "Tesla Gateway",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),
        ),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out", alignment: new go.Spot(1, 0.4) }),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out", alignment: new go.Spot(1, 0.6) })
        );

        // For fused gateway
        var fusedDisconnectTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
        $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                )
            }
        ),
        $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
        $(go.TextBlock,
            "Fused Disconnect",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),
        ),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out", alignment: new go.Spot(1, 0.5) })

        );
    // For Solardeck Template
    var soladeckTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
        $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                )
            }
        ),
        $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
        $(go.TextBlock,
            "Soladeck",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),
        ),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out1", alignment: new go.Spot(1, 0.2) }),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out2", alignment: new go.Spot(1, 0.4) }),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out3", alignment: new go.Spot(1, 0.6) }),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out4", alignment: new go.Spot(1, 0.8) }),

        );
    // For battery
    var batteryTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
        $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                )
            }
        ),
        $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
        $(go.TextBlock,
            "Battery",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),
        ),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out1", alignment: new go.Spot(1, 0.5) })

        );
     // For meter template
     var meterTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
        $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                )
            }
        ),
        $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
        $(go.TextBlock,
            "Meter",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),
        ),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out1", alignment: new go.Spot(1, 0.5) })

        );
    // For monitoring template
    var monitoringTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
        $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                )
            }
        ),
        $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
        $(go.TextBlock,
            "Monitoring",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),
        ),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out1", alignment: new go.Spot(1, 0.5) })

        );
        // Ats template
        var atsTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
        $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                )
            }
        ),
        $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
        $(go.TextBlock,
            "ATS",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),
        ),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out1", alignment: new go.Spot(1, 0.5) })

        );
        // JUnctionbox template
        var junctionBoxTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
        $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                )
            }
        ),
        $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
        $(go.TextBlock,
            "Junction Box Template",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),
        ),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out1", alignment: new go.Spot(1, 0.2) }),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out2", alignment: new go.Spot(1, 0.4) }),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out3", alignment: new go.Spot(1, 0.6) }),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out4", alignment: new go.Spot(1, 0.8) })

        );
        // Optimizer template
        var optimizrTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
        $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                )
            }
        ),
        $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
        $(go.TextBlock,
            "Optimizer",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),
        ),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out1", alignment: new go.Spot(1, 0.45) })

        );
// Transformer template
var transformerTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
        $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                )
            }
        ),
        $(go.Picture,
                { margin: 10, width: 80, height: 100, background: "white" },
                new go.Binding("source")),
        $(go.TextBlock,
            "Transformer",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),
        ),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out1", alignment: new go.Spot(1, 0.45) })

        );
      // Dc inverter template
      var dcInverterTemplate =
        $(go.Node, "Spot", nodeStyle(),
          { background: "#fff" },
        $(go.Panel, "Table",
        $(go.Panel, "Table",
            new go.Binding("itemArray", "inputPorts"),
            {
                margin: new go.Margin(4, 0),
                alignment: go.Spot.Left,
                stretch: go.GraphObject.Vertical,
                rowSizing: go.RowColumnDefinition.ProportionalExtra,
                itemTemplate:
                $(go.Panel, "TableRow",
                    $(go.Shape, "Rectangle", portStyle(true), new go.Binding("portId"))
                )
            }
        ),
        $(go.Picture,
                { margin: 10, width: 80, height: 200, background: "white" },
                new go.Binding("source")),
        $(go.TextBlock,
            "Dc Inverter",
            { row: 1, column: 0, margin: 0, stroke: "black", font: "bold 10px sans-serif", },
                new go.Binding("text", "name")),
        ),
        $(go.Shape, "Rectangle", portStyle(false),
            { portId: "out1", alignment: new go.Spot(1, 0.45) })

        );


    var blankTemplate =
              $(go.Node, "Spot", nodeStyle(),
              { background: "#fff" },
                $(go.Picture,
                    { width: 80, height: 60, background: "white" },
                    new go.Binding("source")));


          // add the templates created above to myDiagram and palette
          myDiagram.nodeTemplateMap.add("blank", blankTemplate);
          myDiagram.nodeTemplateMap.add("pvModule", pvModuleTemplate);
          myDiagram.nodeTemplateMap.add("microInverter", microInverterTemplate);
          myDiagram.nodeTemplateMap.add("solarLoadCenter", solarLoadCenterTemplate);
          myDiagram.nodeTemplateMap.add("soladeck", soladeckTemplate);
          myDiagram.nodeTemplateMap.add("nonFusedDisconnect", nonFusedDisconnectTemplate);
          myDiagram.nodeTemplateMap.add("teslaGateway", teslaGatewayTemplate);
          myDiagram.nodeTemplateMap.add("fusedDisconnect", fusedDisconnectTemplate);
          myDiagram.nodeTemplateMap.add("battery", batteryTemplate);
          myDiagram.nodeTemplateMap.add("meter", meterTemplate);
          myDiagram.nodeTemplateMap.add("monitoring", monitoringTemplate);
          myDiagram.nodeTemplateMap.add("ats", atsTemplate);
          myDiagram.nodeTemplateMap.add("junctionBox", junctionBoxTemplate);
          myDiagram.nodeTemplateMap.add("optimizr", optimizrTemplate);
          myDiagram.nodeTemplateMap.add("transformer", transformerTemplate);
          myDiagram.nodeTemplateMap.add("dcInverter", dcInverterTemplate);
        //   $(function() {
        //         myDiagram.mouseDrop = function(e) {
        //             openForm();
        //         }
        //   });

      // share the template map with the Palette
      palette.nodeTemplateMap = myDiagram.nodeTemplateMap;

      palette.model.nodeDataArray = [
        { category: "blank", source: "{{ asset('front/images/blank.png')}}",numInputs: 0, inputPorts: [] },
        { category: "blank",  source: "{{ asset('front/images/blank.png')}}", numInputs: 0, inputPorts: [] },
        { category: "blank", source: "{{ asset('front/images/blank.png')}}", numInputs: 0, inputPorts: []},

        // { category: "pvModule", source: "{{ asset('front/images/b1.png')}}", numInputs: 0,numOutputs: 1, inputPorts: [],outputPorts: [] },
        { category: "pvModule", source: "{{ asset('front/images/b1.png')}}", inputs: [{ id: "A" }, { id: "B" }],
            outputs: [{ id: "W" }, { id: "X" }, { id: "Y" }, { id: "Z" }]  },
        { category: "microInverter", source: "{{ asset('front/images/b2.png')}}", numInputs: 1, inputPorts: [{ portId: "in1" }] ,outputPorts: []},

        { category: "nonFusedDisconnect", source: "{{ asset('front/images/b4.png')}}", numInputs: 1, inputPorts: [{ portId: "in1" }],outputPorts: [] },

        { category: "fusedDisconnect", source: "{{ asset('front/images/b6.png')}}", numInputs: 1, inputPorts: [{ portId: "in1" }] ,outputPorts: []},
        { category: "soladeck", source: "{{ asset('front/images/b7.png')}}", numInputs: 4, inputPorts: [{ portId: "in1" },{ portId: "in2" },{ portId: "in3" },{ portId: "in4" }] ,outputPorts: []},
        { category: "battery", source: "{{ asset('front/images/b8.png')}}", numInputs: 0, inputPorts: [] ,outputPorts: [],outputPorts: []},
        { category: "meter", source: "{{ asset('front/images/b9.png')}}", numInputs: 1, inputPorts: [{ portId: "in1" }] ,outputPorts: []},
        { category: "monitoring", source: "{{ asset('front/images/b10.png')}}", numInputs: 0, inputPorts: [] ,outputPorts: []},
        { category: "ats", source: "{{ asset('front/images/b11.png')}}", numInputs: 2, inputPorts: [{ portId: "in1" },{ portId: "in2" }] ,outputPorts: []},
        { category: "junctionBox", source: "{{ asset('front/images/b12.png')}}", numInputs: 4, inputPorts: [{ portId: "in1" },{ portId: "in2" },{ portId: "in3" },{ portId: "in4" }] ,outputPorts: []},
        { category: "optimizr", source: "{{ asset('front/images/b13.png')}}", numInputs: 1, inputPorts: [{ portId: "in1" }] ,outputPorts: []},
        { category: "transformer", source: "{{ asset('front/images/b14.png')}}", numInputs: 1, inputPorts: [{ portId: "in1" }] ,outputPorts: []},
        { category: "solarLoadCenter", source: "{{ asset('front/images/b3.png')}}", numInputs: 1, inputPorts: [{ portId: "in1" }] ,outputPorts: []},
        { category: "teslaGateway", source: "{{ asset('front/images/b5.png')}}" , numInputs: 1, inputPorts: [{ portId: "in1" }] ,outputPorts: []},
        { category: "dcInverter", source: "{{ asset('front/images/b15.png')}}", numInputs: 10, inputPorts: [{ portId: "in1" },{ portId: "in2" },{ portId: "in3" },{ portId: "in4" },{ portId: "in5" },{ portId: "in6" },{ portId: "in7" },{ portId: "in8" },{ portId: "in9" },{ portId: "in10" }] ,outputPorts: [] },

        ];
      // support editing the properties of the selected person in HTML
      if (window.Inspector) {
        // Inspector helper function
        function updateNumInputs(name, value) {
           console.log("myInspector.inspectedObject",myInspector.inspectedObject.data);
          if (name !== "numInputs") return;
          var data = myInspector.inspectedObject.data;
          //console.log("data",data);
          var inputs = data.inputPorts;
          var outputs = data.outputPorts;
          console.log("outputs",outputs);

          if (value > inputs.length) {
            while (value > inputs.length) {
              myDiagram.model.addArrayItem(inputs, { portId: "in" + (inputs.length+1).toString() });
            }
          } else if (value < inputs.length) {
            while (value < inputs.length) {
              myDiagram.model.removeArrayItem(inputs, inputs.length - 1);
            }
          }

        }

        function updateNumOutputs(name, value) {

          if (name !== "numOutputs") return;

          var data = myInspector.inspectedObject.data;
          console.log("data",data);
          var outputs = data.outputPorts;
          console.log("outputs",outputs);

          if (value > outputs.length) {
            console.log("value > outputs.length",value +" > "+ outputs.length);
            while (value > outputs.length) {
              myDiagram.model.addArrayItem(outputs, { portId: "out" + (outputs.length+1).toString() });
            }
          } else if (value < outputs.length) {
            while (value < outputs.length) {
              myDiagram.model.removeArrayItem(outputs, outputs.length - 1);
            }
          }

        }

        myInspector = new Inspector("myInspector", myDiagram,
        {
          properties: {
            "key": { show: false },
            "category": { show: false },
            "source": { show: false },
            "loc": { show: false },
            "from":{ show: false },
            "to":{ show: false },
            "fromPort":{ show: false },
            "toPort":{ show: false },
            //"text": { },
            //"fill": { type: "color", defaultValue: "lightgray", show: Inspector.showIfNode },
            //"stroke": { type: "color", defaultValue: "black", show: Inspector.showIfLink },
            "inputs": { type: "select", choices: [1,2,3,4,5], defaultValue: 2, show: Inspector.showIfPresent },
            "outputs": { type: "select", choices: [1,2,3,4,5], defaultValue: 2, show: Inspector.showIfPresent },
            // "numInputs": { type: "number",defaultValue: 2, show: Inspector.showIfPresent },
            "numOutputs": { type: "number",defaultValue: 2, show: Inspector.showIfPresent },
            "inputPorts": { show: false},
            "outputPorts": { show: false}
          },
          propertyModified: updateNumInputs,
          //propertyModified: updateNumOutputs,
        });
      }



      // load the initial diagram
      load();

      // continually update the diagram
      loop();


    }

    function findNextPortId(node) {
      let i = 0;
      const defaultport = node.port;
      while (node.findPort("P" + i) !== defaultport) i++;
      return "P" + i;
    }

    function addPort(input) {
      myDiagram.commit(diag => {
        diag.selection.each(n => {
            console.log("n >> "+JSON.stringify(n.data));
          if (!(n instanceof go.Node)) return;
          const m = diag.model;
          m.addArrayItem(input ? n.data.inputs : n.data.outputs, { id: findNextPortId(n) });
        })
      });
    }

    function removePort(input) {
      myDiagram.commit(diag => {
        diag.selection.each(n => {
          if (!(n instanceof go.Node)) return;
          const m = diag.model;
          const arr = input ? n.data.inputs : n.data.outputs;
          if (arr.length > 0) m.removeArrayItem(arr, arr.length-1);
        })
      });
    }

    // update the diagram every 250 milliseconds
    function loop() {
      setTimeout(function() { updateStates(); loop(); }, 250);
    }

    // update the value and appearance of each node according to its type and input values
    function updateStates() {
      var oldskip = myDiagram.skipsUndoManager;
      myDiagram.skipsUndoManager = true;
      // do all "input" nodes first
      myDiagram.nodes.each(function(node) {
        if (node.category === "input") {
          doInput(node);
        }
      });
      // now we can do all other kinds of nodes
      myDiagram.nodes.each(function(node) {
        switch (node.category) {
          case "and": doAnd(node); break;
          case "or": doOr(node); break;
          case "xor": doXor(node); break;
          case "not": doNot(node); break;
          case "nand": doNand(node); break;
          case "nor": doNor(node); break;
          case "xnor": doXnor(node); break;
          case "output": doOutput(node); break;
          case "input": break;  // doInput already called, above
        }
      });
      myDiagram.nodes.each(function(node) {
        switch (node.name) {
          case "Don Meow": doAnd(node); break;
        }
      });
      myDiagram.skipsUndoManager = oldskip;
    }

    // helper predicate
    function linkIsTrue(link) {  // assume the given Link has a Shape named "SHAPE"
      return link.findObject("SHAPE").stroke === green;
    }

    // helper function for propagating results
    function setOutputLinks(node, color) {
      node.findLinksOutOf().each(function(link) { link.findObject("SHAPE").stroke = color; });
    }

    // update nodes by the specific function for its type
    // determine the color of links coming out of this node based on those coming in and node type

    function doInput(node) {
      // the output is just the node's Shape.fill
      setOutputLinks(node, node.findObject("NODESHAPE").fill);
    }

    function doAnd(node) {
      var color = node.findLinksInto().all(linkIsTrue) ? green : red;
      setOutputLinks(node, color);
    }
    function doNand(node) {
      var color = !node.findLinksInto().all(linkIsTrue) ? green : red;
      setOutputLinks(node, color);
    }
    function doNot(node) {
      var color = !node.findLinksInto().all(linkIsTrue) ? green : red;
      setOutputLinks(node, color);
    }

    function doOr(node) {
      var color = node.findLinksInto().any(linkIsTrue) ? green : red;
      setOutputLinks(node, color);
    }
    function doNor(node) {
      var color = !node.findLinksInto().any(linkIsTrue) ? green : red;
      setOutputLinks(node, color);
    }

    function doXor(node) {
      var truecount = 0;
      node.findLinksInto().each(function(link) { if (linkIsTrue(link)) truecount++; });
      var color = truecount % 2 !== 0 ? green : red;
      setOutputLinks(node, color);
    }
    function doXnor(node) {
      var truecount = 0;
      node.findLinksInto().each(function(link) { if (linkIsTrue(link)) truecount++; });
      var color = truecount % 2 === 0 ? green : red;
      setOutputLinks(node, color);
    }

    function doOutput(node) {
      // assume there is just one input link
      // we just need to update the node's Shape.fill
      node.linksConnected.each(function(link) { node.findObject("NODESHAPE").fill = link.findObject("SHAPE").stroke; });
    }

    // save a model to and load a model from JSON text, displayed below the Diagram
    function save() {
      document.getElementById("mySavedModel").value = myDiagram.model.toJson();
      myDiagram.isModified = false;
    }
    function load() {
      myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
    }


    window.addEventListener('DOMContentLoaded', init);

  </script>


@endpush
@endsection
