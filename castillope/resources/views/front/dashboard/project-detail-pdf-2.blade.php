<!DOCTYPE html>
<html>
<head>
	<title>	{{ $data['page_title'] }}</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="{{url('front/images/favicon.ico')}}">

	<style type="text/css">
		.main {
			max-width: 600px;
			margin: 0 auto;
			text-align: center;
			border: solid 1px black;
			margin-top: 20px;

			padding: 20px;

		}
		.structural-details-col {
			display: flex;
			justify-content: space-between;
			box-shadow: 0 0 2px rgb(0 0 0 / 16%);

			margin: 0 auto;
			border-radius: 2px;
			margin-bottom: 20px;

			padding: 0 15px;
		}
		.details-panel {
			border-right: solid 2px #ddd;
			padding-right: 12px;
			width: 30%;
		}
		.p_head h2 {
			font-size: 18px;
			text-transform: uppercase;
			font-family: sans-serif;
			border-bottom: solid 1px #ddd;
			padding: 12px 0;
			color: white;
			margin: 0;
			/* background: #a80d2c; */
			background: linear-gradient(
				135deg
				/* , #a80d2c 0%, #78021a 34%, #78021a 99%); */
				, #808080	 0%, #808080	 34%, #808080	 99%);

		}
        .p_head td,th {
            width:25%;
        }
		table {
			font-family: arial, sans-serif;
			border-collapse: collapse;
			width: 100%;
		}

		td, th {
			border: 1px solid #dddddd;
			text-align: left;
			padding: 10px 5px;
		}




		tr:nth-child(even) {
			background-color: #f6f6f6;
		}
		.width_de {
			margin-top: 20px;
		}
		.roff_1_deatils h4, .width_de h4{
			text-align: left;
			font-size: 16px;
			text-transform: uppercase;
			font-family: sans-serif;
			border-bottom: solid 1px #ddd;
			padding: 12px 10px;
			color: white;
			margin: 0;
			/* background: #a80d2c; */
			background: linear-gradient(
				135deg
				/* , #a80d2c 0%, #78021a 34%, #78021a 99%); */
				, #808080	 0%, #808080	 34%, #808080	 99%);


		}
		.roff_1_deatils {
    margin-bottom: 20px;
}
.project_deatils {
    margin-bottom: 20px;
    /* margin-top: 20px; */
    border: 1px solid #dddddd;
}
.project_deatils td, .project_deatils th {
			border-left: none;
            border-bottom: none;
            border-top: none;

		}

	</style>
</head>
<body>
@php
    $project = $data['project'];
@endphp
	<div class="main">
		<div class="p_head">
			<h2>Project Details</h2>
            <table>
                <tr>
                    <th>Owner Name</th>
                    <td>{{ $project->owner_name }}</td>
                    <th>Address</th>
                    <td>{{ $project->address }}</td>

                </tr>
                <tr>
                    <th>State</th>
                    <td>{{ $project->state }}</td>
                    <th>Ult Wind Speed</th>
                    <td>{{ $project->ult_wind_speed }}</td>

                </tr>
                <tr>
                    <th>Ground Snow Load</th>
                    <td>{{ $project->ground_snow_load }}</td>
                    <th>Ahj</th>
                    <td>{{ $project->ahj }}</td>
                </tr>


            </table>
		</div>
        @if($project->site)
        <div class="roff_1_deatils" style="margin-top: 20px;">
			<h4>Site Info Details</h4>
            <table>
                <tr>
                    <th>ASCE Version</th>
                    <td>{{ $project->site->asce_version }}</td>
                    <th>Exposure Category</th>
                    <td>{{ $project->site->exposure_category }}</td>
                </tr>
                <tr>
                    <th>Ultimate Wind Speed</th>
                    <td>{{ $project->site->ultimate_wind_speed }}</td>
                    <th>Normal Wind Speed</th>
                    <td>{{ $project->site->normal_wind_speed }}</td>
                </tr>
                <tr>
                    <th>Risk Category</th>
                    <td>{{ $project->site->risk_category }}</td>
                    <th>Mean Roof Height (ft)</th>
                    <td>{{ $project->site->mean_roof_height }}</td>
                </tr>
                <tr>
                    <th>Roof Length (ft)</th>
                    <td>{{ $project->site->roof_length }}</td>
                    <th>Roof Width (ft)</th>
                    <td>{{ $project->site->roof_width }}</td>
                </tr>
                <tr>
                    <th>Ground Snow Load (psf)</th>
                    <td>{{ $project->site->ground_snow_load }}</td>
                    <th>Dead Load (psf)</th>
                    <td>{{ $project->site->dead_load }}</td>
                </tr>
                <tr>
                    <th>Live Load (psf)</th>
                    <td>{{ $project->site->live_load }}</td>
                    <th>Seismic Load (psf)</th>
                    <td>{{ $project->site->seismic_load }}</td>
                </tr>
                <tr>
                    <th>Roof Type</th>
                    <td>{{ $project->site->roof_type }}</td>
                    <th>HVHZ</th>
                    <td>{{ $project->site->hvhz }}</td>
                </tr>
                <tr>
                    <th>Parapet Height (ft)</th>
                    <td>{{ $project->site->parapet_height }}</td>
                    <th>Ground Elevation (ft)</th>
                    <td>{{ $project->site->ground_elevation }}</td>
                </tr>
                <tr>
                    <th>Sloped Roof Snow Load (psf)</th>
                    <td>{{ $project->site->sloped_roof_snow_load }}</td>
                    <th>Exposure Factor (Ce)</th>
                    <td>{{ $project->site->exposure_factor }}</td>
                </tr>
                <tr>
                    <th>Importance Factor (Is)</th>
                    <td>{{ $project->site->importance_factor }}</td>
                    <th>Slope Factor (Cs)</th>
                    <td>{{ $project->site->slope_factor }}</td>
                </tr>
                <tr>
                    <th>Wind Directionality Factor (Kd)</th>
                    <td>{{ $project->site->wind_directionality_factor }}</td>
                    <th>Topographic Factor (Kzt)</th>
                    <td>{{ $project->site->topographic_factor }}</td>
                </tr>
                <tr>
                    <th>Ke</th>
                    <td>{{ $project->site->ke }}</td>
                    <th>Kz</th>
                    <td>{{ $project->site->kz }}</td>
                </tr>
                <tr>
                    <th>Temperature Factor (Ct)</th>
                    <td>{{ $project->site->temperature_factor }}</td>
                </tr>


            </table>
		</div>
		<div class="project_deatils">
			<div class="width_de">
				<h4>	WIDTH OF PRESSURE COEFFICIENT</h4>
                <table>
                    <tr>
                        <th width="25%"><p><span id="wp1">{{ $project->site->PressureCoefficient->wp1 ?? 0 }}</span>' * 10%</p></th>
                        <td width="25%"><p><span id="wp1_val">{{ $project->site->PressureCoefficient->wp1_val ?? 0 }}</span>'</p>
                        </td>
                        <th width="25%">ZONE WIDTH A</th>
                        <td width="25%"><span id="zone1">{{ $project->site->PressureCoefficient->zone1 ?? 0 }}</span></td>

                    </tr>
                    <tr>
                        <th width="25%"><p><span id="wp2">{{ $project->site->PressureCoefficient->wp2 ?? 0 }}</span>' * 40%</p></th>
                        <td width="25%"><p><span id="wp2_val">{{ $project->site->PressureCoefficient->wp2_val ?? 0 }}</span>'</p></td>
                        @if($project->site && $project->site->asce_version == 'asce-7-16')
                        <th width="25%">ZONE 2 WIDTH</th>
                        <td width="25%"><span id="zone2">{{ $project->site->PressureCoefficient->zone2 ?? 0 }}</span></td>
                        @endif
                    </tr>
                    @if($project->site && $project->site->asce_version == 'asce-7-16')
                    <tr>
                        <th width="50%" colspan="2">&nbsp;</th>
                        <th width="25%" >ZONE 3 WIDTH</th>
                        <td width="25%"><span id="zone3">{{ $project->site->PressureCoefficient->zone3 ?? 0 }}</span></td>
                    </tr>
                    @endif



                </table>
				{{-- <div class="structural-details-col">
					<div class="details-panel">
						<p><span id="wp1">70</span>' * 10%</p>
						<p><span id="wp2">30</span>' * 40%</p>
					</div>
					<div class="details-panel">
						<p><span id="wp1_val">7</span>'</p>
						<p><span id="wp2_val">12</span>'</p>
					</div>
					<div class="details-panel details-panel-last ml-auto" style="border: none;">
						<p>ZONE WIDTH A  : <span id="zone1">7FT</span></p>
						<p class="asce-version-table-7-16" style="display: none;">ZONE 2 WIDTH  : <span id="zone2">N/A</span></p>
						<p class="asce-version-table-7-16" style="display: none;">ZONE 3 WIDTH  : <span id="zone3">N/A</span></p>
                     <div class="d-flex align-items-center">
                      <p class="d-flex w-100">3' <span class="ml-auto">0"</span></p>
                  </div>
              </div> --}}
          </div>
      </div>
      @endif
      @if($project->RoofInfo)
      @foreach($project->RoofInfo as $roof)
      <div class="roff_1_deatils">
      	<h4>	Roof {{ $loop->iteration }}</h4>
      	<table>
      		<tr>
                <th>Module Manufacturer</th>
                <td>{{ $roof->manufacturer->title }}</td>
                <th>Attachment Manufacturer</th>
                <td>{{ $roof->attachment_manufacturer->title }}</td>
      		</tr>
            <tr>
                <th>PV Module</th>
                <td>{{ $roof->module->module_model }}</td>
                <th>Attachment </th>
                <td>{{ $roof->attachment->model_no }}</td>
      		</tr>
            <tr>
                <th>Module Length</th>
                <td>{{ $roof->module_length }}</td>
                <th>Attachment type</th>
                <td>{{ $roof->attachment_type }}</td>
      		</tr>
            <tr>
                <th>Module Width</th>
                <td>{{ $roof->module_width }}</td>
                <th>Compression Strength</th>
                <td>{{ $roof->compression_strength }}</td>
      		</tr>
            <tr>
                <th>Module Orientation</th>
                <td>{{ $roof->module_orientation }}</td>
                <th>Pullout Strength</th>
                <td>{{ $roof->pullout_strength }}</td>
      		</tr>
            <tr>
                <th>Module Area (sq.ft.)</th>
                <td>{{ $roof->module_area}}</td>
                <th>Roof Height (ft)</th>
                <td>{{ $roof->roof_height}}</td>
      		</tr>
            <tr>
                <th>Limit Max Span To (in)</th>
                <td>{{ $roof->limit_max_span_to }}</td>
                <th>Rafter/Seam Spacing (in)</th>
                <td>{{ $roof->rafter_seam_spacing }}</td>
      		</tr>
            <tr>
                <th>Roof Slope</th>
                <td>{{ $roof->roof_slope }}/12</td>
                <th>Roof Slope (º)</th>
                <td>{{ $roof->roof_slope_degree }}</td>
      		</tr>
            <tr>
                @if($project->site->asce_version == 'asce-7-10')
                <th>No of Rails</th>
                <td>{{ $roof->no_of_rails }}</td>
                @endif
                <th>Effective Wind Area</th>
                <td>{{ $roof->effective_wind_area }}</td>
      		</tr>
            @if($project->site->asce_version == 'asce-7-16')
            <tr>
                <th>No of Rails Exposed</th>
                <td>{{ $roof->no_of_rails_exposed }}</td>
                <th>No of Rails Non-Exposed</th>
                <td>{{ $roof->no_of_rails_non_exposed }}</td>
      		</tr>
            @endif

      	</table>
      	<h4 style="margin-top:10px;">Zone Info</h4>
        @if($project->site->asce_version == 'asce-7-10')
        <table>
            <tr>
              <th>&nbsp;</th>
              <th>Zone 1</th>
              <th>Zone 2</th>
              <th>Zone 3</th>
            </tr>
            <tr>
                <th>External Positive Pressure Coefficient</th>
                <th>{{ ($roof->ext_pos_pre_coef0_7_10 == 0)?"X":$roof->ext_pos_pre_coef0_7_10 }}</th>
                <th>{{ ($roof->ext_pos_pre_coef1_7_10 == 0)?"X":$roof->ext_pos_pre_coef1_7_10 }}</th>
                <th>{{ ($roof->ext_pos_pre_coef2_7_10 == 0)?"X":$roof->ext_pos_pre_coef1_7_10 }}</th>
            </tr>
            <tr>
                <th>External Negative Pressure Coefficient</th>
                <th>{{ ($roof->ext_neg_pre_coef0_7_10 == 0)?"X":$roof->ext_neg_pre_coef0_7_10 }}</th>
                <th>{{ ($roof->ext_neg_pre_coef1_7_10 == 0)?"X":$roof->ext_neg_pre_coef1_7_10 }}</th>
                <th>{{ ($roof->ext_neg_pre_coef2_7_10 == 0)?"X":$roof->ext_neg_pre_coef2_7_10 }}</th>
            </tr>
            <tr>
                <th>Positive Pressure</th>
                <th>{{ ($roof->positive_pressure0_7_10 == 0)?"X":$roof->positive_pressure0_7_10 }}</th>
                <th>{{ ($roof->positive_pressure1_7_10 == 0)?"X":$roof->positive_pressure1_7_10 }}</th>
                <th>{{ ($roof->positive_pressure2_7_10 == 0)?"X":$roof->positive_pressure2_7_10 }}</th>
            </tr>
            <tr>
                <th>Negative Pressure</th>
                <th>{{ ($roof->negative_pressure0_7_10 == 0)?"X":$roof->negative_pressure0_7_10 }}</th>
                <th>{{ ($roof->negative_pressure1_7_10 == 0)?"X":$roof->negative_pressure1_7_10 }}</th>
                <th>{{ ($roof->negative_pressure2_7_10 == 0)?"X":$roof->negative_pressure2_7_10 }}</th>
            </tr>
            <tr>
                <th>Positive Loading</th>
                <th>{{ ($roof->positive_loading0_7_10 == 0)?"X":$roof->positive_loading0_7_10 }}</th>
                <th>{{ ($roof->positive_loading1_7_10 == 0)?"X":$roof->positive_loading1_7_10 }}</th>
                <th>{{ ($roof->positive_loading2_7_10 == 0)?"X":$roof->positive_loading2_7_10 }}</th>
            </tr>
            <tr>
                <th>Negative Loading</th>
                <th>{{ ($roof->negative_loading0_7_10 == 0)?"X":$roof->negative_loading0_7_10 }}</th>
                <th>{{ ($roof->negative_loading1_7_10 == 0)?"X":$roof->negative_loading1_7_10 }}</th>
                <th>{{ ($roof->negative_loading2_7_10 == 0)?"X":$roof->negative_loading2_7_10 }}</th>
            </tr>
        </table>
        @endif
        @if($project->site->asce_version == 'asce-7-16')
        <table  style="font-size: 25px;">
            <tr>
              <th>&nbsp;</th>
              <th>Zone 1</th>
              <th>Zone 1'</th>
              <th>Zone 2e</th>
              <th>Zone 2n</th>
              <th>Zone 2r</th>
              <th>Zone 3e</th>
              <th>Zone 3r</th>
            </tr>
            <tr>
                <th>External Positive Pressure Coefficient</th>
                <td>{{ ($roof->ext_pos_pre_coef0 == 0)?"X":$roof->ext_pos_pre_coef0 }}</td>
                <td>{{ ($roof->ext_pos_pre_coef1 == 0)?"X":$roof->ext_pos_pre_coef1 }}</td>
                <td>{{ ($roof->ext_pos_pre_coef2 == 0)?"X":$roof->ext_pos_pre_coef2 }}</td>
                <td>{{ ($roof->ext_pos_pre_coef3 == 0)?"X":$roof->ext_pos_pre_coef3 }}</td>
                <td>{{ ($roof->ext_pos_pre_coef4 == 0)?"X":$roof->ext_pos_pre_coef4 }}</td>
                <td>{{ ($roof->ext_pos_pre_coef5 == 0)?"X":$roof->ext_pos_pre_coef5 }}</td>
                <td>{{ ($roof->ext_pos_pre_coef6 == 0)?"X":$roof->ext_pos_pre_coef6 }}</td>
            </tr>
            <tr>
                <th>External Negative Pressure Coefficient</th>
                <td>{{ ($roof->ext_neg_pre_coef0 == 0)?"X":$roof->ext_neg_pre_coef0 }}</td>
                <td>{{ ($roof->ext_neg_pre_coef1 == 0)?"X":$roof->ext_neg_pre_coef1 }}</td>
                <td>{{ ($roof->ext_neg_pre_coef2 == 0)?"X":$roof->ext_neg_pre_coef2 }}</td>
                <td>{{ ($roof->ext_neg_pre_coef3 == 0)?"X":$roof->ext_neg_pre_coef3 }}</td>
                <td>{{ ($roof->ext_neg_pre_coef4 == 0)?"X":$roof->ext_neg_pre_coef4 }}</td>
                <td>{{ ($roof->ext_neg_pre_coef5 == 0)?"X":$roof->ext_neg_pre_coef5 }}</td>
                <td>{{ ($roof->ext_neg_pre_coef6 == 0)?"X":$roof->ext_neg_pre_coef6 }}</td>
            </tr>
            <tr>
                <th>Kz</th>
                <td>{{ ($roof->zone_kz0 == 0)?"X":$roof->zone_kz0 }}</td>
                <td>{{ ($roof->zone_kz1 == 0)?"X":$roof->zone_kz1 }}</td>
                <td>{{ ($roof->zone_kz2 == 0)?"X":$roof->zone_kz2 }}</td>
                <td>{{ ($roof->zone_kz3 == 0)?"X":$roof->zone_kz3 }}</td>
                <td>{{ ($roof->zone_kz4 == 0)?"X":$roof->zone_kz4 }}</td>
                <td>{{ ($roof->zone_kz5 == 0)?"X":$roof->zone_kz5 }}</td>
                <td>{{ ($roof->zone_kz6 == 0)?"X":$roof->zone_kz6 }}</td>
            </tr>
            <tr>
                <th>Positive Pressure</th>
                <td>{{ ($roof->positive_pressure0 == 0)?"X":$roof->positive_pressure0 }}</td>
                <td>{{ ($roof->positive_pressure1 == 0)?"X":$roof->positive_pressure1 }}</td>
                <td>{{ ($roof->positive_pressure2 == 0)?"X":$roof->positive_pressure2 }}</td>
                <td>{{ ($roof->positive_pressure3 == 0)?"X":$roof->positive_pressure3 }}</td>
                <td>{{ ($roof->positive_pressure4 == 0)?"X":$roof->positive_pressure4 }}</td>
                <td>{{ ($roof->positive_pressure5 == 0)?"X":$roof->positive_pressure5 }}</td>
                <td>{{ ($roof->positive_pressure6 == 0)?"X":$roof->positive_pressure6 }}</td>
            </tr>
            <tr>
                <th>Negative Pressure</th>
                <td>{{ ($roof->negative_pressure0 == 0)?"X":round($roof->negative_pressure0,4) }}</td>
                <td>{{ ($roof->negative_pressure1 == 0)?"X":round($roof->negative_pressure1,4) }}</td>
                <td>{{ ($roof->negative_pressure2 == 0)?"X":round($roof->negative_pressure2,4) }}</td>
                <td>{{ ($roof->negative_pressure3 == 0)?"X":round($roof->negative_pressure3,4) }}</td>
                <td>{{ ($roof->negative_pressure4 == 0)?"X":round($roof->negative_pressure4,4) }}</td>
                <td>{{ ($roof->negative_pressure5 == 0)?"X":round($roof->negative_pressure5,4) }}</td>
                <td>{{ ($roof->negative_pressure6 == 0)?"X":round($roof->negative_pressure6,4) }}</td>
            </tr>
            <tr>
                <th>Adjusted Positive Pressure</th>
                <td>{{ ($roof->adjusted_pos_pre0 == 0)?"X":$roof->adjusted_pos_pre0 }}</td>
                <td>{{ ($roof->adjusted_pos_pre1 == 0)?"X":$roof->adjusted_pos_pre1 }}</td>
                <td>{{ ($roof->adjusted_pos_pre2 == 0)?"X":$roof->adjusted_pos_pre2 }}</td>
                <td>{{ ($roof->adjusted_pos_pre3 == 0)?"X":$roof->adjusted_pos_pre3 }}</td>
                <td>{{ ($roof->adjusted_pos_pre4 == 0)?"X":$roof->adjusted_pos_pre4 }}</td>
                <td>{{ ($roof->adjusted_pos_pre5 == 0)?"X":$roof->adjusted_pos_pre5 }}</td>
                <td>{{ ($roof->adjusted_pos_pre6 == 0)?"X":$roof->adjusted_pos_pre6 }}</td>
            </tr>
            <tr>
                <th>Adjusted Exposed Pressure</th>
                <td>{{ ($roof->adjusted_exp_neg_pre0 == 0)?"X":$roof->adjusted_exp_neg_pre0 }}</td>
                <td>{{ ($roof->adjusted_exp_neg_pre1 == 0)?"X":$roof->adjusted_exp_neg_pre1 }}</td>
                <td>{{ ($roof->adjusted_exp_neg_pre2 == 0)?"X":$roof->adjusted_exp_neg_pre2 }}</td>
                <td>{{ ($roof->adjusted_exp_neg_pre3 == 0)?"X":$roof->adjusted_exp_neg_pre3 }}</td>
                <td>{{ ($roof->adjusted_exp_neg_pre4 == 0)?"X":$roof->adjusted_exp_neg_pre4 }}</td>
                <td>{{ ($roof->adjusted_exp_neg_pre5 == 0)?"X":$roof->adjusted_exp_neg_pre5 }}</td>
                <td>{{ ($roof->adjusted_exp_neg_pre6 == 0)?"X":$roof->adjusted_exp_neg_pre6 }}</td>
            </tr>
            <tr>
                <th>Adjusted Non Exposed Pressure</th>
                <td>{{ ($roof->adjusted_non_exp_neg_pre0 == 0)?"X":$roof->adjusted_non_exp_neg_pre0 }}</td>
                <td>{{ ($roof->adjusted_non_exp_neg_pre0 == 0)?"X":$roof->adjusted_non_exp_neg_pre0 }}</td>
                <td>{{ ($roof->adjusted_non_exp_neg_pre0 == 0)?"X":$roof->adjusted_non_exp_neg_pre0 }}</td>
                <td>{{ ($roof->adjusted_non_exp_neg_pre0 == 0)?"X":$roof->adjusted_non_exp_neg_pre0 }}</td>
                <td>{{ ($roof->adjusted_non_exp_neg_pre0 == 0)?"X":$roof->adjusted_non_exp_neg_pre0 }}</td>
                <td>{{ ($roof->adjusted_non_exp_neg_pre0 == 0)?"X":$roof->adjusted_non_exp_neg_pre0 }}</td>
                <td>{{ ($roof->adjusted_non_exp_neg_pre0 == 0)?"X":$roof->adjusted_non_exp_neg_pre0 }}</td>
            </tr>
            <tr>
                <th>Positive Loading</th>
                <td>{{ ($roof->positive_loading0 == 0)?"X":$roof->positive_loading0 }}</td>
                <td>{{ ($roof->positive_loading1 == 0)?"X":$roof->positive_loading1 }}</td>
                <td>{{ ($roof->positive_loading2 == 0)?"X":$roof->positive_loading2 }}</td>
                <td>{{ ($roof->positive_loading3 == 0)?"X":$roof->positive_loading3 }}</td>
                <td>{{ ($roof->positive_loading4 == 0)?"X":$roof->positive_loading4 }}</td>
                <td>{{ ($roof->positive_loading5 == 0)?"X":$roof->positive_loading5 }}</td>
                <td>{{ ($roof->positive_loading6 == 0)?"X":$roof->positive_loading6 }}</td>
            </tr>
            <tr>
                <th>Negative Exposed Loading</th>
                <td>{{ ($roof->negative_exposed_loading0 == 0)?"X":$roof->negative_exposed_loading0 }}</td>
                <td>{{ ($roof->negative_exposed_loading1 == 0)?"X":$roof->negative_exposed_loading1 }}</td>
                <td>{{ ($roof->negative_exposed_loading2 == 0)?"X":$roof->negative_exposed_loading2 }}</td>
                <td>{{ ($roof->negative_exposed_loading3 == 0)?"X":$roof->negative_exposed_loading3 }}</td>
                <td>{{ ($roof->negative_exposed_loading4 == 0)?"X":$roof->negative_exposed_loading4 }}</td>
                <td>{{ ($roof->negative_exposed_loading5 == 0)?"X":$roof->negative_exposed_loading5 }}</td>
                <td>{{ ($roof->negative_exposed_loading6 == 0)?"X":$roof->negative_exposed_loading6 }}</td>
            </tr>
            <tr>
                <th>Negative Non Exposed Loading</th>
                <td>{{ ($roof->negative_non_exposed_loading0 == 0)?"X":$roof->negative_non_exposed_loading0 }}</td>
                <td>{{ ($roof->negative_non_exposed_loading1 == 0)?"X":$roof->negative_non_exposed_loading1 }}</td>
                <td>{{ ($roof->negative_non_exposed_loading2 == 0)?"X":$roof->negative_non_exposed_loading2 }}</td>
                <td>{{ ($roof->negative_non_exposed_loading3 == 0)?"X":$roof->negative_non_exposed_loading3 }}</td>
                <td>{{ ($roof->negative_non_exposed_loading4 == 0)?"X":$roof->negative_non_exposed_loading4 }}</td>
                <td>{{ ($roof->negative_non_exposed_loading5 == 0)?"X":$roof->negative_non_exposed_loading5 }}</td>
                <td>{{ ($roof->negative_non_exposed_loading6 == 0)?"X":$roof->negative_non_exposed_loading6 }}</td>
            </tr>
            <tr>
                <th>Attachment Exposed Negative Spacing</th>
                <td>{{ ($roof->attachment_exposed_negative_spacing0 == 0)?"X":$roof->attachment_exposed_negative_spacing0 }}</td>
                <td>{{ ($roof->attachment_exposed_negative_spacing1 == 0)?"X":$roof->attachment_exposed_negative_spacing1 }}</td>
                <td>{{ ($roof->attachment_exposed_negative_spacing2 == 0)?"X":$roof->attachment_exposed_negative_spacing2 }}</td>
                <td>{{ ($roof->attachment_exposed_negative_spacing3 == 0)?"X":$roof->attachment_exposed_negative_spacing3 }}</td>
                <td>{{ ($roof->attachment_exposed_negative_spacing4 == 0)?"X":$roof->attachment_exposed_negative_spacing4 }}</td>
                <td>{{ ($roof->attachment_exposed_negative_spacing5 == 0)?"X":$roof->attachment_exposed_negative_spacing5 }}</td>
                <td>{{ ($roof->attachment_exposed_negative_spacing6 == 0)?"X":$roof->attachment_exposed_negative_spacing6 }}</td>
            </tr>
            <tr>
                <th>Attachment Non Exposed Negative Spacing</th>
                <td>{{ ($roof->attachment_non_exposed_negative_spacing0 == 0)?"X":$roof->attachment_non_exposed_negative_spacing0 }}</td>
                <td>{{ ($roof->attachment_non_exposed_negative_spacing1 == 0)?"X":$roof->attachment_non_exposed_negative_spacing1 }}</td>
                <td>{{ ($roof->attachment_non_exposed_negative_spacing2 == 0)?"X":$roof->attachment_non_exposed_negative_spacing2 }}</td>
                <td>{{ ($roof->attachment_non_exposed_negative_spacing3 == 0)?"X":$roof->attachment_non_exposed_negative_spacing3 }}</td>
                <td>{{ ($roof->attachment_non_exposed_negative_spacing4 == 0)?"X":$roof->attachment_non_exposed_negative_spacing4 }}</td>
                <td>{{ ($roof->attachment_non_exposed_negative_spacing5 == 0)?"X":$roof->attachment_non_exposed_negative_spacing5 }}</td>
                <td>{{ ($roof->attachment_non_exposed_negative_spacing6 == 0)?"X":$roof->attachment_non_exposed_negative_spacing6 }}</td>
            </tr>
        </table>
        @endif
      </div>
      @endforeach
      @endif





  </div>
</div>
</body>
</html>
