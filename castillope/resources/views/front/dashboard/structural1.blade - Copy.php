@extends('front.layouts.app')
@section('title','Login')
@section('content')

@push('styles')
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
@endpush
<!-- Inner Banner -->
<!-- <section class="inner-bnr"></section> -->

 <!-- Recent Projects section -->
 <section class="recent-projects-sec structural-sec">
    <div class="d-flex structural-sec-inner">
      <div class="sidebar">
        <div class="sidebar-inner">
          <button id="close-btn" class="d-lg-none d-block">X</button>
          <h5>Please fill the details first </h5>
        @include('front.includes.message')
        <div class="message"></div>
          <form id="create-site-form" action="@if($site){{url('site/'.$site->id)}}@else {{url('site')}} @endif" method="post">
          @csrf
          @if($site)
          @method("PUT")
          @endif

          <input type="hidden" name="velocity_pressure" id="velocity_pressure" value="" />
          <input type="hidden" name="project_id" id="project_id" value="{{$project_id}}" />
          <input type="hidden" name="roof_no" id="roof_no" value="{{count($roofinfos)}}" />
          <input type="hidden" name="site_formula" id="site_formula" value="{{url('site-formula')}}" />
            <div class="input-box">

              <div class="form-group height-sec">
                  <label for="">Select FBC Version</label>
                  <div class="select-area form-control" style="width:100%;">
                      <select name="asce_version" id="asce_version">
                         <option value=""> Select ASCE Version</option>

                       <option value="asce-7-10" @if($site) {{$site->asce_version=="asce-7-10" ? "selected":""}} @endif>ASCE 7-10</option>
                       <option value="asce-7-16" @if($site) {{$site->asce_version=="asce-7-16" ? "selected":""}} @endif>ASCE 7-16</option>

                      </select>
                  </div>
              </div>

              <div class="form-group">
                <label for="">Mean Roof Height (ft)</label>
                <input type="text" class="form-control  kz_cal numeric_feild_discount" placeholder="" name="mean_roof_height" id="mean_roof_height" value="{{$site->mean_roof_height ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Roof Length (ft)</label>
                <input type="text" class="form-control  numeric_feild_discount" placeholder="" name="roof_length" id="roof_length" value="{{$site->roof_length ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Roof Width (ft)</label>
                <input type="text" class="form-control  numeric_feild_discount" placeholder="" name="roof_width" id="roof_width" value="{{$site->roof_width ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Parapet Height (ft)</label>
                <input type="text" class="form-control numeric_feild_discount" placeholder="" name="parapet_height" id="parapet_height" value="{{$site->parapet_height ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Module Length (in)</label>
                <input type="text" class="form-control site_module numeric_feild_discount" placeholder="" name="module_length" id="module_length" value="{{$site->module_length ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Module Width (in)</label>
                <input type="text" class="form-control site_module numeric_feild_discount" placeholder="" name="module_width" id="module_width" value="{{$site->module_width ?? ''}}">
              </div>

              <div class="form-group">
                <label for="">Module Orientation</label>
                <div class="select-area form-control" style="width:100%;">
                    <select name="module_orientation" id="module_orientation">
                         <option value=""> Select Module Orientation</option>
                         <option value="PORTRAIT" @if($site) {{$site->module_orientation=='PORTRAIT' ? "selected":""}} @endif>PORTRAIT</option>
                         <option value="LANDSCAPE" @if($site) {{$site->module_orientation=='LANDSCAPE' ? "selected":""}} @endif>LANDSCAPE</option>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label for="">Module Area (sq. ft.)</label>
                <input type="text" class="form-control numeric_feild_discount" placeholder="" name="module_area" id="module_area" value="{{$site->module_area ?? ''}}" readonly>
              </div>
              <div class="form-group">
                <label for="">Ground Snow Load (psf)</label>
                <input type="text" class="form-control numeric_feild_discount cal" placeholder="" name="ground_snow_load" id="ground_snow_load" value="{{$site->ground_snow_load ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Dead Load (psf)</label>
                <input type="text" class="form-control numeric_feild_discount" placeholder="" name="dead_load" id="dead_load" value="{{$site->dead_load ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Live Load (psf)</label>
                <input type="text" class="form-control numeric_feild_discount" placeholder="" name="live_load" id="live_load" value="{{$site->live_load ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Seismic Load (psf)</label>
                <input type="text" class="form-control numeric_feild_discount" placeholder="" name="seismic_load" id="seismic_load" value="{{$site->seismic_load ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Ground Elevation (ft)</label>
                <input type="text" class="form-control ground_elevation numeric_feild_discount" placeholder="" name="ground_elevation" id="ground_elevation" value="{{$site->ground_elevation ?? ''}}" >
              </div>
              <div class="form-group">
                <label for="">Exposure Factor (Ce)</label>
                <input type="text" class="form-control numeric_feild_discount cal" placeholder="" name="exposure_factor" id="exposure_factor" value="{{$site->exposure_factor ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Temperature Factor (Ct)</label>
                <div class="select-area form-control" style="width:100%;">
                    <select class="cal" name="temperature_factor" id="temperature_factor">
                         <option value="">Select Temperature Factor</option>
                         <option value="1.0" @if($site) {{$site->temperature_factor==1.0 ? "selected":""}} @endif>1.0</option>
                         <option value="1.1" @if($site) {{$site->temperature_factor==1.1 ? "selected":""}} @endif>1.1</option>
                         <option value="1.2" @if($site) {{$site->temperature_factor==1.2 ? "selected":""}} @endif>1.2</option>
                    </select>
                </div>

              </div>
              <div class="form-group">
                <label for="">Importance Factor (Is)</label>
                <input type="text" class="form-control numeric_feild_discount cal" placeholder="" name="importance_factor" id="importance_factor" value="{{$site->importance_factor ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Slope Factor (Cs)</label>
                <input type="text" class="form-control numeric_feild_discount cal" placeholder="" name="slope_factor" id="slope_factor" value="{{$site->slope_factor ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Wind Directionality Factor (Kd)</label>
                <input type="text" class="form-control numeric_feild_discount" placeholder="" name="wind_directionality_factor" id="wind_directionality_factor" value="{{$site->wind_directionality_factor ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Topographic Factor (Kzt)</label>
                <input type="text" class="form-control numeric_feild_discount" placeholder="" name="topographic_factor" id="topographic_factor" value="{{$site->topographic_factor ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Ke</label>
                <input type="text" class="form-control numeric_feild_discount" placeholder="" name="ke" id="ke" value="{{$site->ke ?? ''}}" readonly>
              </div>
              <div class="form-group">
                <label for="">Kz</label>
                <input type="text" class="form-control numeric_feild_discount" placeholder="" name="kz" id="kz" value="{{$site->kz ?? ''}}" readonly>
              </div>
              <div class="form-group">
                <label for="">Sloped Roof Snow Load (psf)</label>

                <input type="text" class="form-control numeric_feild_discount" placeholder="" name="sloped_roof_snow_load" id="sloped_roof_snow_load" value="{{$site->sloped_roof_snow_load ?? ''}}" readonly>
              </div>
              <div class="form-group">
                <label for="">Effective Wind Area (ft2)</label>
                <input type="text" class="form-control numeric_feild_discount" placeholder="" name="effective_wind_area" id="effective_wind_area" value="{{$site->effective_wind_area ?? ''}}" readonly>
              </div>


              {{-- <div class="form-group">
                <label for="">Building Length (ft)</label>
                <input type="text" class="form-control numeric_feild_discount" placeholder="" name="building_length" id="building_length" value="{{$site->building_length ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Building Width (ft)</label>
                <input type="text" class="form-control numeric_feild_discount" placeholder="" name="building_width" id="building_width" value="{{$site->building_width ?? ''}}">
              </div> --}}

                <div class="form-group">
                  <label for=""> Select Risk Category</label>
                  <div class="select-area form-control" style="width:100%;">
                      <select name="risk_category" id="risk_category">
                           <option value=""> Select Risk Category</option>
                           <option value="I" @if($site) {{$site->risk_category=='I' ? "selected":""}} @endif>I</option>
                           <option value="II" @if($site) {{$site->risk_category=='II' ? "selected":""}} @endif>II</option>
                           <option value="III" @if($site) {{$site->risk_category=='III' ? "selected":""}} @endif>III</option>
                           <option value="IV" @if($site) {{$site->risk_category=='IV' ? "selected":""}} @endif>IV</option>
                      </select>
                  </div>
                </div>




               <div class="form-group">
                  <label for=""> Select Exposure Category</label>
                  <div class="select-area form-control" style="width:100%;">
                      <select class="kz_cal"  name="exposure_category" id="exposure_category">
                       <option value=""> Select Exposure Category</option>
                       <option value="B" @if($site) {{$site->exposure_category=='B' ? "selected":""}} @endif>B</option>
                       <option value="C" @if($site) {{$site->exposure_category=='C' ? "selected":""}} @endif>C</option>
                       <option value="D" @if($site) {{$site->exposure_category=='D' ? "selected":""}} @endif>D</option>
                      </select>
                  </div>
                </div>




              <div class="form-group">
                <label for="">Ultimate Wind Speed</label>
                <input type="text" class="form-control numeric_feild_discount uws" placeholder="" name="ultimate_wind_speed" id="ultimate_wind_speed" value="{{$site->ultimate_wind_speed ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Normal Wind Speed</label>
                <input readonly type="text" class="form-control numeric_feild_discount" placeholder="" name="normal_wind_speed" id="normal_wind_speed" value="{{$site->normal_wind_speed ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Roof Slope</label>
                <input type="text" class="form-control  site_roof_slope numeric_feild_discount" placeholder="" name="roof_slope" id="roof_slope" value="{{$site->roof_slope ?? ''}}">
              </div>
              <div class="form-group">
                <label for="">Roof Slope (&#186;)</label>
                <input readonly type="text" class="form-control  numeric_feild_discount" placeholder="" name="roof_slope_degree" id="roof_slope_degree" value="{{$site->roof_slope_degree ?? ''}}" readonly>
              </div>

               <div class="form-group">
                  <label for=""> Select Roof Type</label>
                  <div class="select-area form-control" style="width:100%;">
                      <select  name="roof_type" id="roof_type">
                       <option value=""> Select Roof Type</option>
                       <option value="HIP" @if($site) {{$site->roof_type=='HIP' ? "selected":""}} @endif>HIP</option>
                       <option value="GABLE" @if($site) {{$site->roof_type=='GABLE' ? "selected":""}} @endif>GABLE</option>
                      </select>
                  </div>
                </div>



              <div class="form-group">
                  <label for=""> Select HVHZ</label>
                  <div class="select-area form-control " style="width:100%;">
                      <select name="hvhz" id="hvhz">
                         <option value=""> Select HVHZ</option>
                         <option value="YES" @if($site) {{$site->hvhz=='YES' ? "selected":""}} @endif>YES</option>
                         <option value="NO" @if($site) {{$site->hvhz=='NO' ? "selected":""}} @endif>NO</option>
                    </select>
                  </div>
              </div>




               {{-- <div class="form-group">
                  <label for=""> Select Type Of Building</label>
                  <div class="select-area form-control" style="width:100%;">
                      <select name="type_of_building" id="type_of_building">
                              <option value=""> Select Type Of Building</option>
                               <option value="RESIDENTIAL" @if($site) {{$site->type_of_building=='RESIDENTIAL' ? "selected":""}} @endif>RESIDENTIAL</option>
                               <option value="COMMERCIAL" @if($site) {{$site->type_of_building=='COMMERCIAL' ? "selected":""}} @endif>COMMERCIAL</option>
                        </select>
                  </div>
              </div> --}}





              <div class="form-group">
                <label for="">No Of Roofs</label>
                <input @if(count($roofinfos) > 0) readonly @endif type="text" class="form-control numeric_feild_discount" placeholder="" name="no_of_roofs" id="no_of_roofs" value="{{$site->no_of_roofs ?? ''}}">
              </div>
               {{-- <div class="form-group">
                <label for="">Zone Width</label>

                <input type="text" class="form-control numeric_feild_discount" placeholder="" name="zone_width" id="zone_width" value="{{$site->zone_width ?? ''}}">
              </div> --}}
            </div>

            <div class="btn-row"><button type="submit" class="btn btn-red w-100 create-site-submit" id="create-site-button">@if($site) Update @else Submit @endif</button></div>

          </form>
        </div>
      </div>

      <div class="structural-col">
        <!-- Inner Banner -->
        <section class="inner-bnr"></section>
        <div class="container-fluid">
          <div class="title-sec">
            <div class="navbar-light left-side-btn mb-4">
              <button class="navbar-toggler d-lg-none d-block">
                <span class="navbar-toggler-icon"></span>
              </button>
            </div>
            <div class="row">
              <div class="col-md-7 mb-md-0 mb-5">
                <h5 class="text-uppercase">Width Of Pressure Coefficient</h5>
                <div class="structural-details-col d-flex">
                  <div class="details-panel">
                    <p><span id="wp1">{{ $pressure_coefficient->wp1 ?? 0 }}</span>' * 10%</p>
                    <p><span id="wp2">{{ $pressure_coefficient->wp2 ?? 0 }}</span>' * 40%</p>
                  </div>
                  <div class="equals"><span class="icon-equal"><span class="path1"></span><span class="path2"></span></span></div>
                  <div class="details-panel">
                    <p><span id="wp1_val">{{ $pressure_coefficient->wp1_val ?? 0 }}</span>'</p>
                    <p><span id="wp2_val">{{ $pressure_coefficient->wp2_val ?? 0 }}</span>'</p>
                  </div>
                  <div class="details-panel details-panel-last ml-auto">
                    <p>ZONE WIDTH A  : <span id="zone1">{{ $pressure_coefficient->zone1 ?? 0 }}</span></p>
                    <p>ZONE 2 WIDTH  : <span id="zone2">{{ $pressure_coefficient->zone2 ?? 0 }}</span></p>
                    <p>ZONE 3 WIDTH  : <span id="zone3">{{ $pressure_coefficient->zone3 ?? 0 }}</span></p>
                    <!-- <div class="d-flex align-items-center">
                      <p class="d-flex w-100">3' <span class="ml-auto">0"</span></p>
                    </div> -->
                  </div>
                </div>


              </div>
              <div class="col-md-5 align-self-center text-right">
                 <button id="add-roof" type="button" class="btn2 btn-red w-50 col text-center add-roof @if(count($roofinfos) ==  0) d-none @endif"  > Add Roof </button>

              </div>
            </div>
          </div>

          <!-- Accordian Section -->
          <div id="accordion" class="accordian-collapse structural-collapse">

            <div class="card d-none" id="roof-blueprint">
              <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                <h5 class="d-flex align-items-center mb-0" ><span class="roof_no"> 1 </span><span class="icon  ml-auto"><i class="fas fa-plus-circle"></i></span></h5>
              </div>
            <input type="hidden" name="roof_info_id[]" value="0" />

              <div id="collapseOne" class="collapse-body collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">

                    <div class="row roof-container" id="roof1">
                      <div class="col-md-7 mb-md-0 mb-5">
                        <div class="row">
                          <div class="col-md-5 col-sm-6">
                            <div class="form-group">
                                <label for="pvmodule">Module Manufacturer</label>
                                <div class="select-area form-control" style="width:100%;">
                                    <select name="manufacturer_id[]" class="manufacturer_id" url="{{url('module-manufacturer')}}"  >
                                      <option value="">Select Module Manufacturer</option>
                                       @foreach($manufacturers as $manufacturer)
                                      <option value="{{$manufacturer->id}}">{{ $manufacturer->title }}</option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-5 offset-md-1  col-sm-6">
                            <div class="form-group">
                                <label for="pvmodule">Attachment Manufacturer</label>
                                <div class="select-area form-control" style="width:100%;">
                                    <select name="attachment_manufacturer_id[]"  class="attachment_manufacturer_id" url="{{url('attachment')}}">
                                      <option value="">Select Attachment Manufacturer</option>
                                      @foreach($manufacturers as $manufacturer)
                                      <option value="{{$manufacturer->id}}">{{ $manufacturer->title }}</option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-5  col-sm-6">
                            <div class="form-group">
                                <label for="pvmodule">PV Module</label>
                                <div class="select-area form-control" style="width:100%;">
                                    <select name="module_id[]"  class="module_id" url="{{url('module')}}">
                                      <option value="">Select Pv Module</option>
                                    </select>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-5 offset-md-1 col-sm-6">
                            <div class="form-group">
                                <label for="pvmodule">Attachment</label>
                                <div class="select-area form-control" style="width:100%;">
                                    <select name="attachment_id[]"  class="attachment_id">
                                      <option value="">Select Attachment</option>

                                    </select>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-5 col-sm-6">
                            <div class="form-group">
                                <label for="mlength">Module Length</label>
                                <input readonly type="text" name="module_length[]"  class="form-control input-style2 module_length numeric_feild_discount" id="mlength" placeholder="">
                            </div>
                          </div>
                          <div class="col-md-5 offset-md-1 col-sm-6">
                            <div class="form-group">
                                <label for="attachmenttype">Attachment type</label>
                                <div class="select-area form-control" style="width:100%;">
                                    <select name="attachment_type[]"  class="attachment_type">
                                      <option value="">Select Attachment Type</option>
                                      <option value="Rafter">Rafter</option>
                                      <option value="Seam">Seam</option>
                                      <option value="Purlin">Purlin</option>
                                      <option value="Decking">Decking</option>
                                    </select>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-5 col-sm-6">
                            <div class="form-group">
                                <label for="mwidth">Module Width</label>
                                <input readonly type="text" class="form-control input-style2 module_width numeric_feild_discount" name="module_width[]" placeholder="">
                            </div>
                          </div>
                          <div class="col-md-5 offset-md-1 col-sm-6">
                            <div class="form-group">
                                <label for="strenght">Compression Strenght</label>
                                <input readonly type="text" name="compression_strenght[]"  class="form-control input-style2 compression_strenght numeric_feild_discount" placeholder="">
                            </div>
                          </div>

                          <div class="col-md-5 col-sm-6">
                            <div class="form-group">
                                <label for="morientation">Module Orientation</label>
                                <div class="select-area form-control" style="width:100%;">
                                    <select name="module_orientation[]"  class="module_orientation">
                                    <option value="PORTRAIT" >PORTRAIT</option>
                                    <option value="LANDSCAPE" >LANDSCAPE</option>
                                    </select>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-5 offset-md-1 col-sm-6">
                            <div class="form-group">
                                <label for="pullout_strenght">Pullout Strenght</label>
                                <input readonly type="text" name="pullout_strenght[]"  class="form-control input-style2 pullout_strenght numeric_feild_discount"  placeholder="">
                            </div>
                          </div>


                          <div class="col-md-5 col-sm-6">
                          <div class="form-group">
                                <label for="roof_height">Roof Height (ft)</label>
                                <div class="select-area form-control" style="width:100%;">
                                <input type="text" name="roof_height[]"  class="form-control input-style2 roof_height numeric_feild_discount"  placeholder="">
                                </div>
                            </div>

                          </div>
                          <div class="col-md-5 offset-md-1 col-sm-6">
                          <div class="form-group">
                                <label for="no_of_rails">No of Rails</label>
                                <input  type="text" name="no_of_rails[]" class="form-control input-style2 no_of_rails numeric_feild_discount"  placeholder="">
                            </div>
                          </div>


                          <div class="col-md-5 col-sm-6">
                          <div class="form-group">
                                <label for="limit_max_span_to">Limit Max Span To (in)</label>
                                <input  type="text" name="limit_max_span_to[]" class="form-control input-style2 limit_max_span_to numeric_feild_discount" placeholder="">
                          </div>

                          </div>
                          <div class="col-md-5 offset-md-1 col-sm-6">
                          <div class="form-group">
                                <label for="rafter_seam_spacing">Rafter/Seam Spacing (in)</label>
                                <input type="text" name="rafter_seam_spacing[]" class="form-control input-style2 rafter_seam_spacing numeric_feild_discount"  placeholder="">
                            </div>
                          </div>
                          <div class="col-md-5 col-sm-6">
                          <div class="form-group">
                                <label for="roof_slope">Roof Slope</label>
                                <input type="text" name="roof_slope[]" class="form-control input-style2 roof_slope numeric_feild_discount" placeholder="">
                            </div>
                          </div>




                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="roof-table">
                          <div class="table-responsive">
                            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th scope="col"></th>
                                  <th scope="col">Zone 1</th>
                                  <th scope="col">Zone 1'</th>
                                  <th scope="col">Zone 2e</th>
                                  <th scope="col">Zone 2n</th>
                                  <th scope="col">Zone 2r</th>
                                  <th scope="col">Zone 3e</th>
                                  <th scope="col">Zone 3r</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <th scope="row">External Positive Pressure Coefficient </th>
                                  <td><span class="ext_pos_pre_coef0"></span><input type="hidden" class="ext_pos_pre_coef0" name="ext_pos_pre_coef0[]" /></td>
                                  <td><span class="ext_pos_pre_coef1"></span><input type="hidden" class="ext_pos_pre_coef1" name="ext_pos_pre_coef1[]" /></td>
                                  <td><span class="ext_pos_pre_coef2"></span><input type="hidden" class="ext_pos_pre_coef2" name="ext_pos_pre_coef2[]" /></td>
                                  <td><span class="ext_pos_pre_coef3"></span><input type="hidden" class="ext_pos_pre_coef3" name="ext_pos_pre_coef3[]" /></td>
                                  <td><span class="ext_pos_pre_coef4"></span><input type="hidden" class="ext_pos_pre_coef4" name="ext_pos_pre_coef4[]" /></td>
                                  <td><span class="ext_pos_pre_coef5"></span><input type="hidden" class="ext_pos_pre_coef5" name="ext_pos_pre_coef5[]" /></td>
                                  <td><span class="ext_pos_pre_coef6"></span><input type="hidden" class="ext_pos_pre_coef6" name="ext_pos_pre_coef6[]" /></td>
                                </tr>
                                <tr>
                                  <th scope="row">External Negative Pressure Coefficient </th>
                                  <td><span class="ext_neg_pre_coef0"></span><input type="hidden" class="ext_neg_pre_coef0" name="ext_neg_pre_coef0[]" /></td>
                                  <td><span class="ext_neg_pre_coef1"></span><input type="hidden" class="ext_neg_pre_coef1" name="ext_neg_pre_coef1[]" /></td>
                                  <td><span class="ext_neg_pre_coef2"></span><input type="hidden" class="ext_neg_pre_coef2" name="ext_neg_pre_coef2[]" /></td>
                                  <td><span class="ext_neg_pre_coef3"></span><input type="hidden" class="ext_neg_pre_coef3" name="ext_neg_pre_coef3[]" /></td>
                                  <td><span class="ext_neg_pre_coef4"></span><input type="hidden" class="ext_neg_pre_coef4" name="ext_neg_pre_coef4[]" /></td>
                                  <td><span class="ext_neg_pre_coef5"></span><input type="hidden" class="ext_neg_pre_coef5" name="ext_neg_pre_coef5[]" /></td>
                                  <td><span class="ext_neg_pre_coef6"></span><input type="hidden" class="ext_neg_pre_coef6" name="ext_neg_pre_coef6[]" /></td>
                                </tr>
                                <tr>
                                  <th scope="row">Kz </th>
                                  <td><span  class="zone_kz0"></span><input type="hidden" class="zone_kz0" name="zone_kz0[]" /></td>
                                  <td><span  class="zone_kz1"></span><input type="hidden" class="zone_kz1" name="zone_kz1[]" /></td>
                                  <td><span  class="zone_kz2"></span><input type="hidden" class="zone_kz2" name="zone_kz2[]" /></td>
                                  <td><span  class="zone_kz3"></span><input type="hidden" class="zone_kz3" name="zone_kz3[]" /></td>
                                  <td><span  class="zone_kz4"></span><input type="hidden" class="zone_kz4" name="zone_kz4[]" /></td>
                                  <td><span  class="zone_kz5"></span><input type="hidden" class="zone_kz5" name="zone_kz5[]" /></td>
                                  <td><span  class="zone_kz6"></span><input type="hidden" class="zone_kz6" name="zone_kz6[]" /></td>
                                </tr>
                                <tr>
                                  <th scope="row">Positive Pressure </th>
                                  <td><span class=" positive_pressure0"></span><input type="hidden" class="positive_pressure0" name="positive_pressure0[]" /></td>
                                  <td><span class=" positive_pressure1"></span><input type="hidden" class="positive_pressure1" name="positive_pressure1[]" /></td>
                                  <td><span class=" positive_pressure2"></span><input type="hidden" class="positive_pressure2" name="positive_pressure2[]" /></td>
                                  <td><span class=" positive_pressure3"></span><input type="hidden" class="positive_pressure3" name="positive_pressure3[]" /></td>
                                  <td><span class=" positive_pressure4"></span><input type="hidden" class="positive_pressure4" name="positive_pressure4[]" /></td>
                                  <td><span class=" positive_pressure5"></span><input type="hidden" class="positive_pressure5" name="positive_pressure5[]" /></td>
                                  <td><span class=" positive_pressure6"></span><input type="hidden" class="positive_pressure6" name="positive_pressure6[]" /></td>
                                </tr>
                                <tr>
                                  <th scope="row">Negative Pressure </th>
                                  <td><span class=" negative_pressure0"></span><input type="hidden" class="negative_pressure0" name="negative_pressure0[]" /></td>
                                  <td><span class=" negative_pressure1"></span><input type="hidden" class="negative_pressure1" name="negative_pressure1[]" /></td>
                                  <td><span class=" negative_pressure2"></span><input type="hidden" class="negative_pressure2" name="negative_pressure2[]" /></td>
                                  <td><span class=" negative_pressure3"></span><input type="hidden" class="negative_pressure3" name="negative_pressure3[]" /></td>
                                  <td><span class=" negative_pressure4"></span><input type="hidden" class="negative_pressure4" name="negative_pressure4[]" /></td>
                                  <td><span class=" negative_pressure5"></span><input type="hidden" class="negative_pressure5" name="negative_pressure5[]" /></td>
                                  <td><span class=" negative_pressure6"></span><input type="hidden" class="negative_pressure6" name="negative_pressure6[]" /></td>
                                </tr>
                                <tr>
                                  <th scope="row">Adjusted Positive Pressure </th>
                                  <td><span class=" adjusted_pos_pre0"></span><input type="hidden" class="adjusted_pos_pre0" name="adjusted_pos_pre0[]" /></td>
                                  <td><span class=" adjusted_pos_pre1"></span><input type="hidden" class="adjusted_pos_pre1" name="adjusted_pos_pre1[]" /></td>
                                  <td><span class=" adjusted_pos_pre2"></span><input type="hidden" class="adjusted_pos_pre2" name="adjusted_pos_pre2[]" /></td>
                                  <td><span class=" adjusted_pos_pre3"></span><input type="hidden" class="adjusted_pos_pre3" name="adjusted_pos_pre3[]" /></td>
                                  <td><span class=" adjusted_pos_pre4"></span><input type="hidden" class="adjusted_pos_pre4" name="adjusted_pos_pre4[]" /></td>
                                  <td><span class=" adjusted_pos_pre5"></span><input type="hidden" class="adjusted_pos_pre5" name="adjusted_pos_pre5[]" /></td>
                                  <td><span class=" adjusted_pos_pre6"></span><input type="hidden" class="adjusted_pos_pre6" name="adjusted_pos_pre6[]" /></td>
                                </tr>
                                <tr>
                                  <th scope="row">Adjusted Exposed Pressure </th>
                                  <td><span class=" adjusted_exp_neg_pre0"></span><input type="hidden" class="adjusted_exp_neg_pre0" name="adjusted_exp_neg_pre0[]" /></td>
                                  <td><span class=" adjusted_exp_neg_pre1"></span><input type="hidden" class="adjusted_exp_neg_pre1" name="adjusted_exp_neg_pre1[]" /></td>
                                  <td><span class=" adjusted_exp_neg_pre2"></span><input type="hidden" class="adjusted_exp_neg_pre2" name="adjusted_exp_neg_pre2[]" /></td>
                                  <td><span class=" adjusted_exp_neg_pre3"></span><input type="hidden" class="adjusted_exp_neg_pre3" name="adjusted_exp_neg_pre3[]" /></td>
                                  <td><span class=" adjusted_exp_neg_pre4"></span><input type="hidden" class="adjusted_exp_neg_pre4" name="adjusted_exp_neg_pre4[]" /></td>
                                  <td><span class=" adjusted_exp_neg_pre5"></span><input type="hidden" class="adjusted_exp_neg_pre5" name="adjusted_exp_neg_pre5[]" /></td>
                                  <td><span class=" adjusted_exp_neg_pre6"></span><input type="hidden" class="adjusted_exp_neg_pre6" name="adjusted_exp_neg_pre6[]" /></td>
                                </tr>
                                <tr>
                                  <th scope="row">Adjusted Non Exposed Pressure </th>
                                  <td><span class=" adjusted_non_exp_neg_pre0"></span><input type="hidden" class="adjusted_non_exp_neg_pre0" name="adjusted_non_exp_neg_pre0[]" /></td>
                                  <td><span class=" adjusted_non_exp_neg_pre1"></span><input type="hidden" class="adjusted_non_exp_neg_pre1" name="adjusted_non_exp_neg_pre1[]" /></td>
                                  <td><span class=" adjusted_non_exp_neg_pre2"></span><input type="hidden" class="adjusted_non_exp_neg_pre2" name="adjusted_non_exp_neg_pre2[]" /></td>
                                  <td><span class=" adjusted_non_exp_neg_pre3"></span><input type="hidden" class="adjusted_non_exp_neg_pre3" name="adjusted_non_exp_neg_pre3[]" /></td>
                                  <td><span class=" adjusted_non_exp_neg_pre4"></span><input type="hidden" class="adjusted_non_exp_neg_pre4" name="adjusted_non_exp_neg_pre4[]" /></td>
                                  <td><span class=" adjusted_non_exp_neg_pre5"></span><input type="hidden" class="adjusted_non_exp_neg_pre5" name="adjusted_non_exp_neg_pre5[]" /></td>
                                  <td><span class=" adjusted_non_exp_neg_pre6"></span><input type="hidden" class="adjusted_non_exp_neg_pre6" name="adjusted_non_exp_neg_pre6[]" /></td>
                                </tr>
                                <tr>
                                  <th scope="row">Positive Loading</th>
                                  <td><span class="positive_loading0"></span><input type="hidden" class="positive_loading0" name="positive_loading0[]" /></td>
                                  <td><span class="positive_loading1"></span><input type="hidden" class="positive_loading1" name="positive_loading1[]" /></td>
                                  <td><span class="positive_loading2"></span><input type="hidden" class="positive_loading2" name="positive_loading2[]" /></td>
                                  <td><span class="positive_loading3"></span><input type="hidden" class="positive_loading3" name="positive_loading3[]" /></td>
                                  <td><span class="positive_loading4"></span><input type="hidden" class="positive_loading4" name="positive_loading4[]" /></td>
                                  <td><span class="positive_loading5"></span><input type="hidden" class="positive_loading5" name="positive_loading5[]" /></td>
                                  <td><span class="positive_loading6"></span><input type="hidden" class="positive_loading6" name="positive_loading6[]" /></td>
                                </tr>
                                <tr>
                                  <th scope="row">Negative Exposed Loading</th>
                                  <td><span class="negative_exposed_loading0"></span><input type="hidden" class="negative_exposed_loading0" name="negative_exposed_loading0[]" /></td>
                                  <td><span class="negative_exposed_loading1"></span><input type="hidden" class="negative_exposed_loading1" name="negative_exposed_loading1[]" /></td>
                                  <td><span class="negative_exposed_loading2"></span><input type="hidden" class="negative_exposed_loading2" name="negative_exposed_loading2[]" /></td>
                                  <td><span class="negative_exposed_loading3"></span><input type="hidden" class="negative_exposed_loading3" name="negative_exposed_loading3[]" /></td>
                                  <td><span class="negative_exposed_loading4"></span><input type="hidden" class="negative_exposed_loading4" name="negative_exposed_loading4[]" /></td>
                                  <td><span class="negative_exposed_loading5"></span><input type="hidden" class="negative_exposed_loading5" name="negative_exposed_loading5[]" /></td>
                                  <td><span class="negative_exposed_loading6"></span><input type="hidden" class="negative_exposed_loading6" name="negative_exposed_loading6[]" /></td>
                                </tr>
                                <tr>
                                  <th scope="row">Negative Non Exposed Loading</th>
                                  <td><span class="negative_non_exposed_loading0"></span><input type="hidden" class="negative_non_exposed_loading0" name="negative_non_exposed_loading0[]" /></td>
                                  <td><span class="negative_non_exposed_loading1"></span><input type="hidden" class="negative_non_exposed_loading1" name="negative_non_exposed_loading1[]" /></td>
                                  <td><span class="negative_non_exposed_loading2"></span><input type="hidden" class="negative_non_exposed_loading2" name="negative_non_exposed_loading2[]" /></td>
                                  <td><span class="negative_non_exposed_loading3"></span><input type="hidden" class="negative_non_exposed_loading3" name="negative_non_exposed_loading3[]" /></td>
                                  <td><span class="negative_non_exposed_loading4"></span><input type="hidden" class="negative_non_exposed_loading4" name="negative_non_exposed_loading4[]" /></td>
                                  <td><span class="negative_non_exposed_loading5"></span><input type="hidden" class="negative_non_exposed_loading5" name="negative_non_exposed_loading5[]" /></td>
                                  <td><span class="negative_non_exposed_loading6"></span><input type="hidden" class="negative_non_exposed_loading6" name="negative_non_exposed_loading6[]" /></td>
                                </tr>
                                <tr>
                                  <th scope="row">Attachment Exposed Negative Spacing</th>
                                  <td><span class="attachment_exposed_negative_spacing0"></span><input type="hidden" class="attachment_exposed_negative_spacing0" name="attachment_exposed_negative_spacing0[]" /></td>
                                  <td><span class="attachment_exposed_negative_spacing1"></span><input type="hidden" class="attachment_exposed_negative_spacing1" name="attachment_exposed_negative_spacing1[]" /></td>
                                  <td><span class="attachment_exposed_negative_spacing2"></span><input type="hidden" class="attachment_exposed_negative_spacing2" name="attachment_exposed_negative_spacing2[]" /></td>
                                  <td><span class="attachment_exposed_negative_spacing3"></span><input type="hidden" class="attachment_exposed_negative_spacing3" name="attachment_exposed_negative_spacing3[]" /></td>
                                  <td><span class="attachment_exposed_negative_spacing4"></span><input type="hidden" class="attachment_exposed_negative_spacing4" name="attachment_exposed_negative_spacing4[]" /></td>
                                  <td><span class="attachment_exposed_negative_spacing5"></span><input type="hidden" class="attachment_exposed_negative_spacing5" name="attachment_exposed_negative_spacing5[]" /></td>
                                  <td><span class="attachment_exposed_negative_spacing6"></span><input type="hidden" class="attachment_exposed_negative_spacing6" name="attachment_exposed_negative_spacing6[]" /></td>
                                </tr>
                                <tr>
                                  <th scope="row">Attachment Non Exposed Negative Spacing</th>
                                  <td><span class="attachment_non_exposed_negative_spacing0"></span><input type="hidden" class="attachment_non_exposed_negative_spacing0" name="attachment_non_exposed_negative_spacing0[]" /></td>
                                  <td><span class="attachment_non_exposed_negative_spacing1"></span><input type="hidden" class="attachment_non_exposed_negative_spacing1" name="attachment_non_exposed_negative_spacing1[]" /></td>
                                  <td><span class="attachment_non_exposed_negative_spacing2"></span><input type="hidden" class="attachment_non_exposed_negative_spacing2" name="attachment_non_exposed_negative_spacing2[]" /></td>
                                  <td><span class="attachment_non_exposed_negative_spacing3"></span><input type="hidden" class="attachment_non_exposed_negative_spacing3" name="attachment_non_exposed_negative_spacing3[]" /></td>
                                  <td><span class="attachment_non_exposed_negative_spacing4"></span><input type="hidden" class="attachment_non_exposed_negative_spacing4" name="attachment_non_exposed_negative_spacing4[]" /></td>
                                  <td><span class="attachment_non_exposed_negative_spacing5"></span><input type="hidden" class="attachment_non_exposed_negative_spacing5" name="attachment_non_exposed_negative_spacing5[]" /></td>
                                  <td><span class="attachment_non_exposed_negative_spacing6"></span><input type="hidden" class="attachment_non_exposed_negative_spacing6" name="attachment_non_exposed_negative_spacing6[]" /></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <form id="roof-info-form" action="{{ url('roofinfo') }}" method="post">
                @csrf

            <div id="room_div">
            @php $k = 1; @endphp
            @foreach($roofinfos as $roofinfo)
            <input type="hidden" name="roof_info_id[]" value="{{ $roofinfo->id }}" />
            <div class="card" id="roof-{{ $k }}">
                <div class="card-header" id="roof-header-{{ $k }}" data-toggle="collapse" data-target="#roof-collapse-{{ $k }}" aria-expanded="true" aria-controls="roof-collapse-{{ $k }}">
                  <h5 class="d-flex align-items-center mb-0 roof_no" ><span class="roof_no">Roof - {{ $k }} </span><span class="icon  ml-auto"><i class="fas @if($loop->iteration == 1) fa-minus-circle @else fa-plus-circle @endif"></i></span></h5>
                </div>

                <div id="roof-collapse-{{ $k }}" class="collapse-body collapse @if($loop->iteration==1) show @endif" aria-labelledby="roof-header-{{ $k }}" data-parent="#accordion">
                  <div class="card-body">

                      <div class="row roof-container" id="roof{{ $k }}">
                        <div class="col-md-7 mb-md-0 mb-5">
                          <div class="row">
                            <div class="col-md-5 col-sm-6">
                              <div class="form-group">
                                  <label for="pvmodule">Module Manufacturer </label>
                                  <div class="select-area form-control" style="width:100%;">
                                      <select name="manufacturer_id[]" class="manufacturer_id" url="{{url('module-manufacturer')}}"  >
                                        <option value="">Select Module Manufacturer</option>
                                         @foreach($manufacturers as $manufacturer)
                                        <option value="{{$manufacturer->id}}" {{ ($manufacturer->id == $roofinfo->manufacturer_id) ? "selected":"" }}>{{ $manufacturer->title }}</option>
                                        @endforeach
                                      </select>
                                  </div>
                              </div>
                            </div>
                            <div class="col-md-5 offset-md-1  col-sm-6">
                              <div class="form-group">
                                  <label for="pvmodule">Attachment Manufacturer</label>
                                  <div class="select-area form-control" style="width:100%;">
                                      <select name="attachment_manufacturer_id[]"  class="attachment_manufacturer_id" url="{{url('attachment')}}">
                                        <option value="">Select Attachment Manufacturer</option>
                                        @foreach($manufacturers as $manufacturer)
                                        <option value="{{$manufacturer->id}}" {{ ($manufacturer->id == $roofinfo->attachment_manufacturer_id) ? "selected":"" }}>{{ $manufacturer->title }}</option>
                                        @endforeach
                                      </select>
                                  </div>
                              </div>
                            </div>
                            @php
                                $modules = DB::table('modules')->where(['manufacturer_id'=>$roofinfo->manufacturer_id])->get();
                            @endphp
                            <div class="col-md-5  col-sm-6">
                              <div class="form-group">
                                  <label for="pvmodule">PV Module</label>
                                  <div class="select-area form-control" style="width:100%;">
                                      <select name="module_id[]"  class="module_id" url="{{url('module')}}">
                                        <option value="">Select Pv Module</option>
                                        @foreach($modules as $module)
                                        <option value="{{ $module->id }}" {{ ($module->id == $roofinfo->module_id) ? "selected":"" }}>{{ $module->id }}</option>
                                        @endforeach
                                      </select>
                                  </div>
                              </div>
                            </div>
                            @php
                                $attachments =  DB::table('attachments')->where(['manufacturer_id'=>$roofinfo->attachment_manufacturer_id])->get();
                            @endphp
                            <div class="col-md-5 offset-md-1 col-sm-6">
                              <div class="form-group">
                                  <label for="pvmodule">Attachment</label>
                                  <div class="select-area form-control" style="width:100%;">
                                      <select name="attachment_id[]"  class="attachment_id">
                                        <option value="">Select Attachment</option>
                                        @foreach($attachments as $attachment)
                                        <option value="{{ $attachment->id }}" {{ ($attachment->id == $roofinfo->attachment_id) ? "selected":"" }}>{{ $attachment->model_no }}</option>
                                        @endforeach
                                      </select>
                                  </div>
                              </div>
                            </div>
                            <div class="col-md-5 col-sm-6">
                              <div class="form-group">
                                  <label for="mlength">Module Length</label>
                                  <input readonly type="text" name="module_length[]"  class="form-control input-style2 module_length numeric_feild_discount" value="{{ $roofinfo->module_length }}" placeholder="">
                              </div>
                            </div>
                            <div class="col-md-5 offset-md-1 col-sm-6">
                              <div class="form-group">
                                  <label for="attachmenttype">Attachment type</label>
                                  <div class="select-area form-control" style="width:100%;">
                                      <select name="attachment_type[]"  class="attachment_type">
                                        <option value="">Select Attachment Type</option>
                                        <option value="Rafter" {{ ("Rafter" == $roofinfo->attachment_type) ? "selected":"" }}>Rafter</option>
                                        <option value="Seam" {{ ("Seam" == $roofinfo->attachment_type) ? "selected":"" }}>Seam</option>
                                        <option value="Purlin" {{ ("Purlin" == $roofinfo->attachment_type) ? "selected":"" }}>Purlin</option>
                                        <option value="Decking" {{ ("Decking" == $roofinfo->attachment_type) ? "selected":"" }}>Decking</option>
                                      </select>
                                  </div>
                              </div>
                            </div>
                            <div class="col-md-5 col-sm-6">
                              <div class="form-group">
                                  <label for="mwidth">Module Width</label>
                                  <input readonly type="text" class="form-control module_width input-style2 numeric_feild_discount" name="module_width[]" value="{{ $roofinfo->module_width }}" placeholder="">
                              </div>
                            </div>
                            <div class="col-md-5 offset-md-1 col-sm-6">
                              <div class="form-group">
                                  <label for="strenght">Compression Strenght</label>
                                  <input readonly type="text" name="compression_strenght[]"  class="form-control input-style2 compression_strenght numeric_feild_discount" value="{{ $roofinfo->compression_strenght }}" placeholder="">
                              </div>
                            </div>

                            <div class="col-md-5 col-sm-6">
                              <div class="form-group">
                                  <label for="morientation">Module Orientation</label>
                                  <div class="select-area form-control" style="width:100%;">
                                      <select name="module_orientation[]"  class="module_orientation">
                                      <option value="PORTRAIT" {{ ($manufacturer->id == $roofinfo->manufacturer_id) ? "selected":"" }}>PORTRAIT</option>
                                      <option value="LANDSCAPE" {{ ($manufacturer->id == $roofinfo->manufacturer_id) ? "selected":"" }}>LANDSCAPE</option>
                                      </select>
                                  </div>
                              </div>
                            </div>
                            <div class="col-md-5 offset-md-1 col-sm-6">
                              <div class="form-group">
                                  <label for="pullout_strenght">Pullout Strenght</label>
                                  <input readonly type="text" name="pullout_strenght[]"  class="form-control input-style2 pullout_strenght numeric_feild_discount" value="{{ $roofinfo->pullout_strenght }}"  placeholder="">
                              </div>
                            </div>


                            <div class="col-md-5 col-sm-6">
                            <div class="form-group">
                                  <label for="roof_height">Roof Height (ft)</label>
                                  <div class="select-area form-control" style="width:100%;">
                                <input type="text" name="roof_height[]"  class="form-control input-style2 roof_height numeric_feild_discount"  placeholder="" value="{{ $roofinfo->roof_height }}">

                                  </div>
                              </div>

                            </div>
                            <div class="col-md-5 offset-md-1 col-sm-6">
                            <div class="form-group">
                                  <label for="no_of_rails">No of Rails</label>
                                  <input  type="text" name="no_of_rails[]" class="form-control input-style2 no_of_rails numeric_feild_discount" value="{{ $roofinfo->no_of_rails }}"  placeholder="">
                              </div>
                            </div>


                            <div class="col-md-5 col-sm-6">
                            <div class="form-group">
                                  <label for="limit_max_span_to">Limit Max Span To (in)</label>
                                  <input  type="text" name="limit_max_span_to[]" class="form-control  input-style2 limit_max_span_to numeric_feild_discount" value="{{ $roofinfo->limit_max_span_to }}" placeholder="">
                            </div>

                            </div>
                            <div class="col-md-5 offset-md-1 col-sm-6">
                            <div class="form-group">
                                  <label for="rafter_seam_spacing">Rafter/Seam Spacing (in)</label>
                                  <input type="text" name="rafter_seam_spacing[]" class="form-control input-style2 rafter_seam_spacing numeric_feild_discount" value="{{ $roofinfo->rafter_seam_spacing }}"  placeholder="">
                              </div>
                            </div>
                            <div class="col-md-5 col-sm-6">
                            <div class="form-group">
                                  <label for="roof_slope">Roof Slope</label>
                                  <input type="text" name="roof_slope[]" class="form-control input-style2 roof_slope numeric_feild_discount" value="{{ $roofinfo->roof_slope }}" placeholder="">
                              </div>
                            </div>




                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="roof-table">
                            <div class="table-responsive">
                              <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Zone 1</th>
                                    <th scope="col">Zone 1'</th>
                                    <th scope="col">Zone 2e</th>

                                    <th scope="col">Zone 2n</th>
                                    <th scope="col">Zone 2r</th>
                                    <th scope="col">Zone 3e</th>
                                    <th scope="col">Zone 3r</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <th scope="row">External Positive Pressure Coefficient </th>
                                    <td><span class="ext_pos_pre_coef0">{{ $roofinfo->ext_pos_pre_coef0 }}</span><input type="hidden" class="ext_pos_pre_coef0" name="ext_pos_pre_coef0[]" /></td>
                                    <td><span class="ext_pos_pre_coef1">{{ $roofinfo->ext_pos_pre_coef1 }}</span><input type="hidden" class="ext_pos_pre_coef1" name="ext_pos_pre_coef1[]" /></td>
                                    <td><span class="ext_pos_pre_coef2">{{ $roofinfo->ext_pos_pre_coef2 }}</span><input type="hidden" class="ext_pos_pre_coef2" name="ext_pos_pre_coef2[]" /></td>
                                    <td><span class="ext_pos_pre_coef3">{{ $roofinfo->ext_pos_pre_coef3 }}</span><input type="hidden" class="ext_pos_pre_coef3" name="ext_pos_pre_coef3[]" /></td>
                                    <td><span class="ext_pos_pre_coef4">{{ $roofinfo->ext_pos_pre_coef4 }}</span><input type="hidden" class="ext_pos_pre_coef4" name="ext_pos_pre_coef4[]" /></td>
                                    <td><span class="ext_pos_pre_coef5">{{ $roofinfo->ext_pos_pre_coef5 }}</span><input type="hidden" class="ext_pos_pre_coef5" name="ext_pos_pre_coef5[]" /></td>
                                    <td><span class="ext_pos_pre_coef6">{{ $roofinfo->ext_pos_pre_coef6 }}</span><input type="hidden" class="ext_pos_pre_coef6" name="ext_pos_pre_coef6[]" /></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">External Negative Pressure Coefficient </th>
                                    <td><span class="ext_neg_pre_coef0">{{ $roofinfo->ext_neg_pre_coef0 }}</span><input type="hidden" class="ext_neg_pre_coef0" name="ext_neg_pre_coef0[]" /></td>
                                    <td><span class="ext_neg_pre_coef1">{{ $roofinfo->ext_neg_pre_coef1 }}</span><input type="hidden" class="ext_neg_pre_coef1" name="ext_neg_pre_coef1[]" /></td>
                                    <td><span class="ext_neg_pre_coef2">{{ $roofinfo->ext_neg_pre_coef2 }}</span><input type="hidden" class="ext_neg_pre_coef2" name="ext_neg_pre_coef2[]" /></td>
                                    <td><span class="ext_neg_pre_coef3">{{ $roofinfo->ext_neg_pre_coef3 }}</span><input type="hidden" class="ext_neg_pre_coef3" name="ext_neg_pre_coef3[]" /></td>
                                    <td><span class="ext_neg_pre_coef4">{{ $roofinfo->ext_neg_pre_coef4 }}</span><input type="hidden" class="ext_neg_pre_coef4" name="ext_neg_pre_coef4[]" /></td>
                                    <td><span class="ext_neg_pre_coef5">{{ $roofinfo->ext_neg_pre_coef5 }}</span><input type="hidden" class="ext_neg_pre_coef5" name="ext_neg_pre_coef5[]" /></td>
                                    <td><span class="ext_neg_pre_coef6">{{ $roofinfo->ext_neg_pre_coef6 }}</span><input type="hidden" class="ext_neg_pre_coef6" name="ext_neg_pre_coef6[]" /></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Kz </th>
                                    <td><span  class="zone_kz0"></span><input type="hidden" class="zone_kz0" name="zone_kz0[]" /></td>
                                    <td><span  class="zone_kz1"></span><input type="hidden" class="zone_kz1" name="zone_kz1[]" /></td>
                                    <td><span  class="zone_kz2"></span><input type="hidden" class="zone_kz2" name="zone_kz2[]" /></td>
                                    <td><span  class="zone_kz3"></span><input type="hidden" class="zone_kz3" name="zone_kz3[]" /></td>
                                    <td><span  class="zone_kz4"></span><input type="hidden" class="zone_kz4" name="zone_kz4[]" /></td>
                                    <td><span  class="zone_kz5"></span><input type="hidden" class="zone_kz5" name="zone_kz5[]" /></td>
                                    <td><span  class="zone_kz6"></span><input type="hidden" class="zone_kz6" name="zone_kz6[]" /></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Positive Pressure </th>
                                    <td><span class=" positive_pressure0">{{ $roofinfo->positive_pressure0 }}</span><input type="hidden" class="positive_pressure0" name="positive_pressure0[]" /></td>
                                    <td><span class=" positive_pressure1">{{ $roofinfo->positive_pressure1 }}</span><input type="hidden" class="positive_pressure1" name="positive_pressure1[]" /></td>
                                    <td><span class=" positive_pressure2">{{ $roofinfo->positive_pressure2 }}</span><input type="hidden" class="positive_pressure2" name="positive_pressure2[]" /></td>
                                    <td><span class=" positive_pressure3">{{ $roofinfo->positive_pressure3 }}</span><input type="hidden" class="positive_pressure3" name="positive_pressure3[]" /></td>
                                    <td><span class=" positive_pressure4">{{ $roofinfo->positive_pressure4 }}</span><input type="hidden" class="positive_pressure4" name="positive_pressure4[]" /></td>
                                    <td><span class=" positive_pressure5">{{ $roofinfo->positive_pressure5 }}</span><input type="hidden" class="positive_pressure5" name="positive_pressure5[]" /></td>
                                    <td><span class=" positive_pressure6">{{ $roofinfo->positive_pressure6 }}</span><input type="hidden" class="positive_pressure6" name="positive_pressure6[]" /></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Negative Pressure </th>
                                    <td><span class=" negative_pressure0">{{ $roofinfo->negative_pressure0 }}</span><input type="hidden" class="negative_pressure0" name="negative_pressure0[]" /></td>
                                    <td><span class=" negative_pressure1">{{ $roofinfo->negative_pressure1 }}</span><input type="hidden" class="negative_pressure1" name="negative_pressure1[]" /></td>
                                    <td><span class=" negative_pressure2">{{ $roofinfo->negative_pressure2 }}</span><input type="hidden" class="negative_pressure2" name="negative_pressure2[]" /></td>
                                    <td><span class=" negative_pressure3">{{ $roofinfo->negative_pressure3 }}</span><input type="hidden" class="negative_pressure3" name="negative_pressure3[]" /></td>
                                    <td><span class=" negative_pressure4">{{ $roofinfo->negative_pressure4 }}</span><input type="hidden" class="negative_pressure4" name="negative_pressure4[]" /></td>
                                    <td><span class=" negative_pressure5">{{ $roofinfo->negative_pressure5 }}</span><input type="hidden" class="negative_pressure5" name="negative_pressure5[]" /></td>
                                    <td><span class=" negative_pressure6">{{ $roofinfo->negative_pressure6 }}</span><input type="hidden" class="negative_pressure6" name="negative_pressure6[]" /></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Adjusted Positive Pressure </th>
                                    <td><span class=" adjusted_pos_pre0">{{ $roofinfo->adjusted_pos_pre0 }}</span><input type="hidden" class="adjusted_pos_pre0" name="adjusted_pos_pre0[]" /></td>
                                    <td><span class=" adjusted_pos_pre1">{{ $roofinfo->adjusted_pos_pre1 }}</span><input type="hidden" class="adjusted_pos_pre1" name="adjusted_pos_pre1[]" /></td>
                                    <td><span class=" adjusted_pos_pre2">{{ $roofinfo->adjusted_pos_pre2 }}</span><input type="hidden" class="adjusted_pos_pre2" name="adjusted_pos_pre2[]" /></td>
                                    <td><span class=" adjusted_pos_pre3">{{ $roofinfo->adjusted_pos_pre3 }}</span><input type="hidden" class="adjusted_pos_pre3" name="adjusted_pos_pre3[]" /></td>
                                    <td><span class=" adjusted_pos_pre4">{{ $roofinfo->adjusted_pos_pre4 }}</span><input type="hidden" class="adjusted_pos_pre4" name="adjusted_pos_pre4[]" /></td>
                                    <td><span class=" adjusted_pos_pre5">{{ $roofinfo->adjusted_pos_pre5 }}</span><input type="hidden" class="adjusted_pos_pre5" name="adjusted_pos_pre5[]" /></td>
                                    <td><span class=" adjusted_pos_pre6">{{ $roofinfo->adjusted_pos_pre6 }}</span><input type="hidden" class="adjusted_pos_pre6" name="adjusted_pos_pre6[]" /></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Adjusted Exposed Pressure </th>
                                    <td><span class=" adjusted_exp_neg_pre0">{{ $roofinfo->adjusted_exp_neg_pre0 }}</span><input type="hidden" class="adjusted_exp_neg_pre0" name="adjusted_exp_neg_pre0[]" /></td>
                                    <td><span class=" adjusted_exp_neg_pre1">{{ $roofinfo->adjusted_exp_neg_pre1 }}</span><input type="hidden" class="adjusted_exp_neg_pre1" name="adjusted_exp_neg_pre1[]" /></td>
                                    <td><span class=" adjusted_exp_neg_pre2">{{ $roofinfo->adjusted_exp_neg_pre2 }}</span><input type="hidden" class="adjusted_exp_neg_pre2" name="adjusted_exp_neg_pre2[]" /></td>
                                    <td><span class=" adjusted_exp_neg_pre3">{{ $roofinfo->adjusted_exp_neg_pre3 }}</span><input type="hidden" class="adjusted_exp_neg_pre3" name="adjusted_exp_neg_pre3[]" /></td>
                                    <td><span class=" adjusted_exp_neg_pre4">{{ $roofinfo->adjusted_exp_neg_pre4 }}</span><input type="hidden" class="adjusted_exp_neg_pre4" name="adjusted_exp_neg_pre4[]" /></td>
                                    <td><span class=" adjusted_exp_neg_pre5">{{ $roofinfo->adjusted_exp_neg_pre5 }}</span><input type="hidden" class="adjusted_exp_neg_pre5" name="adjusted_exp_neg_pre5[]" /></td>
                                    <td><span class=" adjusted_exp_neg_pre6">{{ $roofinfo->adjusted_exp_neg_pre6 }}</span><input type="hidden" class="adjusted_exp_neg_pre6" name="adjusted_exp_neg_pre6[]" /></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Adjusted Non Exposed Pressure </th>
                                    <td><span class=" adjusted_non_exp_neg_pre0">{{ $roofinfo->adjusted_non_exp_neg_pre0 }}</span><input type="hidden" class="adjusted_non_exp_neg_pre0" name="adjusted_non_exp_neg_pre0[]" /></td>
                                    <td><span class=" adjusted_non_exp_neg_pre1">{{ $roofinfo->adjusted_non_exp_neg_pre1 }}</span><input type="hidden" class="adjusted_non_exp_neg_pre1" name="adjusted_non_exp_neg_pre1[]" /></td>
                                    <td><span class=" adjusted_non_exp_neg_pre2">{{ $roofinfo->adjusted_non_exp_neg_pre2 }}</span><input type="hidden" class="adjusted_non_exp_neg_pre2" name="adjusted_non_exp_neg_pre2[]" /></td>
                                    <td><span class=" adjusted_non_exp_neg_pre3">{{ $roofinfo->adjusted_non_exp_neg_pre3 }}</span><input type="hidden" class="adjusted_non_exp_neg_pre3" name="adjusted_non_exp_neg_pre3[]" /></td>
                                    <td><span class=" adjusted_non_exp_neg_pre4">{{ $roofinfo->adjusted_non_exp_neg_pre4 }}</span><input type="hidden" class="adjusted_non_exp_neg_pre4" name="adjusted_non_exp_neg_pre4[]" /></td>
                                    <td><span class=" adjusted_non_exp_neg_pre5">{{ $roofinfo->adjusted_non_exp_neg_pre5 }}</span><input type="hidden" class="adjusted_non_exp_neg_pre5" name="adjusted_non_exp_neg_pre5[]" /></td>
                                    <td><span class=" adjusted_non_exp_neg_pre6">{{ $roofinfo->adjusted_non_exp_neg_pre6 }}</span><input type="hidden" class="adjusted_non_exp_neg_pre6" name="adjusted_non_exp_neg_pre6[]" /></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Positive Loading</th>
                                    <td><span class="positive_loading0">{{ $roofinfo->positive_loading0 }}</span><input type="hidden" class="positive_loading0" name="positive_loading0[]" /></td>
                                    <td><span class="positive_loading1">{{ $roofinfo->positive_loading1 }}</span><input type="hidden" class="positive_loading1" name="positive_loading1[]" /></td>
                                    <td><span class="positive_loading2">{{ $roofinfo->positive_loading2 }}</span><input type="hidden" class="positive_loading2" name="positive_loading2[]" /></td>
                                    <td><span class="positive_loading3">{{ $roofinfo->positive_loading3 }}</span><input type="hidden" class="positive_loading3" name="positive_loading3[]" /></td>
                                    <td><span class="positive_loading4">{{ $roofinfo->positive_loading4 }}</span><input type="hidden" class="positive_loading4" name="positive_loading4[]" /></td>
                                    <td><span class="positive_loading5">{{ $roofinfo->positive_loading5 }}</span><input type="hidden" class="positive_loading5" name="positive_loading5[]" /></td>
                                    <td><span class="positive_loading6">{{ $roofinfo->positive_loading6 }}</span><input type="hidden" class="positive_loading6" name="positive_loading6[]" /></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Negative Exposed Loading</th>
                                    <td><span class="negative_exposed_loading0">{{ $roofinfo->negative_exposed_loading0 }}</span><input type="hidden" class="negative_exposed_loading0" name="negative_exposed_loading0[]" /></td>
                                    <td><span class="negative_exposed_loading1">{{ $roofinfo->negative_exposed_loading1 }}</span><input type="hidden" class="negative_exposed_loading1" name="negative_exposed_loading1[]" /></td>
                                    <td><span class="negative_exposed_loading2">{{ $roofinfo->negative_exposed_loading2 }}</span><input type="hidden" class="negative_exposed_loading2" name="negative_exposed_loading2[]" /></td>
                                    <td><span class="negative_exposed_loading3">{{ $roofinfo->negative_exposed_loading3 }}</span><input type="hidden" class="negative_exposed_loading3" name="negative_exposed_loading3[]" /></td>
                                    <td><span class="negative_exposed_loading4">{{ $roofinfo->negative_exposed_loading4 }}</span><input type="hidden" class="negative_exposed_loading4" name="negative_exposed_loading4[]" /></td>
                                    <td><span class="negative_exposed_loading5">{{ $roofinfo->negative_exposed_loading5 }}</span><input type="hidden" class="negative_exposed_loading5" name="negative_exposed_loading5[]" /></td>
                                    <td><span class="negative_exposed_loading6">{{ $roofinfo->negative_exposed_loading6 }}</span><input type="hidden" class="negative_exposed_loading6" name="negative_exposed_loading6[]" /></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Negative Non Exposed Loading</th>
                                    <td><span class="negative_non_exposed_loading0">{{ $roofinfo->negative_non_exposed_loading0 }}</span><input type="hidden" class="negative_non_exposed_loading0" name="negative_non_exposed_loading0[]" /></td>
                                    <td><span class="negative_non_exposed_loading1">{{ $roofinfo->negative_non_exposed_loading1 }}</span><input type="hidden" class="negative_non_exposed_loading1" name="negative_non_exposed_loading1[]" /></td>
                                    <td><span class="negative_non_exposed_loading2">{{ $roofinfo->negative_non_exposed_loading2 }}</span><input type="hidden" class="negative_non_exposed_loading2" name="negative_non_exposed_loading2[]" /></td>
                                    <td><span class="negative_non_exposed_loading3">{{ $roofinfo->negative_non_exposed_loading3 }}</span><input type="hidden" class="negative_non_exposed_loading3" name="negative_non_exposed_loading3[]" /></td>
                                    <td><span class="negative_non_exposed_loading4">{{ $roofinfo->negative_non_exposed_loading4 }}</span><input type="hidden" class="negative_non_exposed_loading4" name="negative_non_exposed_loading4[]" /></td>
                                    <td><span class="negative_non_exposed_loading5">{{ $roofinfo->negative_non_exposed_loading5 }}</span><input type="hidden" class="negative_non_exposed_loading5" name="negative_non_exposed_loading5[]" /></td>
                                    <td><span class="negative_non_exposed_loading6">{{ $roofinfo->negative_non_exposed_loading6 }}</span><input type="hidden" class="negative_non_exposed_loading6" name="negative_non_exposed_loading6[]" /></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Attachment Exposed Negative Spacing</th>
                                    <td><span class="attachment_exposed_negative_spacing0">{{ $roofinfo->attachment_exposed_negative_spacing0 }}</span><input type="hidden" class="attachment_exposed_negative_spacing0" name="attachment_exposed_negative_spacing0[]" /></td>
                                    <td><span class="attachment_exposed_negative_spacing1">{{ $roofinfo->attachment_exposed_negative_spacing1 }}</span><input type="hidden" class="attachment_exposed_negative_spacing1" name="attachment_exposed_negative_spacing1[]" /></td>
                                    <td><span class="attachment_exposed_negative_spacing2">{{ $roofinfo->attachment_exposed_negative_spacing2 }}</span><input type="hidden" class="attachment_exposed_negative_spacing2" name="attachment_exposed_negative_spacing2[]" /></td>
                                    <td><span class="attachment_exposed_negative_spacing3">{{ $roofinfo->attachment_exposed_negative_spacing3 }}</span><input type="hidden" class="attachment_exposed_negative_spacing3" name="attachment_exposed_negative_spacing3[]" /></td>
                                    <td><span class="attachment_exposed_negative_spacing4">{{ $roofinfo->attachment_exposed_negative_spacing4 }}</span><input type="hidden" class="attachment_exposed_negative_spacing4" name="attachment_exposed_negative_spacing4[]" /></td>
                                    <td><span class="attachment_exposed_negative_spacing5">{{ $roofinfo->attachment_exposed_negative_spacing5 }}</span><input type="hidden" class="attachment_exposed_negative_spacing5" name="attachment_exposed_negative_spacing5[]" /></td>
                                    <td><span class="attachment_exposed_negative_spacing6">{{ $roofinfo->attachment_exposed_negative_spacing6 }}</span><input type="hidden" class="attachment_exposed_negative_spacing6" name="attachment_exposed_negative_spacing6[]" /></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Attachment Non Exposed Negative Spacing</th>
                                    <td><span class="attachment_non_exposed_negative_spacing0">{{ $roofinfo->attachment_non_exposed_negative_spacing0 }}</span><input type="hidden" class="attachment_non_exposed_negative_spacing0" name="attachment_non_exposed_negative_spacing0[]" /></td>
                                    <td><span class="attachment_non_exposed_negative_spacing1">{{ $roofinfo->attachment_non_exposed_negative_spacing1 }}</span><input type="hidden" class="attachment_non_exposed_negative_spacing1" name="attachment_non_exposed_negative_spacing1[]" /></td>
                                    <td><span class="attachment_non_exposed_negative_spacing2">{{ $roofinfo->attachment_non_exposed_negative_spacing2 }}</span><input type="hidden" class="attachment_non_exposed_negative_spacing2" name="attachment_non_exposed_negative_spacing2[]" /></td>
                                    <td><span class="attachment_non_exposed_negative_spacing3">{{ $roofinfo->attachment_non_exposed_negative_spacing3 }}</span><input type="hidden" class="attachment_non_exposed_negative_spacing3" name="attachment_non_exposed_negative_spacing3[]" /></td>
                                    <td><span class="attachment_non_exposed_negative_spacing4">{{ $roofinfo->attachment_non_exposed_negative_spacing4 }}</span><input type="hidden" class="attachment_non_exposed_negative_spacing4" name="attachment_non_exposed_negative_spacing4[]" /></td>
                                    <td><span class="attachment_non_exposed_negative_spacing5">{{ $roofinfo->attachment_non_exposed_negative_spacing5 }}</span><input type="hidden" class="attachment_non_exposed_negative_spacing5" name="attachment_non_exposed_negative_spacing5[]" /></td>
                                    <td><span class="attachment_non_exposed_negative_spacing6">{{ $roofinfo->attachment_non_exposed_negative_spacing6 }}</span><input type="hidden" class="attachment_non_exposed_negative_spacing6" name="attachment_non_exposed_negative_spacing6[]" /></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
              @php $k++; @endphp
            @endforeach

            </div>
            <div class="container">
                <div class="row">
                  <div class="col-lg-6 mt-2 text-lg-right text-center">
                    <button id="create-roof-info" type="submit" class="btn btn-red w-50 text-center create-roof-info @if(count($roofinfos) ==  0) d-none @endif"  id="create-site-button"> Save </button>
                  </div>

                  <div class="col-lg-6 mt-2 text-lg-left text-center">
                      <button id="add-roof" type="button" class="btn btn-red w-50 text-center add-roof @if(count($roofinfos) ==  0) d-none @endif"  > Add Roof </button>
                  </div>
                </div>
            </div>
        </form>


          </div>

        </div>


      </div>
    </div>
  </section>
@push('scripts')
<script src="{{asset('front/js/sites/event.js')}}"></script>
<script src="{{asset('front/js/roofinfo/event.js')}}"></script>
<script>

</script>

@endpush
@endsection
