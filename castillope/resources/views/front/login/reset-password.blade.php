@extends('front.layouts.app')
@section('title','Reset Password')
@section('content')
<!-- Banner -->
<section class="common-bnr">
    <div class="container-fluid">
      <div class="signup-block reset-block mx-auto">
        <h2 class="text-center">Reset Password.</h2>
        <p class="sub-title text-center">Your new password must be different from previous used passwords.</p>
        @include('front.includes.message')

        <form autocomplete="off" class="form-horizontal m-t-20" id="loginform" method="post" action="{{url('reset-password')}}">
        @csrf
        <input name="email" type="hidden" value="{{$email}}" />
        <input name="token" type="hidden" value="{{$token}}" />
          <div class="row">
            <div class="col-md-6 mb-4 pb-3">
              <div class="form-group mb-0">
                  <label for="npassword"><span class="icon-lock"></span>New Password</label>
                  <input type="password" class="form-control" id="password" name="password" placeholder="" value="">
                  <span id_key="password" class="icon-right icon-eye toggle-password"></span>
                  <span class="text-white">{{$errors->first('password')}}</span>

                </div>
            </div>
            <div class="col-md-6 mb-4 pb-3">
              <div class="form-group mb-0">
                  <label for="cpassword"><span class="icon-lock"></span>Confirm Password </label>
                  <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="" value="">
                  <span id_key="password_confirmation" class="icon-right icon-eye toggle-password"></span>
                  <span class="text-white">{{$errors->first('password_confirmation')}}</span>

                </div>
            </div>
          </div>


          <button id="submit-btn" class="btn btn-submit w-100 mb-0" type="submit">Reset Password</button>

        </form>
      </div>
    </div>
  </section>
@push('scripts')
<script>
    $(document).ready(function(){
        $(".toggle-password").click(function(){
            $(this).toggleClass("fa-eye fa-eye-slash");
var id = $(this).attr('id_key');
var input = $("#"+id).attr("type");
// console.log("type >> "+input);

if (input == "password") {
    $("#"+id).attr("type", "text");
} else {
    $("#"+id).attr("type", "password");
}

        });
    });

</script>


@endpush


@endsection
