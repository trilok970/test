@extends('front.layouts.app')
@section('title','Signup')
@section('content')

 <!-- Banner -->
 <section class="common-bnr">
    <div class="container-fluid">
      <div class="signup-block mx-auto">
        <h2 class="text-center">Sign up.</h2>
        <p class="sub-title text-center">Give us some of your information to get access to fieldly.</p>
        @include('front.includes.message')

        <form action="{{url('signup')}}" method="post" >
            @csrf
          <div class="row mb-4">
            <div class="col-md-6 mb-md-6">
              <div class="form-group mb-0">
                  <label for="firstname"><span class="icon-first-name"></span>First Name</label>
                  <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{old('first_name')}}">
                  <span class="text-white">{{$errors->first('first_name')}}</span>
                </div>
            </div>
            <div class="col-md-6 mb-md-6">
              <div class="form-group mb-0">
                  <label for="lastname"><span class="icon-last-name"></span>Last Name</label>
                  <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{old('last_name')}}">
                  <span class="text-white">{{$errors->first('last_name')}}</span>

                </div>
            </div>
            <div class="col-md-6 mb-md-6">
              <div class="form-group mb-0">
                  <label for="emailtext"><span class="icon-email"></span>Your E-mail</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Your E-mail" value="{{old('email')}}">
                  <span class="text-white">{{$errors->first('email')}}</span>

                </div>
            </div>
            <div class="col-md-6 mb-md-6">
              <div class="form-group phonenumber mb-0">
                  <label for="phonenum"><span class="icon-mobile"></span>Phone Number</label>
                  <div class="row">
                    <div class="col-sm-4 col-5">
                      <div class="input-group">
                        <div class="custom-select custom-select-lg" style="width:100%;">
                            <select name="country_code" id="country_code">
                            @foreach($codes as $code)
                            <option value="{{$code->phonecode}}" {{ old('country_code') == $code->phonecode ? "selected" :"" }}>{{$code->phonecode}}</option>
                            @endforeach
                            </select>
                  <span class="text-white">{{$errors->first('country_code')}}</span>

                        </div>
                      </div>
                    </div>
                    <div class="col-sm-8 col-7">
                        <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Phone Number" value="{{old('first_name')}}">
                        <span class="text-white">{{$errors->first('phone_number')}}</span>

                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-6 mb-md-3 mb-md-6">
              <div class="form-group mb-0">
                  <label for="password"><span class="icon-lock"></span>Password</label>
                  <input type="password" class="form-control " id="password" name="password" placeholder="Password" value="" />
                  <span id_key="password" class="icon-right icon-eye toggle-password"></span>
                  <span class="text-white">{{$errors->first('password')}}</span>

                </div>
            </div>
            <div class="col-md-6 mb-md-3 mb-4">
              <div class="form-group mb-0">
                  <label for="cpassword"><span class="icon-lock"></span>Confirm Password</label>
                  <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password" value="">
                  <span id_key="password_confirmation" class="icon-right icon-eye toggle-password"></span>
                  <span class="text-white">{{$errors->first('password_confirmation')}}</span>

                </div>
            </div>
          </div>

          <div class="custom-control custom-checkbox mb-4 pb-2">
              <input type="checkbox" class="custom-control-input" id="customCheck" name="tac" name="example1" checked >
              <label class="custom-control-label" for="customCheck">By Creating an account you agree to the <a href="#">Terms of use</a> and <a href="#"> Privacy Policy</a>.</label>
              <span class="text-white">{{$errors->first('tac')}}</span>

          </div>

          <button class="btn btn-submit w-100" type="submit">Create Account</button>

          <div class="social-signup">
            <h5 class="text-center">Sign up with social account</h5>
            <ul class="d-flex align-items-center justify-content-center">
              <li><a href="#"><span class="icon-google-hangouts position-relative">
                <span class="path1 position-absolute"></span>
                <span class="path2 position-absolute"></span>
                <span class="path3 position-absolute"></span>
                <span class="path4"></span>
              </span></a></li>
              <li><a href="#"><span class="icon-facebook"></span></a></li>
              <li><a href="#"><span class="icon-twitter"></span></a></li>
              <li><a href="#"><span class="icon-instagram-sketched"></span></a></li>
            </ul>
          </div>

        </form>
      </div>
    </div>
  </section>
@push('scripts')
<script>
    $(document).ready(function(){
        $(".toggle-password").click(function(){
            $(this).toggleClass("fa-eye fa-eye-slash");
var id = $(this).attr('id_key');
var input = $("#"+id).attr("type");
// console.log("type >> "+input);

if (input == "password") {
    $("#"+id).attr("type", "text");
} else {
    $("#"+id).attr("type", "password");
}

        });
    });

</script>
@endpush

@endsection
