@extends('front.layouts.app')
@section('title','Forgot Password')
@section('content')

<!-- Banner -->
<section class="common-bnr">
    <div class="container-fluid">
      <div class="signup-block forgot-block mx-auto">
        <h2 class="text-center">Forgot Password.</h2>
        <p class="sub-title text-center">Enter email address associated with your account.</p>
        @include('front.includes.message')

        <form action="{{url('forgot-password')}}" method="post">
            @csrf
          <div class="row mb-4 no-gutters">
            <div class="col-12">
              <div class="row no-gutters align-items-end">
                <div class="col-md-7">
                  <div class="form-group mb-0">
                      <label for="emailtext"><span class="icon-email"></span>Your E-mail</label>
                      <input type="email" class="form-control" id="emailtext" name="email" placeholder="Your E-mail" value="{{old('email')}}">

                  </div>

                </div>

                <div class="col-md-5">
                  <button class="btn btn-submit w-100 mb-0" type="submit">Next</button>
                </div>
              </div>
              <span class="text-white">{{$errors->first('email')}}</span>

            </div>
          </div>

        </form>
      </div>
    </div>
  </section>


@endsection
