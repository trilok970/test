@extends('front.layouts.app')
@section('title','Login')
@section('content')
<!-- Banner -->
<section class="common-bnr">
    <div class="container-fluid">
      <div class="signup-block login-block mx-auto">
        <h2 class="text-center">Log in.</h2>
        <p class="sub-title text-center">Log in with your data that you entered during your registration.</p>

        @include('front.includes.message')
        <form action="{{url('login')}}" method="post" >
            @csrf
          <div class="row mb-4">
            <div class="col-md-6 mb-md-4 mb-sm-5 mb-5">
              <div class="form-group mb-0">
                  <label for="emailtext"><span class="icon-email"></span>Your E-mail</label>
                  <input type="email" class="form-control" id="emailtext" name="email" placeholder="Your E-mail" value="{{old('email')}}">
                  <span class="text-white">{{$errors->first('email')}}</span>

                </div>
            </div>
            <div class="col-md-6 mb-md-4 mb-sm-4 mb-4">
              <div class="form-group mb-0">
                  <label for="password"><span class="icon-lock"></span>Password</label>
                  <input type="password" class="form-control"  id="password" name="password" placeholder="Password" value="">
                  <span id_key="password"  class="icon-right icon-eye toggle-password" ></span>
                  <span class="text-white">{{$errors->first('password')}}</span>

                </div>
            </div>
          </div>

          <div class="d-sm-flex d-block align-items-center mb-4 pb-2">
            <div class="custom-control custom-checkbox mb-sm-0 mb-5">
                <input type="checkbox" class="custom-control-input" id="customCheck" name="example1" checked>
                <label class="custom-control-label" for="customCheck">keep me logged in</label>
            </div>

            <a href="{{url('forgot-password')}}" class="ml-auto forgot-link">Forgot Password?</a>
          </div>



          <button class="btn btn-submit w-100" type="submit">Log In</button>

          <div class="social-signup">
            <h5 class="text-center">Log in with social account</h5>
            <ul class="d-flex align-items-center justify-content-center">
              <li><a href="#"><span class="icon-google-hangouts position-relative">
                <span class="path1 position-absolute"></span>
                <span class="path2 position-absolute"></span>
                <span class="path3 position-absolute"></span>
                <span class="path4"></span>
              </span></a></li>
              <li><a href="#"><span class="icon-facebook"></span></a></li>
              <li><a href="#"><span class="icon-twitter"></span></a></li>
              <li><a href="#"><span class="icon-instagram-sketched"></span></a></li>
            </ul>
          </div>

        </form>
      </div>
    </div>
  </section>
@push('scripts')
<script>
    $(document).ready(function(){
        $(".toggle-password").click(function(){
            $(this).toggleClass("fa-eye fa-eye-slash");
var id = $(this).attr('id_key');
var input = $("#"+id).attr("type");
// console.log("type >> "+input);

if (input == "password") {
    $("#"+id).attr("type", "text");
} else {
    $("#"+id).attr("type", "password");
}

        });
    });

</script>
@endpush


@endsection
