@extends('admin.layouts.app')
@section('content')

<div class="row">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session('success_msg'))
        <div class="alert alert-success">
            {{ session('success_msg') }}
        </div>
        @endif
        @if (session('error_msg'))
        <div class="alert alert-danger">
            {{ session('error_msg') }}
        </div>
        @endif

    </div>
      <div class="row">
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">{{$title}}</h3>




			<form method="POST" action="" accept-charset="UTF-8" id="submit-form" enctype="multipart/form-data">
			 @csrf
					<div class="tile-body">
						<div class="form-group m-t-20">
							<label class="">Manufacturer</label>

                            <div class="input-group mb-3">
                                   <select class="form-control" id="manufacturer_id" name="manufacturer_id">
                                    <option value="">Select Manufacturer</option>
                                    @foreach($manufacturers as $key=>$manufacturer)
                                    <option @if(!empty($data)){{$data->manufacturer_id==$manufacturer->id?'selected':''}}@endif value="{{$manufacturer->id}}">{{$manufacturer->title}}</option>
                                   @endforeach
                                   </select>
                                   <div class="input-group-append">
                                   <button class="btn btn-success add" data-toggle="tooltip" title="Add Manufacturer" ><i class="fa fa-plus-square"></i></button>
                                   </div>
                            </div>



						</div>
						<div class="form-group">
							<label class="">Module Model</label>
							<input class="form-control" placeholder="Module Model" id="module_model" name="module_model" type="text" value="@if(!empty($data)){{$data->module_model}}@endif">
						</div>


						<div class="form-group">
							<label class="">Pmp (W)</label>
							<input class="form-control" placeholder="Pmp (W)" id="pmp" name="pmp" type="number" step="0.01"  value="@if(!empty($data)){{$data->pmp}}@endif">
						</div>


						<div class="form-group">
							<label class="">Voc (V)</label>
							<input class="form-control" placeholder="Voc (V)" id="voc" name="voc" type="number" step="0.01"  value="@if(!empty($data)){{$data->voc}}@endif">
						</div>


						<div class="form-group">
							<label class="">Vmpp (V)</label>
							<input class="form-control" placeholder="Vmpp (V)" id="vmpp" name="vmpp" type="number" step="0.01"  value="@if(!empty($data)){{$data->vmpp}}@endif">
						</div>


						<div class="form-group">
							<label class="">Tvoc (%/C)</label>
							<input class="form-control" placeholder="Tvoc (%/C)" id="tvoc" name="tvoc" type="number" step="0.01"  value="@if(!empty($data)){{$data->tvoc}}@endif">
						</div>


						<div class="form-group">
							<label class="">Isc (A)</label>
							<input class="form-control" placeholder="Isc (A)" id="isc" name="isc" type="number" step="0.01"  value="@if(!empty($data)){{$data->isc}}@endif">
						</div>


						<div class="form-group">
							<label class="">Imp</label>
							<input class="form-control" placeholder="Imp" id="imp" name="imp" type="number" step="0.01"  value="@if(!empty($data)){{$data->imp}}@endif">
						</div>

						<div class="form-group">
							<label class="">Tcpm (%/C</label>
							<input class="form-control" placeholder="Tcpm (%/C)" id="tcpm" name="tcpm" type="number" step="0.01"  value="@if(!empty($data)){{$data->tcpm}}@endif">
						</div>


						<div class="form-group">
							<label class="">Module Length (in)</label>
							<input class="form-control" placeholder="Module Length (in)" id="module_lenght" name="module_lenght" type="number" step="0.01"  value="@if(!empty($data)){{$data->module_lenght}}@endif">
						</div>

						<div class="form-group">
							<label class="">Module Width (in)</label>
							<input class="form-control" placeholder="Module Width (in)" id="module_width" name="module_width" type="number" step="0.01"  value="@if(!empty($data)){{$data->module_width}}@endif">
						</div>

						<div class="form-group">
							<label class="">Allowable Pressure  (Long Side)</label>
							<input class="form-control" placeholder="Allowable Pressure  (Long Side)" id="allowed_pressure_long_side" step="0.01"  name="allowed_pressure_long_side" type="number" value="@if(!empty($data)){{$data->allowed_pressure_long_side}}@endif">
						</div>

						<div class="form-group">
							<label class="">Allowable Pressure (Short Side)</label>
							<input class="form-control" placeholder="Allowable Pressure (Short Side)" id="allowed_pressure_short_side" step="0.01"  name="allowed_pressure_short_side" type="number" value="@if(!empty($data)){{$data->allowed_pressure_short_side}}@endif">
						</div>



					</div>
					<div class="tile-footer">
					  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>
					  @if(!empty($data))
					    @lang("Update Module")
					  @else
						@lang("Add Module")
					  @endif
					  </button>
					</div>
			  </form>


          </div>
        </div>
      </div>


      <!-- The Modal -->
    <div class="modal" id="add_manufacturer">
        <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
            <h4 class="modal-title">Add Manufacturer</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="POST" action="{{ route('admin.manufacturer_add') }}" accept-charset="UTF-8" id="manufacturer-form" enctype="multipart/form-data">
                    @csrf
                           <div class="tile-body">

                               <div class="form-group">
                                   <label class="">Manufacturer</label>
                                   <input name="type" type="hidden" value="module">
                                   <input class="form-control" placeholder="Manufacturer" id="title" name="title" type="text" value="@if(!empty($data)){{$data->title}}@endif">
                               </div>


                           </div>


            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="submit" class="btn btn-success manufacturer-submit">Submit</button>
            </form>

            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
        </div>
    </div>
@endsection


@section('scripts')



<script>
    $(function(){
    $(".add").click(function(e){
        e.preventDefault();
        $("#manufacturer-form").find('#title').val('');
        $("#add_manufacturer").modal();

    });
    $(".manufacturer-submit").click(function(e){
        e.preventDefault();
        let formData = $("#manufacturer-form").serialize();
        let url = $("#manufacturer-form").attr('action');
        let self = this;
        $("#manufacturer-form").find('.form-group > .error').remove();

        $.post(url,formData,function(out) {
            if(out.result === 0) {
                for(var i in out.errors) {
                console.log("#"+i);

                    $("#"+i).parents('.form-group').append('<span class="text-danger error">'+out.errors[i]+'</span>');
                }
            }
            else {
                $("#manufacturer_id").append('<option selected value="'+out.data.id+'">'+out.data.title+'</option>');
        $("#add_manufacturer").modal('hide');

            }
        });

    });
  $('#submit-form').ajaxForm({

    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true);
	  $("body").LoadingOverlay("show");
    },
    error:function(err){
	 $("body").LoadingOverlay("hide");
      handleError(err);
      disable("#submit-btn",false);
    },

    success:function(response){
      disable("#submit-btn",false);
	   $("body").LoadingOverlay("hide");
      if(response.status=='true'){
		  $('#turn-up-error').html('');
                swal({
                    title: response.message,
                    icon: "success",
                    dangerMode: false,
                }).then(function(isConfirm) {
                      if (isConfirm) {
                         window.location.href = '{{route('admin.model_index')}}';
                      }
			});
        //window.location.href = '{{route('admin.model_index')}}';
      }else{
		  $('#turn-up-error').html('');
                  swal({
                    title: response.message,
                    icon: "error",
                    dangerMode: true,
                }).then(function(isConfirm) {
                      if (isConfirm) {
                         window.location.href = '{{route('admin.model_index')}}';
                      }
                });
        //Alert(response.message,false);
      }
    }
  });
});
</script>
@endsection
