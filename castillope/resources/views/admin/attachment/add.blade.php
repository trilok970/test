@extends('admin.layouts.app')
@section('content')

<div class="row">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session('success_msg'))
        <div class="alert alert-success">
            {{ session('success_msg') }}
        </div>
        @endif
        @if (session('error_msg'))
        <div class="alert alert-danger">
            {{ session('error_msg') }}
        </div>
        @endif

    </div>
      <div class="row">
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">{{$title}}</h3>




			<form method="POST" action="" accept-charset="UTF-8" id="submit-form" enctype="multipart/form-data">
			 @csrf
					<div class="tile-body">
						<div class="form-group m-t-20">
							<label class="">Manufacturer</label>

							<div class="input-group mb-3">
                                <select class="form-control" id="manufacturer_id" name="manufacturer_id">
                                 <option value="">Select Manufacturer</option>
                                 @foreach($manufacturers as $key=>$manufacturer)
                                 <option @if(!empty($data)){{$data->manufacturer_id==$manufacturer->id?'selected':''}}@endif value="{{$manufacturer->id}}">{{$manufacturer->title}}</option>
                                @endforeach
                                </select>
                                <div class="input-group-append">
                                <button class="btn btn-success add" data-toggle="tooltip" title="Add Manufacturer" ><i class="fa fa-plus-square"></i></button>
                                </div>
                            </div>

						</div>

						<div class="form-group m-t-20">
							<label class="">Attachment Model No</label>
							<input class="form-control" placeholder="Attachment Model No" id="model_no" name="model_no" type="text" value="@if(!empty($data)){{$data->model_no}}@endif">



						</div>



                        <div class="form-group">
							<label class="">Allowable Pullout Strength (psf)</label>
							<input class="form-control" placeholder="Allowable Pullout Strength (psf)" id="allowable_pollout_strength" name="allowable_pollout_strength" type="number" value="@if(!empty($data)){{$data->allowable_pollout_strength}}@endif">
						</div>


						<div class="form-group">
							<label class="">Allowable Compression Strength (psf)</label>
							<input class="form-control" placeholder="Allowable Compression Strength (psf)" id="allowable_compression_strength" name="allowable_compression_strength" type="number" value="@if(!empty($data)){{$data->allowable_compression_strength}}@endif">
						</div>

						<div class="form-group">
							<label class="">Number of Screws </label>
							<input class="form-control" placeholder="Number of Screws" id="number_of_screws" name="number_of_screws" type="number" value="@if(!empty($data)){{$data->number_of_screws}}@endif">
						</div>


						<div class="form-group">
							<label class="">Note</label>
							<textarea class="form-control" placeholder="Note" id="note" name="note">@if(!empty($data)){{$data->note}}@endif</textarea>
						</div>





					</div>
					<div class="tile-footer">
					  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>
					  @if(!empty($data))
					    @lang("Update Attachmnet")
					  @else
						@lang("Add Attachmnet")
					  @endif
					  </button>
					</div>
			  </form>


          </div>
        </div>
      </div>
  <!-- The Modal -->
  <div class="modal" id="add_manufacturer">
    <div class="modal-dialog">
    <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
        <h4 class="modal-title">Add Manufacturer</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
            <form method="POST" action="{{ route('admin.manufacturer_add') }}" accept-charset="UTF-8" id="manufacturer-form" enctype="multipart/form-data">
                @csrf
                       <div class="tile-body">

                           <div class="form-group">
                               <label class="">Manufacturer</label>
                               <input name="type" type="hidden" value="attachment">
                               <input class="form-control" placeholder="Manufacturer" id="title" name="title" type="text" value="@if(!empty($data)){{$data->title}}@endif">
                           </div>


                       </div>


        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
            <button type="submit" class="btn btn-success manufacturer-submit">Submit</button>
        </form>

        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

    </div>
    </div>
</div>
@endsection

@section('scripts')




<script>
$(function(){
    $(".add").click(function(e){
        e.preventDefault();
        $("#manufacturer-form").find('#title').val('');
        $("#add_manufacturer").modal();

    });
    $(".manufacturer-submit").click(function(e){
        e.preventDefault();
        let formData = $("#manufacturer-form").serialize();
        let url = $("#manufacturer-form").attr('action');
        let self = this;
        $("#manufacturer-form").find('.form-group > .error').remove();

        $.post(url,formData,function(out) {
            if(out.result === 0) {
                for(var i in out.errors) {
                console.log("#"+i);

                    $("#"+i).parents('.form-group').append('<span class="text-danger error">'+out.errors[i]+'</span>');
                }
            }
            else {
                $("#manufacturer_id").append('<option selected value="'+out.data.id+'">'+out.data.title+'</option>');
                $("#add_manufacturer").modal('hide');

            }
        });

    });

  $('#submit-form').ajaxForm({

    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true);
	  $("body").LoadingOverlay("show");
    },
    error:function(err){
	 $("body").LoadingOverlay("hide");
      handleError(err);
      disable("#submit-btn",false);
    },

    success:function(response){
      disable("#submit-btn",false);
	   $("body").LoadingOverlay("hide");
      if(response.status=='true'){
		  $('#turn-up-error').html('');
                swal({
                    title: response.message,
                    icon: "success",
                    dangerMode: false,
                }).then(function(isConfirm) {
                      if (isConfirm) {
                         window.location.href = '{{route('admin.attachment_index')}}';
                      }
			});

        //window.location.href = '{{route('admin.attachment_index')}}';
      }else{
		  $('#turn-up-error').html('');
                  swal({
                    title: response.message,
                    icon: "error",
                    dangerMode: true,
                }).then(function(isConfirm) {
                      if (isConfirm) {
                         window.location.href = '{{route('admin.attachment_index')}}';
                      }
                });
        //Alert(response.message,false);
      }
    }
  });
});
</script>
@endsection
