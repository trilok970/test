@extends('admin.layouts.app')
@section('content')

   <div class="row">
        <div class="col-md-5">
          <div class="tile p-0">
            <h4 class="tile-title folder-head">{{$title}}</h4>
            <div class="tile-body">
              <table class="table table-striped">

              <tbody>
                <tr>
                  <th>Manufacturer</th>
                  <td>{{$data->manufacturer['title']}}</td>

                </tr>
                <tr>
                 <th>Module Model</th>
                 <td>{{$data->model_no}}</td>

                </tr>
                <tr>
                 <th>Allowable Pllout Strength</th>
                 <td>{{$data->allowable_pollout_strength}}</td>

                </tr>

				<tr>
                 <th>Allowable Compression Strength</th>
                 <td>{{$data->allowable_compression_strength}}</td>

                </tr>

				<tr>
                 <th>Number Of Screws</th>
                 <td>{{$data->manufacturer['title']}}</td>

                </tr>
              </tbody>
            </table>
            </div>
          </div>
        </div>
        <div class="col-md-7">
          <div class="tile">

            <div class="mailbox-controls">
              <div class="animated-checkbox">
                <h4 class="tile-title folder-head">Note</h4>
              </div>
            <!--  <div class="btn-group">
                <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-trash-o"></i></button>
                <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-reply"></i></button>
                <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-share"></i></button>
                <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-refresh"></i></button>
              </div> -->
            </div>
            <div class="table-responsive mailbox-messages">
             <table class="table table-striped">

              <tbody>
                <tr>
                  <td>{{$data->note}}</td>

                </tr>

              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>

@endsection
