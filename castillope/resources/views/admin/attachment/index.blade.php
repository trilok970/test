@extends('admin.layouts.app')
@section('content')


 <div class="row">
        <div class="col-md-12">
          <div class="tile">

			  <div class="card-header">
			  <div class="mailbox-controls">
              <h3 class="card-title">{{ $title }}
              </h3>
              <a href="{{route('admin.attachment_create')}}"  class="btn btn-sm btn-dark float-right">Add Attachment</a>
			  </div>
            </div>


            <div class="tile-body">
              <div class="table-responsive">
                <table class="table table-hover table-bordered" id="userTable">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Manufacturer</th>
                      <th>Model No</th>
                      <th>Pollout Strength</th>
                      <th>Compression Strength</th>
                      <th>Screws</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('scripts')



<script>
     $('#userTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.attachment_datatables') !!}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'manufacturer_id', name: 'manufacturer_id'},
            {data: 'model_no', name: 'model_no'},
            {data: 'allowable_pollout_strength', name: 'allowable_pollout_strength'},
            {data: 'allowable_compression_strength', name: 'allowable_compression_strength'},
            {data: 'number_of_screws', name: 'number_of_screws'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        order:[[0,'desc']]
    });


</script>
@endsection
