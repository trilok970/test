@extends('admin.layouts.app')
@section('content')

      <div class="row">
      <div class="col-md-12">

          <div class="tile">
          <div class=" m-b-5">
			  <div class="mailbox-controls">
              <h3 class="card-title">{{ $title }}
              </h3>

			  </div>
            </div>
          <form class="m-t-5" action="{{url()->current()}}" >
                                 <div class="form-group row">
                                    <label class="col-md-1 m-t-15"><b>Search</b></label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="search" name="search" placeholder="Enter Name,Phone no,Email ..." value="{{$search}}" required="">

                                    </div>
                                    <div class="col-md-1">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                    </div>

                                    <div class="col-md-2">
                                    <a href="{{url()->current()}}" class="btn btn-danger">Reset</a>
                                    </div>
                                </div>
                            </form>
            <div class="tile-body">
              <div class="table-responsive">
                <table class="table table-hover table-bordered" id="sampleTable">
                  <thead>
                    <tr>
                      <th>Sr. No.</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Phone Number</th>
                      <th>Email</th>
                      <th>Created On</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @php $i= $users->toArray()['from'] ?? 1; @endphp
                  @foreach($users as $row)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$row->first_name}}</td>
                        <td>{{$row->last_name}}</td>
                        <td>({{ $row->country_code}}) {{$row->phone_number}}</td>
                        <td>{{$row->email}}</td>
                        <td>{{date('d-M-Y',strtotime($row->created_at))}}</td>
                        <td>
                        <div class="float-left mr-2">
                            <!-- <a href="{{ url('admin/user/'.$row->id)}}" class="btn btn-info btn-darken-3 tab-order" > <i class="fa fa-eye"> </i></a> -->
                        </div>
                                    <div class="float-left">
                                        <form action="{{ url('admin/user/'.$row->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">

                                        <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fa fa-lg fa-trash"></i> </button>
                                         </form>

                                    </div>

                        </td>
                        </tr>
                        @php $i++; @endphp
                        @endforeach
                  </tbody>
                </table>
              </div>
              {{ $users->appends(request()->query())}}

            </div>
          </div>
        </div>

      </div>


      @push('scripts')
         <!-- Data table plugin-->
    <script type="text/javascript" src="{{asset('assets/admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <!-- <script type="text/javascript">$('#sampleTable').DataTable();</script> -->

    <script >
                $(function () {
              $('#sampleTable').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true,
                'pageLength'  : 50,
                "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                "aoColumns": [
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                  { "bSortable": false }
                              ]

              });


            });
            </script>

      @endpush
@endsection
