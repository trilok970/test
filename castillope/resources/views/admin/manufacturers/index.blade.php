@extends('admin.layouts.app')
@section('content')


 <div class="row">
        <div class="col-md-12">
          <div class="tile">

			  <div class="card-header">
			  <div class="mailbox-controls">
              <h3 class="card-title">{{ $title }}
              </h3>
              <a href="{{route('admin.manufacturer_create')}}"  class="btn btn-sm btn-dark float-right">Add Manufacturer</a>
			  </div>
            </div>


            <div class="tile-body">
              <div class="table-responsive">
                <table class="table table-hover table-bordered" id="userTable">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Type</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('scripts')



<script>
     $('#userTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.manufacturer_table') !!}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'title', name: 'title'},
            {data: 'type', name: 'type'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        order:[[0,'desc']]
    });


</script>
@endsection
