@extends('admin.layouts.app')
@section('content')

<div class="row">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session('success_msg'))
        <div class="alert alert-success">
            {{ session('success_msg') }}
        </div>
        @endif
        @if (session('error_msg'))
        <div class="alert alert-danger">
            {{ session('error_msg') }}
        </div>
        @endif

    </div>
      <div class="row">
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">{{$title}}</h3>




			<form method="POST" action="" accept-charset="UTF-8" id="submit-form" enctype="multipart/form-data">
			 @csrf
					<div class="tile-body">

						<div class="form-group">
							<label class="">Manufacturer</label>
							<input class="form-control" placeholder="Manufacturer" id="title" name="title" type="text" value="@if(!empty($data)){{$data->title}}@endif">
						</div>
                        <div class="form-group">
							<label class="">Type</label>
                            <select class="form-control" id="type" name="type">
                                <option value="">Select type</option>
                                @foreach($type_array as $key=>$value)
                                <option @if(!empty($data)){{$data->type==$key?'selected':''}}@endif value="{{$key}}">{{$value}}</option>
                               @endforeach
                            </select>
						</div>


					</div>
					<div class="tile-footer">
					  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>
					  @if(!empty($data))
					    @lang("Update Manufacturer")
					  @else
						@lang("Add Manufacturer")
					  @endif
					  </button>
					</div>
			  </form>


          </div>
        </div>
      </div>

@endsection

@section('scripts')



<script>
$(function(){
  $('#submit-form').ajaxForm({

    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true);
	  $("body").LoadingOverlay("show");
    },
    error:function(err){
	 $("body").LoadingOverlay("hide");
      handleError(err);
      disable("#submit-btn",false);
    },

    success:function(response){
      disable("#submit-btn",false);
	   $("body").LoadingOverlay("hide");
      if(response.status=='true'){
			$('#turn-up-error').html('');
                swal({
                    title: response.message,
                    icon: "success",
                    dangerMode: false,
                }).then(function(isConfirm) {
                      if (isConfirm) {
                         window.location.href = '{{route('admin.manufacturer_index')}}';
                      }
			});

      }else{
				$('#turn-up-error').html('');
                  swal({
                    title: response.message,
                    icon: "error",
                    dangerMode: true,
                }).then(function(isConfirm) {
                      if (isConfirm) {
                         window.location.href = '{{route('admin.manufacturer_index')}}';
                      }
                });
        //Alert(response.message,false);
      }
    }

  });
});
</script>
@endsection
