<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{ cAsset('css/main.css') }}">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="{{ cAsset('css/font-awesome.min.css') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/admin/images/favicon.ico')}}">

    <title>Login - Vali Admin</title>
  </head>
  <body>
    <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo">
        <h1>Castillope</h1>
      </div>
      <div class="login-box">

		  <form autocomplete="off" class="login-form" id="loginform" method="post" action="{{url('admin/login')}}">
        {{csrf_field()}}
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>SIGN IN</h3>
			 <div class="form-group">

					@if (count($errors) > 0)
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					@if (session('success_msg'))
					<div class="alert alert-success">
						{{ session('success_msg') }}
					</div>
					@endif
					@if (session('error_msg'))
					<div class="alert alert-danger">
						{{ session('error_msg') }}
					</div>
					@endif

    	</div>
          <div class="form-group">
            <label class="control-label">EMAIL</label>
			  <input autocomplete="off" type="email" id="email" name="email" class="form-control" placeholder="Email" aria-label="Username" aria-describedby="basic-addon1" required="" autofocus>
          </div>
          <div class="form-group">
            <label class="control-label">PASSWORD</label>
			  <input name="password" id="password" type="password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1" required="">
          </div>
          <div class="form-group">
            <div class="utility">
              <div class="animated-checkbox">
                <label>
                  <input type="checkbox"><span class="label-text">Stay Signed in</span>
                </label>
              </div>
              <p class="semibold-text mb-2"><a href="#" data-toggle="flip">Forgot Password ?</a></p>
            </div>
          </div>
          <div class="form-group btn-container">
			  <button id="submit-btn" class="btn btn-primary btn-block" type="submit"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
          </div>
        </form>

		<form class="forget-form" id="submit-form" action="{{route('admin.forgotpassword')}}" method="post">
			{{csrf_field()}}
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?</h3>
          <div class="form-group">
            <label class="control-label">EMAIL</label>
			  <input type="email" name="email" id="email" class="form-control" placeholder="Email Address" aria-label="Username" aria-describedby="basic-addon1">
          </div>
          <div class="form-group btn-container">
			  <button id="submit-btn" class="btn btn-primary btn-block" type="submit"><i class="fa fa-unlock fa-lg fa-fw"></i>RESET</button>
          </div>
          <div class="form-group mt-3">
            <p class="semibold-text mb-0"><a href="#" data-toggle="flip"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
          </div>
        </form>
      </div>
    </section>
    <!-- Essential javascripts for application to work-->

	   <!-- ============================================================== -->
        <!-- All Required js -->
        <!-- ============================================================== -->
        <script src="{{ cAsset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{ cAsset('js/popper.min.js')}}"></script>
    <script src="{{ cAsset('js/bootstrap.min.js')}}"></script>
    <script src="{{ cAsset('js/main.js')}}"></script>
	   <!-- <script src="{{url('admin/js/form.js')}}"></script>-->
    <!-- The javascript plugin to display page loading on top-->
   <script src="{{ cAsset('js/plugins/pace.min.js')}}"></script>

  <script type="text/javascript">
      // Login Page Flipbox control
      $('.login-content [data-toggle="flip"]').click(function() {
      	$('.login-box').toggleClass('flipped');
      	return false;
      });
    </script>
  </body>
</html>
