<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('admin/css/main.css') }}">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="{{ url('admin/css/font-awesome.min.css') }}">
    <title>Login - Vali Admin</title>
  </head>
  <body>
    <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo">
        <h1>Vali</h1>
      </div>
      <div class="login-box">
		  <form autocomplete="off" class="login-form" id="loginform" method="post" action="{{route('admin.login')}}">
        {{csrf_field()}}
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>SIGN IN</h3>
          <div class="form-group">
            <label class="control-label">EMAIL</label>
			  <input autocomplete="off" type="email" id="email" name="email" class="form-control" placeholder="Email" aria-label="Username" aria-describedby="basic-addon1" required="" autofocus>
          </div>
          <div class="form-group">
            <label class="control-label">PASSWORD</label>
			  <input name="password" id="password" type="password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1" required="">
          </div>
          <div class="form-group">
            <div class="utility">
              <div class="animated-checkbox">
                <label>
                  <input type="checkbox"><span class="label-text">Stay Signed in</span>
                </label>
              </div>
              <p class="semibold-text mb-2"><a href="#" data-toggle="flip">Forgot Password ?</a></p>
            </div>
          </div>
          <div class="form-group btn-container">
			  <button id="submit-btn" class="btn btn-primary btn-block" type="submit"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
          </div>
        </form>
        
		<form class="forget-form" id="submit-form" action="{{route('admin.forgotpassword')}}" method="post">
			{{csrf_field()}}
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?</h3>
          <div class="form-group">
            <label class="control-label">EMAIL</label>
			  <input type="email" name="email" id="email" class="form-control" placeholder="Email Address" aria-label="Username" aria-describedby="basic-addon1"> 
          </div>
          <div class="form-group btn-container">
			  <button id="submit-btn" class="btn btn-primary btn-block" type="submit"><i class="fa fa-unlock fa-lg fa-fw"></i>RESET</button>
          </div>
          <div class="form-group mt-3">
            <p class="semibold-text mb-0"><a href="#" data-toggle="flip"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
          </div>
        </form>
      </div>
    </section>
    <!-- Essential javascripts for application to work-->
   <!-- <script src="{{ url('admin/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{ url('admin/js/popper.min.js')}}"></script>
    <script src="{{ url('admin/js/bootstrap.min.js')}}"></script>
    <script src="{{ url('admin/js/main.js')}}"></script>
	   <script src="{{url('admin/js/form.js')}}"></script>-->
    <!-- The javascript plugin to display page loading on top-->
   <!-- <script src="{{ url('admin/js/plugins/pace.min.js')}}"></script>-->
	  
	  
	   <!-- ============================================================== -->
        <!-- All Required js -->
        <!-- ============================================================== -->
        <script src="{{url('admin/assets/libs/jquery/dist/jquery.min.js')}}"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="{{url('admin/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
        <script src="{{url('admin/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{url('admin/dist/js/form.js')}}"></script>
        <script src="{{url('admin/assets/libs/SnackBar-master/dist/snackbar.min.js')}}"></script>
        <script src="{{url('admin/dist/js/custom.js')}}"></script>
	  
	  
    <script type="text/javascript">
		
		
		
		
		$(document).ready(function() { 
	
    // bind 'myForm' and provide a simple callback function 
    $('#loginform').ajaxForm({
    	beforeSubmit:function(){
    		$(".error").remove();
    		disable("#submit-btn",true); 
    	},
    	error:function(err){ 
    		handleError(err);
    		disable("#submit-btn",false); 
    	},
    	success:function(response){ 
			console.log(response);
    		if(response.status=='true'){
    			window.location.href = response.url;
    		}else{
    			disable("#submit-btn",false); 
    			Alert(response.message,false);
    		}
    	}
    });

    $('#submit-form').ajaxForm({
    	beforeSubmit:function(){
    		$(".error").remove();
    		disable("#submit-btn",true); 
    	},
    	error:function(err){ 
    		handleError(err);
    		disable("#submit-btn",false); 
    	},
    	success:function(response){ 
    		disable("#submit-btn",false); 
    		if(response.status=='true'){
    			Alert(response.message,true);
    			$("#email").val(" ");
    		}else{
    			Alert(response.message,false);
    		}
    	}
    }); 
}); 
		
		
      // Login Page Flipbox control
      $('.login-content [data-toggle="flip"]').click(function() {
      	$('.login-box').toggleClass('flipped');
      	return false;
      });
    </script>
  </body>
</html>