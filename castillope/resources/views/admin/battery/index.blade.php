@extends('admin.layouts.app')
@section('content')


 <div class="row">
        <div class="col-md-12">
          <div class="tile">

			  <div class="card-header">
			  <div class="mailbox-controls">
              <h3 class="card-title">{{ $title }}
              </h3>
              <a href="{{route('admin.battery_create')}}"  class="btn btn-sm btn-dark float-right">Add Battery</a>
			  </div>
            </div>


            <div class="tile-body">
              <div class="table-responsive">
                <table class="table table-hover table-bordered" id="userTable">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Model No</th>
                      <th>Manufacturer</th>
                      <th>Maximum Output Power</th>
                      <th>Maximum Output Voltage</th>
                      <th>Maximum Output Current</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('scripts')



<script>
     $('#userTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.battery_datatables') !!}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'model_no', name: 'model_no'},
            {data: 'manufacrer_id', name: 'manufacrer_id'},
            {data: 'maximum_output_power', name: 'maximum_output_power'},
            {data: 'maximum_output_voltage', name: 'maximum_output_voltage'},
            {data: 'maximum_output_current', name: 'maximum_output_current'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        order:[[0,'desc']]
    });


</script>
@endsection

