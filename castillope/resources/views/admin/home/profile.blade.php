@extends('admin.layouts.app')
@section('content')

<div class="row">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session('success_msg'))
        <div class="alert alert-success">
            {{ session('success_msg') }}
        </div>
        @endif
        @if (session('error_msg'))
        <div class="alert alert-danger">
            {{ session('error_msg') }}
        </div>
        @endif

    </div>
      <div class="row">
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">Profile</h3>
			  {{Form::model($data,['id'=>'submit-form','files'=>true])}}
            <div class="tile-body">
            
                <div class="form-group m-t-20">
                    <label class="">Name</label>
                   {{Form::text('name',null,['class'=>'form-control name','placeholder'=>'Name','id'=>'name'])}}
                </div>
                <div class="form-group">
                    <label class="">Email</label>
                    {{Form::email('email',null,['class'=>'form-control name','placeholder'=>'Email','id'=>'email'])}}
                </div>
               
               
            </div>
            <div class="tile-footer">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>@lang("Update Profile")</button>
            </div>
			  {{Form::close()}}
          </div>
        </div>
      </div>

@endsection