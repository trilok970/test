@extends('admin.layouts.app')
@section('content')

<div class="row">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session('success_msg'))
        <div class="alert alert-success">
            {{ session('success_msg') }}
        </div>
        @endif
        @if (session('error_msg'))
        <div class="alert alert-danger">
            {{ session('error_msg') }}
        </div>
        @endif

    </div>
      <div class="row">
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">Change Password</h3>
			   {{Form::model(null,['id'=>'submit-form'])}}
            <div class="tile-body">

                <div class="form-group m-t-20">
                    <label class="">Current Password</label>
                    {{Form::password('current_password',['class'=>'form-control','id'=>'current_password','placeholder'=>'Current Password'])}}
                </div>
                <div class="form-group">
                    <label class="">New Password</label>
                    {{Form::password('new_password',['class'=>'form-control','placeholder'=>'New Password','id'=>'new_password'])}}
                </div>
                <div class="form-group">
                    <label class="">Confirm New Password</label>
                    {{Form::password('confirm_password',['class'=>'form-control','placeholder'=>'Confirm Password','id'=>'confirm_password'])}}
                </div>

            </div>
            <div class="tile-footer">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>@lang("Change Password")</button>
            </div>
			  {{Form::close()}}
          </div>
        </div>
      </div>

@endsection
