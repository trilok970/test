@extends('admin.layouts.app')
@section('content')

<div class="row">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session('success_msg'))
        <div class="alert alert-success">
            {{ session('success_msg') }}
        </div>
        @endif
        @if (session('error_msg'))
        <div class="alert alert-danger">
            {{ session('error_msg') }}
        </div>
        @endif

    </div>
      <div class="row">
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">{{$title}}</h3>
			
			
			
			
			<form method="POST" action="" accept-charset="UTF-8" id="submit-form" enctype="multipart/form-data">
			 @csrf
					<div class="tile-body">
						
						<div class="form-group">
							<label class="">AHJ</label>
							<input class="form-control" placeholder="AHJ" id="ahj" name="ahj" type="text" value="@if(!empty($data)){{$data->ahj}}@endif">
						</div>
						
						
						<div class="form-group">
							<label class="">State</label>
							<input class="form-control" placeholder="Allowable Compression Strength (psf)" id="state" name="state" type="text" value="@if(!empty($data)){{$data->state}}@endif">
						</div>
						
						
						<div class="form-group row">
							  <label class="control-label col-md-3">Fire Setback</label>
							  <div class="col-md-9">
								<div class="form-check">
								  <label class="form-check-label">
									<input class="form-check-input" @if(!empty($data)){{$data->fire_setback==1?'checked':''}}@endif type="radio" value="1" name="fire_setback">Yes
								  </label>
								</div>
								<div class="form-check">
								  <label class="form-check-label">
									<input class="form-check-input" @if(!empty($data)){{$data->fire_setback==0?'checked':''}}@endif type="radio" value="0" name="fire_setback">No
								  </label>
								</div>
							  </div>
						</div>
						
						
						<div class="form-group">
							<label class="">Note</label>
							<textarea class="form-control" placeholder="Note" id="note" name="note">@if(!empty($data)){{$data->note}}@endif</textarea>
						</div>
						
						
						
						
						
					</div>
					<div class="tile-footer">
					  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>
					  @if(!empty($data))
					    @lang("Update AHJ")
					  @else
						@lang("Add AHJ")
					  @endif
					  </button>
					</div>
			  </form>
			
			
          </div>
        </div>
      </div>

@endsection

@section('scripts')



<script>
$(function(){
  $('#submit-form').ajaxForm({
	  
    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true); 
	  $("body").LoadingOverlay("show");
    },
    error:function(err){ 
	 $("body").LoadingOverlay("hide");
      handleError(err);
      disable("#submit-btn",false);  
    },
	 
    success:function(response){ 
      disable("#submit-btn",false); 
	   $("body").LoadingOverlay("hide");
      if(response.status=='true'){
			$('#turn-up-error').html('');
                swal({
                    title: response.message,
                    icon: "success",
                    dangerMode: false,
                }).then(function(isConfirm) {
                      if (isConfirm) {
                         window.location.href = '{{route('admin.ahj_index')}}';
                      }
			});
		  
        //window.location.href = '{{route('admin.ahj_index')}}';
      }else{
				$('#turn-up-error').html('');
                  swal({
                    title: response.message,
                    icon: "error",
                    dangerMode: true,
                }).then(function(isConfirm) {
                      if (isConfirm) {
                         window.location.href = '{{route('admin.ahj_index')}}';
                      }
                });
        //Alert(response.message,false);
      }
    }
	
  }); 
});
</script>
@endsection
