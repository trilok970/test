@extends('admin.layouts.app')
@section('content')


 <div class="row">
        <div class="col-md-12">
          <div class="tile">
			  
			  <div class="card-header">
			  <div class="mailbox-controls">
              <h3 class="card-title">{{ $title }}
              </h3>
              <a href="{{route('admin.ahj_create')}}"  class="btn btn-sm btn-dark float-right">Add AHJ</a>
			  </div>
            </div>
			  
			  
            <div class="tile-body">
              <div class="table-responsive">
                <table class="table table-hover table-bordered" id="userTable">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>AHJ</th>
                      <th>State</th>
                      <th>Fire Setback</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                   
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('scripts')



<script>
     $('#userTable').DataTable({
        processing: true,
        serverSide: true, 
        ajax: '{!! route('admin.ahj_datatables') !!}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'ahj', name: 'ahj'}, 
            {data: 'state', name: 'state'}, 
            {data: 'fire_setback', name: 'fire_setback'}, 
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        order:[[0,'desc']]
    });


</script>
@endsection