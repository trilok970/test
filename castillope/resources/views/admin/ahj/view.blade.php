@extends('admin.layouts.app')
@section('content')

   <div class="row">
        <div class="col-md-5">
          <div class="tile p-0">
            <h4 class="tile-title folder-head">{{$title}}</h4>
            <div class="tile-body">
              <table class="table table-striped">
           
              <tbody>
                <tr>
                  <th>AHJ</th>
                  <td>{{$data->ahj}}</td>
                  
                </tr>
                <tr>
                 <th>State</th>
                 <td>{{$data->state}}</td>
                  
                </tr>
                <tr>
                 <th>Fire Setback</th>
                 <td>@if($data->fire_setback == 1) Yes @else No @endif</td>
                  
                </tr>
              </tbody>
            </table>
            </div>
          </div>
        </div>
        <div class="col-md-7">
          <div class="tile">

            <div class="mailbox-controls">
              <div class="animated-checkbox">
                <h4 class="tile-title folder-head">Note</h4>
              </div>
            <!--  <div class="btn-group">
                <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-trash-o"></i></button>
                <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-reply"></i></button>
                <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-share"></i></button>
                <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-refresh"></i></button>
              </div> -->
            </div>
            <div class="table-responsive mailbox-messages">
             <table class="table table-striped">
              
              <tbody>
                <tr>
                  <td>{{$data->note}}</td>
                  
                </tr>
                
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>

@endsection