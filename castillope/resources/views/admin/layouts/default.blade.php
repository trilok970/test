<!DOCTYPE html>
  <html>

  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
 <!-- <title>::@yield('title','Castillope')::</title> -->
  <title>::@yield('title','fresh cart')::</title>
      <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
      @include('Admin.layout.css')
  </head>
  <body class="app sidebar-mini">
	  <div class="main_wrap">
	  <!-- Navbar-->
	   @include('Admin.layout.header')
	    <!-- Sidebar menu-->
	  @include('Admin.layout.sidebar')
	    <!-- <main class="app-content">

			  <?php //echo  $pageContent ?>


	  </main> -->
	  </div>


  @include('Admin.layout.footer')
  @include('Admin.layout.js')
  </body>
</html>
