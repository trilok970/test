<?php

$adminData = App\Models\Admin::find(Auth::guard('admin')->user()->id);

?>
<!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="{{url('assets/admin/images/user.jpg')}}" alt="User Image" style="width: 50px;">
        <div>
          <p class="app-sidebar__user-name">{{$adminData->name}}</p>
          <p class="app-sidebar__user-designation">{{$adminData->email}}</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item {{ (\Request::is('admin/home')  ? 'active' : '') }}" href="{{route('admin.dashboard')}}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>

        <li><a class="app-menu__item {{ (\Request::is('admin/profile')  ? 'active' : '') }}" href="{{route('admin.profile')}}"><i class="app-menu__icon fa fa-user fa-lg"></i><span class="app-menu__label">Profile</span></a></li>

        <li><a class="app-menu__item {{ (\Request::is('admin/user')  ? 'active' : '') }}" href="{{url('admin/user')}}"><i class="app-menu__icon fa fa-users fa-lg"></i><span class="app-menu__label">Users</span></a></li>

		<li class="treeview {{ (\Request::is('admin/manufacturers') || \Request::is('admin/modules') || \Request::is('admin/attachment') || \Request::is('admin/ahj') || \Request::is('admin/inverter') || \Request::is('admin/battery') || \Request::is('admin/battery/*') || \Request::is('admin/inverter/*')  ? 'is-expanded' : '') }}"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-cog"></i><span class="app-menu__label">Master Settings</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item {{ (\Request::is('admin/manufacturers') || \Request::is('admin/manufacturers/*')  ? 'active' : '') }}" href="{{url('admin/manufacturers')}}"><i class="icon fa fa-circle-o"></i> Manufacturer</a></li>
            <li><a class="treeview-item {{ (\Request::is('admin/modules') || \Request::is('admin/modules/*')  ? 'active' : '') }}" href="{{url('admin/modules')}}"><i class="icon fa fa-circle-o"></i> Module Model</a></li>
            <li><a class="treeview-item {{ (\Request::is('admin/attachment') || \Request::is('admin/attachment/*')  ? 'active' : '') }}" href="{{url('admin/attachment')}}"><i class="icon fa fa-circle-o"></i> Attachments</a></li>
            <li><a class="treeview-item {{ (\Request::is('admin/ahj') || \Request::is('admin/ahj/*')  ? 'active' : '') }}" href="{{url('admin/ahj')}}"><i class="icon fa fa-circle-o"></i> AHJ</a></li>
            <li><a class="treeview-item {{ (\Request::is('admin/inverter') || \Request::is('admin/inverter/*')  ? 'active' : '') }}" href="{{url('admin/inverter')}}"><i class="icon fa fa-circle-o"></i> Inverter</a></li>
            <li><a class="treeview-item {{ (\Request::is('admin/battery') || \Request::is('admin/battery/*') ? 'active' : '') }}" href="{{url('admin/battery')}}"><i class="icon fa fa-circle-o"></i> Battery</a></li>


          </ul>
        </li>


        <li><a class="app-menu__item {{ (\Request::is('admin/change-password')  ? 'active' : '') }}" href="{{route('admin.changepassword')}}"><i class="app-menu__icon fa fa-key fa-lg"></i><span class="app-menu__label">Change Password</span></a></li>
        <li><a class="app-menu__item" href="{{route('admin.logout')}}"><i class="app-menu__icon fa fa-sign-out fa-lg"></i><span class="app-menu__label">Logout</span></a></li>

      </ul>
    </aside>
