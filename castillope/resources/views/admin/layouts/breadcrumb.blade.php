<div class="app-title">
        <div>
          <h1><i class="{{$breadcrumbs[0]['icon'] ?? 'fa fa-th-list'}}"></i>{{isset($title)?$title:''}}</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

		  <?php if(isset($breadcrumbs) && count($breadcrumbs)>0){ ?>
                            <?php  foreach($breadcrumbs as $breadcrumb){ ?>
                                <?php if($breadcrumb['relation']=="link"){?>
                                    <li class="breadcrumb-item"><a href="{{$breadcrumb['url']}}">{{$breadcrumb['name']}}</a></li>
                                <?php }else{ ?>
                                    <li class="breadcrumb-item active">{{$breadcrumb['name']}}</li>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>

        </ul>
      </div>
