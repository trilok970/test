
<script src="{{ asset('assets/admin/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/bootstrap.min.js') }}"></script>

<script src="{{asset('assets/admin/js/main.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/pace.min.js')}}"></script>


<script src="{{asset('assets/admin/js/plugins/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>

<script src="{{asset('assets/admin/js/form.js')}}"></script>
 <script src="{{asset('assets/admin/js/loadingoverlay.min.js')}}"></script>
 <script src="{{asset('assets/admin/assets/libs/SnackBar-master/src/js/snackbar.js')}}"></script>
 <script src="{{asset('assets/admin/js/sweetalert.min.js')}}"></script>
 <script src="{{asset('assets/admin/js/custom.js')}}"></script>

 @stack('scripts')
