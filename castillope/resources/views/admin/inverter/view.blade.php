@extends('admin.layouts.app')
@section('content')

   <div class="row">
        <div class="col-md-5">
          <div class="tile p-0">
            <h4 class="tile-title folder-head">{{$title}}</h4>
            <div class="tile-body">
              <table class="table table-striped">

              <tbody>
                <tr>
                  <th>Manufacturer</th>
                  <td>{{$data->manufacturer->title}}</td>

                </tr>
                <tr>
                 <th>Model No</th>
                 <td>{{$data->model_no}}</td>
                </tr>
                <tr>
                    <th>Inverter Type</th>
                    <td>{{$data->InverterType->name}}</td>
                   </tr>
                <tr>
                 <th>Maximum Input Power (W)</th>
                 <td>{{$data->maximum_input_power}}</td>
                </tr>
                <tr>
                 <th>Maximum Input Voltage (V)</th>
                 <td>{{$data->maximum_input_voltage}}</td>
                </tr>
                <tr>
                 <th>Maximum Input Current (I)</th>
                 <td>{{$data->maximum_input_current}}</td>
                </tr>
                <tr>
                 <th>Maximum Output Power (W)</th>
                 <td>{{$data->maximum_output_power}}</td>
                </tr>

				<tr>
                 <th>Maximum Output Voltage (V)</th>
                 <td>{{$data->maximum_output_voltage}}</td>
                </tr>

				<tr>
                 <th>Maximum Output Current (I)</th>
                 <td>{{$data->maximum_output_current}}</td>
                </tr>
                <tr>
                    <th>Operating Voltage Range (V)</th>
                    <td>{{$data->operating_voltage_range}}</td>
                </tr>
                <tr>
                    <th>MPPT Voltage Range (V)</th>
                    <td>{{$data->mppt_voltage_range}}</td>
                </tr>
                <tr>
                    <th>Number of MPPTs</th>
                    <td>{{$data->no_of_mppts}}</td>
                </tr>
                <tr>
                    <th>Number of Inputs/MPPT</th>
                    <td>{{$data->no_of_inputs_mppt}}</td>
                </tr>
                <tr>
                    <th>Maximum Modules Per String</th>
                    <td>{{$data->maximum_modules_per_string}}</td>
                </tr>
              </tbody>
            </table>
            </div>
          </div>
        </div>
        <div class="col-md-7">
          <div class="tile">

            <div class="mailbox-controls">
              <div class="animated-checkbox">
                <h4 class="tile-title folder-head">Note</h4>
              </div>
            <!--  <div class="btn-group">
                <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-trash-o"></i></button>
                <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-reply"></i></button>
                <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-share"></i></button>
                <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-refresh"></i></button>
              </div> -->
            </div>
            <div class="table-responsive mailbox-messages">
             <table class="table table-striped">

              <tbody>
                <tr>
                  <td>{{$data->note}}</td>

                </tr>

              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>

@endsection
