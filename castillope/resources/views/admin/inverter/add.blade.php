@extends('admin.layouts.app')
@section('title',$title)
@section('content')

<div class="row">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session('success_msg'))
        <div class="alert alert-success">
            {{ session('success_msg') }}
        </div>
        @endif
        @if (session('error_msg'))
        <div class="alert alert-danger">
            {{ session('error_msg') }}
        </div>
        @endif

    </div>
      <div class="row">
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">{{$title}}</h3>




			<form method="POST" action="" accept-charset="UTF-8" id="submit-form" enctype="multipart/form-data">
			 @csrf
					<div class="tile-body">
						<div class="form-group m-t-20">
							<label class="">Manufacturer</label>

                            <div class="input-group mb-3">
                                   <select class="form-control" id="manufacturer_id" name="manufacturer_id">
                                    <option value="">Select Manufacturer</option>
                                    @foreach($manufacturers as $key=>$manufacturer)
                                    <option @if(!empty($data)){{$data->manufacturer_id==$manufacturer->id?'selected':''}}@endif value="{{$manufacturer->id}}">{{$manufacturer->title}}</option>
                                   @endforeach
                                   </select>
                                   <div class="input-group-append">
                                   <button class="btn btn-success add" data-toggle="tooltip" title="Add Manufacturer" ><i class="fa fa-plus-square"></i></button>
                                   </div>
                            </div>



						</div>
						<div class="form-group">
							<label class="">Module Model</label>
							<input class="form-control" placeholder="Model no" id="model_no" name="model_no" type="text" value="@if(!empty($data)){{$data->model_no}}@endif">
						</div>
                        <div class="form-group">
							<label class="">Inverter Type</label>
                            <select class="form-control" id="inverter_type_id" name="inverter_type_id">
                                <option value="">Select inverter type</option>
                                @foreach($inverter_types as $inverter_type)
                                <option @if(!empty($data)){{$data->inverter_type_id==$inverter_type->id?'selected':''}}@endif value="{{$inverter_type->id}}">{{$inverter_type->name}}</option>
                               @endforeach
                            </select>
						</div>

						<div class="form-group">
							<label class="">Maximum Input Power (W)</label>
							<input class="form-control" placeholder="Maximum Input Power (W)" id="maximum_input_power" name="maximum_input_power" type="number" step="0.01"  value="@if(!empty($data)){{$data->maximum_input_power}}@endif">
						</div>


						<div class="form-group">
							<label class="">Maximum Input Voltage (V)</label>
							<input class="form-control" placeholder="Maximum Input Voltage (V)" id="maximum_input_voltage" name="maximum_input_voltage" type="number" step="0.01"  value="@if(!empty($data)){{$data->maximum_input_voltage}}@endif">
						</div>


						<div class="form-group">
							<label class="">Maximum Input Current (I)</label>
							<input class="form-control" placeholder="Maximum Input Current (I)" id="maximum_input_current" name="maximum_input_current" type="number" step="0.01"  value="@if(!empty($data)){{$data->maximum_input_current}}@endif">
						</div>


						<div class="form-group">
							<label class="">Maximum Output Power (W)</label>
							<input class="form-control" placeholder="Maximum Output Power (W)" id="maximum_output_power" name="maximum_output_power" type="number" step="0.01"  value="@if(!empty($data)){{$data->maximum_output_power}}@endif">
						</div>


						<div class="form-group">
							<label class="">Maximum Output Voltage (V)</label>
							<input class="form-control" placeholder="Maximum Output Voltage (V)" id="maximum_output_voltage" name="maximum_output_voltage" type="number" step="0.01"  value="@if(!empty($data)){{$data->maximum_output_voltage}}@endif">
						</div>


						<div class="form-group">
							<label class="">Maximum Output Current (I)</label>
							<input class="form-control" placeholder="Maximum Output Current (I)" id="maximum_output_current" name="maximum_output_current" type="number" step="0.01"  value="@if(!empty($data)){{$data->maximum_output_current}}@endif">
						</div>

						<div class="form-group">
							<label class="">Operating Voltage Range (V)</label>
							<input class="form-control" placeholder="Operating Voltage Range (V)" id="operating_voltage_range" name="operating_voltage_range" type="text"    value="@if(!empty($data)){{$data->operating_voltage_range}}@endif">
						</div>


						<div class="form-group">
							<label class="">MPPT Voltage Range (V)</label>
							<input class="form-control" placeholder="MPPT Voltage Range (V)" id="mppt_voltage_range" name="mppt_voltage_range" type="text"   value="@if(!empty($data)){{$data->mppt_voltage_range}}@endif">
						</div>

						<div class="form-group">
							<label class="">Number of MPPTs</label>
							<input class="form-control" placeholder="Number of MPPTs" id="no_of_mppts" name="no_of_mppts" type="number" step="0.01"  value="@if(!empty($data)){{$data->no_of_mppts}}@endif">
						</div>

						<div class="form-group">
							<label class="">Number of Inputs/MPPT</label>
							<input class="form-control" placeholder="Number of Inputs/MPPT" id="no_of_inputs_mppt" step="0.01"  name="no_of_inputs_mppt" type="number" value="@if(!empty($data)){{$data->no_of_inputs_mppt}}@endif">
						</div>

						<div class="form-group">
							<label class="">Maximum Modules Per String</label>
							<input class="form-control" placeholder="Maximum Modules Per String" id="maximum_modules_per_string" step="0.01"  name="maximum_modules_per_string" type="number" value="@if(!empty($data)){{$data->maximum_modules_per_string}}@endif">
						</div>



					</div>
					<div class="tile-footer">
					  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>
					  @if(!empty($data))
					    @lang("Update Inverter")
					  @else
						@lang("Add Inverter")
					  @endif
					  </button>
					</div>
			  </form>


          </div>
        </div>
      </div>


      <!-- The Modal -->
    <div class="modal" id="add_manufacturer">
        <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
            <h4 class="modal-title">Add Manufacturer</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="POST" action="{{ route('admin.manufacturer_add') }}" accept-charset="UTF-8" id="manufacturer-form" enctype="multipart/form-data">
                    @csrf
                           <div class="tile-body">

                               <div class="form-group">
                                   <label class="">Manufacturer</label>
                                   <input name="type" type="hidden" value="inverter">
                                   <input class="form-control" placeholder="Manufacturer" id="title" name="title" type="text" value="@if(!empty($data)){{$data->title}}@endif">
                               </div>


                           </div>


            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="submit" class="btn btn-success manufacturer-submit">Submit</button>
            </form>

            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
        </div>
    </div>
@endsection


@section('scripts')



<script>
    $(function(){
    $(".add").click(function(e){
        e.preventDefault();
        $("#manufacturer-form").find('#title').val('');
        $("#add_manufacturer").modal();

    });
    $(".manufacturer-submit").click(function(e){
        e.preventDefault();
        let formData = $("#manufacturer-form").serialize();
        let url = $("#manufacturer-form").attr('action');
        let self = this;
        $("#manufacturer-form").find('.form-group > .error').remove();

        $.post(url,formData,function(out) {
            if(out.result === 0) {
                for(var i in out.errors) {
                console.log("#"+i);

                    $("#"+i).parents('.form-group').append('<span class="text-danger error">'+out.errors[i]+'</span>');
                }
            }
            else {
                $("#manufacturer_id").append('<option selected value="'+out.data.id+'">'+out.data.title+'</option>');
        $("#add_manufacturer").modal('hide');

            }
        });

    });
  $('#submit-form').ajaxForm({

    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true);
	  $("body").LoadingOverlay("show");
    },
    error:function(err){
	 $("body").LoadingOverlay("hide");
      handleError(err);
      disable("#submit-btn",false);
    },

    success:function(response){
      disable("#submit-btn",false);
	   $("body").LoadingOverlay("hide");
      if(response.status=='true'){
		  $('#turn-up-error').html('');
                swal({
                    title: response.message,
                    icon: "success",
                    dangerMode: false,
                }).then(function(isConfirm) {
                      if (isConfirm) {
                         window.location.href = '{{route('admin.inverter')}}';
                      }
			});
        //window.location.href = '{{route('admin.model_index')}}';
      }else{
		  $('#turn-up-error').html('');
                  swal({
                    title: response.message,
                    icon: "error",
                    dangerMode: true,
                }).then(function(isConfirm) {
                      if (isConfirm) {
                         window.location.href = '{{route('admin.inverter')}}';
                      }
                });
        //Alert(response.message,false);
      }
    }
  });
});
</script>
@endsection
