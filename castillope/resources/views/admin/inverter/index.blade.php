@extends('admin.layouts.app')
@section('content')


 <div class="row">
        <div class="col-md-12">
          <div class="tile">

			  <div class="card-header">
			  <div class="mailbox-controls">
              <h3 class="card-title">{{ $title }}
              </h3>
              <a href="{{route('admin.inverter_create')}}"  class="btn btn-sm btn-dark float-right">Add Inverter</a>
			  </div>
            </div>


            <div class="tile-body">
              <div class="table-responsive">
                <table class="table table-hover table-bordered" id="userTable">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Module No</th>
                      <th>Manufacturer</th>
                      <th>Maximum Input Power</th>
                      <th>Maximum Input Voltage</th>
                      <th>Maximum Input Current</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('scripts')



<script>
     $('#userTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.inverter_datatables') !!}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'model_no', name: 'model_no'},
            {data: 'manufacrer_id', name: 'manufacrer_id'},
            {data: 'maximum_input_power', name: 'maximum_input_power'},
            {data: 'maximum_input_voltage', name: 'maximum_input_voltage'},
            {data: 'maximum_input_current', name: 'maximum_input_current'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        order:[[0,'desc']]
    });


</script>
@endsection

