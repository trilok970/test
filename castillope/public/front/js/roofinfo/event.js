var Roof = function(){
    this.__construct = function(){
        this.createRoof();
        this.collapseChange();
    }
this.createRoof = function(){
    $(document).on("click",".create-roof-info",function(evt){
        evt.preventDefault();
        var postData = $("#roof-info-form").serialize();
        var project_id = $("#project_id").val();
        var no_of_roofs = $("#no_of_roofs").val();
        var asce_version = $("#asce_version").val();
        var url = $("#roof-info-form").attr('action');
            postData = postData+"&project_id="+project_id+'&asce_version='+asce_version;
            // console.log(postData);
            console.log("no_of_roofs >> "+no_of_roofs);


            $(".form-group > .error").remove();

            $.post(url,postData,function(out){

                if(out.result === 0)
                {
                    // for(let j in out.errors) {
                    //     let index = j.replace(/^\D+/g,'');
                    //     let field = j.replace(/\.[0-9]/g,'').trim();
                    //     let error = out.errors[j].toString();
                    //     error = error.replace(/\.(\d)+/g,'');
                    //     error = error.replace(/_/g,' ');
                    //     $("#roof-info-form").find('.roof-container').eq(index).find('.'+field).parents(".form-group").append('<span class="error">'+error+'</span>');
                    // }
                    for (let i in out.errors) {
                        let index = i.replace(/^\D+/g,'');
                        let feild = i.replace(/\.[0-9]/g,'').trim();
                        let error = out.errors[i].toString();
                        error = error.replace(/\.(\d)+/g,'');
                        error = error.replace(/_/g,' ');
                        $("#roof-info-form").find(".roof-container").eq(index).find('.'+feild).parents(".form-group").append('<span class="error">'+error+'</span>');

                    }


                }
                if(out.result === 1 )
                {
                    location.reload();
                }
            })

    });
}
this.collapseChange = function() {
    $(document).on("click",".card-header",function() {
        // $("#room_div").find(".fas").removeClass("fa-minus-circle").addClass("fa-plus-circle");
        $(this).find(".fas").toggleClass("fa-plus-circle fa-minus-circle");
    });

}
this.__construct();
}

var roof = new Roof;
