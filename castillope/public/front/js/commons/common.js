var Common = function(){
    this.__construct = function(){

        this.formSubmit();

    }

    this.formSubmit = function(){


       $('#create-site-button').click(function(evt){
        //    $(document).on('submit','.create-site-form',function(evt){
            evt.preventDefault();

            var postData = $('#create-site-form').serialize();
            var url = $('#create-site-form').attr('action');
            postData = postData;
            // console.log(postData);
            $.ajaxSetup({
                headers: {
                    'Access-Control-Allow-Headers': '*',
                    'Accept': 'xml'
                }
            });
            $(".form-group > .error").remove();

            $.post(url,postData,function(out){
                // console.log(out);
                if(out.result === 0)
                {
                    for(var i in out.errors){
                        // console.log(i);
                        $("#"+i).parents(".form-group").append('<span class="error" >'+out.errors[i]+'</span>');
                    }
                }
                if(out.result === 1 )
                {
                    if(out.message){
                    $(".message").html(out.message);
                        setTimeout(function(){ $(".alert-success").fadeOut(1000); },1000 );
                    }
                }
            })

        })
    }


    this.__construct();
};

var common = new Common();
