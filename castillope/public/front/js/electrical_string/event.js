var Electrical = function(){
    this.__construct = function() {


        this.createElectricalString();
        this.onLoad();
        this.calculateConduitDerate();
        this.calculateTempDerate();
        this.calculateCorrectedWireSize();
        this.calculateConductorAmpacity();
        this.calculateDeratedAmpacity();
        this.calculateGroundWire();
        this.calculateCorrectedGroundWire();
        this.calculateMaxLength();
        this.ampacityRating();
        this.addCombiner();
        this.addCombinerStringToCurrent();
        this.fillCurrent();
        this.stringCheckbox();
        this.combinerEdit();
        this.voltageDcStringInverter();
        this.voltageDcGeneracInverter();
        this.addOptimizer();
        this.addOptimizerStringToCurrent();
        this.fillCurrentOptimizer();
        this.stringCheckboxOptimizer();
        this.optimizerEdit();

    }
   this.onLoad  = function() {
       $(document).ready(function(){
        $(".ocpd").each(function(){
            
            var current = parseFloat($(this).parents(".roof-container").find(".current").val());
            var ocpd = parseFloat($(this).parents(".roof-container").find(".ocpd").val());
            var self = this;
           
            
            if(isNaN(current))
            current = 0;
            if(isNaN(ocpd))
            ocpd = 0;
    
        if(ocpd == 0) {
            set_ocpd_value(self,current,ocpd);
        }
       });
           $(".ocpd").each(function(){
            var ocpd = parseFloat($(this).parents(".roof-container").find(".ocpd").val());
            var conductor_material = $(this).parents(".roof-container").find(".conductor_material").val();
            var ground_wire_val = $(this).parents(".roof-container").find(".ground_wire").val();
            var self = this;
            if(isNaN(ocpd))
            ocpd = 0;
            if(ground_wire_val == '') {
                ground_wire(self,ocpd,conductor_material);
            }
           });

           $(".no_of_conductors").each(function(){
            var no_of_conductors = parseFloat($(this).parents(".roof-container").find(".no_of_conductors").val());
            var sets_of_wire = parseFloat($(this).parents(".roof-container").find(".sets_of_wire").val());
            var no_of_conduits = parseFloat($(this).parents(".roof-container").find(".no_of_conduits").val());
            var conduit_derate_val = parseFloat($(this).parents(".roof-container").find(".conduit_derate").val());
            var self = this;

            if(conduit_derate_val == 0) {
                conduit_derate(self,no_of_conductors,sets_of_wire,no_of_conduits);
            }
           });

           $(".conduit_location").each(function(){
            var conduit_location = $(this).parents(".roof-container").find(".conduit_location").val();
            var temp_derate_val = $(this).parents(".roof-container").find(".temp_derate").val();
            var self = this;

            if(temp_derate_val == 0) {
                temp_derate(self,conduit_location);
            }
           });
           $(".wire_ampacity").each(function(){
                var conduit_location = $(this).parents(".roof-container").find(".conduit_location").val();
                var wire_ampacity  = parseFloat($(this).parents(".roof-container").find(".wire_ampacity ").val());
                var conductor_material = $(this).parents(".roof-container").find(".conductor_material").val();
                var corrected_wire_size = parseFloat($(this).parents(".roof-container").find(".corrected_wire_size").val());
                var conductor_ampacity_val = parseFloat($(this).parents(".roof-container").find(".conductor_ampacity").val());
                var self = this;
                if(isNaN(conductor_ampacity_val))
                conductor_ampacity_val = 0;


            if(conductor_ampacity_val == 0) {
                conductor_ampacity(self,conduit_location,wire_ampacity,conductor_material,corrected_wire_size,conduit_location);
            }
           });

           $(".max_length").each(function(){
            
            var vd = parseFloat($(this).parents(".roof-container").find(".vd").val());
            var voltage = parseFloat($(this).parents(".roof-container").find(".voltage").val());
            var current = parseFloat($(this).parents(".roof-container").find(".current").val());
            var phase = $(this).parents(".roof-container").find(".phase").val();
            var wire_size = parseFloat($(this).parents(".roof-container").find(".wire_size").val());
            var conductor_material = $(this).parents(".roof-container").find(".conductor_material").val();
            var max_length_val = $(this).parents(".roof-container").find(".max_length").val();
            var self = this;
           
            if(isNaN(vd))
                vd = 0;
            if(isNaN(current))
            current = 0;
            if(isNaN(wire_size))
            wire_size = 0;
            if(isNaN(max_length_val))
            max_length_val = 0;

        if(max_length_val == 0) {
            max_length(self,vd,voltage,current,phase,wire_size,conductor_material);
        }
       });
       


       });
   }
   function set_ocpd_value(self,current,ocpd)
   {
    postData = {
        "current":current,
        "ocpd":ocpd,
        "type":"ocpd_value",
    }
    var url = $('#electrical_formula').val();


    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    $.post(url,postData,function(data){
    if(data.result == 1)
    {
        $(self).parents(".roof-container").find(".ocpd").val(data.data).trigger('change');
    }
    });
   }
   this.ampacityRating = function() {
    $(document).on('change','.terminal_ampacity, .wire_ampacity',function() {
        var terminal_ampacity = parseFloat($(this).parents(".roof-container").find(".terminal_ampacity").val());
        var wire_ampacity = parseFloat($(this).parents(".roof-container").find(".wire_ampacity").val());
        var self = this;


        if(terminal_ampacity > wire_ampacity) {
            $(self).parents(".roof-container").find('.warning_message_terminal_ampacity').html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Terminal Ampacity Rating should not be greater than the wire ampacity rating.');
            $(self).parents(".roof-container").find('.warning_message_terminal_ampacity').val('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Terminal Ampacity Rating should not be greater than the wire ampacity rating.');
        }
        else {
            $(self).parents(".roof-container").find('.warning_message_terminal_ampacity').html('');
            $(self).parents(".roof-container").find('.warning_message_terminal_ampacity').val('');
        }
    });
   };
   function conduit_derate(self,no_of_conductors,sets_of_wire,no_of_conduits) {
    if(isNaN(no_of_conductors))
    no_of_conductors = 0;
    if(isNaN(sets_of_wire))
    sets_of_wire = 0;
    if(isNaN(no_of_conduits))
    no_of_conduits = 0;

        postData = {
            "no_of_conductors":no_of_conductors,
            "sets_of_wire":sets_of_wire,
            "no_of_conduits":no_of_conduits,
            "type":"conduit_derate",
        }
        var url = $('#electrical_formula').val();


        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
        $.post(url,postData,function(data){
        if(data.result == 1)
        {

            $(self).parents(".roof-container").find(".conduit_derate").val(data.data);
        }
        });
   }
   this.calculateConduitDerate = function(){
    $(document).on("change",".conduit-derate-cal",function(){
        var no_of_conductors = parseFloat($(this).parents(".roof-container").find(".no_of_conductors").val());
        var sets_of_wire = parseFloat($(this).parents(".roof-container").find(".sets_of_wire").val());
        var no_of_conduits = parseFloat($(this).parents(".roof-container").find(".no_of_conduits").val());
        var self = this;
        conduit_derate(self,no_of_conductors,sets_of_wire,no_of_conduits)
    });
    };
    function temp_derate(self,conduit_location) {
        if(conduit_location != '') {
            var conduit_location = parseFloat(conduit_location);


            var ambient_temp = parseFloat($("#ambient_temp").val());
            if(isNaN(conduit_location))
            conduit_location = 0;
            if(isNaN(ambient_temp))
            ambient_temp = 0;
                postData = {
                    "conduit_location":conduit_location,
                    "ambient_temp":ambient_temp,
                    "type":"temp_derate",
                }
                var url = $('#electrical_formula').val();

                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.post(url,postData,function(data){
                if(data.result == 1)
                {

                    $(self).parents(".roof-container").find(".temp_derate").val(data.data);
                }
                });
        }
    }
    this.calculateTempDerate = function(){
        $(document).on("change",".temp-derate-cal",function(){
            var conduit_location = $(this).parents(".roof-container").find(".conduit_location").val();
            var self = this;
            temp_derate(self,conduit_location);
        });
        };
        this.calculateCorrectedWireSize = function(){
            $(document).on("change",".terminal-rating-cal",function(){
                var terminal_ampacity = parseFloat($(this).parents(".roof-container").find(".terminal_ampacity").val());
                var conductor_material = $(this).parents(".roof-container").find(".conductor_material").val();
                var wire_size = parseFloat($(this).parents(".roof-container").find(".wire_size").val());
                var current = parseFloat($(this).parents(".roof-container").find(".current").val());
                if(isNaN(terminal_ampacity))
                terminal_ampacity = 0;
                if(isNaN(wire_size))
                wire_size = 0;
                if(isNaN(current))
                current = 0;
                    postData = {
                        "terminal_ampacity":terminal_ampacity,
                        "conductor_material":conductor_material,
                        "wire_size":wire_size,
                        "current":current,
                        "type":"terminal_rating",
                    }
                    var url = $('#electrical_formula').val();
                    var self = this;

                    $(self).parents(".roof-container").find(".corrected_wire_size").val(wire_size).trigger('change');
                    $.ajaxSetup({
                        headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                      });
                    if(terminal_ampacity != 0 && wire_size != 0 && conductor_material != '') {
                        $.post(url,postData,function(data){
                        if(data.result == 1)
                        {

                            $(self).parents(".roof-container").find(".warning_message").html(data.warning_message);
                            $(self).parents(".roof-container").find(".warning_message").val(data.warning_message);

                        }
                        });
                    }
                    else {
                        $(self).parents(".roof-container").find(".warning_message").html('');
                        $(self).parents(".roof-container").find(".warning_message").val('');

                    }
            });
            };
            function conductor_ampacity(self,conduit_location,wire_ampacity,conductor_material,corrected_wire_size,conduit_location) {

                if(conduit_location != 'i' && conduit_location != 'e' && conduit_location != '') {
                    if(isNaN(wire_ampacity ))
                    wire_ampacity  = 0;
                    if(isNaN(corrected_wire_size))
                        corrected_wire_size = 0;
                    // if(isNaN(conduit_location))
                    //     conduit_location = 0;
                    postData = {
                                "wire_ampacity":wire_ampacity,
                                "conductor_material":conductor_material,
                                "corrected_wire_size":corrected_wire_size,
                                "conduit_location":conduit_location,
                                "type":"conductor_ampacity",
                    }
                    var url = $('#electrical_formula').val();
                    $.ajaxSetup({
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    if(wire_ampacity  != 0 && corrected_wire_size != 0 && conductor_material != '') {
                        $.post(url,postData,function(data){
                        if(data.result == 1)
                        {

                            $(self).parents(".roof-container").find(".conductor_ampacity").html(data.ampacity_rating_dropdown).trigger('change');
                        }
                        });
                    }
                    else {
                            $(self).parents(".roof-container").find(".conductor_ampacity").html('').trigger('change');
                    }


                }


            }
            this.calculateConductorAmpacity = function(){
                $(document).on("change",".conductor-ampacity-cal",function(){

                var conduit_location = $(this).parents(".roof-container").find(".conduit_location").val();
                var wire_ampacity  = parseFloat($(this).parents(".roof-container").find(".wire_ampacity ").val());
                var conductor_material = $(this).parents(".roof-container").find(".conductor_material").val();
                var corrected_wire_size = parseFloat($(this).parents(".roof-container").find(".corrected_wire_size").val());
                var self = this;

                conductor_ampacity(self,conduit_location,wire_ampacity,conductor_material,corrected_wire_size,conduit_location);


                });
                };
                this.calculateDeratedAmpacity = function(){
                    $(document).on("change",".derated-ampacity-cal",function(){
                        var conductor_ampacity = parseFloat($(this).parents(".roof-container").find(".conductor_ampacity").val());
                        var temp_derate = $(this).parents(".roof-container").find(".temp_derate").val();
                        var conduit_derate = parseFloat($(this).parents(".roof-container").find(".conduit_derate").val());
                        var ocpd = parseFloat($(this).parents(".roof-container").find(".ocpd").val());
                        var wire_size = parseFloat($(this).parents(".roof-container").find(".wire_size").val());
                        var terminal_ampacity = parseFloat($(this).parents(".roof-container").find(".terminal_ampacity").val());
                        var conductor_material = $(this).parents(".roof-container").find(".conductor_material").val();

                        if(isNaN(conductor_ampacity))
                        conductor_ampacity = 0;
                        if(isNaN(temp_derate))
                        temp_derate = 0;
                        if(isNaN(conduit_derate))
                        conduit_derate = 0;
                        if(isNaN(ocpd))
                        ocpd = 0;
                        if(isNaN(wire_size))
                        wire_size = 0;
                        if(isNaN(terminal_ampacity))
                        terminal_ampacity = 0;
                        if(isNaN(terminal_ampacity))
                        terminal_ampacity = 0;
                            postData = {
                                "conductor_ampacity":conductor_ampacity,
                                "temp_derate":temp_derate,
                                "terminal_ampacity":terminal_ampacity,
                                "conductor_material":conductor_material,
                                "conduit_derate":conduit_derate,
                                "ocpd":ocpd,
                                "wire_size":wire_size,
                                "type":"derated_ampacity",
                            }

                            var url = $('#electrical_formula').val();
                            var self = this;

                            $.ajaxSetup({
                                headers: {
                                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                              });
                            if(terminal_ampacity != 0 && conduit_derate != 0 && conductor_material != ''  && ocpd != 0 && wire_size != 0) {
                                $.post(url,postData,function(data){
                                if(data.result == 1)
                                {

                                    $(self).parents(".roof-container").find(".derated_ampacity").val(data.data);
                                    $(self).parents(".roof-container").find(".warning_message_ter").html(data.warning_message_ter);
                                    $(self).parents(".roof-container").find(".warning_message_ter").val(data.warning_message_ter);
                                    $(self).parents(".roof-container").find(".warning_message_ocpd").html(data.warning_message_ocpd);
                                    $(self).parents(".roof-container").find(".warning_message_ocpd").val(data.warning_message_ocpd);

                                }
                                });
                            }
                            else {
                                    $(self).parents(".roof-container").find(".derated_ampacity").val('');
                                    $(self).parents(".roof-container").find(".warning_message_ter").html('');
                                    $(self).parents(".roof-container").find(".warning_message_ter").val('');
                                    $(self).parents(".roof-container").find(".warning_message_ocpd").html('');
                                    $(self).parents(".roof-container").find(".warning_message_ocpd").val('');
                            }
                    });
                    };
                    function ground_wire(self,ocpd,conductor_material) {

                        postData = {
                            "conductor_material":conductor_material,
                            "ocpd":ocpd,
                            "type":"ground_wire",
                        }

                        var url = $('#electrical_formula').val();


                        $.ajaxSetup({
                            headers: {
                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                          });
                        if( conductor_material != '' && ocpd != 0 ) {
                            $.post(url,postData,function(data){
                            if(data.result == 1)
                            {

                                $(self).parents(".roof-container").find(".ground_wire").html(data.ground_wire_dropdown).trigger('change');
                                $(self).parents(".roof-container").find(".ground_wire").val(10);
                            }
                            });
                        }
                        else {
                            $(self).parents(".roof-container").find(".ground_wire").val(10);
                        }
                    }
                    this.calculateGroundWire = function(){
                        $(document).on("change",".ground-wire-cal",function(){
                            var ocpd = parseFloat($(this).parents(".roof-container").find(".ocpd").val());
                            var conductor_material = $(this).parents(".roof-container").find(".conductor_material").val();
                            var self = this;

                            if(isNaN(ocpd))
                            ocpd = 0;
                            ground_wire(self,ocpd,conductor_material);
                        });
                        };
                        this.calculateCorrectedGroundWire = function(){
                            $(document).on("change",".corrected-ground-wire-cal",function(){
                                var wire_size = parseFloat($(this).parents(".roof-container").find(".wire_size").val());
                                var corrected_wire_size = parseFloat($(this).parents(".roof-container").find(".corrected_wire_size").val());
                                var ground_wire = parseFloat($(this).parents(".roof-container").find(".ground_wire").val());
                                var ocpd = parseFloat($(this).parents(".roof-container").find(".ocpd").val());
                                var conductor_material = $(this).parents(".roof-container").find(".conductor_material").val();

                                if(isNaN(wire_size))
                                wire_size = 0;
                                if(isNaN(corrected_wire_size))
                                corrected_wire_size = 0;
                                if(isNaN(ground_wire))
                                ground_wire = 0;
                                if(isNaN(ocpd))
                                ocpd = 0;
                                    postData = {
                                        "wire_size":wire_size,
                                        "corrected_wire_size":corrected_wire_size,
                                        "ground_wire":ground_wire,
                                        "ocpd":ocpd,
                                        "conductor_material":conductor_material,
                                        "type":"corrected_ground_wire",
                                    }

                                    var url = $('#electrical_formula').val();
                                    var self = this;

                                    $.ajaxSetup({
                                        headers: {
                                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                      });
                                    if( wire_size != 0 && corrected_wire_size != 0 && ground_wire != 0 ) {
                                        $.post(url,postData,function(data){
                                        if(data.result == 1)
                                        {
                                            $(self).parents(".roof-container").find(".corrected_ground_wire").val(data.data);
                                        }
                                        });
                                    }
                                    else {
                                        $(self).parents(".roof-container").find(".corrected_ground_wire").val('');
                                    }
                            });
                            };
                            function max_length(self,vd,voltage,current,phase,wire_size,conductor_material) {
                                if(isNaN(vd))
                                vd = 0;
                                if(isNaN(voltage))
                                voltage = 0;
                                if(isNaN(current))
                                current = 0;
                                if(isNaN(wire_size))
                                wire_size = 0;
                                    postData = {
                                        "vd":vd,
                                        "voltage":voltage,
                                        "current":current,
                                        "phase":phase,
                                        "wire_size":wire_size,
                                        "conductor_material":conductor_material,
                                        "type":"max_length",
                                    }

                                    var url = $('#electrical_formula').val();

                                    $.ajaxSetup({
                                        headers: {
                                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                      });
                                    if( vd != 0 && voltage != 0 && phase != '' && current != 0 && wire_size != 0 && conductor_material != '' ) {
                                        $.post(url,postData,function(data){
                                        if(data.result == 1)
                                        {

                                            $(self).parents(".roof-container").find(".max_length").val(data.data);
                                        }
                                        });
                                    }
                                    else {
                                        $(self).parents(".roof-container").find(".max_length").val('');
                                    }
                            }
                            this.calculateMaxLength = function(){
                                $(document).on("change",".max-length-cal",function(){
                                    var vd = parseFloat($(this).parents(".roof-container").find(".vd").val());
                                    var voltage = parseFloat($(this).parents(".roof-container").find(".voltage").val());
                                    var current = parseFloat($(this).parents(".roof-container").find(".current").val());
                                    var phase = $(this).parents(".roof-container").find(".phase").val();
                                    var wire_size = parseFloat($(this).parents(".roof-container").find(".wire_size").val());
                                    var conductor_material = $(this).parents(".roof-container").find(".conductor_material").val();
                                    var self = this;

                                    max_length(self,vd,voltage,current,phase,wire_size,conductor_material);

                                });
                                };
    // For quantity validation
    function isNumber(evt, element)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (
               (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
               (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
               (charCode < 48 || charCode > 57))
               return false;

               return true;
     }
      /* Submit Electrical Info  */
      this.createElectricalString = function() {
        $(".create-string-info").click(function(evt) {
            evt.preventDefault();
            var self = this;
            var postData = $(self).parents(".structural-collapse").find(".create-electrical-string-form").serialize();
            var url = $(self).parents(".structural-collapse").find(".create-electrical-string-form").attr('action');

            var current = $(".card").last().find(".current").val();
            $(".combiner-current-div").html("");

            if(current == 0) {
                $(".combiner-current-div").html("<b class='alert alert-danger'>Please fill current value.</b>");
                return false;
            }



            $(".form-group > .error").remove();
            $.post(url,postData,function(out) {
                if(out.result === 0)
                {
                    $(".combiner-current-div").html("<b class='alert alert-danger'>Please fill all requied feild.</b>");
                    for(var i in out.errors) {
                        let index = i.replace(/^\D+/g,'');
                        let feild = i.replace(/\.[0-9]/g,'').trim();
                        // console.log(feild);
                        let error = out.errors[i].toString();
                        error = error.replace(/\.(\d)+/g,'');
                        error = error.replace(/_/g,' ');
                        $(".card").find('.card-body').eq(index).find('.'+feild).parents(".form-group").append('<span class="error">'+error+'</span>');

                    }
                }
                else if(out.result === 1) {
                    location.reload();
                }
            });
        });
    };

    this.addCombiner = function() {
        $(".add-combiner").click(function(evt){
            evt.preventDefault();
           var combiner_length = parseInt($("#combiner_div .card").length)+1;
            $(".combiner-edit").hide();
            var section = $(".card.string").not('generac').last().clone(true);

            $("#add_type").val('combiner');
            $(".add-optimizer").hide();



            $(section).find('.string_id').html('AC');
            $(section).find('.string_id_no').html(" &nbsp;#"+combiner_length);
            $(section).find('.from_val').val('inverter').trigger('change');
            $(section).find('.current').remove();
            $(section).find('.current-label').append('<input readonly type="text" class="form-control input-style2 current" name="current[]"  value="0">');
            $(section).find('.current').val(0);
            $(section).find('.id').val(0);
            $(section).find('.type').val('combiner');
            $(section).find('.combiner-string-button-div').show();
            $(section).find('.add-combiner').removeClass('fill-combiner').addClass('add-combiner-string-to-current');
            $(section).find('.add-combiner').show();
            $(section).find('.add-combiner').html('Add Combiner String');
            $(section).find('.rm').html('');
            // $(section).find(".roof_info_id").last().val(0);
            if(parseInt($("#combiner_div .card").length) == 0) {
                $(section).find('.to_val').append('<option value="combiner" selected>Combiner</option>');
            }
            $(section).find('.card-header').attr('id','combiner-header-'+combiner_length);
            $(section).find('.card-header').attr('data-target','#combiner-collapse-'+combiner_length);
            $(section).find('.card-header').attr('aria-controls','combiner-collapse-'+combiner_length);
            $(section).find('.cus_acro').attr('id','combiner-collapse-'+combiner_length);
            $(section).find('.cus_acro').attr('aria-labelledby','combiner-header-'+combiner_length);

            $('.card').find('.cus_acro').removeClass('show');
            $('.card').find('.ml-auto').removeClass('icon-accordian-2-minus').addClass('icon-accordian-2-add');
            $(section).find('.cus_acro').addClass('show');
            $(section).find('.ml-auto').removeClass('icon-accordian-2-add').addClass('icon-accordian-2-minus');
            $(section).find('.combiner-edit').remove();

            // $(section).find('.rm').append('<span class="text-danger icon-accordian-2 ml-auto remove-combiner"><i class="fas fa-trash-alt"></i></span>');

            $(section).find('span.selection').remove();
            $(section).find('.custom-select2').select2();

            $("#combiner_div").append(section);

            $("#combiner_div").find('.card').last().attr('id','combiner-'+combiner_length);
            $("#combiner_div").find('.string_checkbox').prev().show();
            $(section).find('.string_checkbox').last().hide();


            // $("#combiner_div").find('.card').last().removeClass('d-none');

            $(document).find('.custom-select2').select2();


            // updateNoOfRoofs();
            $(".combiner-current-values").val(0);

            $(".numeric_feild_discount").keypress(function(event){
            return isNumber(event, this);
            });
            $(this).hide();

        });
    };
    this.addCombinerStringToCurrent = function() {
        $(document).on("click",".add-combiner-string-to-current",function(evt) {
            evt.preventDefault();
            var self = this;
            $(".notSelectedCombiner:checkbox").show();
            $(self).html('Fill Current');
            $(self).removeClass('add-combiner-string-to-current').addClass('fill-current');
            var current = parseFloat($(self).parents('.roof-container').find('.current').val());
            if(isNaN(current))
                current = 0;
            $(".combiner-current-values").val(current);

        });
    };
    this.fillCurrent = function() {
        $(document).on("click",".fill-current",function(evt) {
            evt.preventDefault();
            var self = this;
            // $(self).html('Add Combiner String');
            $(self).addClass('add-combiner-string-to-current').removeClass('fill-current');
            var combiner_current_values = parseFloat($(".combiner-current-values").val());
            var combiner_set_of_wire_values = parseFloat($(".combiner-set-of-wire-values").val());

            if(combiner_current_values > 0) {
                // $(".add-combiner").show();

                $(self).hide();
                $(".string_checkbox_val").hide();
                $(".create-string-info").show();

            }

            $(self).parents('.roof-container').find(".current").val(combiner_current_values);
            $(self).parents('.roof-container').find(".sets_of_wire").val(combiner_set_of_wire_values).trigger('change');
        });
    };
    this.stringCheckbox = function() {
        $(document).on("click",".string_checkbox_val",function() {
            var self = this;
            var combiner_current_values = parseFloat($(".combiner-current-values").val());
            var combiner_set_of_wire_values = parseFloat($(".combiner-set-of-wire-values").val());


            if(isNaN(combiner_set_of_wire_values))
            combiner_set_of_wire_values = 0;

            if($(self).is(":checked")) {
                var val = parseFloat($(self).parents(".card").find(".current").val());
                var sets_of_wire = parseFloat($(self).parents('.card').find('.sets_of_wire').val());
            }
            else {
                var val = 0;
                var sets_of_wire = 0;
                combiner_current_values = combiner_current_values - parseFloat($(self).parents(".card").find(".current").val());
                console.log("combiner_current_values >> "+combiner_current_values);
                combiner_set_of_wire_values = combiner_set_of_wire_values - parseFloat($(self).parents('.card').find('.sets_of_wire').val());
            }
            combiner_current_values = combiner_current_values + val;
            combiner_current_values = combiner_current_values.toFixed(4);

            $(".combiner-current-values").val(combiner_current_values );

            combiner_set_of_wire_values = combiner_set_of_wire_values + sets_of_wire;
            combiner_set_of_wire_values = combiner_set_of_wire_values.toFixed(4);

            $(".combiner-set-of-wire-values").val(combiner_set_of_wire_values);
        });
    };
    this.combinerEdit = function() {
        $(document).on("click",".combiner-edit",function() {
            var self = this;
            var combiner_id = $(self).attr('id');
            var sets_of_wire = $(self).parents('.card').find('.sets_of_wire').val();
            var current = $(self).parents('.card').find('.current').val();
            var collapse_id = $(self).parents('.card').find('.cus_acro').attr('id');
            // $('.card').find('.cus_acro').addClass('show');
            $(self).parents('.card').find('.cus_acro').removeClass('show');
            $("#add_type").val('combiner');


            console.log("collapse_id >> "+collapse_id);

            var url = $('#electrical_formula').val();

            $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });
              postData = {
                "combiner_id":combiner_id,
                "type":"combinerEdit",
            }
            if( combiner_id != 0 ) {
                $.post(url,postData,function(data){
                if(data.result == 1)
                {
                    $('.combiner-edit').removeClass('text-warning').addClass('text-primary');
                    $(self).removeClass('text-primary').addClass('text-warning');
                    $(".combiner-current-values").val(0);
                    $(".combiner-set-of-wire-values").val(0);
                    $("#combiner_id").val(combiner_id);
                    $(".string_checkbox_val").prop('checked',false);
                    $(".string_checkbox_val").hide();
                    $(".combiner-string-button-div").hide();
                    $(".create-string-info").hide();
                    $(".add-combiner").hide();
                    $(".add-optimizer").hide();
                    $(".optimizer-edit").hide();

                    $(".notSelectedCombiner:checkbox").show();
                    $(self).parents('.card').find('.combiner-string-button-div').show();
                    $(self).parents('.card').find('.string_checkbox_val').hide();
                    $(self).parents('.card').find('.add-combiner-string-to-current').html('Fill Current');
                    $(self).parents('.card').find('.add-combiner-string-to-current').removeClass('add-combiner-string-to-current').addClass('fill-current');
                    var total_val = 0;
                    $(".string_checkbox_val").each(function(){
                        var string_id = parseInt($(this).attr('data-string-id'));


                        if(jQuery.inArray(string_id, data.data) !== -1) {
                            total_val += parseFloat($(this).val());


                            $(this).show();
                            $(this).prop('checked',true);
                            }
                    });
                    total_val = total_val.toFixed(4);

                    // $(".combiner-current-values").val(total_val);
                    $(".combiner-current-values").val(current);
                    $(".combiner-set-of-wire-values").val(sets_of_wire);


                }
                else {
                    $("#combiner_id").val(0);
                }
                });
            }
            else {
            }


        });
    };
    this.voltageDcStringInverter = function() {
        $(document).on("change keyup",".voltage_dc_string_inverter",function() {
            var self = this;
            var value = $(self).parents('.card').find(".voltage_dc_string_inverter").val();
            if(value > 600){
                $(self).parents('.card').find(".warning_message_voltage_dc_string_inverter").show();
                $(self).parents('.card').find(".warning_message_voltage_dc_string_inverter").html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Voltage can not be over 600.');
                $(self).parents('.card').find(".warning_message_voltage_dc_string_inverter").val('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Voltage can not be over 600.');
            }
            else {
                $(self).parents('.card').find(".warning_message_voltage_dc_string_inverter").hide();
                $(self).parents('.card').find(".warning_message_voltage_dc_string_inverter").html('');
                $(self).parents('.card').find(".warning_message_voltage_dc_string_inverter").val('');
            }
        });
    };
    this.voltageDcGeneracInverter = function() {
        $(document).on("change keyup",".voltage_dc_generac_inverter",function() {
            var self = this;
            var value = $(self).parents('.card').find(".voltage_dc_generac_inverter").val();
            if(value > 420){
                $(self).parents('.card').find(".warning_message_voltage_dc_generac_inverter").show();
                $(self).parents('.card').find(".warning_message_voltage_dc_generac_inverter").html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Voltage can not be over 420.');
                $(self).parents('.card').find(".warning_message_voltage_dc_generac_inverter").val('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Voltage can not be over 420.');
            }
            else {
                $(self).parents('.card').find(".warning_message_voltage_dc_generac_inverter").hide();
                $(self).parents('.card').find(".warning_message_voltage_dc_generac_inverter").html('');
                $(self).parents('.card').find(".warning_message_voltage_dc_generac_inverter").val('');
            }
        });
    };
    this.addOptimizer = function() {
        $(".add-optimizer").click(function(evt){
            evt.preventDefault();
            var optimizer_length = parseInt($("#optimizer_div .card").length)+1;
            console.log("optimizer_length >> " + optimizer_length);
            $(".optimizer-edit").hide();
            var section = $(".card.generac").last().clone(true);

            $("#add_type").val('optimizer');
            $(".add-combiner").hide();


            $(section).find('.string_id').html('Optimizer');
            $(section).find('.string_id_no').html(" &nbsp;#"+optimizer_length);
            $(section).find('.from_val').val('inverter').trigger('change');
            $(section).find('.current').remove();
            $(section).find('.current-label').append('<input readonly type="text" class="form-control input-style2 current" name="current[]"  value="0">');
            $(section).find('.current').val(0);
            $(section).find('.id').val(0);
            $(section).find('.type').val('optimizer');
            $(section).find('.optimizer-string-button-div').show();
            $(section).find('.add-optimizer').removeClass('fill-optimizer').addClass('add-optimizer-string-to-current');
            $(section).find('.add-optimizer').show();
            $(section).find('.add-optimizer').html('Add Optimizer String');
            $(section).find('.rm').html('');
            // $(section).find(".roof_info_id").last().val(0);
            
            $(section).find('.card-header').attr('id','optimizer-header-'+optimizer_length);
            $(section).find('.card-header').attr('data-target','#optimizer-collapse-'+optimizer_length);
            $(section).find('.card-header').attr('aria-controls','optimizer-collapse-'+optimizer_length);
            $(section).find('.cus_acro').attr('id','optimizer-collapse-'+optimizer_length);
            $(section).find('.cus_acro').attr('aria-labelledby','optimizer-header-'+optimizer_length);

            $('.card').find('.cus_acro').removeClass('show');
            $('.card').find('.ml-auto').removeClass('icon-accordian-2-minus').addClass('icon-accordian-2-add');
            $(section).find('.cus_acro').addClass('show');
            $(section).find('.ml-auto').removeClass('icon-accordian-2-add').addClass('icon-accordian-2-minus');
            $(section).find('.optimizer-edit').remove();

            // $(section).find('.rm').append('<span class="text-danger icon-accordian-2 ml-auto remove-combiner"><i class="fas fa-trash-alt"></i></span>');

            $(section).find('span.selection').remove();
            $(section).find('.custom-select2').select2();

            $("#optimizer_div").append(section);

            $("#optimizer_div").find('.card').last().attr('id','optimizer-'+optimizer_length);
            $("#optimizer_div").find('.string_checkbox').prev().show();
            $(section).find('.string_checkbox').last().hide();


            // $("#optimizer_div").find('.card').last().removeClass('d-none');

            $(document).find('.custom-select2').select2();


            // updateNoOfRoofs();
            $(".optimizer-current-values").val(0);

            $(".numeric_feild_discount").keypress(function(event){
            return isNumber(event, this);
            });
            $(this).hide();

        });
    };
    this.addOptimizerStringToCurrent = function() {
        $(document).on("click",".add-optimizer-string-to-current",function(evt) {
            evt.preventDefault();
            var self = this;
            $(".notSelectedOptimizer:checkbox").show();
            $(self).html('Fill Current');
            $(self).removeClass('add-optimizer-string-to-current').addClass('fill-current-optimizer');
            var current = parseFloat($(self).parents('.roof-container').find('.current').val());
            if(isNaN(current))
                current = 0;
            $(".optimizer-current-values").val(current);
    
        });
    };
    this.fillCurrentOptimizer = function() {
        $(document).on("click",".fill-current-optimizer",function(evt) {
            evt.preventDefault();
            var self = this;
            // $(self).html('Add Optimizer String');
            $(self).addClass('add-optimizer-string-to-current').removeClass('fill-current-optimizer');
            var optimizer_current_values = parseFloat($(".optimizer-current-values").val());
            var optimizer_set_of_wire_values = parseFloat($(".optimizer-set-of-wire-values").val());
    
            if(optimizer_current_values > 0) {
                // $(".add-optimizer").show();
    
                $(self).hide();
                $(".string_checkbox_val").hide();
                $(".create-string-info").show();
    
            }
    
            $(self).parents('.roof-container').find(".current").val(optimizer_current_values);
            $(self).parents('.roof-container').find(".sets_of_wire").val(optimizer_set_of_wire_values).trigger('change');
        });
    };
    this.stringCheckboxOptimizer = function() {
        $(document).on("click",".string_checkbox_val",function() {
            var self = this;
            var optimizer_current_values = parseFloat($(".optimizer-current-values").val());
            var optimizer_set_of_wire_values = parseFloat($(".optimizer-set-of-wire-values").val());
    
    
            if(isNaN(optimizer_set_of_wire_values))
            optimizer_set_of_wire_values = 0;
    
            if($(self).is(":checked")) {
                var val = parseFloat($(self).parents(".card").find(".current").val());
                var sets_of_wire = parseFloat($(self).parents('.card').find('.sets_of_wire').val());
            }
            else {
                var val = 0;
                var sets_of_wire = 0;
                optimizer_current_values = optimizer_current_values - parseFloat($(self).parents(".card").find(".current").val());
                console.log("optimizer_current_values >> "+optimizer_current_values);
                optimizer_set_of_wire_values = optimizer_set_of_wire_values - parseFloat($(self).parents('.card').find('.sets_of_wire').val());
            }
            optimizer_current_values = optimizer_current_values + val;
            optimizer_current_values = optimizer_current_values.toFixed(4);
    
            $(".optimizer-current-values").val(optimizer_current_values );
    
            optimizer_set_of_wire_values = optimizer_set_of_wire_values + sets_of_wire;
            optimizer_set_of_wire_values = optimizer_set_of_wire_values.toFixed(4);
    
            $(".optimizer-set-of-wire-values").val(optimizer_set_of_wire_values);
        });
    };
    this.optimizerEdit = function() {
        $(document).on("click",".optimizer-edit",function() {
            var self = this;
            var optimizer_id = $(self).attr('id');
            var sets_of_wire = $(self).parents('.card').find('.sets_of_wire').val();
            var current = $(self).parents('.card').find('.current').val();
            var collapse_id = $(self).parents('.card').find('.cus_acro').attr('id');
            // $('.card').find('.cus_acro').addClass('show');
            $(self).parents('.card').find('.cus_acro').removeClass('show');
            $("#add_type").val('optimizer');
    
    
            console.log("collapse_id >> "+collapse_id);
            console.log("current >> "+current);
    
            var url = $('#electrical_formula').val();
    
            $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });
              postData = {
                "optimizer_id":optimizer_id,
                "type":"optimizerEdit",
            }
            if( optimizer_id != 0 ) {
                $.post(url,postData,function(data){
                if(data.result == 1)
                {
                    $('.optimizer-edit').removeClass('text-warning').addClass('text-primary');
                    $(self).removeClass('text-primary').addClass('text-warning');
                    $(".optimizer-current-values").val(0);
                    $(".optimizer-set-of-wire-values").val(0);
                    $("#optimizer_id").val(optimizer_id);
                    $(".string_checkbox_val").prop('checked',false);
                    $(".string_checkbox_val").hide();
                    $(".optimizer-string-button-div").hide();
                    $(".create-string-info").hide();
                    $(".add-optimizer").hide();
                    $(".add-combiner").hide();
                    $(".combiner-edit").hide();
    
                    $(".notSelectedOptimizer:checkbox").show();
                    $(self).parents('.card').find('.optimizer-string-button-div').show();
                    $(self).parents('.card').find('.string_checkbox_val').hide();
                    $(self).parents('.card').find('.add-optimizer-string-to-current').html('Fill Current');
                    $(self).parents('.card').find('.add-optimizer-string-to-current').removeClass('add-optimizer-string-to-current').addClass('fill-current-optimizer');
                    var total_val = 0;
                    $(".string_checkbox_val").each(function(){
                        var string_id = parseInt($(this).attr('data-string-id'));
    
    
                        if(jQuery.inArray(string_id, data.data) !== -1) {
                            total_val += parseFloat($(this).val());
    
    
                            $(this).show();
                            $(this).prop('checked',true);
                            }
                    });
                    total_val = total_val.toFixed(4);
    
                    // $(".optimizer-current-values").val(total_val);
                    $(".optimizer-current-values").val(current);
                    $(".optimizer-set-of-wire-values").val(sets_of_wire);
    
    
                }
                else {
                    $("#optimizer_id").val(0);
                }
                });
            }
            else {
            }
    
    
        });
    };
    this.__construct();
}
var electrical = new Electrical();
