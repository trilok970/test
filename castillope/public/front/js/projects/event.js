var Project = function(){
    this.__construct = function(){

        this.createProject();
        this.editProject();
        this.createProjectModal();
        this.deleteProject();
    }
    this.createProjectModal = function(){
        $(document).on('click','.create-project',function(evt){
            evt.preventDefault();
            url = $("#create-project-url").val();
            // console.log(url);
            $(".input-group > .error").remove();

            
            $("#startprojectmodal").modal('show');
            $("#owner_name").val('');
            $("#address").val('');
            $("#state").val('');
            $("#ult_wind_speed").val('');
            $("#ground_snow_load").val('');
            $("#ahj").val('');
            $("#create-project-form").prop('action',url);
            $("#_method").val("");
            $("#structural_report").html("Create Structural Report");
            $("#electrical_report").html("Create Electrical Report");
            $("#full_report").html("Create Full Report");

            $(".create-project-div").show();
            $(".update-project-div").hide();
            $("#ult_wind_speed").show();
            $("#ground_snow_load").show();


        })
    }
    this.createProject = function(){
        $(document).on('click','.create-project-submit',function(evt){
            evt.preventDefault();
            var postData = $('#create-project-form').serialize();
            var url = $('#create-project-form').attr('action');
            var href = $(this).attr('href');
            var type = $(this).attr('id');
            postData = postData+'&href='+href+"&type="+type;
            // console.log(type);
            $.ajaxSetup({
                headers: {
                    'Access-Control-Allow-Headers': '*',
                    'Accept': 'xml'
                }
            });

            $.post(url,postData,function(out){
                // console.log(out);
                if(out.result === 0)
                {
                    $(".input-group > .error").remove();
                    for(var i in out.errors){
                        // console.log(i);
                        $("#"+i).parents(".input-group").append('<span class="error" >'+out.errors[i]+'</span>');
                    }
                }
                if(out.result === 1 )
                window.location.href = out.url;
            })

        })
    }

    // For project edit
    this.editProject = function(){
        $(document).on('click','.edit-project',function(evt){
            evt.preventDefault();
            url = $(this).attr('href');
            postData = $(this).attr('id');
            $(".input-group > .error").remove();

            $.get(url,postData,function(out){
                $("#owner_name").val(out.data.owner_name);
                $("#address").val(out.data.address);
                $("#state").val(out.data.state);
                $("#ult_wind_speed").val(out.data.ult_wind_speed);
                $("#ground_snow_load").val(out.data.ground_snow_load);
                $("#ahj").val(out.data.ahj);
                $("#create-project-form").prop('action',out.url);
                $("#_method").val("PUT");
                if(out.data.type == 'full' || out.data.type == 'structutal')
                {
                    $("#ult_wind_speed").show();
                    $("#ground_snow_load").show();
                }
                else{
                    $("#ult_wind_speed").hide();
                    $("#ground_snow_load").hide();
                }

                $(".create-project-div").hide();
                $(".update-project-div").show();

                $("#startprojectmodal").modal('show');
            })
        })
    }
    this.deleteProject = function() {
        $(document).on("click",".delete-project",function(evt) {
           evt.preventDefault();
           var url = $(this).attr('href');
           var postData = $(this).attr('id');
           var self = this;
            bootbox.confirm({
                message: "Are you sure you want to delete this project?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                   if(result) {
                    $.get(url,postData,function(out){
                        if(out.result === 1) {
                            $(self).parents('.card').fadeOut(2000);
                        }
                    });
                   }
                }
            });


        });
    }
    this.__construct();
};

var project = new Project();
