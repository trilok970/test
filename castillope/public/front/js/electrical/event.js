var Electrical = function(){
    this.__construct = function() {

        this.noOfInverters();
        this.differentInverterModal();
        this.fetchManufacturerModule();
        this.fetchInverterManufacturerModule();
        this.fetchInverterManufacturerModule1();
        this.createElectrical();
        this.diffInverterModal();
        this.createDiffInverter();
        this.addStringRow();
        this.removeStringRow();
        this.createElectricalString();
        this.removeInverterRow();
        this.mixtureOfInverter();
        this.battery();
        this.size();
        this.inverterTypeId();
        this.sizeInverter();
        this.addStringRowDuplicate();

        var no_of_inverters = parseInt($(".no_of_inverters").val());

        no_of_inverters_hide_show(no_of_inverters)
    }
    /* Open Different Inverters Model  */
    function no_of_inverters_hide_show(no_of_inverters) {
        if(isNaN(no_of_inverters)) {
            no_of_inverters = 0;
        }
        console.log("no_of_inverters >> "+no_of_inverters);
        if(no_of_inverters > 0 && no_of_inverters == 1) {
            $(".no_of_inverter").show();
            $(".no_of_diff_inverter").hide();
        }
        else if(no_of_inverters > 1){
            $(".no_of_inverter").hide();
            $(".no_of_diff_inverter").show();
        }
        else {
            $(".no_of_inverter").hide();
            $(".no_of_diff_inverter").hide();
        }
        battery();
        // mixture_of_inverter();
    }
    this.noOfInverters = function() {
        // $(document).on("click",".differentModal",function(evt) {
            $(".no_of_inverters").on("keypress keyup",function() {
            var self = this;
            var no_of_inverters = parseInt($(self).parents('.electrical-info-feilds').find(".no_of_inverters").val());

            no_of_inverters_hide_show(no_of_inverters);

        });
    }
     /* Open Different Inverters Model  */
     this.differentInverterModal = function() {
        // $(document).on("click",".differentModal",function(evt) {
            $(".differentModal").click(function(evt) {
            evt.preventDefault();
            var self = this;
            var no_of_diff_inverters = $(self).parents('.electrical-info-feilds').find(".no_of_diff_inverters").val();
            diff_inv_model(no_of_diff_inverters);

        });
    }
    /* Fetch Manufacturer Model  */
    this.fetchManufacturerModule = function(){
        $(document).on('change','.manufacturer_id',function(evt){
            var id = $(this).val();
            var self = this;
            $(self).parents(".electrical-info-feilds").find(".module_id").html('');
            var url = $(this).attr('url')+'/'+id;
            $.get(url,id,function(out){
                $(self).parents(".electrical-info-feilds").find(".module_id").html(out.data);
            });
        });
    };
    /* Fetch Manufacturer Model  */
    this.fetchInverterManufacturerModule = function(){
        $(document).on('change','.electrical-info-feilds .inverter_manufacturer_id',function(evt){
            var id = $(this).val();
            var self = this;
            $(self).parents(".electrical-info-feilds").find(".inverter_model_id").html('');
            var url = $(this).attr('url')+'/'+id;
            $.get(url,id,function(out){
                $(self).parents(".electrical-info-feilds").find(".inverter_model_id").html(out.data);
            });
        });
    };
     /* Fetch Manufacturer Model  */
     this.fetchInverterManufacturerModule1 = function(){
        $(document).on('change','.create-diff-inverter-form .inverter_manufacturer_id',function(evt){
            var id = $(this).val();
            var self = this;
            $(self).parents('.diff-inverter-row').find(".inverter_id").html('');
            var url = $(this).attr('url')+'/'+id;
            $.get(url,id,function(out){
                $(self).parents('.diff-inverter-row').find(".inverter_id").html(out.data);
            });
        });
    };
    /* Change Invert type id  */
    this.inverterTypeId = function(){
        $(document).on('change','.inverter_type_id',function(evt){
            var inverter_type_id = $(this).val();
            var self = this;
            var url = $('#electrical_formula').val();
            postData = {
                "inverter_type_id":inverter_type_id,
                "type":"inverter_type_id",
            }
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.post(url,id,function(out){
                if(out.result === 0){

                }
                else {

                }
            });
        });
    };
    /* Submit Electrical Info  */
    this.createElectrical = function() {
        $(".create-electrical-button").click(function(evt) {
            evt.preventDefault();
            var self = this;
            var postData = $(self).parents(".electrical-info-feilds").find(".create-electrical-form").serialize();
            var url = $(self).parents(".electrical-info-feilds").find(".create-electrical-form").attr('action');
            $(".form-group > .error").remove();
            $.post(url,postData,function(out) {
                if(out.result === 0)
                {
                    for(var i in out.errors) {
                        console.log(i);
                        $(self).parents(".electrical-info-feilds").find("."+i).parents(".form-group").append('<span class="error" >'+out.errors[i]+'</span>');
                    }
                }
                else if(out.result === 1) {
                    $(self).parents(".electrical-info-feilds").find(".id").val(out.data.id);
                    // $(self).parents(".electrical-info-feilds").find("._method").val(out.method);
                    // $(self).parents(".electrical-info-feilds").find(".create-electrical-form").attr("action",out.url);
                    if(out.message) {
                        setTimeout(function(){ $(".alert-success").fadeOut(1000); },1000 );
                        fill_module(out.module);
                        location.reload();
                    }
                }
            });
        });
    };
    function fill_module(module) {
        for(var i in module) {
            $('.'+i).html(module[i]);
        }
    }
    function diff_inv_model(no_of_diff_inverters) {
        var length =  parseInt($(".diff-inverter-row-new").find(".diff-inverter-row").length);
        if(isNaN(length))
        length = 0;
        console.log('length >> '+length);
        var no_of_diff = no_of_diff_inverters - length;
        console.log(no_of_diff);
        if(no_of_diff_inverters > 0 ) {
            // $(".diff-inverter-row-new").html('');
            for(var i = 1; i <= no_of_diff; i++ ) {
                var section = $("#diff-inverter-row").clone();
                $(".diff-inverter-row-new").append(section);
                $(".diff-inverter-row-new").find(".diff-inverter-row").last().attr("id",'diff-inverter-'+i);
                $(".diff-inverter-row-new").find(".diff-inverter-row").last().removeClass("d-none");
            }
            // $("#diff_inv_model").modal('show');
            $('#diff_inv_model').modal({backdrop: 'static', keyboard: false});
        }
    }
    this.diffInverterModal = function() {
        $(".no_of_diff_inverters").on("change",function() {
            var self = this;
            var no_of_diff_inverters = $(self).parents('.electrical-info-feilds').find(".no_of_diff_inverters").val();
            diff_inv_model(no_of_diff_inverters);
        });
    };
      /* Submit Electrical Info  */
      this.createDiffInverter = function() {
        $(".create-diff-inverter-button").click(function(evt) {
            evt.preventDefault();
            var self = this;
            var postData = $(self).parents(".model_string").find(".create-diff-inverter-form").serialize();
            var url = $(self).parents(".model_string").find(".create-diff-inverter-form").attr('action');
            console.log(url);
            $(".form-group > .error").remove();
            $.post(url,postData,function(out) {
                if(out.result === 0)
                {
                    for(var i in out.errors) {
                        console.log(i);
                        let index = i.replace(/^\D+/g,'');
                        let feild = i.replace(/\.[0-9]/g,'').trim();
                        // console.log(feild);
                        console.log(index);
                        let error = out.errors[i].toString();
                        error = error.replace(/\.(\d)+/g,'');
                        error = error.replace(/_/g,' ');
                        $(".diff-inverter-row-new").find('.diff-inverter-row').eq(index).find('.'+feild).parents(".form-group").append('<span class="error">'+error+'</span>');

                        // let index = i.replace(/^\D+/g,'');
                        // let feild = i.replace(/\.[0-9]/g,'').trim();
                        // console.log("feild >> "+feild);
                        // let error = out.errors[i].toString();
                        //     error = error.replace(/\.(\d)+/g,'');
                        //     error = error.replace(/_/g,' ');
                        // $(".model_string").find(".create-diff-inverter-form").eq(index).find('.'+feild).parents(".form-group").append('<span class="error">'+error+'</span>');
                    }
                }
                else if(out.result === 1) {
                    var section = $(".diff-inverter-row-new").find('.diff-inverter-row');

                    $(".diff-inverter-row-new").append(section);
                    $(".model-data-row").html('');
                    $(".model-data-row").append(section);
                    $("#diff_inv_model").modal('hide');

                }
            });
        });
    };

    this.addStringRow = function() {
        $(".add-string").click(function() {
            console.log("check hhh");
            var section = $(".string_row").last().clone();
            var length = $(".new_row").find(".string_row").length + 1;
            console.log("length >> "+length);
            $("#new_row").append(section);
            $(".new_row").find(".string_row").last().attr("id",'string-'+length);
            $(".new_row").find(".string_id").last().val(0);
            $(".new_row").find(".size").last().val(0);
            $(".new_row").find(".string_row").last().removeClass("d-none");
            const randomString = getRandomString();
            $(".new_row").find(".add-string-duplicate").last().attr("randomString",randomString);

            updateNoOfStrings();
            stringNoArrange();

            $(".numeric_feild_discount").keypress(function(event){
                return isNumber(event, this);
            });
            $('.select2').select2({
                // closeOnSelect: false
                tags: true
              });
        });
    };
    this.removeStringRow = function() {
        $(document).on("click",".remove-string",function() {
            var self = this;
            var randomStringPre = $(self).parents('tr').find(".add-string-duplicate").attr('randomString');
            var string_id_duplicate = $(self).parents('tr').find(".add-string-duplicate").attr('string_id_duplicate');
            console.log("randomStringPre > "+randomStringPre);
            console.log("string_id_duplicate > "+string_id_duplicate);
            // remove all previous added duplicate string
            if(randomStringPre && string_id_duplicate == 0) {
                $(self).parents(".new_row").find("."+randomStringPre).closest('tr').remove();
            }
            $(self).parents('tr').remove();

            updateNoOfStrings();
            
            stringNoArrange();
        });
    };
    function updateNoOfStrings()
    {
        var roof_length = parseInt($(".new_row").find(".string_row").length);
        $(".no_of_strings").val((roof_length));
    }
    
    function stringNoArrange()
    {
        var roof_length = parseInt($("#room_div").find('.card').length);
        // for(let i = 1; i <= roof_length; i++) {
            // console.log("roof_no >> "+i);
            let j = 1;
            $("#new_row").find('.string_no').each(function(){
                $(this).html("#"+j);
                var randomStringPre = $(this).parents('tr').find(".add-string-duplicate").attr('randomString');
            console.log("randomStringPre > "+randomStringPre);
            // remove all previous added duplicate string
            if(randomStringPre) {
                duplicateStringNoArrange(randomStringPre,parseFloat(j));
            }
                j++;
            });
        // }

    }
    function duplicateStringNoArrange(randomStringPre,j)
    {
        j = parseFloat(j);
        var k = 1;
            $("#new_row").find('.'+randomStringPre).each(function(){
                $(this).html("#"+(j+0.1).toFixed(1));
            console.log("j > "+j);
            console.log("k > "+k);
                k++;
                j = parseFloat(j)+0.1;
            });
        
    }
    // For quantity validation
    function isNumber(evt, element)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (
               (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
               (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
               (charCode < 48 || charCode > 57))
               return false;

               return true;
     }
      /* Submit Electrical Info  */
      this.createElectricalString = function() {
        $(".create-string-info").click(function(evt) {
            evt.preventDefault();
            var self = this;
            var postData = $(self).parents(".structural-details-col").find(".create-electrical-string-form").serialize();
            var url = $(self).parents(".structural-details-col").find(".create-electrical-string-form").attr('action');
            console.log(url);
            $(".form-group > .error").remove();
            $.post(url,postData,function(out) {
                if(out.result === 0)
                {
                    for(var i in out.errors) {
                        console.log(i);
                        let index = i.replace(/^\D+/g,'');
                        let feild = i.replace(/\.[0-9]/g,'').trim();
                        // console.log(feild);
                        console.log(index);
                        let error = out.errors[i].toString();
                        error = error.replace(/\.(\d)+/g,'');
                        error = error.replace(/_/g,' ');
                        $(".new_row").find('.string_row').eq(index).find('.'+feild).parents(".form-group").append('<span class="error">'+error+'</span>');

                    }
                }
                else if(out.result === 1) {
                    var electrical_url = $("#electrical_url").val();
                    location.href = electrical_url;
                }
            });
        });
    };
    this.removeInverterRow = function() {
        $(document).on("click",".remove-inverter",function() {
            $(this).parents('.diff-inverter-row').remove();
            updateNoOfInverters();
        });
    };
    function updateNoOfInverters()
    {
        var inverter_length = parseInt($(".diff-inverter-row-new").find(".diff-inverter-row").length);
        if(isNaN(inverter_length))
        inverter_length = 0;
        $(".no_of_diff_inverters").val(inverter_length);
    }
    function mixture_of_inverter() {
        var mixture_of_inverters = $(".mixture_of_inverters:checked").val();
        if(mixture_of_inverters && mixture_of_inverters == 'No') {
            $(".no_of_inverters").val('');
            no_of_inverters = $(".no_of_inverters").val();
            $(".no_of_inverters_div").hide();
            no_of_inverters_hide_show(no_of_inverters);
        }
        else if(mixture_of_inverters && mixture_of_inverters == 'Yes') {
            $(".no_of_inverters_div").show();
        }
    }
    this.mixtureOfInverter = function() {
        $(".mixture_of_inverters").on("click",function() {
            self = this;
            var mixture_of_inverters = $(self).parents('.electrical-info-feilds').find(".mixture_of_inverters:checked").val();
            // console.log("mixture_of_inverters >> "+mixture_of_inverters);
            if(mixture_of_inverters && mixture_of_inverters == 'No') {
                $(self).parents('.electrical-info-feilds').find(".no_of_inverters").val('');
                no_of_inverters = $(self).parents('.electrical-info-feilds').find(".no_of_inverters").val();
                $(self).parents('.electrical-info-feilds').find(".no_of_inverters_div").hide();
                no_of_inverters_hide_show(no_of_inverters);
            }
            else if(mixture_of_inverters && mixture_of_inverters == 'Yes') {
                $(self).parents('.electrical-info-feilds').find(".no_of_inverters_div").show();
            }
        });
    }
    this.battery = function() {
        $(".battery").on("click",function() {
            battery();
        });
    }
    function battery() {
        var battery = $('.electrical-info-feilds').find(".battery:checked").val();
        // console.log("battery >> "+battery);
        if(battery && battery == 'No') {
            $('.electrical-info-feilds').find(".number_div").hide();
        }
        else if(battery && battery == 'Yes') {
            $('.electrical-info-feilds').find(".number_div").show();
        }
    }
    this.size = function() {
        $(document).on("click keyup",".size",function() {
            self = this;
            var size = parseInt($(self).parents('.string_row').find(".size").val());
            var inverter_id = parseInt($(self).parents('.string_row').find(".inverter_id").val());
            if(isNaN(size))
            size = 0;
            if(isNaN(inverter_id))
            inverter_id = 0;
            console.log("size >> "+size);
            console.log("inverter_id >> "+inverter_id);

            postData = {
                            "size":size,
                            "inverter_id":inverter_id,
                            "type":"size",
                        }
            var url = $('#electrical_formula').val();
            var self = this;
            $.ajaxSetup({
                headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            if(inverter_id  != 0 && size != 0) {
            $.post(url,postData,function(data){
            if(data.result == 1)
            {
                console.log("size >> "+data.data);
                // $(self).parents(".string_row").find(".size").val(data.data);
                $(self).parents(".string_row").find(".size-warning-message").html(data.size_warning_message);
                $(self).parents(".string_row").find(".size-warning-message").val(data.size_warning_message);
            }
            });
            }
            else {
                // $(self).parents(".string_row").find(".size").val('');
                $(self).parents(".string_row").find(".size-warning-message").html('');
                $(self).parents(".string_row").find(".size-warning-message").val('');

            }

        });
    }
    this.sizeInverter = function() {
        $(document).on("change",".sizeInverter",function(){
            var self = this;
            var inverter_id = $(self).parents('.string_row').find(".inverter_id").val();
            var randomStringPre = $(self).parents('.string_row').find(".add-string-duplicate").attr('randomString');
            console.log("randomStringPre > "+randomStringPre);
            // remove all previous added duplicate string
            $(self).parents(".new_row").find("."+randomStringPre).closest('tr').remove();
            postData = {
                "inverter_id":inverter_id,
                "type":"sizeInverter",
            }
            var url = $('#electrical_formula').val();
            var self = this;
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            if(inverter_id  != 0 ) {
            $.post(url,postData,function(data){
            if(data.result == 1)
            {
                console.log("size >> "+data.data);
                if(data.data == 'string inverter') {
                    $(self).parents(".string_row").find(".add-string-duplicate").show();
                    $(self).parents(".string_row").find(".add-string-duplicate").attr('maxDuplicateString',data.maxDuplicateString);
                    const randomString = getRandomString();
                    $(self).parents(".string_row").find(".add-string-duplicate").attr('randomString',randomString);
                }
                else {
                    $(self).parents(".string_row").find(".add-string-duplicate").hide();
                    $(self).parents(".string_row").find(".add-string-duplicate").attr('maxDuplicateString',0);

                }
            }
            }); 
            }
            else {
                $(self).parents(".string_row").find(".add-string-duplicate").hide();
                $(self).parents(".string_row").find(".add-string-duplicate").attr('maxDuplicateString',0);

            }

            });
        
    }
    function getRandomString(){
       return Math.random().toString(36).substr(2, 10);
    }
    this.addStringRowDuplicate = function() {
        $(document).on("click",".add-string-duplicate",function(e) {
            e.preventDefault();
            const self = this;
            const currentStringNo = $(self).parents('tr').find(".string_no").text().replace("#","");
            const maxDuplicateString = $(self).parents('tr').find(".add-string-duplicate").attr('maxDuplicateString');
            const randomString = $(self).parents('tr').find(".add-string-duplicate").attr('randomString');
            console.log("currentStringNo > "+currentStringNo);
            console.log("maxDuplicateString > "+maxDuplicateString);
            console.log("duplicate length > "+$(self).parents('.new_row').find("."+randomString).length);
            console.log("getRandomString > "+randomString);
            $(".message").hide();
            if($(self).parents('.new_row').find("."+randomString).length < maxDuplicateString) {
                
                var section = $(self).closest('tr').last().clone();
                var length = $(".new_row").find("tr").length + 1;
                var lengthDupString = $(self).parents('.new_row').find("."+randomString).length + 1;
                console.log("length >> "+length);
                console.log("lengthDupString >> "+lengthDupString);
                $("#new_row").append(section);
                $(".new_row").find(".string_no").last().addClass("string_no_duplicate").removeClass("string_no");
                $(".new_row").find(".string_no_duplicate").last().addClass(randomString).removeClass("string_no");
                const string_no_duplicate = parseInt(currentStringNo)+parseFloat(lengthDupString/10);
                $(".new_row").find(".string_no_duplicate").last().html("#"+string_no_duplicate);
                $(".new_row").find(".string_row").last().addClass("string_row_duplicate").removeClass("string_row");
                $(".new_row").find(".string_row_duplicate").last().attr("id",'string-'+length);
                $(".new_row").find(".string_id").last().val(0);
                $(".new_row").find(".string_id_duplicate").last().val(1);
                $(".new_row").find(".string_row_duplicate").last().removeClass("d-none");
                $(".new_row").find(".add-string-duplicate").last().hide();
                $(".new_row").find(".add-string-duplicate").last().attr('string_id_duplicate',1);
                updateNoOfStrings();
                // stringNoArrange();

                $(".numeric_feild_discount").keypress(function(event){
                    return isNumber(event, this);
                });
                $('.select2').select2({
                    // closeOnSelect: false
                    tags: true
                });
            }
            else {
                $(".message").html("You can't add more duplicate string");
                $(".message").show();
            }
            
        });
    };
    this.__construct();
}
var electrical = new Electrical();
// Effective wind area calculate with module length and module width but we dont have module length and wuidth please how can i calculate this value
