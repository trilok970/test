var Project = function(){
    this.__construct = function(){

        this.createProject();
        this.calculateSRSL();
        this.calculateNWS();
        this.fectchManufacturerModule();
        this.fectchPvModule();
        this.calculateModuleArea();
        this.calculateRoofSlopeDegree();
        this.calculateKe();
        this.calculateKz();
        this.calculateWidthOfPressureCoefficient();
        this.calculateZone();
        this.fectchAttachment();
        this.fectchAttachmentMethod();
        this.fectchInnerRoofSlope();
        this.addRoof();
        this.removeRoof();
        this.siteModal();
        this.onLoad();

        let asce_version = $("#asce_version").val();
        zone_show(asce_version);

    }
    this.onLoad = function() {
        $(document).ready(function(){
            $(".roof_type,.velocity_pressure,.sloped_roof_snow_load,.dead_load,.asce_version,.kz,.topographic_factor,.wind_directionality_factor,.ke,.normal_wind_speed,.parapet_height,.exposure_category").change(function(){
                // calculateRightRoofValuesForAll();
            });


        });
    };
    this.fectchManufacturerModule = function(){
        $(document).on('change','.manufacturer_id',function(evt){
            var id = $(this).val();
            var self = this;
            // console.log("id >> "+id);
            // alert('check');
            $(self).parents(".roof-container").find(".pv_module").html('');

            var url = $(this).attr('url')+'/'+id;
            $.get(url,id,function(out){
                $(self).parents(".roof-container").find(".module_id").html(out.data);
                // reset_roof_values(self);
                $(self).parents(".roof-container").find(".module_length").val('');
                $(self).parents(".roof-container").find(".module_width").val('');
            });

        })
    };

    this.fectchPvModule = function(){
        $(document).on('change','.module_id',function(evt){
            self = this;

            var module_id = $(self).val();
            var manufacturer_id = $('option:selected',self).attr("manufacturer_id");
            var url = $(self).attr('url')+'/'+module_id+'/'+manufacturer_id;
            console.log("url >> "+url);
            if(module_id)
            {
                var module_width = $('option:selected',self).attr("module_width");
                var module_lenght = $('option:selected',self).attr("module_lenght");
                $(self).parents(".roof-container").find(".module_length").val(module_lenght);
                $(self).parents(".roof-container").find(".module_width").val(module_width);
                $(self).parents(".roof-container").find(".module_area").val(((module_lenght * module_width) / 144).toFixed(4));
                $(self).parents(".roof-container").find(".effective_wind_area").val(((module_lenght * module_width) / 144).toFixed(4));

                // $.get(url,module_id,function(out){
                //     $(self).parents(".roof-container").find(".attachment_manufacturer_id").html(out.data);

                // });

            }
            else
            {
                $(self).parents(".roof-container").find(".module_length").val('');
                $(self).parents(".roof-container").find(".module_width").val('');
                $(self).parents(".roof-container").find(".module_area").val('');
                $(self).parents(".roof-container").find(".attachment_manufacturer_id").html('<option value="">Select Attachment Manufacturer</option>');
                $(self).parents(".roof-container").find(".compression_strength").val('');
                $(self).parents(".roof-container").find(".pullout_strength").val('');
                $(self).parents(".roof-container").find(".effective_wind_area").val('');
            }

            console.log("module_id >> "+module_id);
            console.log("module_width >> "+module_width);
            console.log("module_lenght >> "+module_lenght);


        })
    };
    this.fectchAttachment = function(){
        $(document).on('change','.attachment_manufacturer_id',function(evt){
            var id = $(this).val();
            var url = $(this).attr('url')+'/'+id;
            var self = this;

            // var compression_strength = $('option:selected',this).attr("allowable_compression_strength");
            // var pullout_strength = $('option:selected',this).attr("allowable_pollout_strength");
            if(id)
            {
                $.get(url,id,function(out){
                    $(self).parents(".roof-container").find(".attachment_id").html(out.data);

                });
                console.log("attachment_manufacturer_id id >> "+id);
                // $(this).parents(".roof-container").find(".compression_strength").val(compression_strength);
                // $(this).parents(".roof-container").find(".pullout_strength").val(pullout_strength);
                $(self).parents(".roof-container").find(".compression_strength").val('');
                $(self).parents(".roof-container").find(".pullout_strength").val('');
                $(self).parents(".roof-container").find(".attachment_id").html('');
            }
            else
            {
                console.log("attachment_manufacturer_id else >> "+id);

                $(self).parents(".roof-container").find(".compression_strength").val('');
                $(self).parents(".roof-container").find(".pullout_strength").val('');
                $(self).parents(".roof-container").find(".attachment_id").html('');
            }
        })
    };
    this.fectchAttachmentMethod = function(){
        $(document).on('change','.attachment_id',function(evt){
            var id = $(this).val();
            var self = this;

            // var strength = $('option:selected',this).attr("allowable_uplift");
            var compression_strength = $('option:selected',this).attr("allowable_compression_strength");
            var pullout_strength = $('option:selected',this).attr("allowable_pollout_strength");
            if(id)
            {

                $(self).parents(".roof-container").find(".compression_strength").val(compression_strength);
                $(self).parents(".roof-container").find(".pullout_strength").val(pullout_strength);
                // $(this).parents(".roof-container").find(".strength").val(strength);
            }
            else
            {
                $(self).parents(".roof-container").find(".compression_strength").val('');
                $(self).parents(".roof-container").find(".pullout_strength").val('');
                // $(self).parents(".roof-container").find(".strength").val('');
            }
        })
    };



    function calculateZoneValues(self) {
        var roof_slope_degree = parseFloat($(self).parents(".roof-container").find(".roof_slope_degree").val());
        var roof_type = $("#roof_type").val();
        var velocity_pressure = $("#velocity_pressure").val();
        var srsl = $("#sloped_roof_snow_load").val();
        var dead_load = $("#dead_load").val();
        var asce_version = $("#asce_version").val();
        var kz = $("#kz").val();
        var topographic_factor = $("#topographic_factor").val();
        var wind_directionality_factor = $("#wind_directionality_factor").val();
        var ke = $("#ke").val();
        var normal_wind_speed = $("#normal_wind_speed").val();
        var parapet_height = $("#parapet_height").val();
        var exposure_category = $("#exposure_category").val();
        var module_orientation = $(self).parents(".roof-container").find(".module_orientation").val();
        var module_length = parseFloat($(self).parents(".roof-container").find(".module_length").val());
        var module_width = parseFloat($(self).parents(".roof-container").find(".module_width").val());
        var module_area = parseFloat($(self).parents(".roof-container").find(".module_area").val());
        var effective_wind_area = module_area;
        var no_of_rails = parseFloat($(self).parents(".roof-container").find(".no_of_rails").val());
        var no_of_rails_exposed = parseFloat($(self).parents(".roof-container").find(".no_of_rails_exposed").val());
        var no_of_rails_non_exposed = parseFloat($(self).parents(".roof-container").find(".no_of_rails_non_exposed").val());
        var limit_max_span_to = parseFloat($(self).parents(".roof-container").find(".limit_max_span_to").val());
        var rafter_seam_spacing = parseFloat($(self).parents(".roof-container").find(".rafter_seam_spacing").val());
        var compression_strength = parseFloat($(self).parents(".roof-container").find(".compression_strength").val());
        var roof_height = parseFloat($(self).parents(".roof-container").find(".roof_height").val());

        if(isNaN(module_length))
        module_length = 0;
        if(isNaN(module_width))
            module_width = 0;
        if(isNaN(module_area))
            module_area = 0;
        if(isNaN(no_of_rails))
            no_of_rails = 0;
        if(isNaN(no_of_rails_exposed))
            no_of_rails_exposed = 0;
        if(isNaN(no_of_rails_non_exposed))
            no_of_rails_non_exposed = 0;
        if(isNaN(limit_max_span_to))
            limit_max_span_to = 0;
        if(isNaN(no_of_rails_non_exposed))
            no_of_rails_non_exposed = 0;
        if(isNaN(rafter_seam_spacing))
            rafter_seam_spacing = 0;
        if(isNaN(compression_strength))
            compression_strength = 0;
        if(isNaN(roof_slope_degree))
            roof_slope_degree = 0;
        if(isNaN(roof_height))
            roof_height = 0;
        if(isNaN(effective_wind_area))
            effective_wind_area = 0;
        // console.log("roof_slope_degree >>"+roof_slope_degree);
        // console.log("roof_type >>"+roof_type);
        // console.log("effective_wind_area >>"+effective_wind_area);
        // console.log("velocity_pressure >>"+velocity_pressure);
        // console.log("dead_load >>"+dead_load);
        // console.log("asce_version >>"+asce_version);
        // console.log("kz >>"+kz);
        // console.log("topographic_factor >>"+topographic_factor);
        // console.log("wind_directionality_factor >>"+wind_directionality_factor);
        // console.log("ke >>"+ke);
        // console.log("normal_wind_speed >>"+normal_wind_speed);
        // console.log("parapet_height >>"+parapet_height);
        // console.log("module_orientation >>"+module_orientation);
        // console.log("module_length >>"+module_length);
        // console.log("module_width >>"+module_width);
        // console.log("module_area >>"+module_area);
        // console.log("no_of_rails >>"+no_of_rails);
        // console.log("no_of_rails_exposed >>"+no_of_rails_exposed);
        // console.log("no_of_rails_non_exposed >>"+no_of_rails_non_exposed);
        // console.log("limit_max_span_to >>"+limit_max_span_to);
        // console.log("rafter_seam_spacing >>"+rafter_seam_spacing);
        // console.log("compression_strength >>"+compression_strength);
        postData = {
            "roof_slope_degree":roof_slope_degree,
            "roof_type":roof_type,
            "effective_wind_area":effective_wind_area,
            "module_area":module_area,
            "velocity_pressure":velocity_pressure,
            "kz":kz,
            "topographic_factor":topographic_factor,
            "wind_directionality_factor":wind_directionality_factor,
            "ke":ke,
            "normal_wind_speed":normal_wind_speed,
            "parapet_height":parapet_height,
            "srsl":srsl,
            "dead_load":dead_load,
            "module_orientation":module_orientation,
            "module_length":module_length,
            "module_width":module_width,
            "no_of_rails":no_of_rails,
            "no_of_rails_exposed":no_of_rails_exposed,
            "no_of_rails_non_exposed":no_of_rails_non_exposed,
            "limit_max_span_to":limit_max_span_to,
            "rafter_seam_spacing":rafter_seam_spacing,
            "compression_strength":compression_strength,
            "asce_version":asce_version,
            "roof_height":roof_height,
            "exposure_category":exposure_category,
            "type":"inner_roof_slope",
        }
        var url = $('#site_formula').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.post(url,postData,function(data){
        if(data.result == 1)
        {


            if(asce_version == 'asce-7-16') {
                zone_show(asce_version);
                $.each(data.data.ext_neg_val, function(i) {
                    $(self).parents(".roof-container").find(".ext_neg_pre_coef"+i).html(data.data.ext_neg_val[i]);
                    $(self).parents(".roof-container").find(".ext_neg_pre_coef"+i).val(data.data.ext_neg_val[i]);
                });
                $.each(data.data.ext_pos_val, function(i) {
                    $(self).parents(".roof-container").find(".ext_pos_pre_coef"+i).html(data.data.ext_pos_val[i]);
                    $(self).parents(".roof-container").find(".ext_pos_pre_coef"+i).val(data.data.ext_pos_val[i]);
                });
                $.each(data.data.design_neg_pressure, function(i) {
                    $(self).parents(".roof-container").find(".negative_pressure"+i).html(data.data.design_neg_pressure[i]);
                    $(self).parents(".roof-container").find(".negative_pressure"+i).val(data.data.design_neg_pressure[i]);
                });
                $.each(data.data.design_pos_pressure, function(i) {
                    $(self).parents(".roof-container").find(".positive_pressure"+i).html(data.data.design_pos_pressure[i]);
                    $(self).parents(".roof-container").find(".positive_pressure"+i).val(data.data.design_pos_pressure[i]);
                });
                $.each(data.data.adjusted_pos_pressure, function(i) {
                    $(self).parents(".roof-container").find(".adjusted_pos_pre"+i).html(data.data.adjusted_pos_pressure[i]);
                    $(self).parents(".roof-container").find(".adjusted_pos_pre"+i).val(data.data.adjusted_pos_pressure[i]);
                });
                $.each(data.data.adjusted_neg_pressure, function(i) {
                    $(self).parents(".roof-container").find(".adjusted_exp_neg_pre"+i).html(data.data.adjusted_neg_pressure[i]);
                    $(self).parents(".roof-container").find(".adjusted_exp_neg_pre"+i).val(data.data.adjusted_neg_pressure[i]);
                });
                $.each(data.data.adjusted_non_neg_pressure, function(i) {
                    $(self).parents(".roof-container").find(".adjusted_non_exp_neg_pre"+i).html(data.data.adjusted_non_neg_pressure[i]);
                    $(self).parents(".roof-container").find(".adjusted_non_exp_neg_pre"+i).val(data.data.adjusted_non_neg_pressure[i]);
                });
                $.each(data.data.adjusted_pos_loading, function(i) {
                    $(self).parents(".roof-container").find(".positive_loading"+i).html(data.data.adjusted_pos_loading[i]);
                    $(self).parents(".roof-container").find(".positive_loading"+i).val(data.data.adjusted_pos_loading[i]);
                });
                $.each(data.data.adjusted_neg_loading, function(i) {
                    $(self).parents(".roof-container").find(".negative_exposed_loading"+i).html(data.data.adjusted_neg_loading[i]);
                    $(self).parents(".roof-container").find(".negative_exposed_loading"+i).val(data.data.adjusted_neg_loading[i]);
                });
                $.each(data.data.adjusted_non_neg_loading, function(i) {
                    $(self).parents(".roof-container").find(".negative_non_exposed_loading"+i).html(data.data.adjusted_non_neg_loading[i]);
                    $(self).parents(".roof-container").find(".negative_non_exposed_loading"+i).val(data.data.adjusted_non_neg_loading[i]);
                });
                $.each(data.data.spanExposed, function(i) {
                    $(self).parents(".roof-container").find(".attachment_exposed_negative_spacing"+i).html(data.data.spanExposed[i]);
                    $(self).parents(".roof-container").find(".attachment_exposed_negative_spacing"+i).val(data.data.spanExposed[i]);
                });
                $.each(data.data.spanNonExposed, function(i) {
                    $(self).parents(".roof-container").find(".attachment_non_exposed_negative_spacing"+i).html(data.data.spanNonExposed[i]);
                    $(self).parents(".roof-container").find(".attachment_non_exposed_negative_spacing"+i).val(data.data.spanNonExposed[i]);
                });
                $.each(data.data.kz_values, function(i) {
                    $(self).parents(".roof-container").find(".zone_kz"+i).html(data.data.kz_values[i]);
                    $(self).parents(".roof-container").find(".zone_kz"+i).val(data.data.kz_values[i]);
                });
            }
            else if(asce_version == 'asce-7-10') {
                zone_show(asce_version);
                $.each(data.data.ext_neg_val_7_10, function(i) {
                    $(self).parents(".roof-container").find(".ext_neg_pre_coef"+i+"_7_10").html(data.data.ext_neg_val_7_10[i]);
                    $(self).parents(".roof-container").find(".ext_neg_pre_coef"+i+"_7_10").val(data.data.ext_neg_val_7_10[i]);
                });
                $.each(data.data.ext_pos_val_7_10, function(i) {
                    $(self).parents(".roof-container").find(".ext_pos_pre_coef"+i+"_7_10").html(data.data.ext_pos_val_7_10[i]);
                    $(self).parents(".roof-container").find(".ext_pos_pre_coef"+i+"_7_10").val(data.data.ext_pos_val_7_10[i]);
                });
                $.each(data.data.design_neg_pressure, function(i) {
                    $(self).parents(".roof-container").find(".negative_pressure"+i+"_7_10").html(data.data.design_neg_pressure[i]);
                    $(self).parents(".roof-container").find(".negative_pressure"+i+"_7_10").val(data.data.design_neg_pressure[i]);
                });
                $.each(data.data.design_pos_pressure, function(i) {
                    $(self).parents(".roof-container").find(".positive_pressure"+i+"_7_10").html(data.data.design_pos_pressure[i]);
                    $(self).parents(".roof-container").find(".positive_pressure"+i+"_7_10").val(data.data.design_pos_pressure[i]);
                });
                $.each(data.data.adjusted_pos_loading_7_10, function(i) {
                    $(self).parents(".roof-container").find(".positive_loading"+i+"_7_10").html(data.data.adjusted_pos_loading_7_10[i]);
                    $(self).parents(".roof-container").find(".positive_loading"+i+"_7_10").val(data.data.adjusted_pos_loading_7_10[i]);
                });
                $.each(data.data.adjusted_neg_loading_7_10, function(i) {
                    $(self).parents(".roof-container").find(".negative_loading"+i+"_7_10").html(data.data.adjusted_neg_loading_7_10[i]);
                    $(self).parents(".roof-container").find(".negative_loading"+i+"_7_10").val(data.data.adjusted_neg_loading_7_10[i]);
                });
                $.each(data.data.spanExposed_7_10, function(i) {
                    $(self).parents(".roof-container").find(".attachment_negative_spacing"+i+"_7_10").html(data.data.spanExposed_7_10[i]);
                    $(self).parents(".roof-container").find(".attachment_negative_spacing"+i+"_7_10").val(data.data.spanExposed_7_10[i]);
                });
                $.each(data.data.spanNonExposed_7_10, function(i) {
                    $(self).parents(".roof-container").find(".attachment_positive_spacing"+i+"_7_10").html(data.data.spanNonExposed_7_10[i]);
                    $(self).parents(".roof-container").find(".attachment_positive_spacing"+i+"_7_10").val(data.data.spanNonExposed_7_10[i]);
                });

            }


        }
        });

    }
    this.fectchInnerRoofSlope = function(){
        $(document).on('change keyup keypress','.roof_slope_cal',function(evt){
            var id = $(this).val();
            var self = this;

            calculateZoneValues(self);

        });
    };
     function calculateRightRoofValuesForAll(){

        $("#room_div").find('.roof_slope_degree').each(function(){
            var self = this;
            calculateZoneValues(self);
        });
    }
    function zone_show(asce_version)
    {
        // console.log("asce_version >> "+asce_version);
        // console.log($(".roof_type").val());
        if(asce_version === 'asce-7-16') {
            $(".asce-version-table-7-16").show();
            $(".asce-version-table-7-10").hide();

        }
        else if(asce_version === 'asce-7-10') {
            $(".asce-version-table-7-16").hide();
            $(".asce-version-table-7-10").show();
        }
        if($(".roof_type").val() == 'HIP' || $(".roof_type").val() == 'GABLE') {
            $(".asce-7-16-zone").hide();
        }
        else {
            $(".asce-7-16-zone").show();
        }
    }
    this.calculateSRSL = function(){
        $(".cal").on("change keyup keypress",function(){
            // var ground_snow_load = parseFloat($("#ground_snow_load").val());
            var ground_snow_load = parseFloat($(this).parents(".site-info-feilds").find('.ground_snow_load').val());
            console.log("ground_snow_load >>> "+ground_snow_load);
            if(isNaN(ground_snow_load))
                ground_snow_load = 0;
            var ce = parseFloat($(this).parents(".site-info-feilds").find(".exposure_factor").val());
            if(isNaN(ce))
                ce = 0;
            var ct = parseFloat($(this).parents(".site-info-feilds").find(".temperature_factor").val());
            if(isNaN(ct))
                ct = 0;
            var is = parseFloat($(this).parents(".site-info-feilds").find(".importance_factor").val());
            if(isNaN(is))
                is = 0;
            var cs = parseFloat($(this).parents(".site-info-feilds").find(".slope_factor").val());
            if(isNaN(cs))
                cs = 0;
                postData = {
                    "ground_snow_load":ground_snow_load,
                    "ce":ce,
                    "ct":ct,
                    "is":is,
                    "cs":cs,
                    "type":"srsl",
                }
                var url = $('#site_formula').val();
                var self = this;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.post(url,postData,function(data){
                if(data.result == 1)
                {
                    console.log("srsl >> "+data.srsl);
                    $(self).parents(".site-info-feilds").find(".sloped_roof_snow_load").val(data.data);
                    calculateRightRoofValuesForAll();
                }
            })
            // var srsl =  ground_snow_load * ce * ct * is * cs;
            //     srsl = parseFloat(srsl).toFixed(4);

        });
    };



    this.calculateNWS = function(){
        $(".uws").on("change keyup keypress",function(){
            var ultimate_wind_speed = parseFloat($(this).parents(".site-info-feilds").find(".ultimate_wind_speed").val());
            if(isNaN(ultimate_wind_speed))
                ultimate_wind_speed = 0;
                postData = {
                    "ultimate_wind_speed":ultimate_wind_speed,
                    "type":"nws",
                }
                var url = $('#site_formula').val();
                var self = this;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.post(url,postData,function(data){
                if(data.result == 1)
                {
                    console.log("uws >> "+data.uws);
                    $(self).parents(".site-info-feilds").find(".normal_wind_speed").val(data.data);

                }
                });
        });
    };
    this.calculateModuleArea = function(){
        $(".site_module").on("change keyup keypress",function(){
            var module_length = parseFloat($(this).parents(".site-info-feilds").find(".module_length").val());
            if(isNaN(module_length))
                module_length = 0;
            var module_width = parseFloat($(this).parents(".site-info-feilds").find(".module_width").val());
            if(isNaN(module_width))
                module_width = 0;
                var self = this;
                postData = {
                    "module_length":module_length,
                    "module_width":module_width,
                    "type":"module_area",
                }
                var url = $('#site_formula').val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.post(url,postData,function(data){
                if(data.result == 1)
                {
                    console.log("module_area >> "+data.data);
                    $(self).parents(".site-info-feilds").find(".module_area").val(data.data);
                    $(self).parents(".site-info-feilds").find(".effective_wind_area").val(data.data);

                }
                });


        });
    };
    this.calculateRoofSlopeDegree = function(){
        $(document).on("change keyup keypress",".roof_slope",function(){
        // $(".roof_slope").on("change keyup keypress",function(){
            var roof_slope = parseFloat($(this).parents(".roof-container").find(".roof_slope").val());

            if(isNaN(roof_slope))
            roof_slope = 0;
            console.log("roof_slope >> "+roof_slope);
            var self = this;
            postData = {
                "roof_slope":roof_slope,
                "type":"roof_slope_degree",
            }
            var url = $('#site_formula').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.post(url,postData,function(data){
            if(data.result == 1)
            {
                console.log("roof_slope_degree >> "+data.data);
                $(self).parents(".roof-container").find(".roof_slope_degree").val(data.data);
            }
            });


        });
    };
    this.calculateKe = function(){
        $(".ground_elevation ").on("change keyup keypress",function(){
            var ground_elevation = parseFloat($(this).parents(".site-info-feilds").find(".ground_elevation").val());
            if(isNaN(ground_elevation))
            ground_elevation = 0;

            postData = {
                "ground_elevation":ground_elevation,
                "type":"ke",
            }
            var url = $('#site_formula').val();
            var self = this;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.post(url,postData,function(data){
            if(data.result == 1)
            {
                console.log("ke >> "+data.data);
                $(self).parents(".site-info-feilds").find(".ke").val(data.data);

            }
            });




        });
    };
    this.calculateKz = function(){
        // =IF(D6<15,2.01*POWER(15/VLOOKUP(H6,Terr_Exp_Coeff,3,0),2/VLOOKUP(H6,Terr_Exp_Coeff,2,0)),
        //           2.01*POWER(D6/VLOOKUP(H6,Terr_Exp_Coeff,3,0),2/VLOOKUP(H6,Terr_Exp_Coeff,2,0)))

        $(".kz_cal").on("change keyup keypress",function(){
            var mean_roof_height  = d6 = parseFloat($(this).parents(".site-info-feilds").find(".mean_roof_height").val());
            var exposure_category = h6 = $(this).parents(".site-info-feilds").find(".exposure_category").val();

            if(isNaN(mean_roof_height))
            mean_roof_height =d6= 0;

            var kz = 0;
            var self = this;

            postData = {
                "mean_roof_height":mean_roof_height,
                "exposure_category":exposure_category,
                "type":"kz",
            }
            var url = $('#site_formula').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.post(url,postData,function(data){
            if(data.result == 1)
            {
                console.log("kz >> "+data.data);
                $(self).parents(".site-info-feilds").find(".kz").val(data.data);

            }
            });
        });
    };
    function exposure_values()
    {
        var exposure_array = {
            B:{"0":7,"1":1200},
            C:{"0":9.5,"1":900},
            D:{"0":11.5,"1":700}
        };
        return exposure_array;
    }
    function reset_roof_values(self)
    {

        $(self).parents(".roof-container").find(".compression_strength").val('');
        $(self).parents(".roof-container").find(".pullout_strength").val('');
    }
    this.calculateWidthOfPressureCoefficient = function(){
        $(".width_cofficient ").on("change keyup keypress",function(){
            var mean_roof_height = d6 = parseFloat($(this).parents(".site-info-feilds").find(".mean_roof_height").val());
            if(isNaN(mean_roof_height))
            mean_roof_height = d6 = 0;
            var roof_length = d7 = parseFloat($(this).parents(".site-info-feilds").find(".roof_length").val());
            if(isNaN(roof_length))
            roof_length = d7 = 0;
            var roof_width = d8 = parseFloat($(this).parents(".site-info-feilds").find(".roof_width").val());
            if(isNaN(roof_width))
            roof_width = d8 = 0;
             // =IF(D7<D8,D7,D8)&"' * 10%"

             postData = {
                "mean_roof_height":mean_roof_height,
                "roof_length":roof_length,
                "roof_width":roof_width,
                "type":"width_pressure_cofficeint",
            }
            var url = $('#site_formula').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.post(url,postData,function(data){
            if(data.result == 1)
            {
                console.log("kz >> "+data.data);
                console.log("wp1 >> "+data.wp1);
                console.log("wp2 >> "+data.wp2);
                console.log("wp1_val >> "+data.wp1_val);
                console.log("wp1 >> "+data.wp1);

                $("#wp1").text(data.wp1);
                $("#wp2").text(data.wp2);
                $("#wp1_val").text(data.wp1_val);
                $("#wp2_val").text(data.wp2_val);

            }
            });



            // =IF(H8>7,IF(D19="NO",4&" FT",MAX(MIN(0.4*D6,0.1*MIN(D7:E8)),0.04*MIN(D7:E8),3)&" FT"), (D6*0.6)&" FT")



        });
    };
    function calculate_width_zone()
    {
        var mean_roof_height = d6 = parseFloat($("#mean_roof_height").val());
        if(isNaN(mean_roof_height))
        mean_roof_height = d6 = 0;
        var roof_length = d7 = parseFloat($("#roof_length").val());
        if(isNaN(roof_length))
        roof_length = d7 = 0;
        var roof_width = d8 = parseFloat($("#roof_width").val());
        if(isNaN(roof_width))
        roof_width = d8 = 0;
        var roof_slope_degree = h8 = parseFloat($("#roof_slope_degree").val());
        if(isNaN(roof_slope_degree))
            roof_slope_degree = h8 = 0;
        var hvhz = d19 = $("#hvhz").val();

        postData = {
            "mean_roof_height":mean_roof_height,
            "roof_length":roof_length,
            "roof_width":roof_width,
            "roof_slope_degree":roof_slope_degree,
            "hvhz":hvhz,
            "type":"width_pressure_cofficeint",
        }
        var url = $('#site_formula').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.post(url,postData,function(data){
        if(data.result == 1)
        {
            console.log("kz >> "+data.data);
            console.log("wp1 >> "+data.wp1);
            console.log("wp2 >> "+data.wp2);
            console.log("wp1_val >> "+data.wp1_val);
            console.log("wp1 >> "+data.wp1);

            $("#wp1").text(data.wp1);
            $("#wp2").text(data.wp2);
            $("#wp1_val").text(data.wp1_val);
            $("#wp2_val").text(data.wp2_val);
            $("#zone1").text(data.zone1);
            $("#zone2").text(data.zone2);
            $("#zone3").text(data.zone3);

        }
        });

    }
    function fill_width_zone(data)
    {
            $("#wp1").text(data.wp1);
            $("#wp2").text(data.wp2);
            $("#wp1_val").text(data.wp1_val);
            $("#wp2_val").text(data.wp2_val);
            $("#zone1").text(data.zone1);
            $("#zone2").text(data.zone2);
            $("#zone3").text(data.zone3);
    }
    function fill_site_values(data)
    {
        for(var i in data) {
            $("."+i).val(data[i]);
            // console.log("key "+i+" >> values >> "+data[i] );
        }
    }
    this.calculateZone = function(){
        $(".zone").on("change keyup keypress",function(){
            var mean_roof_height = d6 = parseFloat($(this).parents(".site-info-feilds").find(".mean_roof_height").val());
            if(isNaN(mean_roof_height))
            mean_roof_height = d7 = 0;
            var roof_length = d7 = parseFloat($(this).parents(".site-info-feilds").find(".roof_length").val());
            if(isNaN(roof_length))
            roof_length = d7 = 0;
            var roof_width = d8 = parseFloat($(this).parents(".site-info-feilds").find(".roof_width").val());
            if(isNaN(roof_width))
            roof_width = d8 = 0;
            var roof_slope_degree = h8 = parseFloat($(this).parents(".site-info-feilds").find(".roof_slope_degree").val());
            if(isNaN(roof_slope_degree))
            roof_slope_degree = h8 = 0;
            var hvhz = d19 = $(this).parents(".site-info-feilds").find(".hvhz").val();

            postData = {
                "mean_roof_height":mean_roof_height,
                "roof_length":roof_length,
                "roof_width":roof_width,
                "roof_slope_degree":roof_slope_degree,
                "hvhz":hvhz,
                "type":"width_pressure_cofficeint",
            }
            var url = $('#site_formula').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.post(url,postData,function(data){
            if(data.result == 1)
            {
                console.log("kz >> "+data.data);
                console.log("wp1 >> "+data.wp1);
                console.log("wp2 >> "+data.wp2);
                console.log("wp1_val >> "+data.wp1_val);
                console.log("wp1 >> "+data.wp1);

                $("#wp1").text(data.wp1);
                $("#wp2").text(data.wp2);
                $("#wp1_val").text(data.wp1_val);
                $("#wp2_val").text(data.wp2_val);
                $("#zone1").text(data.zone1);
                $("#zone2").text(data.zone2);
                $("#zone3").text(data.zone3);

            }
            });


        });
    };


    this.createProject = function(){
       $('.create-site-button').click(function(evt){
            evt.preventDefault();

            var postData = $(this).parents(".site-info-feilds").find('.create-site-form').serialize();
            var url = $(this).parents(".site-info-feilds").find('.create-site-form').attr('action');
            postData = postData;
            var self = this;
            console.log("url >> "+url);
            $.ajaxSetup({
                headers: {
                    'Access-Control-Allow-Headers': '*',
                    'Accept': 'xml'
                }
            });

            $(".form-group > .error").remove();

            $.post(url,postData,function(out){
                // console.log(out);
                if(out.result === 0)
                {
                    for(var i in out.errors){
                        // console.log(i);
                        $(self).parents(".site-info-feilds").find("."+i).parents(".form-group").append('<span class="error" >'+out.errors[i]+'</span>');
                    }
                    $(self).parents(".site-info-feilds").find(".create-roof-info").addClass('d-none');

                }
                if(out.result === 1 )
                {
                    $(self).parents(".site-info-feilds").find("._method").val(out.method);
                    $(self).parents(".site-info-feilds").find(".create-site-form").attr('action',out.url);

                   fill_width_zone(out.width_of_pressure_coefficient);
                   fill_site_values(out.data);
                   $("#roof-blueprint").find('.roof_height').val(out.data.mean_roof_height);
                   $("#velocity_pressure").val(out.velocity_pressure);

                    if(out.message){
                    $(".message").html(out.message);
                        setTimeout(function(){ $(".alert-success").fadeOut(1000); },1000 );

                        console.log("no_of_roofs >> "+out.data.no_of_roofs);

                        zone_show(out.data.asce_version);


                        if(out.roof_info_count == 0) {
                        $("#room_div").html('');

                            for(i=1; i <= (out.data.no_of_roofs); i++)
                            {
                                var section = $("#roof-blueprint").clone();
                                $(section).find('.roof_no').html('Roof Type - '+i);
                                $(section).find('.card-header').attr('id','roof-header-'+i);
                                $(section).find('.card-header').attr('data-target','#roof-collapse-'+i);
                                $(section).find('.card-header').attr('aria-controls','roof-collapse-'+i);
                                $(section).find('.collapse-body').attr('id','roof-collapse-'+i);
                                $(section).find('.collapse-body').attr('aria-labelledby','roof-header-'+i);

                                if(i != 1) {
                                    $(section).find('.collapse-body').removeClass('show');
                                }
                                else {
                                    $(section).find('.fas').removeClass('fa-plus-circle').addClass("fa-minus-circle");
                                }


                                $("#room_div").append(section);
                                $("#room_div").find('.card').last().attr('id','roof-'+i);
                                $("#room_div").find('.card').last().removeClass('d-none');
                                $(".numeric_feild_discount").keypress(function(event){
                                    return isNumber(event, this);
                                });
                            }
                            $(".create-roof-info").removeClass('d-none');
                        }

                    }
                    calculateRightRoofValuesForAll();

                    $("#exampleModal").modal('hide');

                }
            })

        })
    }
    this.addRoof = function() {
        $(document).on("click",".add-roof",function() {
           var roof_length = $("#room_div").find('.card').length;
           var roof_length = parseInt(roof_length) +1;

            // var section = $("#roof-blueprint").clone();
            var section = $(".card").last().clone();
            $(section).find('.roof_no').html('Roof Type - '+roof_length);
            $(section).find('.icon-accordian-2-minus').removeClass('icon-accordian-2-minus').addClass('icon-accordian-2-add');
            $(section).find('.rm').html('');
            $(section).find(".roof_info_id").last().val(0);

            $(section).find('.card-header').attr('id','roof-header-'+roof_length);
            $(section).find('.card-header').attr('data-target','#roof-collapse-'+roof_length);
            $(section).find('.card-header').attr('aria-controls','roof-collapse-'+roof_length);
            $(section).find('.collapse-body').attr('id','roof-collapse-'+roof_length);
            $(section).find('.collapse-body').attr('aria-labelledby','roof-header-'+roof_length);

            $(section).find('.collapse-body').removeClass('show');

            $(section).find('.rm').append('<span class="text-danger icon-accordian-2 ml-auto remove-roof"><i class="fas fa-trash-alt"></i></span>');


            $("#room_div").append(section);
            $("#room_div").find('.card').last().attr('id','roof-'+roof_length);
            $("#room_div").find('.card').last().removeClass('d-none');

            updateNoOfRoofs();

            $(".numeric_feild_discount").keypress(function(event){
            return isNumber(event, this);
            });
        });
    }
    this.removeRoof = function() {
        $(document).on("click",".remove-roof",function() {
            $(this).parents('.card').remove();
            updateNoOfRoofs();
            roofNoArrange();
        });
    }
    this.siteModal = function() {
        $(".siteModal").on('click',function() {
            console.log("modal");
            $("#exampleModal").modal();
        });
    }
    function updateNoOfRoofs()
    {
        var roof_length = parseInt($("#room_div").find('.card').length);
        $("#no_of_roofs").val((roof_length));
    }
    function roofNoArrange()
    {
        var roof_length = parseInt($("#room_div").find('.card').length);
        // for(let i = 1; i <= roof_length; i++) {
            // console.log("roof_no >> "+i);
            let j = 1;
            $("#room_div").find('.roof_no').each(function(){
                $(this).html("Roof - "+j);
                j++;
            });
        // }

    }
    function isNumber(evt, element)
     {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (
                (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
                (charCode < 48 || charCode > 57))
                return false;

                return true;
      }
    function external_posituve_and_negative_formula()
    {
        // =IF($H$9="GABLE",
        //     IF($H$8<7,HLOOKUP(1,'asce 7-16 tables pos gab'!gcp_10_100,2,0),
        //     IF(AND($H$8>7,$H$8<20),HLOOKUP(1,INDIRECT($'ASCE 7-16 Tables Pos Gab'.$E$19),3,0),
        //     IF(AND($H$8>20,$H$8<27),HLOOKUP(1,'asce 7-16 tables pos gab'!gcp_10_100,4,0),
        //     HLOOKUP(1,'asce 7-16 tables pos gab'!gcp_10_100,5,0)))),
        //     IF($H$8<7,HLOOKUP(1,'asce 7-16 tables pos hip'!gcp_10_100,2,0),
        //     IF(AND($H$8>7,$H$8<20),HLOOKUP(1,'asce 7-16 tables pos hip'!gcp_10_100,3,0),
        //     IF(AND($H$8>20,$H$8<27),HLOOKUP(1,'asce 7-16 tables pos hip'!gcp_10_100,4,0),
        //     HLOOKUP(1,'asce 7-16 tables pos hip'!gcp_10_100,5,0)))))

        // if(roof_type == "GABLE")
        // {
        //     if(roof_slope_degree < 7)
        //         var val = HLOOKUP(1,'asce 7-16 tables pos gab'!gcp_10_100,2,0);
        //     if(roof_slope_degree > 7 && roof_slope_degree < 20)
        //         var val = HLOOKUP(1,INDIRECT($'ASCE 7-16 Tables Pos Gab'.$E$19),3,0);
        //     if(roof_slope_degree > 20 && roof_slope_degree < 27)
        //         var val = HLOOKUP(1,'asce 7-16 tables pos gab'!gcp_10_100,4,0);
        //     else
        //         var val = HLOOKUP(1,'asce 7-16 tables pos gab'!gcp_10_100,5,0);
        // }
        // else
        // {
        //     if(roof_slope_degree < 7)
        //         var val = HLOOKUP(1,'asce 7-16 tables pos hip'!gcp_10_100,2,0);
        //     if(roof_slope_degree > 7 && roof_slope_degree < 20)
        //         var val = HLOOKUP(1,INDIRECT($'ASCE 7-16 Tables Pos hip'.$E$19),3,0);
        //     if(roof_slope_degree > 20 && roof_slope_degree < 27)
        //         var val = HLOOKUP(1,'asce 7-16 tables pos hip'!gcp_10_100,4,0);
        //     else
        //         var val = HLOOKUP(1,'asce 7-16 tables pos hip'!gcp_10_100,5,0);
        // }

        // =IF($H$9="GABLE",
        //     IF($H$8<7,HLOOKUP(1,GCP_10_100,2,0),
        //     IF(AND($H$8>7,$H$8<20),HLOOKUP(1,GCP_10_100,3,0),
        //     IF(AND($H$8>20,$H$8<27),HLOOKUP(1,GCP_10_100,4,0),
        //     HLOOKUP(1,GCP_10_100,5,0)))),
        //     IF($H$8<7,HLOOKUP(1,'asce 7-16 tables neg hip'!gcp_10_100,2,0),
        //     IF(AND($H$8>7,$H$8<20),HLOOKUP(1,'asce 7-16 tables neg hip'!gcp_10_100,3,0),
        //     IF(AND($H$8>20,$H$8<27),HLOOKUP(1,'asce 7-16 tables neg hip'!gcp_10_100,4,0),
        //     HLOOKUP(1,'asce 7-16 tables neg hip'!gcp_10_100,5,0)))))

        // if(roof_type == "GABLE")
        // {
        //     if(roof_slope_degree < 7)
        //         var val = HLOOKUP(1,GCP_10_100,2,0);
        //     if(roof_slope_degree > 7 && roof_slope_degree < 20)
        //         var val = HLOOKUP(1,GCP_10_100,3,0);
        //     if(roof_slope_degree > 20 && roof_slope_degree < 27)
        //         var val = HLOOKUP(1,GCP_10_100,4,0);
        //     else
        //         var val = HLOOKUP(1,GCP_10_100,5,0);
        // }
        // else
        // {
        //     if(roof_slope_degree < 7)
        //         var val = HLOOKUP(1,'asce 7-16 tables neg hip'!gcp_10_100,2,0);
        //     if(roof_slope_degree > 7 && roof_slope_degree < 20)
        //         var val = HLOOKUP(1,'asce 7-16 tables neg hip'!gcp_10_100,3,0);
        //     if(roof_slope_degree > 20 && roof_slope_degree < 27)
        //         var val = HLOOKUP(1,'asce 7-16 tables neg hip'!gcp_10_100,4,0);
        //     else
        //         var val = HLOOKUP(1,'asce 7-16 tables neg hip'!gcp_10_100,5,0);
        // }
    }


    this.__construct();
};

var project = new Project();
