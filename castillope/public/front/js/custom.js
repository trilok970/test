/////////////////////////////////////fixed header/////////////////////////////////////////

$(window).scroll(function(){
  if ($(window).scrollTop() >= 1) {
      $('header').addClass('fixed-header');
  }
  else {
      $('header').removeClass('fixed-header');

  }
});

///////////////////////////////////fixed header End//////////////////////////////////////


/////////////////////////////////////Custom Select//////////////////////////////////////
var x, i, j, l, ll, selElmnt, a, b, c;
/* Look for any elements with the class "custom-select": */
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /* For each element, create a new DIV that will act as the selected item: */
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /* For each element, create a new DIV that will contain the option list: */
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /* For each option in the original select element,
    create a new DIV that will act as an option item: */
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /* When an item is clicked, update the original select box,
        and the selected item: */
        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
    /* When the select box is clicked, close any other select boxes,
    and open/close the current select box: */
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}

function closeAllSelect(elmnt) {
  /* A function that will close all select boxes in the document,
  except the current select box: */
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);

/////////////////////////////////////Custom Select End//////////////////////////////////////


/////////////////////////////////////Recent projects collapse//////////////////////////////

  $(document).ready(function() {
      // Configure/customize these variables.
      if ($(window).width() < 479) {
        var showChar = 95;  // How many characters are shown by default
      }
      else if ($(window).width() < 575) {
        var showChar = 150;
      }
      else if ($(window).width() < 767) {
        var showChar = 180;
      }
      else if ($(window).width() < 991) {
        var showChar = 240;
      }
      else if ($(window).width() < 1199) {
        var showChar = 280;
      }
      else if ($(window).width() < 1366) {
        var showChar = 350;
      }
      else if ($(window).width() < 1439) {
        var showChar = 360;
      }
      else {
        var showChar = 510;
      }
      var ellipsestext = "...";
      var moretext = "+";
      var lesstext = "-";


      $('.more p').each(function() {
          var content = $(this).html();

          if(content.length > showChar) {
              var c = content.substr(0, showChar);
              var h = content.substr(showChar, content.length - showChar);
              var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink" style="display:none">' + moretext + '</a></span>';

              $(this).html(html);
          }

      });

      $(".morelink").click(function(){
          if($(this).hasClass("less")) {
              $(this).removeClass("less");
              $(this).removeClass("icon-accordian-minus");
              $(this).addClass("icon-accordian-add");
              //$(this).html(moretext);
          } else {
              $(this).removeClass("icon-accordian-add");
              $(this).addClass("less icon-accordian-minus");
              //$(this).html(lesstext);
          }
          $(this).parent().parent().parent().find(".morecontent").children().prev().toggle();
          $(this).parent().parent().parent().find(".morecontent").prev().toggle();
          return false;
      });



});


/////////////////////////////////////Recent projects collapse End//////////////////////////////





/////////////////////////////////////Structural Page accordian Js////////////////////////////

$(document).ready(function(){
    // Add minus icon for collapse element which is open by default
    $(".collapse.show").each(function(){
      $(this).prev(".card-header").find(".icon").addClass("icon-accordian-2-minus").removeClass("icon-accordian-2-add");
    });

    // Toggle plus minus icon on show hide of collapse element
    // $(".collapse").on('show.bs.collapse', function(){
        $(document).on('show.bs.collapse','.collapse',function(){
      $(this).prev(".card-header").find(".icon").removeClass("icon-accordian-2-add").addClass("icon-accordian-2-minus");
    });
    $(document).on('hide.bs.collapse','.collapse', function(){
      $(this).prev(".card-header").find(".icon").removeClass("icon-accordian-2-minus").addClass("icon-accordian-2-add");
    });
});

///////////////////////////////////Structural Page accordian Js End/////////////////////////



///////////////////////////////////HEader search in responsive////////////////////////////////////////////////////
$(document).ready(function(){
  $(".header-inner form span").click(function(){
    $(".input-group").slideToggle();
  });
});





$(document).ready(function(){
    $('.left-side-btn').click(function(e){
        e.stopPropagation();
         $('.sidebar').toggleClass('show-sidebar');
    });
    $('.sidebar').click(function(e){
        e.stopPropagation();
    });
    $('#close-btn').click(function(e){
           $('.sidebar').removeClass('show-sidebar');
    });
});
