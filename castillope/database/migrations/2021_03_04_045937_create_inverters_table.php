<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvertersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('inverters')) {
        Schema::create('inverters', function (Blueprint $table) {
            $table->id();
            $table->integer('manufacturer_id')->default(0);
            $table->string('model_no');
            $table->float('maximum_input_power')->default(0);
            $table->float('maximum_input_valtage')->default(0);
            $table->float('maximum_input_current')->default(0);
            $table->float('maximum_output_power')->default(0);
            $table->float('maximum_output_voltage')->default(0);
            $table->float('maximum_output_current')->default(0);
            $table->float('operating_voltage_range')->default(0);
            $table->float('mppt_voltage_range')->default(0);
            $table->float('no_of_mppts')->default(0);
            $table->float('no_of_inputs_mppt')->default(0);
            $table->float('maximum_modules_per_string')->default(0);
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inverters');
    }
}
