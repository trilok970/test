<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDifferentInvertersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('different_inverters')) {

        Schema::create('different_inverters', function (Blueprint $table) {
            $table->id();
            $table->integer('electrical_id')->default(0);
            $table->integer('module_manufacturer_id')->default(0);
            $table->integer('module_inverter_id')->default(0);
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('different_inverters');
    }
}
