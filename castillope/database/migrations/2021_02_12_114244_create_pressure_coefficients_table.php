<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePressureCoefficientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pressure_coefficients', function (Blueprint $table) {
            $table->id();
            $table->string('site_id');
            $table->float('wp1')->default(0);
            $table->float('wp2')->default(0);
            $table->float('wp1_val')->default(0);
            $table->float('wp2_val')->default(0);
            $table->string('zone1')->default(0);
            $table->string('zone2')->default(0);
            $table->string('zone3')->default(0);
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pressure_coefficients');
    }
}
