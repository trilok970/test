<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptimizerStrings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('optimizer_strings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('electrical_id')->default(0);
            $table->bigInteger('electrical_string_id')->default(0);
            $table->bigInteger('optimizer_id')->default(0);
            $table->float('value')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('optimizer_strings');
    }
}
