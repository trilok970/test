<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('projects')) {
            Schema::create('projects', function (Blueprint $table) {
                $table->id();
                $table->integer('user_id')->default(0);
                $table->string('owner_name');
                $table->text('address');
                $table->string('state');
                $table->string('ult_wind_speed');
                $table->string('ground_snow_load');
                $table->string('ahj');
                $table->integer('status')->default(1);
                $table->integer('is_deleted')->default(0);
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
