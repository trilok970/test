<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElectricalStringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('electrical_strings')) {

        Schema::create('electrical_strings', function (Blueprint $table) {
            $table->id();
            $table->integer('electrical_id')->default(0);
            $table->integer('module_id')->default(0);
            $table->integer('inverter_id')->default(0);
            $table->integer('size')->default(0);
            $table->date('from')->nullable();
            $table->date('to')->nullable();
            $table->float('current')->default(0);
            $table->integer('terminal_ampacity')->default(0);
            $table->integer('wire_ampacity')->default(0);
            $table->integer('no_of_conductors')->default(0);
            $table->integer('sets_of_wire')->default(0);
            $table->integer('no_of_conduits')->default(0);
            $table->string('conduit_location')->nullable();
            $table->string('conduit_or_material')->nullable();
            $table->integer('conduit_derate')->default(0);
            $table->integer('temp_derate')->default(0);
            $table->integer('wire_size')->default(0);
            $table->integer('corrected_wire_size')->default(0);
            $table->integer('conductor_ampacity')->default(0);
            $table->integer('derated_ampacity')->default(0);
            $table->integer('ground_wire')->default(0);
            $table->integer('corrected_ground_wire')->default(0);
            $table->integer('ocpd')->default(0);
            $table->integer('conductor_type')->default(0);
            $table->integer('vd')->default(0);
            $table->integer('max_length')->default(0);

            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('electrical_strings');
    }
}
