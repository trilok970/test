<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_infos', function (Blueprint $table) {
            $table->id();
            $table->integer('prodject_id')->default(0);
            $table->integer('manufacturer_id')->default(0);
            $table->integer('attachment_manufacturer_id')->default(0);
            $table->integer('attachment_id')->default(0);
            $table->integer('module_id')->default(0);
            $table->float('module_length')->default(0);
            $table->string('attachment_type');
            $table->float('module_width')->default(0);
            $table->float('module_area')->default(0);
            $table->float('compression_strenght')->default(0);
            $table->string('module_orientation',100);
            $table->float('pullout_strenght')->default(0);
            $table->float('roof_height')->default(0);
            $table->integer('no_of_rails')->default(0);
            $table->integer('limit_max_span_to')->default(0);
            $table->integer('rafter_seam_spacing')->default(0);
            $table->float('roof_slope')->default(0);
            $table->float('ext_pos_pre_coef0')->default(0);
            $table->float('ext_pos_pre_coef1')->default(0);
            $table->float('ext_pos_pre_coef2')->default(0);
            $table->float('ext_pos_pre_coef3')->default(0);
            $table->float('ext_pos_pre_coef4')->default(0);
            $table->float('ext_pos_pre_coef5')->default(0);
            $table->float('ext_pos_pre_coef6')->default(0);
            $table->float('ext_neg_pre_coef0')->default(0);
            $table->float('ext_neg_pre_coef1')->default(0);
            $table->float('ext_neg_pre_coef2')->default(0);
            $table->float('ext_neg_pre_coef3')->default(0);
            $table->float('ext_neg_pre_coef4')->default(0);
            $table->float('ext_neg_pre_coef5')->default(0);
            $table->float('ext_neg_pre_coef6')->default(0);
            $table->float('zone_kz0')->default(0);
            $table->float('zone_kz1')->default(0);
            $table->float('zone_kz2')->default(0);
            $table->float('zone_kz3')->default(0);
            $table->float('zone_kz4')->default(0);
            $table->float('zone_kz5')->default(0);
            $table->float('zone_kz6')->default(0);
            $table->float('positive_pressure0')->default(0);
            $table->float('positive_pressure1')->default(0);
            $table->float('positive_pressure2')->default(0);
            $table->float('positive_pressure3')->default(0);
            $table->float('positive_pressure4')->default(0);
            $table->float('positive_pressure5')->default(0);
            $table->float('positive_pressure6')->default(0);
            $table->float('negative_pressure0')->default(0);
            $table->float('negative_pressure1')->default(0);
            $table->float('negative_pressure2')->default(0);
            $table->float('negative_pressure3')->default(0);
            $table->float('negative_pressure4')->default(0);
            $table->float('negative_pressure5')->default(0);
            $table->float('negative_pressure6')->default(0);
            $table->float('adjusted_pos_pre0')->default(0);
            $table->float('adjusted_pos_pre1')->default(0);
            $table->float('adjusted_pos_pre2')->default(0);
            $table->float('adjusted_pos_pre3')->default(0);
            $table->float('adjusted_pos_pre4')->default(0);
            $table->float('adjusted_pos_pre5')->default(0);
            $table->float('adjusted_pos_pre6')->default(0);
            $table->float('adjusted_exp_neg_pre0')->default(0);
            $table->float('adjusted_exp_neg_pre1')->default(0);
            $table->float('adjusted_exp_neg_pre2')->default(0);
            $table->float('adjusted_exp_neg_pre3')->default(0);
            $table->float('adjusted_exp_neg_pre4')->default(0);
            $table->float('adjusted_exp_neg_pre5')->default(0);
            $table->float('adjusted_exp_neg_pre6')->default(0);
            $table->float('adjusted_non_exp_neg_pre0')->default(0);
            $table->float('adjusted_non_exp_neg_pre1')->default(0);
            $table->float('adjusted_non_exp_neg_pre2')->default(0);
            $table->float('adjusted_non_exp_neg_pre3')->default(0);
            $table->float('adjusted_non_exp_neg_pre4')->default(0);
            $table->float('adjusted_non_exp_neg_pre5')->default(0);
            $table->float('adjusted_non_exp_neg_pre6')->default(0);
            $table->float('positive_loading0')->default(0);
            $table->float('positive_loading1')->default(0);
            $table->float('positive_loading2')->default(0);
            $table->float('positive_loading3')->default(0);
            $table->float('positive_loading4')->default(0);
            $table->float('positive_loading5')->default(0);
            $table->float('positive_loading6')->default(0);
            $table->float('negative_exposed_loading0')->default(0);
            $table->float('negative_exposed_loading1')->default(0);
            $table->float('negative_exposed_loading2')->default(0);
            $table->float('negative_exposed_loading3')->default(0);
            $table->float('negative_exposed_loading4')->default(0);
            $table->float('negative_exposed_loading5')->default(0);
            $table->float('negative_exposed_loading6')->default(0);
            $table->float('negative_non_exposed_loading0')->default(0);
            $table->float('negative_non_exposed_loading1')->default(0);
            $table->float('negative_non_exposed_loading2')->default(0);
            $table->float('negative_non_exposed_loading3')->default(0);
            $table->float('negative_non_exposed_loading4')->default(0);
            $table->float('negative_non_exposed_loading5')->default(0);
            $table->float('negative_non_exposed_loading6')->default(0);
            $table->float('attachment_exposed_negative_spacing0')->default(0);
            $table->float('attachment_exposed_negative_spacing1')->default(0);
            $table->float('attachment_exposed_negative_spacing2')->default(0);
            $table->float('attachment_exposed_negative_spacing3')->default(0);
            $table->float('attachment_exposed_negative_spacing4')->default(0);
            $table->float('attachment_exposed_negative_spacing5')->default(0);
            $table->float('attachment_exposed_negative_spacing6')->default(0);
            $table->float('attachment_non_exposed_negative_spacing0')->default(0);
            $table->float('attachment_non_exposed_negative_spacing1')->default(0);
            $table->float('attachment_non_exposed_negative_spacing2')->default(0);
            $table->float('attachment_non_exposed_negative_spacing3')->default(0);
            $table->float('attachment_non_exposed_negative_spacing4')->default(0);
            $table->float('attachment_non_exposed_negative_spacing5')->default(0);
            $table->float('attachment_non_exposed_negative_spacing6')->default(0);

            $table->float('ext_pos_pre_coef0_7_10')->default(0);
            $table->float('ext_pos_pre_coef1_7_10')->default(0);
            $table->float('ext_pos_pre_coef0_7_10')->default(0);
            $table->float('ext_neg_pre_coef0_7_10')->default(0);
            $table->float('ext_neg_pre_coef1_7_10')->default(0);
            $table->float('ext_neg_pre_coef2_7_10')->default(0);
            $table->float('positive_pressure0_7_10')->default(0);
            $table->float('positive_pressure1_7_10')->default(0);
            $table->float('positive_pressure2_7_10')->default(0);
            $table->float('negative_pressure0_7_10')->default(0);
            $table->float('negative_pressure1_7_10')->default(0);
            $table->float('negative_pressure2_7_10')->default(0);
            $table->float('positive_loading0_7_10')->default(0);
            $table->float('positive_loading1_7_10')->default(0);
            $table->float('positive_loading2_7_10')->default(0);
            $table->float('negative_loading0_7_10')->default(0);
            $table->float('negative_loading1_7_10')->default(0);
            $table->float('negative_loading2_7_10')->default(0);


            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_infos');
    }
}
