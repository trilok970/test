<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('sites')) {
        Schema::create('sites', function (Blueprint $table) {
            $table->id();
            $table->integer('project_id')->default(0);
            $table->integer('fbc_version')->default(0);
            $table->float('mean_roof_height')->default(0);
            $table->float('dead_load')->default(0);
            $table->float('live_load')->default(0);
            $table->float('seismic_load')->default(0);
            $table->float('ground_snow_load')->default(0);
            $table->float('exposure_factor')->default(0);
            $table->float('temperature_factor')->default(0);
            $table->float('importance_factor')->default(0);
            $table->float('slope_factor')->default(0);
            $table->float('wind_directionality_factor')->default(0);
            $table->float('topographic_factor')->default(0);
            $table->float('sloped_roof_snow_load')->default(0);
            $table->float('building_length')->default(0);
            $table->float('building_width')->default(0);
            $table->integer('risk_category')->default(0);
            $table->integer('exposure_category')->default(0);
            $table->float('ultimate_wind_speed')->default(0);
            $table->float('normal_wind_speed')->default(0);
            $table->integer('roof_type')->default(0);
            $table->integer('hvhz')->default(0);
            $table->integer('type_of_building')->default(0);
            $table->integer('no_of_roofs')->default(0);
            $table->float('zone_width')->default(0);
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
