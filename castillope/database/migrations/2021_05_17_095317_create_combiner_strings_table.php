<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCombinerStringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('combiner_strings')) {

        Schema::create('combiner_strings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('electrical_id')->default(0);
            $table->bigInteger('electrical_string_id')->default(0);
            $table->bigInteger('combiner_id')->default(0);
            $table->float('value')->default(0);
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('combiner_strings');
    }
}
