<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttachmentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('attachment_methods')) {
        Schema::create('attachment_methods', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->float('allowable_uplift')->default(0);
            $table->float('screw_diameter')->default(0);
            $table->integer('no_of_screws')->default(0);
            $table->text('description');
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachment_methods');
    }
}
