<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElectricalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('electricals')) {

        Schema::create('electricals', function (Blueprint $table) {
            $table->id();
            $table->integer('project_id')->default(0);
            $table->string('inverter_type');
            $table->integer('manufacturer_id')->default(0);
            $table->integer('module_id')->default(0);
            $table->integer('inverter_manufacturer_id')->default(0);
            $table->integer('inverter_module_id')->default(0);
            $table->integer('no_of_inverters')->default(0);
            $table->string('mixture_of_inverters',10);
            $table->integer('no_of_diff_inverters')->default(0);
            $table->integer('no_of_strings')->default(0);
            $table->integer('battery_id')->default(0);
            $table->float('number')->default(0);
            $table->float('ambient_temp')->default(0);
            $table->float('minimum_temp')->default(0);
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('electricals');
    }
}
