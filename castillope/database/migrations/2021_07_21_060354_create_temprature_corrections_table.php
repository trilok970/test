<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempratureCorrectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('temprature_corrections')) {

        Schema::create('temprature_corrections', function (Blueprint $table) {
            $table->id();
            $table->integer('min_val')->default(0);
            $table->integer('max_val')->default(0);
            $table->float('factor')->default(0);
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temprature_corrections');
    }
}
