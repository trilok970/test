<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    public function site() {
        return $this->hasOne(Site::class);
    }
    public function electrical() {
        return $this->hasOne(Electrical::class);
    }
    public function RoofInfo() {
        return $this->hasMany(RoofInfo::class);
    }
    public function BlockDesigns() {
        return $this->hasMany(BlockDesign::class);
    }

}
