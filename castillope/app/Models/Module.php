<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    use HasFactory;
	
	 protected $fillable = [
        'manufacturer_id','module_model','pmp','voc','vmpp','tvoc','isc','imp','tcpm','module_lenght','module_width','allowed_pressure_short_side','allowed_pressure_long_side'
    ];
	public function manufacturer() {
		return $this->belongsTo(Manufacturer::class,'manufacturer_id');
	}
}
