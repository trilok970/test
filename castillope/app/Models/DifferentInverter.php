<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DifferentInverter extends Model
{
    use HasFactory;
    public function inverter_manufacturer() {
		return $this->belongsTo(Manufacturer::class,'inverter_manufacturer_id');
	}
    public function inverter() {
		return $this->belongsTo(Inverter::class,'inverter_id');
	}
}
