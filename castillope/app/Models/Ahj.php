<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ahj extends Model
{
    use HasFactory;
	
	 protected $fillable = [
        'ahj','state','fire_setback','note'
    ];
}
