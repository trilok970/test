<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ElectricalString extends Model
{
    use HasFactory;
    public function module() {
		return $this->belongsTo(Module::class);
	}
    public function inverter() {
		return $this->belongsTo(Inverter::class);
	}
    public function electrical() {
		return $this->belongsTo(Electrical::class);
	}
    public function CombinerString() {
		return $this->hasMany(CombinerString::class,'electrical_id','electrical_id');
	}
	public function OptimizerString() {
		return $this->hasMany(OptimizerString::class,'electrical_id','electrical_id');
	}
}
