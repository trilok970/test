<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    use HasFactory;

	 protected $fillable = [
        'title','type'
    ];

	public static function getManufacturerModel($limit, $offset, $search, $orderby,$order)
    {
        $orderby  = $orderby ? $orderby : 'id';
        $order    = $order ? $order : 'desc';

        $q = new Manufacturer();

        if($search && !empty($search)){
            $q = $q->where(function($query) use ($search) {
                $query->orWhere('name', 'LIKE', $search.'%');
                $query->orWhere('type', 'LIKE', $search.'%');
            });
        }

        $response   =   $q->orderBy($orderby, $order)
                            ->offset($offset)
                            ->limit($limit)
                            ->get();

        $response   =   json_decode(json_encode($response));
        return $response;
    }


}
