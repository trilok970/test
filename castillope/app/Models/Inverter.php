<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inverter extends Model
{
    use HasFactory;
    protected $fillable = [
        'manufacturer_id','model_no','inverter_type_id','maximum_input_power','maximum_input_voltage','maximum_input_current','maximum_output_power','maximum_output_voltage','maximum_output_current','operating_voltage_range','mppt_voltage_range','no_of_mppts','no_of_inputs_mppt','maximum_modules_per_string',
    ];
    public function manufacturer() {
		return $this->belongsTo(Manufacturer::class);
	}
    public function InverterType() {
        return $this->belongsTo(InverterType::class);
    }
}
