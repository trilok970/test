<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlockDesign extends Model
{
    use HasFactory;
    public function project() {
        return $this->belongsTo(Project::class);
    }
    public function getFileAttribute($value) {
        return url($value);
    }
}
