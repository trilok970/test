<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoofInfo extends Model
{
    use HasFactory;
    public function manufacturer() {
		return $this->belongsTo(Manufacturer::class);
	}
    public function module() {
		return $this->belongsTo(Module::class);
	}
    public function attachment_manufacturer() {
		return $this->belongsTo(Manufacturer::class,'attachment_manufacturer_id');
	}
    public function attachment() {
		return $this->belongsTo(Attachment::class);
	}
}
