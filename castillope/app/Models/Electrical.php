<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Electrical extends Model
{
    use HasFactory;
    public function manufacturer() {
		return $this->belongsTo(Manufacturer::class);
	}
    public function inverter_manufacturer() {
		return $this->belongsTo(Manufacturer::class,'inverter_manufacturer_id');
	}
    public function module() {
		return $this->belongsTo(Module::class);
	}
    public function project() {
		return $this->belongsTo(Project::class);
	}
    public function inverter_model() {
		return $this->belongsTo(Inverter::class,'inverter_model_id');
	}
    public function battery() {
		return $this->belongsTo(Battery::class);
	}
    public function DifferentInverter() {
		return $this->hasMany(DifferentInverter::class);
	}
    public function CombinerString() {
		return $this->hasMany(CombinerString::class);
	}
    public function InverterType() {
        return $this->belongsTo(InverterType::class);
    }
    public function ElectricalString() {
		return $this->hasMany(ElectricalString::class);
	}
	public function OptimizerString() {
		return $this->hasMany(OptimizerString::class);
	}
}
