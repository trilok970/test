<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    use HasFactory;

	 protected $fillable = [
        'manufacturer_id','model_no','title','allowable_pollout_strength','allowable_compression_strength','number_of_screws','note'
    ];

	public function manufacturer() {
		return $this->belongsTo(Manufacturer::class);
	}

	public function moduleModel() {
		return $this->belongsTo(Module::class,'model_id');
	}
}
