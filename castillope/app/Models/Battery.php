<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Battery extends Model
{
    use HasFactory;
    protected $fillable = [
        'manufacturer_id','model_no','maximum_output_power','maximum_output_voltage','maximum_output_current','operating_voltage_range',
    ];
    public function manufacturer() {
		return $this->belongsTo(Manufacturer::class);
	}
}
