<?php

namespace App\Traits;

use App\Models\Inverter;
use App\Models\CombinerString;
use App\Models\InverterType;
use App\Models\Manufacturer;
use App\Models\OptimizerString;
use Illuminate\Http\Request;
use Config;
use Artisan;
trait ElectricalFormulaTrait {

    public function conduitDerate($no_of_conductors,$sets_of_wire,$no_of_conduits)
    {
        $no_of_wire =  ($no_of_conductors * $sets_of_wire) / $no_of_conduits;
        $conduit_derate_array = config('constants.conduit_derate');
        $conduit_derate = $conduit_derate_array[$no_of_wire] ?? 0;
        return  round($conduit_derate,4);
    }
    public function conduitDerateCalculation(Request $request)
    {
        $conduit_derate = $this->conduitDerate($request->no_of_conductors,$request->sets_of_wire,$request->no_of_conduits);
        $data['result'] = 1;
        $data['data'] = round($conduit_derate,4);
        return response()->json($data);

    }
    public function tempDerate($conduit_location,$ambient_temp)
    {
        $temp =  ($conduit_location + $ambient_temp);
        $temp_derate_array = config('constants.temp_derate');
        $temp_derate = $temp_derate_array[$temp] ?? 0;
        return round($temp_derate,4);
    }
    public function tempDerateCalculation(Request $request)
    {
        $temp_derate = $this->tempDerate($request->conduit_location,$request->ambient_temp);
        $data['result'] = 1;
        $data['data'] = round($temp_derate,4);
        return response()->json($data);
    }
    public function terminalRatingCalculation(Request $request)
    {
        $terminal_rating = $this->terminal_rating($request->terminal_ampacity,strtolower($request->conductor_material),$request->wire_size);
        $current = $request->current;
        if($terminal_rating < $current){
            $warning_message = '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Terminal rating is not less than current.';
        }
        else {
            $warning_message = '';
        }
        $data['result'] = 1;
        $data['warning_message'] = $warning_message;
        $data['data'] = round($terminal_rating,4);
        return response()->json($data);
    }
    public function ampacityRating($conduit_location,$corrected_wire_size,$conductor_material,$wire_ampacity) {
        // echo "conduit_location >> ".$conduit_location;
        // echo "corrected_wire_size >> ".$corrected_wire_size;
        // echo "conductor_material >> ".$conductor_material;
        // echo "wire_ampacity   >> ".$wire_ampacity;
        if($conduit_location == 'f')
        {
            $ampacity_rating_array = config('constants.terminal_rating.free_air');
            $conductor_ampacity_dropdown_array = config('constants.terminal_rating_dropdown.free_air');
        }
        else {
            $ampacity_rating_array = config('constants.terminal_rating.wire_size');
            $conductor_ampacity_dropdown_array = config('constants.terminal_rating_dropdown.wire_size');

        }
//         echo $conductor_material;
//         echo $wire_ampacity;
// echo "<pre>";
            // print_r($conductor_ampacity_dropdown_array);exit;
        $ampacity_rating = $ampacity_rating_array[$corrected_wire_size][$conductor_material][$wire_ampacity] ?? 0;
        $ampacity_rating_dropdown = $conductor_ampacity_dropdown_array[$conductor_material][$wire_ampacity] ?? 0;

        // echo "<pre>";
        // print_r($ampacity_rating_dropdown);exit;
        if($ampacity_rating_dropdown && count($ampacity_rating_dropdown) > 0) {
            $option = '';
            foreach($ampacity_rating_dropdown as $val) {
                if($val == $ampacity_rating) {
                    $selected = "selected";
                }
                else {
                    $selected = "";
                }
                $option .= '<option '.$selected.' value="'.$val.'">'.$val.'</option>';
            }
        $ampacity_rating_dropdown_val = $option;

        }
        else {
            $ampacity_rating_dropdown_val = '';
        }
        return array("ampacity_rating"=> $ampacity_rating,"ampacity_rating_dropdown" => $ampacity_rating_dropdown_val,"ampacity_rating_dropdown_array"=>$ampacity_rating_dropdown);
    }
    public function ampacityRatingCalculation(Request $request)
    {
        $wire_ampacity = $request->wire_ampacity;
        $conductor_material = strtolower($request->conductor_material);
        $corrected_wire_size = $request->corrected_wire_size;
        $conduit_location = $request->conduit_location;

        $result = $this->ampacityRating($conduit_location,$corrected_wire_size,$conductor_material,$wire_ampacity);

        $data['result'] = 1;
        $data['ampacity_rating_dropdown'] = $result['ampacity_rating_dropdown'];
        $data['data'] = $result['ampacity_rating'];
        return response()->json($data);
    }
    public function terminal_rating($terminal_ampacity,$conductor_material,$wire_size) {
        $terminal_rating_array = config('constants.terminal_rating.wire_size');
        // echo $terminal_ampacity;
        // echo "wire_size >> ". $wire_size;
        // echo "conductor_material >> ". $conductor_material;
        // echo "<pre>";
        // print_r($terminal_rating_array);exit;
       return $terminal_rating = $terminal_rating_array[$wire_size][$conductor_material][$terminal_ampacity] ?? 0;
    }
    public function deratedAmpacityCalculation(Request $request)
    {
        $terminal_rating = $this->terminal_rating($request->terminal_ampacity,strtolower($request->conductor_material),$request->wire_size);

        $derated_ampacity = $request->conductor_ampacity * $request->temp_derate * $request->conduit_derate;
        $ocpd = $request->ocpd;
        if($derated_ampacity < $ocpd){
            $warning_message_ocpd = '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Derated ampacity is less than OCPD rating.';
        }
        else {
            $warning_message_ocpd = '';
        }
        if($terminal_rating > $derated_ampacity){
            $warning_message_ter = '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Terminal rating should not greater than Derated ampacity.';
        }
        else {
            $warning_message_ter = '';
        }
        $data['result'] = 1;
        $data['warning_message_ocpd'] = $warning_message_ocpd;
        $data['warning_message_ter'] = $warning_message_ter;
        $data['terminal_rating'] = $terminal_rating;
        $data['data'] = round($derated_ampacity,4);
        return response()->json($data);
    }
    public function getLargestValue(array $arr, $val)
    {
        $last = null;
        foreach ($arr as $key => $value) {
            if($value > $val) {
                return $value;
            }
            $last = $value;
        }
        return $last;
    }
    public function ground_wire() {
        $ground_wire_array = config('constants.ground_wire.ocpd');
        foreach($ground_wire_array as $key => $ground_wire) {
            $data['value1'][] = $key;
            $data['value2'][] = $ground_wire['cu'];
            $data['value3'][] = $ground_wire['al'];
        }
        $data['value2'] = array_unique($data['value2']);
        $data['value3'] = array_unique($data['value3']);
        $data['ground_wire_array'] = $ground_wire_array;
        return $data;
    }
    public function groundWireCalculation(Request $request)
    {
        $conductor_material = strtolower($request->conductor_material);
        $ground_wire_datas = $this->ground_wire();
        $ground_wire_array = $ground_wire_datas['ground_wire_array'];
        $value1 = $ground_wire_datas['value1'];
        $value2 = $ground_wire_datas['value2'];
        $value3 = $ground_wire_datas['value3'];
        $ground_wire_values_array = $value1;
        // echo "<pre>";
        // print_r($value1);
        // print_r($value2);
        // print_r($value3);
        // print_r($ground_wire_array);
        // exit;
        if(in_array($request->ocpd,$ground_wire_values_array))
        {
            $ground_wire = $ground_wire_array[$request->ocpd][$conductor_material] ?? 0;
        }
        else {
            $ocpd_next_value = $this->getLargestValue($ground_wire_values_array,$request->ocpd);
            $ground_wire = $ground_wire_array[$ocpd_next_value][$conductor_material] ?? 0;
        }

        if($conductor_material == 'cu') {
            $ground_wire_dropdown = $value2;
        }
        else if($conductor_material == 'al') {
            $ground_wire_dropdown = $value3;
        }
        if($ground_wire_dropdown && count($ground_wire_dropdown) > 0) {
            $option = '';
            foreach($ground_wire_dropdown as $val) {
                if($val == $ground_wire) {
                    $selected = "selected";
                }
                else {
                    $selected = "";
                }
                if($val === 1 || $val === 2 || $val === 3 || $val === 4 || $val === 6 || $val === 8 || $val === 10 || $val === 12 || $val === 14) {
                    $option .= '<option '.$selected.' value="'.$val.'">#'.$val.'</option>';
                }
                else {
                    $option .= '<option '.$selected.' value="'.$val.'">'.$val.'</option>';
                }
            }
        $data['ground_wire_dropdown'] = $option;

        }

        $data['result'] = 1;
        $data['data'] = $ground_wire;
        return response()->json($data);
    }
    public function correctedGroundWireCalculation(Request $request)
    {

        $wire_size = $request->wire_size;
        $corrected_wire_size = $request->corrected_wire_size;
        $ground_wire = $request->ground_wire;

        $terminal_rating_array = config('constants.terminal_rating.wire_size');
        $wire_size_val = $terminal_rating_array[$wire_size]['circular_mils'];
        $corrected_wire_size_val = $terminal_rating_array[$corrected_wire_size]['circular_mils'];
        $ground_wire_val = $terminal_rating_array[$ground_wire]['circular_mils'];
        if($wire_size_val > 0) {
            $corrected_ground_wire = ($corrected_wire_size_val / $wire_size_val) * $ground_wire_val;
        }
        $data['wire_size_val'] = $wire_size_val;
        $data['corrected_wire_size_val'] = $corrected_wire_size_val;
        $data['ground_wire_val'] = $ground_wire_val;
        $data['corrected_ground_wire'] = $corrected_ground_wire;

        foreach($terminal_rating_array as $key => $terminal_rating) {
            if($terminal_rating['circular_mils'] > 0 ) {
                $circular_mils_dropdown[] = $terminal_rating['circular_mils'];
                $circular_mils_values[$terminal_rating['circular_mils']] = $key;
            }
        }

        if(in_array($corrected_ground_wire,$circular_mils_dropdown))
        {
            $corrected_ground_wire = $corrected_ground_wire ?? 0;
        }
        else {
            $corrected_ground_wire_next_value = $this->getLargestValue($circular_mils_dropdown,$corrected_ground_wire);
            $corrected_ground_wire = $corrected_ground_wire_next_value ?? 0;
        }
        $data['corrected_ground_wire1'] = $corrected_ground_wire;

        $corrected_ground_wire = $circular_mils_values[$corrected_ground_wire];
        // echo "<pre>";
        // print_r($value1);
        // exit;


        $data['result'] = 1;
        $data['circular_mils_dropdown'] = $circular_mils_dropdown;
        $data['data'] = $corrected_ground_wire;
        return response()->json($data);
    }
    public function maxLengthCalculation(Request $request)
    {
        $vd = $request->vd;
        $voltage = $request->voltage;
        $phase = $request->phase;
        $current = $request->current;
        if($phase == 'dc' || $phase == 'single_phase') {
            $p = 2;
        }
        else if($phase == 'three_phase') {
            $p = 3;
        }
         $r = $this->terminal_rating("ohm",strtolower($request->conductor_material),$request->wire_size);
        if($p > 0 && $r >0 && $current > 0) {
            $max_length = ($vd * $voltage * 10) / ($p * $r * $current);
        }

        // echo "<pre>";
        // print_r($value1);
        // exit;


        $data['result'] = 1;
        $data['data'] = round($max_length ?? 0,4);
        $data['r'] = $r ?? 0;
        return response()->json($data);
    }

    public function sizeCalculation(Request $request) {

        $size = $request->size;
        $inverter_id = $request->inverter_id;
        $size_warning_message = "";

        if($inverter_id > 0) {
            $inverter = Inverter::find($inverter_id);

            if($size < $inverter->maximum_modules_per_string) {
                // echo "<pre>";
                // print_r($inverter);
                // exit;
                $size_warning_message = "String size should not be less than $inverter->maximum_modules_per_string";
            }
        }
        $data['result'] = 1;
        $data['data'] = $size ?? "";
        $data['size_warning_message'] = $size_warning_message ?? "";
        $data['maximum_modules_per_string'] = $inverter->maximum_modules_per_string ?? "";
        return response()->json($data);
    }
    public function combinerEdit(Request $request) {
        $combiner_id = $request->combiner_id;
        $stringIds = CombinerString::where('combiner_id',$combiner_id)->pluck('electrical_string_id');
        $data['result'] = 1;
        $data['data'] = $stringIds ?? "";
        return response()->json($data);
    }
    public function inverterTypeId(Request $request) {
        $inverter_type_id = $request->inverter_type_id;
        $inverter_type = InverterType::find($inverter_type_id);
        $inverterManufacturerIds = Inverter::where('inverter_type_id',$inverter_type_id)->pluck('manufacturer_id');
        $inverter_manufacturers = Manufacturer::where('type','inverter')->whereIn('id',$inverterManufacturerIds)->get();
        if($inverter_manufacturers) {
            $data = '';
            foreach($inverter_manufacturers as $inv_manufacturer) {
                $data .='<option '.$inv_manufacturer->id.' ></option>';
            }
        }
        $data['result'] = 1;
        $data['data'] = $stringIds ?? "";
        return response()->json($data);
    }
    public function sizeInverter(Request $request) {
        $inverter_id = $request->inverter_id;
        $inverter = Inverter::where('id',$inverter_id)->with('InverterType')->first();
        
        $data['result'] = 1;
        $data['data'] = strtolower($inverter->InverterType->name) ?? "";
        $data['maxDuplicateString'] = $inverter->no_of_mppts * $inverter->no_of_inputs_mppt ?? "";
        return response()->json($data);
    }
    public function optimizerEdit(Request $request) {
        $optimizer_id = $request->optimizer_id;
        $stringIds = OptimizerString::where('optimizer_id',$optimizer_id)->pluck('electrical_string_id');
        $data['result'] = 1;
        $data['data'] = $stringIds ?? "";
        return response()->json($data);
    }
    public function ocpd_value(Request $request) {
        $ocpd_rating = config('constants.ocpd_rating');
        $current = $request->current;
        $largeValue = $this->nextLargestValueCurrent($ocpd_rating, $current);
        $data['result'] = 1;
        $data['data'] = $largeValue;
        return $data;
    }
    function nextLargestValueCurrent(array $arr, $val)
    {
        $last = null;
        foreach ($arr as $key => $value) {
            if($value > $val) {
                return $value;
            }
            $last = $value;
        }
        return $last;
    }


///////////////////////////////////////////////////////////////////////
}


