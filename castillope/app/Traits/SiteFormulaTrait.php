<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Config;
use Artisan;
trait SiteFormulaTrait {

    public function srslCalculation(Request $request)
    {
        $ground_snow_load = $request->ground_snow_load;
        $ce = $request->ce;
        $ct = $request->ct;
        $is = $request->is;
        $cs = $request->cs;
        $srsl =  $ground_snow_load * $ce * $ct * $is * $cs;
        $srsl =  round($srsl,4);

        $data['result'] = 1;
        $data['data'] = $srsl;
        return response()->json($data);

    }
    public function nws($ultimate_wind_speed)
    {
        $uws = $ultimate_wind_speed * sqrt(0.6);
        $nws = round($uws,4);

        return $nws;
    }
    public function nwsCalculation(Request $request)
    {
        $nws = $this->nws($request->ultimate_wind_speed);

        $data['result'] = 1;
        $data['data'] = $nws;
        return response()->json($data);
    }
    public function moduleAreaCalculation(Request $request)
    {
        $module_length = $request->module_length;
        $module_width = $request->module_width;
        $module_area = ($module_length * $module_width) / 144;
        $module_area = round($module_area,4);

        $data['result'] = 1;
        $data['data'] = $module_area;
        return response()->json($data);
    }
    public function moduleRoofSlopeDegree(Request $request)
    {
        $roof_slope = $request->roof_slope;
        $roof_slope_degree = atan($roof_slope/12) * 180 / pi();
        $roof_slope_degree = round($roof_slope_degree,4);

        $data['result'] = 1;
        $data['data'] = $roof_slope_degree;
        return response()->json($data);
    }
    public function moduleKe(Request $request)
    {
        $ground_elevation = $request->ground_elevation;
        $ke = exp((-0.0000362*$ground_elevation));
        $ke = round($ke,4);

        $data['result'] = 1;
        $data['data'] = $ke;
        return response()->json($data);
    }
    public function moduleKz(Request $request)
    {
        $mean_roof_height = $request->mean_roof_height;
        $exposure_category = $request->exposure_category;
        $kz = 0;
        if($exposure_category != '')
            {
                $exposures = config('constants.exposures');
                $exp_val1 = $exposures[$exposure_category][1];
                $exp_val0 = $exposures[$exposure_category][0];
                $p2 = 2 / $exp_val0;
                if($mean_roof_height < 15)
                {
                    $p1 = 15 / $exp_val1;
                    $power_val = pow($p1,$p2);
                }
                else
                {
                    $p1 = $mean_roof_height / $exp_val1;
                    $power_val = pow($p1,$p2);
                }
                $kz = 2.01 * $power_val;
                $kz = round($kz,4);

            }

        $data['result'] = 1;
        $data['data'] = $kz;
        return response()->json($data);
    }
    public function moduleKzRoofInfo($roof_height,$exposure_category)
    {
        $kz = 0;
        if($exposure_category != '')
        {
            $exposures = config('constants.exposures');
            $exp_val1 = $exposures[$exposure_category][1];
            $exp_val0 = $exposures[$exposure_category][0];
            $p2 = 2 / $exp_val0;
            if($roof_height < 15) {
                $p1 = 15 / $exp_val1;
                $power_val = pow($p1,$p2);
            }
            else {
                 $p1 = $roof_height / $exp_val1;
                 $power_val = pow($p1,$p2);
            }
            $kz = 2.01 * $power_val;
            $kz = round($kz,4);

        }
        for($i = 0;$i <= 6;$i++)
        {
        $kz_val[] = $kz ?? 0;
        }


        return $kz_val;
    }
    public function widthOfPressureCoefficient($mean_roof_height,$roof_length,$roof_width,$roof_slope_degree,$hvhz,$roof_type)
    {
        $mean_roof_height = (float) $mean_roof_height ?? 0;
        $roof_length = (float) $roof_length ?? 0;
        $roof_width = (float) $roof_width ?? 0;
        $roof_slope_degree = (float) $roof_slope_degree ?? 0;
        $hvhz = $hvhz ?? "";

         // For zone
        // =IF(H8>7,IF(D19="NO",4&" FT",MAX(MIN(0.4*D6,0.1*MIN(D7:E8)),0.04*MIN(D7:E8),3)&" FT"), (D6*0.6)&" FT")

        if($roof_length < $roof_width)
        $wp1 = $roof_length;
        else
        $wp1 = $roof_width;
        $wp2 = $mean_roof_height;

        $wp1_val = ($wp1 * 10)/100;
        $wp2_val = ($wp2 * 40)/100;

        // if($roof_slope_degree > 7)
        // {
        //     if($hvhz !='' && $hvhz == "NO")
        //     $zone1 = "4FT";
        //     else
        //     $zone1 = max(min(0.4*$mean_roof_height,0.1*min($roof_length,$roof_width)),0.04*min($roof_length,$roof_width),3) ."FT";
        // }
        // else
        //     $zone1 = ($mean_roof_height*0.6) . " FT";

        if($roof_type == 'FLAT') {
            $zone1 = ($mean_roof_height*0.6) . " FT";
        }
        else {
            if($hvhz !='' && $hvhz == "NO") {
                $zone1 = "4FT";
            }
            else {
                $zone1 = max(min(0.4*$mean_roof_height,0.1*min($roof_length,$roof_width)),0.04*min($roof_length,$roof_width),3) ."FT";
            }
        }

        // =IF(H8<7,((0.6*D6)& " FT"), "N/A")
        // if($roof_slope_degree < 7)
        //     $zone2 = (0.6 * $mean_roof_height) . "FT";
        // else
        //     $zone2 = "N/A";

        if($roof_type == 'FLAT') {
            $zone2 = "N/A";
        }
        else {
            $zone2 = (0.6 * $mean_roof_height) . "FT";
        }

        //    =IF(H8<7,((0.2*D6)& " FT"), "N/A")
        // if($roof_slope_degree < 7)
        //     $zone3 = (0.2 * $mean_roof_height) . "FT";
        // else
        //     $zone3 = "N/A";

        if($roof_type == 'FLAT') {
            $zone3 = "N/A";
        }
        else {
            $zone3 = (0.2 * $mean_roof_height) . "FT";
        }


        $data['result'] = 1;
        $data['wp1'] = $wp1;
        $data['wp2'] = $wp2;
        $data['wp1_val'] = $wp1_val;
        $data['wp2_val'] = $wp2_val;
        $data['zone1'] = $zone1;
        $data['zone2'] = $zone2;
        $data['zone3'] = $zone3;
        return $data;
    }
    public function widthOfPressureCoefficient_7_10($mean_roof_height,$roof_length,$roof_width,$roof_slope_degree,$hvhz,$roof_type)
    {
        $mean_roof_height = (float) $mean_roof_height ?? 0;
        $roof_length = (float) $roof_length ?? 0;
        $roof_width = (float) $roof_width ?? 0;
        $roof_slope_degree = (float) $roof_slope_degree ?? 0;
        $hvhz = $hvhz ?? "";

         // For zone
        // =IF(H8>7,IF(D19="NO",4&" FT",MAX(MIN(0.4*D6,0.1*MIN(D7:E8)),0.04*MIN(D7:E8),3)&"FT"), (D6*0.6)&" FT")
          // =                          MAX(MIN(0.4*D6,0.1*MIN(D7:E8)),0.04*MIN(D7:E8),3)&"FT"


        if($roof_length < $roof_width)
        $wp1 = $roof_length;
        else
        $wp1 = $roof_width;
        $wp2 = $mean_roof_height;

        $wp1_val = ($wp1 * 10)/100;
        $wp2_val = ($wp2 * 40)/100;
        if($hvhz !='' && $hvhz == "NO") {
            $zone1 = "4FT";
        }
        else {
            $zone1 = max(min(0.4*$mean_roof_height,0.1*min($roof_length,$roof_width)),0.04*min($roof_length,$roof_width),3) ."FT";
        }
        $zone2 = "N/A";
        $zone3 = "N/A";

        $data['result'] = 1;
        $data['wp1'] = $wp1;
        $data['wp2'] = $wp2;
        $data['wp1_val'] = $wp1_val;
        $data['wp2_val'] = $wp2_val;
        $data['zone1'] = $zone1;
        $data['zone2'] = $zone2;
        $data['zone3'] = $zone3;
        return $data;
    }
    public function calculate_asce_neg_gab($module_area)
    {

        $constants = config('constants');
        $asce = config('constants.asce');



            /************************** first row
             * ************************
             */
            $b13       = $b[] = $asce['neg_gab']['roof_slope'][0][0]['1'];
            $p13       = $p[]  = $asce['neg_gab']['roof_slope'][0][2]['1'];

            if($b13 == "X" || $b13 == "x") {
                $i13 = "X";
            }
            else{
                $i13 = $b13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($b13-$p13));
            }

            $constants['asce']['neg_gab']['roof_slope'][0][1]['1'] = $i13;
            $constants['asce']['neg_gab']['roof_slope'][0][1]['1dash'] = 0.9;

            $d13  = $asce['neg_gab']['roof_slope'][0][0]['2e'];
            $r13  = $asce['neg_gab']['roof_slope'][0][2]['2e'];

            if($d13 == "X" || $d13 == "x") {
                $k13 = "X";
            }
            else{
                $k13 = $d13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($d13-$r13));
            }

            $constants['asce']['neg_gab']['roof_slope'][0][1]['2e'] = $k13;
            // --------------------------

            $e13  = $asce['neg_gab']['roof_slope'][0][0]['2n'];
            $s13  = $asce['neg_gab']['roof_slope'][0][2]['2n'];

            if($e13 == "X" || $e13 == "x") {
                $l13 = "X";
            }
            else{
                $l13 = $e13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($e13-$s13));
            }

            $constants['asce']['neg_gab']['roof_slope'][0][1]['2n'] = $l13;
            // --------------------------

            $f13  = $asce['neg_gab']['roof_slope'][0][0]['2r'];
            $t13  = $asce['neg_gab']['roof_slope'][0][2]['2r'];

            if($f13 == "X" || $f13 == "x") {
                $m13 = "X";
            }
            else{
                $m13 = $f13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($f13-$t13));
            }

            $constants['asce']['neg_gab']['roof_slope'][0][1]['2r'] = $m13;
            // --------------------------
            $g13  = $asce['neg_gab']['roof_slope'][0][0]['3e'];
            $u13  = $asce['neg_gab']['roof_slope'][0][2]['3e'];

            if($g13 == "X" || $g13 == "x") {
                $n13 = "X";
            }
            else{
                $n13 = $g13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($g13-$u13));
            }

            $constants['asce']['neg_gab']['roof_slope'][0][1]['3e'] = $n13;

            // --------------------------
            $h13  = $asce['neg_gab']['roof_slope'][0][0]['3r'];
            $v13  = $asce['neg_gab']['roof_slope'][0][2]['3r'];

            if($h13 == "X" || $h13 == "x") {
                $o13 = "X";
            }
            else{
                $o13 = $h13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($h13-$v13));
            }

            $constants['asce']['neg_gab']['roof_slope'][0][1]['3r'] = $o13;


            /************************** second row
             * ************************
             */

            $b14       = $b[] = $asce['neg_gab']['roof_slope'][1][0]['1'];
            $p14       = $p[]  = $asce['neg_gab']['roof_slope'][1][2]['1'];

            if($b14 == "X" || $b14 == "x") {
                $i14 = "X";
            }
            else{
                $i14 = $b14+(((LOG($module_area,10)-LOG(20,10))/(LOG(20,10)-LOG(100,10)))*($b14-$p14));
            }
            // -------------------------------
            $constants['asce']['neg_gab']['roof_slope'][1][1]['1'] = $i14;

            $c14       = $b[] = $asce['neg_gab']['roof_slope'][1][0]['1dash'];
            $q14       = $p[]  = $asce['neg_gab']['roof_slope'][1][2]['1dash'];

            if($c14 == "X" || $c14 == "x") {
                $j14 = "X";
            }
            else{
                $j14 = $c14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($c14-$q14));
            }

            $constants['asce']['neg_gab']['roof_slope'][1][1]['1dash'] = $j14;

            // -------------------------------

            $d14  = $asce['neg_gab']['roof_slope'][1][0]['2e'];
            $r14  = $asce['neg_gab']['roof_slope'][1][2]['2e'];

            if($d14 == "X" || $d14 == "x") {
                $k14 = "X";
            }
            else{
                $k14 = $d14+(((LOG($module_area,10)-LOG(20,10))/(LOG(20,10)-LOG(100,10)))*($d14-$r14));
            }

            $constants['asce']['neg_gab']['roof_slope'][1][1]['2e'] = $k14;
            // --------------------------

            $e14  = $asce['neg_gab']['roof_slope'][1][0]['2n'];
            $s14  = $asce['neg_gab']['roof_slope'][1][2]['2n'];

            if($e14 == "X" || $e14 == "x") {
                $l14 = "X";
            }
            else{
                $l14 = $e14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(250,10)))*($e14-$s14));
            }

            $constants['asce']['neg_gab']['roof_slope'][1][1]['2n'] = $l14;
            // --------------------------

            $f14  = $asce['neg_gab']['roof_slope'][1][0]['2r'];
            $t14  = $asce['neg_gab']['roof_slope'][1][2]['2r'];

            if($f14 == "X" || $f14 == "x") {
                $m14 = "X";
            }
            else{
                $m14 = $f14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(250,10)))*($f14-$t14));
            }

            $constants['asce']['neg_gab']['roof_slope'][1][1]['2r'] = $m14;
            // --------------------------
            $g14  = $asce['neg_gab']['roof_slope'][1][0]['3e'];
            $u14  = $asce['neg_gab']['roof_slope'][1][2]['3e'];

            if($g14 == "X" || $g14 == "x") {
                $n14 = "X";
            }
            else{
                $n14 = $g14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(250,10)))*($g14-$u14));
            }

            $constants['asce']['neg_gab']['roof_slope'][1][1]['3e'] = $n14;

            // --------------------------
            $h14  = $asce['neg_gab']['roof_slope'][1][0]['3r'];
            $v14  = $asce['neg_gab']['roof_slope'][1][2]['3r'];

            if($h14 == "X" || $h14 == "x") {
                $o14 = "X";
            }
            else{
                $o14 = $h14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($h14-$v14));
            }

            $constants['asce']['neg_gab']['roof_slope'][1][1]['3r'] = $o14;

            /************************** 3rd row
             * ************************
             */

            $b15 = $asce['neg_gab']['roof_slope'][2][0]['1'];
            $p15 = $asce['neg_gab']['roof_slope'][2][2]['1'];

            if($b15 == "X" || $b15 == "x") {
                $i15 = "X";
            }
            else{
                $i15 = $b15+(((LOG($module_area,10)-LOG(20,10))/(LOG(20,10)-LOG(300,10)))*($b15-$p15));
            }
            // -------------------------------
            $constants['asce']['neg_gab']['roof_slope'][2][1]['1'] = $i15;

            $c15       = $b[] = $asce['neg_gab']['roof_slope'][2][0]['1dash'];
            $q15       = $p[]  = $asce['neg_gab']['roof_slope'][2][2]['1dash'];

            if($c15 == "X" || $c15 == "x") {
                $j15 = "X";
            }
            else{
                $j15 = $c15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($c15-$q15));
            }

            $constants['asce']['neg_gab']['roof_slope'][2][1]['1dash'] = $j15;

            // -------------------------------

            $d15  = $asce['neg_gab']['roof_slope'][2][0]['2e'];
            $r15  = $asce['neg_gab']['roof_slope'][2][2]['2e'];

            if($d15 == "X" || $d15 == "x") {
                $k15 = "X";
            }
            else{
                $k15 = $d15+(((LOG($module_area,10)-LOG(20,10))/(LOG(20,10)-LOG(300,10)))*($d15-$r15));
            }

            $constants['asce']['neg_gab']['roof_slope'][2][1]['2e'] = $k15;
            // --------------------------

            $e15  = $asce['neg_gab']['roof_slope'][2][0]['2n'];
            $s15  = $asce['neg_gab']['roof_slope'][2][2]['2n'];

            if($e15 == "X" || $e15 == "x") {
                $l15 = "X";
            }
            else{
                $l15 = $e15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(150,10)))*($e15-$s15));
            }

            $constants['asce']['neg_gab']['roof_slope'][2][1]['2n'] = $l15;
            // --------------------------

            $f15  = $asce['neg_gab']['roof_slope'][2][0]['2r'];
            $t15  = $asce['neg_gab']['roof_slope'][2][2]['2r'];

            if($f15 == "X" || $f15 == "x") {
                $m15 = "X";
            }
            else{
                $m15 = $f15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(150,10)))*($f15-$t15));
            }

            $constants['asce']['neg_gab']['roof_slope'][2][1]['2r'] = $m15;
            // --------------------------
            $g15  = $asce['neg_gab']['roof_slope'][2][0]['3e'];
            $u15  = $asce['neg_gab']['roof_slope'][2][2]['3e'];

            if($g15 == "X" || $g15 == "x") {
                $n15 = "X";
            }
            else{
                $n15 = $g15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(150,10)))*($g15-$u15));
            }

            $constants['asce']['neg_gab']['roof_slope'][2][1]['3e'] = $n15;

            // --------------------------
            $h15  = $asce['neg_gab']['roof_slope'][2][0]['3r'];
            $v15  = $asce['neg_gab']['roof_slope'][2][2]['3r'];

            if($h15 == "X" || $h15 == "x") {
                $o15 = "X";
            }
            else{
                $o15 = $h15+(((LOG($module_area,10)-LOG(4,10))/(LOG(4,10)-LOG(50,10)))*($h15-$v15));
            }

            $constants['asce']['neg_gab']['roof_slope'][2][1]['3r'] = $o15;

            /************************** 4th row
             * ************************
             */

            $b16 = $asce['neg_gab']['roof_slope'][3][0]['1'];
            $p16 = $asce['neg_gab']['roof_slope'][3][2]['1'];

            if($b16 == "X" || $b16 == "x") {
                $i16 = "X";
            }
            else{
                $i16 = $b16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($b16-$p16));
            }
            // -------------------------------
            $constants['asce']['neg_gab']['roof_slope'][3][1]['1'] = $i16;

            $c16       = $b[] = $asce['neg_gab']['roof_slope'][3][0]['1dash'];
            $q16       = $p[]  = $asce['neg_gab']['roof_slope'][3][2]['1dash'];

            if($c16 == "X" || $c16 == "x") {
                $j16 = "X";
            }
            else{
                $j16 = $c16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($c16-$q16));
            }

            $constants['asce']['neg_gab']['roof_slope'][3][1]['1dash'] = $j16;

            // -------------------------------

            $d16  = $asce['neg_gab']['roof_slope'][3][0]['2e'];
            $r16  = $asce['neg_gab']['roof_slope'][3][2]['2e'];

            if($d16 == "X" || $d16 == "x") {
                $k16 = "X";
            }
            else{
                $k16 = $d16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($d16-$r16));
            }

            $constants['asce']['neg_gab']['roof_slope'][3][1]['2e'] = $k16;
            // --------------------------

            $e16  = $asce['neg_gab']['roof_slope'][3][0]['2n'];
            $s16  = $asce['neg_gab']['roof_slope'][3][2]['2n'];

            if($e16 == "X" || $e16 == "x") {
                $l16 = "X";
            }
            else{
                $l16 = $e16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(200,10)))*($e16-$s16));
            }

            $constants['asce']['neg_gab']['roof_slope'][3][1]['2n'] = $l16;
            // --------------------------

            $f16  = $asce['neg_gab']['roof_slope'][3][0]['2r'];
            $t16  = $asce['neg_gab']['roof_slope'][3][2]['2r'];

            if($f16 == "X" || $f16 == "x") {
                $m16 = "X";
            }
            else{
                $m16 = $f16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($f16-$t16));
            }

            $constants['asce']['neg_gab']['roof_slope'][3][1]['2r'] = $m16;
            // --------------------------
            $g16  = $asce['neg_gab']['roof_slope'][3][0]['3e'];
            $u16  = $asce['neg_gab']['roof_slope'][3][2]['3e'];

            if($g16 == "X" || $g16 == "x") {
                $n16 = "X";
            }
            else{
                $n16 = $g16+(((LOG($module_area,10)-LOG(2,10))/(LOG(2,10)-LOG(300,10)))*($g16-$u16));
            }

            $constants['asce']['neg_gab']['roof_slope'][3][1]['3e'] = $n16;

            // --------------------------
            $h16  = $asce['neg_gab']['roof_slope'][3][0]['3r'];
            $v16  = $asce['neg_gab']['roof_slope'][3][2]['3r'];

            if($h16 == "X" || $h16 == "x") {
                $o16 = "X";
            }
            else{
                $o16 = $h16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(200,10)))*($h16-$v16));
            }

            $constants['asce']['neg_gab']['roof_slope'][3][1]['3r'] = $o16;




            Config::set('constants.asce',$constants['asce']);

            Artisan::call('config:clear');




        $constants = config('constants');


    }

    public function calculate_asce_pos_gab($module_area)
    {

        $constants = config('constants');
        $asce = config('constants.asce');
        // echo "<pre>";
        // print_r($constants);exit;


            /************************** first row
             * ************************
             */
            $b13 = $asce['pos_gab']['roof_slope'][0][0]['1'];
            $p13 = $asce['pos_gab']['roof_slope'][0][2]['1'];

            if($b13 == "X" || $b13 == "x") {
                $i13 = "X";
            }
            else{
                $i13 = $b13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($b13-$p13));
            }

            $constants['asce']['pos_gab']['roof_slope'][0][1]['1'] = $i13;
            // --------------------------
            $c13       = $asce['pos_gab']['roof_slope'][0][0]['1dash'];
            $q13        = $asce['pos_gab']['roof_slope'][0][2]['1dash'];

            if($c13 == "X" || $c13 == "x") {
                $j13 = "X";
            }
            else{
                $j13 = $c13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($c13-$q13));
            }


            $constants['asce']['pos_gab']['roof_slope'][0][1]['1dash'] = $j13;

            // --------------------------


            $d13  = $asce['pos_gab']['roof_slope'][0][0]['2e'];
            $r13  = $asce['pos_gab']['roof_slope'][0][2]['2e'];

            if($d13 == "X" || $d13 == "x") {
                $k13 = "X";
            }
            else{
                $k13 = $d13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($d13-$r13));
            }

            $constants['asce']['pos_gab']['roof_slope'][0][1]['2e'] = $k13;
            // --------------------------

            $e13  = $asce['pos_gab']['roof_slope'][0][0]['2n'];
            $s13  = $asce['pos_gab']['roof_slope'][0][2]['2n'];

            if($e13 == "X" || $e13 == "x") {
                $l13 = "X";
            }
            else{
                $l13 = $e13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($e13-$s13));
            }

            $constants['asce']['pos_gab']['roof_slope'][0][1]['2n'] = $l13;
            // --------------------------

            $f13  = $asce['pos_gab']['roof_slope'][0][0]['2r'];
            $t13  = $asce['pos_gab']['roof_slope'][0][2]['2r'];

            if($f13 == "X" || $f13 == "x") {
                $m13 = "X";
            }
            else{
                $m13 = $f13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($f13-$t13));
            }

            $constants['asce']['pos_gab']['roof_slope'][0][1]['2r'] = $m13;
            // --------------------------
            $g13  = $asce['pos_gab']['roof_slope'][0][0]['3e'];
            $u13  = $asce['pos_gab']['roof_slope'][0][2]['3e'];

            if($g13 == "X" || $g13 == "x") {
                $n13 = "X";
            }
            else{
                $n13 = $g13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($g13-$u13));
            }

            $constants['asce']['pos_gab']['roof_slope'][0][1]['3e'] = $n13;

            // --------------------------
            $h13  = $asce['pos_gab']['roof_slope'][0][0]['3r'];
            $v13  = $asce['pos_gab']['roof_slope'][0][2]['3r'];

            if($h13 == "X" || $h13 == "x") {
                $o13 = "X";
            }
            else{
                $o13 = $h13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($h13-$v13));
            }

            $constants['asce']['pos_gab']['roof_slope'][0][1]['3r'] = $o13;


            /************************** second row
             * ************************
             */

            $b14       = $asce['pos_gab']['roof_slope'][1][0]['1'];
            $p14       = $asce['pos_gab']['roof_slope'][1][2]['1'];

            if($b14 == "X" || $b14 == "x") {
                $i14 = "X";
            }
            else{
                $i14 = $b14+(((LOG($module_area,10)-LOG(2,10))/(LOG(2,10)-LOG(100,10)))*($b14-$p14));
            }
            // -------------------------------
            $constants['asce']['pos_gab']['roof_slope'][1][1]['1'] = $i14;

            $c14       = $asce['pos_gab']['roof_slope'][1][0]['1dash'];
            $q14       = $asce['pos_gab']['roof_slope'][1][2]['1dash'];

            if($c14 == "X" || $c14 == "x") {
                $j14 = "X";
            }
            else{
                $j14 = $c14+(((LOG($module_area,10)-LOG(2,10))/(LOG(2,10)-LOG(100,10)))*($c14-$q14));
            }

            $constants['asce']['pos_gab']['roof_slope'][1][1]['1dash'] = $j14;

            // -------------------------------

            $d14  = $asce['pos_gab']['roof_slope'][1][0]['2e'];
            $r14  = $asce['pos_gab']['roof_slope'][1][2]['2e'];

            if($d14 == "X" || $d14 == "x") {
                $k14 = "X";
            }
            else{
                $k14 = $d14+(((LOG($module_area,10)-LOG(2,10))/(LOG(2,10)-LOG(100,10)))*($d14-$r14));
            }

            $constants['asce']['pos_gab']['roof_slope'][1][1]['2e'] = $k14;
            // --------------------------

            $e14  = $asce['pos_gab']['roof_slope'][1][0]['2n'];
            $s14  = $asce['pos_gab']['roof_slope'][1][2]['2n'];

            if($e14 == "X" || $e14 == "x") {
                $l14 = "X";
            }
            else{
                $l14 = $e14+(((LOG($module_area,10)-LOG(2,10))/(LOG(2,10)-LOG(100,10)))*($e14-$s14));
            }

            $constants['asce']['pos_gab']['roof_slope'][1][1]['2n'] = $l14;
            // --------------------------

            $f14  = $asce['pos_gab']['roof_slope'][1][0]['2r'];
            $t14  = $asce['pos_gab']['roof_slope'][1][2]['2r'];

            if($f14 == "X" || $f14 == "x") {
                $m14 = "X";
            }
            else{
                $m14 = $f14+(((LOG($module_area,10)-LOG(2,10))/(LOG(2,10)-LOG(100,10)))*($f14-$t14));
            }

            $constants['asce']['pos_gab']['roof_slope'][1][1]['2r'] = $m14;
            // --------------------------
            $g14  = $asce['pos_gab']['roof_slope'][1][0]['3e'];
            $u14  = $asce['pos_gab']['roof_slope'][1][2]['3e'];

            if($g14 == "X" || $g14 == "x") {
                $n14 = "X";
            }
            else{
                $n14 = $g14+(((LOG($module_area,10)-LOG(2,10))/(LOG(2,10)-LOG(100,10)))*($g14-$u14));
            }

            $constants['asce']['pos_gab']['roof_slope'][1][1]['3e'] = $n14;

            // --------------------------
            $h14  = $asce['pos_gab']['roof_slope'][1][0]['3r'];
            $v14  = $asce['pos_gab']['roof_slope'][1][2]['3r'];

            if($h14 == "X" || $h14 == "x") {
                $o14 = "X";
            }
            else{
                $o14 = $h14+(((LOG($module_area,10)-LOG(2,10))/(LOG(2,10)-LOG(100,10)))*($h14-$v14));
            }

            $constants['asce']['pos_gab']['roof_slope'][1][1]['3r'] = $o14;

            /************************** 3rd row
             * ************************
             */

            $b15 = $asce['pos_gab']['roof_slope'][2][0]['1'];
            $p15 = $asce['pos_gab']['roof_slope'][2][2]['1'];

            if($b15 == "X" || $b15 == "x") {
                $i15 = "X";
            }
            else{
                $i15 = $b15+(((LOG($module_area,10)-LOG(2,10))/(LOG(2,10)-LOG(100,10)))*($b15-$p15));
            }
            // -------------------------------
            $constants['asce']['pos_gab']['roof_slope'][2][1]['1'] = $i15;

            $c15       = $b[] = $asce['pos_gab']['roof_slope'][2][0]['1dash'];
            $q15       = $p[]  = $asce['pos_gab']['roof_slope'][2][2]['1dash'];

            if($c15 == "X" || $c15 == "x") {
                $j15 = "X";
            }
            else{
                $j15 = $c15+(((LOG($module_area,10)-LOG(2,10))/(LOG(2,10)-LOG(100,10)))*($c15-$q15));
            }

            $constants['asce']['pos_gab']['roof_slope'][2][1]['1dash'] = $j15;

            // -------------------------------

            $d15  = $asce['pos_gab']['roof_slope'][2][0]['2e'];
            $r15  = $asce['pos_gab']['roof_slope'][2][2]['2e'];

            if($d15 == "X" || $d15 == "x") {
                $k15 = "X";
            }
            else{
                $k15 = $d15+(((LOG($module_area,10)-LOG(2,10))/(LOG(2,10)-LOG(100,10)))*($d15-$r15));
            }

            $constants['asce']['pos_gab']['roof_slope'][2][1]['2e'] = $k15;
            // --------------------------

            $e15  = $asce['pos_gab']['roof_slope'][2][0]['2n'];
            $s15  = $asce['pos_gab']['roof_slope'][2][2]['2n'];

            if($e15 == "X" || $e15 == "x") {
                $l15 = "X";
            }
            else{
                $l15 = $e15+(((LOG($module_area,10)-LOG(2,10))/(LOG(2,10)-LOG(100,10)))*($e15-$s15));
            }

            $constants['asce']['pos_gab']['roof_slope'][2][1]['2n'] = $l15;
            // --------------------------

            $f15  = $asce['pos_gab']['roof_slope'][2][0]['2r'];
            $t15  = $asce['pos_gab']['roof_slope'][2][2]['2r'];

            if($f15 == "X" || $f15 == "x") {
                $m15 = "X";
            }
            else{
                $m15 = $f15+(((LOG($module_area,10)-LOG(2,10))/(LOG(2,10)-LOG(100,10)))*($f15-$t15));
            }

            $constants['asce']['pos_gab']['roof_slope'][2][1]['2r'] = $m15;
            // --------------------------
            $g15  = $asce['pos_gab']['roof_slope'][2][0]['3e'];
            $u15  = $asce['pos_gab']['roof_slope'][2][2]['3e'];

            if($g15 == "X" || $g15 == "x") {
                $n15 = "X";
            }
            else{
                $n15 = $g15+(((LOG($module_area,10)-LOG(2,10))/(LOG(2,10)-LOG(100,10)))*($g15-$u15));
            }

            $constants['asce']['pos_gab']['roof_slope'][2][1]['3e'] = $n15;

            // --------------------------
            $h15  = $asce['pos_gab']['roof_slope'][2][0]['3r'];
            $v15  = $asce['pos_gab']['roof_slope'][2][2]['3r'];

            if($h15 == "X" || $h15 == "x") {
                $o15 = "X";
            }
            else{
                $o15 = $h15+(((LOG($module_area,10)-LOG(2,10))/(LOG(2,10)-LOG(100,10)))*($h15-$v15));
            }

            $constants['asce']['pos_gab']['roof_slope'][2][1]['3r'] = $o15;

            /************************** 4th row
             * ************************
             */

            $b16 = $asce['pos_gab']['roof_slope'][3][0]['1'];
            $p16 = $asce['pos_gab']['roof_slope'][3][2]['1'];

            if($b16 == "X" || $b16 == "x") {
                $i16 = "X";
            }
            else{
                $i16 = $b16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($b16-$p16));
            }
            // -------------------------------
            $constants['asce']['pos_gab']['roof_slope'][3][1]['1'] = $i16;

            $c16       = $b[] = $asce['pos_gab']['roof_slope'][3][0]['1dash'];
            $q16       = $p[]  = $asce['pos_gab']['roof_slope'][3][2]['1dash'];

            if($c16 == "X" || $c16 == "x") {
                $j16 = "X";
            }
            else{
                $j16 = $c16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($c16-$q16));
            }

            $constants['asce']['pos_gab']['roof_slope'][3][1]['1dash'] = $j16;

            // -------------------------------

            $d16  = $asce['pos_gab']['roof_slope'][3][0]['2e'];
            $r16  = $asce['pos_gab']['roof_slope'][3][2]['2e'];

            if($d16 == "X" || $d16 == "x") {
                $k16 = "X";
            }
            else{
                $k16 = $d16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($d16-$r16));
            }

            $constants['asce']['pos_gab']['roof_slope'][3][1]['2e'] = $k16;
            // --------------------------

            $e16  = $asce['pos_gab']['roof_slope'][3][0]['2n'];
            $s16  = $asce['pos_gab']['roof_slope'][3][2]['2n'];

            if($e16 == "X" || $e16 == "x") {
                $l16 = "X";
            }
            else{
                $l16 = $e16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($e16-$s16));
            }

            $constants['asce']['pos_gab']['roof_slope'][3][1]['2n'] = $l16;
            // --------------------------

            $f16  = $asce['pos_gab']['roof_slope'][3][0]['2r'];
            $t16  = $asce['pos_gab']['roof_slope'][3][2]['2r'];

            if($f16 == "X" || $f16 == "x") {
                $m16 = "X";
            }
            else{
                $m16 = $f16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($f16-$t16));
            }

            $constants['asce']['pos_gab']['roof_slope'][3][1]['2r'] = $m16;
            // --------------------------
            $g16  = $asce['pos_gab']['roof_slope'][3][0]['3e'];
            $u16  = $asce['pos_gab']['roof_slope'][3][2]['3e'];

            if($g16 == "X" || $g16 == "x") {
                $n16 = "X";
            }
            else{
                $n16 = $g16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($g16-$u16));
            }

            $constants['asce']['pos_gab']['roof_slope'][3][1]['3e'] = $n16;

            // --------------------------
            $h16  = $asce['pos_gab']['roof_slope'][3][0]['3r'];
            $v16  = $asce['pos_gab']['roof_slope'][3][2]['3r'];

            if($h16 == "X" || $h16 == "x") {
                $o16 = "X";
            }
            else{
                $o16 = $h16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($h16-$v16));
            }

            $constants['asce']['pos_gab']['roof_slope'][3][1]['3r'] = $o16;




            Config::set('constants.asce',$constants['asce']);

            Artisan::call('config:clear');




        $constants = config('constants');


    }
    public function calculate_asce_neg_hip($module_area)
    {

        $constants = config('constants');
        $asce = config('constants.asce');



            /************************** first row
             * ************************
             */
            $b13       = $b[] = $asce['neg_hip']['roof_slope'][0][0]['1'];
            $p13       = $p[]  = $asce['neg_hip']['roof_slope'][0][2]['1'];

            if($b13 == "X" || $b13 == "x") {
                $i13 = "X";
            }
            else{
                $i13 = $b13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($b13-$p13));
            }

            $constants['asce']['neg_hip']['roof_slope'][0][1]['1'] = $i13;
            $constants['asce']['neg_hip']['roof_slope'][0][1]['1dash'] = 0.9;

            $d13  = $asce['neg_hip']['roof_slope'][0][0]['2e'];
            $r13  = $asce['neg_hip']['roof_slope'][0][2]['2e'];

            if($d13 == "X" || $d13 == "x") {
                $k13 = "X";
            }
            else{
                $k13 = $d13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($d13-$r13));
            }

            $constants['asce']['neg_hip']['roof_slope'][0][1]['2e'] = $k13;
            // --------------------------

            $e13  = $asce['neg_hip']['roof_slope'][0][0]['2n'];
            $s13  = $asce['neg_hip']['roof_slope'][0][2]['2n'];

            if($e13 == "X" || $e13 == "x") {
                $l13 = "X";
            }
            else{
                $l13 = $e13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($e13-$s13));
            }

            $constants['asce']['neg_hip']['roof_slope'][0][1]['2n'] = $l13;
            // --------------------------

            $f13  = $asce['neg_hip']['roof_slope'][0][0]['2r'];
            $t13  = $asce['neg_hip']['roof_slope'][0][2]['2r'];

            if($f13 == "X" || $f13 == "x") {
                $m13 = "X";
            }
            else{
                $m13 = $f13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($f13-$t13));
            }

            $constants['asce']['neg_hip']['roof_slope'][0][1]['2r'] = $m13;
            // --------------------------
            $g13  = $asce['neg_hip']['roof_slope'][0][0]['3e'];
            $u13  = $asce['neg_hip']['roof_slope'][0][2]['3e'];

            if($g13 == "X" || $g13 == "x") {
                $n13 = "X";
            }
            else{
                $n13 = $g13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($g13-$u13));
            }

            $constants['asce']['neg_hip']['roof_slope'][0][1]['3e'] = $n13;

            // --------------------------
            $h13  = $asce['neg_hip']['roof_slope'][0][0]['3r'];
            $v13  = $asce['neg_hip']['roof_slope'][0][2]['3r'];

            if($h13 == "X" || $h13 == "x") {
                $o13 = "X";
            }
            else{
                $o13 = $h13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($h13-$v13));
            }

            $constants['asce']['neg_hip']['roof_slope'][0][1]['3r'] = $o13;


            /************************** second row
             * ************************
             */

            $b14       = $b[] = $asce['neg_hip']['roof_slope'][1][0]['1'];
            $p14       = $p[]  = $asce['neg_hip']['roof_slope'][1][2]['1'];

            if($b14 == "X" || $b14 == "x") {
                $i14 = "X";
            }
            else{
                $i14 = $b14+(((LOG($module_area,10)-LOG(20,10))/(LOG(20,10)-LOG(100,10)))*($b14-$p14));
            }
            // -------------------------------
            $constants['asce']['neg_hip']['roof_slope'][1][1]['1'] = $i14;

            $c14       = $b[] = $asce['neg_hip']['roof_slope'][1][0]['1dash'];
            $q14       = $p[]  = $asce['neg_hip']['roof_slope'][1][2]['1dash'];

            if($c14 == "X" || $c14 == "x") {
                $j14 = "X";
            }
            else{
                $j14 = $c14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($c14-$q14));
            }

            $constants['asce']['neg_hip']['roof_slope'][1][1]['1dash'] = $j14;

            // -------------------------------

            $d14  = $asce['neg_hip']['roof_slope'][1][0]['2e'];
            $r14  = $asce['neg_hip']['roof_slope'][1][2]['2e'];

            if($d14 == "X" || $d14 == "x") {
                $k14 = "X";
            }
            else{
                $k14 = $d14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(200,10)))*($d14-$r14));
            }

            $constants['asce']['neg_hip']['roof_slope'][1][1]['2e'] = $k14;
            // --------------------------

            $e14  = $asce['neg_hip']['roof_slope'][1][0]['2n'];
            $s14  = $asce['neg_hip']['roof_slope'][1][2]['2n'];

            if($e14 == "X" || $e14 == "x") {
                $l14 = "X";
            }
            else{
                $l14 = $e14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($e14-$s14));
            }

            $constants['asce']['neg_hip']['roof_slope'][1][1]['2n'] = $l14;
            // --------------------------

            $f14  = $asce['neg_hip']['roof_slope'][1][0]['2r'];
            $t14  = $asce['neg_hip']['roof_slope'][1][2]['2r'];

            if($f14 == "X" || $f14 == "x") {
                $m14 = "X";
            }
            else{
                $m14 = $f14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(200,10)))*($f14-$t14));
            }

            $constants['asce']['neg_hip']['roof_slope'][1][1]['2r'] = $m14;
            // --------------------------
            $g14  = $asce['neg_hip']['roof_slope'][1][0]['3e'];
            $u14  = $asce['neg_hip']['roof_slope'][1][2]['3e'];

            if($g14 == "X" || $g14 == "x") {
                $n14 = "X";
            }
            else{
                $n14 = $g14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(200,10)))*($g14-$u14));
            }

            $constants['asce']['neg_hip']['roof_slope'][1][1]['3e'] = $n14;

            // --------------------------
            $h14  = $asce['neg_hip']['roof_slope'][1][0]['3r'];
            $v14  = $asce['neg_hip']['roof_slope'][1][2]['3r'];

            if($h14 == "X" || $h14 == "x") {
                $o14 = "X";
            }
            else{
                $o14 = $h14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($h14-$v14));
            }

            $constants['asce']['neg_hip']['roof_slope'][1][1]['3r'] = $o14;

            /************************** 3rd row
             * ************************
             */

            $b15 = $asce['neg_hip']['roof_slope'][2][0]['1'];
            $p15 = $asce['neg_hip']['roof_slope'][2][2]['1'];

            if($b15 == "X" || $b15 == "x") {
                $i15 = "X";
            }
            else{
                $i15 = $b15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($b15-$p15));
            }
            // -------------------------------
            $constants['asce']['neg_hip']['roof_slope'][2][1]['1'] = $i15;

            $c15       = $b[] = $asce['neg_hip']['roof_slope'][2][0]['1dash'];
            $q15       = $p[]  = $asce['neg_hip']['roof_slope'][2][2]['1dash'];

            if($c15 == "X" || $c15 == "x") {
                $j15 = "X";
            }
            else{
                $j15 = $c15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($c15-$q15));
            }

            $constants['asce']['neg_hip']['roof_slope'][2][1]['1dash'] = $j15;

            // -------------------------------

            $d15  = $asce['neg_hip']['roof_slope'][2][0]['2e'];
            $r15  = $asce['neg_hip']['roof_slope'][2][2]['2e'];

            if($d15 == "X" || $d15 == "x") {
                $k15 = "X";
            }
            else{
                $k15 = $d15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(200,10)))*($d15-$r15));
            }

            $constants['asce']['neg_hip']['roof_slope'][2][1]['2e'] = $k15;
            // --------------------------

            $e15  = $asce['neg_hip']['roof_slope'][2][0]['2n'];
            $s15  = $asce['neg_hip']['roof_slope'][2][2]['2n'];

            if($e15 == "X" || $e15 == "x") {
                $l15 = "X";
            }
            else{
                $l15 = $e15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($e15-$s15));
            }

            $constants['asce']['neg_hip']['roof_slope'][2][1]['2n'] = $l15;
            // --------------------------

            $f15  = $asce['neg_hip']['roof_slope'][2][0]['2r'];
            $t15  = $asce['neg_hip']['roof_slope'][2][2]['2r'];

            if($f15 == "X" || $f15 == "x") {
                $m15 = "X";
            }
            else{
                $m15 = $f15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(200,10)))*($f15-$t15));
            }

            $constants['asce']['neg_hip']['roof_slope'][2][1]['2r'] = $m15;
            // --------------------------
            $g15  = $asce['neg_hip']['roof_slope'][2][0]['3e'];
            $u15  = $asce['neg_hip']['roof_slope'][2][2]['3e'];

            if($g15 == "X" || $g15 == "x") {
                $n15 = "X";
            }
            else{
                $n15 = $g15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(200,10)))*($g15-$u15));
            }

            $constants['asce']['neg_hip']['roof_slope'][2][1]['3e'] = $n15;

            // --------------------------
            $h15  = $asce['neg_hip']['roof_slope'][2][0]['3r'];
            $v15  = $asce['neg_hip']['roof_slope'][2][2]['3r'];

            if($h15 == "X" || $h15 == "x") {
                $o15 = "X";
            }
            else{
                $o15 = $h15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($h15-$v15));
            }

            $constants['asce']['neg_hip']['roof_slope'][2][1]['3r'] = $o15;

            /************************** 4th row
             * ************************
             */

            $b16 = $asce['neg_hip']['roof_slope'][3][0]['1'];
            $p16 = $asce['neg_hip']['roof_slope'][3][2]['1'];

            if($b16 == "X" || $b16 == "x") {
                $i16 = "X";
            }
            else{
                $i16 = $b16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($b16-$p16));
            }
            // -------------------------------
            $constants['asce']['neg_hip']['roof_slope'][3][1]['1'] = $i16;

            $c16       = $b[] = $asce['neg_hip']['roof_slope'][3][0]['1dash'];
            $q16       = $p[]  = $asce['neg_hip']['roof_slope'][3][2]['1dash'];

            if($c16 == "X" || $c16 == "x") {
                $j16 = "X";
            }
            else{
                $j16 = $c16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($c16-$q16));
            }

            $constants['asce']['neg_hip']['roof_slope'][3][1]['1dash'] = $j16;

            // -------------------------------

            $d16  = $asce['neg_hip']['roof_slope'][3][0]['2e'];
            $r16  = $asce['neg_hip']['roof_slope'][3][2]['2e'];

            if($d16 == "X" || $d16 == "x") {
                $k16 = "X";
            }
            else{
                $k16 = $d16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($d16-$r16));
            }

            $constants['asce']['neg_hip']['roof_slope'][3][1]['2e'] = $k16;
            // --------------------------

            $e16  = $asce['neg_hip']['roof_slope'][3][0]['2n'];
            $s16  = $asce['neg_hip']['roof_slope'][3][2]['2n'];

            if($e16 == "X" || $e16 == "x") {
                $l16 = "X";
            }
            else{
                $l16 = $e16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($e16-$s16));
            }

            $constants['asce']['neg_hip']['roof_slope'][3][1]['2n'] = $l16;
            // --------------------------

            $f16  = $asce['neg_hip']['roof_slope'][3][0]['2r'];
            $t16  = $asce['neg_hip']['roof_slope'][3][2]['2r'];

            if($f16 == "X" || $f16 == "x") {
                $m16 = "X";
            }
            else{
                $m16 = $f16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($f16-$t16));
            }

            $constants['asce']['neg_hip']['roof_slope'][3][1]['2r'] = $m16;
            // --------------------------
            $g16  = $asce['neg_hip']['roof_slope'][3][0]['3e'];
            $u16  = $asce['neg_hip']['roof_slope'][3][2]['3e'];

            if($g16 == "X" || $g16 == "x") {
                $n16 = "X";
            }
            else{
                $n16 = $g16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($g16-$u16));
            }

            $constants['asce']['neg_hip']['roof_slope'][3][1]['3e'] = $n16;

            // --------------------------
            $h16  = $asce['neg_hip']['roof_slope'][3][0]['3r'];
            $v16  = $asce['neg_hip']['roof_slope'][3][2]['3r'];

            if($h16 == "X" || $h16 == "x") {
                $o16 = "X";
            }
            else{
                $o16 = $h16+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($h16-$v16));
            }

            $constants['asce']['neg_hip']['roof_slope'][3][1]['3r'] = $o16;




            Config::set('constants.asce',$constants['asce']);

            Artisan::call('config:clear');




        $constants = config('constants');


    }
    public function calculate_asce_pos_hip($module_area)
    {

        $constants = config('constants');
        $asce = config('constants.asce');
        // echo "<pre>";
        // print_r($constants);exit;


            /************************** first row
             * ************************
             */
            $b13 = $asce['pos_hip']['roof_slope'][0][0]['1'];
            $p13 = $asce['pos_hip']['roof_slope'][0][2]['1'];

            if($b13 == "X" || $b13 == "x") {
                $i13 = "X";
            }
            else{
                $i13 = $b13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($b13-$p13));
            }

            $constants['asce']['pos_hip']['roof_slope'][0][1]['1'] = $i13;
            // --------------------------
            $c13       = $asce['pos_hip']['roof_slope'][0][0]['1dash'];
            $q13        = $asce['pos_hip']['roof_slope'][0][2]['1dash'];

            if($c13 == "X" || $c13 == "x") {
                $j13 = "X";
            }
            else{
                $j13 = $c13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($c13-$q13));
            }


            $constants['asce']['pos_hip']['roof_slope'][0][1]['1dash'] = $j13;

            // --------------------------


            $d13  = $asce['pos_hip']['roof_slope'][0][0]['2e'];
            $r13  = $asce['pos_hip']['roof_slope'][0][2]['2e'];

            if($d13 == "X" || $d13 == "x") {
                $k13 = "X";
            }
            else{
                $k13 = $d13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($d13-$r13));
            }

            $constants['asce']['pos_hip']['roof_slope'][0][1]['2e'] = $k13;
            // --------------------------

            $e13  = $asce['pos_hip']['roof_slope'][0][0]['2n'];
            $s13  = $asce['pos_hip']['roof_slope'][0][2]['2n'];

            if($e13 == "X" || $e13 == "x") {
                $l13 = "X";
            }
            else{
                $l13 = $e13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($e13-$s13));
            }

            $constants['asce']['pos_hip']['roof_slope'][0][1]['2n'] = $l13;
            // --------------------------

            $f13  = $asce['pos_hip']['roof_slope'][0][0]['2r'];
            $t13  = $asce['pos_hip']['roof_slope'][0][2]['2r'];

            if($f13 == "X" || $f13 == "x") {
                $m13 = "X";
            }
            else{
                $m13 = $f13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($f13-$t13));
            }

            $constants['asce']['pos_hip']['roof_slope'][0][1]['2r'] = $m13;
            // --------------------------
            $g13  = $asce['pos_hip']['roof_slope'][0][0]['3e'];
            $u13  = $asce['pos_hip']['roof_slope'][0][2]['3e'];

            if($g13 == "X" || $g13 == "x") {
                $n13 = "X";
            }
            else{
                $n13 = $g13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($g13-$u13));
            }

            $constants['asce']['pos_hip']['roof_slope'][0][1]['3e'] = $n13;

            // --------------------------
            $h13  = $asce['pos_hip']['roof_slope'][0][0]['3r'];
            $v13  = $asce['pos_hip']['roof_slope'][0][2]['3r'];

            if($h13 == "X" || $h13 == "x") {
                $o13 = "X";
            }
            else{
                $o13 = $h13+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(500,10)))*($h13-$v13));
            }

            $constants['asce']['pos_hip']['roof_slope'][0][1]['3r'] = $o13;


            /************************** second row
             * ************************
             */

            $b14       = $asce['pos_hip']['roof_slope'][1][0]['1'];
            $p14       = $asce['pos_hip']['roof_slope'][1][2]['1'];

            if($b14 == "X" || $b14 == "x") {
                $i14 = "X";
            }
            else{
                $i14 = $b14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($b14-$p14));
            }
            // -------------------------------
            $constants['asce']['pos_hip']['roof_slope'][1][1]['1'] = $i14;

            $c14       = $asce['pos_hip']['roof_slope'][1][0]['1dash'];
            $q14       = $asce['pos_hip']['roof_slope'][1][2]['1dash'];

            if($c14 == "X" || $c14 == "x") {
                $j14 = "X";
            }
            else{
                $j14 = $c14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($c14-$q14));
            }

            $constants['asce']['pos_hip']['roof_slope'][1][1]['1dash'] = $j14;

            // -------------------------------

            $d14  = $asce['pos_hip']['roof_slope'][1][0]['2e'];
            $r14  = $asce['pos_hip']['roof_slope'][1][2]['2e'];

            if($d14 == "X" || $d14 == "x") {
                $k14 = "X";
            }
            else{
                $k14 = $d14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($d14-$r14));
            }

            $constants['asce']['pos_hip']['roof_slope'][1][1]['2e'] = $k14;
            // --------------------------

            $e14  = $asce['pos_hip']['roof_slope'][1][0]['2n'];
            $s14  = $asce['pos_hip']['roof_slope'][1][2]['2n'];

            if($e14 == "X" || $e14 == "x") {
                $l14 = "X";
            }
            else{
                $l14 = $e14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($e14-$s14));
            }

            $constants['asce']['pos_hip']['roof_slope'][1][1]['2n'] = $l14;
            // --------------------------

            $f14  = $asce['pos_hip']['roof_slope'][1][0]['2r'];
            $t14  = $asce['pos_hip']['roof_slope'][1][2]['2r'];

            if($f14 == "X" || $f14 == "x") {
                $m14 = "X";
            }
            else{
                $m14 = $f14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($f14-$t14));
            }

            $constants['asce']['pos_hip']['roof_slope'][1][1]['2r'] = $m14;
            // --------------------------
            $g14  = $asce['pos_hip']['roof_slope'][1][0]['3e'];
            $u14  = $asce['pos_hip']['roof_slope'][1][2]['3e'];

            if($g14 == "X" || $g14 == "x") {
                $n14 = "X";
            }
            else{
                $n14 = $g14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($g14-$u14));
            }

            $constants['asce']['pos_hip']['roof_slope'][1][1]['3e'] = $n14;

            // --------------------------
            $h14  = $asce['pos_hip']['roof_slope'][1][0]['3r'];
            $v14  = $asce['pos_hip']['roof_slope'][1][2]['3r'];

            if($h14 == "X" || $h14 == "x") {
                $o14 = "X";
            }
            else{
                $o14 = $h14+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($h14-$v14));
            }

            $constants['asce']['pos_hip']['roof_slope'][1][1]['3r'] = $o14;

            /************************** 3rd row
             * ************************
             */

            $b15 = $asce['pos_hip']['roof_slope'][2][0]['1'];
            $p15 = $asce['pos_hip']['roof_slope'][2][2]['1'];

            if($b15 == "X" || $b15 == "x") {
                $i15 = "X";
            }
            else{
                $i15 = $b15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($b15-$p15));
            }
            // -------------------------------
            $constants['asce']['pos_hip']['roof_slope'][2][1]['1'] = $i15;

            $c15       = $b[] = $asce['pos_hip']['roof_slope'][2][0]['1dash'];
            $q15       = $p[]  = $asce['pos_hip']['roof_slope'][2][2]['1dash'];

            if($c15 == "X" || $c15 == "x") {
                $j15 = "X";
            }
            else{
                $j15 = $c15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($c15-$q15));
            }

            $constants['asce']['pos_hip']['roof_slope'][2][1]['1dash'] = $j15;

            // -------------------------------

            $d15  = $asce['pos_hip']['roof_slope'][2][0]['2e'];
            $r15  = $asce['pos_hip']['roof_slope'][2][2]['2e'];

            if($d15 == "X" || $d15 == "x") {
                $k15 = "X";
            }
            else{
                $k15 = $d15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($d15-$r15));
            }

            $constants['asce']['pos_hip']['roof_slope'][2][1]['2e'] = $k15;
            // --------------------------

            $e15  = $asce['pos_hip']['roof_slope'][2][0]['2n'];
            $s15  = $asce['pos_hip']['roof_slope'][2][2]['2n'];

            if($e15 == "X" || $e15 == "x") {
                $l15 = "X";
            }
            else{
                $l15 = $e15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($e15-$s15));
            }

            $constants['asce']['pos_hip']['roof_slope'][2][1]['2n'] = $l15;
            // --------------------------

            $f15  = $asce['pos_hip']['roof_slope'][2][0]['2r'];
            $t15  = $asce['pos_hip']['roof_slope'][2][2]['2r'];

            if($f15 == "X" || $f15 == "x") {
                $m15 = "X";
            }
            else{
                $m15 = $f15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($f15-$t15));
            }

            $constants['asce']['pos_hip']['roof_slope'][2][1]['2r'] = $m15;
            // --------------------------
            $g15  = $asce['pos_hip']['roof_slope'][2][0]['3e'];
            $u15  = $asce['pos_hip']['roof_slope'][2][2]['3e'];

            if($g15 == "X" || $g15 == "x") {
                $n15 = "X";
            }
            else{
                $n15 = $g15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($g15-$u15));
            }

            $constants['asce']['pos_hip']['roof_slope'][2][1]['3e'] = $n15;

            // --------------------------
            $h15  = $asce['pos_hip']['roof_slope'][2][0]['3r'];
            $v15  = $asce['pos_hip']['roof_slope'][2][2]['3r'];

            if($h15 == "X" || $h15 == "x") {
                $o15 = "X";
            }
            else{
                $o15 = $h15+(((LOG($module_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*($h15-$v15));
            }

            $constants['asce']['pos_hip']['roof_slope'][2][1]['3r'] = $o15;

            /************************** 4th row
             * ************************
             */

            $b16 = $asce['pos_hip']['roof_slope'][3][0]['1'];
            $p16 = $asce['pos_hip']['roof_slope'][3][2]['1'];

            if($b16 == "X" || $b16 == "x") {
                $i16 = "X";
            }
            else{
                $i16 = $b16+(((LOG($module_area,10)-LOG(3,10))/(LOG(3,10)-LOG(100,10)))*($b16-$p16));
            }
            // -------------------------------
            $constants['asce']['pos_hip']['roof_slope'][3][1]['1'] = $i16;

            $c16       = $b[] = $asce['pos_hip']['roof_slope'][3][0]['1dash'];
            $q16       = $p[]  = $asce['pos_hip']['roof_slope'][3][2]['1dash'];

            if($c16 == "X" || $c16 == "x") {
                $j16 = "X";
            }
            else{
                $j16 = $c16+(((LOG($module_area,10)-LOG(3,10))/(LOG(3,10)-LOG(100,10)))*($c16-$q16));
            }

            $constants['asce']['pos_hip']['roof_slope'][3][1]['1dash'] = $j16;

            // -------------------------------

            $d16  = $asce['pos_hip']['roof_slope'][3][0]['2e'];
            $r16  = $asce['pos_hip']['roof_slope'][3][2]['2e'];

            if($d16 == "X" || $d16 == "x") {
                $k16 = "X";
            }
            else{
                $k16 = $d16+(((LOG($module_area,10)-LOG(3,10))/(LOG(3,10)-LOG(100,10)))*($d16-$r16));
            }

            $constants['asce']['pos_hip']['roof_slope'][3][1]['2e'] = $k16;
            // --------------------------

            $e16  = $asce['pos_hip']['roof_slope'][3][0]['2n'];
            $s16  = $asce['pos_hip']['roof_slope'][3][2]['2n'];

            if($e16 == "X" || $e16 == "x") {
                $l16 = "X";
            }
            else{
                $l16 = $e16+(((LOG($module_area,10)-LOG(3,10))/(LOG(3,10)-LOG(100,10)))*($e16-$s16));
            }

            $constants['asce']['pos_hip']['roof_slope'][3][1]['2n'] = $l16;
            // --------------------------

            $f16  = $asce['pos_hip']['roof_slope'][3][0]['2r'];
            $t16  = $asce['pos_hip']['roof_slope'][3][2]['2r'];

            if($f16 == "X" || $f16 == "x") {
                $m16 = "X";
            }
            else{
                $m16 = $f16+(((LOG($module_area,10)-LOG(3,10))/(LOG(3,10)-LOG(100,10)))*($f16-$t16));
            }

            $constants['asce']['pos_hip']['roof_slope'][3][1]['2r'] = $m16;
            // --------------------------
            $g16  = $asce['pos_hip']['roof_slope'][3][0]['3e'];
            $u16  = $asce['pos_hip']['roof_slope'][3][2]['3e'];

            if($g16 == "X" || $g16 == "x") {
                $n16 = "X";
            }
            else{
                $n16 = $g16+(((LOG($module_area,10)-LOG(3,10))/(LOG(3,10)-LOG(100,10)))*($g16-$u16));
            }

            $constants['asce']['pos_hip']['roof_slope'][3][1]['3e'] = $n16;

            // --------------------------
            $h16  = $asce['pos_hip']['roof_slope'][3][0]['3r'];
            $v16  = $asce['pos_hip']['roof_slope'][3][2]['3r'];

            if($h16 == "X" || $h16 == "x") {
                $o16 = "X";
            }
            else{
                $o16 = $h16+(((LOG($module_area,10)-LOG(3,10))/(LOG(3,10)-LOG(100,10)))*($h16-$v16));
            }

            $constants['asce']['pos_hip']['roof_slope'][3][1]['3r'] = $o16;




            Config::set('constants.asce',$constants['asce']);

            Artisan::call('config:clear');




        $constants = config('constants');

        // echo "<pre>";
        // print_r($b);
        // print_r($p);
        // print_r($constants);
        // exit;
    }

    public function calculate_asce_7_10_neg_gab($effective_wind_area)
    {

        // $effective_wind_area = $e17= 21;
        $constants = config('constants');
        $asce = config('constants.asce_7_10');

        //  =POWER(H13,1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))
         // *POWER(B13,1-1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))

            /************************** first row * *************************/

            $b13 = $asce['neg_gab']['roof_slope'][0][0]['1'];
            $h13 = $asce['neg_gab']['roof_slope'][0][2]['1'];

            $temp1 = ((LOG(100,10)-LOG($effective_wind_area,10))/(LOG($effective_wind_area,10)-LOG(10,10))+1);


            $e13 = pow($h13, 1 / $temp1) * pow($b13, 1 - 1 / $temp1);

            $constants['asce_7_10']['neg_gab']['roof_slope'][0][1]['1'] = $e13;

            // =POWER(I13,1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))
            // *POWER(C13,1-1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))

            $c13  = $asce['neg_gab']['roof_slope'][0][0]['2'];
            $i13  = $asce['neg_gab']['roof_slope'][0][2]['2'];

            $f13 = pow($i13, 1 / $temp1) * pow($c13, 1 - 1 / $temp1);

            $constants['asce_7_10']['neg_gab']['roof_slope'][0][1]['2'] = $f13;
            // --------------------------
            // =POWER(J13,1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))
            // *POWER(D13,1-1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))

            $d13  = $asce['neg_gab']['roof_slope'][0][0]['3'];
            $j13  = $asce['neg_gab']['roof_slope'][0][2]['3'];

            $g13 = pow($j13, 1 / $temp1) * pow($d13, 1 - 1 / $temp1);

            $constants['asce_7_10']['neg_gab']['roof_slope'][0][1]['3'] = $g13;
            // --------------------------

            /************************** second row * *************************/

             //  =POWER(H14,1/  ((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))
             //  *POWER(B14,1-1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))


            $b14 = $asce['neg_gab']['roof_slope'][1][0]['1'];
            $h14 = $asce['neg_gab']['roof_slope'][1][2]['1'];

            $e14 = pow($h14, 1 / $temp1) * pow($b14, 1 - 1 / $temp1);

            $constants['asce_7_10']['neg_gab']['roof_slope'][1][1]['1'] = $e14;

            // =POWER(I14,1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))
            // *POWER(C14,1-1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))

            $c14  = $asce['neg_gab']['roof_slope'][1][0]['2'];
            $i14  = $asce['neg_gab']['roof_slope'][1][2]['2'];

            $f14 = pow($i14, 1 / $temp1) * pow($c14, 1 - 1 / $temp1);

            $constants['asce_7_10']['neg_gab']['roof_slope'][1][1]['2'] = $f14;
            // --------------------------
            // =POWER(J13,1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))
            // *POWER(D13,1-1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))

            $d14  = $asce['neg_gab']['roof_slope'][1][0]['3'];
            $j14  = $asce['neg_gab']['roof_slope'][1][2]['3'];

            $g14 = pow($j14, 1 / $temp1) * pow($d14, 1 - 1 / $temp1);

            $constants['asce_7_10']['neg_gab']['roof_slope'][1][1]['3'] = $g14;
            // --------------------------

            /************************** 3rd row* ************************
             */

            //  =POWER(H14,1/  ((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))
             //  *POWER(B14,1-1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))


             $b15 = $asce['neg_gab']['roof_slope'][2][0]['1'];
             $h15 = $asce['neg_gab']['roof_slope'][2][2]['1'];

             $e15 = pow($h15, 1 / $temp1) * pow($b15, 1 - 1 / $temp1);

             $constants['asce_7_10']['neg_gab']['roof_slope'][2][1]['1'] = $e15;

             // =POWER(I14,1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))
             // *POWER(C14,1-1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))

             $c15  = $asce['neg_gab']['roof_slope'][2][0]['2'];
             $i15  = $asce['neg_gab']['roof_slope'][2][2]['2'];

             $f15 = pow($i15, 1 / $temp1) * pow($c15, 1 - 1 / $temp1);

             $constants['asce_7_10']['neg_gab']['roof_slope'][2][1]['2'] = $f15;
             // --------------------------
             // =POWER(J13,1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))
             // *POWER(D13,1-1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))

             $d15  = $asce['neg_gab']['roof_slope'][2][0]['3'];
             $j15  = $asce['neg_gab']['roof_slope'][2][2]['3'];

             $g15 = pow($j15, 1 / $temp1) * pow($d15, 1 - 1 / $temp1);

             $constants['asce_7_10']['neg_gab']['roof_slope'][2][1]['3'] = $g15;
             // --------------------------

            Config::set('constants.asce_7_10',$constants['asce_7_10']);

            Artisan::call('config:clear');


    }

    public function calculate_asce_7_10_pos_gab($effective_wind_area)
    {

        // $effective_wind_area = $e17= 21;
        $constants = config('constants');
        $asce = config('constants.asce_7_10');


        // =IF(E17<10,B25,IF(E17>100,H25,POWER(H25,1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))*POWER(B25,1-1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))))

            /************************** first row **************************/
            // =POWER(D37,1/((LOG(500,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))
            // *POWER(B37,1-1/((LOG(500,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))
         $b37 = $asce['pos_int_gab']['roof_slope'][0][0]['1'];
         $d37 = $asce['pos_int_gab']['roof_slope'][0][0]['3'];

         $temp1 = ((LOG(500,10)-LOG($effective_wind_area,10))/(LOG($effective_wind_area,10)-LOG(10,10))+1);

        $c37 = pow($d37, 1 / $temp1) * pow($b37, 1 - (1 / $temp1));
        $constants['asce_7_10']['pos_int_gab']['roof_slope'][0][0]['1'] = $c37;


         $b25 = $asce['pos_gab']['roof_slope'][0][0]['1'];
         $h25 = $asce['pos_gab']['roof_slope'][0][2]['1'];

         $temp1 = ((LOG(100,10)-LOG($effective_wind_area,10))/(LOG($effective_wind_area,10)-LOG(10,10))+1);


            if($effective_wind_area < 10) {
                $e25 = $b25;
            }
            else if($effective_wind_area > 100){
                $e25 = $h25;
            }
            else {

                $e25 = pow($h25, 1 / $temp1) * pow($b25, 1 - (1 / $temp1));
            }


            $constants['asce_7_10']['pos_gab']['roof_slope'][0][1]['1'] = $e25;
            $constants['asce_7_10']['pos_gab']['roof_slope'][0][1]['2'] = "X";
            $constants['asce_7_10']['pos_gab']['roof_slope'][0][1]['3'] = "X";

            /************************** second row * *************************/

             //  =IF(E17<10,B26,IF(E17>100,H26,POWER(H26,1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))*POWER(B26,1-1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))))


            $b26 = $asce['pos_gab']['roof_slope'][1][0]['1'];
            $h26 = $asce['pos_gab']['roof_slope'][1][2]['1'];

            if($effective_wind_area < 10) {
                $e26 = $b26;
            }
            else if($effective_wind_area > 100){
                $e26 = $h26;
            }
            else {
                $e26 = pow($h26, 1 / $temp1) * pow($b26, 1 - 1 / $temp1);
            }

            $constants['asce_7_10']['pos_gab']['roof_slope'][1][1]['1'] = $e26;
            $constants['asce_7_10']['pos_gab']['roof_slope'][1][1]['2'] = "X";
            $constants['asce_7_10']['pos_gab']['roof_slope'][1][1]['3'] = "X";



            /************************** 3rd row* *************************/

            //  =IF(E17<10,B27,IF(E17>100,H27,POWER(H27,1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))*POWER(B27,1-1/((LOG(100,10)-LOG($E$17,10))/(LOG($E$17,10)-LOG(10,10))+1))))


             $b27 = $asce['pos_gab']['roof_slope'][2][0]['1'];
             $h27 = $asce['pos_gab']['roof_slope'][2][2]['1'];

             if($effective_wind_area < 10) {
                $e27 = $b27;
            }
            else if($effective_wind_area > 100){
                $e27 = $h27;
            }
            else {
                $e27 = pow($h27, 1 / $temp1) * pow($b27, 1 - 1 / $temp1);
            }

             $constants['asce_7_10']['pos_gab']['roof_slope'][2][1]['1'] = $e27;
             $constants['asce_7_10']['pos_gab']['roof_slope'][2][1]['2'] = "X";
             $constants['asce_7_10']['pos_gab']['roof_slope'][2][1]['3'] = "X";



            Config::set('constants.asce_7_10',$constants['asce_7_10']);

            Artisan::call('config:clear');
    }



    public function external_pressure_coefficient(Request $request) {
        $roof_type = $request->roof_type;
        $roof_slope_degree = $request->roof_slope_degree;
        $effective_wind_area = $request->effective_wind_area;
        $module_area = $request->module_area;
        // $velocity_pressure = $request->velocity_pressure;
        $srsl = $request->srsl;
        $dead_load = $request->dead_load;
        $module_orientation = $request->module_orientation;
        $module_length = $request->module_length;
        $module_width = $request->module_width;
        $no_of_rails = 2;
        $no_of_rails_exposed = $request->no_of_rails_exposed;
        $no_of_rails_non_exposed = $request->no_of_rails_non_exposed;
        $limit_max_span_to = $request->limit_max_span_to;
        $rafter_seam_spacing = $request->rafter_seam_spacing;
        $compression_strength = $request->compression_strength;

        $kz = $request->kz;
        $ke = $request->ke;
        $topographic_factor = $request->topographic_factor;
        $wind_directionality_factor = $request->wind_directionality_factor;
        $normal_wind_speed = $request->normal_wind_speed;
        $exposure_category = $request->exposure_category;
        $roof_height = $request->roof_height;

        $velocity_pressure = $this->velocity_pressure($kz,$topographic_factor,$wind_directionality_factor,$ke,$normal_wind_speed);

        $solar_panel_pressure = $this->solar_panel_pressure_equalization_factor($effective_wind_area);

        $this->calculate_asce_neg_gab($module_area);
        $this->calculate_asce_pos_gab($module_area);
        $this->calculate_asce_neg_hip($module_area);
        $this->calculate_asce_pos_hip($module_area);

        $keyval = array("1","1dash","2e","2n","2r","3e","3r");

        // echo "roof_slope_degree >> ".$roof_slope_degree;
        // echo "<pre>";
        // print_r($result);exit;
        foreach($keyval as $key) {
             // For positive pressure
            if($roof_type == "GABLE") {
                $asceConstants = config('constants.asce.pos_gab.roof_slope');

                if($roof_slope_degree < 7) {
                    // var $val = HLOOKUP(1,'asce 7-16 tables pos gab'!gcp_10_100,2,0);
                    $val = $asceConstants[0][1][$key];
                }
                if($roof_slope_degree > 7 && $roof_slope_degree < 20) {
                    // var $val = HLOOKUP(1,INDIRECT($'ASCE 7-16 Tables Pos Gab'.$E$19),3,0);
                    // =IF(E18<10,"GCP_10",IF(E18>100,"GCP_100","GCP_10_100"))
                    if($effective_wind_area < 10) {
                        $val = $asceConstants[0][1][$key];
                    }
                    else if($effective_wind_area > 100) {
                        $val = $asceConstants[1][1][$key];
                    }
                    else {
                        $val = $asceConstants[2][1][$key];
                    }

                }
                if($roof_slope_degree > 20 && $roof_slope_degree < 27) {
                    // var $val = HLOOKUP(1,'asce 7-16 tables pos gab'!gcp_10_100,4,0);
                    $val = $asceConstants[2][1]['1'];
                }
                else {
                    // var $val = HLOOKUP(1,'asce 7-16 tables pos gab'!gcp_10_100,5,0);
                    $val = $asceConstants[3][1][$key];
                }
            }
            else {
                $asceConstants = config('constants.asce.pos_hip.roof_slope');
                // echo "<pre>";
                // print_r($asceConstants);
                // exit;
                if($roof_slope_degree < 7) {
                    // var val = HLOOKUP(1,'asce 7-16 tables pos hip'!gcp_10_100,2,0);
                    $val = $asceConstants[0][1][$key];

                }
                if($roof_slope_degree > 7 && $roof_slope_degree < 20) {
                    // var val = HLOOKUP(1,INDIRECT($'ASCE 7-16 Tables Pos hip'.$E$19),3,0);
                    if($effective_wind_area < 10) {
                        $val = $asceConstants[0][1][$key];
                    }
                    else if($effective_wind_area > 100) {
                        $val = $asceConstants[1][1][$key];
                    }
                    else {
                        $val = $asceConstants[2][1][$key];
                    }

                }
                if($roof_slope_degree > 20 && $roof_slope_degree < 27) {
                    // var val = HLOOKUP(1,'asce 7-16 tables pos hip'!gcp_10_100,4,0);
                    $val = $asceConstants[2][1][$key];

                }
                else {
                    // var val = HLOOKUP(1,'asce 7-16 tables pos hip'!gcp_10_100,5,0);
                    $val = $asceConstants[3][1][$key];

                }
            }
            $result['ext_pos_val'][] = round($val,4);

            // For negative

            if($roof_type == "GABLE") {
                $asceConstants = config('constants.asce.neg_gab.roof_slope');

                if($roof_slope_degree < 7) {
                    // var val = HLOOKUP(1,GCP_10_100,2,0);
                    $val_neg = $asceConstants[0][1][$key];
                }
                if($roof_slope_degree > 7 && $roof_slope_degree < 20) {
                    // var val = HLOOKUP(1,GCP_10_100,3,0);
                    $val_neg = $asceConstants[1][1][$key];
                }
                if($roof_slope_degree > 20 && $roof_slope_degree < 27) {
                    // var val = HLOOKUP(1,GCP_10_100,4,0);
                    $val_neg = $asceConstants[2][1][$key];
                }
                else {
                    // var val = HLOOKUP(1,GCP_10_100,5,0);
                    $val_neg = $asceConstants[3][1][$key];
                }
            }
            else {
                $asceConstants = config('constants.asce.neg_hip.roof_slope');

                if($roof_slope_degree < 7) {
                    // var val = HLOOKUP(1,'asce 7-16 tables neg hip'!gcp_10_100,2,0);
                    $val_neg = $asceConstants[0][1][$key];
                }
                if($roof_slope_degree > 7 && $roof_slope_degree < 20) {
                    // var val = HLOOKUP(1,'asce 7-16 tables neg hip'!gcp_10_100,3,0);
                    $val_neg = $asceConstants[1][1][$key];
                }
                if($roof_slope_degree > 20 && $roof_slope_degree < 27) {
                    // var val = HLOOKUP(1,'asce 7-16 tables neg hip'!gcp_10_100,4,0);
                    $val_neg = $asceConstants[2][1][$key];
                }
                else {
                    // var val = HLOOKUP(1,'asce 7-16 tables neg hip'!gcp_10_100,5,0);
                    $val_neg = $asceConstants[3][1][$key];
                }
            }
            $result['ext_neg_val'][] = round($val_neg,4);


        }

        // echo "<pre>";
        // print_r($result['ext_neg_val']);
        // $this->moveElement($result['ext_neg_val'],3,2);
        // print_r($result['ext_neg_val']);

        // exit;

        $design_pressure = $this->positive_pressure($result['ext_pos_val'],$result['ext_neg_val'],$velocity_pressure);
        $result['design_pos_pressure'] = $design_pressure['positive'];
        $result['design_neg_pressure'] = $design_pressure['negative'];

        $adjusted_design_pressure = $this->adjusted_design_pressure($result['ext_pos_val'],$result['ext_neg_val'],$result['design_pos_pressure'],$result['design_neg_pressure'],$dead_load,$solar_panel_pressure,$srsl);
        $result['adjusted_pos_pressure'] = $adjusted_design_pressure['positive'];
        $result['adjusted_neg_pressure'] = $adjusted_design_pressure['negative'];
        $result['adjusted_non_neg_pressure'] = $adjusted_design_pressure['non_negative'];

        $adjusted_loading = $this->attachment_loads($result['ext_pos_val'],$result['ext_neg_val'],$result['adjusted_pos_pressure'],$result['adjusted_neg_pressure'],$result['adjusted_non_neg_pressure'],$module_orientation,$module_length,$module_width,$no_of_rails,$no_of_rails_exposed,$no_of_rails_non_exposed,$limit_max_span_to,$rafter_seam_spacing,$compression_strength);

        $result['adjusted_pos_loading'] = $adjusted_loading['positive'];
        $result['adjusted_neg_loading'] = $adjusted_loading['negative'];
        $result['adjusted_non_neg_loading'] = $adjusted_loading['non_negative'];
        $result['spanExposed'] = $adjusted_loading['spanExposed'];
        $result['spanNonExposed'] = $adjusted_loading['spanNonExposed'];

        $kz_values = $this->moduleKzRoofInfo($roof_height,$exposure_category);
        $result['kz_values'] = $kz_values;

        foreach($result['ext_neg_val'] as $pos_key => $pos_val) {
            if($pos_val == 0) {
                $result['design_pos_pressure'][$pos_key] = 0;
                $result['design_neg_pressure'][$pos_key] = 0;
                $result['adjusted_pos_pressure'][$pos_key] = 0;
                $result['adjusted_neg_pressure'][$pos_key] = 0;
                $result['adjusted_non_neg_pressure'][$pos_key] = 0;
                $result['adjusted_pos_loading'][$pos_key] = 0;
                $result['adjusted_neg_loading'][$pos_key] = 0;
                $result['adjusted_non_neg_loading'][$pos_key] = 0;
                $result['spanExposed'][$pos_key] = 0;
                $result['spanNonExposed'][$pos_key] = 0;
                $result['kz_values'][$pos_key] = 0;
            }
        }
        array_walk_recursive($result['ext_neg_val'], array($this,'array_replacing'));
        array_walk_recursive($result['design_pos_pressure'], array($this,'array_replacing'));
        array_walk_recursive($result['design_neg_pressure'], array($this,'array_replacing'));
        array_walk_recursive($result['adjusted_pos_pressure'], array($this,'array_replacing'));
        array_walk_recursive($result['adjusted_neg_pressure'], array($this,'array_replacing'));
        array_walk_recursive($result['adjusted_non_neg_pressure'], array($this,'array_replacing'));
        array_walk_recursive($result['adjusted_pos_loading'], array($this,'array_replacing'));
        array_walk_recursive($result['adjusted_neg_loading'], array($this,'array_replacing'));
        array_walk_recursive($result['adjusted_non_neg_loading'], array($this,'array_replacing'));
        array_walk_recursive($result['spanExposed'], array($this,'array_replacing'));
        array_walk_recursive($result['spanNonExposed'], array($this,'array_replacing'));
        array_walk_recursive($result['kz_values'], array($this,'array_replacing'));
        // echo "<pre>";
        // print_r($result['ext_neg_val']);
        // exit;

        $data['result'] = 1;
        $data['data'] = $result;
        return response()->json($data);

    }
    public function moveElement(&$array, $a, $b) {
        $out = array_splice($array, $a, 1);
        array_splice($array, $b, 0, $out);
    }

    function array_replacing(&$item, $key)
    {
        if($item == 0) {
            $item = 'X';
            // echo $item;exit;

        }
    }
    public function external_pressure_coefficient_7_10(Request $request) {
        $roof_type = $request->roof_type;
        $roof_slope_degree = $request->roof_slope_degree;
        $effective_wind_area = $request->effective_wind_area;

        $module_orientation = $request->module_orientation;
        $module_length = $request->module_length;
        $module_width = $request->module_width;
        $no_of_rails = 2;
        $limit_max_span_to = $request->limit_max_span_to;
        $rafter_seam_spacing = $request->rafter_seam_spacing;
        $compression_strength = $request->compression_strength;
        $kz = $request->kz;
        $topographic_factor = $request->topographic_factor;
        $wind_directionality_factor = $request->wind_directionality_factor;
        $normal_wind_speed = $request->normal_wind_speed;
        $roof_type = $request->roof_type;
        $parapet_height = $request->parapet_height;
        $velocity_pressure_7_10 = $this->velocity_pressure_7_10($kz,$topographic_factor,$wind_directionality_factor,$normal_wind_speed);
        $srsl = $request->srsl;
        $dead_load = $request->dead_load;
        $solar_panel_pressure = $this->solar_panel_pressure_equalization_factor($effective_wind_area);

        $this->calculate_asce_7_10_neg_gab($effective_wind_area);
        $this->calculate_asce_7_10_pos_gab($effective_wind_area);


        $keyval1 = array("1","2","3");

        //for pos zone 1
        // =IF($H$7="< 7°",HLOOKUP(1,INDIRECT('ASCE 7-10 Tables'!$G$30),2,FALSE),IF($H$7="7°< Θ < 27°",HLOOKUP(1,INDIRECT('ASCE 7-10 Tables'!$G$30),3,FALSE),HLOOKUP(1,INDIRECT('ASCE 7-10 Tables'!$G$30),4,FALSE)))
        //for pos zone 2
        // =IF(AND(H7<7,D9>2.999),Z45GCPpos,IF($H$7="< 7°",HLOOKUP(1,INDIRECT('ASCE 7-10 Tables'!$G$30),2,FALSE),IF($H$7="7°< Θ < 27°",HLOOKUP(1,INDIRECT('ASCE 7-10 Tables'!$G$30),3,FALSE),HLOOKUP(1,INDIRECT('ASCE 7-10 Tables'!$G$30),4,FALSE))))
        //for pos zone 3
        // =IF(AND(H7<7,D9>2.999),Z45GCPpos,IF($H$7="< 7°",HLOOKUP(1,INDIRECT('ASCE 7-10 Tables'!$G$30),2,FALSE),IF($H$7="7°< Θ < 27°",HLOOKUP(1,INDIRECT('ASCE 7-10 Tables'!$G$30),3,FALSE),HLOOKUP(1,INDIRECT('ASCE 7-10 Tables'!$G$30),4,FALSE))))

        // for neg
        // =-1*IF($H$7<7,HLOOKUP(1,INDIRECT('ASCE 7-10 Tables'!$E$18),2,FALSE),IF($H$7<27,HLOOKUP(1,INDIRECT('ASCE 7-10 Tables'!$E$18),3,FALSE),HLOOKUP(1,INDIRECT('ASCE 7-10 Tables'!$E$18),4,FALSE)))

        $asce_7_10Constants_pos = config('constants.asce_7_10.pos_gab.roof_slope');
        $asce_7_10Constants_pos_int = config('constants.asce_7_10.pos_int_gab.roof_slope');
        $asce_7_10Constants_neg = config('constants.asce_7_10.neg_gab.roof_slope');

        // echo "<pre>";
        // print_r($asce_7_10Constants_neg);exit;
        $m = 0;
        foreach($keyval1 as $key) {
            if($roof_slope_degree < 7) {
                $val_asc_7_10_neg = -$asce_7_10Constants_neg[0][1][$key];
                if($m == 0) {
                    if($effective_wind_area < 10) {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[0][1][$key];
                    }
                    else if($effective_wind_area > 100) {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[1][1][$key];
                    }
                    else {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[2][1][$key];
                    }
                }

            }
            else if($roof_slope_degree > 7 && $roof_slope_degree < 27) {
                $val_asc_7_10_neg = -$asce_7_10Constants_neg[1][1][$key];
                if($m == 0) {
                    if($effective_wind_area < 10) {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[0][1][$key];
                    }
                    else if($effective_wind_area > 100) {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[1][1][$key];
                    }
                    else {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[2][1][$key];
                    }
                }
            }
            else {
                $val_asc_7_10_neg = -$asce_7_10Constants_neg[2][1][$key];
                if($m == 0) {
                    if($effective_wind_area < 10) {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[0][1][$key];
                    }
                    else if($effective_wind_area > 100) {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[1][1][$key];
                    }
                    else {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[2][1][$key];
                    }
                }
            }
            // =IF(AND(H7<7,D9>2.999),Z45GCPpos,IF($H$7="< 7°",HLOOKUP(1,INDIRECT('ASCE 7-10 Tables'!$G$30),2,FALSE),IF($H$7="7°< Θ < 27°",HLOOKUP(1,INDIRECT('ASCE 7-10 Tables'!$G$30),3,FALSE),HLOOKUP(1,INDIRECT('ASCE 7-10 Tables'!$G$30),4,FALSE))))

            // *** Comment for temporary *** /
            if($m > 0) {
                if($roof_slope_degree < 7 && $parapet_height > 2.999) {
                   // =IF(D39<10,B37,IF(D39>500,D37,C37))
                    if($effective_wind_area < 10) {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos_int[0][0][$key];
                    }
                    else if($effective_wind_area > 100) {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos_int[0][1][$key];
                    }
                    else {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos_int[0][1][$key];
                    }
                }
               else if($roof_slope_degree < 7 ) {
                    $val_asc_7_10_pos = $asce_7_10Constants_pos[0][1][$key];
                    if($effective_wind_area < 10) {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[0][1][$key];
                    }
                    else if($effective_wind_area > 100) {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[1][1][$key];
                    }
                    else {
                           $val_asc_7_10_pos = $asce_7_10Constants_pos[2][1][$key];
                    }
                }
                else if($roof_slope_degree > 7 && $roof_slope_degree < 27) {
                    if($effective_wind_area < 10) {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[0][1][$key];
                    }
                    else if($effective_wind_area > 100) {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[1][1][$key];
                    }
                    else {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[2][1][$key];
                    }
                }
                else {
                    if($effective_wind_area < 10) {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[0][1][$key];
                    }
                    else if($effective_wind_area > 100) {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[1][1][$key];
                    }
                    else {
                        $val_asc_7_10_pos = $asce_7_10Constants_pos[2][1][$key];
                    }
                }
            }





            $m++;

            $result['ext_pos_val_7_10'][] = round($val_asc_7_10_pos,4);
            $result['ext_neg_val_7_10'][] = round($val_asc_7_10_neg,4);




        }

        // echo "roof_slope_degree >> ".$roof_slope_degree;
        // echo "<pre>";
        // print_r($asce_7_10Constants_neg);
        // print_r($result);
        // exit;

        $design_pressure = $this->positive_pressure_7_10($result['ext_pos_val_7_10'],$result['ext_neg_val_7_10'],$velocity_pressure_7_10,$roof_type,$roof_slope_degree,$parapet_height);



        $result['design_pos_pressure'] = $design_pressure['positive'];
        $result['design_neg_pressure'] = $design_pressure['negative'];

        $adjusted_design_pressure = $this->adjusted_design_pressure_7_10($result['ext_pos_val_7_10'],$result['ext_neg_val_7_10'],$result['design_pos_pressure'],$result['design_neg_pressure'],$dead_load,$solar_panel_pressure,$srsl);
        $result['adjusted_pos_pressure'] = $adjusted_design_pressure['positive'];
        $result['adjusted_neg_pressure'] = $adjusted_design_pressure['negative'];
        $result['adjusted_non_neg_pressure'] = $adjusted_design_pressure['non_negative'];
        // echo "check >> ";exit;

        $adjusted_loading_7_10 = $this->attachment_loads_7_10($result['ext_pos_val_7_10'],$result['ext_neg_val_7_10'],$result['adjusted_pos_pressure'],$result['adjusted_neg_pressure'],$module_orientation,$module_length,$module_width,$no_of_rails,$limit_max_span_to,$rafter_seam_spacing,$compression_strength);

        $result['adjusted_pos_loading_7_10'] = $adjusted_loading_7_10['positive'];
        $result['adjusted_neg_loading_7_10'] = $adjusted_loading_7_10['negative'];
        $result['spanExposed_7_10'] = $adjusted_loading_7_10['spanExposed'];
        $result['spanNonExposed_7_10'] = $adjusted_loading_7_10['spanNonExposed'];


        foreach($result['ext_neg_val_7_10'] as $pos_key => $pos_val) {
            if($pos_val == 0) {
                $result['design_pos_pressure'][$pos_key] = 0;
                $result['design_neg_pressure'][$pos_key] = 0;
                $result['adjusted_pos_loading_7_10'][$pos_key] = 0;
                $result['adjusted_neg_loading_7_10'][$pos_key] = 0;
                $result['spanExposed_7_10'][$pos_key] = 0;
                $result['spanNonExposed_7_10'][$pos_key] = 0;
            }
        }

        array_walk_recursive($result['ext_pos_val_7_10'], array($this,'array_replacing'));
        array_walk_recursive($result['ext_neg_val_7_10'], array($this,'array_replacing'));
        array_walk_recursive($result['design_pos_pressure'], array($this,'array_replacing'));
        array_walk_recursive($result['design_neg_pressure'], array($this,'array_replacing'));
        array_walk_recursive($result['adjusted_pos_loading_7_10'], array($this,'array_replacing'));
        array_walk_recursive($result['adjusted_neg_loading_7_10'], array($this,'array_replacing'));
        array_walk_recursive($result['spanExposed_7_10'], array($this,'array_replacing'));
        array_walk_recursive($result['spanNonExposed_7_10'], array($this,'array_replacing'));



        // echo "<pre>";
        // print_r($data);

        $data['result'] = 1;
        $data['data'] = $result;
        return response()->json($data);

    }
    public function velocity_pressure($kz,$topographic_factor,$wind_directionality_factor,$ke,$normal_wind_speed)
    {
        // =0.00256*H19*H17*H16*H18*POWER(H11,2)

       return $velocity_pressure = (0.00256 * $kz * $topographic_factor * $wind_directionality_factor * $ke) * pow($normal_wind_speed,2);
    }
    public function velocity_pressure_7_10($kz,$topographic_factor,$wind_directionality_factor,$normal_wind_speed)
    {
        // =0.00256*H13*H12*H11*POWER(H10,2)

       return $velocity_pressure = (0.00256 * $kz * $topographic_factor * $wind_directionality_factor ) * pow($normal_wind_speed,2);
    }
    public function positive_pressure($zone_pos_array,$zone_neg_array,$velocity_pressure)
    {
        // =IF(F29="X","X",IF($D$23*(F29+$E$37)<16,16,$D$23*(F29+$E$37)))
        $internal_pressure_coefficient = config('constants.internal_pressure_coefficient');
        $zone1 = 0.596;
        for($i = 0; $i <= 6; $i++)
        {

            $pos_pressure_val = $zone_pos_array[$i];

            if($zone_pos_array[$i] == "X" || $zone_pos_array[$i] == "x") {
                $pos_pressure_val = "X";
            }
            else
            {
                if(($velocity_pressure * ($zone_pos_array[$i] + $internal_pressure_coefficient)) < 16) {
                    $pos_pressure_val  = 16;
                }
                else {
                    $pos_pressure_val  = $velocity_pressure * ($zone_pos_array[$i] + $internal_pressure_coefficient);
                }
            }

            $pressure_array['positive'][]  = round($pos_pressure_val,4);

            // For negative pressure
            // =IF(G29="X","X",$D$23*(G29-$E$37))

            if($zone_neg_array[$i] == "X" || $zone_neg_array[$i] == "x")
            $neg_pressure_val = "X";
            else
            {
                $neg_pressure_val  = $velocity_pressure * ($zone_neg_array[$i] - $internal_pressure_coefficient);

            }
            $pressure_array['negative'][]  = round($neg_pressure_val,4);
        }


        // $data['result'] = 1;
        // $data['pressure_array'] = $pressure_array;
        // $data['velocity_pressure'] = $velocity_pressure;
        return $pressure_array;

    }
    public function positive_pressure_7_10($zone_pos_array,$zone_neg_array,$velocity_pressure,$roof_type,$roof_slope,$parapet_height)
    {
        // =IF($D$17*(F22+$E$26)<16,16,$D$17*(F22+$E$26))
        $internal_pressure_coefficient = config('constants.internal_pressure_coefficient');

        for($i = 0; $i <= 2; $i++)
        {
            if(($velocity_pressure * ($zone_pos_array[$i] + $internal_pressure_coefficient)) < 16) {
                $pos_pressure_val  = 16;
            }
            else {
                $pos_pressure_val  = $velocity_pressure * ($zone_pos_array[$i] + $internal_pressure_coefficient);
            }

            $pressure_array['positive'][]  = round($pos_pressure_val,4);

            // For negative pressure
            // =$D$17*(G22-$E$26)
            if($i < 2) {
                $neg_pressure_val  = $velocity_pressure * ($zone_neg_array[$i] - $internal_pressure_coefficient);

            }
            if($i == 2) {
                //  =IF(OR(AND(H8="HIP",H7<25),AND(D9>2.999,H7<7)),E31,$D$17*(G24-$E$26))
                if(($roof_type == 'HIP' && $roof_slope < 25) || ($parapet_height > 2.999 && $roof_slope < 7)) {
                    $neg_pressure_val  = $pressure_array['negative'][1];
                    if($neg_pressure_val > -16) {
                        $neg_pressure_val = -16;
                    }

                }
                else {
                    $neg_pressure_val  = $velocity_pressure * ($zone_neg_array[$i] - $internal_pressure_coefficient);
                    if($neg_pressure_val > -16) {
                        $neg_pressure_val = -16;
                   }

                }
            }
            $pressure_array['negative'][]  = round($neg_pressure_val,4);
        }
        // $data['result'] = 1;
        // $data['pressure_array'] = $pressure_array;
        // $data['velocity_pressure'] = $velocity_pressure;
        return $pressure_array;

    }
    public function solar_panel_pressure_equalization_factor($effective_wind_area)
    {
       return  $val = 0.8+(((LOG($effective_wind_area,10)-LOG(10,10))/(LOG(10,10)-LOG(100,10)))*(0.8-0.4));
    }
    public function adjusted_design_pressure($zone_pos_array,$zone_neg_array,$design_pos_array,$design_neg_array,$dead_load,$solar_panel_pressure,$srsl)
    {
        // =IF(F29="X","X",MAX(($D$15+(0.45*D41*$I$51)+(0.75*$D$16)),16))
        // d15 = dead_load;
        // d16 = dead_load;
        // d41 = design_pos_array_zone1;
        // i51 = solar_panel_pressure;
        // e41 = design_neg_array_zone1;
        $internal_pressure_coefficient = config('constants.internal_pressure_coefficient');
        $zone1 = 0.596;

        $array_edge_factor_exposed = config('constants.array_edge_factor_exposed');
        $array_edge_factor_non_exposed = config('constants.array_edge_factor_non_exposed');


        for($i = 0; $i <= 6; $i++) {

            $pos_pressure_val = $zone_pos_array[$i];

            if($zone_pos_array[$i] == "X" || $zone_pos_array[$i] == "x") {
                $pos_pressure_val = "X";
            }
            else {
                $pos_pressure_val  = max(($dead_load + ( 0.45 * $design_pos_array[$i] * $solar_panel_pressure) + (0.75 * $srsl)),16);
            }

            $pressure_array['positive'][]  = round($pos_pressure_val,4);

            // For exposed negative pressure
            // =IF(G29="X","X",MIN(E41*$E$51,-16))

            if($zone_neg_array[$i] == "X" || $zone_neg_array[$i] == "x") {
            $neg_pressure_val = "X";
            }
            else {
                // $neg_pressure_val  = min(($design_neg_array[$i] * $array_edge_factor_exposed),-16);
                $neg_pressure_val  = min(($design_neg_array[$i] * $array_edge_factor_exposed * $solar_panel_pressure),-16);

            }
            $pressure_array['negative'][]  = round($neg_pressure_val,4);

            // For non exposed negative pressure
            // =IF(G29="X","X",MIN(E41*$E$52,-16))

            if($zone_neg_array[$i] == "X" || $zone_neg_array[$i] == "x") {
                $non_neg_pressure_val = "X";
            }
            else {
                // $non_neg_pressure_val  = min(($design_neg_array[$i] * $array_edge_factor_non_exposed),-16);
                $non_neg_pressure_val  = min(($design_neg_array[$i] * $array_edge_factor_non_exposed * $solar_panel_pressure),-16);

            }
            $pressure_array['non_negative'][]  = round($non_neg_pressure_val,4);
        }
        // $data['result'] = 1;
        // $data['pressure_array'] = $pressure_array;
        return $pressure_array;

    }
    public function attachment_loads($zone_pos_array,$zone_neg_array,$adjusted_pos_array,$adjusted_neg_array,$adjusted_non_neg_array,$module_orientation,$module_length,$module_width,$no_of_rails,$no_of_rails_exposed,$no_of_rails_non_exposed,$limit_max_span_to,$rafter_seam_spacing,$compression_strength)
    {
        // For positive loads
        // =IF(F29="X","X",(D57*MAX(H73,J73)*(IF($D$12="PORTRAIT",$D$10,$D$11)))/(144*$I$71))
        // d57 = adjusted_pos_array;
        // H73 = spanExposed;
        // j73 = spanNonExposed;
        // d12 = module_orientation;
        // d10 = module_length;
        // d11 = module_width;
        // i71 = no_of_rails;
        $no_of_rails = 2;


        $spanExposed = array(0,0,0,0,0,0,0);
        $spanNonExposed = array(0,0,0,0,0,0,0);


        // print_r($zone_pos_array);
        // print_r($zone_neg_array);
        // print_r($adjusted_pos_array);
        // print_r($adjusted_neg_array);
        // print_r($adjusted_non_neg_array);
        // exit;
        $non_neg_loads_val = 0;
        $neg_loads_val = 0;
        $pos_loads_val = 0;
        for($i = 0; $i <= 6; $i++) {

            if($module_orientation == "PORTRAIT") {
                $temp_val = $module_length;
            }
            else {
                $temp_val = $module_width;
            }

            $start = $rafter_seam_spacing;
            $end = $limit_max_span_to;

            $spanExposed[$i] = $start;

            $count = 1;
            if($start > 0 && $end > 0) {
                while($start <= $end) {
                    if($no_of_rails_exposed > 0) {
                        $neg_loads_val  = abs(($adjusted_neg_array[$i] * $start * $temp_val)) / (144 * $no_of_rails_exposed);
                    }
                    if($neg_loads_val <= $compression_strength) {
                        $spanExposed[$i] = $start;
                        // $start = $start * 2;
                        $start = $start + $rafter_seam_spacing;
                    }else {
                        break;
                    }

                    $count++;
                }
            }



            $start = $rafter_seam_spacing;
            $end = $limit_max_span_to;

            $spanNonExposed[$i] = $start;
            if($start > 0 && $end > 0) {
                while($start <= $end) {

                    if($no_of_rails_non_exposed > 0) {
                        $non_neg_loads_val  = abs(($adjusted_non_neg_array[$i] * $start * $temp_val)) / (144 * $no_of_rails_non_exposed);
                    }
                    // echo "start -- ".$start. " >>> ".$non_neg_loads_val;
                    if($non_neg_loads_val <= $compression_strength) {
                        $spanNonExposed[$i] = $start;
                        // $start = $start * 2;
                        $start = $start + $rafter_seam_spacing;
                    }
                    else {
                        break;
                    }
                }
            }


            // For exposed negative pressure
            // =IF($G29="X","X",ABS((E57*H73*(IF($D$12="PORTRAIT",$D$10,$D$11)))/(144*$I$71)))
            // e57 = adjusted_neg_array
            // h73 = adjusted_neg_array
            if($zone_neg_array[$i] == "X" || $zone_neg_array[$i] == "x") {
            $neg_loads_val = "X";
            }
            else {
                if($no_of_rails_exposed > 0) {
                    $neg_loads_val  = abs(($adjusted_neg_array[$i] * $spanExposed[$i] * $temp_val)) / (144 * $no_of_rails_exposed);
                }

            }
            $loads_array['negative'][]  = round($neg_loads_val,4);



            // For non exposed negative pressure
            // =IF($G29="X","X",ABS((F57*J73*(IF($D$12="PORTRAIT",$D$10,$D$11)))/(144*$I$71)))

            if($zone_neg_array[$i] == "X" || $zone_neg_array[$i] == "x") {
                $non_neg_loads_val = "X";
            }
            else {
                if($module_orientation == "PORTRAIT") {
                    $temp_val = $module_length;
                }
                else {
                    $temp_val = $module_width;
                }
                if($no_of_rails_non_exposed > 0) {
                    $non_neg_loads_val  = abs(($adjusted_non_neg_array[$i] * $spanNonExposed[$i] * $temp_val)) / (144 * $no_of_rails_non_exposed);
                }

            }
            $loads_array['non_negative'][]  = round($non_neg_loads_val,4);

            // For positive

            if($zone_pos_array[$i] == "X" || $zone_pos_array[$i] == "x") {
                $pos_loads_val = "X";
            }
            else {
                $pos_loads_val  = $adjusted_pos_array[$i] * max($spanExposed[$i],$spanNonExposed[$i]);

                $pos_loads_val =  $pos_loads_val * $temp_val;
                if($no_of_rails > 0) {
                    $pos_loads_val =  $pos_loads_val / (144 * $no_of_rails);
                }
            }
            $loads_array['positive'][]  = round($pos_loads_val,4);
        }
        $loads_array['spanExposed'] = $spanExposed;
        $loads_array['spanNonExposed'] = $spanNonExposed;
        // $data['result'] = 1;
        // $data['pressure_array'] = $loads_array;
        return $loads_array;

    }
    public function attachment_loads_7_10($zone_pos_array,$zone_neg_array,$adjusted_pos_array,$adjusted_neg_array,$module_orientation,$module_length,$module_width,$no_of_rails,$limit_max_span_to,$rafter_seam_spacing,$compression_strength)
    {

        // For positive loads
        // =IF(F29="X","X",(D57*MAX(H73,J73)*(IF($D$12="PORTRAIT",$D$10,$D$11)))/(144*$I$71))
        // d57 = adjusted_pos_array;
        // H73 = spanExposed;
        // j73 = spanNonExposed;
        // d12 = module_orientation;
        // d10 = module_length;
        // d11 = module_width;
        // i71 = no_of_rails;


        $spanExposed = array(0,0,0);
        $spanNonExposed = array(0,0,0);


        // print_r($zone_pos_array);
        // print_r($zone_neg_array);
        // print_r($adjusted_pos_array);
        // print_r($adjusted_neg_array);
        // print_r($adjusted_non_neg_array);
        // exit;
        $non_neg_loads_val = 0;
        $neg_loads_val = 0;
        $pos_loads_val = 0;

        for($i = 0; $i <= 2; $i++) {

            if($module_orientation == "PORTRAIT") {
                $temp_val = $module_length;
            }
            else {
                $temp_val = $module_width;
            }

            $start = $rafter_seam_spacing;
            $end = $limit_max_span_to;

            $spanExposed[$i] = $start;

            $count = 1;
            if($start > 0 && $end > 0) {
                while($start <= $end) {

                    if($no_of_rails > 0) {
                        $neg_loads_val  = abs(($adjusted_neg_array[$i] * $start * $temp_val)) / (144 * $no_of_rails);
                    }
                    if($neg_loads_val <= $compression_strength) {
                        $spanExposed[$i] = $start;
                        // $start = $start * 2;
                        $start = $start + $rafter_seam_spacing;
                    }else {
                        break;
                    }

                    $count++;
                }
            }



            $start = $rafter_seam_spacing;
            $end   = $limit_max_span_to;

            $spanNonExposed[$i] = $start;
            if($start > 0 && $end > 0) {
                while($start <= $end) {

                    if($no_of_rails > 0) {
                        $non_neg_loads_val  = abs(($adjusted_neg_array[$i] * $start * $temp_val)) / (144 * $no_of_rails);
                    }
                    // echo "start -- ".$start. " >>> ".$non_neg_loads_val;
                    if($non_neg_loads_val <= $compression_strength) {
                        $spanNonExposed[$i] = $start;
                        // $start = $start * 2;
                        $start = $start + $rafter_seam_spacing;
                    }
                    else {
                        break;
                    }
                }
            }


            // For exposed negative pressure
            // =IF($G29="X","X",ABS((E57*H73*(IF($D$12="PORTRAIT",$D$10,$D$11)))/(144*$I$71)))
            // e57 = adjusted_neg_array
            // h73 = adjusted_neg_array
            if($zone_neg_array[$i] == "X" || $zone_neg_array[$i] == "x") {
            $neg_loads_val = "X";
            }
            else {
                if($no_of_rails > 0) {
                    $neg_loads_val  = abs(($adjusted_neg_array[$i] * $spanExposed[$i] * $temp_val)) / (144 * $no_of_rails);
                }

            }
            $loads_array['negative'][]  = round($neg_loads_val,4);

            // For positive

            if($zone_pos_array[$i] == "X" || $zone_pos_array[$i] == "x") {
                $pos_loads_val = "X";
            }
            else {
                $pos_loads_val  = $adjusted_pos_array[$i] * max($spanExposed[$i],$spanNonExposed[$i]);

                $pos_loads_val =  $pos_loads_val * $temp_val;
                if($no_of_rails > 0) {
                    $pos_loads_val =  $pos_loads_val / (144 * $no_of_rails);
                }
            }
            $loads_array['positive'][]  = round($pos_loads_val,4);
        }
        $loads_array['spanExposed'] = $spanExposed;
        $loads_array['spanNonExposed'] = $spanNonExposed;
        // $data['result'] = 1;
        // $data['pressure_array'] = $loads_array;
        return $loads_array;

    }
    public function adjusted_design_pressure_7_10($zone_pos_array,$zone_neg_array,$design_pos_array,$design_neg_array,$dead_load,$solar_panel_pressure,$srsl)
    {
        // =IF(F29="X","X",MAX(($D$15+(0.45*D41*$I$51)+(0.75*$D$16)),16))
        // d15 = dead_load;
        // d16 = dead_load;
        // d41 = design_pos_array_zone1;
        // i51 = solar_panel_pressure;
        // e41 = design_neg_array_zone1;
        $internal_pressure_coefficient = config('constants.internal_pressure_coefficient');

        $array_edge_factor_exposed = config('constants.array_edge_factor_exposed');
        $array_edge_factor_non_exposed = config('constants.array_edge_factor_non_exposed');


        for($i = 0; $i <= 2; $i++) {

            $pos_pressure_val = $zone_pos_array[$i];

            if($zone_pos_array[$i] == "X" || $zone_pos_array[$i] == "x") {
                $pos_pressure_val = "X";
            }
            else {
                $pos_pressure_val  = max(($dead_load + ( 0.45 * $design_pos_array[$i] * $solar_panel_pressure) + (0.75 * $srsl)),16);
            }

            $pressure_array['positive'][]  = round($pos_pressure_val,4);

            // For exposed negative pressure
            // =IF(G29="X","X",MIN(E41*$E$51,-16))

            if($zone_neg_array[$i] == "X" || $zone_neg_array[$i] == "x") {
            $neg_pressure_val = "X";
            }
            else {
                // $neg_pressure_val  = min(($design_neg_array[$i] * $array_edge_factor_exposed),-16);
                $neg_pressure_val  = min(($design_neg_array[$i] * $array_edge_factor_exposed * $solar_panel_pressure),-16);

            }
            $pressure_array['negative'][]  = round($neg_pressure_val,4);

            // For non exposed negative pressure
            // =IF(G29="X","X",MIN(E41*$E$52,-16))

            if($zone_neg_array[$i] == "X" || $zone_neg_array[$i] == "x") {
                $non_neg_pressure_val = "X";
            }
            else {
                // $non_neg_pressure_val  = min(($design_neg_array[$i] * $array_edge_factor_non_exposed),-16);
                $non_neg_pressure_val  = min(($design_neg_array[$i] * $array_edge_factor_non_exposed * $solar_panel_pressure),-16);

            }
            $pressure_array['non_negative'][]  = round($non_neg_pressure_val,4);
        }
        // $data['result'] = 1;
        // $data['pressure_array'] = $pressure_array;
        return $pressure_array;

    }







///////////////////////////////////////////////////////////////////////
}


