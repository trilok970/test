<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;

use App\Models\Login;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Hash;
use DB;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Events\PasswordReset;

class LoginController extends Controller
{
    public function login()
    {
        if(Auth::check())
        return redirect('recent-projects');
        return view('front.login.login');
    }
    public function loginPost(Request $request)
    {
        $validator = request()->validate([
            'email'=>'required|email',
            'password'=>'required',
        ]);

        $count = User::where(['email'=>$request->email])->count();

        if($count > 0)
        {
            if(Auth::attempt(['email'=>$request->email,'password'=>$request->password]))
            {
                return redirect('recent-projects');
            }
            else
            {
                return back()->with('error_message','Email or password are incorrect');
            }
        }
        else
        {
            return back()->with('error_message','Email or password are incorrect');
        }


    }
    public function checkUser($email)
    {
       return $count = User::where(['is_deleted'=>0,'email'=>$email])->count();
    }
    public function signup()
    {
        $codes = DB::table('country')->where('phonecode','>',0)->get();
        return view('front.login.signup',compact('codes'));
    }
    public function signupPost(Request $request)
    {


        $validator = Validator::make($request->all(),[
            'first_name'=>'required|max:255',
            'last_name'=>'required|max:255',
            'email'=>'required|email',
            'country_code'=>'required',
            'phone_number'=>'required|max:15',
            'password'=>'required|min:8|max:45|required_with:password_confirmation|confirmed',
            'password_confirmation'=>'required',
            'tac'=>'accepted',
        ],[
            'tac.accepted'=>'The terms and conditions must be accepted.'
        ]);
            if($validator->fails())
            {
                return back()
                ->withInput()
                ->withErrors($validator);
            }
        if($this->checkUser($request->email) > 0)
        {
            $validator = $validator->errors()->add('email','The email has already been taken.');
            return back()->withInput()->withErrors($validator);
        }

        $user = new User;
        $user->first_name = ucfirst($request->first_name);
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->country_code = $request->country_code;
        $user->phone_number = $request->phone_number;
        $user->password = Hash::make($request->password);
        if($user->save())
        {
            return redirect('login')->with('message','Your registration process has been done. You can login here...');
        }
        else
        {
            return back()->with('error_message','Something went wrong');
        }
    }

    public function forgotPassword()
    {
        return view('front.login.forgot-password');
    }
    public function forgotPasswordPost(Request $request)
    {
        $credentials = request()->validate([
            'email'=>'required|email',
        ]);

        $count = User::where(['email'=>$request->email,'is_deleted'=>0])->count();

        if($count > 0)
        {
            Password::sendResetLink($credentials);
            return redirect('login')->with('message','Your password reset link sent to your registered e-mail id');

        }
        else
        {
            return back()->with('error_message','This e-mail id is not registered!')->withInput();
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
    public function resetPassword(Request $request,$token)
    {
            $email = $request->email;
            return view('front.login.reset-password',compact('token','email'));
    }
    public function resetPasswordPost(Request $request)
    {


            $request->validate([
                'token' => 'required',
                'email' => 'required|email',
                'password' => 'required|min:8|confirmed',
            ]);

            $status = Password::reset(
                $request->only('email', 'password', 'password_confirmation', 'token'),
                function ($user, $password) use ($request) {
                    $user->forceFill([
                        'password' => Hash::make($password)
                    ])->save();

                    $user->setRememberToken(Str::random(60));

                    event(new PasswordReset($user));
                }
            );

return $status == Password::PASSWORD_RESET ? redirect()->route('login')->with('error_message', __($status))
                        : back()->withErrors(['email' => [__($status)]])->with('error_message', __($status));



    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Login  $login
     * @return \Illuminate\Http\Response
     */
    public function show(Login $login)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Login  $login
     * @return \Illuminate\Http\Response
     */
    public function edit(Login $login)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Login  $login
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Login $login)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Login  $login
     * @return \Illuminate\Http\Response
     */
    public function destroy(Login $login)
    {
        //
    }
}
