<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;
use App\Models\InverterType;
use Illuminate\Http\Request;

class InverterTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InverterType  $inverterType
     * @return \Illuminate\Http\Response
     */
    public function show(InverterType $inverterType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InverterType  $inverterType
     * @return \Illuminate\Http\Response
     */
    public function edit(InverterType $inverterType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InverterType  $inverterType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InverterType $inverterType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InverterType  $inverterType
     * @return \Illuminate\Http\Response
     */
    public function destroy(InverterType $inverterType)
    {
        //
    }
}
