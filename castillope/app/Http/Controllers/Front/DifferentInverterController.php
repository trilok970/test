<?php

namespace App\Http\Controllers\Front;

use App\Models\DifferentInverter;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
class DifferentInverterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            //         echo "<pre>";
    //         print_r($_POST);
    // exit;
    $validator = Validator::make($request->all(),[
        'inverter_manufacturer_id.*'=>'required',
        'inverter_id.*'=>'required',

    ]);

    if($validator->fails())
    {

        $data['result'] = 0;
        $data['errors'] = $validator->errors();
    }
    else {
        $data['result'] = 1;
        $data['errors'] = '';
    }
    return response()->json($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DifferentInverter  $differentInverter
     * @return \Illuminate\Http\Response
     */
    public function show(DifferentInverter $differentInverter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DifferentInverter  $differentInverter
     * @return \Illuminate\Http\Response
     */
    public function edit(DifferentInverter $differentInverter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DifferentInverter  $differentInverter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DifferentInverter $differentInverter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DifferentInverter  $differentInverter
     * @return \Illuminate\Http\Response
     */
    public function destroy(DifferentInverter $differentInverter)
    {
        //
    }
}
