<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;
use App\Models\Ahj;
use App\Models\User;
use App\Models\Site;
use App\Models\Manufacturer;
use App\Models\Module;
use App\Models\Attachment;
use App\Models\AttachmentMethod;
use App\Models\Battery;
use App\Models\DifferentInverter;
use App\Models\Electrical;
use App\Models\ElectricalString;
use App\Models\Inverter;
use App\Models\PressureCoefficient;
use App\Models\Project;
use App\Models\RoofInfo;
use App\Models\InverterType;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Hash;
use DB;
use Illuminate\Support\Facades\Config;
use Artisan;
use App\Traits\SiteFormulaTrait;
use App\Models\BlockDesign;

class DashboardController extends Controller
{
    use SiteFormulaTrait;
    public $paginate_no;
    public function __construct()
    {
        $this->paginate_no = config('constants.paginate_no');
    }
    public function recentProjects()
    {
        return view('front.dashboard.recent-projects');
    }
    public function structural(Request $request,$id)
    {
        // $module_area = 18.16;
        // $this->calculate_asce_neg_gab($module_area);
        // $this->calculate_asce_pos_gab($module_area);
        // $this->calculate_asce_neg_hip($module_area);
        // $this->calculate_asce_pos_hip($module_area);
        // $roof_type = "HIP";$roof_slope_degree = 22.6;$effective_wind_area = 18.2;
        // $calculate_asce_pos_gab = $this->external_pressure_coefficient($roof_type,$roof_slope_degree,$effective_wind_area);
        // exit;

        $site = Site::where(['project_id'=>$id])->first();
        $roofinfos = RoofInfo::where(['project_id'=>$id])->get();
        if($site) {
            $pressure_coefficient = PressureCoefficient::where(['site_id'=>$site->id])->first();
        }
        else {
            $pressure_coefficient = '';
        }
        $manufacturers = Manufacturer::where('type','module')->get();
        $attachment_manufacturers = Manufacturer::where('type','attachment')->get();
        $attachments = Attachment::all();
        $attachment_methods = AttachmentMethod::all();

        $project_id = $id ?? 0;
        $project = Project::find($id);
        return view('front.dashboard.structural',compact('site','project_id','manufacturers','attachments','attachment_methods','roofinfos','pressure_coefficient','project','attachment_manufacturers'));
    }
    public function module_manufacturer($id)
    {
        $modules = Module::where(['manufacturer_id'=>$id])->get();
        if($modules)
        {
            $result ='';
            foreach($modules as $module)
            {
                $result .= '<option id="'.$module->id.'">'.$module->module_model.'</option>';
            }
            $data['result'] = 1;
            $data['data'] = $result;
            return $data;
        }

    }

    public function electrical(Request $request,$id)
    {

        $electrical = Electrical::where(['project_id'=>$id])->first();
        if($electrical) {
            $modules = Module::where(['manufacturer_id'=>$electrical->manufacturer_id])->get();
            // echo "<pre>";
            // print_r($modules);exit;
            $inverter_modules = Inverter::where(['manufacturer_id'=>$electrical->inverter_manufacturer_id])->get();
            $module = $this->get_module($electrical->module_id);

            $electrical_strings = ElectricalString::where(['is_deleted'=>0,'electrical_id'=>$electrical->id,'type'=>'string'])->get();
            if($electrical->no_of_inverters > 1) {
                $string_inverters = Inverter::where(['is_deleted'=>0])->whereIn('id',$electrical->DifferentInverter->pluck('inverter_id')->toArray())->orderBy('id','desc')->get();
            //     echo "<pre>";
            // print_r($electrical->DifferentInverter->pluck('inverter_id')->toArray());exit;
            }
            else if($electrical->no_of_inverters == 1) {
                $string_inverters = Inverter::where(['is_deleted'=>0])->where('id',$electrical->inverter_model_id)->orderBy('id','desc')->get();
            }

        }
        else {
            $pressure_coefficient = '';
            $modules = [];
            $inverter_modules = [];
            $module = '';
            $electrical_strings = [];
            $string_inverters = Inverter::where(['is_deleted'=>0])->orderBy('id','desc')->get();

        }
        $manufacturers = Manufacturer::where('type','module')->get();
        $inverter_manufacturers = Manufacturer::where('type','inverter')->get();
        $batteries = Battery::where(['is_deleted'=>0])->orderBy('id','desc')->get();
        $string_modules = Module::orderBy('id','desc')->get();
        $inverter_types = InverterType::where(['is_deleted'=>0,'status'=>1])->get();

        $project_id = $id ?? 0;
        return view('front.dashboard.electrical',compact('electrical','project_id','manufacturers','modules','inverter_modules','batteries','module','string_modules','string_inverters','electrical_strings','inverter_types','inverter_manufacturers'));
    }
    public function get_module($id) {
        return  Module::select('pmp','voc','vmpp','tvoc','isc','imp','tcpm')->where(['id'=>$id])->first();
    }
    public function get_inverter($id) {
        return  Inverter::where(['id'=>$id])->first();
    }
    public function electrical1(Request $request,$id)
    {


        $site = Site::where(['project_id'=>$id])->first();
        $roofinfos = RoofInfo::where(['project_id'=>$id])->get();
        if($site) {
            $pressure_coefficient = PressureCoefficient::where(['site_id'=>$site->id])->first();
        $modules = Module::where(['manufacturer_id'=>$site->manufacturer_id])->get();
        $inverter_modules = Module::where(['manufacturer_id'=>$site->inverter_manufacturer_id])->get();
        }
        else {
            $pressure_coefficient = '';
            $modules = [];
            $inverter_modules = [];

        }
        $manufacturers = Manufacturer::all();
        $attachments = Attachment::all();
        $attachment_methods = AttachmentMethod::all();

        $project_id = $id ?? 0;
        return view('front.dashboard.electrical1',compact('site','project_id','manufacturers','attachments','attachment_methods','roofinfos','pressure_coefficient','modules','inverter_modules'));
    }
    public function modules() {
        $modules = Module::select('module_model','manufacturer_id','pmp','voc','vmpp','tvoc','isc','imp','tcpm')->orderByDesc('id')->paginate($this->paginate_no);
        $data['page_title'] = "PV Modules";
        return view('front.dashboard.modules',compact('modules','data'));
    }
    public function ahj() {
        $ahjs = Ahj::orderByDesc('id')->paginate($this->paginate_no);
        $data['page_title'] = "AHJ";

        return view('front.dashboard.ahj',compact('ahjs','data'));
    }
    public function inverters() {
        $inverters = Inverter::where(['is_deleted'=>0])->orderByDesc('id')->paginate($this->paginate_no);
        $data['page_title'] = "Inverters";
        return view('front.dashboard.inverters',compact('inverters','data'));
    }
    public function project_detail_pdf($id)
    {
        $project = Project::find($id);

        $data = [
            'page_title' => 'Project Details',
            'date' => date('m/d/Y'),
            'project' => $project
        ];
        $html = view('front.dashboard.project-detail-pdf', compact('data'))->render();
        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            'format' => 'A4-L',
            'orientation' => 'L'
        ]);
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
    public function structural_project()
    {
        $data = [
            'page_title' => 'Structural Project',
            'date' => date('m/d/Y')
        ];
        return view('front.dashboard.structural-project', compact('data'));

    }
    public function electrical_project()
    {
        $data = [
            'page_title' => 'Electrical Project',
            'date' => date('m/d/Y')
        ];
        return view('front.dashboard.electrical-project', compact('data'));

    }
    public function create_new_project()
    {
        $data = [
            'page_title' => 'Create New Project',
            'date' => date('m/d/Y')
        ];
        return view('front.dashboard.create-new-project', compact('data'));

    }
    public function board()
    {
        $data = [
            'page_title' => 'Board',
            'date' => date('m/d/Y')
        ];
        return view('front.dashboard.board', compact('data'));

    }

    public function boardDemo()
    {
        $data = [
            'page_title' => 'Board',
            'date' => date('m/d/Y')
        ];
        return view('front.dashboard.board_demo', compact('data'));

    }

    public function konvasExample()
    {
        $data = [
            'page_title' => 'konvasExample',
            'date' => date('m/d/Y')
        ];
        return view('front.dashboard.konvasExample', compact('data'));

    }
    public function gojsExample()
    {
        $data = [
            'page_title' => 'gojsExample',
            'date' => date('m/d/Y')
        ];
        return view('front.dashboard.gojsExample', compact('data'));

    }
    public function blockDesign(Request $request)
    {
        if(!$request->project_id) {
            return redirect()->route('block.design.project');
        }
        $project_id = $request->project_id;
        $blockDesign = blockDesign::whereProjectId($project_id);
        $project = [];
        if($blockDesign->exists()){
            $project = $blockDesign->first();
        }

        $data = [
            'page_title' => 'Block Design',
            'date' => date('m/d/Y'),
            'project_id' => $project_id,
            
        ];
        return view('front.dashboard.block-design', compact('data','project'));

    }
    public function blockDesignProject()
    {
        $data = [
            'page_title' => 'Block Design Project',
            'date' => date('m/d/Y')
        ];
        return view('front.dashboard.block-design-project', compact('data'));

    }
    public function blockDesignPdf(Request $request)
    {
        if(blockDesign::whereProjectId($request->project_id)->exists()){
            $blockDesign = blockDesign::whereProjectId($request->project_id)->first();
        }else{
            $blockDesign = new blockDesign;    
        }

        if ($request->hasFile('pdf') && $request->pdf->isValid()) {
            $pdfArray  =  file_upload($request->pdf,"uploads/pdfs",'public');
            // echo "<pre>";
            // print_r($pdfArray);
            // exit;
            $blockDesign->file = $pdfArray['fileName'];
        } else {
            $blockDesign->file = 'uploads/default.pdf';
        }
 
        $blockDesign->project_id = $request->project_id ?? 0;
        $blockDesign->model_json = $request->model_json ?? NULL;
        if($blockDesign->save()) {
            return response()->json(['status'=>1,'message'=>'Pdf uploaded successfully']);
        }
        else {
            return response()->json(['status'=>0,'message'=>'Pdf not uploaded.']);
        }


    }
    public function blockDesignReports(Request $request,$project_id)
    {
        $project = Project::where('id',$project_id)->first();
        $blockDesigns = BlockDesign::where('project_id',$project_id)->orderBy('id','desc')->paginate(10);
        // echo "<pre>";
        //     print_r($blockDesigns);
        //     exit;
        $data = [
            'page_title' => 'Block Design Reports',
            'date' => date('m/d/Y'),
            'blockDesigns' => $blockDesigns,
            'project' => $project
        ];
        return view('front.dashboard.block-design-reports', compact('data'));

    }



    //////////////////////////////////////////////////
}
