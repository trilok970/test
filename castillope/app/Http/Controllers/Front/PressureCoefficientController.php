<?php

namespace App\Http\Controllers;

use App\Models\PressureCoefficient;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PressureCoefficientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PressureCoefficient  $pressureCoefficient
     * @return \Illuminate\Http\Response
     */
    public function show(PressureCoefficient $pressureCoefficient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PressureCoefficient  $pressureCoefficient
     * @return \Illuminate\Http\Response
     */
    public function edit(PressureCoefficient $pressureCoefficient)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PressureCoefficient  $pressureCoefficient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PressureCoefficient $pressureCoefficient)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PressureCoefficient  $pressureCoefficient
     * @return \Illuminate\Http\Response
     */
    public function destroy(PressureCoefficient $pressureCoefficient)
    {
        //
    }
}
