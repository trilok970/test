<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;

use App\Models\RoofInfo;
use App\Models\Site;
use Illuminate\Http\Request;
use Validator;
class RoofInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function site($project_id)
    {
        return Site::where('project_id',$project_id)->first();
    }
    public function store(Request $request)
    {

    //         echo "<pre>";
    //         print_r($_POST);
    // exit;
        $validator = Validator::make($request->all(),[
            'project_id'=>'required|numeric',
            'manufacturer_id.*'=>'required',
            'attachment_manufacturer_id.*'=>'required',
            'module_id.*'=>'required',
            'attachment_id.*'=>'required',
            'module_length.*'=>'required',
            'module_area.*'=>'required',
            'attachment_type.*'=>'required',
            'module_width.*'=>'required',
            'compression_strength.*'=>'required',
            'module_orientation.*'=>'required',
            'pullout_strength.*'=>'required',
            'roof_height.*'=>'required',
            // 'limit_max_span_to.*'=>'required',
            'rafter_seam_spacing.*'=>'required',
            'roof_slope.*'=>'required',
            'roof_slope_degree.*'=>'required',
            // 'no_of_rails.*'=>'required',
            'effective_wind_area.*'=>'required',

        ]);
        $validator->sometimes(['no_of_rails_exposed.*','no_of_rails_non_exposed.*'], 'required', function ($request) {
            return $request->asce_version === 'asce-7-16';
        });
        // $validator->sometimes('no_of_rails.*','required',function($request) {
        //     return $request->asce_version === 'asce-7-10';
        // });
        if($validator->fails())
        {

            $data['result'] = 0;
            $data['errors'] = $validator->errors();
            return response()->json($data);
        }

        $i = 0;
        $site = $this->site($request->project_id);
        if($request->roof_info_id) {
            RoofInfo::where('project_id',$request->project_id)->whereNotIn('id',$request->roof_info_id)->delete();

            foreach($request->manufacturer_id as $val) {

            $roof_info =  RoofInfo::where('id',$request->roof_info_id[$i] ?? 0)->first();
            if($roof_info) {
                $roof_info = $roof_info;
            }
            else {
                $roof_info = new RoofInfo;
            }

            $roof_info->project_id = $request->project_id ?? 0;
            $roof_info->manufacturer_id = $request->manufacturer_id[$i] ?? 0;
            $roof_info->attachment_manufacturer_id = $request->attachment_manufacturer_id[$i] ?? 0;
            $roof_info->module_id = $request->module_id[$i] ?? 0;
            $roof_info->attachment_id = $request->attachment_id[$i] ?? 0;
            $roof_info->module_length = $request->module_length[$i] ?? 0;
            $roof_info->attachment_type = $request->attachment_type[$i] ?? "";
            $roof_info->module_width = $request->module_width[$i] ?? 0;
            $roof_info->module_area = $request->module_area[$i] ?? 0;
            $roof_info->compression_strength = $request->compression_strength[$i] ?? 0;
            $roof_info->module_orientation = $request->module_orientation[$i] ?? "";
            $roof_info->pullout_strength = $request->pullout_strength[$i] ?? 0;
            $roof_info->roof_height = $request->roof_height[$i] ?? 0;
            $roof_info->no_of_rails = $request->no_of_rails[$i] ?? 0;
            $roof_info->no_of_rails_exposed = $request->no_of_rails_exposed[$i] ?? 0;
            $roof_info->no_of_rails_non_exposed = $request->no_of_rails_non_exposed[$i] ?? 0;
            $roof_info->limit_max_span_to = $request->limit_max_span_to[$i] ?? 0;
            $roof_info->rafter_seam_spacing = $request->rafter_seam_spacing[$i] ?? 0;
            $roof_info->roof_slope = $request->roof_slope[$i] ?? 0;
            $roof_info->roof_slope_degree = $request->roof_slope_degree[$i] ?? 0;
            $roof_info->effective_wind_area = $request->effective_wind_area[$i] ?? 0;
            if($site->asce_version == 'asce-7-16') {
                for($j=0;$j<=6;$j++) {
                    $roof_info->{"ext_pos_pre_coef$j"}= $this->check_float($request->{"ext_pos_pre_coef$j"}[$i]) ?? 0;
                    $roof_info->{"ext_neg_pre_coef$j"} = $this->check_float($request->{"ext_neg_pre_coef$j"}[$i]) ?? 0;
                    $roof_info->{"zone_kz$j"} = $this->check_float($request->{"zone_kz$j"}[$i]) ?? 0;
                    $roof_info->{"positive_pressure$j"} = $this->check_float($request->{"positive_pressure$j"}[$i]) ?? 0;
                    $roof_info->{"negative_pressure$j"} = $this->check_float($request->{"negative_pressure$j"}[$i]) ?? 0;
                    $roof_info->{"adjusted_pos_pre$j"} = $this->check_float($request->{"adjusted_pos_pre$j"}[$i]) ?? 0;
                    $roof_info->{"adjusted_exp_neg_pre$j"} = $this->check_float($request->{"adjusted_exp_neg_pre$j"}[$i]) ?? 0;
                    $roof_info->{"adjusted_non_exp_neg_pre$j"} = $this->check_float($request->{"adjusted_non_exp_neg_pre$j"}[$i]) ?? 0;
                    $roof_info->{"positive_loading$j"} = $this->check_float($request->{"positive_loading$j"}[$i]) ?? 0;
                    $roof_info->{"negative_exposed_loading$j"} = $this->check_float($request->{"negative_exposed_loading$j"}[$i]) ?? 0;
                    $roof_info->{"negative_non_exposed_loading$j"} = $this->check_float($request->{"negative_non_exposed_loading$j"}[$i]) ?? 0;
                    $roof_info->{"attachment_exposed_negative_spacing$j"} = $this->check_float($request->{"attachment_exposed_negative_spacing$j"}[$i]) ?? 0;
                    $roof_info->{"attachment_non_exposed_negative_spacing$j"} = $this->check_float($request->{"attachment_non_exposed_negative_spacing$j"}[$i]) ?? 0;
                }
            }
            if($site->asce_version == 'asce-7-10') {
                for($j=0;$j<=2;$j++) {
                    $roof_info->{"ext_pos_pre_coef$j"."_7_10"}= $this->check_float($request->{"ext_pos_pre_coef$j"."_7_10"}[$i]) ?? 0;
                    $roof_info->{"ext_neg_pre_coef$j"."_7_10"} = $this->check_float($request->{"ext_neg_pre_coef$j"."_7_10"}[$i]) ?? 0;
                    $roof_info->{"positive_pressure$j"."_7_10"} = $this->check_float($request->{"positive_pressure$j"."_7_10"}[$i]) ?? 0;
                    $roof_info->{"negative_pressure$j"."_7_10"} = $this->check_float($request->{"negative_pressure$j"."_7_10"}[$i]) ?? 0;
                    $roof_info->{"positive_loading$j"."_7_10"} = $this->check_float($request->{"positive_loading$j"."_7_10"}[$i]) ?? 0;
                    $roof_info->{"negative_loading$j"."_7_10"} = $this->check_float($request->{"negative_loading$j"."_7_10"}[$i]) ?? 0;
                    $roof_info->{"attachment_negative_spacing$j"."_7_10"} = $this->check_float($request->{"attachment_negative_spacing$j"."_7_10"}[$i]) ?? 0;
                    $roof_info->{"attachment_positive_spacing$j"."_7_10"} = $this->check_float($request->{"attachment_positive_spacing$j"."_7_10"}[$i]) ?? 0;
                   }
            }

            $roof_info->save();

            $i++;
            }

            $site = Site::where('project_id',$request->project_id)->first();
            $site->no_of_roofs = count($request->manufacturer_id);
            $site->save();
        }
        else {
            RoofInfo::where('project_id',$request->project_id)->delete();
            $site = Site::where('project_id',$request->project_id)->first();
            $site->no_of_roofs = 1;
            $site->save();
        }

        $data['result'] = 1;
        $data['data'] = $roof_info ?? "";
        $data['message'] = '<p class="alert alert-success">Roof info saved successfully.</p>';
        return response()->json($data);



    }
    public function check_float($val)
    {
        if($val == "X" || $val == 'x') {
            return 0;
        }
        else {
            return $val;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RoofInfo  $RoofInfo
     * @return \Illuminate\Http\Response
     */
    public function show(RoofInfo $Roofinfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RoofInfo  $RoofInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(RoofInfo $Roofinfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RoofInfo  $RoofInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RoofInfo $Roofinfo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RoofInfo  $RoofInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(RoofInfo $Roofinfo)
    {
        //
    }
}
