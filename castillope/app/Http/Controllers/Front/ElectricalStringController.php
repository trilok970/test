<?php

namespace App\Http\Controllers\Front;

use App\Models\ElectricalString;
use App\Models\CombinerString;
use App\Http\Controllers\Controller;
use App\Models\OptimizerString;
use App\Traits\ElectricalFormulaTrait;
use Illuminate\Http\Request;
use Validator;
use Session;
class ElectricalStringController extends Controller
{
    use ElectricalFormulaTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//         $duplicate_string_value = $this->getLastInsertIdForDuplicate($request->electrical_id);
// exit;
        // echo $this->getLastInsertId($request->electrical_id);
        // exit;
        // echo "<pre>";
        // print_r($_POST);
        // exit;
        $module_id = $request->module_id;
        $inverter_id = $request->inverter_id;
        $size = $request->size;
        $string_id = $request->string_id;
        $size_warning_message = $request->size_warning_message;
        $string_id_duplicate = $request->string_id_duplicate;
        array_shift($module_id);
        array_shift($inverter_id);
        array_shift($size);
        array_shift($string_id);
        array_shift($size_warning_message);
        array_shift($string_id_duplicate);
        $request->module_id = $module_id;
        $request->inverter_id = $inverter_id;
        $request->size = $size;
        $request->string_id = $string_id;
        $request->size_warning_message = $size_warning_message;
        $request->string_id_duplicate = $string_id_duplicate;
        $requestData = $request->all();
        $requestData['module_id'] = $module_id;
        $requestData['inverter_id'] = $inverter_id;
        $requestData['size'] = $size;
        $requestData['string_id'] = $string_id;
        $requestData['size_warning_message'] = $size_warning_message;
        $requestData['string_id_duplicate'] = $string_id_duplicate;
            // echo "<pre>";
            // print_r($requestData);
            // exit;
           
    $validator = Validator::make($requestData,[
        'module_id.*'=>'required',
        'inverter_id.*'=>'required',
        'size.*'=>'required',
    ]);

    if($validator->fails())
    {

        $data['result'] = 0;
        $data['errors'] = $validator->errors();
        return response()->json($data);

    }
    if($request->module_id && count($request->module_id) > 0) {
        $i = 0;
    // echo "<pre>";
    // print_r($request->string_id);
    // exit;
        $duplicate_string_value = 0;
        $arr = [];
        ElectricalString::where(['electrical_id'=>$request->electrical_id,'type'=>'string'])->whereNotIn('id',$request->string_id)->delete();
        foreach($request->module_id as $value) {
                $electricalstring = ElectricalString::where('id',$request->string_id[$i])->first();
                if($electricalstring) {
                    $electricalstring = $electricalstring;
                }
                else {
                    $electricalstring = new ElectricalString();
                }
                $electricalstring->electrical_id = $request->electrical_id ?? 0;
                $electricalstring->module_id = $request->module_id[$i] ?? 0;
                $electricalstring->inverter_id = $request->inverter_id[$i] ?? 0;
                $electricalstring->size = $request->size[$i] ?? 0;
                $electricalstring->size_warning_message = $request->size_warning_message[$i] ?? "";
                // $electricalstring->string_id_duplicate = $request->string_id_duplicate[$i] ?? "";
                if($request->string_id_duplicate[$i] == 1) {
                    $duplicate_string_value = $this->getLastInsertIdForDuplicate($request->electrical_id);
                    $arr[] = $this->getLastInsertIdForDuplicate($request->electrical_id);
                }
                else if($request->string_id_duplicate[$i] > 1) {
                    $duplicate_string_value = $request->string_id_duplicate[$i];
                }
                else {
                    $duplicate_string_value = 0;
                }
                $electricalstring->string_id_duplicate = $duplicate_string_value;
                $electricalstring->save();
        $i++;
        }
        // echo "<pre>";
        // print_r($arr);
        // exit;
        $data['result'] = 1;
        $data['data'] = $electricalstring;
        return response()->json($data);
    }






    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ElectricalString  $electricalString
     * @return \Illuminate\Http\Response
     */
    public function show(ElectricalString $electricalString)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ElectricalString  $electricalString
     * @return \Illuminate\Http\Response
     */
    public function edit(ElectricalString $electricalString)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ElectricalString  $electricalString
     * @return \Illuminate\Http\Response
     */
    public function getLastInsertId($electrical_id){
       return $electricalstring = ElectricalString::where(['electrical_id'=>$electrical_id])->latest()->first()->id;

    }
    public function getLastInsertIdForDuplicate($electrical_id){
        return $electricalstring = ElectricalString::where(['electrical_id'=>$electrical_id,'string_id_duplicate'=>0,'type'=>'string'])->orderBy('id','desc')->first()->id;
    }
    public function update(Request $request, ElectricalString $electricalString)
    {
        //   echo "<pre>";
        //     print_r($_POST);
        //     exit;
    $validator = Validator::make($request->all(),[
        // 'module_id.*'=>'required',
        // 'inverter_id.*'=>'required',
        // 'size.*'=>'required',
        'from_val.*'=>'required',
        'to_val.*'=>'required',
        'current.*'=>'required',
        'terminal_ampacity.*'=>'required',
        'wire_ampacity.*'=>'required',
        'no_of_conductors.*'=>'required',
        'sets_of_wire.*'=>'required',
        'no_of_conduits.*'=>'required',
        'conduit_location.*'=>'required',
        'conductor_material.*'=>'required',
        'conduit_derate.*'=>'required',
        'temp_derate.*'=>'required',
        'wire_size.*'=>'required',
        'corrected_wire_size.*'=>'required',
        'derated_ampacity.*'=>'required',
        'ground_wire.*'=>'required',
        'corrected_ground_wire.*'=>'required',
        'ocpd.*'=>'required',
        'conductor_type.*'=>'required',
        'vd.*'=>'required',
        'max_length.*'=>'required',
        'voltage.*'=>'required',
        'phase.*'=>'required',
        'conductor_ampacity.*'=>'required',
    ]);

    if($validator->fails())
    {

        $data['result'] = 0;
        $data['errors'] = $validator->errors();
        return response()->json($data);

    }
    if($request->from_val && count($request->from_val) > 0) {
        $i = 0;
        // ElectricalString::where('electrical_id',$request->electrical_id)->delete();
        foreach($request->from_val as $value) {

                $electricalstring = ElectricalString::find($request->id[$i]);
                if($electricalstring) {
                    $electricalstring = $electricalstring;
                }
                else {
                $electricalstring = new ElectricalString;

                }
                // $electricalstring->electrical_id = $request->electrical_id >> 0;
                // $electricalstring->module_id = $request->module_id[$i] ?? 0;
                // $electricalstring->inverter_id = $request->inverter_id[$i] ?? 0;
                // $electricalstring->size = $request->size[$i] ?? 0;
                $electricalstring->electrical_id = $request->electrical_id;
                $electricalstring->module_id = $request->module_id[$i] ?? "";
                $electricalstring->inverter_id = $request->inverter_id[$i] ?? "";
                $electricalstring->size = $request->size[$i] ?? "";
                $electricalstring->from_val = $request->from_val[$i] ?? "";
                $electricalstring->to_val   = $request->to_val[$i] ?? "";
                $electricalstring->terminal_ampacity = $request->terminal_ampacity[$i] ?? 0;
                $electricalstring->wire_ampacity = $request->wire_ampacity[$i] ?? 0;
                $electricalstring->no_of_conductors = $request->no_of_conductors[$i] ?? 0;
                $electricalstring->sets_of_wire = $request->sets_of_wire[$i] ?? 0;
                $electricalstring->no_of_conduits = $request->no_of_conduits[$i] ?? 0;
                $electricalstring->conduit_location = $request->conduit_location[$i] ?? 0;
                $electricalstring->conductor_material = $request->conductor_material[$i] ?? 0;
                $electricalstring->conduit_derate = $request->conduit_derate[$i] ?? 0;
                $electricalstring->temp_derate = $request->temp_derate[$i] ?? 0;
                $electricalstring->wire_size = $request->wire_size[$i] ?? 0;
                $electricalstring->corrected_wire_size = $request->corrected_wire_size[$i] ?? 0;
                $electricalstring->conductor_ampacity = $request->conductor_ampacity[$i] ?? 0;
                $electricalstring->derated_ampacity = $request->derated_ampacity[$i] ?? 0;
                $electricalstring->ground_wire = $request->ground_wire[$i] ?? 0;
                $electricalstring->corrected_ground_wire = $request->corrected_ground_wire[$i] ?? 0;
                $electricalstring->conductor_type = $request->conductor_type[$i] ?? 0;
                $electricalstring->vd = $request->vd[$i] ?? 0;
                $electricalstring->max_length = $request->max_length[$i] ?? 0;
                if($request->type[$i] == 'string') {
                    if($request->url_type == 'DC' && strtolower($electricalstring->inverter->InverterType->name) == 'string inverter' && strtolower($electricalstring->inverter->manufacturer->title) == 'solaredge')
                    {
                        $electricalstring->voltage_dc_solaredge_string_inverter = $request->voltage[$i] ?? "0";
                    }
                    if($request->url_type == 'AC' && strtolower($electricalstring->inverter->InverterType->name) == 'string inverter')
                    {
                        $electricalstring->voltage_ac_string_inverter = $request->voltage[$i] ?? "0";
                    }
                    if($request->url_type == 'DC' && strtolower($electricalstring->inverter->InverterType->name) == 'string inverter' && strtolower($electricalstring->inverter->manufacturer->title) != 'solaredge')
                    {
                        $electricalstring->voltage_dc_string_inverter = $request->voltage[$i] ?? "0";
                        $electricalstring->warning_message_voltage_dc_string_inverter = $request->warning_message_voltage_dc_string_inverter[$i] ?? "0";
                    }
                    if($request->url_type == 'DC' && strtolower($electricalstring->inverter->InverterType->name) == 'generac')
                    {
                        $electricalstring->voltage_dc_generac_inverter = $request->voltage[$i] ?? "0";
                        $electricalstring->warning_message_voltage_dc_generac_inverter = $request->warning_message_voltage_dc_generac_inverter[$i] ?? "0";
                    }
                    if(strtolower($electricalstring->inverter->InverterType->name) == 'microinverter')
                    {
                        $electricalstring->voltage = $request->voltage[$i] ?? "0";
                    }
                    // For current
                    if($request->url_type == 'DC' && strtolower($electricalstring->inverter->InverterType->name) == 'string inverter' && strtolower($electricalstring->inverter->manufacturer->title) == 'solaredge')
                    {
                        $electricalstring->current_dc_solaredge_string_inverter = $request->current[$i] ?? "0";
                    }
                    if($request->url_type == 'AC' && strtolower($electricalstring->inverter->InverterType->name) == 'string inverter')
                    {
                        $electricalstring->current_ac_string_inverter = $request->current[$i] ?? "0";
                    }
                    if($request->url_type == 'DC' && strtolower($electricalstring->inverter->InverterType->name) == 'string inverter' && strtolower($electricalstring->inverter->manufacturer->title) != 'solaredge')
                    {
                        $electricalstring->current_dc_string_inverter = $request->current[$i] ?? "0";
                    }
                    if($request->url_type == 'DC' && strtolower($electricalstring->inverter->InverterType->name) == 'generac')
                    {
                        $electricalstring->current_dc_generac_inverter = $request->current[$i] ?? "0";
                    }
                    if(strtolower($electricalstring->inverter->InverterType->name) == 'microinverter')
                    {
                        $electricalstring->current = $request->current[$i] ?? "0";
                    }
                }

                else {
                    $electricalstring->voltage = $request->voltage[$i] ?? "0";
                    $electricalstring->current = $request->current[$i] ?? "0";
                }

                if($request->url_type == 'AC' && $request->type[$i] == 'string') {
                $electricalstring->ocpd_ac = $request->ocpd[$i] ?? 0;
                }else {
                $electricalstring->ocpd = $request->ocpd[$i] ?? 0;
                }
                $electricalstring->phase = $request->phase[$i] ?? "";
                $electricalstring->warning_message = $request->warning_message[$i] ?? "";
                $electricalstring->warning_message_ter = $request->warning_message_ter[$i] ?? "";
                $electricalstring->warning_message_ocpd = $request->warning_message_ocpd[$i] ?? "";
                $electricalstring->warning_message_terminal_ampacity = $request->warning_message_terminal_ampacity[$i] ?? "";
                $electricalstring->type = $request->type[$i] ?? "";
                if(isset($request->string_checkbox_val[$request->id[$i]])) {
                    $electricalstring->string_checkbox_val = $request->id[$i];
                }
                $electricalstring->save();
        $i++;
        }
         $electricalstring->id = $this->getLastInsertId($request->electrical_id);
        // For combiner section
        if($request->add_type == 'combiner') {
            if($request->combiner_id) {
                $combiner_id = $request->combiner_id;
                CombinerString::where(['electrical_id'=>$request->electrical_id,'combiner_id'=>$combiner_id])->whereNotIn('electrical_string_id',array_keys($request->string_checkbox_val))->delete();
            }
            else {
                $combiner_id = $electricalstring->id;
            }
            if(isset($request->string_checkbox_val)) {
                foreach($request->string_checkbox_val as $key => $value) {
                    $combiner_string = CombinerString::where('electrical_string_id',$key)->first();
                    if($combiner_string) {
                        $combiner_string =  $combiner_string;
                    }
                    else {
                        $combiner_string = new CombinerString;
                    }
                    $combiner_string->combiner_id = $combiner_id;
                    $combiner_string->electrical_string_id = $key;
                    $combiner_string->electrical_id = $request->electrical_id;
                    $combiner_string->value = $value;
                    $combiner_string->save();
                }
            }
        }
        elseif($request->add_type == 'optimizer') {
            // For optimizer
    
            if($request->optimizer_id) {
                $optimizer_id = $request->optimizer_id;
                OptimizerString::where(['electrical_id'=>$request->electrical_id,'optimizer_id'=>$optimizer_id])->whereNotIn('electrical_string_id',array_keys($request->string_checkbox_val))->delete();
            }
            else {
                $optimizer_id = $electricalstring->id;
            }
            if(isset($request->string_checkbox_val)) {
                foreach($request->string_checkbox_val as $key => $value) {
                    $optimizer_string = OptimizerString::where('electrical_string_id',$key)->first();
                    if($optimizer_string) {
                        $optimizer_string =  $optimizer_string;
                    }
                    else {
                        $optimizer_string = new OptimizerString;
                    }
                    $optimizer_string->optimizer_id = $optimizer_id;
                    $optimizer_string->electrical_string_id = $key;
                    $optimizer_string->electrical_id = $request->electrical_id;
                    $optimizer_string->value = $value;
                    $optimizer_string->save();
                }
            }
        }
        $data['result'] = 1;
        $data['data'] = $electricalstring;
        Session::flash('message','Electrical string updated successfully.');

        return response()->json($data);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ElectricalString  $electricalString
     * @return \Illuminate\Http\Response
     */
    public function destroy(ElectricalString $electricalString)
    {
        //
    }
    public function electricalFormula(Request $request)
    {
      if($request->type == 'conduit_derate')
      return  $this->conduitDerateCalculation($request);
      if($request->type == 'temp_derate')
      return  $this->tempDerateCalculation($request);
      if($request->type == 'terminal_rating')
      return  $this->terminalRatingCalculation($request);
      if($request->type == 'conductor_ampacity')
      return  $this->ampacityRatingCalculation($request);
      if($request->type == 'derated_ampacity')
      return  $this->deratedAmpacityCalculation($request);
      if($request->type == 'ground_wire')
      return  $this->groundWireCalculation($request);
      if($request->type == 'corrected_ground_wire')
      return  $this->correctedGroundWireCalculation($request);
      if($request->type == 'max_length')
      return  $this->maxLengthCalculation($request);
      if($request->type == 'size')
      return  $this->sizeCalculation($request);
      if($request->type == 'combinerEdit')
      return  $this->combinerEdit($request);
      if($request->type == 'sizeInverter')
      return  $this->sizeInverter($request);
      if($request->type == 'optimizerEdit')
      return  $this->optimizerEdit($request);
      if($request->type == 'ocpd_value')
      return  $this->ocpd_value($request);
    }
}
