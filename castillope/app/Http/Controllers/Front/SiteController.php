<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;

use App\Models\Site;
use Illuminate\Http\Request;
use Validator;
use Session;
use DB;
use Auth;
use App\Models\Manufacturer;
use App\Models\Attachment;
use App\Models\AttachmentMethod;
use App\Models\Module;
use App\Models\PressureCoefficient;
use App\Models\RoofInfo;
use App\Traits\SiteFormulaTrait;
class SiteController extends Controller
{
    use SiteFormulaTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $data['result'] = 1;
        //     $data['data'] = '';
        //     $data['message'] = '<p class="alert alert-success">Sites details updated successfully.</p>';
        //     return response()->json($data);

        $validator = Validator::make($request->all(),[
            'project_id'=>'required|numeric',
            'asce_version'=>'required',
            'mean_roof_height'=>'required|numeric',
            'dead_load'=>'required|numeric',
            'live_load'=>'required|numeric',
            'seismic_load'=>'required|numeric',
            'ground_snow_load'=>'required|numeric',
            'exposure_factor'=>'required|numeric',
            'temperature_factor'=>'required|numeric',
            'importance_factor'=>'required|numeric',
            'slope_factor'=>'required|numeric',
            'wind_directionality_factor'=>'required|numeric',
            'topographic_factor'=>'required|numeric',
            'sloped_roof_snow_load'=>'required|numeric',
            // 'building_length'=>'required|numeric',
            // 'building_width'=>'required|numeric',
            'risk_category'=>'required',
            'exposure_category'=>'required',
            'ultimate_wind_speed'=>'required|numeric',
            'normal_wind_speed'=>'required|numeric',
            'roof_type'=>'required',
            'hvhz'=>'required',
            // 'type_of_building'=>'required',
            // 'zone_width'=>'required',
            'no_of_roofs'=>'required|numeric',
            'roof_length'=>'required|numeric',
            'roof_width'=>'required|numeric',
            'parapet_height'=>'required|numeric',
            // 'module_length'=>'required|numeric',
            // 'module_width'=>'required|numeric',
            // 'module_orientation'=>'required',
            // 'module_area'=>'required|numeric',
            'ground_elevation'=>'required|numeric',
            'ke'=>'required|numeric',
            'kz'=>'required|numeric',
            // 'site_roof_slope'=>'required|numeric',
            // 'effective_wind_area'=>'required|numeric',

        ]);
        if($validator->fails())
        {
            $data['result'] = 0;
            $data['errors'] = $validator->errors();
            return response()->json($data);
        }
        $site = new Site;
        $site->project_id = $request->project_id ?? 0;
        $site->asce_version = $request->asce_version ?? 0;
        $site->mean_roof_height = $request->mean_roof_height ?? 0;
        $site->dead_load = $request->dead_load ?? 0;
        $site->live_load = $request->live_load ?? 0;
        $site->seismic_load = $request->seismic_load ?? 0;
        $site->ground_snow_load = $request->ground_snow_load ?? 0;
        $site->exposure_factor = $request->exposure_factor ?? 0;
        $site->temperature_factor = $request->temperature_factor ?? 0;
        $site->importance_factor = $request->importance_factor ?? 0;
        $site->slope_factor = $request->slope_factor ?? 0;
        $site->wind_directionality_factor = $request->wind_directionality_factor ?? 0;
        $site->topographic_factor = $request->topographic_factor ?? 0;
        $site->sloped_roof_snow_load = $request->sloped_roof_snow_load ?? 0;
        $site->building_length = $request->building_length ?? 0;
        $site->building_width = $request->building_width ?? 0;
        $site->risk_category = $request->risk_category;
        $site->exposure_category = $request->exposure_category;
        $site->ultimate_wind_speed = $request->ultimate_wind_speed ?? 0;
        $site->normal_wind_speed = $request->normal_wind_speed ?? 0;
        $site->roof_type = $request->roof_type;
        $site->hvhz = $request->hvhz;
        $site->type_of_building = $request->type_of_building ?? "";
        $site->no_of_roofs = $request->no_of_roofs ?? 1;
        $site->zone_width = $request->zone_width ?? 0;
        $site->roof_length = $request->roof_length ?? 0;
        $site->roof_width = $request->roof_width ?? 0;
        $site->parapet_height = $request->parapet_height ?? 0;
        $site->ground_elevation = $request->ground_elevation ?? 0;
        $site->ke = $request->ke ?? 0;
        $site->kz = $request->kz ?? 0;
        $site->site_roof_slope = $request->site_roof_slope ?? 0;
        $site->effective_wind_area = $request->effective_wind_area ?? 0;
        if($site->save())
        {

            if($site->asce_version == 'asce-7-10') {
                $width_of_pressure_coefficient = $this->widthOfPressureCoefficient_7_10($site->mean_roof_height,$site->roof_length,$site->roof_width,$site->roof_slope_degree,$site->hvhz,$site->roof_type);
            }
            else {
                $width_of_pressure_coefficient = $this->widthOfPressureCoefficient($site->mean_roof_height,$site->roof_length,$site->roof_width,$site->roof_slope_degree,$site->hvhz,$site->roof_type);

            }
            $velocity_pressure = $this->velocity_pressure($site->kz,$site->topographic_factor,$site->wind_directionality_factor,$site->ke,$site->normal_wind_speed);

            $this->calculate_asce_neg_gab($site->module_area);
            $this->calculate_asce_pos_gab($site->module_area);
            $this->calculate_asce_neg_hip($site->module_area);
            $this->calculate_asce_pos_hip($site->module_area);

            // Add Pressure Coefficient
            $this->pressure_coefficient($site->id,$width_of_pressure_coefficient,$site->asce_version);

            $roof_info_count = RoofInfo::where('project_id',$site->project_id)->count();

            $data['result'] = 1;
            $data['data'] = $site;
            $data['width_of_pressure_coefficient'] = $width_of_pressure_coefficient;
            $data['velocity_pressure'] = $velocity_pressure;
            $data['roof_info_count'] = $roof_info_count;
            $data['message'] = '<p class="alert alert-success">Sites details added successfully.</p>';
            $data['url'] = url('site/'.$site->id);
            $data['method'] = "PUT";
            return response()->json($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function edit(Site $site)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Site $site)
    {

        $validator = Validator::make($request->all(),[
            'project_id'=>'required|numeric',
            'asce_version'=>'required',
            'mean_roof_height'=>'required|numeric',
            'dead_load'=>'required|numeric',
            'live_load'=>'required|numeric',
            'seismic_load'=>'required|numeric',
            'ground_snow_load'=>'required|numeric',
            'exposure_factor'=>'required|numeric',
            'temperature_factor'=>'required|numeric',
            'importance_factor'=>'required|numeric',
            'slope_factor'=>'required|numeric',
            'wind_directionality_factor'=>'required|numeric',
            'topographic_factor'=>'required|numeric',
            'sloped_roof_snow_load'=>'required|numeric',
            // 'building_length'=>'required|numeric',
            // 'building_width'=>'required|numeric',
            'risk_category'=>'required',
            'exposure_category'=>'required',
            'ultimate_wind_speed'=>'required|numeric',
            'normal_wind_speed'=>'required|numeric',
            'roof_type'=>'required',
            'hvhz'=>'required',
            // 'type_of_building'=>'required',
            // 'zone_width'=>'required',
            'no_of_roofs'=>'required|numeric',

            'roof_length'=>'required|numeric',
            'roof_width'=>'required|numeric',
            'parapet_height'=>'required|numeric',
            // 'module_length'=>'required|numeric',
            // 'module_width'=>'required|numeric',
            // 'module_orientation'=>'required',
            // 'module_area'=>'required|numeric',
            'ground_elevation'=>'required|numeric',
            'ke'=>'required|numeric',
            'kz'=>'required|numeric',
            // 'site_roof_slope'=>'required|numeric',
            // 'effective_wind_area'=>'required|numeric',
        ]);
        if($validator->fails())
        {
            $data['result'] = 0;
            $data['errors'] = $validator->errors();
            return response()->json($data);
        }
        $site->project_id = $request->project_id ?? 0;
        $site->asce_version = $request->asce_version ?? 0;
        $site->mean_roof_height = $request->mean_roof_height ?? 0;
        $site->dead_load = $request->dead_load ?? 0;
        $site->live_load = $request->live_load ?? 0;
        $site->seismic_load = $request->seismic_load ?? 0;
        $site->ground_snow_load = $request->ground_snow_load ?? 0;
        $site->exposure_factor = $request->exposure_factor ?? 0;
        $site->temperature_factor = $request->temperature_factor ?? 0;
        $site->importance_factor = $request->importance_factor ?? 0;
        $site->slope_factor = $request->slope_factor ?? 0;
        $site->wind_directionality_factor = $request->wind_directionality_factor ?? 0;
        $site->topographic_factor = $request->topographic_factor ?? 0;
        $site->sloped_roof_snow_load = $request->sloped_roof_snow_load ?? 0;
        $site->building_length = $request->building_length ?? 0;
        $site->building_width = $request->building_width ?? 0;
        $site->risk_category = $request->risk_category;
        $site->exposure_category = $request->exposure_category;
        $site->ultimate_wind_speed = $request->ultimate_wind_speed ?? 0;
        $site->normal_wind_speed = $request->normal_wind_speed ?? 0;
        $site->roof_type = $request->roof_type;
        $site->hvhz = $request->hvhz;
        $site->type_of_building = $request->type_of_building ?? "";
        $site->no_of_roofs = $request->no_of_roofs ?? 0;
        $site->zone_width = $request->zone_width ?? 0;

        $site->roof_length = $request->roof_length ?? 0;
        $site->roof_width = $request->roof_width ?? 0;
        $site->parapet_height = $request->parapet_height ?? 0;
        $site->ground_elevation = $request->ground_elevation ?? 0;
        $site->ke = $request->ke ?? 0;
        $site->kz = $request->kz ?? 0;
        $site->site_roof_slope = $request->site_roof_slope ?? 0;
        $site->effective_wind_area = $request->effective_wind_area ?? 0;

        if($site->save())
        {
            if($site->asce_version == 'asce-7-10') {
                $width_of_pressure_coefficient = $this->widthOfPressureCoefficient_7_10($site->mean_roof_height,$site->roof_length,$site->roof_width,$site->roof_slope_degree,$site->hvhz,$site->roof_type);
            }
            else {
                $width_of_pressure_coefficient = $this->widthOfPressureCoefficient($site->mean_roof_height,$site->roof_length,$site->roof_width,$site->roof_slope_degree,$site->hvhz,$site->roof_type);

            }

            $velocity_pressure = $this->velocity_pressure($site->kz,$site->topographic_factor,$site->wind_directionality_factor,$site->ke,$site->normal_wind_speed);

            $this->calculate_asce_neg_gab($site->module_area);
            $this->calculate_asce_pos_gab($site->module_area);
            $this->calculate_asce_neg_hip($site->module_area);
            $this->calculate_asce_pos_hip($site->module_area);

            // Add Pressure Coefficient
            $this->pressure_coefficient($site->id,$width_of_pressure_coefficient,$site->asce_version);

            $roof_info_count = RoofInfo::where('project_id',$site->project_id)->count();


            $data['result'] = 1;
            $data['data'] = $site;
            $data['width_of_pressure_coefficient'] = $width_of_pressure_coefficient;
            $data['velocity_pressure'] = $velocity_pressure;
            $data['roof_info_count'] = $roof_info_count;
            $data['message'] = '<p class="alert alert-success">Sites details updated successfully.</p>';
            $data['url'] = url('site/'.$site->id);
            $data['method'] = "PUT";
            return response()->json($data);
        }
    }
    public function pressure_coefficient($site_id,$width_of_pressure_coefficient,$asce_version)
    {
        $pressure_coefficient = PressureCoefficient::where('site_id',$site_id)->first();
        if($pressure_coefficient) {
            $pressure_coefficient = $pressure_coefficient;
        }
        else {
            $pressure_coefficient = new PressureCoefficient;
        }

        $pressure_coefficient->site_id = $site_id;
        $pressure_coefficient->wp1 = $width_of_pressure_coefficient["wp1"];
        $pressure_coefficient->wp2 = $width_of_pressure_coefficient["wp2"];
        $pressure_coefficient->wp1_val = $width_of_pressure_coefficient["wp1_val"];
        $pressure_coefficient->wp2_val = $width_of_pressure_coefficient["wp2_val"];
        $pressure_coefficient->zone1 = $width_of_pressure_coefficient["zone1"];
        if($asce_version == '7-16') {
            $pressure_coefficient->zone2 = $width_of_pressure_coefficient["zone2"];
            $pressure_coefficient->zone3 = $width_of_pressure_coefficient["zone3"];
        }
        $pressure_coefficient->save();


    }
    public function manufacture_module($id)
    {
        $modules = Module::where(['manufacturer_id'=>$id])->orderBy('id','desc')->get();
        $data = '<option value="">Select Pv Module</option>';

        if($modules)
        {
            foreach($modules as $module)
            {
                $data .= '<option manufacturer_id="'.$module->manufacturer_id.'" module_width="'.$module->module_width.'" module_lenght="'.$module->module_lenght.'" value="'.$module->id.'" >'.$module->module_model.'</option>';
            }
            $result['result'] = 1;
        }
        else
        {

            $result['result'] = 0;

        }
        $result['data'] = $data;
            return $result;
    }
    public function module($module_id,$manufacrer_id)
    {
        $attachments = Attachment::where(['manufacrer_id'=>$manufacrer_id,'model_id'=>$module_id])->get();
        $data = '<option value="">Select Attachment Manufacturer</option>';

        if($attachments)
        {
            foreach($attachments as $attachment)
            {
                $data .= '<option allowable_pollout_strength="'.$attachment->allowable_pollout_strength.'" allowable_compression_strength="'.$attachment->allowable_compression_strength.'" value="'.$attachment->id.'" >'.$attachment->title.'</option>';
            }
            $result['result'] = 1;
        }
        else
        {

            $result['result'] = 0;

        }
        $result['data'] = $data;
            return $result;
    }
    public function attachment($id)
    {
        $attachments = Attachment::where(['manufacturer_id'=>$id])->orderBy('id','desc')->get();
        $data = '<option value="">Select Attachment</option>';

        if($attachments)
        {
            foreach($attachments as $attachment)
            {
                $data .= '<option manufacturer_id="'.$attachment->manufacturer_id.'" allowable_pollout_strength="'.$attachment->allowable_pollout_strength.'" allowable_compression_strength="'.$attachment->allowable_compression_strength.'" value="'.$attachment->id.'" >'.$attachment->model_no.'</option>';
            }
            $result['result'] = 1;
        }
        else
        {

            $result['result'] = 0;

        }
        $result['data'] = $data;
            return $result;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function destroy(Site $site)
    {
        //
    }

    public function siteFormula(Request $request)
    {
      if($request->type == 'srsl')
      return  $this->srslCalculation($request);
      if($request->type == 'nws')
      return  $this->nwsCalculation($request);
      if($request->type == 'module_area')
      return $this->moduleAreaCalculation($request);
      if($request->type == 'roof_slope_degree')
      return $this->moduleRoofSlopeDegree($request);
      if($request->type == 'ke')
      return $this->moduleKe($request);
      if($request->type == 'kz')
      return $this->moduleKz($request);
      if($request->type == 'inner_roof_slope') {
          if($request->asce_version == 'asce-7-10') {
              return $this->external_pressure_coefficient_7_10($request);
          }
          else if($request->asce_version == 'asce-7-16') {
              return $this->external_pressure_coefficient($request);
          }

      }

    }
    public function structure_room_details(Request $request)
    {

    }




}
