<?php

namespace App\Http\Controllers;

use App\Models\AttachmentMethod;
use Illuminate\Http\Request;

class AttachmentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AttachmentMethod  $attachmentMethod
     * @return \Illuminate\Http\Response
     */
    public function show(AttachmentMethod $attachmentMethod)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AttachmentMethod  $attachmentMethod
     * @return \Illuminate\Http\Response
     */
    public function edit(AttachmentMethod $attachmentMethod)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AttachmentMethod  $attachmentMethod
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AttachmentMethod $attachmentMethod)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AttachmentMethod  $attachmentMethod
     * @return \Illuminate\Http\Response
     */
    public function destroy(AttachmentMethod $attachmentMethod)
    {
        //
    }
}
