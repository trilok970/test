<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;

use App\Models\Project;
use App\Models\Site;
use Illuminate\Http\Request;
use Validator;
use Session;
use Auth;
class ProjectController extends Controller
{
    public $paginate_no;
    public function __construct()
    {
        $this->paginate_no = config('constants.paginate_no');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $projects = Project::where(['is_deleted'=>0,'user_id'=>Auth::user()->id])->orderByDesc('id')->paginate($this->paginate_no);
        return view('front.dashboard.recent-projects',compact('projects'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'owner_name'=>'required|max:255',
            'address'=>'required',
            'state'=>'required|max:255',
            // 'ult_wind_speed'=>'required|max:255',
            // 'ground_snow_load'=>'required|max:255',
            'ahj'=>'required|max:255',
        ]);
        if($validator->fails())
        {
            $data['result'] = 0;
            $data['errors'] = $validator->errors();
            return response()->json($data);
        }
        $project = new Project;
        $project->owner_name = ucwords($request->owner_name);
        $project->address = $request->address;
        $project->state = $request->state;
        $project->ult_wind_speed = $request->ult_wind_speed ?? "";
        $project->ground_snow_load = $request->ground_snow_load ?? "";
        $project->ahj = $request->ahj;
        $project->user_id = Auth::user()->id;
        $project->type = str_replace("_report","",$request->type);
        if($project->save())
        {
            Session::flash('message','Project report submitted successfully.');

            $data['result'] = 1;
            $data['url'] = url($request->href).'/'.$project->id;
            return response()->json($data);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function getStructuralElectricalId($project_id)
    {
        $site = Site::where(['project_id'=>$project_id])->latest()->first()->id ?? 0;
        $electrical = 0;
        return array("site"=>$site,'electrical'=>$electrical);
    }
    public function edit(Project $project)
    {
        if(!$project)
        {
            $data['result'] = 0;
            $data['errors'] = "Project details not exist.";
            return response()->json($data);
        }
        else
        {
            $get_struc_elec_data = $this->getStructuralElectricalId($project->id);
            $data['result'] = 1;
            $data['data'] = $project;
            $data['structural_url'] = url('structural/'.$get_struc_elec_data['site'].'/edit').'?project_id='.$project->id;
            $data['electrical_url'] = url('structural/'.$get_struc_elec_data['electrical']).'?project_id='.$project->id;
            $data['url'] = url('project/'.$project->id);
            return response()->json($data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $validator = Validator::make($request->all(),[
            'owner_name'=>'required|max:255',
            'address'=>'required',
            'state'=>'required|max:255',
            // 'ult_wind_speed'=>'required|max:255',
            // 'ground_snow_load'=>'required|max:255',
            'ahj'=>'required|max:255',
        ]);
        if($validator->fails())
        {
            $data['result'] = 0;
            $data['errors'] = $validator->errors();
            return response()->json($data);
        }
        // echo "<pre>";print_r($project);exit;

        $project->owner_name = ucwords($request->owner_name);
        $project->address = $request->address;
        $project->state = $request->state;
        $project->ult_wind_speed = $request->ult_wind_speed ?? "";
        $project->ground_snow_load = $request->ground_snow_load ?? "";
        $project->ahj = $request->ahj;
        $project->user_id = Auth::user()->id;
        if($project->save())
        {
            Session::flash('message','Project report updated successfully.');
            $data['result'] = 1;
            $data['url'] = url()->previous();
            return response()->json($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        $project->is_deleted = 1;
        if($project->save()) {
            Session::flash('message','Project report deleted successfully.');
            $data['result'] = 1;
            return response()->json($data);
        }
    }
}
