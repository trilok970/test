<?php

namespace App\Http\Controllers;

use App\Models\BlockDesign;
use Illuminate\Http\Request;

class BlockDesignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BlockDesign  $blockDesign
     * @return \Illuminate\Http\Response
     */
    public function show(BlockDesign $blockDesign)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BlockDesign  $blockDesign
     * @return \Illuminate\Http\Response
     */
    public function edit(BlockDesign $blockDesign)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BlockDesign  $blockDesign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlockDesign $blockDesign)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BlockDesign  $blockDesign
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlockDesign $blockDesign)
    {
        //
    }
}
