<?php

namespace App\Http\Controllers\Front;

use App\Models\Electrical;
use App\Http\Controllers\Controller;
use App\Models\DifferentInverter;
use App\Models\ElectricalString;
use App\Models\Inverter;
use App\Models\Module;
use App\Traits\ElectricalFormulaTrait;
use Illuminate\Http\Request;
use Validator;

class ElectricalController extends Controller
{
    use ElectricalFormulaTrait;
    public function manufacture_inverter($id)
    {
        $inverters = Inverter::where(['manufacturer_id'=>$id])->get();
        if($inverters)
        {
            $result ='';
            $result .= '<option value="">Select Inverter Model</option>';

            foreach($inverters as $inverter)
            {
                $result .= '<option id="'.$inverter->id.'" value="'.$inverter->id.'">'.$inverter->model_no.'</option>';
            }
            $data['result'] = 1;
            $data['data'] = $result;
            return $data;
        }

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo "<pre>";
        // print_r($_POST);exit;
        $validator = Validator::make($request->all(),[
            'project_id'=>'required|numeric',
            'inverter_type_id'=>'required',
            'manufacturer_id'=>'required|numeric',
            'module_id'=>'required|numeric',
            // 'inverter_manufacturer_id'=>'required|numeric',
            // 'inverter_model_id'=>'required|numeric',
            'mixture_of_inverters'=>'required',
            // 'no_of_inverters'=>'required|numeric|min:1',
            // 'no_of_diff_inverters'=>'required|numeric',
            'battery'=>'required',
            // 'number'=>'required|numeric',
            'ambient_temp'=>'required|numeric',
            'minimum_temp'=>'required|numeric',

        ]);
        $validator->sometimes(['inverter_manufacturer_id','inverter_model_id'],'required',function($request){
            return $request->no_of_inverters == 1;
        });
        $validator->sometimes(['no_of_diff_inverters'],'required',function($request){
            return $request->no_of_inverters > 1;
        });
        $validator->sometimes(['no_of_inverters'], 'required|numeric', function ($request) {
            return $request->mixture_of_inverters === 'Yes';
        });
        $validator->sometimes(['number'], 'required', function ($request) {
            return $request->battery === 'Yes';
        });
        if($validator->fails())
        {
            $data['result'] = 0;
            $data['errors'] = $validator->errors();
            return response()->json($data);
        }
        if($request->id > 0) {
        $electrical = Electrical::where(['id'=>$request->id])->first();
        }
        else if($request->id == 0) {
        $electrical = new Electrical();
        }
        $electrical->project_id = $request->project_id ?? 0;
        $electrical->inverter_type_id = $request->inverter_type_id ?? 0;
        $electrical->manufacturer_id = $request->manufacturer_id ?? 0;
        $electrical->module_id = $request->module_id ?? 0;
        if($request->no_of_inverters === 1) {
            $electrical->inverter_manufacturer_id = $request->inverter_manufacturer_id ?? 0;
            $electrical->inverter_model_id = $request->inverter_model_id ?? 0;
        }
        $electrical->mixture_of_inverters = $request->mixture_of_inverters ?? "";
        $electrical->no_of_inverters = $request->no_of_inverters ?? 0;
        $electrical->no_of_diff_inverters = $request->no_of_diff_inverters ?? 0;
        $electrical->battery = $request->battery ?? "No";
        $electrical->number = $request->number ?? 0;
        $electrical->ambient_temp = $request->ambient_temp ?? 0;
        $electrical->minimum_temp = $request->minimum_temp ?? 0;
        $electrical->no_of_strings = $request->no_of_strings ?? 1;

        if($electrical->save())
        {
            if($request->no_of_inverters > 1) {
                if($request->inverter_id && count($request->inverter_id) > 0) {
                    $i = 0;
                    DifferentInverter::where('electrical_id',$electrical->id)->delete();
                    foreach($request->inverter_manufacturer_id as $value) {
                        $different_inverter = new DifferentInverter();
                        $different_inverter->electrical_id = $electrical->id >> 0;
                        $different_inverter->inverter_manufacturer_id = $request->inverter_manufacturer_id[$i] >> 0;
                        $different_inverter->inverter_id = $request->inverter_id[$i] >> 0;
                        $different_inverter->save();
                        $i++;

                    }
                }

            }
            $data['result'] = 1;
            $data['data'] = $electrical;
            $data['message'] = '<p class="alert alert-success">Electrical details added successfully.</p>';
            $data['module'] = $this->get_module($electrical->module_id);
            return response()->json($data);
        }
    }
    public function get_module($id) {
    return  Module::select('pmp','voc','vmpp','tvoc','isc','imp','tcpm')->where(['id'=>$id])->first();
    }

    public function dc_conductor_ampacity($id,$type)
    {
        $string_id_duplicate = 0;
        $optimizer = 0;

        if($type == 'AC' || $type == 'Battery') {
            $inverter_type_ids = [1,2,3,4];
            $string_id_duplicate = 1;
            $optimizer = 1;
        }
        else{
            $inverter_type_ids = [2,3,4];
        }

        $electrical = Electrical::where('id',$id)->first();
        $electrical_strings = ElectricalString::where('electrical_id',$id)
                            ->with(['electrical','module','inverter.InverterType'])
                            ->whereHas('inverter',function($q) use ($inverter_type_ids){
                                $q->whereIn('inverter_type_id',$inverter_type_ids);
                            })
                            ->when($string_id_duplicate, function($q) use ($string_id_duplicate) {
                                $q->where('string_id_duplicate',0);
                            })
                            ->when($optimizer, function($q) use ($optimizer) {
                                $q->where('type','!=','optimizer');
                            })
                            ->orderBy('id','asc')
                            ->get();
        // echo "<pre>";
        // print_r($electrical_strings->toArray());exit;
        $conduit_locations = config('constants.conduit_location');
        $awjs = config('constants.awj');
        $ocpd_rating = config('constants.ocpd_rating');
        $conductors = config('constants.conductors');

        return view('front.dashboard.dc-conductor-ampacity',compact('electrical_strings','conduit_locations','awjs','ocpd_rating','conductors','electrical','type'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Electrical  $electrical
     * @return \Illuminate\Http\Response
     */
    public function show(Electrical $electrical)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Electrical  $electrical
     * @return \Illuminate\Http\Response
     */
    public function edit(Electrical $electrical)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Electrical  $electrical
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Electrical $electrical)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Electrical  $electrical
     * @return \Illuminate\Http\Response
     */
    public function destroy(Electrical $electrical)
    {
        //
    }
}
