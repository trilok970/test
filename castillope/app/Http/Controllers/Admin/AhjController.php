<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Module;
use App\Models\Settings;
use App\Models\Admin;
use App\Models\Manufacturer;
use App\Models\Attachment;
use App\Models\Ahj;
use Illuminate\Http\Request;
use Auth;
use Session;
use DataTables;
use Validator;
use Hash;
use App\Lib\Uploader;

class AhjController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $title = "AHJ List";
        $breadcrumbs = [
            ['name'=>'AHJ List','relation'=>'Current','url'=>'']
        ];

		/*$pages = Attachment::with('manufacturer','module_model')->get();

		foreach($pages as $val){
			echo $val->manufacturer['title'];
		}


		pr($pages->ToArray());die; */

        return view('admin/ahj/index',compact('title','breadcrumbs'));
    }


		public function view($id)
		{
			$data = ($id)?Ahj::find($id):array();

		   $title = "AHJ Detail";
		   $title1 = $data->ahj." Detail";
			$breadcrumbs = [
                ['name'=>'AHJ','relation'=>'link','url'=>route('admin.ahj_index')],
                ['name'=>$title1,'relation'=>'Current','url'=>'']
            ];
			return view('admin/ahj/view',compact('title','breadcrumbs','data'));
		}

     public function datatables()
    {
        $pages = Ahj::get();
        return DataTables::of($pages)
            ->addColumn('action', function ($page) {
                return '<a href="'.route('admin.ahj_create',$page->id).'" class="btn btn-xs btn-info btn-sm"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;<a href="'.route('admin.ahj_view',$page->id).'" class="btn btn-xs btn-primary btn-sm"><i class="fa fa-eye"></i></a>&nbsp;|&nbsp;<a data-link="'.route('admin.ahj_delete').'" id="delete_'.$page->id.'" onclick="confirm_delete('.$page->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
            })
            ->editColumn('ahj',function($page){
                return $page->ahj;
            })
			->editColumn('state',function($page){
                return $page->state;
            })
			->editColumn('fire_setback',function($page){
						if($page->fire_setback == 1){
							$fireSetback = 'Yes';
						}else{
							$fireSetback = 'No';
						}
			   return $fireSetback;
            })
            ->rawColumns(['action','ahj','state','fire_setback'])
            ->make(true);
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id=null)
    {


	//pr($request->all());die;

        if($id){
            $title = "Edit AHJ";
            $breadcrumbs = [
                ['name'=>'AHJ','relation'=>'link','url'=>route('admin.ahj_index')],
                ['name'=>'Edit AHJ','relation'=>'Current','url'=>'']
            ];
        }else{
            $title = "Add New AHJ";
            $breadcrumbs = [
                ['name'=>'AHJ','relation'=>'link','url'=>route('admin.ahj_index')],
                ['name'=>'Add New AHJ','relation'=>'Current','url'=>'']
            ];
        }


        $data = ($id)?Ahj::find($id):array();


        if($request->ajax() && $request->isMethod('post')){
            try {
                $rules = [
                    'ahj'             => 'required',
                    'state'             => 'required',
                    'fire_setback'             => 'required',
                    'note'             => 'required',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                   // $formData = $request->except('image');
                   // $formData['question'] = $request->get('question');
                    $formData = $request->all();
                    if($id){
                        $data->update($formData);
						return ['status' => 'true', 'message' => 'AHJ updated successfully.'];
                        //Session::flash('success','AHJ updated successfully.');
                    }else{
                        Ahj::create($formData);
						return ['status' => 'true', 'message' => 'AHJ created successfully.'];
                        //Session::flash('success','AHJ created successfully.');
                    }
                   // return ['status' => 'true', 'message' => 'AHJ updated successfully.'];
                }
            } catch (\Exception $e) {
                return ['status' => false, 'message' => $e->getMessage()];
            }
        }
        return view('admin/ahj/add',compact('id','data','title','breadcrumbs'));
    }


 public function delete(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id;
            try{
                $delete = Ahj::where('id','=',$id)->delete();
                if($delete){
                    return ["status"=>"true","message"=>"Record Deleted."];
                }else{
                    return ["status"=>"false","message"=>"Could not deleted Record."];
                }
            }catch(\Exception $e){
                return ["status"=>"false","message"=>$e->getMessage()];
            }
        }
    }


}
