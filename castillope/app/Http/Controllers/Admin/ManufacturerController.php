<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Settings;
use App\Models\Admin;
use App\Models\Manufacturer;
use Illuminate\Http\Request;
use Auth;
use Session;
use Validator;
use DataTables;
use Hash;
use App\Lib\Uploader;
use App\Models\Inverter;

class ManufacturerController extends Controller
{



	public function index()
    {
       $title = "Manufacturer List";
        $breadcrumbs = [
            ['name'=>'Manufacturer List','relation'=>'Current','url'=>'']
        ];

        return view('admin/manufacturers/index',compact('title','breadcrumbs'));
    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	 public function datatables()
    {
        $pages = Manufacturer::get();
        return DataTables::of($pages)
            ->addColumn('action', function ($page) {
                return '<a href="'.route('admin.manufacturer_create',$page->id).'" class="btn btn-xs btn-primary btn-sm"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;<a data-link="'.route('admin.manufacturer_delete').'" id="delete_'.$page->id.'" onclick="confirm_delete('.$page->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
            })
            ->editColumn('title',function($page){
                return $page->title;
            })
            ->editColumn('type',function($page){
                return ucfirst($page->type);
            })
            ->rawColumns(['action','title','type'])
            ->make(true);
    }


	  /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id=null)
    {


	//pr($request->all());die;

        if($id){
            $title = "Edit Manufacturer";
            $breadcrumbs = [
                ['name'=>'Manufacturer','relation'=>'link','url'=>route('admin.manufacturer_index')],
                ['name'=>'Edit Manufacturer','relation'=>'Current','url'=>'']
            ];
        }else{
            $title = "Add New Manufacturer";
            $breadcrumbs = [
                ['name'=>'AHJ','relation'=>'link','url'=>route('admin.manufacturer_index')],
                ['name'=>'Add New Manufacturer','relation'=>'Current','url'=>'']
            ];
        }

        $data = ($id)?Manufacturer::find($id):array();
        $type_array = ['module'=>'Module','attachment'=>'Attachment','inverter'=>"Inverter",'battery'=>"Battery"];
        if($request->ajax() && $request->isMethod('post')){
            try {
                $rules = [
                    'title'             => 'required',
                    'type'             => 'required',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages(),'result'=>0), 422);
                }else{
                   // $formData = $request->except('image');
                   // $formData['question'] = $request->get('question');
                    $formData = $request->all();
                    if($id){
                        $data->update($formData);
						return ['status' => 'true', 'message' => 'Manufacturer updated successfully.'];
                        //Session::flash('success','AHJ updated successfully.');
                    }else{
                        Manufacturer::create($formData);
						return ['status' => 'true', 'message' => 'Manufacturer created successfully.'];
                        //Session::flash('success','AHJ created successfully.');
                    }
                   // return ['status' => 'true', 'message' => 'AHJ updated successfully.'];
                }
            } catch (\Exception $e) {
                return ['status' => false, 'message' => $e->getMessage()];
            }
        }
        return view('admin/manufacturers/add',compact('id','data','title','breadcrumbs','type_array'));
    }
    public function manufacturer_add(Request $request)
    {
        try {
            $rules = [
                'title' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()){
                return response()->json(array('errors' => $validator->messages(),'result'=>0));
            }else{
               // $formData = $request->except('image');
               // $formData['question'] = $request->get('question');
                  $formData = $request->all();
                  $formData['title'] = ucwords($request->title);
                   $data =  Manufacturer::create($formData);
                    return ['status' => 'true', 'message' => 'Manufacturer created successfully.','result'=>1,'data'=>$data];
                    //Session::flash('success','AHJ created successfully.');

               // return ['status' => 'true', 'message' => 'AHJ updated successfully.'];
            }
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }





    public function delete(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id;
            try{
                $delete = Manufacturer::where('id','=',$id)->delete();
                if($delete){
                    return ["status"=>"true","message"=>"Record Deleted."];
                }else{
                    return ["status"=>"false","message"=>"Could not deleted Record."];
                }
            }catch(\Exception $e){
                return ["status"=>"false","message"=>$e->getMessage()];
            }
        }
    }


}
