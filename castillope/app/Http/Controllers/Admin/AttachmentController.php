<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Module;
use App\Models\Settings;
use App\Models\Admin;
use App\Models\Manufacturer;
use App\Models\Attachment;
use App\Models\Ahj;
use Illuminate\Http\Request;
use Auth;
use Session;
use DataTables;
use Validator;
use Hash;
use App\Lib\Uploader;

class AttachmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $title = "Attachment List";
        $breadcrumbs = [
            ['name'=>'Attachment List','relation'=>'Current','url'=>'']
        ];

		/*$pages = Attachment::with('manufacturer','module_model')->get();

		foreach($pages as $val){
			echo $val->manufacturer['title'];
		}


		pr($pages->ToArray());die; */

        return view('admin/attachment/index',compact('title','breadcrumbs'));
    }

public function view($id)
		{
			$data = ($id)?Attachment::with('manufacturer','moduleModel')->find($id):array();

		//	pr($data->ToArray());die;

		   $title = "Attachment Detail";
			$breadcrumbs = [
                ['name'=>'Attachments','relation'=>'link','url'=>route('admin.attachment_index')],
                ['name'=>'Attachment Detail','relation'=>'Current','url'=>'']
            ];
			return view('admin/attachment/view',compact('title','breadcrumbs','data'));
		}


     public function datatables()
    {
        $pages = Attachment::with('manufacturer','moduleModel')->get();
        // echo "<pre>";
        // print_r($pages);die;
        return DataTables::of($pages)
            ->addColumn('action', function ($page) {
                return '<a href="'.route('admin.attachment_create',$page->id).'" class="btn btn-xs btn-info btn-sm"><i class="app-menu__icon fa fa-edit"></i></a>&nbsp;|&nbsp;<a href="'.route('admin.attachment_view',$page->id).'" class="btn btn-xs btn-primary btn-sm"><i class="app-menu__icon fa fa-eye"></i></a>&nbsp;|&nbsp;<a data-link="'.route('admin.attachment_delete').'" id="delete_'.$page->id.'" onclick="confirm_delete('.$page->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger btn-sm"><i class="app-menu__icon fa fa-trash"></i></a>';
            })
            ->editColumn('manufacturer_id',function($page){
                return $page->manufacturer['title'];
            })
			->editColumn('model_no',function($page){
                return $page->model_no;
            })
			->editColumn('allowable_pollout_strength',function($page){
                return $page->allowable_pollout_strength;
            })
			->editColumn('allowable_compression_strength',function($page){
                return $page->allowable_compression_strength;
            })
			->editColumn('number_of_screws',function($page){
                return $page->number_of_screws;
            })
            ->rawColumns(['action','manufacturer_id','model_no','allowable_pollout_strength','allowable_compression_strength','number_of_screws'])
            ->make(true);
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id=null)
    {


	//pr($request->all());die;

        if($id){
            $title = "Edit Attachment";
            $breadcrumbs = [
                ['name'=>'Attachment','relation'=>'link','url'=>route('admin.attachment_index')],
                ['name'=>'Edit Attachment','relation'=>'Current','url'=>'']
            ];
        }else{
            $title = "Add New Attachment";
            $breadcrumbs = [
                ['name'=>'Attachment','relation'=>'link','url'=>route('admin.attachment_index')],
                ['name'=>'Add New Attachment','relation'=>'Current','url'=>'']
            ];
        }

		      $manufacturers = Manufacturer::select('id','title')->where('type','attachment')->get();
		      $modules = Module::select('id','module_model')->get();

        $data = ($id)?Attachment::find($id):array();


        if($request->ajax() && $request->isMethod('post')){
            try {
                $rules = [
                    //'question'             => 'required|max:70|unique:questions,question,'.$id,
                    'manufacturer_id'                     => 'required',
                    'model_no'                          => 'required',
                    'allowable_pollout_strength'        => 'required|numeric',
                    'allowable_compression_strength'    => 'required|numeric',
                    'number_of_screws'                  => 'required|numeric',
                    'note'                              => 'required',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                   // $formData = $request->except('image');
                   // $formData['question'] = $request->get('question');
                    $formData = $request->all();
                    if($id){
                        $data->update($formData);
						return ['status' => 'true', 'message' => 'Attachment updated successfully.'];
                        // Session::flash('success','Attachment updated successfully.');
                    }else{
                        Attachment::create($formData);
						return ['status' => 'true', 'message' => 'Attachment created successfully.'];
                        //Session::flash('success','Attachment created successfully.');
                    }

                }
            } catch (\Exception $e) {
                return ['status' => false, 'message' => $e->getMessage()];
            }
        }
        return view('admin/attachment/add',compact('id','data','manufacturers','modules','title','breadcrumbs'));
    }


 public function delete(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id;
            try{
                $delete = Attachment::where('id','=',$id)->delete();
                if($delete){
                    return ["status"=>"true","message"=>"Record Deleted."];
                }else{
                    return ["status"=>"false","message"=>"Could not deleted Record."];
                }
            }catch(\Exception $e){
                return ["status"=>"false","message"=>$e->getMessage()];
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Module $module)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module)
    {
        //
    }
}
