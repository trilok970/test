<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\User;
use DB;
use Validator;
use Auth;
class UserController extends Controller
{
    public $paginate_no;

  public function __construct()
    {
        $this->paginate_no = config('constants.paginate_no');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "Users List";
        $breadcrumbs = [
            ['name'=>'Users List','relation'=>'Current','url'=>'']
        ];
        if($request->query())
        {
            $search = trim($request->search);
            $users = User::where(['is_deleted'=>0])
            ->where(function($query) use ($search){
                $query->orWhere(DB::raw('concat(first_name," ",last_name)'),'like','%'.$search.'%');
                $query->orWhere(DB::raw('concat(country_code," ",phone_number)'),'like','%'.$search.'%');
                $query->orWhere('email','like','%'.$search.'%');
                $query->orWhere('created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
            })
            ->orderByDesc('id')->paginate($this->paginate_no);
        }
        else
        {
            $users = User::where(['is_deleted'=>0])->orderByDesc('id')->paginate($this->paginate_no);
            $search = '';
        }
        // echo "<pre>";
        // print_r($users);die;

        return view('admin.user.index',compact('users','search','title','breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
       $user->is_deleted = 1;
       if($user->save())
       {
        return back()->with('message','User deleted successfully');
       }
       else
        return back()->with('message','Something went wrong');
    }
}
