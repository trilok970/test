<?php

namespace App\Http\Controllers\Admin;

use App\Models\Battery;
use App\Models\Manufacturer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Validator;
class BatteryController extends Controller
{
    public function __construct()
    {
        $this->model = new Battery();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Battery List";
        $breadcrumbs = [
            ['name'=>'Battery','relation'=>'Current','url'=>'']
        ];

		// $pages = Module::with('manufacturer')->get();

		//pr($pages->ToArray());die;

        return view('admin/battery/index',compact('title','breadcrumbs'));
    }
    public function datatables()
    {
        // echo "check";exit;
        $pages = $this->model::where(['is_deleted'=>0])->with('manufacturer')->get();
        // echo "<pre>";
        // print_r($pages);exit;
        return DataTables::of($pages)
            ->addColumn('action', function ($page) {
                return '<a href="'.route('admin.battery_create',$page->id).'" class="btn btn-xs btn-info btn-sm"><i class="app-menu__icon fa fa-edit"></i></a>&nbsp;|&nbsp;<a href="'.route('admin.battery_view',$page->id).'" class="btn btn-xs btn-primary btn-sm"><i class="app-menu__icon fa fa-eye"></i></a>&nbsp;|&nbsp;<a data-link="'.route('admin.battery_delete').'" id="delete_'.$page->id.'" onclick="confirm_delete('.$page->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger btn-sm"><i class="app-menu__icon fa fa-trash"></i></a>';
            })
            ->editColumn('model_no',function($page){
                return $page->model_no;
            })
			->editColumn('manufacrer_id',function($page){
                return $page->manufacturer['title'];
            })
			->editColumn('maximum_output_power',function($page){
                return $page->maximum_output_power;
            })
			->editColumn('maximum_output_voltage',function($page){
                return $page->maximum_output_voltage;
            })
			->editColumn('maximum_output_current',function($page){
                return $page->maximum_output_current;
            })

            ->rawColumns(['action','model_no','manufacrer_id','maximum_output_power','maximum_output_voltage','maximum_output_current'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id=null)
    {


	//pr($request->all());die;

    if($id){
        $title = "Edit Battery";
        $breadcrumbs = [
            ['name'=>'Battery','relation'=>'link','url'=>route('admin.battery')],
            ['name'=>'Edit Battery','relation'=>'Current','url'=>'']
        ];
    }else{
        $title = "Add New Battery";
        $breadcrumbs = [
            ['name'=>'Battery','relation'=>'link','url'=>route('admin.battery')],
            ['name'=>'Add New Battery','relation'=>'Current','url'=>'']
        ];
    }

    $manufacturers = Manufacturer::select('id','title')->where('type','battery')->get();

    $data = ($id)?$this->model::find($id):array();


    if($request->ajax() && $request->isMethod('post')){
        try {
            $rules = [
                //'question'             => 'required|max:70|unique:questions,question,'.$id,
                'manufacturer_id'               => 'required',
                'model_no'                      => 'required',
                'maximum_output_power'          => 'required|numeric',
                'maximum_output_voltage'        => 'required|numeric',
                'maximum_output_current'        => 'required|numeric',
                'operating_voltage_range'       => 'required|numeric',
            ];
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()){
                return response()->json(array('errors' => $validator->messages()), 422);
            }else{
               // $formData = $request->except('image');
               // $formData['question'] = $request->get('question');
                $formData = $request->all();
                if($id){
                    $data->update($formData);
                     return ['status' => 'true', 'message' => 'Battery updated successfully.'];
                    //Session::flash('success','Module updated successfully.');
                }else{
                    $this->model::create($formData);
                     return ['status' => 'true', 'message' => 'Battery created successfully.'];
                   // Session::flash('success','Module created successfully.');
                }

            }
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }
    return view('admin/battery/add',compact('id','data','manufacturers','title','breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Battery  $battery
     * @return \Illuminate\Http\Response
     */
    public function show(Battery $battery,$id)
    {

		$data = $this->model::find($id);

        $title = "Battery View";
        $breadcrumbs = [
            ['name'=>'Battery','relation'=>'Current','url'=>'']
        ];
        return view('admin.battery.view',compact('data','title','breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Battery  $battery
     * @return \Illuminate\Http\Response
     */
    public function edit(Battery $battery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Battery  $battery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Battery $battery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Battery  $battery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id;
            try{
                $delete = $this->model::where('id','=',$id)->update(['is_deleted'=>1]);
                if($delete){
                    return ["status"=>"true","message"=>"Record Deleted."];
                }else{
                    return ["status"=>"false","message"=>"Could not deleted Record."];
                }
            }catch(\Exception $e){
                return ["status"=>"false","message"=>$e->getMessage()];
            }
        }
    }
}
