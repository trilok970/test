<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Session;
use Validator;
use Hash;
use App\Lib\Uploader;

class HomeController extends Controller
{



	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $title = "Dashboard";
         $breadcrumbs = [
            ['name'=>'Dashboard','relation'=>'Current','url'=>'','icon'=>'fa fa-dashboard']
        ];
        $adminData = Admin::find(Auth::guard('admin')->user()->id);
		$user_count = User::where('is_deleted',0)->count();
		//print_r($data->ToArray());die;
       // $users_count = User::where('role','User')->count();
       // $contractor_count = User::where('role','Contractor')->count();
       // $categories_count = Category::count();
        //return view('admin.home.dashboard',compact('title','users_count','categories_count','contractor_count'));
        return view('admin.home.dashboard',compact('title','user_count','breadcrumbs'));
    }



	  public function profile(Request $request){
        $title = "Profile";
        $breadcrumbs = [
            ['name'=>'Profile','relation'=>'Current','url'=>'']
        ];
		  $date = date('Y-m-d h:i:s', time());
        $data = Admin::find(Auth::guard('admin')->user()->id);
        if($request->isMethod('post')){
            try {
                $validator = Validator::make($request->all(), [
                    'name'              => 'required|max:45',
                    'email'             => 'required|email|unique:admins,email,'.$data->id,
                   // 'profile_picture'     => 'nullable|image'
                ]);
                if($validator->fails()){
                   foreach ($validator->messages()->getMessages() as $field_name => $messages) {
						if (!isset($firstError))
							$firstError = $messages[0];
						$error[$field_name] = $messages[0];
					}

					return cRedirect("profile")->with('error_msg', $firstError)->send();
                   // return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                    $formData = [
                        'name'      =>      $request->get('name'),
                        'email'     =>      $request->get('email'),
                    ];
                   /* if ($request->hasFile('profile_picture')) {
                        if($request->file('profile_picture')->isValid()) {
                            $path = "/uploads/users/";
                            $responseData =  Uploader::doUpload($request->file('profile_picture'),$path,false);
                            $formData['profile_picture'] = $responseData['file'];
                        }
                    } */
                    $data->update($formData);
					return cRedirect("profile")->with('success_msg', "Profile updated successfully.")->send();
                   // return ['status'=>'true','message'=>__("Profile updated successfully.")];
                }
            } catch (\Exception $e) {
				return cRedirect("profile")->with('error_msg', $e->getMessage())->send();
               // return ['status'=>'false','message'=>$e->getMessage()];
            }
        }
        return view('admin.home.profile',compact('title','breadcrumbs','data'));
    }

    public function changePassword(Request $request)
    {
        $title = "Change Password";
        $breadcrumbs = [
            ['name'=>'Change Password','relation'=>'Current','url'=>'']
        ];
        if($request->isMethod('post')){

            try {
                $validator = Validator::make($request->all(), [
                    'current_password'      => 'required|max:45',
                    'new_password'          => 'required|max:45|min:8|same:confirm_password',
                    'confirm_password'      => 'required|max:45|min:8'
                ]);
                if($validator->fails()){
					foreach ($validator->messages()->getMessages() as $field_name => $messages) {
						if (!isset($firstError))
							$firstError = $messages[0];
						$error[$field_name] = $messages[0];
					}

					return cRedirect("change-password")->with('error_msg', $firstError)->send();
                   // return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                    $data = Admin::find(Auth::guard('admin')->user()->id);
                    if(Hash::check($request->get('current_password'),$data->password)){
                        $data->update(['password'=>Hash::make($request->get('new_password'))]);
                        Session::flash('success',__("Password updated successfully."));
						 return cRedirect("change-password")->with('success_msg', "Password successfully changed")->send();
                       // return ['status'=>'true','message'=>__("Password updated successfully.")];
                    }else{
						return cRedirect("change-password")->with('error_msg', "Current password does't match.")->send();
                       // return ['status'=>'false','message'=>__("Current password does't match.")];
                    }
                }
            } catch (\Exception $e) {
				 return cRedirect("change-password")->with('error_msg', $e->getMessage())->send();
                //return ['status'=>'false','message'=>$e->getMessage()];
            }
        }
        return view('admin.home.change_password',compact('title','breadcrumbs'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
    }
}
