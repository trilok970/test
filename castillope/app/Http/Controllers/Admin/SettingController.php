<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Settings;
use App\Models\Admin;
use App\Models\Manufacturer;
use Illuminate\Http\Request;
use Auth;
use Session;
use Validator;
use Hash;
use App\Lib\Uploader;

class SettingController extends Controller
{
    
	 public function __construct()
    {
        $this->sortable_columns = [
            0 => 'id',
            1 => 'title',
        ];
    }

	
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function manufacturer(Request $request)
    {
         $title = "Manufacturer List";
		$adminData = Admin::find(Auth::guard('admin')->user()->id);
		//print_r($data->ToArray());die;
       // $users_count = User::where('role','User')->count();
       // $contractor_count = User::where('role','Contractor')->count();
       // $categories_count = Category::count();
        //return view('admin.home.dashboard',compact('title','users_count','categories_count','contractor_count')); 
		
		
		 $breadcrumbs = [ 
            ['name'=>'Manufacturer List','relation'=>'Current','url'=>'']
        ];
		
		
		
		if($request->ajax())
        {
            $totalCms       = Manufacturer::count();
            $limit          = $request->input('length');
            $start          = $request->input('start');
            $search         = $request['search']['value'];
            $orderby        = $request['order']['0']['column'];
            $order          = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw           = $request['draw'];
          
            $response       = Manufacturer::getManufacturerModel($limit , $start , $search, $this->sortable_columns[$orderby], $order);

            if(!$response){
                $page       = [];
                $paging     = [];
            }
            else{
                $page       = $response;
                $paging     = $response;
            }

            $data = array();

            foreach ($page as $category) { 
            	$u['id']         = ($category->id);
                $u['set_name']         = ucwords($category->title);
				
				$actions = '<a class="btn btn-danger btn-sm manufacturalTypeDelete" data-id="'.$category->id.'" href="#">Delete</a> | ';
				$actions .= '<a class="btn btn-dark btn-sm manufacturalTypeEdit"  data-toggle="modal" data-target="#manufacturalTypeEdit" data-id="'.$category->id.'" href="#"> Edit</a>';
			
                //$u['actions']       = $actions->render(); 
                $u['actions']       = $actions; 

                $data[] = $u;

                unset($u);
            }

            $return = [
                "draw"              =>  intval($draw),
                "recordsFiltered"   =>  intval( $totalCms),
                "recordsTotal"      =>  intval( $totalCms),
                "data"              =>  $data
            ];
            return $return;
        }

        return view('admin.setting.manufacturer', compact('title','breadcrumbs'));
    }
	
	public function addManufacturer(Request $request)
    {
        if($request->ajax())
        {
            $validator = Validator::make($request->all(), [
                'set_name' => 'required',          
            ],[],[
                'set_name'=>'Manufactural name'
            ]);
            if ($validator->fails()) {            
                $response['message'] = $validator->errors()->first();
                $response['status'] = 'false';
                return response()->json($response);
            }
            $manufactural = Manufacturer::where('title',$request->set_name)->count('id');
            if($manufactural > 0)
            {
                $response['status'] = 'false';
                $response['message'] = 'This Manufactural already exist!';
                return response()->json($response);
            }
            $newManufactural = Manufacturer::create([
                'title'=>$request->set_name,
            ]);
            if($newManufactural)
            {
                $response['status'] = 'true';
                $response['message'] = 'New Manufactural added!';
                return response()->json($response);
            }
            $response['status'] = 'false';
            $response['message'] = 'Something went wrong!';
            return response()->json($response);
        }
        abort(404);
    }
	
	
	 public function updateManufacturer(Request $request)
    {
        if($request->ajax())
        {
            $validator = Validator::make($request->all(), [
                'set_name' => 'required',          
            ],[],[
                'set_name'=>'Manufactural name'
            ]);
            if ($validator->fails()) {            
                $response['message'] = $validator->errors()->first();
                $response['status'] = 'false';
                return response()->json($response);
            }
            $manufactural = Manufacturer::where('title',$request->set_name)->where('id','!=',$request->id)->count('id');
            if($manufactural > 0)
            {
                $response['status'] = 'false';
                $response['message'] = 'This Manufactural already exist!';
                return response()->json($response);
            }
            $newManufactural = Manufacturer::where('id',$request->id)->first();
            if(!$newManufactural)
            {
                $response['status'] = 'false';
                $response['message'] = 'Something went wrong!';
                return response()->json($response);
            }
            $response['status'] = 'true';
            $response['message'] = 'Manufactural updated successfully!';         
            $newManufactural->title = $request->set_name;
            $newManufactural->save();
            return response()->json($response);
        }
        abort(404);
    }
	
	 public function getManufacturer(Request $request)
    {
        if($request->ajax())
        {
            $response['newManufactural'] = Manufacturer::find($request->id);
            if(!$response['newManufactural'])
            {
                $response['status'] = 'false';
                $response['message'] = 'Something went wrong!';
                return response()->json($response);
            }
            return response()->json($response);
        }
        abort(404);
    }

	public function changeStatus(Request $request, $id)
    {
        $manufactural = Manufacturer::find($id);
        $manufactural->status = ($request->manufactural_status == 'Active') ? 1 : 0;
        $manufactural->save();

        return response()->json(array('success' => true));
    }
  
    public function deleteManufacturer(Request $request,$id)
    {
        $manufactural = Manufacturer::find($id);
        $manufactural->delete();
        return response()->json(array('success' => true));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function show(Settings $settings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function edit(Settings $settings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Settings $settings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function destroy(Settings $settings)
    {
        //
    }
}
