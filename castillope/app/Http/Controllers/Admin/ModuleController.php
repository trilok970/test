<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Module;
use App\Models\Settings;
use App\Models\Admin;
use App\Models\Manufacturer;
use App\Models\Attachment;
use App\Models\Ahj;
use Illuminate\Http\Request;
use Auth;
use Session;
use DataTables;
use Validator;
use Hash;
use App\Lib\Uploader;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $title = "Module Models List";
        $breadcrumbs = [
            ['name'=>'Module Models','relation'=>'Current','url'=>'']
        ];

		// $pages = Module::with('manufacturer')->get();

		//pr($pages->ToArray());die;

        return view('admin/modules/index',compact('title','breadcrumbs'));
    }


     public function datatables()
    {
        $pages = Module::with('manufacturer')->get();
        return DataTables::of($pages)
            ->addColumn('action', function ($page) {
                return '<a href="'.route('admin.model_create',$page->id).'" class="btn btn-xs btn-primary btn-sm"><i class="fa fa-edit"></i></a>&nbsp;<a data-link="'.route('admin.model_delete').'" id="delete_'.$page->id.'" onclick="confirm_delete('.$page->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
            })
            ->editColumn('module_model',function($page){
                return $page->module_model;
            })
			->editColumn('manufacrer_id',function($page){
                return $page->manufacturer['title'];
            })
			->editColumn('pmp',function($page){
                return $page->pmp;
            })
			->editColumn('voc',function($page){
                return $page->voc;
            })
			->editColumn('vmpp',function($page){
                return $page->vmpp;
            })
			->editColumn('module_lenght',function($page){
                return $page->module_lenght;
            })
			->editColumn('module_width',function($page){
                return $page->module_width;
            })
            ->rawColumns(['action','module_model','manufacrer_id','pmp','voc','vmpp','module_lenght','module_width'])
            ->make(true);
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id=null)
    {


	//pr($request->all());die;

        if($id){
            $title = "Edit Module Model";
            $breadcrumbs = [
                ['name'=>'Module Model','relation'=>'link','url'=>route('admin.model_index')],
                ['name'=>'Edit Module Model','relation'=>'Current','url'=>'']
            ];
        }else{
            $title = "Add New Module Model";
            $breadcrumbs = [
                ['name'=>'Module Model','relation'=>'link','url'=>route('admin.model_index')],
                ['name'=>'Add New Module Model','relation'=>'Current','url'=>'']
            ];
        }

		      $manufacturers = Manufacturer::select('id','title')->where('type','module')->get();

        $data = ($id)?Module::find($id):array();


        if($request->ajax() && $request->isMethod('post')){
            try {
                $rules = [
                    //'question'             => 'required|max:70|unique:questions,question,'.$id,
                    'manufacturer_id'             => 'required',
                    'module_model'             => 'required',
                    'pmp'             => 'required|numeric',
                    'voc'             => 'required|numeric',
                    'vmpp'             => 'required|numeric',
                    'tvoc'             => 'required|numeric',
                    'isc'             => 'required|numeric',
                    'imp'             => 'required|numeric',
                    'tcpm'             => 'required|numeric',
                    'module_lenght'             => 'required|numeric',
                    'module_width'             => 'required|numeric',
                    'allowed_pressure_long_side'             => 'required|numeric',
                    'allowed_pressure_short_side'             => 'required|numeric',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                   // $formData = $request->except('image');
                   // $formData['question'] = $request->get('question');
                    $formData = $request->all();
                    if($id){
                        $data->update($formData);
						 return ['status' => 'true', 'message' => 'Module updated successfully.'];
                        //Session::flash('success','Module updated successfully.');
                    }else{
                        Module::create($formData);
						 return ['status' => 'true', 'message' => 'Module created successfully.'];
                       // Session::flash('success','Module created successfully.');
                    }

                }
            } catch (\Exception $e) {
                return ['status' => false, 'message' => $e->getMessage()];
            }
        }
        return view('admin/modules/add',compact('id','data','manufacturers','title','breadcrumbs'));
    }


 public function delete(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id;
            try{
                $delete = Module::where('id','=',$id)->delete();
                if($delete){
                    return ["status"=>"true","message"=>"Record Deleted."];
                }else{
                    return ["status"=>"false","message"=>"Could not deleted Record."];
                }
            }catch(\Exception $e){
                return ["status"=>"false","message"=>$e->getMessage()];
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Module $module)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module)
    {
        //
    }
}
