<?php

use Illuminate\Support\Facades\Input;

function is_in_array($array,$key_value){

    foreach($array as $i =>$arr1)
    {
        if($arr1->key == $key_value)
        {
            echo 'Yes';
        }
    }
    // return $within_array;
}


function cRedirect($url){
    return redirect("admin/".$url);
}
function cAsset($url){
    return URL::asset("assets/admin/".$url);
}

function cUrl($url){
    return url("admin/".$url);
}

function uRedirect($url){
    return redirect("useradmin/".$url);
}
function uAsset($url){
    return URL::asset("assets/useradmin/".$url);
}

function uUrl($url){
    return url("useradmin/".$url);
}

function orderBy($name,$nameLink=""){
    $query= Request::query();
    $send="?";
    $image="";
    $order="ASC";
    if(isset($query['orderby'])){
        if($query['orderby']==$name){
            $query['order']=$query['order']=="ASC"?"DESC":'ASC';
            $image='<img src="' .cAsset('images/'.strtolower($query['order'])). '.gif" />';
        }
        else
            $query['orderby']=!in_array("orderby",$query)? $name:@$query['orderby'];

    }
    else{
        $query['orderby']=$name;
        $query['order']='ASC';
    }

    foreach($query as $q=>$value){
        $send.="$q=$value&";
    }
    $name=$nameLink!=""?$nameLink:$name;
    return '<a href="'.$send.'">'.ucfirst($name).'</a>'.$image;


}

function inputSet($param){
    foreach($param as $k=>$p){
        $change[$k]=Input::get($k)==""?$p:Input::get($k);
    }
    Input::merge($change)  ;
}
function pr($arr,$isStop=0){
    echo "<pre>";
    print_r($arr);
    echo "<pre>";
    if($isStop)
        die("stopping");

}
function base_url($url=""){

    $url=(string)$url;
    return  str_replace("public","",url($url));

}

function upload_url($url=""){

    $url=(string)$url;
    return url("public/Uploads/".$url);

}


function base_url_image($url=""){

    $url=(string)$url;
    if($url==""){
        return url("public/images/avatar.jpg");
    }
    return  str_replace("public","",url($url));

}
function _t($value,$default="",$lang=""){

    return App\Library\LanguageConvertor\Translator::getWordValue($value);

}
function isApp(){
    return @$_POST['is_app']=="1"?true:false;
}
function setPlatform($type="APP"){
    if($type=="APP")
        return @$_POST['is_app']="1";
    if($type=="WEB")
        return @$_POST['is_web']="1";

}

function getLatandLong($zip){
    $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($zip)."&amp;sensor=false";
    $result_string = file_get_contents($url);
    $result = json_decode($result_string, true);
    $result1[]=$result['results'][0];
    $result2[]=$result1[0]['geometry'];
    $result3[]=$result2[0]['location'];
    return $result3[0];
}
if (!function_exists('file_upload')) {
    function file_upload($file,$filePath,$fileType) {
        if($file) {
            $extension = $file->extension();
            $fileName  = $filePath.'/' . time() . ".$extension";
            

            $ff = $file->move(public_path($filePath), $fileName);
            
            
        }
        $array = array("fileName"=>$fileName ?? "",'finalFilePath'=>$finalFilePath ?? "","extension"=>$extension ?? "");
        return $array;
        }
    }

