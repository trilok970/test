<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\ManufacturerController;
use App\Http\Controllers\Admin\ModuleController;
use App\Http\Controllers\Admin\AttachmentController;
use App\Http\Controllers\Admin\AhjController;
use App\Http\Controllers\Admin\BatteryController;
use App\Http\Controllers\Admin\InverterController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     echo "check";
// });


//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});
Route::get('/check', function() {
    return '<h1>Cache facade value cleared</h1>';
});


//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

//Clear Config cache:
Route::get('/config-ff', function() {
    $exitCode = Artisan::call('storage:link');
    return '<h1>Clear Configwwwww cleared</h1>';
});

Route::any('admin',function(){
      return redirect(route('admin.login'));
    });


// Front Routes
Route::group(['namespace'=>'App\Http\Controllers\Front'],function(){




    Route::get('/','LoginController@login');
    Route::get('login','LoginController@login')->name('login');
    Route::post('login','LoginController@loginPost');
    Route::get('signup','LoginController@signup');
    Route::post('signup','LoginController@signupPost');
    Route::get('forgot-password','LoginController@forgotPassword');
    Route::post('forgot-password','LoginController@forgotPasswordPost');
    Route::get('logout','LoginController@logout');
    Route::any('reset-password/{token}','LoginController@resetPassword');
    Route::post('reset-password','LoginController@resetPasswordPost');

    Route::group(['middleware'=>'auth'],function(){


        Route::get('recent-projects','ProjectController@index');
        // Route::get('structural','DashboardController@structural');
        Route::get('structural/{id}','DashboardController@structural');
        Route::get('structural','DashboardController@structural_project');
        Route::get('electrical/{id}','DashboardController@electrical');
        Route::get('electrical1/{id}','DashboardController@electrical1');
        Route::resource('project','ProjectController');
        Route::get('project/{id}/delete','ProjectController@destroy');
        Route::resource('site','SiteController');
        Route::get('module-manufacturer/{id}','SiteController@manufacture_module');
        Route::get('manufacturer-inverter/{id}','ElectricalController@manufacture_inverter');
        Route::get('module/{module_id}/{manufacturer_id}','SiteController@module');
        Route::get('attachment/{manufacturer_id}','SiteController@attachment');
        Route::post('site-formula','SiteController@siteFormula');
        Route::post('structure-room-details','SiteController@structure_room_details');
        Route::resource('roofinfo','RoofInfoController');
        Route::resource('electrical','ElectricalController');
        Route::resource('different-inverter','DifferentInverterController');
        Route::resource('electrical-string','ElectricalStringController');
        Route::get('conductor-ampacity-calculations/{id}/{type}','ElectricalController@dc_conductor_ampacity')->name('conductor.ampacity.calculations');
        Route::get('modules','DashboardController@modules');
        Route::get('inverters','DashboardController@inverters');
        Route::get('ahj','DashboardController@ahj');
        Route::get('project-detail-pdf/{id}','DashboardController@project_detail_pdf');
        Route::get('electrical','DashboardController@electrical_project');
        Route::post('electrical-formula','ElectricalStringController@electricalFormula');
        Route::get('create-new-project','DashboardController@create_new_project');
        Route::get('block-design/{project_id?}','DashboardController@blockDesign')->name('block.design');
        Route::get('board-demo','DashboardController@boardDemo');
        Route::get('konvasExample','DashboardController@konvasExample');
        Route::get('gojsExample','DashboardController@gojsExample');
        Route::get('demo','DashboardController@demo');
        Route::get('block-design-project','DashboardController@blockDesignProject')->name('block.design.project');
        Route::post('block-design-pdf','DashboardController@blockDesignPdf')->name('block.design.pdf');
        Route::get('block-design-reports/{project_id?}','DashboardController@blockDesignReports')->name('block.design.reports');

    });

});


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia\Inertia::render('Dashboard');
})->name('dashboard');

Route::group(['prefix' => 'admin','namespace'=>'Admin'], function () {
	Route::get('/', [AuthController::class,'login_show']);
	Route::get('login', [AuthController::class,'login_show'])->name('admin.login');
	Route::post('login', [AuthController::class,'login'])->name('admin.login.post');
	Route::post('forgot-password',[AuthController::class,'forgotPassword'])->name('admin.forgotpassword');
	Route::any('reset-password/{token}',[AuthController::class,'resetPassword'])->name('admin.resetpassword');
});



Route::group(['prefix' => 'admin','namespace'=>'App\Http\Controllers\Admin','middleware'=>'admin'], function () {
  Route::get('home', [HomeController::class,'index'])->name('admin.dashboard');
	Route::any('profile',[HomeController::class,'profile'])->name('admin.profile');


	 Route::get('/manufacturers', [ManufacturerController::class,'index'])->name('admin.manufacturer_index');
	 Route::any('/manufacturers/moduledatatables', [ManufacturerController::class,'datatables'])->name('admin.manufacturer_table');
	 Route::any('/manufacturers/add/{id?}', [ManufacturerController::class,'create'])->name('admin.manufacturer_create');
	 Route::any('/manufacturers/delete/', [ManufacturerController::class,'delete'])->name('admin.manufacturer_delete');
	 Route::post('/manufacturers-add', [ManufacturerController::class,'manufacturer_add'])->name('admin.manufacturer_add');


	 Route::get('/modules', [ModuleController::class,'index'])->name('admin.model_index');
	 Route::any('/modules/moduledatatables', [ModuleController::class,'datatables'])->name('admin.model_datatables');
	 Route::any('/modules/add/{id?}', [ModuleController::class,'create'])->name('admin.model_create');
	 Route::any('/modules/delete/', [ModuleController::class,'delete'])->name('admin.model_delete');

	 Route::get('/attachment', [AttachmentController::class,'index'])->name('admin.attachment_index');
	 Route::any('/attachment/datatables', [AttachmentController::class,'datatables'])->name('admin.attachment_datatables');
	 Route::any('/attachment/add/{id?}', [AttachmentController::class,'create'])->name('admin.attachment_create');
	 Route::any('/attachment/view/{id?}', [AttachmentController::class,'view'])->name('admin.attachment_view');
	 Route::any('/attachment/delete/', [AttachmentController::class,'delete'])->name('admin.attachment_delete');

	 Route::get('/ahj', [AhjController::class,'index'])->name('admin.ahj_index');
	 Route::any('/ahj/ahj_datatables', [AhjController::class,'datatables'])->name('admin.ahj_datatables');
	 Route::any('/ahj/add/{id?}', [AhjController::class,'create'])->name('admin.ahj_create');
	 Route::any('/ahj/view/{id?}', [AhjController::class,'view'])->name('admin.ahj_view');
	 Route::any('/ahj/delete/', [AhjController::class,'delete'])->name('admin.ahj_delete');


     Route::get('/inverter', [InverterController::class,'index'])->name('admin.inverter');
	 Route::any('/inverter/inverter_datatables', [InverterController::class,'datatables'])->name('admin.inverter_datatables');
	 Route::any('/inverter/add/{id?}', [InverterController::class,'create'])->name('admin.inverter_create');
	 Route::any('/inverter/view/{id?}', [InverterController::class,'show'])->name('admin.inverter_view');
	 Route::any('/inverter/delete/', [InverterController::class,'destroy'])->name('admin.inverter_delete');

     Route::get('/battery', [BatteryController::class,'index'])->name('admin.battery');
	 Route::any('/battery/battery_datatables', [BatteryController::class,'datatables'])->name('admin.battery_datatables');
	 Route::any('/battery/add/{id?}', [BatteryController::class,'create'])->name('admin.battery_create');
	 Route::any('/battery/view/{id?}', [BatteryController::class,'show'])->name('admin.battery_view');
	 Route::any('/battery/delete/', [BatteryController::class,'destroy'])->name('admin.battery_delete');

     Route::any('change-password',[HomeController::class,'changePassword'])->name('admin.changepassword');
	Route::get('logout',[AuthController::class,'logout'])->name('admin.logout');

    Route::resource('user',"UserController");

});


