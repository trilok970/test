<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    protected $appends = [];
	
     public function audio()
    {
    	return $this->belongsTo(Audio::class);
    }
     public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
