<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Myplaylist extends Model
{
    public function MyplaylistAudio()
    {
    	return $this->hasMany(MyplaylistAudio::class);
    }
}
