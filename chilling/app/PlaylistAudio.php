<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaylistAudio extends Model
{
    //
    protected $fillable = [
        'file', 'playlist_id','extension'
    ];

    public function playlist()
	{
	return $this->belongsTo(Playlist::class)->orderBy('order_number','asc');
	}
	public function category()
	{
		return $this->belongsTo(Category::class);
	}
	public function audio()
	{
		return $this->belongsTo(Audio::class)->orderBy('id','desc');
	}


}
