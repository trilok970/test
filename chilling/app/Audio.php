<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Audio extends Model
{
    //
    protected $appends = ['is_favourite','audio_duration','audio_title','is_finish'];

    public function getIsFavouriteAttribute()
    {
    	$user = Auth::user();
    	$count = Favourite::where(['user_id'=>$user->id,'audio_id'=>$this->id])->count() ?? 0;
        return $count;
    }
    public function getAudioDurationAttribute()
    {
        $user = Auth::user();
    	// $count = AudioDuration::select("audio_current_time")->where(['user_id'=>$user->id,'audio_id'=>$this->id])->first()->audio_current_time ?? "0";
    	$count = AudioDuration::select("audio_current_time")->where(['user_id'=>$user->id,'audio_id'=>$this->id])->max('audio_current_time') ?? "0";
        return $count;
    }
    public function getIsFinishAttribute()
    {
        //SELECT (is_finish) FROM `audio_durations` WHERE user_id=3464 and `audio_current_time` 
        //in(SELECT max(audio_current_time) FROM `audio_durations` WHERE user_id=3464 );
        $user = Auth::user();
        $user_id = $user->id;
        $audio_id = $this->id;
    	// $count = AudioDuration::select("is_finish")->where(['user_id'=>$user->id,'audio_id'=>$this->id])->first()->is_finish ?? 0;
    	$audioCount = AudioDuration::where(["user_id"=>$user_id,"audio_id"=>$audio_id])->count();
        if($audioCount > 0) {
            $count = AudioDuration::select("is_finish")->whereRaw("user_id=$user_id and audio_id=$audio_id and audio_current_time in(SELECT max(audio_current_time) FROM audio_durations WHERE user_id=$user_id and audio_id=$audio_id )")->first()->is_finish ?? 0;
        }
        else {
            $count = 0;
        }
        return $count;
    }
    public function getAudioTitleAttribute()
    {
        return strip_tags(html_entity_decode($this->title, ENT_QUOTES, 'UTF-8'));
    }
    // public function getAudioCategoryAttribute()
    // {
    	
    // 	$categories_ids = AudioCategory::where('audio_id',$this->id)->pluck('category_id')->toArray();
    //     // echo "<pre>";
    //     // print_r($categories_ids);exit;
    // 	$count = Category::select('name')->whereIn('id',$categories_ids)->get() ?? 0;
    //     $category = [];
    //     foreach($count as $value) {
    //         $category['category']['name'] = $value->name;
    //         $data[] = $category;
    //     }
    //     return $data;
    // }
    public function AudioCategory()
    {
        return $this->hasMany(AudioCategory::class);
    }
    public function Favourite()
    {
    	return $this->hasOne(Favourite::class);
    }
    public function viewCount()
    {
        return $this->view_count;
    }
    public function getImageAttribute($value)
    {
        if ($value) {
            // $client = \Storage::disk('s3')->getDriver()->getAdapter()->getClient();
            // $bucket = \Config::get('filesystems.disks.s3.bucket');

            // $command = $client->getCommand('GetObject', [
            //     'Bucket' => $bucket,
            //     'Key' => $value  // file name in s3 bucket which you want to access
            // ]);

            // $request = $client->createPresignedRequest($command, '+10 minutes');

            // Get the actual presigned-url
            // return $presignedUrl = (string)$request->getUri();
            return \Storage::disk('s3')->url($value);
        }
    }
    public function getFileAttribute($value)
    {
        if ($value) {
            // $client = \Storage::disk('s3')->getDriver()->getAdapter()->getClient();
            // $bucket = \Config::get('filesystems.disks.s3.bucket');

            // $command = $client->getCommand('GetObject', [
            //     'Bucket' => $bucket,
            //     'Key' => $value  // file name in s3 bucket which you want to access
            // ]);

            // $request = $client->createPresignedRequest($command, '+10 minutes');

            // // Get the actual presigned-url
            // return $presignedUrl = (string)$request->getUri();
            return \Storage::disk('s3')->url($value);

        }
    }
}
