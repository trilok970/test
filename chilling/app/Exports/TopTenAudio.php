<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use App\Audio;
class TopTenAudio implements FromArray
{
    public $key_val;

    public function __construct($key_val)
    {
        $this->key_val = $key_val;
    }
    /**
    * @return \Illuminate\Support\FromArray
    */
    public function array(): array
    {
        $y = date('Y');

        if($this->key_val == 'year')
        {
            $from_date = date("$y-01-01");
            $to_date = date("$y-12-31");

        }
        else if($this->key_val == 'month')
        {
            $from_date = date("$y-m-01");
            $to_date = date("$y-m-t");

        }
        else if($this->key_val == 'week')
        {
            $from_date = date("$y-m-Y",strtotime("-7 day"));
            $to_date = date("$y-m-d");

        }
        $audios = Audio::select('title','view_count','updated_at')->where(['is_deleted'=>0])
                ->where('view_count','>',0)
                ->whereBetween("updated_at",[$from_date,$to_date])
                ->orderBy('view_count','desc')
                ->limit(10)->get();



        // echo "<pre>";
        // print_r($audios);
        // exit;
        $data[0][0] = 'Sr. No.';
        $data[0][1] = 'Title';
        $data[0][2] = 'View Count';
        $data[0][3] = 'Updated At';
        $j=1;
        foreach($audios as $audio)
        {
            $data[$j][0] = $j;
            $data[$j][1] = strip_tags(html_entity_decode($audio->title,ENT_QUOTES,"UTF-8"));
            $data[$j][2] = strip_tags($audio->view_count);
            $data[$j][3] = date("d-M-Y",strtotime($audio->updated_at));
            $j++;
        }
        // echo "<pre>";
        // print_r($data);
        // exit;
        return $data;
    }
}
