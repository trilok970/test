<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use DB;

class UserExport implements FromArray
{
    /**
    * @return \Illuminate\Support\FromArray
    */
    public function array(): array
    {
        $users =  User::select('fullname','country_code','phone_number','email','created_at','profile_pic','social_type',DB::raw("if(status = 1,'Active','Inactive') as status"))->where(['is_deleted'=>0])->orderBy('id','desc')->get();
// echo "<pre>";
//         print_r($users);
//         exit;
        $data[0][0] = 'Sr. No.';
        $data[0][1] = 'Name';
        $data[0][2] = 'Phone Number';
        $data[0][3] = 'Email';
        $data[0][4] = 'Created On';
        $data[0][5] = 'User singned in as';
        $data[0][6] = 'Profile Pic';
        $data[0][7] = 'Subscription';
        $data[0][8] = 'Status';
        $j=1;
        foreach($users as $user)
        {
            if($user->is_premium == 1) {
                $subcription = "Active";
            }
            else {
                $subcription = "Inactive";
            }

            if($user->phone_number == "" || $user->phone_number == 'null') {
                $phone_number = "";
            }
            else {
                $phone_number = "(".$user->country_code.") ".$user->phone_number;
            }

            $data[$j][0] = $j;
            $data[$j][1] = $user->fullname;
            $data[$j][2] = $phone_number;
            $data[$j][3] = $user->email;
            $data[$j][4] = date("d-M-Y",strtotime($user->created_at));
            $data[$j][5] = $user->social_type ?? "SIGNUP";
            $data[$j][6] = $user->profile_pic;
            $data[$j][7] = $subcription;
            $data[$j][8] = $user->status;

            $j++;
        }
        // echo "<pre>";
        // print_r($data);
        // exit;
        return $data;
    }
}
