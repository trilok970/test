<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use App\Audio;
use DB;
class TopTenFavouriteAudio implements FromArray
{
    public $key_val;

    public function __construct($key_val)
    {
        $this->key_val = $key_val;
    }
    /**
    * @return \Illuminate\Support\FromArray
    */
    public function array(): array
    {
        $y = date('Y');

        if($this->key_val == 'year')
        {
            $from_date = date("$y-01-01");
            $to_date = date("$y-12-31");

        }
        else if($this->key_val == 'month')
        {
            $from_date = date("$y-m-01");
            $to_date = date("$y-m-t");

        }
        else if($this->key_val == 'week')
        {
            $from_date = date("$y-m-Y",strtotime("-7 day"));
            $to_date = date("$y-m-d");

        }

        $audios = Audio::select('audio.id','audio.updated_at','audio.title',DB::raw("count(fv.audio_id) as audio_count"))
                ->leftjoin("favourites as fv",function($join){
                    $join->on("audio.id","=","fv.audio_id");
                })
                ->where(['audio.is_deleted'=>0])
                ->whereBetween("fv.created_at",[$from_date,$to_date])
                ->groupBy('fv.audio_id')
                ->havingRaw("audio_count > 0")
                ->orderBy("audio_count",'desc')
                ->limit(10)->get();



        // echo "<pre>";
        // print_r($audios);
        // exit;
        $data[0][0] = 'Sr. No.';
        $data[0][1] = 'Title';
        $data[0][2] = 'Story Count';
        $data[0][3] = 'Updated At';
        $j=1;
        foreach($audios as $audio)
        {
            $data[$j][0] = $j;
            $data[$j][1] = strip_tags(html_entity_decode($audio->title,ENT_QUOTES,"UTF-8"));
            $data[$j][2] = strip_tags($audio->audio_count);
            $data[$j][3] = date("d-M-Y",strtotime($audio->updated_at));
            $j++;
        }
        // echo "<pre>";
        // print_r($data);
        // exit;
        return $data;
    }
}
