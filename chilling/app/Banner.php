<?php
namespace App;


use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //

   public function playlist()
   {
   	return $this->hasOne(Playlist::class)->orderBy('order_number','asc');
   }
   public function getImageAttribute($value)
    {
        if ($value) {
            // $client = \Storage::disk('s3')->getDriver()->getAdapter()->getClient();
            // $bucket = \Config::get('filesystems.disks.s3.bucket');

            // $command = $client->getCommand('GetObject', [
            //     'Bucket' => $bucket,
            //     'Key' => $value  // file name in s3 bucket which you want to access
            // ]);

            // $request = $client->createPresignedRequest($command, '+10 minutes');

            // Get the actual presigned-url
            // return $presignedUrl = (string)$request->getUri();
            return \Storage::disk('s3')->url($value);
        }
    }
}
