<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Playlist extends Model
{
    //
    protected $appends = ['current_audio_id'];

	public function channel()
	{
		return $this->belongsTo(Channel::class);
	}
	public function category()
	{
		return $this->belongsTo(Category::class);
	}
	public function subcategory()
	{
		return $this->belongsTo(SubCategory::class);
	}
	public function songs()
	{
		return $this->hasMany(Song::class)->where(['is_deleted'=>0])->orderByDesc('id');
	}
	public function banner()
	{
		return $this->belongsTo(Banner::class);
	}
	public function PlaylistCategory()
	{
		return $this->hasMany(PlaylistCategory::class);
	}
	public function PlaylistAudio()
	{
		return $this->hasMany(PlaylistAudio::class)->orderBy('order_number','asc');
	}
	public function viewCount()
    {
        return $this->view_count;
    }
    public function getCurrentAudioIdAttribute()
    {
        $user = Auth::user();
    	$count = PlaylistCurrentAudio::select("audio_id")->where(['user_id'=>$user->id,'playlist_id'=>$this->id])->first()->audio_id ?? "0";
        return $count;
    }
    public function getImageAttribute($value)
    {
        if ($value) {
            // $client = \Storage::disk('s3')->getDriver()->getAdapter()->getClient();
            // $bucket = \Config::get('filesystems.disks.s3.bucket');

            // $command = $client->getCommand('GetObject', [
            //     'Bucket' => $bucket,
            //     'Key' => $value  // file name in s3 bucket which you want to access
            // ]);

            // $request = $client->createPresignedRequest($command, '+10 minutes');

            // Get the actual presigned-url
            // return $presignedUrl = (string)$request->getUri();
            return \Storage::disk('s3')->url($value);
        }
    }
    public function getThumbnailAttribute($value)
    {
        if ($value) {
            // $client = \Storage::disk('s3')->getDriver()->getAdapter()->getClient();
            // $bucket = \Config::get('filesystems.disks.s3.bucket');

            // $command = $client->getCommand('GetObject', [
            //     'Bucket' => $bucket,
            //     'Key' => $value  // file name in s3 bucket which you want to access
            // ]);

            // $request = $client->createPresignedRequest($command, '+10 minutes');

            // Get the actual presigned-url
            // return $presignedUrl = (string)$request->getUri();
            return \Storage::disk('s3')->url($value);
        }
    }

}
