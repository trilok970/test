<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaylistCurrentAudio extends Model
{

    public function playlist()
	{
	return $this->belongsTo(Playlist::class)->orderBy('order_number','asc');
	}
	public function audio()
	{
		return $this->belongsTo(Audio::class)->orderBy('order_number','asc');
    }
    public function user()
	{
		return $this->belongsTo(User::class);
    }

}
