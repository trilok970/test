<?php

namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\DeviceToken as fcmToken;
use App\Notification;
use Exception;
use DB;
class NotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $title;
    protected $message;
    protected $type;
    protected $file;
    protected $users;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($title,$message,$type,$file,$users)
    {
        $this->title = $title;
        $this->message = $message;
        $this->type = $type;
        $this->file = $file;
        $this->users = $users;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            // $users = User::where(['is_deleted'=>0,'status'=>1,'notification'=>1])->get();
            // foreach ($users as $key => $value) {

                // if ($FCMTokenData = fcmToken::select(DB::raw("distinct(device_token)"),'device_type','user_id')
                // ->where('device_token','!=',null)
                // ->whereIn('device_type',['ANDROID','IOS'])
                // ->whereHas('user',function($q) {
                //     $q->where(['is_deleted'=>0,'notification'=>1,'status'=>1]);
                // })
                // ->get())
                if($this->users)
                {
                    $tokens = $this->users->where('device_type','ANDROID')->pluck('device_token')->toArray();
                    $apns_ids = $this->users->where('device_type','IOS')->pluck('device_token')->toArray();

                    // foreach ($this->users as $key => $user)
                    // {

                    //     if($user->device_type == 'ANDROID') {
                    //         $tokens[] = $user->device_token;
                    //     }
                    //     if($user->device_type == 'IOS') {
                    //         $apns_ids[] = $user->device_token;
                    //     }

                    //     $notification['user_id'] = $user->id;
                    //     $notification['title'] = $this->title;
                    //     $notification['message'] = $this->message;
                    //     $notification['type'] = $this->type;
                    //     if($this->file) {
                    //     $notification['image'] = str_replace("https://chilling-assets.s3.us-east-2.amazonaws.com/", '', $this->file);
                    //     }
                    //     $notification_array[] = $notification;
                    //     // yield $user;


                    // }




                if($tokens) {
                    $androidChunks = array_chunk($tokens,1000);
                    foreach($androidChunks as $androidToken){
                    $this->sendNotification($androidToken,$this->title,$this->message,$this->type,$this->file,'android');
                }
                }
                if($apns_ids) {
                    $iosChunks = array_chunk($apns_ids,1000);
                    foreach($iosChunks as $iosToken){
                        $this->sendNotification($iosToken,$this->title,$this->message,$this->type,$this->file,'ios');
                    }
                }


                // }
                // if($notification_array) {
                //     $notification_array = array_unique($notification_array, SORT_REGULAR);

                //     $this->notification_entry_for_all($notification_array);
                // }

            }
          } catch (Exception $e) {
             $exception = $e->getMessage();

          }

    }

    public function notification_entry_for_all($notification_array)
    {
        $notification = new Notification;
        $notification->insert($notification_array);
        // echo "<pre>";
        // print_r($notification_array);
        // exit();
    }
    public function notificationCount($user_id)
    {
        return Notification::select('read_unread')->where(['user_id'=>$user_id,'read_unread'=>0])->count() ?? 0;
    }
 public function sendNotification($tokens,$title,$description,$type,$file=null,$type_val)
    {
        // $user_id=83;$description="Test notification from laravel ".date("d-m-Y h:i:s");$title="Test from web";$type="order";$file=null;
        // $tokens = [];
        // $apns_ids = [];
        $responseData = [];

        // for Android
            if ($type_val == 'android')
            {


                $msg = array('type'=>$type,'title' => $title,'body'  => $description,'subtitle' => 'This is a subtitle','image'=>$file,'android'=>array("imageUrl"=>$file),
                      );

                $fields = array
                        (
                            'registration_ids'  => $tokens,
                            'notification'  => $msg
                        );

                $headers = array
                        (
                            'Authorization: key=' . env('FCM_SERVER_KEY'),
                            'Content-Type: application/json'
                        );

                $ch = curl_init();
                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                curl_setopt( $ch,CURLOPT_POST, true );
                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                $result = curl_exec($ch );

                if ($result === FALSE)
                {
                    die('FCM Send Error: ' . curl_error($ch));
                }

                $result = json_decode($result,true);

                $responseData['android'] =[
                           "result" =>$result
                        ];

                curl_close( $ch );

            }

        // for IOS
            // $read_unread = $this->notificationCount($user_id);
            $read_unread = 0;
            if ($type_val == 'ios')
            {

                $apns_ids = $tokens;


                $url = "https://fcm.googleapis.com/fcm/send";

                $serverKey = env('FCM_SERVER_KEY');
                $title = $title;
                $body = $description;
                $notification = array('type'=>$type, 'title' =>$title , 'body' => $body ,'text' => $body, 'sound' => 'default', 'badge' => $read_unread,'image'=>$file);
                $arrayToSend = array('registration_ids' => $apns_ids, 'notification' => $notification,'priority'=>'high');
                $json = json_encode($arrayToSend);
                $headers = array();
                $headers[] = 'Content-Type: application/json';
                $headers[] = 'Authorization: key='. $serverKey;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                //Send the request
                $result = curl_exec($ch);

                if ($result === FALSE)
                {
                    die('FCM Send Error: ' . curl_error($ch));
                }
                $result = json_decode($result,true);
                // echo "<pre>";
                // print_r($result);
                // exit();
                $responseData['ios'] =[
                            "result" =>$result
                        ];

                //Close request
                curl_close($ch);
            }

         return $responseData;

     }
     public function handleOld()
    {

        try {

            $users = User::where(['is_deleted'=>0,'status'=>1,'notification'=>1])
            ->has('DeviceToken')->orderBy('id','desc')->get();
            foreach ($users as $key => $value) {
                $this->sendNotification($value->id,$this->title,$this->message,$this->type,$this->file,'');
                $notification['user_id'] = $value->id;
                $notification['title'] = $this->title;
                $notification['message'] = $this->message;
                $notification['type'] = $this->type;
                if($this->file) {
                $notification['image'] = str_replace("https://chilling-assets.s3.us-east-2.amazonaws.com/", '', $this->file);
                }
                $notification_array[] = $notification;
            }
            $this->notification_entry_for_all($notification_array);
          } catch (Exception $e) {
             $exception = $e->getMessage();
            //  Api\ApplicationEvents\LogWritter::write( $exception, 'Error');

          }
    }
    public function sendNotificationOld($user_id,$title,$description,$type,$file=null)
    {
        // $user_id=83;$description="Test notification from laravel ".date("d-m-Y h:i:s");$title="Test from web";$type="order";$file=null;
        $tokens = [];
        $apns_ids = [];
        $responseData = [];

        // for Android
            if ($FCMTokenData = fcmToken::where('user_id',$user_id)->where('device_token','!=',null)->where('device_type','ANDROID')->select('device_token')->get())
            {
                foreach ($FCMTokenData as $key => $value)
                {
                    $tokens[] = $value->device_token;

                }

                $msg = array('type'=>$type,'title' => $title,'body'  => $description,'subtitle' => 'This is a subtitle','image'=>$file,'android'=>array("imageUrl"=>$file),
                      );

                $fields = array
                        (
                            'registration_ids'  => $tokens,
                            'notification'  => $msg
                        );

                $headers = array
                        (
                            'Authorization: key=' . env('FCM_SERVER_KEY'),
                            'Content-Type: application/json'
                        );

                $ch = curl_init();
                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                curl_setopt( $ch,CURLOPT_POST, true );
                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                $result = curl_exec($ch );

                if ($result === FALSE)
                {
                    die('FCM Send Error: ' . curl_error($ch));
                }

                $result = json_decode($result,true);

                $responseData['android'] =[
                           "result" =>$result
                        ];

                curl_close( $ch );

            }

        // for IOS
            $read_unread = $this->notificationCount($user_id);
            if ($FCMTokenData = fcmToken::where('user_id',$user_id)->where('device_token','!=',null)->where('device_type','IOS')->select('device_token')->get())
            {
               foreach ($FCMTokenData as $key => $value)
                {
                    $apns_ids[] = $value->device_token;
                }

                $url = "https://fcm.googleapis.com/fcm/send";

                $serverKey = env('FCM_SERVER_KEY');
                $title = $title;
                $body = $description;
                $notification = array('type'=>$type, 'title' =>$title , 'body' => $body ,'text' => $body, 'sound' => 'default', 'badge' => $read_unread,'image'=>$file);
                $arrayToSend = array('registration_ids' => $apns_ids, 'notification' => $notification,'priority'=>'high');
                $json = json_encode($arrayToSend);
                $headers = array();
                $headers[] = 'Content-Type: application/json';
                $headers[] = 'Authorization: key='. $serverKey;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                //Send the request
                $result = curl_exec($ch);

                if ($result === FALSE)
                {
                    die('FCM Send Error: ' . curl_error($ch));
                }
                $result = json_decode($result,true);
                // echo "<pre>";
                // print_r($result);
                // exit();
                $responseData['ios'] =[
                            "result" =>$result
                        ];

                //Close request
                curl_close($ch);
            }

         return $responseData;

     }
}
