<?php

namespace App\Console\Commands;

use App\SubscriptionCheck;
use Illuminate\Console\Command;

class SubscriptionCheckCrone extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:subscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This crone job check every user subcription plan within 72 hour daily.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

    }
    public function checkSubscriptionPlan() {
        $users = SubscriptionCheck::whereHas('user',function($q){
            $q->where(['is_deleted'=>0,'status'=>1]);
        })->whereRaw("created_At > DATE_SUB(now(), INTERVAL 3 DAY)")->get();


    }
}
