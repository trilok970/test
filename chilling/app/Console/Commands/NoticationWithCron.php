<?php

namespace App\Console\Commands;

use App\Jobs\NotificationJob;
use App\Notification;
use App\NotificationTemp;
use App\NotificationTempEntry;
use App\User;
use Illuminate\Console\Command;
use Artisan;
use Illuminate\Support\Facades\Log;
use DB;
use Exception;

class NoticationWithCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:crone';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Store Notification to table with last user id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        Log::info('Sleep start : '.date("Y-m-d h:i:s"));

        sleep(rand(10,30));

        Log::info('Sleep end : '.date("Y-m-d h:i:s"));

        $notificationTempEntry = NotificationTempEntry::where(['status'=>0])->first();

        if($notificationTempEntry) {
            $notificationTemp = NotificationTemp::where(['notification_id'=>$notificationTempEntry->id])->latest()->first();
            if($notificationTemp) {
                try {
                    $from = (int)$notificationTemp->user_id_to;
                    $to = $from+5000;
                    $from = $from+1;
                    $notification_temp_exist = NotificationTemp::where(['notification_id'=>$notificationTempEntry->id,'user_id_from'=>$from,'user_id_to'=>$to])->exists();
                    Log::info('Starting from : '.date("Y-m-d h:i:s"));
                    if(!$notification_temp_exist) {

                        $notificationTemp = NotificationTemp::where(['notification_id'=>$notificationTempEntry->id])->update(['user_id_from'=>$from,'user_id_to'=>$to]);

                        Log::info('Start : from '.$from.' , to : '.$to.' notification id >> '.@$notificationTempEntry->id);

                        $users = User::where(['is_deleted'=>0,'status'=>1,'notification'=>1])
                                ->whereRaw("(device_token !='' and device_token is not null)")
                                ->whereBetween('id',[$from,$to])
                                ->get(['id','is_deleted','status','device_token','notification','device_type']);

                                // Log::info('Sql query : '.$users->toSql());

                        $usersIds = $users->pluck('id')->toArray();
                        if(count($users) === 0) {
                            NotificationTemp::where(['notification_id'=>$notificationTempEntry->id])->delete();
                            NotificationTempEntry::where(['id'=>$notificationTempEntry->id])->update(['status'=>1]);

                            echo "Users not found.";
                            return false;
                        }
                        if(count($users) > 0) {
                            $notificationCount = Notification::where('notification_temp_entriy_id',$notificationTempEntry->id)->whereIn('user_id',$usersIds)->count();
                            Log::info('notificationCount : '.$notificationCount);

                            // if($notificationCount == 0) {

                                if($this->notification_entry_for_all($users,$notificationTempEntry))
                                {
                                    NotificationJob::dispatch(ucwords($notificationTempEntry->title),$notificationTempEntry->message,$notificationTempEntry->type,$notificationTempEntry->image,$users);

                                    // $notificationTemp = NotificationTemp::where(['notification_id'=>$notificationTempEntry->id])->update(['user_id_from'=>$from,'user_id_to'=>$to]);

                                    Log::info('End : '.date("Y-m-d h:i:s"));


                                    echo "Notification send successfully.";
                                    return true;
                                }

                        // }
                        }

                    }
                }
                catch(\Exception $e) {
                    Log::info('Exception : '.$e->getLine()." ".$e->getMessage());
                }
            }

        }
        else {
            echo "No record found.";
            return false;
        }
    }
    public function notification_entry_for_all($users,$notificationTempEntry)
    {
        $tokens = [];
        $apns_ids = [];
        Log::info('users : '.json_encode($users->pluck('id')->toArray()));
        $notification_array = [];
        foreach ($users as $key => $user)
        {
            $notification = [];
            $notification['user_id'] = $user->id;
            $notification['notification_temp_entriy_id'] = $notificationTempEntry->id;
            $notification['title'] = $notificationTempEntry->title;
            $notification['message'] = $notificationTempEntry->message;
            $notification['type'] = $notificationTempEntry->type;
            if($notificationTempEntry->file) {
            $notification['image'] = str_replace("https://chilling-assets.s3.us-east-2.amazonaws.com/", '', $notificationTempEntry->file);
            }
            $notification_array[] = $notification;
        }
        // Log::info('notification_array : '.json_encode($notification_array));

        if($notification_array) {
            //$notification = new Notification();
            //$notification->insert($notification_array);
            //$values = array('id' => 1,'name' => 'Dayle');
            DB::table('notifications')->insert($notification_array);
            return true;
        }
        else {
            return false;
        }
    }

}
