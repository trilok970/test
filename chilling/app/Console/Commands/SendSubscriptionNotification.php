<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Http\Controllers\UserFcmTokenController;
use App\Jobs\NotificationJob;

class SendSubscriptionNotification extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification to all users that subscription plan will expire soon.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // echo "done successfully";
        // $fcm = new UserFcmTokenController;

        // $fcm->sendNotificationSubscriptionPlan("Subscription Plan Expire Soon","Your subscription plan will be expire soon.Please recharge your account to continue enjoy this service.","plan");

        $fileName  = 'images/1615808524.png';
        $file = env('AWS_URL')."/".$fileName;
        $users = User::where(['is_deleted'=>0,'status'=>1,'notification'=>1])->whereRaw('(device_token != "" or device_token != null)')->whereRaw("subscription_plan_last_date BETWEEN DATE(now()) and DATE_ADD(DATE(now()),INTERVAL 5 DAY)")->get();
        NotificationJob::dispatch("Subscription Plan Expire Soon","Your subscription plan will be expire soon.Please recharge your account to continue enjoy this service.","plan",$file,$users);
        // echo "<pre>";
        // print_r($users);
        echo "Send notification to all users that subscription plan will expire soon.";
    }
}
