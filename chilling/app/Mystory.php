<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mystory extends Model
{
    public function category()
    {
    	return $this->belongsTo(Category::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    public function viewCount()
    {
        return $this->view_count;
    }
}
