<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminSetting extends Model
{

    protected $fillable = ['animated_banner'];

    protected $primaryKey = 'id';

    public $table = "admin_setting";

    protected $appends = [];
    public function getAnimatedBannerAttribute($value)
    {
        if ($value) {
           // $client = \Storage::disk('s3')->getDriver()->getAdapter()->getClient();
            // $bucket = \Config::get('filesystems.disks.s3.bucket');

            // $command = $client->getCommand('GetObject', [
            //     'Bucket' => $bucket,
            //     'Key' => $value  // file name in s3 bucket which you want to access
            // ]);

            // $request = $client->createPresignedRequest($command, '+10 minutes');

            // Get the actual presigned-url
            // return $presignedUrl = (string)$request->getUri();
            return \Storage::disk('s3')->url($value);
        }
    }
    public function getLoginBannerAttribute($value)
    {
        if ($value) {
            // $client = \Storage::disk('s3')->getDriver()->getAdapter()->getClient();
            // $bucket = \Config::get('filesystems.disks.s3.bucket');

            // $command = $client->getCommand('GetObject', [
            //     'Bucket' => $bucket,
            //     'Key' => $value  // file name in s3 bucket which you want to access
            // ]);

            // $request = $client->createPresignedRequest($command, '+10 minutes');

            // Get the actual presigned-url
            // return $presignedUrl = (string)$request->getUri();
            return \Storage::disk('s3')->url($value);
        }
    }
    public function getRegisterBannerAttribute($value)
    {
        if ($value) {
            // $client = \Storage::disk('s3')->getDriver()->getAdapter()->getClient();
            // $bucket = \Config::get('filesystems.disks.s3.bucket');

            // $command = $client->getCommand('GetObject', [
            //     'Bucket' => $bucket,
            //     'Key' => $value  // file name in s3 bucket which you want to access
            // ]);

            // $request = $client->createPresignedRequest($command, '+10 minutes');

            // Get the actual presigned-url
            // return $presignedUrl = (string)$request->getUri();
            return \Storage::disk('s3')->url($value);
        }
    }
}
