<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable implements MustVerifyEmail
{
   use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $appends = ['is_premium','is_receipt'];
    public function makeEmailAsVerified(){
        $user = User::find($this->id);
        if($user && $user->email_verified_at == null) {
            $user->email_verified_at = date('Y-m-d h:i:s');
            $user->save();
        }
    }
    public function getIsPremiumAttribute()
    {

        $date = date("Y-m-d");
        $milliseconds = round(microtime(true) * 1000);
        $subscription = $this->getUserSubscription();

        if($this->subscription_plan_admin == 1){
            return 1;
        }
        else if($this->expire_mili_seconds != null || $this->expire_mili_seconds != '') {
            if($this->expire_mili_seconds > $milliseconds) {
                return 1;
            }
            else {
               return $this->receiptCheck($subscription);
            }
        }
        else
        {
             return $this->receiptCheck($subscription);
        }

        // if(strtoupper($this->device_type) == 'IOS') {

        // }
        // else if(strtoupper($this->device_type) == 'ANDROID') {
        //     if($subscription && ($subscription->receipt != null || $subscription->receipt != '')) {
        //         return $this->AndroidReceiptValidator($subscription->receipt);
        //     }
        //     else {
        //         return 0;
        //     }
        //     // return $user = User::where('id',$this->id)->where('subscription_plan_last_date','>=',$date)->count();

        // }

    }
    public function receiptCheck($subscription) {
        if($subscription && ($subscription->receipt != null || $subscription->receipt != '')) {

                if(is_string($subscription->receipt))
                {
                    $receipt_data = json_decode($subscription->receipt);

                    if(isset($receipt_data->purchaseToken)) {
                        return $this->AndroidReceiptValidator($subscription->receipt);
                    }
                    else{
                        return $this->IosReceiptValidator($subscription->receipt);
                    }
                }
                else {
                    return 0;
                }

            }
            else {
                return 0;
            }
    }
    public function getIsReceiptAttribute()
    {
        $subscription = $this->getUserSubscription();
        if($subscription) {
            if($subscription->receipt == null || $subscription->receipt == '') {
                return 0;
            }
            else {
                return 1;
            }
        }
        else {
            return 0;
        }

    }
    protected function getUserSubscription() {
      return  $subscription = UserSubscription::where('user_id',$this->id)->latest()->first();
    }
    protected function IosReceiptValidator($receipt_data) {
        // Make Post Fields Array
        $data = [
            'receipt-data' => $receipt_data,
            'password' => env('IOS_SECRET'),
        ];
        $url = "https://buy.itunes.apple.com/verifyReceipt";
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $response = json_decode($response,true);
        // echo "<pre>";
        // echo $response['status'];
        // print_r($response);exit;
        if($response && $response['status'] == 0 && isset($response['latest_receipt_info'][0]['expires_date'])) {
            $expire_date =  date("Y-m-d h:i:s",strtotime($response['latest_receipt_info'][0]['expires_date']));
            $expiryTimeMillis = $response['latest_receipt_info'][0]['expires_date_ms'];
            $today_date = date("Y-m-d h:i:s");
            $milliseconds = round(microtime(true) * 1000);

            // if($expire_date > $today_date) {
            if($expiryTimeMillis > $milliseconds) {
                $this->userSubscriptionDateUpdate($expire_date,$expiryTimeMillis);
                return 1;
            }
            else {
                return 0;
            }
        }
        else {
            return 0;
        }
    }
    protected function userSubscriptionDateUpdate($date,$expire_mili_seconds) {
        $user = User::where(['id'=>$this->id,'is_deleted'=>0])->first();
        $user->subscription_plan_last_date = date('Y-m-d',strtotime($date));
        $user->expire_mili_seconds = $expire_mili_seconds;
        $user->save();
    }
    protected function AndroidReceiptValidator($receipt_data) {
       $receipt_data = json_decode($receipt_data);

        $client = new \Google\Client();
        $client->setApplicationName('Chilling.io');
        $client->setAuthConfig(json_decode(env('GOOGLE_SECRET'),true));
        $client->setScopes([\Google_Service_AndroidPublisher::ANDROIDPUBLISHER]);
        // For Auth2
        // $validator = new PlayValidator(new \Google_Service_AndroidPublisher($client));
        // For Service Account

        $googleAndroidPublisher = new \Google_Service_AndroidPublisher($client);
        $validator = new \ReceiptValidator\GooglePlay\Validator($googleAndroidPublisher);

        try {
          $response = $validator->setPackageName($receipt_data->packageName ?? "com.chilling")
            ->setProductId($receipt_data->productId ?? "com.1monthunlimited")
            ->setPurchaseToken($receipt_data->purchaseToken)
            ->validateSubscription();
           $response = (array)$response;
           // echo count($response);
            // print_r($response) ;
            if(!is_null($response)) {
                foreach ($response as $key => $value) {
                $expiryTimeMillis =  $value['expiryTimeMillis'];
                $expire_date = date('Y-m-d h:i:s', $expiryTimeMillis/1000. - date("Z"));
                break;
                }
                $today_date = date("Y-m-d h:i:s");
                $milliseconds = round(microtime(true) * 1000);
                // if($expire_date > $today_date) {
                if($expiryTimeMillis > $milliseconds) {
                    $this->userSubscriptionDateUpdate($expire_date,$expiryTimeMillis);
                    return 1;
                }
                else {
                    return 0;
                }
            }
            else {
                return 0;
            }

        } catch (\Exception $e){
          // var_dump($e->getMessage());
          return 0;
          // example message: Error calling GET ....: (404) Product not found for this application.
        }
        // success

exit;
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {
        // Your your own implementation.
        $this->notify(new ResetPasswordNotification($token));
    }

    public function DeviceToken()
    {
        return $this->hasMany(DeviceToken::class);
    }
    public function UserSubscription()
    {
        return $this->hasMany(UserSubscription::class);
    }
    public function getProfilePicAttribute($value)
    {
        if ($value) {
            // $client = \Storage::disk('s3')->getDriver()->getAdapter()->getClient();
            // $bucket = \Config::get('filesystems.disks.s3.bucket');

            // $command = $client->getCommand('GetObject', [
            //     'Bucket' => $bucket,
            //     'Key' => $value  // file name in s3 bucket which you want to access
            // ]);

            // $request = $client->createPresignedRequest($command, '+10 minutes');

            // Get the actual presigned-url
            // return $presignedUrl = (string)$request->getUri();
            return \Storage::disk('s3')->url($value);
        }
    }

}
