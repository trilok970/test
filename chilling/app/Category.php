<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    public function playlists()
    {
    	return $this->hasMany(Playlist::class);
    }
    public function subcategorys()
    {
    	return $this->hasMany(SubCategory::class);
    }
    public function mystories()
    {
    	return $this->hasMany(Mystory::class);
    }
}
