<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AudioDuration extends Model
{
    public function audio()
    {
    	return $this->belongsTo(Audio::class);
    }
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
