<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyplaylistAudio extends Model
{
    public function myplaylist()
    {
    	return $this->belongsTo(Myplalist::class);
    }
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    public function audio()
    {
    	return $this->belongsTo(Audio::class);
    }

}
