<?php

namespace App\Http\Controllers;

use App\Audio;
use App\AudioCategory;
use App\Category;
use App\Channel;
use App\Playlist;
use App\PlaylistAudio;
use App\Subscriptionplan;
use App\Banner;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\UserFcmTokenController;
use DB;
use App\Lib\VIDEOSTREAM;
use wapmorgan\Mp3Info\Mp3Info;


class AudioController extends Controller
{
    public $fcm;
    public $paginate_no;

  public function __construct()
    {
        $this->paginate_no = config('constants.paginate_no');
        $this->api_per_page = config('constants.api_per_page');
        $this->fcm = new UserFcmTokenController;


    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        if($request->query())
        {
            $search = trim($request->search);
            $search = str_replace("’","'",$request->search);

            $datas = Audio::select(DB::raw("DISTINCT(audio.id)"),'audio.*')->where(['audio.is_deleted'=>0,'audio.status'=>1])
            ->leftjoin("audio_categories as au_cat",function($join){
                $join->on("au_cat.audio_id",'=','audio.id');
            })
            ->leftjoin("categories as cat",function($join){
                $join->on("cat.id",'=','au_cat.category_id');
            })
            ->where(function($query) use ($search){
                $query->orWhere('audio.title','like','%'.$search.'%');
                $query->orWhere('audio.tags','like','%'.$search.'%');
                $query->orWhere('audio.author','like','%'.$search.'%');
                $query->orWhere('audio.narrator','like','%'.$search.'%');
                $query->orWhere('cat.name','like','%'.$search.'%');
                $query->orWhere('audio.created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
            })
            ->orderBy('audio.id','desc')
            ->orderBy('audio.order_number','asc')
            ->paginate($this->paginate_no);
        }
        else
        {
            $datas = Audio::where(['is_deleted'=>0])->orderBy('audio.id','desc')->orderBy('order_number','asc')->paginate($this->paginate_no);
            $search = '';
        }


        return view('admin/audio.index', compact('datas','search'));
    }
    public function audio_search(Request $request)
    {
            // echo "<pre>";
            // print_r($playlistAudios);
            // exit;

        if($request->query())
        {
        $playlist = Playlist::find($request->playlist_id);
        if($playlist) {
            $playlistAudios = $playlist->PlaylistAudio->pluck('audio_id')->toArray() ?? "";
        }
        else {
            $playlistAudios = "";

        }

            $search = trim($request->search);
            $search = str_replace("’","'",$request->search);

            $datas = Audio::select(DB::raw("DISTINCT(audio.id)"),'audio.*')->where(['audio.is_deleted'=>0,'audio.status'=>1])
            ->leftjoin("audio_categories as au_cat",function($join){
                $join->on("au_cat.audio_id",'=','audio.id');
            })
            ->leftjoin("categories as cat",function($join){
                $join->on("cat.id",'=','au_cat.category_id');
            })
            ->where(function($query) use ($search){
                $query->orWhere('audio.title','like','%'.$search.'%');
                $query->orWhere('audio.tags','like','%'.$search.'%');
                $query->orWhere('audio.author','like','%'.$search.'%');
                $query->orWhere('audio.narrator','like','%'.$search.'%');
                $query->orWhere('cat.name','like','%'.$search.'%');
                $query->orWhere('audio.created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
            })
            ->when($playlistAudios,function($q) use ($playlistAudios) {
                return $q->whereNotIn('audio.id',$playlistAudios);
            })
            ->orderBy('audio.order_number','asc')->get();
        }
        else
        {
            $datas = Audio::where(['is_deleted'=>0])->whereNotIn('id',$playlistAudios)->orderBy('order_number','asc')->get();
            $search = '';
        }
        $data = '<table class="table table-striped table-bordered" style="width:82%;">';


                $i = 1;
                foreach($datas as $row)
                {
                    // if(in_array($row->id,$playlistAudios))
                    // $audio_check = "checked";
                    // else
                    // $audio_check = "";


                    $data .='<tr>
                    <td><input type="checkbox" name="audio_id[]" id="audio_id" value="'.$row->id.'"  /></td>
                    <td>'.$row->title.'</td>
                    <td>'.$row->tags.'</td>
                    <td>'.$row->author.'</td>
                    <td>'.$row->narrator.'</td>
                    <td><a target="_blank" href="'.$row->image.'"><img src="'.$row->image.'" width="50" /></a></td>

                   </tr> ';
                }

                $data .= '</table>';

                return $data;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where(['is_deleted'=>0,'status'=>1])->get();

        return view('admin/audio.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = validator::make($request->all(), [
            'title' => 'required',
            'category_id' => 'required',
            'tags' => 'required',
            'author' => 'required',
            'narrator' => 'required',
            'description' => 'required',
            'file' => 'required|max:150000',
            'image' => 'required|mimes:jpeg,jpg,gif,png|max:5048',
        ]);
        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }
        $audio = new Audio;


        if ($request->hasFile('file') && $request->file->isValid()) {
            $extensionAudio = $request->file->extension();
            $extensionAudio = str_replace("oga","mp3",$extensionAudio);
            $extensionAudio = str_replace("ogg","mp3",$extensionAudio);

            // $fileNameAudio  = "uploads/audio/" . time() . ".$extensionAudio";
            // $ff1 = $request->file->move(public_path('uploads/audio'), $fileNameAudio);
            // $MOVIE = new VIDEOSTREAM();
		    // $duration = $MOVIE->getDurationSeconds($ff1);
            if(in_array($extensionAudio,['mp3','MP3','wav','WAV','ogg','OGG','oga','OGA'])) {

                $fileArray  =  audio_upload($request->file,"audio",'public');
                $audio->file  = $fileArray['fileName'];
                $audio->extension = $extensionAudio;
            }
            else {
                $validator = $validator->errors()->add('file','The file must be a file of type: mp3, MP3, wav, WAV, ogg, OGG.');
                return back()
                ->withInput()
                ->withErrors($validator);
            }
        }
        else {
            $audio->file = "";
            $audio->extension = "";
        }
        if ($request->hasFile('image') && $request->image->isValid()) {
            $audio->image  =  image_upload($request->image,"images",'public');
        } else {
            $audio->image = 'images/default.jpg';
        }
        $audio->title = ucwords($request->title);
        $audio->is_free = $request->is_free ?? 0;
        $audio->tags = $request->tags;
        $audio->author = $request->author;
        $audio->narrator = $request->narrator;
        $audio->description = $request->description;
        $audio->duration  = $request->duration;
        if ($audio->save()) {
            if ($request->category_id) {
                foreach ($request->category_id as $category) {
                    $audio_category = new AudioCategory;
                    $audio_category->audio_id = $audio->id;
                    $audio_category->category_id = $category;
                    $audio_category->save();
                }
            }
            // Send notification to all users
            // $this->fcm->sendNotificationToAllUsers("New song added","1 new song added in audio list","audio");
            return redirect('admin/audio')->with('message', 'Audio added successfully');
        }
    }
    public function colors()
    {
     return $color = array("success","danger","warning","info","light","dark","secondary");
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function show(Audio $audio)
    {
        //
        $colors  = $this->colors();

        return view('admin/audio.show', compact('audio','colors'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function edit(Audio $audio)
    {
        $categories = Category::where(['is_deleted'=>0,'status'=>1])->get();

        return view('admin/audio.edit', compact('audio','categories'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Audio $audio)
    {



        $validator = validator::make($request->all(), [
            'title' => 'required',
            'category_id' => 'required',
            'tags' => 'required',
            'author' => 'required',
            'narrator' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }

        if ($request->hasFile('image') && $request->image->isValid()) {
            $validator = validator::make($request->all(), [
                'image' => 'required|mimes:jpeg,jpg,gif,png|max:2048',
            ]);
            if ($validator->fails()) {
                return back()
                    ->withInput()
                    ->withErrors($validator);
            }

        // $file = $request->file('image');
        // $imageName=time().$file->getClientOriginalName();
        // $filePath = 'images/' . $imageName;
        // \Storage::disk('s3')->put($filePath, file_get_contents($file));
        $audio->image  =  image_upload($request->image,"images",'public');

        }
        if ($request->hasFile('file') && $request->file->isValid()) {
            $validator = validator::make($request->all(), [
                'file' => 'required|max:150000',
            ]);
            if ($validator->fails()) {
                return back()
                    ->withInput()
                    ->withErrors($validator);
            }
            $extensionAudio = $request->file->extension();
            $extensionAudio = str_replace("oga","mp3",$extensionAudio);
            $extensionAudio = str_replace("ogg","mp3",$extensionAudio);

            // $fileNameAudio  = "audio/" . time() . ".$extensionAudio";
            // $ff1 = $request->file->move(public_path('audio'), $fileNameAudio);

            if(in_array($extensionAudio,['mp3','MP3','wav','WAV','ogg','OGG','oga','OGA'])) {

                $fileArray  =  audio_upload($request->file,"audio",'public');
                $audio->file  = $fileArray['fileName'];
                $audio->extension = $extensionAudio;
            }
            else {
                $validator = $validator->errors()->add('file','The file must be a file of type: mp3, MP3, wav, WAV, ogg, OGG.');
                return back()
                ->withInput()
                ->withErrors($validator);
            }

            // $MOVIE = new VIDEOSTREAM();
		    // $audio->duration = $MOVIE->getDurationSeconds($finalFilePath);
        }

        $audio->title = ucwords($request->title);
        $audio->is_free = $request->is_free ?? 0;
        $audio->tags = $request->tags;
        $audio->author = $request->author;
        $audio->narrator = $request->narrator;
        $audio->description = $request->description;
        $audio->duration  = $request->duration;
        if ($audio->save()) {
            if ($request->category_id) {
                AudioCategory::where('audio_id',$audio->id)->delete();
                foreach ($request->category_id as $category) {
                    $audio_category = new AudioCategory;
                    $audio_category->audio_id = $audio->id;
                    $audio_category->category_id = $category;
                    $audio_category->save();
                }
            }
            // Send notification to all users
            // $this->fcm->sendNotificationToAllUsers("New song added","1 new song added in audio list","group");
            return redirect('admin/audio')->with('message', 'Audio updated successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Audio $audio)
    {
        //
        $audio->is_deleted = 1;
        if ($audio->save()) {
            return redirect('admin/audio')->with('message', 'Audio deleted successfully');
        } else {
            return back()->with('message', 'Audio not deleted');
        }
    }
    public function update_order(Request $request)
    {
        $post_order = isset($_POST["post_order_ids"]) ? $_POST["post_order_ids"] : [];

        if(count($post_order)>0)
        {
            for($order_no= 0; $order_no < count($post_order); $order_no++)
            {
            if($request->type=='audio')
            $data = Audio::where(['id'=>$post_order[$order_no]])->first();
            else if($request->type=='channel')
            $data = Channel::where(['id'=>$post_order[$order_no]])->first();
            else if($request->type=='playlist')
            $data = Playlist::where(['id'=>$post_order[$order_no]])->first();
            else if($request->type=='playlist_audio')
            $data = PlaylistAudio::where(['id'=>$post_order[$order_no]])->first();
            else if($request->type=='subscriptionplan')
            $data = Subscriptionplan::where(['id'=>$post_order[$order_no]])->first();
            else if($request->type=='banner')
            $data = Banner::where(['id'=>$post_order[$order_no]])->first();
            $data->order_number = $order_no+1;
            $data->save();

            }
            echo true;
        }
        else
        {
            echo false;
        }
    }
    public function audios(Request $request)
    {
        if($request->author)
           { $val = $author = trim($request->author);$key = "Author"; }
        else
           {  $author = '';  }
        if($request->narrator)
            { $val = $narrator = trim($request->narrator); $key = "Narrator"; }
            else
            { $narrator = ''; }
        if($request->title)
            {  $val = $title = trim($request->title); $key = "Title"; }
        else
            {  $title = ''; }
        if($request->description)
            { $val = $description = trim($request->description); $key = "Description"; }
        else
            {  $description = ''; }



            $audios = Audio::where(['is_deleted'=>0])
                    ->when($author,function($q) use ($author){
                        return $q->where('author','like','%'.$author.'%');
                    })
                    ->when($narrator,function($q,$narrator){
                      return  $q->where('narrator','like','%'.$narrator.'%');
                    })
                    ->when($title,function($q,$title){
                        return $q->where('title','like','%'.$title.'%');
                    })
                    ->when($description,function($q,$description){
                        return $q->where('description','like','%'.$description.'%');
                    })
                ->orderBy('order_number','asc')->paginate($this->paginate_no);
                $colors  = $this->colors();

                return view('admin.audio.audios',compact('audios','colors','key','val'));

    }



    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
