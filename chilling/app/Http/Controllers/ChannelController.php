<?php

namespace App\Http\Controllers;

use App\Channel;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Http\Controllers\UserFcmTokenController;

class ChannelController extends Controller
{
    public $fcm;
    public $paginate_no;

  public function __construct()
    {
        $this->paginate_no = config('constants.paginate_no');
        $this->api_per_page = config('constants.api_per_page');
        $this->fcm = new UserFcmTokenController;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datas = Channel::where(['is_deleted'=>0])->orderBy('order_number','asc')->get();
        return view('admin/channel.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin/channel.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function check_name($name)
    {
        return Channel::where(['name'=>$name,'is_deleted'=>0])->count();
    }
    public function store(Request $request)
    {
        //
        $validator = validator::make($request->all(),[
        'name' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        if($this->check_name($request->name) > 0)
        {
            $validator = $validator->errors()->add('name','The name has already been taken.');
            return back()->withInput()->withErrors($validator);
        }
        $channel = new Channel;
        $channel->name = ucfirst($request->name);
        $channel->user_id = Auth::user()->id;
        $channel->status = $request->status ?? 1;
        if($channel->save())
        {
             // Send notification to all users
            //  $this->fcm->sendNotificationToAllUsers("New channel added","1 new channel added in channel list","channel");
            return redirect('admin/channel')->with('message','Channel added successfully');
        }
        else
        {
            return back()->with('message','Channel not added');
        }
    }
    public function colors()
    {
     return $color = array("success","danger","warning","info","light","dark","secondary");
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $channel = Channel::find($id);
        // echo "<pre>";
        // print_r($channel->playlists->toArray());
        // exit;
        $colors  = $this->colors();

        return view('admin/channel.show',compact('channel','colors'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function edit(Channel $channel)
    {
        //
        return view('admin/channel.edit',compact('channel'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Channel $channel)
    {
        //
        $validator = validator::make($request->all(),[
        'name' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        if($request->name != $channel->name && $this->check_name($request->name) > 0)
        {
            $validator = $validator->errors()->add('name','The name has already been taken.');
            return back()->withInput()->withErrors($validator);
        }
        $channel->name = ucfirst($request->name);
        $channel->user_id = Auth::user()->id;
        $channel->status = $request->status ?? 1;
        if($channel->save())
        {
            // Send notification to all users
            // $this->fcm->sendNotificationToAllUsers("New channel updated","1 new channel updated in channel list","channel");
            return redirect('admin/channel')->with('message','Channel Updated Successfully');
        }
        else
        {
            return back()->with('message','Channel Not Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Channel $channel)
    {
        //
        $channel->is_deleted = 1;
        if($channel->save())
        {
            return redirect('admin/channel')->with('message','Channel Deleted Successfully');
        }
        else
        {
            return back()->with('message','Channel Not Deleted');
        }
    }
}
