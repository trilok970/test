<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Passport;
use App\User;
use DB;
use Hash;
use File;
use Str;
use Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserMail;
use Auth;
use App\Channel;
use App\Category; 
use App\SubCategory;
use App\PlaylistAudio;
use App\Playlist;
use App\Banner;
use App\Song;
use App\Ambiance;
use App\Mystory;
use App\Audio;
use App\Myplaylist;
use App\MyplaylistAudio;
use App\Favourite;
use App\Subscriptionplan;
use App\Cms;
use App\DeviceToken;
use App\UserSubscription;
use App\Notification;
use App\AdminSetting;
use App\Appversion;
use App\AudioDuration;
use App\PlaylistCurrentAudio;
use App\PlaylistUser;
use App\SubscriptionCheck;
use Illuminate\Support\Facades\Log;
 
class PassportController extends Controller
{
    //
	 public $successStatus = 200;
	 public $failureStatus = 401;

	public function __construct()
    {
        $this->api_per_page = config('constants.api_per_page');
    }
	public function sendResponse($result, $message)
    {
    	$response = [
            'status' => true,
			'message' => $message,
            'data'    => $result,

        ];


        return response()->json($response, 200);
    }
     /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 200)
    {
    	$response = [
            'status' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }

    private function check_phone_number($phone_number)
    {
		return $user = User::where(["phone_number"=>$phone_number,"is_deleted"=>0])->count();
	}
	private function check_email($email)
    {
		return $user = User::where(["email"=>$email,"is_deleted"=>0])->count();
	}
	public function check_device_token($user_id,$device_token,$device_type)
	{
		$count = DeviceToken::where(['device_type'=>strtoupper($device_type),'device_token'=>$device_token])->count();
		if($count == 0)
		{
			$devicetoken = new DeviceToken;
			$devicetoken->user_id = $user_id;
			$devicetoken->device_token = $device_token;
			$devicetoken->device_type = strtoupper($device_type);
			$devicetoken->save();
		}



	}
	public function delete_device_token($user_id,$device_token)
	{
		$device_token = DeviceToken::where(['user_id'=>$user_id,'device_token'=>$device_token])->first();
		if($device_token)
			DeviceToken::where(['user_id'=>$user_id,'device_token'=>$device_token])->delete();


	}
	public function deletekDuplicateUser($email) {
		$userCount = $this->check_email($email);
		if($userCount > 1) {
			$lastId = User::select('id')->where(['is_deleted'=>0])->orderBy('id','desc')->first()->id;
			User::where('id',$lastId)->update(['is_deleted'=>0]);
		}
	}
	public function register(Request $request)
	{
		try {
        // $user = User::find(6);
        // if($user->sendEmailVerificationNotification())
        // {
        //     echo "send";
        // }
        // else {
        //     echo "not";

        // }
        // exit;
		// echo "<pre>";
		// print_r($_POST);exit;
		$validator = Validator::make($request->all(),[
			'fullname' => 'required',
			'email' => 'required|unique:users,email|email',
            'password' => 'required',
			'device_type'=>'required',
			'device_token'=>'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
			 $data['message']	= $validator->errors()->first();
              return response()->json($data);
			}
			// sleep(rand(10,20));

			// if($this->check_email($request->email) > 0)
			// {
			// 	$data['status'] = false;
			// 	$data['message']= "The email already been taken.";
			// 	return response()->json($data);
			// 	exit;
			// }
			$user = new User;

		
			// if($request->hasFile('image') && $request->image->isValid())
   //         {
   //             $extension = $request->image->extension();
   //             $fileName  = time().".$extension";
   //             $request->image->move(public_path('images'),$fileName);
   //             $user->profile_pic = image_upload($request->profile_pic,"images",'public');

   //         }
   //         else
   //         {
   //             $fileName = "default.jpg";
   //         }


			$user->fullname = ucwords($request->fullname);
			$user->phone_number = $request->phone_number;
			$user->email = $request->email;
			$user->password = bcrypt($request->password);
			$user->device_type = strtoupper($request->device_type) ??  "NA";
			$user->device_token = $request->device_token ??  "NA";




	
		if($user->save())
		{
			$user->sendEmailVerificationNotification();

			// Delete duplicate user
			// $this->deletekDuplicateUser($request->email);
			
            // $this->check_device_token($user->id,$request->device_token,$request->device_type);

        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['user'] =  $user;
	

        return $this->sendResponse($success, 'Your registration has been done successfully and email verification link sent on your email id, please check your email!');
		}
	
	}
	catch (\Throwable $e) {
		$data['status'] = false;
		$data['message'] = $e->getMessage();
		$data['data'] = array();
		return response()->json($data, 200);
	}
	}

	 public function login(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'email' => 'required|email',
            'password' => 'required',
			'device_type'=>'required',
			'device_token'=>'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
			 $data['message']	= $validator->errors()->first();
              return response()->json($data);
			}
        $user = User::where(['email'=> $request->email,'is_deleted'=>0])->first();

        if($user)
        {

        	if($user->status==0)
	        {
	        	$data['status'] = false;
				$data['message'] = 'Your account deactivated by administrator.Please contact to administrator.';
				$data['data'] = array();
				return response()->json($data, 200);
                exit;
	        }
	        if($user->is_deleted==1)
	        {
	        	$data['status'] = false;
				$data['message'] = 'Your account deleted by administrator.Please contact to administrator.';
				$data['data'] = array();
				return response()->json($data, 200);
                exit;
	        }
            if($user->email_verified_at==null)
	        {
                if($user->created_at < date('2021-07-01')) {
			        //$user->sendEmailVerificationNotification();

                    $data['status'] = false;
                    $data['message'] = 'Your account is not verified.Email verification link sent on your email id, please check your mail and verify your account.';
                    $data['data'] = array();
                    return response()->json($data, 200);
                    exit;
                }
                $user->sendEmailVerificationNotification();
	        	$data['status'] = false;
				$data['message'] = 'Your account is not verified.Please check your mail and verify your account.';
				$data['data'] = array();
				return response()->json($data, 200);
                exit;
	        }

			if (Hash::check($request->password, $user->password))
			{
				try{
				$token = $user->createToken('Laravel Password Grant Client')->accessToken;
				$user->device_token = $request->device_token;
				$user->device_type = strtoupper($request->device_type);
				$user->remember_token = Str::random(100);
				$user->save();

			// $this->check_device_token($user->id,$request->device_token,$request->device_type);


				$data['status'] = true;
				$data['message'] = 'Login Success';
				$path = url('/').'/public/images/'.'/';;
				$thumb = url('/').'/public/thumbnail/';
				$data['data'][0] =  $user;
				$data['data'][0]['token'] =  $token;
				$data['data'][0]['path'] =  $path;
				}
		     catch (\Exception $e) {
                $data['status'] = false;
				$data['message'] = $e->getMessage();
				$data['data'] = array();
			    return response()->json($data, 200);
			 }

				//$response['data'] =  $user;
				$msg="Dear ".$user->fullname."<br>You are successfully login";
				//$this->notification($user->device_token, "Login", $msg,"login");

			    return response()->json($data, 200);
			}
			else
			{
				$data['status'] = false;
				$data['message'] = 'Email and password incorrect';
				$data['data'] = array();
			    return response()->json($data, 200);
			}

    }
	else
	{
				$data['status'] = false;
				$data['message'] = 'User does not exist';
				$data['data'] = array();
			    return response()->json($data, 200);
    }

	}
	 public function forgot_password(Request $request)
    {
        $validator = Validator::make($request->all(),[
			'email' => 'required|email',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
			 $data['message']	= $validator->errors()->first();
              return response()->json($data);
			}


             $user = User::where(["email"=>$request->email,'is_deleted'=>0])->first();

            // echo "<pre>";
            // print_r($user);exit;

             if($user!=null)
             {
                if($user->is_deleted==1)
                {
                    $data['status'] = false;
                    $data['message'] = 'Your account deleted by administrator.Please contact to administrator.';
                    $data['data'] = array();
                    return response()->json($data, 200);
                    exit;
                }
                if($user->email_verified_at==null)
                {
                    $data['status'] = false;
                    $data['message'] = 'Please verify your e-mail first.';
                    $data['data'] = array();
                    return response()->json($data, 200);
                    exit;
                }

                 $credentials = request()->validate(['email' => 'required|email']);

                 Password::sendResetLink($credentials);
                 $data['status']  =  true;
                 $data['message'] =  'Your password reset link sent to your registered e-mail id';
                 return response()->json($data, 200);
            }
            else{
                $data['status'] = false;
				$data['message'] = "This e-mail id is not registered";
				$data['data'] = array();
			    return response()->json($data, 200);
            }
    }
    public function reset(Request $request)
    {

        // $credentials = request()->validate([
        //     'email' => 'required|email',
        //     'token' => 'required|string',
        //     'password' => 'required|string|confirmed'
        // ]);
        $validator = Validator::make($request->all(),[
        'email' => 'required|email',
            'token' => 'required',
            'password' => 'required|confirmed'
        ]);
        if($validator->fails())
        {


            return back()
            ->withInput()
            ->withErrors($validator);
        }
// echo "<pre>";
//     	dd(request());
//     	exit;
        $reset_password_status = Password::reset($credentials, function ($user, $password) {
            $user->password = Hash::make($password);
            $user->save();
        });

        if ($reset_password_status == Password::INVALID_TOKEN) {
            return response()->json(["status"=>false,"message" => "Invalid token provided"], 400);
        }

        return response()->json(["status"=>true,"message" => "Password has been successfully changed"]);
    }
    public function test_mail()
    {
    	 Mail::to('trilok_kumar@ninehertzindia.com')->send(new UserMail());
    }
    public function edit_profile(Request $request)
    {
		$user = Auth::user();
    	$validator = Validator::make($request->all(),[
			'fullname' => 'required',
			'email' => 'required|email|unique:users,email,'.$user->id,
            'country_code' => 'required',
			// 'phone_number'=>'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
			 $data['message']	= $validator->errors()->first();
              return response()->json($data);
			}
			$path = url('public/images/').'/';
			if($user)
			{

				if($request->email != $user->email)
				{
					// if($this->check_email($request->email) > 0)
					// {
					// 	$data['status'] = false;
					// 	$data['message']= "The email already been taken.";
					// 	return response()->json($data);
					// }
                    // else
                    // {
                    //     $user->sendEmailVerificationNotification();
                    //     $user->email_verified_at = null;
                    // }
					$user->sendEmailVerificationNotification();
                    $user->email_verified_at = null;
				}


				if($request->hasFile('profile_pic') && $request->profile_pic->isValid())
	           {

	            //    $extension = $request->profile_pic->extension();
	            //    $fileName  = time().".$extension";
	            //    $request->profile_pic->move(public_path('images'),$fileName);
                   $user->profile_pic = image_upload($request->profile_pic,"images",'public');

	           }



				$user->fullname = ucwords($request->fullname);
				$user->phone_number = $request->phone_number ?? "";
				$user->email = $request->email;
				$user->country_code = $request->country_code;
				if($user->save())
				{
				 $user['path'] = $path;
		         $data['status'] =  true;
		         $data['path'] =  $path;
	             $data['message'] =  'User profile updated successfully';
	             $data['data'] =  $user;
		         return response()->json($data, $this->successStatus);
				}
			}
			else
			{
				 $data['status']  = false;
		         $data['path'] =  $path;
	             $data['message'] = 'User not found';
		         return response()->json($data, $this->successStatus);
			}

    }
     public function get_profile(Request $request)
    {

			$user = Auth::user();
			$path = url('public/images/').'/';
			if($user)
			{
				if($request->device_type){
				$user->device_type = strtoupper($request->device_type) ?? "";
				$user->save();
				}
				 $user['path'] = $path;
		         $data['status'] =  true;
		         $data['path'] =  $path;
	             $data['message'] =  'User detail found successfully';
	             $data['data'] =  $user;
		         return response()->json($data, $this->successStatus);

			}
			else
			{
				 $data['status']  = false;
		         $data['path'] =  $path;
				 $data['message'] = 'user not found';
		         return response()->json($data, $this->successStatus);
			}

    }
     public function change_password(Request $request)
    {
    	$validator = Validator::make($request->all(),[
			'old_password' => 'required',
			'new_password' => 'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
			 $data['message']	= $validator->errors()->first();
              return response()->json($data);
			}
			$user = Auth::user();
			$path = url('public/images/').'/';
			if($user)
			{
				 if(Hash::check($request->old_password,$user->password))
				 {
				 	$user->update(['password' => Hash::make($request->new_password)]);
				    $data['status']  =  true;
		            $data['message'] =  'Your password changed successfully';
			        return response()->json($data, $this->successStatus);
				 }
				 else
				 {
				 	 $data['status']  =  false;
		             $data['message'] =  'Old password does not match';
			         return response()->json($data, $this->successStatus);
				 }

			}
			else
			{
				 $data['status']  = false;
	             $data['message'] = 'user not found';
		         return response()->json($data, $this->successStatus);
			}

    }

    public function social_check(Request $request)
    {
		try {

		
    	$validator = Validator::make($request->all(),[
			'social_id' => 'required',
			'social_type' => 'required|in:FACEBOOK,GOOGLE,INSTAGRAM,APPLE',
			// 'email' => 'required',
			// 'fullname' => 'required',
			// 'phone_number' => 'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
			 $data['message']	= $validator->errors()->first();
              return response()->json($data);
			}
			sleep(rand(10,20));

			$user_count = User::where(['social_id'=>$request->social_id,'social_type'=>$request->social_type,'is_deleted'=>0])->count();

			$path = url('public/images/').'/';
			if($user_count > 0)
			{
				$user = User::where(['social_id'=>$request->social_id,'social_type'=>$request->social_type,'is_deleted'=>0])->first();

				if($user->status==0)
		        {
		        	$data['status'] = false;
					$data['message'] = 'Your account deactivated by administrator.Please contact to administrator.';
					$data['data'] = array();
					return response()->json($data, 200);
		        }
		        if($user->is_deleted==1)
		        {
		        	$data['status'] = false;
					$data['message'] = 'Your account deleted by administrator.Please contact to administrator.';
					$data['data'] = array();
					return response()->json($data, 200);
		        }

				$token = $user->createToken('Laravel Password Grant Client')->accessToken;
				if($request->profile_pic && $user->profile_pic == 'user.jpg')
				{

					$imgurl = $request->get('profile_pic');
	                $image_name = 'user_'.$user->id.'.jpeg';
                    $user->profile_pic = 'images/user_'.$user->id.'.jpeg';

                    image_upload_social($image_name,"images",$imgurl);

	            //     $destinationPath = '/images/';
	            //     $ppath = public_path().'/'.$destinationPath;

	            //    file_put_contents($ppath.$image_name,file_get_contents($imgurl));

	            }

				if(!empty($request->fullname) && ($user->fullname == null || $user->fullname == '') ) {
					$user->fullname 	= ucwords($request->fullname);
				}
				$user->device_type  = strtoupper($request->device_type) ??  "";
				$user->device_token = $request->device_token ??  "";
				$user->social_id    = $request->social_id ??  "";
				$user->social_type  = $request->social_type ??  "";
				$user->remember_token = Str::random(100);
                if($user->email_verified_at == null) {
                    $user->email_verified_at = date('Y-m-d h:i:s');
                }
                $user->save();

				$data['status'] = true;
				$data['message'] = 'Login Success';
				$data['data'][0] =  $user;
				$data['data'][0]['token'] =  $token;
				$data['data'][0]['path'] =  $path;

			}
			else
			{
				if($request->email){
					$validator = Validator::make($request->all(),[
						'email' => 'required|unique:users,email',
					]);
			
						if($validator->fails())
						{
						 $data['status'] = false;
						 $data['message']	= $validator->errors()->first();
						  return response()->json($data);
						}
				}
				if($this->check_email($request->email) > 0) {
                    $user = User::where(['email'=>$request->email,'is_deleted'=>0])->first();
                    if(!empty($request->fullname) && ($user->fullname == null || $user->fullname == '') ) {
                        $user->fullname = ucwords($request->fullname);
                    }
                    if(!empty($request->email) && ($user->email == null || $user->email == '') ) {
                        $user->email = $request->email;
                    }
                    if(!empty($request->phone_number) && ($user->phone_number == null || $user->phone_number == '') ) {
                        $user->phone_number = $request->phone_number;
                    }
                }
				else {
                    $user = new User;
                    $user->fullname	    = ucwords($request->fullname);
                    $user->email 		= $request->email ?? null;
				    $user->phone_number = $request->phone_number ?? "";

                }

				if($request->phone_number && $this->check_phone_number($request->phone_number) > 0) {
                    $user = User::where(['phone_number'=>$request->phone_number,'is_deleted'=>0])->first();
                    if(!empty($request->fullname) && ($user->fullname == null || $user->fullname == '') ) {
                        $user->fullname = ucwords($request->fullname);
                    }
                    if(!empty($request->email) && ($user->email == null || $user->email == '') ) {
                        $user->email = $request->email;
                    }
                    if(!empty($request->phone_number) && ($user->phone_number == null || $user->phone_number == '') ) {
                        $user->phone_number = $request->phone_number;
                    }
                }






				$user->device_type  = strtoupper($request->device_type) ??  "";
				$user->device_token = $request->device_token ??  "";
				$user->social_id    = $request->social_id ??  "";
				$user->social_type  = $request->social_type ??  "";
				$user->remember_token = Str::random(100);
                $user->email_verified_at = date('Y-m-d h:i:s');
                $user->save();

				$user = User::find($user->id);
				if($request->profile_pic && $user->profile_pic == 'user.jpg')
				{
					$imgurl = $request->get('profile_pic');
                    $image_name = 'user_'.$user->id.'.jpeg';
                    $user->profile_pic = 'images/user_'.$user->id.'.jpeg';
                    image_upload_social($image_name,"images",$imgurl);
	                $user->save(); // change by mahesh


	            //     $destinationPath = '/images/';
	            //     $ppath = public_path().'/'.$destinationPath;

	            //    file_put_contents($ppath.$image_name,file_get_contents($imgurl));
	            }


				$token = $user->createToken('Laravel Password Grant Client')->accessToken;

				$data['status'] = true;
				$data['message'] = 'Login Success';
				$data['data'][0] =  $user;
				$data['data'][0]['token'] =  $token;
				$data['data'][0]['path'] =  $path;
				$msg="Dear ".$user->fullname."<br>You are successfully login";
				//$this->notification($user->device_token, "Login", $msg,"login");

			} 
			// if($request->device_token) {
			// $this->check_device_token($user->id,$request->device_token,$request->device_type);
			// }
			    return response()->json($data, 200);
			}
			catch (\Throwable $e) {
				return response()->json(['status' => false, 'message' => $e->getMessage()]);
			}

    }
     public function logout(Request $request)
    {

			$user = Auth::user();
			if($user)
			{
				$request->user()->token()->revoke();
				if($request->device_token)
				$this->delete_device_token($user->id,$request->device_token);

				$data['status'] = true;
				$data['message'] = 'Logout successfully';

			    return response()->json($data, 200);
			}
			else
			{
				 $data['status']  = false;
				 $data['message'] = 'User not found';
		         return response()->json($data, $this->successStatus);
			}

    }
// Get all category
     public function categories(Request $request)
    {
		$path = url('public/images/').'/';

    	$category = Category::where(['is_deleted'=>0,'status'=>1])->orderByDesc('id')->get();
    	if($category)
    	{
    		$data['status']  = true;
			$data['message'] = 'Categories found';
			$data['path']	 = $path;
			$data['data'] = $category;

		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Categories not found';
			$data['path']	 = $path;
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }
    // Get all subcategory
     public function subcategories(Request $request)
    {
		$path = url('public/images/').'/';

    	$category = SubCategory::where(['is_deleted'=>0])->with('category')->orderByDesc('id')->get();
    	if($category)
    	{
    		$data['status']  = true;
			$data['message'] = 'Subcategories found';
			$data['path']	 = $path;
			$data['data'] = $category;

		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Subcategories not found';
			$data['path']	 = $path;
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }
// Get all channel
    public function channels(Request $request)
    {
		$path = url('public/images/').'/';

    	if($request->page != '')
			$page = $request->page * $this->api_per_page;
		else
			$page = $this->api_per_page;

		$fpage = $page - $this->api_per_page;

    	// $channels = Channel::where(['is_deleted'=>0,'status'=>1])->skip($fpage)->take($this->api_per_page)->orderByDesc('id')->get();
    	$channels = Channel::select('id','user_id','name')->where(['is_deleted'=>0,'status'=>1])->with(['playlists'=>function($q){
            $q->select('id','user_id','name','channel_id','image','thumbnail')->orderBy('id','desc')->orderBy('order_number','asc');
         }])->has('playlists')->orderBy('order_number','asc')->paginate($this->api_per_page);
    	$count = Channel::where(['is_deleted'=>0,'status'=>1])->count();

    	$total_page = ceil($count / $this->api_per_page);

    	if($channels)
    	{
    		$data['status']  = true;
			$data['message'] = 'Channels Found';
			$data['path']	 = $path;
			$data['data']	 = $channels;
			// $data['total_page']	 = $total_page;
			$i=0;
			// foreach ($channels as $key => $value) {
			// 	$data['data'][$i] = $value;
			// 	$data['data'][$i]['categories'] = $this->get_channel_category($value->id);
			// 	$i++;
			// }
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Channels Not Found';
			$data['path']	 = $path;
			$data['total_page']	 = $total_page;
			$data['data'] = '';
    	}

		    return response()->json($data, $this->successStatus);


    }
    public function channels_new(Request $request)
    {
		$path = url('public/images/').'/';

    	if($request->page != '')
			$page = $request->page * $this->api_per_page;
		else
			$page = $this->api_per_page;

		$fpage = $page - $this->api_per_page;

    	// $channels = Channel::where(['is_deleted'=>0,'status'=>1])->skip($fpage)->take($this->api_per_page)->orderByDesc('id')->get();
    	$channels = Channel::where(['is_deleted'=>0,'status'=>1])->with('playlists')->has('playlists')->whereHas('playlists',function($q){
            $q->orderBy('id','desc')->orderBy('order_number','asc');
         })->orderBy('order_number','asc')->get();
    	$count = Channel::where(['is_deleted'=>0,'status'=>1])->count();

    	$total_page = ceil($count / $this->api_per_page);

    	if($channels)
    	{
    		$data['status']  = true;
			$data['message'] = 'Channels Found';
			$data['path']	 = $path;
			$data['data']	 = $channels;
			// $data['total_page']	 = $total_page;
			$i=0;
			// foreach ($channels as $key => $value) {
			// 	$data['data'][$i] = $value;
			// 	$data['data'][$i]['categories'] = $this->get_channel_category($value->id);
			// 	$i++;
			// }
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Channels Not Found';
			$data['path']	 = $path;
			$data['total_page']	 = $total_page;
			$data['data'] = '';
    	}

		    return response()->json($data, $this->successStatus);


    }
    protected function get_channel_category($channel_id)
    {
		$category_ids = Playlist::where(['channel_id'=>$channel_id,'is_deleted'=>0,'status'=>1])->orderBy('id','desc')->orderBy('order_number','asc')->pluck('category_id')->toArray();
		$categories = Category::whereIn('id',$category_ids)->get();
		return $categories;
		// print_r($categories);

    }
    protected function get_channel_categoryIds($channel_id)
    {
		$category_ids = Playlist::where(['channel_id'=>$channel_id,'is_deleted'=>0,'status'=>1])->orderBy('id','desc')->orderBy('orer_number','asc')->pluck('category_id')->toArray();

		return $category_ids;
		// print_r($categories);

    }
    // Get all playlist
    public function playlists(Request $request)
    {
		$path = url('public/images/').'/';
		$path_songs_images = url('public/uploads/images/').'/';
		$path_songs_audios = url('public/uploads/audio/').'/';

    	$playlist = Playlist::where(['is_deleted'=>0,'status'=>1])->with('channel')->with('category')->with('songs')->orderBy('id','desc')->orderBy('order_number','asc')->get();
    	if($playlist)
    	{
    		$data['status']  = true;
			$data['message'] = 'Playlist Found';
			$data['path']	 = $path;
			$data['path_songs_images']	 = $path_songs_images;
			$data['path_songs_audios']	 = $path_songs_audios;
			$data['data'] = $playlist;
		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Playlist Not Found';
			$data['path']	 = $path;
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }
    // Get all playlist banner
    public function get_playlist_banner(Request $request,$playlist_id)
    {
		$path = url('public/images/').'/';

    	$banner = Banner::where(['is_deleted'=>0,'status'=>1,'playlist_id'=>$playlist_id])->with('playlist')->whereHas('playlists',function($q){
            $q->orderBy('id','desc')->orderBy('order_number','asc');
         })->orderByDesc('id')->get();
    	if($banner)
    	{
    		$data['status']  = true;
			$data['message'] = 'Banner Found';
			$data['path']	 = $path;
			$data['data'] = $banner;
		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Banner Not Found';
			$data['path']	 = $path;
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }
 // Get category playlist
    public function categories_playlists(Request $request,$id)
    {


		$path = url('public/images/').'/';

    	$playlist = Category::where(['is_deleted'=>0,'id'=>$id])->with('playlists')->whereHas('playlists',function($q){
            $q->orderBy('id','desc')->orderBy('order_number','asc');
         })->orderByDesc('id')->get();
    	if($playlist)
    	{
    		$data['status']  = true;
			$data['message'] = 'Category Playlist Found';
			$data['path']	 = $path;
			$data['data'] = $playlist;
		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Category Playlist Not Found';
			$data['path']	 = $path;
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }
    // Get channel playlist
    public function channels_playlists(Request $request,$id)
    {


		$path = url('public/images/').'/';

    	$channel = Channel::select('name')->where(['is_deleted'=>0,'id'=>$id,'status'=>1])->first();
    	// $playlist = Playlist::where(['channel_id'=>$id])->with('channel','category','PlaylistCategory.category','PlaylistAudio.audio')->orderBy('id','desc')->orderBy('order_number','asc')->paginate($this->api_per_page);
    	$playlist = Playlist::select('id','name','image','thumbnail')->where(['channel_id'=>$id])->orderBy('id','desc')->orderBy('order_number','asc')->paginate($this->api_per_page);
    	if($playlist)
    	{
    		$data['status']  = true;
			$data['message'] = 'Channel Playlist Found';
			$data['path']	 = $path;
			$data['channel'] = $channel;
			$data['data']    = $playlist;
		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Channel Playlist Not Found';
			$data['path']	 = $path;
			$data['channel'] = '';
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }
// Get channel category playlist
    public function channels_category_playlists(Request $request,$channel_id,$category_id)
    {
		$path = url('public/images/').'/';
		$path_songs_images = url('public/uploads/images/').'/';
		$path_songs_audios = url('public/uploads/audio/').'/';

		if($request->page != '')
			$page = $request->page * $this->api_per_page;
		else
			$page = $this->api_per_page;

		$fpage = $page - $this->api_per_page;

    	$playlistids = Playlist::where(['is_deleted'=>0,'channel_id'=>$channel_id,'category_id'=>$category_id])->orderBy('id','desc')->orderBy('order_number','asc')->pluck('id')->toArray();
    	$playlist = Song::where(['is_deleted'=>0,'playlist_id'=>$playlistids])->skip($fpage)->take($this->api_per_page)->orderByDesc('id')->get();
    	$count = Song::where(['is_deleted'=>0,'playlist_id'=>$playlistids])->count();
        $channel = Channel::find($channel_id);

    	$total_page = ceil($count / $this->api_per_page);

    	if($playlist)
    	{
    		$data['status']  = true;
			$data['message'] = 'Playlist Found';
			$data['path']	 = $path;
			$data['path_songs_images']	 = $path_songs_images;
			$data['path_songs_audios']	 = $path_songs_audios;
			$data['total_page']	 = $total_page;
			$data['channel'] = $channel;
			$data['data'] = $playlist;
		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Playlist Not Found';
			$data['path']	 = $path;
			$data['path_songs_images']	 = $path_songs_images;
			$data['path_songs_audios']	 = $path_songs_audios;
			$data['channel'] = '';
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }
// Get all banners with playlist
    public function banners(Request $request)
    {
		$path = url('public/images/').'/';

    	$banner = Banner::where(['is_deleted'=>0,'status'=>1])->with('playlist')->orderBy('order_number','asc')->get();
    	if($banner)
    	{
    		$data['status']  = true;
			$data['message'] = 'Banner Found';
			$data['path']	 = $path;
			$data['data'] = $banner;
		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Banner Not Found';
			$data['path']	 = $path;
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }

 public function channels_categories($channel_id)
 {
	 $path = url('public/images/').'/';

     $category_ids = $this->get_channel_categoryIds($channel_id);
     $channel = Channel::find($channel_id);
     $category = Category::where(['is_deleted'=>0,'status'=>1])->whereIn('id',$category_ids)->orderByDesc('id')->paginate($this->api_per_page);
    	if($category)
    	{
    		$data['status']  = true;
			$data['message'] = 'Channel category Found';
			$data['path']	 = $path;
			$data['channel'] = $channel;
			$data['data'] = $category;
		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Channel category Not Found';
			$data['path']	 = $path;
			$data['channel'] = '';
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}

 }
 public function playlist_audios($playlist_id)
 {
	 $path = url('public/images/').'/';


     $audios = Playlist::where(['is_deleted'=>0,'status'=>1,'id'=>$playlist_id])->with('PlaylistAudio.audio','channel','PlaylistCategory.category')->orderBy('id','desc')->orderBy('order_number','asc')->paginate($this->api_per_page);
    	if($audios)
    	{
    		$data['status']  = true;
			$data['message'] = 'Playlist audio found';
			$data['path']	 = $path;
			$data['data'] = $audios;
		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Playlist audio not found';
			$data['path']	 = $path;
			$data['data'] 	 = '';
		    return response()->json($data, $this->successStatus);
    	}

 }
 public function playlist_audios_new($playlist_id)
 {
	 $path = url('public/images/').'/';


     $playlist = Playlist::where(['is_deleted'=>0,'status'=>1,'id'=>$playlist_id])->first();
     // $audioIds = PlaylistAudio::select('audio_id')->where(['playlist_id'=>$playlist_id])->pluck('audio_id')->toArray();
     // $audios = Audio::where(['is_deleted'=>0,'status'=>1])->whereIn('id',$audioIds)->orderBy('order_number','asc')->paginate(20);
	 $max_order_number = PlaylistAudio::where(['playlist_id'=>$playlist->id])->max('order_number');

	 $audios = PlaylistAudio::where(['playlist_id'=>$playlist->id])->with([
		'audio.AudioCategory' => function ($query) {
			$query->select('category_id','audio_id');
		},
		'audio.AudioCategory.category' => function ($query) {
			$query->select('id','name');
		}
	 ])
	 ->whereHas('audio',function($q){
		 $q->orderBy('id','desc')->orderBy('order_number','asc');
	  });
	  if($max_order_number == 0) {
		 $audios =  $audios->orderBy('id','desc')->orderBy('order_number','asc')->paginate(20);
	  }
	  else {
		 $audios =  $audios->orderBy('order_number','asc')->paginate(20);
	  }

    	if($audios)
    	{
    		$data['status']  = true;
			$data['message'] = 'Playlist audio found';
			$data['path']	 = $path;
			$data['playlist'] = $playlist;
			$data['data'] = $audios;
		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Playlist audio not found';
			$data['path']	 = $path;
			$data['data'] 	 = '';
		    return response()->json($data, $this->successStatus);
    	}

 }
 public function ambiance_songs()
 {
	 $path = url('public/images/').'/';


     $audios = Ambiance::where(['is_deleted'=>0,'status'=>1])->orderByDesc('id')->paginate($this->api_per_page);
    	if($audios)
    	{
    		$data['status']  = true;
			$data['message'] = 'Ambiance audio found';
			$data['path']	 = $path;
			$data['data'] = $audios;
		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Ambiance audio not found';
			$data['path']	 = $path;
			$data['data'] 	 = '';
		    return response()->json($data, $this->successStatus);
    	}

 }

public function mystory_add(Request $request)
{
	 $path = url('public/images/').'/';

    $validator = Validator::make($request->all(),[
			'content' => 'required',
			'category_id' => 'required|numeric|min:1',

		]);

	if($validator->fails())
	{
        $data['status'] = false;
		$data['message']	= $validator->errors()->first();
        return response()->json($data);
	}
	$user = Auth::user();
	$mystory = new Mystory;
	$mystory->user_id = $user->id;
	$mystory->category_id = $request->category_id;
	$mystory->content = $request->content;
	// $mystory->genre = $request->genre;
	if($mystory->save())
	{
    	$data['status']  = true;
		$data['message'] = 'My story added successfully';
		$data['path']	 = $path;
		$data['data'] = $mystory;
		return response()->json($data, $this->successStatus);
   	}
    else
    {
    	$data['status']  = false;
		$data['message'] = 'My story not added';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }

}
public function mystory(Request $request)
{
	 $path = url('public/images/').'/';


	$user = Auth::user();
	$mystory = Mystory::where(['is_deleted'=>0,'user_id'=>$user->id])->with('category')->orderBy('id','DESC')->paginate($this->api_per_page);
	if($mystory)
	{
    	$data['status']  = true;
		$data['message'] = 'My story list';
		$data['path']	 = $path;
		$data['data'] = $mystory;
		return response()->json($data, $this->successStatus);
   	}
    else
    {
    	$data['status']  = false;
		$data['message'] = 'My story not found';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }

}

public function mystory_show(Request $request,$id)
{
	 $path = url('public/images/').'/';


	$user = Auth::user();
	$mystory = Mystory::where(['id'=>$id])->with('category')->first();
	if($mystory)
	{
    	$data['status']  = true;
		$data['message'] = 'My story details';
		$data['path']	 = $path;
		$data['data'] = $mystory;
		return response()->json($data, $this->successStatus);
   	}
    else
    {
    	$data['status']  = false;
		$data['message'] = 'My story not found';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }

}
public function mystory_update(Request $request,$id)
{
	 $path = url('public/images/').'/';

    $validator = Validator::make($request->all(),[
			'content' => 'required',
			'category_id' => 'required|numeric|min:1',

		]);

	if($validator->fails())
	{
        $data['status'] = false;
		$data['message']	= $validator->errors()->first();
        return response()->json($data);
	}
	$user = Auth::user();
	$mystory = Mystory::where(['id'=>$id])->first();
	$mystory->user_id = $user->id;
	$mystory->category_id = $request->category_id;
	$mystory->content = $request->content;
	// $mystory->genre = $request->genre;
	if($mystory->save())
	{
    	$data['status']  = true;
		$data['message'] = 'My story updated successfully';
		$data['path']	 = $path;
		$data['data'] = $mystory;
		return response()->json($data, $this->successStatus);
   	}
    else
    {
    	$data['status']  = false;
		$data['message'] = 'My story not updated';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }

}
public function mystory_delete(Request $request,$id)
{
	 $path = url('public/images/').'/';


	$user = Auth::user();
	$mystory = Mystory::where(['id'=>$id])->first();
	$mystory->is_deleted = 1;
	if($mystory->save())
	{
    	$data['status']  = true;
		$data['message'] = 'My story deleted successfully';
		$data['path']	 = $path;
		$data['data'] = $mystory;
		return response()->json($data, $this->successStatus);
   	}
    else
    {
    	$data['status']  = false;
		$data['message'] = 'My story not deleted';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }

}
public function audio_details($search)
{

    $authors = Audio::select(DB::raw('distinct(audio.author)'))->where(['is_deleted'=>0])
            ->when($search,function($q,$search){
                return $q->where(function($query) use ($search){
                $query->orWhere('audio.author','like','%'.$search.'%');
            });
            })
            ->orderBy('author','asc')->get();
    $narrators = Audio::select(DB::raw('distinct(audio.narrator)'))->where(['is_deleted'=>0])
            ->when($search,function($q,$search){
                return $q->where(function($query) use ($search){
                $query->orWhere('audio.narrator','like','%'.$search.'%');
            });
            })
            ->orderBy('author','asc')->get();
    $categories = Audio::select(DB::raw('distinct(cat.name)'))->where(['cat.is_deleted'=>0])
                ->leftjoin("audio_categories as au_cat",function($join){
                    $join->on("au_cat.audio_id",'=','audio.id');
                })
                ->leftjoin("categories as cat",function($join){
                    $join->on("cat.id",'=','au_cat.category_id');
                })
                ->when($search,function($q,$search){
                    return $q->where(function($query) use ($search){
                $query->orWhere('cat.name','like','%'.$search.'%');
                });
                })
                ->orderBy('cat.name','asc')->get();
    $data['authors'] = $authors;
    $data['narrators'] = $narrators;
    $data['categories'] = $categories;
    return $data;

}
public function audios(Request $request)
 {
     $user = Auth::user();

	 $path = url('public/images/').'/';
     $playlist = trim($request->playlist);
     $search = trim($request->search);
     $search = str_replace("’","'",$request->search);
     $author = ($request->author);
     $narrator = ($request->narrator);
     $genre = ($request->genre);
     $favorite = $request->favorite;
     if(isset($request->length))
     {
        $length = trim($request->length);
        $length = explode("-",$length);
        $length_from = $length[0] ?? 0;
        $length_to = $length[1] ?? 0;

     }
     else
     {
        $length = "";
        $length_from = "";
        $length_to = "";

     }
     $sort_title = $request->sort_title;
     $sort_lenght = $request->sort_lenght;
     $sort_date = $request->sort_date;
     $sort_author = $request->sort_author;
     $sort_narrator = $request->sort_narrator;
     $sort_category = $request->sort_category;

    //  echo "<pre>";
    //  echo $length_from;
    //  echo $length_to;
    //  print_r($length);
    //  exit;
    if(isset($request->from_date))
     $from_date = date("Y-m-d",strtotime(trim($request->from_date)));
    else
    $from_date = "";

    if(isset($request->to_date))
     $to_date =  date("Y-m-d",strtotime(trim($request->to_date)));
     else
     $to_date =  "";



	 if(isset($request->playlist) && !empty($request->playlist)){
         $alreadyExist = MyplaylistAudio::where('myplaylist_id',$request->playlist)->pluck('audio_id')->toArray();
          if(count($alreadyExist) > 0) {
        	 $alreadyExist = $alreadyExist;
        }
        else {
        	 $alreadyExist = '';
        }
     }
     else {
        	 $alreadyExist = '';
        }
     if(isset($request->favorite) && !empty($request->favorite)){
        $favoriteIds = Favourite::where('user_id',$user->id)->pluck('audio_id')->toArray();
        if(count($favoriteIds) > 0) {
        	 $favoriteIds = $favoriteIds;
        }
        else {
        	 $favoriteIds = '';
        }
    }
    else {
        	$favoriteIds = '';
    }

    // echo "check";
    // echo(count($request->all()));
    // exit;
     if( count($request->all()) > 0)
     {
		// echo "true";
		// echo $length;exit;
	 	$audios = Audio::select(DB::raw("DISTINCT(audio.id)"),'audio.*')->where(['audio.is_deleted'=>0,'audio.status'=>1])
	 	->leftjoin("audio_categories as au_cat",function($join){
                $join->on("au_cat.audio_id",'=','audio.id');
            })
            ->leftjoin("categories as cat",function($join){
                $join->on("cat.id",'=','au_cat.category_id');
            })
	 	->when($search,function($q,$search){
	 		  return $q->where(function($query) use ($search){
                $query->orWhere('audio.title','like','%'.$search.'%');
                $query->orWhere('audio.tags','like','%'.$search.'%');
                $query->orWhere('audio.author','like','%'.$search.'%');
                $query->orWhere('audio.narrator','like','%'.$search.'%');
                $query->orWhere('cat.name','like','%'.$search.'%');
                $query->orWhere('audio.created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
			});
         })
         ->when($alreadyExist,function($q,$alreadyExist){
             return $q->whereNotIn('audio.id',$alreadyExist);
         })
         ->when($favoriteIds,function($q,$favoriteIds){
            return $q->whereIn('audio.id',$favoriteIds);
        })
         ->when($author,function($q,$author){
            return $q->where(function($query) use ($author) {
                foreach($author as $row) {
                    $query->orWhere('audio.author','like','%'.$row.'%');
                }
            });
         })
         ->when($narrator,function($q,$narrator){
            return $q->where(function($query) use ($narrator) {
                foreach($narrator as $row) {
                    $query->orWhere('audio.narrator','like','%'.$row.'%');
                }
            });
         })
         ->when($genre,function($q,$genre){
            return $q->where(function($query) use ($genre) {
                foreach($genre as $row) {
                    $query->orWhere('cat.name','like','%'.$row.'%');
                }
            });
         })
         ->when($length,function($q) use($length_from,$length_to){
            return $q->whereBetween('audio.duration',[$length_from,$length_to]);
         })
         ->when($from_date,function($q) use($from_date,$to_date){
            return $q->whereBetween('audio.created_at',[$from_date,$to_date]);
         })
         ->when($sort_title,function($q) use($sort_title) {
            return $q->orderBy(DB::raw("fnStripTags(audio.title)"),$sort_title);
        })
        ->when($sort_lenght,function($q) use($sort_lenght) {
            return $q->orderBy('audio.duration',$sort_lenght);
        })
        ->when($sort_date,function($q) use($sort_date) {
            return $q->orderBy('audio.created_at',$sort_date);
        })
        ->when($sort_author,function($q) use($sort_author) {
            return $q->orderBy(DB::raw("fnStripTags(audio.author)"),$sort_author);
        })
        ->when($sort_narrator,function($q) use($sort_narrator) {
            return $q->orderBy(DB::raw("fnStripTags(audio.narrator)"),$sort_narrator);
        })
        ->when($sort_category,function($q) use($sort_category) {
            return $q->orderBy('cat.name',$sort_category);
        })
		->with('AudioCategory.category')
	 	 ->orderBy('audio.id','desc')
	 	 ->orderBy('audio.order_number','asc')
        ->paginate($this->api_per_page);
     }
     else
     {

	 	$audios = Audio::select(DB::raw("DISTINCT(audio.id)"),'audio.*')->where(['audio.is_deleted'=>0,'audio.status'=>1])
	 	->leftjoin("audio_categories as au_cat",function($join){
                $join->on("au_cat.audio_id",'=','audio.id');
            })
            ->leftjoin("categories as cat",function($join){
                $join->on("cat.id",'=','au_cat.category_id');
            })
		->with('AudioCategory.category')
		->orderBy('audio.id','desc')
	 	->orderBy('audio.order_number','asc')
        ->paginate($this->api_per_page);
	 }


    	if($audios)
    	{
            // $audiodetails = $this->audio_details($search);
    		$data['status']  = true;
			$data['message'] = 'Audio list';
			$data['path']	 = $path;
			// $data['authors'] = $audiodetails['authors'];
			// $data['narrators'] = $audiodetails['narrators'];
			// $data['categories'] = $audiodetails['categories'];
			$data['data'] = $audios;
		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{

    		$data['status']  = false;
			$data['message'] = 'Audio not found';
			$data['path']	 = $path;
			$data['data'] 	 = '';
		    return response()->json($data, $this->successStatus);
    	}


 }

 public function myplaylist_add(Request $request)
{
	 $path = url('public/images/').'/';

    $validator = Validator::make($request->all(),[
			'title' => 'required|max:255',
		]);

	if($validator->fails())
	{
        $data['status'] = false;
		$data['message']	= $validator->errors()->first();
        return response()->json($data);
	}
	$user = Auth::user();
	$myplaylist = new Myplaylist;
	$myplaylist->user_id = $user->id;
	$myplaylist->title = ucfirst($request->title);
	if($myplaylist->save())
	{
    	$data['status']  = true;
		$data['message'] = 'My Playlist added successfully';
		$data['path']	 = $path;
		$data['data'] = $myplaylist;
		return response()->json($data, $this->successStatus);
   	}
    else
    {
    	$data['status']  = false;
		$data['message'] = 'My Playlist not added';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }

}
public function myplaylist(Request $request)
{
	 $path = url('public/images/').'/';


	$user = Auth::user();
	$myplaylist = Myplaylist::where(['is_deleted'=>0,'user_id'=>$user->id])
			                ->with('MyplaylistAudio.audio.AudioCategory.category')
			                /*->whereHas('MyplaylistAudio.audio',function($q){
						        $q->orderBy('id','desc')->orderBy('order_number','asc');
						     })*/
			                ->withCount('MyplaylistAudio')
			                ->paginate($this->api_per_page);


    /*$queryString = Myplaylist::where(['is_deleted'=>0,'user_id'=>$user->id])
                ->with('MyplaylistAudio.audio.AudioCategory.category')
                ->whereHas('MyplaylistAudio.audio',function($q){
			        $q->orderBy('id','desc')->orderBy('order_number','asc');
			     })
                ->withCount('MyplaylistAudio')->toSql();

    Log::info('PlayList Sql : '.$queryString);
    Log::info('My playing list: '.json_encode($myplaylist));
    Log::info('User Id : '.$user->id);*/
	if($myplaylist)
	{
    	$data['status']  = true;
		$data['message'] = 'My Playlist list';
		$data['path']	 = $path;
		$data['data'] = $myplaylist;
		return response()->json($data, $this->successStatus);
   	}
    else
    {
    	$data['status']  = false;
		$data['message'] = 'My Playlist not found';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }

}

public function myplaylist_show(Request $request,$id)
{
	 $path = url('public/images/').'/';


	$user = Auth::user();
	$myplaylist = Myplaylist::where(['id'=>$id])->with('MyplaylistAudio.audio.AudioCategory.category')
	->whereHas('MyplaylistAudio.audio',function($q){
        $q->orderBy('id','desc')->orderBy('order_number','asc');
     })->withCount('MyplaylistAudio')->first();
	if($myplaylist)
	{
    	$data['status']  = true;
		$data['message'] = 'My Playlist details';
		$data['path']	 = $path;
		$data['data'] = $myplaylist;
		return response()->json($data, $this->successStatus);
   	}
    else
    {
    	$data['status']  = false;
		$data['message'] = 'My Playlist not found';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }

}
public function myplaylist_update(Request $request,$id)
{
	 $path = url('public/images/').'/';

    $validator = Validator::make($request->all(),[
			'title' => 'required|max:255',
		]);

	if($validator->fails())
	{
        $data['status'] = false;
		$data['message']	= $validator->errors()->first();
        return response()->json($data);
	}
	$user = Auth::user();
	$myplaylist = Myplaylist::where(['id'=>$id])->first();
	$myplaylist->user_id = $user->id;
	$myplaylist->title = ucfirst($request->title);
	if($myplaylist->save())
	{
    	$data['status']  = true;
		$data['message'] = 'My Playlist updated successfully';
		$data['path']	 = $path;
		$data['data'] = $myplaylist;
		return response()->json($data, $this->successStatus);
   	}
    else
    {
    	$data['status']  = false;
		$data['message'] = 'Myplaylist not updated';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }

}
public function myplaylist_delete(Request $request,$id)
{
	 $path = url('public/images/').'/';


	$user = Auth::user();
	$myplaylist = Myplaylist::where(['id'=>$id])->first();
	$myplaylist->is_deleted = 1;
	if($myplaylist->save())
	{
    	$data['status']  = true;
		$data['message'] = 'My Playlist deleted successfully';
		$data['path']	 = $path;
		$data['data'] = $myplaylist;
		return response()->json($data, $this->successStatus);
   	}
    else
    {
    	$data['status']  = false;
		$data['message'] = 'My Playlist not deleted';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }

}
public function checkMyPlaylistSong($user_id,$audio_id,$myplaylist_id) {
	return MyplaylistAudio::where(['user_id'=>$user_id,'audio_id'=>$audio_id,'myplaylist_id'=>$myplaylist_id])->count();

}
public function myplaylist_audio_add(Request $request,$myplaylist_id)
{
	 $path = url('public/images/').'/';
// echo "<pre>";
// print_r(json_decode($request->audio_ids));exit;
    $validator = Validator::make($request->all(),[
			'audio_ids' => 'required',
		]);

	if($validator->fails())
	{
        $data['status'] = false;
		$data['message']	= $validator->errors()->first();
        return response()->json($data);
	}
	$user = Auth::user();
	$array = [];
	foreach (json_decode($request->audio_ids) as $key => $value) {
		if($this->checkMyPlaylistSong($user->id,$request->audio_id,$request->myplaylist_id) == 0) {
			$myplaylist = new MyplaylistAudio;
			$myplaylist->user_id = $user->id;
			$myplaylist->myplaylist_id = $request->myplaylist_id;
			$myplaylist->audio_id = $value->audio_id;
			$myplaylist->save();
			$array[] = $myplaylist;
		}
	}

    	$data['status']  = true;
		$data['message'] = 'My Playlist audio added successfully';
		$data['path']	 = $path;
		$data['data'] 	 = $array;
		return response()->json($data, $this->successStatus);
}
public function myplaylist_audio_delete(Request $request,$id)
{
	 $path = url('public/images/').'/';
// echo "<pre>";
// print_r(json_decode($request->audio_ids));exit;
    $validator = Validator::make($request->all(),[
			'audio_ids' => 'required',
		]);

	if($validator->fails())
	{
        $data['status'] = false;
		$data['message']	= $validator->errors()->first();
        return response()->json($data);
	}
	$user = Auth::user();
	foreach (json_decode($request->audio_ids) as $key => $value) {
		$myplaylist = MyplaylistAudio::where('id',$value->audio_id)->delete();
	}

    	$data['status']  = true;
		$data['message'] = 'My Playlist audio deleted successfully';
		$data['path']	 = $path;
		return response()->json($data, $this->successStatus);
}

 public function makeFavorite(Request $request){

 	 $validator = Validator::make($request->all(),[
			'audio_id' => 'required',
		]);

	if($validator->fails())
	{
        $data['status'] = false;
		$data['message']	= $validator->errors()->first();
        return response()->json($data);
	}
    	$userId = Auth::user()->id;
    	$audio_id = $request->audio_id;
    	$favourite = Favourite::where('user_id',$userId)
    					->where('audio_id',$audio_id)->first();
    	if($favourite){
    		$favourite->delete();
    		$message = "Audio removed as favourite.";
    	}
    	else{
    		$favourite = new Favourite;
    		$favourite->user_id = $userId;
    		$favourite->audio_id = $audio_id;
    		$favourite->save();

    		$message = "Audio marked as favourite.";
    	}




    	$data['status']  = true;
		$data['message'] = $message;
		// $data['path']	 = $path;
		return response()->json($data, $this->successStatus);
    }
public function subscriptionplans(Request $request)
{
	 $path = url('public/images/').'/';


	$user = Auth::user();
	$myplaylist = Subscriptionplan::where(['is_deleted'=>0,'status'=>1])->orderBy('order_number','asc')->paginate($this->api_per_page);
	if($myplaylist)
	{
    	$data['status']  = true;
		$data['message'] = 'Subscriptionplan list';
		$data['path']	 = $path;
		$data['data'] = $myplaylist;
		return response()->json($data, $this->successStatus);
   	}
    else
    {
    	$data['status']  = false;
		$data['message'] = 'Subscriptionplan not found';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }

}
public function privacy_policy(Request $request)
{
	 $path = url('public/images/').'/';


	$user = Auth::user();
	$cms = Cms::where(['is_deleted'=>0,'slug'=>'privacy-policy'])->first();
	if($cms)
	{
    	$data['status']  = true;
		$data['message'] = 'Privacy policy found';
		$data['path']	 = $path;
		$data['data'] = $cms;
		return response()->json($data, $this->successStatus);
   	}
    else
    {
    	$data['status']  = false;
		$data['message'] = 'Privacy policy not found';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }

}
public function about_us(Request $request)
{
	 $path = url('public/images/').'/';


	$user = Auth::user();
	$cms = Cms::where(['is_deleted'=>0,'slug'=>'about-us'])->first();
	if($cms)
	{
    	$data['status']  = true;
		$data['message'] = 'About us found';
		$data['path']	 = $path;
		$data['data'] = $cms;
		return response()->json($data, $this->successStatus);
   	}
    else
    {
    	$data['status']  = false;
		$data['message'] = 'About us not found';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }

}
public function terms_and_conditions(Request $request)
{
	 $path = url('public/images/').'/';


	$user = Auth::user();
	$cms = Cms::where(['is_deleted'=>0,'slug'=>'terms-and-conditions'])->first();
	if($cms)
	{
    	$data['status']  = true;
		$data['message'] = 'Terms and conditions found';
		$data['path']	 = $path;
		$data['data'] = $cms;
		return response()->json($data, $this->successStatus);
   	}
    else
    {
    	$data['status']  = false;
		$data['message'] = 'Terms and conditions not found';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }

}
public function notification_enable(Request $request)
{
    $validator = Validator::make($request->all(), [
        'notification' => 'required|numeric|min:0|max:1',
        ]);
    if ($validator->fails()) {
        return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
     }
	$user = Auth::user();

    if($user)
    {
        $user = User::find($user->id);
        $user->notification = $request->notification;
        $user->save();

        if($request->notification==1)
        $type = "enable";
        else $type = "disable";

        $response['status'] = true;
        $response['message'] = "Notification $type";
        $response['data'] = $user;
        return response()->json($response);
    }

}
public function check_receipt($receipt)
{
	return UserSubscription::where(['receipt'=>$receipt])->count();
}
public function check_transaction_id($transaction_id)
{
	return UserSubscription::where(['transaction_id'=>$transaction_id])->count();
}
public function check_user_transaction_id($user_id,$transaction_id)
{
	return UserSubscription::where(['user_id'=>$user_id,'transaction_id'=>$transaction_id])->count();
}
public function userSubscription(Request $request)
{
	 $path = url('public/images/').'/';
     $user = Auth::user();

    $validator = Validator::make($request->all(), [
        'plan_id' => 'required|max:255',
        'amount' => 'required',
        'transaction_id' => 'required',
        'currency_name' => 'required',
        ]);
    if ($validator->fails()) {
        return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
     }
    $from_date =  date("Y-m-d");
    $to_date =  date("Y-m-d",strtotime("+30 days",strtotime($from_date)));
    $usernew = User::find($user->id);
	if($request->receipt && $this->check_receipt($request->receipt) > 0)
	{
		$data['status'] 				= true;
		$data['message']				= "Receipt already exists.";
		$data['data']					= '';
		return response()->json($data, $this->successStatus);
		exit;
	}
	if($this->check_transaction_id($request->transaction_id) > 0)
	{
		if($this->check_user_transaction_id($user->id,$request->transaction_id) > 0)
		{
			$user_subscription = UserSubscription::where(['user_id'=>$user->id,'transaction_id'=>$request->transaction_id])->first();
			$data['status']  = true;
			$data['message'] = 'Subscription plan already added';
			$data['path']	 = $path;
			$data['data'] = $user_subscription;
			return response()->json($data, $this->successStatus);
		}
		else
		{
			$data['status']  = false;
			$data['message'] = 'Transaction id is already exist.';
			$data['path']	 = $path;
			$data['data'] 	 = '';
			return response()->json($data, $this->successStatus);
			// $user_subscription = new UserSubscription;
			// $user_subscription->user_id = $user->id;
			// $user_subscription->plan_id = $request->plan_id;
			// $user_subscription->amount = $request->amount;
			// $user_subscription->transaction_id = $request->transaction_id;
			// $user_subscription->currency_name = $request->currency_name ?? "";
			// $user_subscription->from_date = $from_date;
			// if($usernew->subscription_plan_last_date && ($usernew->subscription_plan_last_date >= $from_date))
			// $user_subscription->to_date = date("Y-m-d",strtotime("+30 days",strtotime($to_date)));
			// else
			// $user_subscription->to_date = $to_date;
			// $user_subscription->app_res = $request->app_res ?? "";
			// $user_subscription->receipt = $request->receipt ?? "";
		}
	}
	else
	{
		$user_subscription = new UserSubscription;
		$user_subscription->user_id = $user->id;
		$user_subscription->plan_id = $request->plan_id;
		$user_subscription->amount = $request->amount;
		$user_subscription->transaction_id = $request->transaction_id;
		$user_subscription->currency_name = $request->currency_name ?? "";
		$user_subscription->from_date = $from_date;
		if($usernew->subscription_plan_last_date && ($usernew->subscription_plan_last_date >= $from_date))
		$user_subscription->to_date = date("Y-m-d",strtotime("+30 days",strtotime($to_date)));
		else
		$user_subscription->to_date = $to_date;
		$user_subscription->app_res = $request->app_res ?? "";
		$user_subscription->receipt = $request->receipt ?? "";
	}


    if($user_subscription->save())
    {

        if($usernew->subscription_plan_last_date && ($usernew->subscription_plan_last_date >= $from_date))
        $usernew->subscription_plan_last_date = date("Y-m-d",strtotime("+30 days",strtotime($to_date)));
        else
        $usernew->subscription_plan_last_date = $to_date;
        $usernew->save();

        $data['status']  = true;
		$data['message'] = 'Subscription plan added';
		$data['path']	 = $path;
		$data['data'] = $user_subscription;
		return response()->json($data, $this->successStatus);
    }
    else
    {
    	$data['status']  = false;
		$data['message'] = 'Subscription plan not added';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }
}
public function notifications(Request $request)
{
	 $path = url('public/images/').'/';


	$user = Auth::user();

	$notifications = Notification::where(['is_deleted'=>0,'user_id'=>$user->id])->whereRaw("created_at > curdate() - interval 7 day")->orderBy('id','desc')->paginate($this->api_per_page);
	if($notifications)
	{

    	$data['status']  = true;
		$data['message'] = 'Notification list';
		$data['path']	 = $path;
		$data['data'] = $notifications;
        $data['total_unread'] = $this->totalUnreadNotification($user->id);
        if($request->read_unread && $request->read_unread == 1) {
		$this->notificationAllRead($user->id);
        }

        return response()->json($data, $this->successStatus);
   	}
    else
    {
    	$data['status']  = false;
		$data['message'] = 'Notifications not found';
		$data['path']	 = $path;
		$data['data'] 	 = '';
        $data['total_unread'] = '';
        return response()->json($data, $this->successStatus);
    }

}
public function totalUnreadNotification($user_id) {
    return Notification::where(['is_deleted'=>0,'user_id'=>$user_id,'read_unread'=>0])->whereRaw("created_at > curdate() - interval 7 day")->count() ?? 0;
}
public function notification_read(Request $request)
{
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'notification_ids' => 'required',
        ]);

        if($validator->fails())
		{
            $data['status'] = false;
			$data['message']	= $validator->errors()->first();
            return response()->json($data);
        }
        // $notification_ids = explode(",",$request->notification_ids);
           $notification_ids = json_decode($request->notification_ids);

        $notification = Notification::whereIn('id',$notification_ids)->update(['read_unread'=>1]);
        if($notification) {
            $data['status'] 				= true;
			$data['message']				= "Notifications read successfully.";
			$data['data']					= $notification;
            $data['total_unread'] = $this->totalUnreadNotification($user->id);
        }
        else {
            $data['status'] 	= false;
			$data['message']	= "Notification not found.";
			$data['data']	    = "";
            $data['total_unread'] = 0;
        }
		return response()->json($data,$this->successStatus);

}
public function userSubscriptionList(Request $request)
{
	 $path = url('public/images/').'/';

    $user = Auth::user();
    $user_subscription = UserSubscription::where(['user_id'=>$user->id])->latest()->first();
    if($user_subscription)
    {

        $data['status']  = true;
		$data['message'] = 'Subscription plan available';
		$data['path']	 = $path;
		$data['data'] = $user_subscription;
		return response()->json($data, $this->successStatus);
    }
    else
    {
    	$data['status']  = false;
		$data['message'] = 'Subscription plan not available';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }
}

public function viewCount($id,$type)
{
    if($type=='audio')
    {
        $audio = Audio::find($id);
        $audio->view_count = $audio->viewCount() +1;
        $audio->save();

    }
    else if($type=='playlist')
    {
        $playlist = Playlist::find($id);
        $playlist->view_count = $playlist->viewCount() +1;
        $playlist->save();

    }
    else if($type=='story')
    {
        $mystory = Mystory::find($id);
        $mystory->view_count = $mystory->viewCount() +1;
        $mystory->save();

    }
    else if($type=='channel')
    {
        $channel = Channel::find($id);
        $channel->view_count = $channel->viewCount() +1;
        $channel->save();

    }


        $data['status']  = true;
		$data['message'] = 'Count added';
        return response()->json($data, $this->successStatus);

}
public function favourites(Request $request)
{
	$path = url('public/images/').'/';

    $user = Auth::user();
    $favourites = Favourite::where(['user_id'=>$user->id])->with('audio.AudioCategory.category')->whereHas('audio',function($q){
        $q->orderBy('id','desc')->orderBy('order_number','asc');
     })->paginate($this->api_per_page);
    if($favourites)
    {

        $data['status']  = true;
		$data['message'] = 'Favourites audio list';
		$data['path']	 = $path;
		$data['data'] = $favourites;
		return response()->json($data, $this->successStatus);
    }
    else
    {
    	$data['status']  = false;
		$data['message'] = 'Favourites audio not found';
		$data['path']	 = $path;
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }
}
public function login_register_banner(Request $request)
{
    $user = Auth::user();
    $banner = AdminSetting::where(['id'=>1])->first();
    if($banner)
    {

        $data['status']  = true;
		$data['message'] = 'Login regiser banner';
		$data['data'] = $banner;
		return response()->json($data, $this->successStatus);
    }
    else
    {
    	$data['status']  = false;
		$data['message'] = 'Login regiser banner not found';
		$data['data'] 	 = '';
		return response()->json($data, $this->successStatus);
    }
}
public function audio_search_content()
{

    $authors = Audio::select(DB::raw('distinct(fnStripTags(audio.author)) as author'))->where(['is_deleted'=>0])
            ->orderBy(DB::raw("fnStripTags(audio.author)"),'asc')->get();
    $narrators = Audio::select(DB::raw('distinct(fnStripTags(audio.narrator)) as narrator'))->where(['is_deleted'=>0])
            ->orderBy(DB::raw("fnStripTags(audio.narrator)"),'asc')->get();
    $categories = Category::select(DB::raw('distinct(name)'))->where(['is_deleted'=>0])
                ->orderBy('name','asc')->get();
    $data['authors'] = $authors;
    $data['narrators'] = $narrators;
    $data['categories'] = $categories;
    return $data;

}
public function search_content(Request $request,$type)
{
    if($type)
    {
        $audio_search_content = $this->audio_search_content();
        $data['status']  = true;
		$data['message'] = 'Search content list';
        if($type == 'authors') {
            $data['authors'] = $audio_search_content['authors'];
        }
       else if($type == 'narrators') {
            $data['narrators'] = $audio_search_content['narrators'];
        }
       else if($type == 'categories') {
            $data['categories'] = $audio_search_content['categories'];
        }
		return response()->json($data, $this->successStatus);
    }

}
public function notificationAllRead($user_id)
{
    Notification::where(['user_id'=>$user_id])->update(['read_unread'=>1]);
}
public function app_version(Request $request)
{
    $user = Auth::user();

    $appversion = Appversion::find(1);
    if($appversion)
    {
        // $this->notificationAllRead($user->id);
        $data['status']  = true;
		$data['message'] = 'Appversion list';
        $data['data'] = $appversion;

		return response()->json($data, $this->successStatus);
    }

}
public function app_version_without_auth(Request $request)
{
    $appversion = Appversion::find(1);
    if($appversion)
    {
        $data['status']  = true;
		$data['message'] = 'Appversion list';
        $data['data'] = $appversion;

		return response()->json($data, $this->successStatus);
    }

}

public function subscriptionCheck(Request $request) {
    $user = Auth::user();
    $subscription = SubscriptionCheck::where('user_id',$user->id)->first();
    if($subscription) {
        $subscription = $subscription;
    }
    else {
        $subscription = new SubscriptionCheck;
    }
    $subscription->user_id = $user->id;
    $subscription->plan_date = date('Y-m-d');
    if($subscription->save()) {
        $data['status'] 				= true;
		$data['message']				= "Subscription added successfully.";
		$data['data']					= $subscription;
    }
    else {
        $data['status'] 	= false;
		$data['message']	= "Subscription not added.";
		$data['data']	    = "";
        $data['total_unread'] = 0;
    }
		return response()->json($data,$this->successStatus);
}
public function receiptSave(Request $request) {
    $user = Auth::user();
	$validator = Validator::make($request->all(), [
            'receipt' => 'required',
        ]);

        if($validator->fails())
		{
            $data['status'] = false;
			$data['message']	= $validator->errors()->first();
            return response()->json($data);
        }
	if($request->receipt && $this->check_receipt($request->receipt) > 0)
	{
		$data['status'] 				= true;
		$data['message']				= "Receipt already exists.";
		$data['data']					= '';
		return response()->json($data, $this->successStatus);
		exit;
	}
	$subscription = UserSubscription::where('user_id',$user->id)->latest()->first();

    if($request->transaction_id && $this->check_transaction_id($request->transaction_id) > 0)
	{
		if($this->check_user_transaction_id($user->id,$request->transaction_id) > 0)
		{
			$subscription->receipt = $request->receipt;
        	$subscription->transaction_id = $request->transaction_id;
	        $subscription->save();
		    $data['status'] 				= true;
			$data['message']				= "Subscription added successfully.";
			$data['data']					= $subscription;
			return response()->json($data, $this->successStatus);

		}
		else
		{
			$data['status']  = true;
			$data['message'] = 'Transaction id is already exist.';
			$data['data'] 	 = '';
			return response()->json($data, $this->successStatus);

		}
	}
    if($subscription) {
        	$subscription->receipt = $request->receipt;
	        if($subscription->save()) {
		        $data['status'] 				= true;
				$data['message']				= "Subscription added successfully.";
				$data['data']					= $subscription;
		    }
		    else {
		        $data['status'] 	= true;
				$data['message']	= "Subscription not added.";
				$data['data']	    = "";
		    }
    }
    else {
		$from_date =  date("Y-m-d");
    	$to_date =  date("Y-m-d",strtotime("+30 days",strtotime($from_date)));

		$user_subscription = new UserSubscription;
		$user_subscription->user_id = $user->id;
		$user_subscription->plan_id = $request->plan_id ?? "";
		$user_subscription->amount = $request->amount ?? 0;
		$user_subscription->transaction_id = $request->transaction_id ?? "";
		$user_subscription->currency_name = $request->currency_name ?? "";
		$user_subscription->from_date = $from_date ?? "";
		$user_subscription->to_date = $to_date;
		$user_subscription->app_res = $request->app_res ?? "";
		$user_subscription->receipt = $request->receipt ?? "";
		$user_subscription->save();

        $data['status'] 	= true;
        $data['message']	= "Subscription added successfully.";
        $data['data']	    = "";
    }
	return response()->json($data,$this->successStatus);


}

public function sendEmailVerificationLink() {
    $user = Auth::user();
    if($user->email_verified_at == null) {
        $user->sendEmailVerificationNotification();
        $data['status'] =  true;
        $data['message'] =  'Email verification link has been sent to your registered mail id.';
        $data['data'] =  $user;
    }
    else {
        $data['status'] =  true;
        $data['message'] =  'Email already verified.Link not send.';
        $data['data'] =  $user;
    }
    return response()->json($data, $this->successStatus);
}
public function getLastTenListeningAudio(Request $request) {
    $user = Auth::user();
    $lastTenAudio = AudioDuration::where('user_id',$user->id)->whereHas('audio',function($q){
        $q->where(['is_deleted'=>0,'status'=>1]);
    })->orderBy('id','desc')->limit(10)->pluck('audio_id');
    $audios = Audio::whereIn('id',$lastTenAudio)->with('AudioCategory.category')->orderBy('id','desc')->orderBy('order_number','asc')->get();
    if($audios) {
        $data['status'] =  true;
        $data['message'] =  'Audio list';
        $data['data'] =  $audios;
    }
    else {
        $data['status'] =  true;
        $data['message'] =  'Audio not found.';
        $data['data'] =  $user;
    }
    return response()->json($data, $this->successStatus);
}
public function checkPlaylistSleepTime($playlist_id,$user_id) {
    return PlaylistUser::where(['user_id'=>$user_id,'playlist_id'=>$playlist_id])->count();
}
public function playlistSleepTime(Request $request) {
    $validator = Validator::make($request->all(), [
        'playlist_id' => 'required|exists:playlists,id',
        'time_duration' => 'required',
    ]);

    if($validator->fails())
    {
        $data['status'] = false;
        $data['message']	= $validator->errors()->first();
        return response()->json($data);
    }

    $user = Auth::user();
    if($this->checkPlaylistSleepTime($request->playlist_id,$user->id) > 0) {
        $playlistUser = PlaylistUser::where(['user_id'=>$user->id,'playlist_id'=>$request->playlist_id])->first();
    }
    else {
        $playlistUser = new PlaylistUser;
    }
    $playlistUser->user_id = $user->id;
    $playlistUser->playlist_id = $request->playlist_id;
    $playlistUser->time_duration = $request->time_duration;

    if($playlistUser->save()) {
        $data['status'] =  true;
        $data['message'] =  'Playlist time duration saved successfully.';
        $data['data'] = $playlistUser;
    }
    else {
        $data['status'] =  true;
        $data['message'] =  'Playlist time duration not saved.';
        $data['data'] =  $user;
    }
    return response()->json($data, $this->successStatus);
}
public function getAudioDuration($user_id,$audio_id) {
	return AudioDuration::where(['user_id'=>$user_id,'audio_id'=>$audio_id])->first();
}
public function audioDurationUpdate(Request $request) {
	try {
    $validator = Validator::make($request->all(), [
        'audio_id' => 'required|exists:audio,id',
        'playlist_id' => 'required|exists:playlists,id',
        'audio_current_time' => 'required',
    ]);

    if($validator->fails())
    {
        $data['status'] = false;
        $data['message']	= $validator->errors()->first();
        return response()->json($data);
    }

    $user = Auth::user();
	$audio_duration = 0;
	$diff = 0;
    $audio = Audio::select('duration')->where('id',$request->audio_id)->first();
	if($audio) {
		$this->savePlaylistCurrentAudio($user->id,$request->audio_id,$request->playlist_id);

		$audio_duration = $audio->duration;
        $diff = $audio_duration - $request->audio_current_time;
		$audioDurationCount = AudioDuration::where(['user_id'=>$user->id,'audio_id'=>$request->audio_id])->count();
		if($audioDurationCount > 0) {
			$audioDuration = AudioDuration::select('is_finish')->where(['user_id'=>$user->id,'audio_id'=>$request->audio_id])->first();
			if($audioDuration->is_finish == 1) {
				AudioDuration::where(['user_id'=>$user->id,'audio_id'=>$request->audio_id])->update(['audio_current_time'=>$request->audio_current_time]);
				$audioDuration = $this->getAudioDuration($user->id,$request->audio_id);
				return response()->json(['status' => true, 'message' => 'Already finished','data'=>$audioDuration]);
			}
			else {
				if($diff < 60) {
					AudioDuration::where(['user_id'=>$user->id,'audio_id'=>$request->audio_id])->update(['audio_current_time'=>$request->audio_current_time,'is_finish'=>1]);
				}
				else {
					AudioDuration::where(['user_id'=>$user->id,'audio_id'=>$request->audio_id])->update(['audio_current_time'=>$request->audio_current_time]);
				}
				$audioDuration = $this->getAudioDuration($user->id,$request->audio_id);
				return response()->json(['status' => true, 'message' => 'Already updated successfully','data'=>$audioDuration]);
			}

		}
		else {
			$audioDuration = new AudioDuration();
			$audioDuration->user_id = $user->id;
			$audioDuration->audio_id = $request->audio_id;
			$audioDuration->audio_current_time = $request->audio_current_time;
			if($diff < 60) {
			$audioDuration->is_finish = 1;
			}
			$audioDuration->save();
			return response()->json(['status' => true, 'message' => 'Already inserted successfully','data'=>$audioDuration]);
		}

	}
    else {
        $data['status'] =  false;
        $data['message'] =  'audio not found.';
        $data['data'] =  array();
		return response()->json($data, $this->successStatus);
    }
    
}
catch (\Throwable $e) {
	return response()->json(['status' => false, 'message' => $e->getLine().' '.$e->getMessage()]);
}
}
public function savePlaylistCurrentAudio($user_id,$audio_id,$playlist_id) {
	$playlistCurrentAudioCount = PlaylistCurrentAudio::where(['user_id'=>$user_id,'playlist_id'=>$playlist_id])->count();
	if($playlistCurrentAudioCount > 0) {
		PlaylistCurrentAudio::where(['user_id'=>$user_id,'playlist_id'=>$playlist_id])->update(['audio_id'=>$audio_id]);
	}
	else {
		$playlistCurrentAudio = new PlaylistCurrentAudio();
		$playlistCurrentAudio->user_id = $user_id;
		$playlistCurrentAudio->playlist_id = $playlist_id;
		$playlistCurrentAudio->audio_id = $audio_id;
		$playlistCurrentAudio->save();
	}
}

public function social_login_new(Request $request){
        try{

            $data = $request->all();

            $validator = Validator::make($request->all(),
                [
                    'social_type'  => 'required|in:GooglePlus,Facebook,Twitter',
                    'social_id' => 'required',
                    'device_type' => 'required',
                    'device_token' => 'required',
                    'email' => 'nullable',
                    'profile_picture' => 'nullable'
                ]
            );
            if($validator->fails()){
                $response['status']     = false;
                $response['message']    = $this->validationHandle($validator->messages());
                $this->response($response);
            }else{

                $check_email = false;

                $check = User::where('social_id',$data['social_id'])->exists();

                if(isset($data['email']) && $data['email'] != '')
                $check_email = User::where('email',$data['email'])->exists();

                $updateArr = [];

                if($check){

                    if(isset($data['email'])){
                        $updateArr['email'] = $data['email'];
                    }

                    if(count($updateArr) > 0)
                    User::where('social_id',$data['social_id'])->where('social_type',$data['social_type'])->update($updateArr);

                    $user = User::where('social_id',$data['social_id'])->first();


                    if($request->get('profile_picture') && !isset($user->profile_picture)){

                        $imgurl = $request->get('profile_picture');
                        $image_name = 'user_'.$user->id.'.jpeg';

                        $destinationPath = '/profile/';
                        $ppath = public_path().'/'.$destinationPath;

                        file_put_contents($ppath.$image_name,file_get_contents($imgurl));
                        if($this->isImage('public/profile/'.$image_name)){

                            $user_img = 'public'.$destinationPath.$image_name;
                            User::where('id',$user->id)->update(['profile_picture' => $user_img]);


                        }
                    }

                    $userData = User::getProfile($user->id);
                    $jwtResponse = User::authorizeToken($user);
                    $userData->security_token = @$jwtResponse['token'];

                    $response['status']     = true;
                    $response['message']    = 'User details';
                    $response['data'] = $userData;

                    if($user){
                        UserDevices::deviceHandle([
                            "id"       =>  $user->id,
                            "device_type"   =>  $data['device_type'],
                            "device_token"  =>  $data['device_token'],
                        ]);
                    }

                }
                else{
                    if($check_email){

                        $user = User::where('email',$data['email'])->first();

                        if($request->get('profile_picture')){

                            $imgurl = $request->get('profile_picture');
                            $image_name = 'user_'.$user->id.'.jpeg';

                            $destinationPath = '/profile/';
                            $ppath = public_path().'/'.$destinationPath;

                            file_put_contents($ppath.$image_name,file_get_contents($imgurl));
                            if($this->isImage('public/profile/'.$image_name)){

                                $user_img = 'public'.$destinationPath.$image_name;
                                $updateArr['profile_picture'] = $user_img;
                            }
                        }

                        $updateArr['social_type'] = $data['social_type'];
                        $updateArr['social_id'] = $data['social_id'];

                        User::where('email',$data['email'])->update($updateArr);

                        $user = User::where('email',$data['email'])->first();

                        $userData = User::getProfile($user->id);
                        $jwtResponse = User::authorizeToken($user);
                        $userData->security_token = @$jwtResponse['token'];

                        $response['status']     = true;
                        $response['message']    = 'User details';
                        $response['data'] = $userData;

                        if($user){
                            UserDevices::deviceHandle([
                                "id"       =>  $user->id,
                                "device_type"   =>  $data['device_type'],
                                "device_token"  =>  $data['device_token'],
                            ]);
                        }
                    }
                    else{

                        if(isset($data['email'])){
                            $updateArr['email'] = $data['email'];
                        }

                        $updateArr['social_type'] = $data['social_type'];
                        $updateArr['social_id'] = $data['social_id'];

                        $user = User::Create($updateArr);

                        if($request->get('profile_picture')){

                            $imgurl = $request->get('profile_picture');
                            $image_name = 'user_'.$user->id.'.jpeg';

                            $destinationPath = '/profile/';
                            $ppath = public_path().'/'.$destinationPath;

                            file_put_contents($ppath.$image_name,file_get_contents($imgurl));
                            if($this->isImage('public/profile/'.$image_name)){

                                $user_img = 'public'.$destinationPath.$image_name;
                                User::where('id',$user->id)->update(['profile_picture' => $user_img]);
                            }
                        }

                        $userData = User::getProfile($user->id);
                        $jwtResponse = User::authorizeToken($user);
                        $userData->security_token = @$jwtResponse['token'];

                        $response['status']     = true;
                        $response['message']    = 'User details';
                        $response['data'] = $userData;

                        if($user){
                            UserDevices::deviceHandle([
                                "id"       =>  $user->id,
                                "device_type"   =>  $data['device_type'],
                                "device_token"  =>  $data['device_token'],
                            ]);
                        }

                    }
                }

                $this->response($response);

            }
        } catch (\Exception $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage().' on '.$e->getLine();
            $response['data'] = [];

            $this->response($response);
        }
    }


	 public function notification($token, $title, $description,$type)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token=$token;
		$url = url('/')."/public/images/logo.jpg";
        $notification = [
            'title' => $title,
			'description'=>$description,
			'type'=>$type,
			//'image'=>$url,
            'sound' => true,
        ];

       $extraNotificationData = ["data" => $notification];

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'data' => $notification
        ];

        $headers = [
            'Authorization: key=AAAAtrVxdWM:APA91bFIaXDMYYdf1Qmwpj-DEqrHe98W-bJQc3hST6BhdBqq9vtXYNdzcOKk_-L24Fxd5HpaRgUkTqrEG4IMUpN-Zuab_PRG1T79Q-l7aCJCw7SU7lkrIWqnMNK4H057fqC2o91YCid6',
            'Content-Type: application/json'
        ];




        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return true;
    }



    /////////////////////////////////////////////////////////////////////////////////////

}
