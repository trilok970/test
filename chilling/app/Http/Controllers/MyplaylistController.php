<?php

namespace App\Http\Controllers;

use App\Myplaylist;
use Illuminate\Http\Request;

class MyplaylistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Myplaylist  $myplaylist
     * @return \Illuminate\Http\Response
     */
    public function show(Myplaylist $myplaylist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Myplaylist  $myplaylist
     * @return \Illuminate\Http\Response
     */
    public function edit(Myplaylist $myplaylist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Myplaylist  $myplaylist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Myplaylist $myplaylist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Myplaylist  $myplaylist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Myplaylist $myplaylist)
    {
        //
    }
}
