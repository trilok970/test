<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Verified;

class VerifyEmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $request) {
        $userID = $request[‘id’];
        $user = User::findOrFail($userID);
        $date = date(“Y-m-d g:i:s”);
        $user->email_verified_at = $date; // to enable the “email_verified_at field of that user be a current time stamp by mimicing the must verify email feature
        $user->save();
        return back()->with('message','Email verified Successfully');
        //return response()->json(‘Email verified!’);

    }

}
