<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\DeviceToken as fcmToken;
use App\Notification;
use DB;
class UserFcmTokenController extends Controller
{
    /**
     *
     * @var user
     * @var FCMToken
     */
    protected $user;
    protected $fcmToken;

    /**
     * Constructor
     *
     * @param
     */
    public function __construct()
    {

    }

     /**
     * Functionality to send notification.
     *
    */
    public function notificationCount($user_id)
    {
        return Notification::select('read_unread')->where(['user_id'=>$user_id,'read_unread'=>0])->count() ?? 0;
    }
    // public function sendNotification()
    public function sendNotification($user_id,$title,$description,$type,$file=null)
    {
    	// $user_id=83;$description="Test notification from laravel ".date("d-m-Y h:i:s");$title="Test from web";$type="order";$file=null;
        $tokens = [];
        $apns_ids = [];
        $responseData = [];

        // $this->notification_entry($user_id,$title,$description,$type,$file);


	// for Android
        $user = User::find($user_id);
        if($user->notification == 1)
        {
            if ($FCMTokenData = fcmToken::where('user_id',$user_id)->where('device_token','!=',null)->where('device_type','ANDROID')->select('device_token')->get())
            {
                foreach ($FCMTokenData as $key => $value)
                {
                    $tokens[] = $value->device_token;

                }

                $msg = array('type'=>$type,'title' => $title,'body'  => $description,'subtitle' => 'This is a subtitle','image'=>$file,'android'=>array("imageUrl"=>$file),
                      );

                $fields = array
                        (
                            'registration_ids'  => $tokens,
                            'notification'  => $msg
                        );


                $headers = array
                        (
                            'Authorization: key=' . env('FCM_SERVER_KEY'),
                            'Content-Type: application/json'
                        );

                $ch = curl_init();
                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                curl_setopt( $ch,CURLOPT_POST, true );
                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                $result = curl_exec($ch );

                if ($result === FALSE)
                {
                    die('FCM Send Error: ' . curl_error($ch));
                }

                $result = json_decode($result,true);

                $responseData['android'] =[
                           "result" =>$result
                        ];

                curl_close( $ch );

            }

        // for IOS
            $read_unread = $this->notificationCount($user_id);
            if ($FCMTokenData = fcmToken::where('user_id',$user_id)->where('device_token','!=',null)->where('device_type','IOS')->select('device_token')->get())
            {
               foreach ($FCMTokenData as $key => $value)
                {
                    $apns_ids[] = $value->device_token;
                }

                $url = "https://fcm.googleapis.com/fcm/send";

                $serverKey = env('FCM_SERVER_KEY');
                $title = $title;
                $body = $description;
                $notification = array('type'=>$type, 'title' =>$title , 'body' => $body ,'text' => $body, 'sound' => 'default', 'badge' => $read_unread,'image'=>$file);
                $arrayToSend = array('registration_ids' => $apns_ids, 'notification' => $notification,'priority'=>'high');
                $json = json_encode($arrayToSend);
                $headers = array();
                $headers[] = 'Content-Type: application/json';
                $headers[] = 'Authorization: key='. $serverKey;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                //Send the request
                $result = curl_exec($ch);

                if ($result === FALSE)
                {
                    die('FCM Send Error: ' . curl_error($ch));
                }
                $result = json_decode($result,true);
                // echo "<pre>";
                // print_r($result);
                // exit();
                $responseData['ios'] =[
                            "result" =>$result
                        ];

                //Close request
                curl_close($ch);
            }
        }




         return $responseData;

     }
     public function sendNotification_new($tokens,$title,$description,$type,$file=null,$type_val)
     {
         // $user_id=83;$description="Test notification from laravel ".date("d-m-Y h:i:s");$title="Test from web";$type="order";$file=null;
        //  $tokens = [];
        //  $apns_ids = [];
         $responseData = [];

         // for Android
             if ($type_val == 'android')
             {
                // echo $type_val;exit;
//                 echo $type_val;
// echo  "<pre>";print_r($tokens);exit;

                 $msg = array('type'=>$type,'title' => $title,'body'  => $description,'subtitle' => 'This is a subtitle','image'=>$file,'android'=>array("imageUrl"=>$file),
                       );

                 $fields = array
                         (
                             'registration_ids'  => $tokens,
                             'notification'  => $msg
                         );

                 $headers = array
                         (
                             'Authorization: key=' . env('FCM_SERVER_KEY'),
                             'Content-Type: application/json'
                         );

                 $ch = curl_init();
                 curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                 curl_setopt( $ch,CURLOPT_POST, true );
                 curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                 curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                 curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                 curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                 $result = curl_exec($ch );

                 if ($result === FALSE)
                 {
                     die('FCM Send Error: ' . curl_error($ch));
                 }

                 $result = json_decode($result,true);
                 echo "<pre>";
                 print_r($result);
                 dd($result);
                 exit();
                 $responseData['android'] =[
                            "result" =>$result
                         ];

                 curl_close( $ch );

             }

         // for IOS
             // $read_unread = $this->notificationCount($user_id);
             $read_unread = 0;
             if ($type_val == 'ios')
             {

                 $apns_ids = $tokens;


                 $url = "https://fcm.googleapis.com/fcm/send";

                 $serverKey = env('FCM_SERVER_KEY');
                 $title = $title;
                 $body = $description;
                 $notification = array('type'=>$type, 'title' =>$title , 'body' => $body ,'text' => $body, 'sound' => 'default', 'badge' => $read_unread,'image'=>$file);
                 $arrayToSend = array('registration_ids' => $apns_ids, 'notification' => $notification,'priority'=>'high');
                 $json = json_encode($arrayToSend);
                 $headers = array();
                 $headers[] = 'Content-Type: application/json';
                 $headers[] = 'Authorization: key='. $serverKey;

                 $ch = curl_init();
                 curl_setopt($ch, CURLOPT_URL, $url);
                 curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
                 curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                 curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                 //Send the request
                 $result = curl_exec($ch);

                 if ($result === FALSE)
                 {
                     die('FCM Send Error: ' . curl_error($ch));
                 }
                 $result = json_decode($result,true);
                 echo "<pre>";
                 print_r($result);
                 dd($result);
                 exit();
                 $responseData['ios'] =[
                             "result" =>$result
                         ];

                 //Close request
                 curl_close($ch);
             }

          return $responseData;

      }
 public function sendNotification1($user_id)
    // public function sendNotification1($user_id,$title,$description,$type,$file=null)
    {
        // echo "check";exit;
        $description="Test notification from laravel ".date("d-m-Y h:i:s");
        $title="Test from web";
        $type="order";
        $file=null;
        $users = User::where(['is_deleted'=>0,'status'=>1,'notification'=>1,'id'=>$user_id])->whereRaw("(device_token !='' and device_token is not null)")->get();
        $androidToken = User::where(['is_deleted'=>0,'status'=>1,'notification'=>1,'device_type'=>'ANDROID','id'=>$user_id])->whereRaw("(device_token !='' and device_token is not null)")->pluck('device_token')->toArray();

        // if ($FCMTokenData = fcmToken::select(DB::raw("distinct(device_token)"),'device_type','user_id')
        //         ->where('device_token','!=',null)
        //         ->whereIn('device_type',['ANDROID','IOS'])
        //         ->whereHas('user',function($q) {
        //             $q->where(['is_deleted'=>0,'notification'=>1,'status'=>1]);
        //         })
        //         ->get())
        //         {
                if($users) {

                    $apns_ids = [];
                    $tokens = [];
                    // foreach ($users as $key => $user)
                    // {

                    //     if($user->device_type == 'ANDROID') {
                    //         $tokens[] = $user->device_token;
                    //     }
                    //     if($user->device_type == 'IOS') {
                    //         $apns_ids[] = $user->device_token;
                    //     }

                    //     $notification['user_id'] = $user->id;
                    //     $notification['title'] = $title;
                    //     $notification['message'] = $description;
                    //     $notification['type'] = $type;
                    //     if($file) {
                    //     $notification['image'] = str_replace("https://chilling-assets.s3.us-east-2.amazonaws.com/", '', $file);
                    //     }
                    //     $notification_array[] = $notification;

                    // }

                    $tokens = $androidToken;

                if($tokens) {
                    $androidChunks = array_chunk($tokens,1000);
                    //  echo "<pre>";
                    // print_r($androidChunks);exit;
                    foreach($androidChunks as $androidToken){
                        $this->sendNotification_new($androidToken,$title,$description,$type,$file,'android');
                    }
                }
                if($apns_ids) {
                    $iosChunks = array_chunk($apns_ids,1000);
                    foreach($iosChunks as $iosToken){
                        $this->sendNotification_new($iosToken,$title,$description,$type,$file,'ios');
                    }
                }
            }
            echo "<pre>";
            print_r($tokens);
            // $notification_array = array_unique($notification_array, SORT_REGULAR);
            exit;

        $tokens = [];
        $apns_ids = [];
        $responseData = [];

        $this->notification_entry($user_id,$title,$description,$type,$file);

  // for Android
        $user = User::find($user_id);

        if($user->notification == 1)
        {

            if ($FCMTokenData = fcmToken::where('user_id',$user_id)->where('device_token','!=',null)->where('device_type','ANDROID')->select('device_token')->get())
            {
                foreach ($FCMTokenData as $key => $value)
                {
                    $tokens[] = $value->device_token;

                }

                $msg = array('type'=>$type,'title' => $title,'body'  => $description,'subtitle' => 'This is a subtitle','image'=>$file,'android'=>array("imageUrl"=>$file),
                      );

                $fields = array
                        (
                            'registration_ids'  => $tokens,
                            'notification'  => $msg
                        );


                $headers = array
                        (
                            'Authorization: key=' . env('FCM_SERVER_KEY'),
                            'Content-Type: application/json'
                        );


                $ch = curl_init();
                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                curl_setopt( $ch,CURLOPT_POST, true );
                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                $result = curl_exec($ch );
            // var_dump($result);
                if ($result === FALSE)
                {
                    die('FCM Send Error: ' . curl_error($ch));
                }

                $result = json_decode($result,true);

                $responseData['android'] =[
                           "result" =>$result
                        ];
                echo "<pre>";
                print_r($responseData);
                exit;

                curl_close( $ch );

            }

        // for IOS
            $read_unread = $this->notificationCount($user_id);
            if ($FCMTokenData = fcmToken::where('user_id',$user_id)->where('device_token','!=',null)->where('device_type','IOS')->select('device_token')->get())
            {
               foreach ($FCMTokenData as $key => $value)
                {
                    $apns_ids[] = $value->device_token;
                }

                $url = "https://fcm.googleapis.com/fcm/send";

                $serverKey = env('FCM_SERVER_KEY');
                $title = $title;
                $body = $description;
                $notification = array('type'=>$type, 'title' =>$title , 'body' => $body ,'text' => $body, 'sound' => 'default', 'badge' => $read_unread,'image'=>$file);
                $arrayToSend = array('registration_ids' => $apns_ids, 'notification' => $notification,'priority'=>'high');
                $json = json_encode($arrayToSend);
                $headers = array();
                $headers[] = 'Content-Type: application/json';
                $headers[] = 'Authorization: key='. $serverKey;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                //Send the request
                $result = curl_exec($ch);

                if ($result === FALSE)
                {
                    die('FCM Send Error: ' . curl_error($ch));
                }
                $result = json_decode($result,true);
                // echo "<pre>";
                // print_r($result);
                // exit();
                $responseData['ios'] =[
                            "result" =>$result
                        ];

                //Close request
                curl_close($ch);
            }
        }




         return $responseData;

     }
     public function notification_entry($user_id,$title,$message,$type,$file=null)
     {
        $notification = new Notification;
        $notification->user_id = $user_id;
        $notification->title = $title;
        $notification->message = $message;
        $notification->type = $type;
        if($file) {
        $notification->image = str_replace("https://chilling-assets.s3.us-east-2.amazonaws.com/", '', $file);
        }

        $notification->save();
     }
     public function notification_entry_for_all($notification_array)
     {
        $notification = new Notification;
        $notification->insert($notification_array);
        // echo "<pre>";
        // print_r($notification_array);exit;
     }

     public function sendNotificationToAllUsers($title,$message,$type,$file=null)
     {
         $users = User::where(['is_deleted'=>0,'status'=>1,'notification'=>1])->has('DeviceToken')->get();
         foreach ($users as $key => $value) {
            $this->sendNotification($value->id,$title,$message,$type,$file);
            $notification['user_id'] = $value->id;
            $notification['title'] = $title;
            $notification['message'] = $message;
            $notification['type'] = $type;
                if($file) {
            $notification['image'] = str_replace("https://chilling-assets.s3.us-east-2.amazonaws.com/", '', $file);
            }
            $notification_array[] = $notification;
        }
        $this->notification_entry_for_all($notification_array);

     }
     public function sendNotificationSubscriptionPlan($title,$description,$type)
     {
        // SELECT * from users where `subscription_plan_last_date` BETWEEN "2020-12-22" and DATE_ADD("2020-12-22",INTERVAL 5 DAY)

        $users = User::where(['is_deleted'=>0,'status'=>1,'notification'=>1])->whereRaw("subscription_plan_last_date BETWEEN DATE(now()) and DATE_ADD(DATE(now()),INTERVAL 5 DAY)")->has('DeviceToken')->get();
// print_r($users);
         foreach ($users as $key => $value) {
            $this->sendNotification($value->id,$title,$description,$type);
        }

     }

}
