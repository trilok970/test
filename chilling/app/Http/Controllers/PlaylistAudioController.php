<?php

namespace App\Http\Controllers;

use App\PlaylistAudio;
use Illuminate\Http\Request;
use DB;
class PlaylistAudioController extends Controller
{
    public function playlist_audio_free($id)
    {
        $playlistaudio = PlaylistAudio::find($id);
        $playlistaudio->is_free = 1;
        if ($playlistaudio->save()) {
            DB::table('playlist_audio')->where(['playlist_id'=>$playlistaudio->playlist_id])->where('id','!=',$id)->update(['is_free'=>0]);
            return back()->with('message', 'Audio song added to free.');
        } else {
            return back()->with('message', 'Audio not found');
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PlaylistAudio  $playlistAudio
     * @return \Illuminate\Http\Response
     */
    public function show(PlaylistAudio $playlistAudio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PlaylistAudio  $playlistAudio
     * @return \Illuminate\Http\Response
     */
    public function edit(PlaylistAudio $playlistAudio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PlaylistAudio  $playlistAudio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PlaylistAudio $playlistAudio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PlaylistAudio  $playlistAudio
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlaylistAudio $playlistAudio)
    {
        //
    }
}
