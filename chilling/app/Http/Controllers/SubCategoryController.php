<?php

namespace App\Http\Controllers;

use App\SubCategory;
use App\Category;
use Illuminate\Http\Request;
use Auth;
use Validator;
use DB;
class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // echo "string";dd();
        $datas = SubCategory::where(['is_deleted'=>0])->get();
        // echo "<pre>";
        // print_r($datas[0]->get_category);
        // dd();

        return view('admin/subcategory.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::where(['is_deleted'=>0])->orderByDesc('id')->get();
        return view('admin/subcategory.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = validator::make($request->all(),[
        'category_id' => 'required',
        'name' => 'required',
        'image' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        if($request->hasFile('image') && $request->image->isValid())
           {
                $extension = $request->image->extension();
                $fileName  = "image".time().".$extension";
                $ff = $request->image->move(public_path('images'),$fileName);

            }
            else
            {
                $fileName  ='default.jpg';
            }
        $subcategory = new SubCategory;
        $subcategory->name = ucfirst($request->name);
        $subcategory->user_id = Auth::user()->id;
        $subcategory->category_id = $request->category_id;
        $subcategory->image = $fileName;
        if($subcategory->save())
        {
            return redirect('admin/subcategory')->with('message','Sub Category Added Successfully');
        }
        else
        {
            return back()->with('message','Sub Category Not Added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function show(SubCategory $subcategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategory $subcategory)
    {
        //
        $categories = Category::where(['is_deleted'=>0])->orderByDesc('id')->get();
        return view('admin/subcategory.edit',compact('subcategory','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubCategory $subcategory)
    {
        //
        $validator = validator::make($request->all(),[
        'category_id' => 'required',
        'name' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        if($request->hasFile('image') && $request->image->isValid())
           {
                $extension = $request->image->extension();
                $fileName  = "image".time().".$extension";
                $ff = $request->image->move(public_path('images'),$fileName);

            }
            else
            {
                $fileName  = $subcategory->image;
            }
        $subcategory->name = ucfirst($request->name);
        $subcategory->user_id = Auth::user()->id;
        $subcategory->category_id = $request->category_id;
        $subcategory->image = $fileName;
        if($subcategory->save())
        {
            return redirect('admin/subcategory')->with('message','Sub Category Updated Successfully');
        }
        else
        {
            return back()->with('message','Sub Category Not Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategory $subcategory)
    {
        //
        $subcategory->is_deleted = 1;
        if($subcategory->save())
        {
            return redirect('admin/subcategory')->with('message','Sub Category Deleted Successfully');
        }
        else
        {
            return back()->with('message','Sub Category Not Deleted');
        }
    }
}
