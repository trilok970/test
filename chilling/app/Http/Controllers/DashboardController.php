<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Channel;
use App\Playlist;
use App\Audio;
use App\Mystory;
use App\Category;
use App\Cms;
use Auth;
use Hash;
use DB;
use Session;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\TopTenAudio;
use App\Exports\TopTenFavouriteAudio;
use App\Exports\TopTenPlaylist;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Mail;
use App\Mail\UserMail;
use App\UserSubscription;
// use ReceiptValidator\iTunes\Validator as iTunesValidator;
// use ReceiptValidator\GooglePlay\Validator as PlayValidator;
use Google_Client;
class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user_count = User::where(['is_deleted'=>0])->where('id','!=',1)->count();
        $verified_user_count = User::where(['is_deleted'=>0])->where('id','!=',1)->where('email_verified_at','!=',null)->count();
        $channel_count = Channel::where(['is_deleted'=>0])->count();
        $playlist_count = Playlist::where(['is_deleted'=>0])->count();
        $audio_count = Audio::where(['is_deleted'=>0])->count();
        $category_count = Category::where(['is_deleted'=>0])->count();
        $user_story_count = Mystory::where(['is_deleted'=>0])->count();





        // echo "<pre>";
        // print_r($top_ten_audios_keys);
        // exit;
        return view('admin/dashboard.index',compact('user_count','channel_count','playlist_count','audio_count','category_count','user_story_count','verified_user_count'));
    }

    public function userDataMonthWise($key_val)
    {
        $y = date('Y');
        if($key_val == 'year')
        {
            for($i=1;$i<=12;$i++)
            {
                $arr[] = User::select(DB::raw("count(id) as total"))
                ->where(['is_deleted'=>0])->where('id','!=',1)
                ->whereMonth('created_at',$i)
                ->whereYear('created_at',$y)
                ->count();
            }
        }
        else if($key_val == 'month')
        {
            $month = date('m');
            $month_last_day = date('t');
            $datesArray = $this->getMonthDates_Ymd($y);
            foreach($datesArray as $dates)
            // for($i=1;$i<=$month_last_day;$i++)
            {
                // $i = str_pad($i,2,0,STR_PAD_LEFT);
                 $i = $dates;
                $arr[] = User::select(DB::raw("count(id) as total"))
                ->where(['is_deleted'=>0])->where('id','!=',1)
                ->whereDate('created_at',date("$i",strtotime($i)))
                ->count();
            }

        }
        else if($key_val == 'week')
        {

            $week_dates = $this->getLastWeekDates($y);
            foreach($week_dates as $date)
            {
                $arr[] = User::select(DB::raw("count(id) as total"))
                ->where(['is_deleted'=>0])->where('id','!=',1)
                ->whereDate('created_at',date("Y-m-d",strtotime($date)))
                ->count();
            }
        }

        return $arr;

    }
    public function getTopTenAudioView($key_val)
    {
        $y = date('Y');

        if($key_val == 'year')
        {
            $from_date = date("$y-01-01");
            $to_date = date("$y-12-31");

        }
        else if($key_val == 'month')
        {
            $from_date = date("$y-m-01");
            $to_date = date("$y-m-t");

        }
        else if($key_val == 'week')
        {
            $from_date = date("$y-m-Y",strtotime("-7 day"));
            $to_date = date("$y-m-d");

        }
        $audios = Audio::select('title','view_count')->where(['is_deleted'=>0])
                ->where('view_count','>',0)
                ->whereBetween("updated_at",[$from_date,$to_date])
                ->orderBy('view_count','desc')
                ->limit(10)->get()->toArray();
        $data['keys'] = array_column($audios,"view_count");
        $data['values'] = array_column($audios,"title");
        $data['values'] = array_map( array($this,'decode'), $data['values'] );
        $data['values'] = array_map( 'strip_tags', $data['values'] );


        // echo "<pre>";
        // print_r($audios);
        // print_r($data);
        // exit;
        return $data;

    }
    function decode($data) {
        return html_entity_decode($data, ENT_QUOTES, 'UTF-8');
    }
    public function getTopTenFavouriteAudio($key_val)
    {
        // SELECT audio.title,COUNT(fv.`audio_id`) as audio_count FROM `audio`
        // left join favourites as fv on audio.id = fv.audio_id
        // GROUP by fv.audio_id HAVING audio_count > 0 order by audio_count DESC limit 10

        $y = date('Y');

        if($key_val == 'year')
        {
            $from_date = date("$y-01-01");
            $to_date = date("$y-12-31");

        }
        else if($key_val == 'month')
        {
            $from_date = date("$y-m-01");
            $to_date = date("$y-m-t");

        }
        else if($key_val == 'week')
        {
            $from_date = date("$y-m-Y",strtotime("-7 day"));
            $to_date = date("$y-m-d");

        }

        $audios = Audio::select('audio.id','audio.title',DB::raw("count(fv.audio_id) as audio_count"))
                ->leftjoin("favourites as fv",function($join){
                    $join->on("audio.id","=","fv.audio_id");
                })
                ->where(['audio.is_deleted'=>0])
                ->whereBetween("fv.created_at",[$from_date,$to_date])
                ->groupBy('fv.audio_id')
                ->havingRaw("audio_count > 0")
                ->orderBy("audio_count",'desc')
                ->limit(10)->pluck('audio_count','title')->toArray();
        $data['values'] = array_keys($audios);
        $data['keys'] = array_values($audios);
        $data['values'] = array_map( array($this,'decode'), $data['values'] );
        $data['values'] = array_map( 'strip_tags', $data['values'] );

        // echo "<pre>";
        // // print_r($audios);
        // print_r($data);
        // exit;
        return $data;

    }
    public function getTopTenPlaylistView($key_val)
    {
        $y = date('Y');

        if($key_val == 'year')
        {
            $from_date = date("$y-01-01");
            $to_date = date("$y-12-31");

        }
        else if($key_val == 'month')
        {
            $from_date = date("$y-m-01");
            $to_date = date("$y-m-t");

        }
        else if($key_val == 'week')
        {
            $from_date = date("$y-m-Y",strtotime("-7 day"));
            $to_date = date("$y-m-d");

        }
        $audios = Playlist::select('name','view_count')->where(['is_deleted'=>0])
                    ->where('view_count','>',0)
                    ->whereBetween("updated_at",[$from_date,$to_date])
                    ->orderBy('view_count','desc')->limit(10)->get()->toArray();

        $data['keys'] = array_column($audios, 'view_count');
        $data['values'] = array_column($audios, 'name');

        // echo "<pre>";
        // print_r($audios);
        // print_r($data);
        // exit;
        return $data;

    }
    public function getMonthNames()
    {
        $array = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
        return $array;
    }
    public function getLastWeekDates($y)
    {
        for($i=1;$i<=7;$i++)
        {
            $array[] = date("d-m-$y",strtotime("-$i day"));
        }
        // echo "<pre>";
        // print_r($array);
        return $array;
    }
    public function getMonthDates_Ymd($y)
    {
        for($i=1;$i<=date('t');$i++)
        {
            $i = str_pad($i,2,0,STR_PAD_LEFT);
            $array[] = date("$y-m-$i");
        }
        // echo "<pre>";
        // print_r($array);
        return $array;
    }
    public function getMonthDates($y)
    {
        for($i=1;$i<=date('t');$i++)
        {
            $i = str_pad($i,2,0,STR_PAD_LEFT);
            $array[] = date("$i-m-$y");
        }
        // echo "<pre>";
        // print_r($array);
        return $array;
    }
    public function graph_date($key_val)
    {
        $y = date('Y');
        $usermonthdata = $this->userDataMonthWise($key_val);
        if($key_val == 'year')
        $usermonthdatakeys = $this->getMonthNames();
        else if($key_val == 'month')
        $usermonthdatakeys = $this->getMonthDates($y);
        else if($key_val == 'week')
        $usermonthdatakeys = $this->getLastWeekDates($y);



        $top_ten_audios = $this->getTopTenAudioView($key_val);
        $top_ten_audios_keys = $top_ten_audios['keys'];
        $top_ten_audios_values = $top_ten_audios['values'];

        $favourites_audios = $this->getTopTenFavouriteAudio($key_val);
        $favourites_audios_keys = $favourites_audios['keys'];
        $favourites_audios_values = $favourites_audios['values'];

        $top_ten_playlist = $this->getTopTenPlaylistView($key_val);
        $top_ten_playlist_keys = $top_ten_playlist['keys'];
        $top_ten_playlist_values = $top_ten_playlist['values'];

        $data['result'] = 1;
        $data['usermonthdata'] = $usermonthdata;
        $data['usermonthdatakeys']   = $usermonthdatakeys;
        $data['user_count']   = array_sum($usermonthdata)." of users";

        $data['top_ten_audios_keys'] = $top_ten_audios_values;
        $data['top_ten_audios_values'] = $top_ten_audios_keys;
        $data['top_ten_audios_count'] = count($top_ten_audios_values)." of stories";

        $data['favourites_audios_keys'] = $favourites_audios_values;
        $data['favourites_audios_values'] = $favourites_audios_keys;
        $data['favourites_audios_count'] = count($favourites_audios_values)." of stories";

        $data['top_ten_playlist_keys'] = $top_ten_playlist_values;
        $data['top_ten_playlist_values'] = $top_ten_playlist_keys;
        $data['top_ten_playlist_count'] = count($top_ten_playlist_values)." of playlists";

        $data['user_export_url'] = url('admin/user-export/'.$key_val);
        $data['top_ten_listening_stories_url'] = url('admin/top-ten-listening-stories-export/'.$key_val);
        $data['top_ten_favourite_stories_url'] = url('admin/top-ten-favourite-stories-export/'.$key_val);
        $data['top_ten_playlist_export_url'] = url('admin/top-ten-playlist-export/'.$key_val);

        return response()->json($data);


    }
    public function topTenListeningStoriesExport($key_val) {
        return Excel::download(new TopTenAudio($key_val),'toptenstories.xlsx');
    }
    public function topTenFavouriteStoriesExport($key_val) {
        return Excel::download(new TopTenFavouriteAudio($key_val),'toptenfavourite.xlsx');
    }
    public function topTenPlaylistExport ($key_val) {
        return Excel::download(new TopTenPlaylist($key_val),'playlist.xlsx');
    }
    public function myProfile()
    {
        return view('admin.dashboard.my-profile');
    }
    public function editProfile()
    {
        $user = Auth::user();
        return view('admin.dashboard.edit-profile',compact('user'));
    }
    public function editProfilePost(Request $request)
    {
        $id = Auth::user()->id;
        $user = User::find($id);
        $credentials = request()->validate([
            'fullname' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'phone_number' => 'required',
        ]);
        if($request->hasFile('profile_pic') && $request->profile_pic->isValid())
        {
            $user->profile_pic  =  image_upload($request->profile_pic,"images",'public');
        }


        $user->fullname = ucwords($request->fullname);
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->save();
        return redirect('admin/dashboard')->with('message','Profile updated successfully.');
    }
    public function createTable()
    {

        Schema::create('sessions', function (Blueprint $table) {
            $table->string('id')->unique();
            $table->foreignId('user_id')->nullable();
            $table->string('ip_address', 45)->nullable();
            $table->text('user_agent')->nullable();
            $table->text('payload');
            $table->integer('last_activity');
        });


        // Schema::dropIfExists('jobs');



    }
    public function privacy_policy()
    {
        $page_title = "Privacy Policy";
        $cms = Cms::where('slug','privacy-policy')->first();
        return view('privacy-policy',compact('cms','page_title'));

    }
    public function check_mail($email)
    {


       Mail::to($email)->send(new UserMail());
        if (Mail::failures())
        {
            echo  "response showing failed emails";
        }

           echo "Email sent successfully";


    }
    public function changePassword()
    {
        $user = Auth::user();

        return view('admin.dashboard.change-password',compact('user'));
    }
    public function changePasswordPost(Request $request)
    {
        $user = Auth::user();

        $request->validate([

            'current_password' => ['required'],

            'new_password' => ['required'],

            'password_confirmation' => ['same:new_password'],

        ]);

        if(!Hash::check($request->current_password, $user->password))
        {
            return back()->with('error_message','Current password does not match.');
        }
        else
        {
            User::find($user->id)->update(['password'=> Hash::make($request->new_password)]);
            return redirect('admin/dashboard')->with('message','Password successfully changed.');
        }

    }
    protected function getUserSubscription($user_id) {
        return  $subscription = UserSubscription::where('user_id',$user_id)->latest()->first();
      }
    public function receiptValidatorCheck(Request $request,$email) {
      //   DB::enableQueryLog();
      //   $user = User::where(['is_deleted'=>0,'status'=>1])->where('subscription_plan_last_date','>=',date('Y-m-d'))->get();
      // print_r(DB::getQueryLog());
       // exit;
        // $users = User::where(['is_deleted'=>0,'status'=>1,'notification'=>1])->whereRaw('(device_token != "" or device_token != null)')->whereRaw("subscription_plan_last_date BETWEEN DATE(now()) and DATE_ADD(DATE(now()),INTERVAL 5 DAY)")->get();

        $client = new \Google\Client();
        $client->setApplicationName('Chilling.io');
        $client->setAuthConfig(json_decode(env('GOOGLE_SECRET'),true));
        $client->setScopes([\Google_Service_AndroidPublisher::ANDROIDPUBLISHER]);
        // For Auth2
        // $validator = new PlayValidator(new \Google_Service_AndroidPublisher($client));
        // For Service Account

        $googleAndroidPublisher = new \Google_Service_AndroidPublisher($client);
        $validator = new \ReceiptValidator\GooglePlay\Validator($googleAndroidPublisher);
        // dd($validator);

        // {"productId":"com.1monthunlimited","purchaseToken":"hnlphemddjaedllaoppimaab.AO-J1OyPaO8Eaww6l9adSgf1sbpmvUXz0QzTFpDuBLhOMbvBb2yaXQ2O1VuLc0aUmCfoi-VrTZR3Rcmh68dAHPuUvbJCcm2l6w","purchaseTime":1619063181597,"developerPayload":null}
        try {
            $user = User::where(['email'=>$email,'is_deleted'=>0])->first();
            if($user) {
                $subscription = $this->getUserSubscription($user->id);
                $receipt_data = json_decode($subscription->receipt);
                if(isset($receipt_data->purchaseToken)) {
                    $productId = $receipt_data->productId ?? "com.1monthunlimited";
                    $packageName = $receipt_data->packageName ?? "com.chilling";
                    $purchaseToken = $receipt_data->purchaseToken;
                    echo "productId >> ".$productId;
                    echo "<br>packageName >> ".$packageName;
                    echo "<br>purchaseToken >> ".$purchaseToken;
                    // exit;
                }
                else {
                    return "This is ios user";
                    exit;
                }
            }
            else {
                $productId = 'com.chilling';
                $packageName = 'com.1monthunlimited';
                $purchaseToken = 'coafenopngpdmgoageddgeae.AO-J1OyzE7dt-YUS4al_-7W5NY4hlEYyB6ahf8NZtKPgihyoJYbc8CvRRDQpS9DDUtPCbdFzFOE84MqWe4ZyA-B6-MQx2_6HXg';
            }
            
            $response = $validator->setPackageName($packageName ?? "com.chilling")
            ->setProductId($productId ?? "com.1monthunlimited")
            ->setPurchaseToken($purchaseToken)
            ->validateSubscription();


           $response = (array)$response;
           echo "<pre>";
            print_r($response) ;
            if(!is_null($response)) {
                foreach ($response as $key => $value) {
                $expiryTimeMillis =  $value['expiryTimeMillis'];
              echo "expire_date >> ".   $expire_date = date('Y-m-d h:i:s', $expiryTimeMillis/1000. - date("Z"));
              echo "<br>purchase_date >> ".   $expire_date = date('Y-m-d h:i:s', $value['startTimeMillis']/1000. - date("Z"));
                break;
                }
               echo "<br/>today_date >> ".  $today_date = date("Y-m-d h:i:s");
                $today_date = date("Y-m-d h:i:s");
                $milliseconds = round(microtime(true) * 1000);
                // if($expire_date > $today_date) {
                if($expiryTimeMillis > $milliseconds) {
                return 1;
                }
                else {
                    return 0;
                }
            }
            else {
                return 0;
            }

        } catch (\Exception $e){
        //   var_dump($e->getMessage());
          return "Exception ".$e->getLine().' '.$e->getMessage();
          // example message: Error calling GET ....: (404) Product not found for this application.
        }
        // success

exit;

    }
    public function IosReceiptValidate(Request $request,$email) {
        $url = "https://buy.itunes.apple.com/verifyReceipt"; // Production
        // $url = "https://sandbox.itunes.apple.com/verifyReceipt"; // Sandbox
        $client = new \GuzzleHttp\Client();
        $user = User::where(['email'=>$email,'is_deleted'=>0])->first();
            if($user) {
                $subscription = $this->getUserSubscription($user->id);

                $receipt_data = json_decode($subscription->receipt);
                if(isset($receipt_data->purchaseToken)) {
                    return "This is android user";
                    exit;
                }
                else {
                  $receipt_data = $subscription->receipt;
                }
            }
            else {
                $receipt_data = 'MIIVoAYJKoZIhvcNAQcCoIIVkTCCFY0CAQExCzAJBgUrDgMCGgUAMIIFQQYJKoZIhvcNAQcBoIIFMgSCBS4xggUqMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEOAgEBBAQCAgChMA0CAQMCAQEEBQwDMS4yMA0CAQoCAQEEBRYDMTIrMA0CAQ0CAQEEBQIDAiU4MA0CARMCAQEEBQwDMS4wMA4CAQECAQEEBgIEXCQ86zAOAgEJAgEBBAYCBFAyNTYwDgIBCwIBAQQGAgQHR8M0MA4CARACAQEEBgIEMjcIRzAQAgEPAgEBBAgCBlSl4SNx4jAUAgEAAgEBBAwMClByb2R1Y3Rpb24wGAIBBAIBAgQQg/0LtfmgNEgmOp3q9iFsDDAcAgEFAgEBBBS1EkB11ZZMBzRjDAnt/vD54/Yn8DAdAgECAgEBBBUME2NvbS5jaGlsbGluZy5pb3NhcHAwHgIBCAIBAQQWFhQyMDIxLTA2LTExVDAwOjI1OjE0WjAeAgEMAgEBBBYWFDIwMjEtMDYtMTFUMDA6MjU6MTRaMB4CARICAQEEFhYUMjAyMS0wNC0yMlQwNzoyNjowNlowOQIBBwIBAQQxWlrhY+bItOnUUVoi39jiXWp/PEVZ90k72eCPLuPLOBTDW8AtTJjK+f+F7cckTKlA8zBFAgEGAgEBBD0t6iJV2lvTpt4yqfLAx2BKCaLT2RDZCENHZ8U0m+1npHjufW7jDrJeHnAnz/bCjtPDANmtYH3PHZHQRiKgMIIBjwIBEQIBAQSCAYUxggGBMAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMAwCAga6AgEBBAMCAQAwDwICBq4CAQEEBgIEXCQ94zASAgIGrwIBAQQJAgcB4ghRORY3MBoCAganAgEBBBEMDzUzMDAwMDc4ODgyOTg2MjAaAgIGqQIBAQQRDA81MzAwMDA3ODg4Mjk4NjIwHgICBqYCAQEEFQwTY29tLjFtb250aHVubGltaXRlZDAfAgIGqAIBAQQWFhQyMDIxLTA1LTA2VDIzOjU2OjI3WjAfAgIGqgIBAQQWFhQyMDIxLTA1LTA2VDIzOjU2OjI4WjAfAgIGrAIBAQQWFhQyMDIxLTA2LTA2VDIzOjU2OjI3WjCCAY8CARECAQEEggGFMYIBgTALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgaxAgEBBAMCAQEwDAICBrcCAQEEAwIBADAMAgIGugIBAQQDAgEAMA8CAgauAgEBBAYCBFwkPeMwEgICBq8CAQEECQIHAeIIUTkWODAaAgIGpwIBAQQRDA81MzAwMDA4MDk2Njc3MDMwGgICBqkCAQEEEQwPNTMwMDAwNzg4ODI5ODYyMB4CAgamAgEBBBUME2NvbS4xbW9udGh1bmxpbWl0ZWQwHwICBqgCAQEEFhYUMjAyMS0wNi0wOFQyMTo0NTozMlowHwICBqoCAQEEFhYUMjAyMS0wNS0wNlQyMzo1NjoyOFowHwICBqwCAQEEFhYUMjAyMS0wNi0xMVQyMTo0NTozMlqggg5lMIIFfDCCBGSgAwIBAgIIDutXh+eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc+B/SWigVvWh+0j2jMcjuIjwKXEJss9xp/sSg1Vhv+kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4+3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7+mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5+givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3+Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC+RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB+GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN+DAb2yriphcrGvzcNFMI+jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz+6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u+y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn+9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t+2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti+9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb+aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU+12TZ/wYdV2aeZuTJC+9jVcZ5+oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBACQhSLLgzlJt/DUI+nVxM0VdNzJ7b2k6t1lMaPfgWL7urckKdBufDH9w8/+YqaCornwdZcRlMSEn1Hdy8FlntMdSGsDqFf6rNg3HXUMptzvAfpnz9CPwnpVsFsakXLeIwrUqUhRcbEUOJTrgvr0qGlHDrlhq/LhsCj0no4BKVu5YUh3RsEK6idXB3bqzqzNzH2VJrl1KldIkzEIm4YfkXv7p0B6Fl8LJqUHH3GLdhRe5TslOtnTC6iM+Hv3wzdgNhG7F57RctbXZv6q9pRiVw7EemxniAuS1Q2HAv708smCVJFkLVadGvwHq1PTLya1Zs3KdbGxiWC04mTjmeMJZmz8='; // live receipt
            }
        // $receipt_data = 'MIIUBgYJKoZIhvcNAQcCoIIT9zCCE/MCAQExCzAJBgUrDgMCGgUAMIIDpwYJKoZIhvcNAQcBoIIDmASCA5QxggOQMAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgELAgEBBAMCAQAwCwIBDgIBAQQDAgFqMAsCAQ8CAQEEAwIBADALAgEQAgEBBAMCAQAwCwIBGQIBAQQDAgEDMAwCAQoCAQEEBBYCNCswDQIBAwIBAQQFDAMxLjAwDQIBDQIBAQQFAgMB1rUwDQIBEwIBAQQFDAMxLjAwDgIBCQIBAQQGAgRQMjU2MBgCAQQCAQIEENOvWM7MY37MFofco65fFkgwGwIBAAIBAQQTDBFQcm9kdWN0aW9uU2FuZGJveDAcAgEFAgEBBBQB49HFdGuB79X0Ex1vX8OC2UQn/TAdAgECAgEBBBUME2NvbS5jaGlsbGluZy5pb3NhcHAwHgIBDAIBAQQWFhQyMDIxLTA1LTI2VDA1OjQwOjQzWjAeAgESAgEBBBYWFDIwMTMtMDgtMDFUMDc6MDA6MDBaMEoCAQcCAQEEQoL3/VgDqxzDvPEniIhaC68H6NuZiZ4tcHh0aqea7py43R39l9jIn3CWlNE8grdh8PkW+wQXzZtT/iyuZaz/RQwE2jBZAgEGAgEBBFEjv5JRPPs/Dw+A5Mk2clraXwfgWRg5zkiGJJ7CGUuwdmw2AWcb8xi+EMHid/b0pssjUejrsZj1dTvjWoeM1drPi+dkH+ahzVkni4Zb6qc2iyUwggGAAgERAgEBBIIBdjGCAXIwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6ohLs/MBsCAganAgEBBBIMEDEwMDAwMDA4MTcxMDMxMjQwGwICBqkCAQEEEgwQMTAwMDAwMDgxNzEwMzEyNDAeAgIGpgIBAQQVDBNjb20uMW1vbnRodW5saW1pdGVkMB8CAgaoAgEBBBYWFDIwMjEtMDUtMjZUMDU6NDA6NDFaMB8CAgaqAgEBBBYWFDIwMjEtMDUtMjZUMDU6NDA6NDNaMB8CAgasAgEBBBYWFDIwMjEtMDUtMjZUMDU6NDU6NDFaoIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQBG8scm/ZNhej0CQnbt7EHMOTxN+Mfm8kGT+lU3ZTyowanV7YAYmT5/4PQGNkKRhviBgeQYXSUZMLs279wO0oUBKuwj7chtPoYFZP8DTUz4FiHaieCI6+xhTs72GGs87+Cwx1sKmcGIFEnSlpUgK8jcQdLX9NJhEluSFxLzno5sK3e69wZt0cgEUQmgFH8/3m6zXwX+b2j4kUagEEy1fjjPlF6UyXKzwe/VbTI/rL+JU4mIIswZuPReaUuRyAsoRCLJt9TfRhE8SGesYVVmZ3TwYQqFrnRCuw1XhcVi6OX78ALdjVfSsG2oEOcO48gl04ateCzoh+0f3jASp9i54ana'; // sandbox receipt
        // $response = $client->request('POST', $endpoint, ['query' => [
        //     'receipt-data' => $receipt_data,
        //     'password' => env('IOS_SECRET'),
        // ]]);


    // Make Post Fields Array
    $data = [
        'receipt-data' => $receipt_data,
        'password' => env('IOS_SECRET'),
    ];

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30000,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array(
            // Set here requred headers
            "accept: */*",
            "accept-language: en-US,en;q=0.8",
            "content-type: application/json",
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $response = json_decode($response,true);
    echo "<pre>";
    print_r($response);
    if($response && $response['status'] == 0) {
        $expire_date =  date("Y-m-d h:i:s",strtotime($response['latest_receipt_info'][0]['expires_date']));
        echo "expire_date >> ".   $expire_date = date('Y-m-d h:i:s A', $response['latest_receipt_info'][0]['expires_date_ms']/1000. - date("Z"));

        echo "<br>today_date >> ".$today_date = date("Y-m-d h:i:s A");
        echo "<br>milliseconds >> ".$milliseconds = round(microtime(true) * 1000);;
        echo "<br>today_date >> ".date('Y-m-d h:i:s A', $milliseconds/1000. - date("Z"));
        $expiryTimeMillis = $response['latest_receipt_info'][0]['expires_date_ms'];
        $milliseconds = round(microtime(true) * 1000);

// echo 'Date test';
// var_dump($expire_date);
// var_dump($today_date);
// var_dump("timestamp expire ", strtotime($expire_date));
// var_dump("timestamp today ", strtotime($today_date));
echo '<br><br>';
        var_dump($expiryTimeMillis);
        var_dump(date('Y-m-d h:i:s', $response['latest_receipt_info'][0]['expires_date_ms']/1000. - date("Z")));
        var_dump($milliseconds);
        var_dump(date('Y-m-d h:i:s', $milliseconds/1000. - date("Z")));

        // if(strtotime($response['latest_receipt_info'][0]['expires_date']) > (time())) {
        if($expiryTimeMillis > $milliseconds) {
            echo "check >>> ";
        // if($expire_date > $today_date) {
        return 1;
        }
        else {
            return 0;
        }
    }
    else {
        return 0;
    }
    }
    public function receiptValidator(Request $request) {
        if($request) {
            echo "<pre>";
            dd($request);
        }



        exit;

    }
    public function iosReceiptValidator() {
        $validator = new iTunesValidator(iTunesValidator::ENDPOINT_PRODUCTION); // Or iTunesValidator::ENDPOINT_SANDBOX if sandbox testing

        // $receiptBase64Data = 'ewoJInNpZ25hdHVyZSIgPSAiQXBNVUJDODZBbHpOaWtWNVl0clpBTWlKUWJLOEVkZVhrNjNrV0JBWHpsQzhkWEd1anE0N1puSVlLb0ZFMW9OL0ZTOGNYbEZmcDlZWHQ5aU1CZEwyNTBsUlJtaU5HYnloaXRyeVlWQVFvcmkzMlc5YVIwVDhML2FZVkJkZlcrT3kvUXlQWkVtb05LeGhudDJXTlNVRG9VaFo4Wis0cFA3MHBlNWtVUWxiZElWaEFBQURWekNDQTFNd2dnSTdvQU1DQVFJQ0NHVVVrVTNaV0FTMU1BMEdDU3FHU0liM0RRRUJCUVVBTUg4eEN6QUpCZ05WQkFZVEFsVlRNUk13RVFZRFZRUUtEQXBCY0hCc1pTQkpibU11TVNZd0pBWURWUVFMREIxQmNIQnNaU0JEWlhKMGFXWnBZMkYwYVc5dUlFRjFkR2h2Y21sMGVURXpNREVHQTFVRUF3d3FRWEJ3YkdVZ2FWUjFibVZ6SUZOMGIzSmxJRU5sY25ScFptbGpZWFJwYjI0Z1FYVjBhRzl5YVhSNU1CNFhEVEE1TURZeE5USXlNRFUxTmxvWERURTBNRFl4TkRJeU1EVTFObG93WkRFak1DRUdBMVVFQXd3YVVIVnlZMmhoYzJWU1pXTmxhWEIwUTJWeWRHbG1hV05oZEdVeEd6QVpCZ05WQkFzTUVrRndjR3hsSUdsVWRXNWxjeUJUZEc5eVpURVRNQkVHQTFVRUNnd0tRWEJ3YkdVZ1NXNWpMakVMTUFrR0ExVUVCaE1DVlZNd2daOHdEUVlKS29aSWh2Y05BUUVCQlFBRGdZMEFNSUdKQW9HQkFNclJqRjJjdDRJclNkaVRDaGFJMGc4cHd2L2NtSHM4cC9Sd1YvcnQvOTFYS1ZoTmw0WElCaW1LalFRTmZnSHNEczZ5anUrK0RyS0pFN3VLc3BoTWRkS1lmRkU1ckdYc0FkQkVqQndSSXhleFRldngzSExFRkdBdDFtb0t4NTA5ZGh4dGlJZERnSnYyWWFWczQ5QjB1SnZOZHk2U01xTk5MSHNETHpEUzlvWkhBZ01CQUFHamNqQndNQXdHQTFVZEV3RUIvd1FDTUFBd0h3WURWUjBqQkJnd0ZvQVVOaDNvNHAyQzBnRVl0VEpyRHRkREM1RllRem93RGdZRFZSMFBBUUgvQkFRREFnZUFNQjBHQTFVZERnUVdCQlNwZzRQeUdVakZQaEpYQ0JUTXphTittVjhrOVRBUUJnb3Foa2lHOTJOa0JnVUJCQUlGQURBTkJna3Foa2lHOXcwQkFRVUZBQU9DQVFFQUVhU2JQanRtTjRDL0lCM1FFcEszMlJ4YWNDRFhkVlhBZVZSZVM1RmFaeGMrdDg4cFFQOTNCaUF4dmRXLzNlVFNNR1k1RmJlQVlMM2V0cVA1Z204d3JGb2pYMGlreVZSU3RRKy9BUTBLRWp0cUIwN2tMczlRVWU4Y3pSOFVHZmRNMUV1bVYvVWd2RGQ0TndOWXhMUU1nNFdUUWZna1FRVnk4R1had1ZIZ2JFL1VDNlk3MDUzcEdYQms1MU5QTTN3b3hoZDNnU1JMdlhqK2xvSHNTdGNURXFlOXBCRHBtRzUrc2s0dHcrR0szR01lRU41LytlMVFUOW5wL0tsMW5qK2FCdzdDMHhzeTBiRm5hQWQxY1NTNnhkb3J5L0NVdk02Z3RLc21uT09kcVRlc2JwMGJzOHNuNldxczBDOWRnY3hSSHVPTVoydG04bnBMVW03YXJnT1N6UT09IjsKCSJwdXJjaGFzZS1pbmZvIiA9ICJld29KSW05eWFXZHBibUZzTFhCMWNtTm9ZWE5sTFdSaGRHVXRjSE4wSWlBOUlDSXlNREV5TFRBMExUTXdJREE0T2pBMU9qVTFJRUZ0WlhKcFkyRXZURzl6WDBGdVoyVnNaWE1pT3dvSkltOXlhV2RwYm1Gc0xYUnlZVzV6WVdOMGFXOXVMV2xrSWlBOUlDSXhNREF3TURBd01EUTJNVGM0T0RFM0lqc0tDU0ppZG5KeklpQTlJQ0l5TURFeU1EUXlOeUk3Q2draWRISmhibk5oWTNScGIyNHRhV1FpSUQwZ0lqRXdNREF3TURBd05EWXhOemc0TVRjaU93b0pJbkYxWVc1MGFYUjVJaUE5SUNJeElqc0tDU0p2Y21sbmFXNWhiQzF3ZFhKamFHRnpaUzFrWVhSbExXMXpJaUE5SUNJeE16TTFOems0TXpVMU9EWTRJanNLQ1NKd2NtOWtkV04wTFdsa0lpQTlJQ0pqYjIwdWJXbHVaRzF2WW1Gd2NDNWtiM2R1Ykc5aFpDSTdDZ2tpYVhSbGJTMXBaQ0lnUFNBaU5USXhNVEk1T0RFeUlqc0tDU0ppYVdRaUlEMGdJbU52YlM1dGFXNWtiVzlpWVhCd0xrMXBibVJOYjJJaU93b0pJbkIxY21Ob1lYTmxMV1JoZEdVdGJYTWlJRDBnSWpFek16VTNPVGd6TlRVNE5qZ2lPd29KSW5CMWNtTm9ZWE5sTFdSaGRHVWlJRDBnSWpJd01USXRNRFF0TXpBZ01UVTZNRFU2TlRVZ1JYUmpMMGROVkNJN0Nna2ljSFZ5WTJoaGMyVXRaR0YwWlMxd2MzUWlJRDBnSWpJd01USXRNRFF0TXpBZ01EZzZNRFU2TlRVZ1FXMWxjbWxqWVM5TWIzTmZRVzVuWld4bGN5STdDZ2tpYjNKcFoybHVZV3d0Y0hWeVkyaGhjMlV0WkdGMFpTSWdQU0FpTWpBeE1pMHdOQzB6TUNBeE5Ub3dOVG8xTlNCRmRHTXZSMDFVSWpzS2ZRPT0iOwoJImVudmlyb25tZW50IiA9ICJTYW5kYm94IjsKCSJwb2QiID0gIjEwMCI7Cgkic2lnbmluZy1zdGF0dXMiID0gIjAiOwp9';
        // $receiptBase64Data = 'MIIUNQYJKoZIhvcNAQcCoIIUJjCCFCICAQExCzAJBgUrDgMCGgUAMIID1gYJKoZIhvcNAQcBoIIDxwSCA8MxggO/MAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEOAgEBBAQCAgDhMA0CAQMCAQEEBQwDMS4wMA0CAQoCAQEEBRYDMTIrMA0CAQ0CAQEEBQIDAiRyMA0CARMCAQEEBQwDMS4wMA4CAQECAQEEBgIEXCQ86zAOAgEJAgEBBAYCBFAyNTYwDgIBCwIBAQQGAgQHR8M0MA4CARACAQEEBgIEMjLohTAQAgEPAgEBBAgCBkRJkf8kBzAUAgEAAgEBBAwMClByb2R1Y3Rpb24wGAIBBAIBAgQQdVpebXMKe2hUcXPuKJSX5zAcAgEFAgEBBBRi8CE8+CAZp+FtOIflndGg7yToHzAdAgECAgEBBBUME2NvbS5jaGlsbGluZy5pb3NhcHAwHgIBCAIBAQQWFhQyMDIxLTA1LTE4VDExOjA4OjE5WjAeAgEMAgEBBBYWFDIwMjEtMDUtMThUMTE6MDg6MTlaMB4CARICAQEEFhYUMjAyMS0wNS0xOFQxMTowNTo0MlowUQIBBwIBAQRJh816mletweLo4huOtJMf0nPLuCdzF19hoVW6Pqyfn/S/ESNh4HtGYaUJkI34Zc8OHDA7oUTnloF9lhjQAKMuagchOgkCKjBnfjBVAgEGAgEBBE0CSLrJnrv4ntnzK3CzZ+jfYzIAHqlf2OX4lnaNbn0obDpnTNza7NEYd3mlU7ktGDkANJ5qzA62faQ5kDja83fvSQcXxddNArf2lTUD6zCCAY8CARECAQEEggGFMYIBgTALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADAMAgIGugIBAQQDAgEAMA8CAgauAgEBBAYCBFwkPeMwEgICBq8CAQEECQIHAT5S0J8dczAaAgIGpwIBAQQRDA8zNTAwMDA5MDI4MTc5MTYwGgICBqkCAQEEEQwPMzUwMDAwOTAyODE3OTE2MB4CAgamAgEBBBUME2NvbS4xbW9udGh1bmxpbWl0ZWQwHwICBqgCAQEEFhYUMjAyMS0wNS0xOFQxMTowODoxNVowHwICBqoCAQEEFhYUMjAyMS0wNS0xOFQxMTowODoxOFowHwICBqwCAQEEFhYUMjAyMS0wNi0xOFQxMTowODoxNVqggg5lMIIFfDCCBGSgAwIBAgIIDutXh+eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc+B/SWigVvWh+0j2jMcjuIjwKXEJss9xp/sSg1Vhv+kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4+3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7+mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5+givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3+Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC+RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB+GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN+DAb2yriphcrGvzcNFMI+jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz+6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u+y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn+9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t+2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti+9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb+aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU+12TZ/wYdV2aeZuTJC+9jVcZ5+oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBAGCY5xi+oJe21HiHvRtKrQHLLblu/JppPIHJ74B7/plroq4iRyWWBxHtEeHKkzItiowqxFzRnS7BBvabk9vxXEUB4Lqu5Qi4WB9hcUWhJv5bfIIi/tO/yKo6VFWVfr+ZsV3puG9Z6N1STlBNN6M7gIjO5u5hyZOUMyrsvax0cISL7f9Af0nqJeHsp/n+6/S4FOwN9SdE4TUOi7f3h4U8j//K/LW+kSnMXtXz08lqlJzuFaEpp/Cc0G6U4BMKMHG09UkGcuhPwLIdEL2L4EJLVFbC7RFGPLbSJPgARqi59yU789r6Us/q6vIOs9Kc7AOyjl+rtOLnQPfhSbX/AxkUC9Y=';
        $receiptBase64Data = 'MIIUBgYJKoZIhvcNAQcCoIIT9zCCE/MCAQExCzAJBgUrDgMCGgUAMIIDpwYJKoZIhvcNAQcBoIIDmASCA5QxggOQMAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgELAgEBBAMCAQAwCwIBDgIBAQQDAgFqMAsCAQ8CAQEEAwIBADALAgEQAgEBBAMCAQAwCwIBGQIBAQQDAgEDMAwCAQoCAQEEBBYCNCswDQIBAwIBAQQFDAMxLjAwDQIBDQIBAQQFAgMB1rUwDQIBEwIBAQQFDAMxLjAwDgIBCQIBAQQGAgRQMjU2MBgCAQQCAQIEENOvWM7MY37MFofco65fFkgwGwIBAAIBAQQTDBFQcm9kdWN0aW9uU2FuZGJveDAcAgEFAgEBBBQB49HFdGuB79X0Ex1vX8OC2UQn/TAdAgECAgEBBBUME2NvbS5jaGlsbGluZy5pb3NhcHAwHgIBDAIBAQQWFhQyMDIxLTA1LTI2VDA1OjQwOjQzWjAeAgESAgEBBBYWFDIwMTMtMDgtMDFUMDc6MDA6MDBaMEoCAQcCAQEEQoL3/VgDqxzDvPEniIhaC68H6NuZiZ4tcHh0aqea7py43R39l9jIn3CWlNE8grdh8PkW+wQXzZtT/iyuZaz/RQwE2jBZAgEGAgEBBFEjv5JRPPs/Dw+A5Mk2clraXwfgWRg5zkiGJJ7CGUuwdmw2AWcb8xi+EMHid/b0pssjUejrsZj1dTvjWoeM1drPi+dkH+ahzVkni4Zb6qc2iyUwggGAAgERAgEBBIIBdjGCAXIwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6ohLs/MBsCAganAgEBBBIMEDEwMDAwMDA4MTcxMDMxMjQwGwICBqkCAQEEEgwQMTAwMDAwMDgxNzEwMzEyNDAeAgIGpgIBAQQVDBNjb20uMW1vbnRodW5saW1pdGVkMB8CAgaoAgEBBBYWFDIwMjEtMDUtMjZUMDU6NDA6NDFaMB8CAgaqAgEBBBYWFDIwMjEtMDUtMjZUMDU6NDA6NDNaMB8CAgasAgEBBBYWFDIwMjEtMDUtMjZUMDU6NDU6NDFaoIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQBG8scm/ZNhej0CQnbt7EHMOTxN+Mfm8kGT+lU3ZTyowanV7YAYmT5/4PQGNkKRhviBgeQYXSUZMLs279wO0oUBKuwj7chtPoYFZP8DTUz4FiHaieCI6+xhTs72GGs87+Cwx1sKmcGIFEnSlpUgK8jcQdLX9NJhEluSFxLzno5sK3e69wZt0cgEUQmgFH8/3m6zXwX+b2j4kUagEEy1fjjPlF6UyXKzwe/VbTI/rL+JU4mIIswZuPReaUuRyAsoRCLJt9TfRhE8SGesYVVmZ3TwYQqFrnRCuw1XhcVi6OX78ALdjVfSsG2oEOcO48gl04ateCzoh+0f3jASp9i54ana';
        try {
        $response = $validator->setReceiptData($receiptBase64Data)->validate();
        // $sharedSecret = '1234...'; // Generated in iTunes Connect's In-App Purchase menu
        // $response = $validator->setSharedSecret($sharedSecret)->setReceiptData($receiptBase64Data)->validate(); // use setSharedSecret() if for recurring subscriptions
        } catch (Exception $e) {
        echo 'got error = ' . $e->getMessage() . PHP_EOL;
        }

        if ($response->isValid()) {
        echo 'Receipt is valid.' . PHP_EOL;
        echo 'Receipt data =<br> ' ;
        echo "<pre>";
        print_r($response->getReceipt()) . PHP_EOL;

        foreach ($response->getPurchases() as $purchase) {
            echo 'getProductId: ' . $purchase->getProductId() . PHP_EOL;
            echo 'getTransactionId: ' . $purchase->getTransactionId() . PHP_EOL;

            if ($purchase->getPurchaseDate() != null) {
            echo 'getPurchaseDate: ' . $purchase->getPurchaseDate()->toIso8601String() . PHP_EOL;
            }
        }
        } else {
        echo 'Receipt is not valid.' . PHP_EOL;
        echo 'Receipt result code = ' . $response->getResultCode() . PHP_EOL;
        }
    }
    

    public function andriodReciptValidatorCurl() {
        //https://stackoverflow.com/questions/41982496/check-google-android-subscription-status-on-backend
        $ch = curl_init();
        $TOKEN_URL = "https://accounts.google.com/o/oauth2/token";
        $VALIDATE_URL = "https://www.googleapis.com/androidpublisher/v3/applications/".
            $appid."/purchases/subscriptions/".
            $productID."/tokens/".$purchaseToken;

        $input_fields = 'refresh_token='.$refreshToken.
            '&client_secret='.$clientSecret.
            '&client_id='.$clientID.
            '&redirect_uri='.$redirectUri.
            '&grant_type=refresh_token';

        //Request to google oauth for authentication
        curl_setopt($ch, CURLOPT_URL, $TOKEN_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input_fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        $result = json_decode($result, true);

        if (!$result || !$result["access_token"]) {
         //error
         return;
        }

        //request to play store with the access token from the authentication request
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$VALIDATE_URL."?access_token=".$result["access_token"]);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        if (!$result || $result["error"] != null) {
            //error
            return;
        }

        $expireTime = date('Y-m-d H:i:s', $result["expiryTimeMillis"]/1000. - date("Z"));
        //You get the purchase expire time, for example 2017-02-22 09:16:42
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
