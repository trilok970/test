<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Category;
use App\SubCategory;
use Validator;
use DB;
use Session;
use App\Passport;
use App\Channel;
use App\Playlist;
use App\Audio;
use App\Mystory;
use Hash;
use App\AdminSetting;
use App\Banner;
use App\Subscriptionplan;
use App\UserSubscription;
use Illuminate\Support\Facades\Password;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UserExport;
use App\Exports\UserAll;
use App\Http\Controllers\UserFcmTokenController;
use App\Jobs\NotificationJob;
use App\Notification;
use App\NotificationTemp;
use App\NotificationTempEntry;
use Exception;
use Rap2hpoutre\FastExcel\FastExcel;
use Artisan;

class UserContoller extends Controller
{
    public $fcm;
    public $paginate_no;

  public function __construct()
    {
        $this->paginate_no = config('constants.paginate_no');
        $this->fcm = new UserFcmTokenController;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reset_password(Request $request)
    {

        $credentials = request()->validate([
            'email' => 'required|email',
            'token' => 'required|string',
            'password' => 'required|string|confirmed|min:8|max:20'
        ]);

// echo "<pre>";
//         dd(request());
//         exit;
        $reset_password_status = Password::reset($credentials, function ($user, $password) {
            $user->password = Hash::make($password);
            $user->save();

            User::where(['email'=>$user->email])->where('id','!=',1)->update(['password'=>$user->password]);

        });
        $status=1;
        if ($reset_password_status == Password::INVALID_TOKEN) {
            $status=0;
            // return response()->json(["status"=>false,"message" => "Invalid token provided"], 400);
            $message = "Invalid token provided.";
            return redirect('success')->with('error_message',$message);
        }

        // return response()->json(["status"=>true,"message" => "Password has been successfully changed"]);
        $message = "Password has been changed successfully.";
        return redirect('success')->with('message',$message);
    }
    public function success_message()
    {
        return view('auth.success');
    }
    public function login()
    {
        return view('auth.login');
    }
    public function do_login(Request $request)
    {
        // echo "<pre>";
        // print_r($_POST);exit;
        $validator = Validator::make($request->all(),[
            'email'=>'required',
            'password'=>'required',
        ]);
        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }
        $count = User::where(['email'=>$request->email])->count();

        if($count > 0)
        {
            if(Auth::attempt(['email'=>$request->email,'password'=>$request->password,'role'=>'admin']))
            {
                return redirect('admin/dashboard');
                // return redirect('admin/dashboard');
            }
            else
            {
                return back()->with('error_message','Email or password are incorrect');
            }
        }
        else
            {
                return back()->with('error_message','Email or password are incorrect');
            }
    }
    public function logout()
    {
        Auth::logout();
        return redirect('admin');
    }
    public function index(Request $request)
    {

        // $users = User::where(['is_deleted'=>0,'status'=>1,'notification'=>1])
        // ->whereRaw("(device_token !='' and device_token is not null)")
        // ->take(3)->skip(0)
        // // ->whereBetween('id',[3464,3464])
        // ->get(['id','is_deleted','status','device_token','notification','device_type']);

        // $usersIds = $users->pluck('id')->toArray();
        // $notificationCount = Notification::where('notification_temp_entriy_id',1)->whereIn('user_id',$usersIds)->count();

        // echo count($usersIds);
        // if(empty($usersIds)){
        //     echo "<br>empty";
        // }
        // if(!empty($users)){
        //     echo "<br>not empty";
        // }
        // echo "<pre>";
        //     print_r($usersIds);exit;
        // exit;
            // $users = UserSubscription::select('user_ids')
            //         ->whereBetween('created_at',['2021-05-13','2021-05-14'])
            //         ->whereHas('user',function($q){
            //             $q->where('device_type','android');
            //         })
            //         ->get();
            //         exit;
        //
        // echo $milliseconds = round(microtime(true) * 1000);
        // User::where('social_ids','!=',null)->where('social_type','!=',null)->update(['email_verified_at'=>date('Y-m-d h:i:s')]);
        // exit;
        $verified = 0;
        if($request->verified && $request->verified == 1) {
            $verified = $request->verified;
        }

        if($request->query())
        {
            $search = trim($request->search);
            $users = User::where(['is_deleted'=>0])->where('role','!=','admin')
            ->where(function($query) use ($search){
                $query->orWhere('id','like','%'.$search.'%');
                $query->orWhere('fullname','like','%'.$search.'%');
                $query->orWhere('phone_number','like','%'.$search.'%');
                $query->orWhere('email','like','%'.$search.'%');
                $query->orWhere('created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
            })
            ->when($verified,function($q) use ($verified) {
                $q->where('email_verified_at','!=',null);
            })
            ->orderByDesc('id')
            ->paginate($this->paginate_no);
        }
        else
        {
            $users = User::where(['is_deleted'=>0])->where('role','!=','admin')
            ->when($verified,function($q) use ($verified) {
                $q->where('email_verified_at','!=',null);
            })
            ->orderByDesc('id')
            ->paginate($this->paginate_no);
            // echo "<pre>";
            // print_r($users);exit;
            $search = '';
        }

        return view('admin/user.index',compact('users','search'));
    }

    public function userExport(Request $request)
    {
        $verified = 0;
        if($request->verified && $request->verified == 1) {
            $verified = $request->verified;
        }
        $users =  User::select('id','fullname','country_code','phone_number','email','created_at','profile_pic','social_type',DB::raw("if(status = 1,'Active','Inactive') as status"),'subscription_plan_admin','expire_mili_seconds')
        ->where(['is_deleted'=>0])
        ->where('role','!=','admin')
        ->when($verified,function($q) use ($verified) {
            $q->where('email_verified_at','!=',null);
        })
        ->orderBy('id','desc')->cursor();



            function usersGenerator($users) {
            foreach ($users as $user) {
                yield $user;
            }
            }
            return (new FastExcel(usersGenerator($users)))->download('users.xlsx', function ($user) {
                $milliseconds = round(microtime(true) * 1000);

                        if($user->subscription_plan_admin == 1){
                            $subcription = "Active";
                        }
                        else if($user->expire_mili_seconds != null || $user->expire_mili_seconds != '') {
                            if($user->expire_mili_seconds > $milliseconds) {
                                $subcription = "Active";
                            }
                            else {
                                $subcription = "Inactive";
                            }
                        }
                        else {
                            $subcription = "Inactive";
                        }


                        if($user->phone_number == "" || $user->phone_number == 'null') {
                            $phone_number = "";
                        }
                        else {
                            $phone_number = "(".$user->country_code.") ".$user->phone_number;
                        }

                return [
                        'Name' => $user->fullname,
                        'Phone Number' => $phone_number,
                        'Email' => $user->email,
                        'Created On' => date("d-M-Y",strtotime($user->created_at)),
                        'User singned in as' => $user->social_type ?? "Web",
                        'Profile Pic' => $user->profile_pic,
                        'Subscription' => $subcription,
                        'Status' => $user->status,
                    ];
            });

    }
    public function userExcelExport($verified) {



        $milliseconds = round(microtime(true) * 1000);


        // $data[0][0] = 'Sr. No.';
        // $data[0][1] = 'Name';
        // $data[0][2] = 'Phone Number';
        // $data[0][3] = 'Email';
        // $data[0][4] = 'Created On';
        // $data[0][5] = 'User singned in as';
        // $data[0][6] = 'Profile Pic';
        // $data[0][7] = 'Subscription';
        // $data[0][8] = 'Status';
        // $j=1;
        // foreach($users as $user)
        // {
        //     if($user->subscription_plan_admin == 1){
        //         $subcription = "Active";
        //     }
        //     else if($user->expire_mili_seconds != null || $user->expire_mili_seconds != '') {
        //         if($user->expire_mili_seconds > $milliseconds) {
        //             $subcription = "Active";
        //         }
        //         else {
        //             $subcription = "Inactive";
        //         }
        //     }
        //     else {
        //         $subcription = "Inactive";
        //     }
        //     // if($user->is_premium == 1) {
        //     //     $subcription = "Active";
        //     // }
        //     // else {
        //     //     $subcription = "Inactive";
        //     // }

        //     if($user->phone_number == "" || $user->phone_number == 'null') {
        //         $phone_number = "";
        //     }
        //     else {
        //         $phone_number = "(".$user->country_code.") ".$user->phone_number;
        //     }

        //     $data[$j][0] = $j;
        //     $data[$j][1] = $user->fullname;
        //     $data[$j][2] = $phone_number;
        //     $data[$j][3] = $user->email;
        //     $data[$j][4] = date("d-M-Y",strtotime($user->created_at));
        //     $data[$j][5] = $user->social_type ?? "Web";
        //     $data[$j][6] = $user->profile_pic;
        //     $data[$j][7] = $subcription;
        //     $data[$j][8] = $user->status;

        //     $j++;
        // }
    }
    public function notificationShow()
    {

        return view('admin.user.notification');
    }
    public function notificationEntry($title,$message,$type,$image) {
        $notificationEntry = new NotificationTempEntry;
        $notificationEntry->title = $title;
        $notificationEntry->message = $message;
        $notificationEntry->type = $type;
        $notificationEntry->image = $image;
        $notificationEntry->save();

       return $notificationEntry->id;
    }
    public function notificationTempEntry($notification_id) {

        $notificationTemp = new NotificationTemp;
        $notificationTemp->notification_id = $notification_id;
        $notificationTemp->user_id_from = 0;
        $notificationTemp->user_id_to = 0;
        $notificationTemp->save();
    }
    public function notificationSend(Request $request)
    {
        $credentials = request()->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
        if ($request->hasFile('image') && $request->image->isValid()) {
            $credentials = request()->validate([
                'image' => 'required|mimes:jpeg,jpg,gif,png|max:2048',
            ]);

            $fileName  =  image_upload($request->image,"images",'public');

        } else {
            $fileName  = 'images/1615808524.png';
        }
        $file = env('AWS_URL')."/".$fileName;

        $notification_id = $this->notificationEntry($request->title,$request->description,"banner",$file);

        // $this->fcm->sendNotificationToAllUsers(ucwords($request->title),$request->description,"banner",$file);
        // $users = User::where(['is_deleted'=>0,'status'=>1,'notification'=>1])->whereRaw("(device_token !='' and device_token is not null)")->whereBetween('id',[3464,3464])->get();
        $users = User::where(['is_deleted'=>0,'status'=>1,'notification'=>1])->whereRaw("(device_token !='' and device_token is not null)")->limit(10000)->get(['id','is_deleted','status','device_token','notification','device_type']);

        if($users){
            $this->notificationTempEntry($notification_id);
        }

        //  echo "<pre>";
        //  print_r($users);
        //  dd();
        //  exit;
        // NotificationJob::dispatch(ucwords($request->title),$request->description,"banner",$file,$users);
        // Artisan::call('notification:crone');
        // Artisan::call('queue:work');
        return back()->with('message','Notification successfully send to all users.');

    }
    protected function user_subscription_entry($user_id)
    {
        $from_date =  date("Y-m-d");
        $count = UserSubscription::where(['user_id'=>$user_id,'is_admin'=>1,'is_deleted'=>0])->count();
        if($count == 0)
        {
            $user_subscription = new UserSubscription;
            $user_subscription->user_id = $user_id;
            $user_subscription->plan_id = 0;
            $user_subscription->amount = 0;
            $user_subscription->transaction_id = 0;
            $user_subscription->currency_name = "USD";
            $user_subscription->from_date = $from_date;
            $user_subscription->to_date =  date("2099-01-01");
            $user_subscription->is_admin =  1;
            $user_subscription->save();
        }

    }

    public function userSubscribe($id)
    {
        $user = User::find($id);
        if($user)
        {
            // echo $user->is_premium;
            // echo "<br>".$user->subscription_plan_admin;exit;
            if($user->is_premium==0 && $user->subscription_plan_admin==0)
            {
                $user->subscription_plan_last_date = date("2099-01-01");
                $user->subscription_plan_admin = 1;
                $user->save();
                $this->user_subscription_entry($user->id);
                $data['result'] = 1;
                $data['msg'] = 'User subscribed successfully.';
                $data['url'] = url('admin/user-unsubscribe/'.$user->id);
                $data['removeClass'] = 'btn-info';
                $data['addClass'] = 'btn-warning';
                $data['removeClass1'] = 'user-subscribe';
                $data['addClass1'] = 'user-unsubscribe';
                $data['title'] = 'Subscribed User';
                $data['activeButton'] = '<span class="badge badge-success">Active</span>';



                return response()->json($data);
            }
            else
            {
                $data['result'] = 0;
                $data['msg'] = 'Something went wrong.';
                return response()->json($data);
            }

        }
        else{
            $data['result'] = 0;
            $data['msg'] = 'User not found.';
            return response()->json($data);
        }
    }
    public function userUnsubscribe(Request $request,$id)
    {
        $user = User::find($id);
        if($user)
        {
            if($user->is_premium==1 && $user->subscription_plan_admin==1)
            {
            $user->subscription_plan_last_date = date("1970-01-01");
            $user->subscription_plan_admin = 0;
            $user->save();
            $data['result'] = 1;
            $data['msg'] = 'User unsubscribed successfully.';
            $data['url'] = url('admin/user-subscribe/'.$user->id);
            $data['removeClass'] = 'btn-warning';
            $data['addClass'] = 'btn-info';
            $data['removeClass1'] = 'user-unsubscribe';
            $data['addClass1'] = 'user-subscribe';
            $data['title'] = 'Unsubscribed User';
            $data['activeButton'] = '<span class="badge badge-danger">Inactive</span>';

            return response()->json($data);
            }
            else
            {
                $data['result'] = 0;
                $data['msg'] = 'Something went wrong.';
                return response()->json($data);
            }
        }
        else{
            $data['result'] = 0;
            $data['msg'] = 'User not found.';
            return response()->json($data);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
       if($user->delete())
       {
        // $token =  DB::table('oauth_access_tokens')
        //     ->where('user_id', $user->id)
        //     ->update(['revoked'=>1]);
        return back()->with('message','User Deleted Successfully');
       }
       else
        return back()->with('message','Something Went Wrong');

    }

    public function update_status(Request $request)
    {

        if($request->type=="channel")
        {
        $banner = Channel::where('id',$request->id)->first();
        $banner->status = $request->status;
        // $this->send_update_notification("banner");
        }
        else if($request->type=="category")
        {
            $banner = Category::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("category");
        }
        else if($request->type=="subcategory")
        {
            $banner = Subcategory::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("subcategory");
        }
        else if($request->type=="playlist")
        {
            $banner = Playlist::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("innercategory");
        }
        else if($request->type=="audio")
        {
            $banner = Audio::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("product");
        }
        else if($request->type=="story")
        {
            $banner = Mystory::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("product");
        }
        else if($request->type=="subscriptionplan")
        {
            $banner = Subscriptionplan::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("product");
        }
        else if($request->type=="banner")
        {
            $banner = Banner::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("product");
        }
        elseif($request->type=="users")
        {
        $banner = User::where('id',$request->id)->first();
        $banner->status = $request->status;

        if($request->status==0)
        {
            // $token =  DB::table('oauth_access_tokens')
            // ->where('user_id', $banner->id)
            // ->update(['revoked'=>1]);
            // print_r($token);
        }


        }
        if($banner->save())
        {

             // session()->put('success','Status change successfully');
            $data['status'] = 1;
            return response()->json($data);
        }
        else{
            session()->put('warning','Status not change successfully');
            $data['status'] = 0;
            return response()->json($data);
        }
    }


public function ajax(Request $request)
     {
        $data = '';
     if($request->type=='innercategory')
     {

        $subcategories = Innercategory::select('id','name')->where(['sub_category_id'=>$request->subcategory_id,'category_master_id'=>$request->category_master_id,'is_deleted'=>0])->get();
        $data .= '<option value="0">-Select-</option>';
     }

     else if($request->type=='subcategory')
     {
        $subcategories = SubCategory::select('id','name')->where(['category_id'=>$request->category_id,'is_deleted'=>0])->get();
        $data .= '<option value="0">-Select-</option>';
     }

       // print_r($subcategories);

      // exit;
      foreach($subcategories as $subcategorie)
      {
        $data .= '<option name="'.ucfirst($subcategorie->name).'" value="'.$subcategorie->id.'">'.$subcategorie->name.'</option>';

      }
     return ($data);



    }



    public function profile()
    {
        return view('admin.commons.coming-soon');
    }
    public function animatedBanner()
    {
        $setting = AdminSetting::find(1);
        return view('admin.commons.animated-banner',['setting'=>$setting]);
    }

    public function UpdateAnimatedBanner(Request $request)
    {
      $validator = validator::make($request->all(), [
            'animated_banner' => 'required|mimes:jpeg,jpg,gif,png,mp4|max:5048',
        ]);
        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }
      $setting = AdminSetting::find(1);

      if ($request->hasFile('animated_banner') && $request->animated_banner->isValid()) {
            $extension = $request->animated_banner->extension();
            $setting->animated_banner  =  image_upload($request->animated_banner,"images",'public');
            if($extension == "mp4") {
                $setting->animated_banner_type = "video";
            }
            else {
                $setting->animated_banner_type = "image";
            }
      }
      $setting->save();
      $message = "Banner successfully updated.";
        return redirect()->back()->with('message',$message);

    }
    public function loginRegisterBanner()
    {
        try{
            $setting = AdminSetting::where('id',1)->first();
        // echo "<pre>";
        // print_r($setting);
        // exit;
        return view('admin.commons.login-register-banner',['setting'=>$setting]);
        }
        catch(Exception $e) {
            echo 'Message: ' .$e->getMessage();
          }
    }
     public function ambianceAudio()
    {
        return view('admin.commons.ambiance-audio');
    }
    public function UpdateloginRegister(Request $request)
    {
      $validator = validator::make($request->all(), [
            'login_banner' => 'required|mimes:mp4|max:10048',
            'register_banner' => 'required|mimes:mp4|max:10048',
        ]);
        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }
      $setting = AdminSetting::find($request->id);

      if ($request->hasFile('login_banner') && $request->login_banner->isValid()) {
            $extension = $request->login_banner->extension();
            $setting->login_banner  =  image_upload($request->login_banner,"images",'public');
            if($extension == "mp4") {
                $setting->login_banner_type = "video";
            }
            else {
                $setting->login_banner_type = "image";

            }
      }
      if ($request->hasFile('register_banner') && $request->register_banner->isValid()) {
        $extension = $request->register_banner->extension();
        $setting->register_banner  =  image_upload($request->register_banner,"images",'public');
        if($extension == "mp4") {
            $setting->register_banner_type = "video";
        }
        else {
            $setting->register_banner_type = "image";

        }
        }
      $setting->save();
      $message = "Banner successfully updated.";
        return redirect()->back()->with('message',$message);

    }






    ////////////////////////////////////////////////////////////////////////////////////////////////
}
