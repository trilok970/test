<?php

namespace App\Http\Controllers;

use App\Content;
use Illuminate\Http\Request;
use Validator;
use Auth;
use DB;
class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $datas = Content::where(['is_deleted'=>0])->get();
        return view('admin/content.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin/content.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = validator::make($request->all(),[
        'title' => 'required',
        'sub_title' => 'required',
        'description' =>  'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        $content = new Content;

        if($request->hasFile('image') && $request->image->isValid())
        {
            $content->image  =  image_upload($request->image,"images",'public');
        }
        else
        {
            $content->image  ='default.jpg';
        }
        $content->title = ucfirst($request->title);
        $content->sub_title = $request->sub_title;
        $content->status = $request->status;
        $content->description = ($request->description);
        if($content->save())
        {
            return redirect('admin/content')->with('message','Content Added Successfully');
        }
        else
        {
            return back()->with('message','Content Not Added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function show(Content $content)
    {
        //
        return view('admin/content.show',compact('content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function edit(Content $content)
    {
        //
         // $Content = Content::find($id);
        // print_r($Content);
        return view('admin/content.edit',compact('content'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Content $content)
    {
        //
        $validator = validator::make($request->all(),[
        'title' => 'required',
        'sub_title' => 'required',
        'description' =>  'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        if($request->hasFile('image') && $request->image->isValid())
        {
            $content->image  =  image_upload($request->image,"images",'public');
        }

        $content->title = ucfirst($request->title);
        $content->sub_title = $request->sub_title;
        $content->status = $request->status;
        $content->description = ($request->description);
        if($content->save())
        {
            return redirect('admin/content')->with('message','Content Updated Successfully');
        }
        else
        {
            return back()->with('message','Content Not Added');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function destroy(Content $content)
    {
        //
    }
}
