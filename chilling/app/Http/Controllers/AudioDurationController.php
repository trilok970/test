<?php

namespace App\Http\Controllers;

use App\AudioDuration;
use Illuminate\Http\Request;

class AudioDurationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AudioDuration  $audioDuration
     * @return \Illuminate\Http\Response
     */
    public function show(AudioDuration $audioDuration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AudioDuration  $audioDuration
     * @return \Illuminate\Http\Response
     */
    public function edit(AudioDuration $audioDuration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AudioDuration  $audioDuration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AudioDuration $audioDuration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AudioDuration  $audioDuration
     * @return \Illuminate\Http\Response
     */
    public function destroy(AudioDuration $audioDuration)
    {
        //
    }
}
