<?php

namespace App\Http\Controllers;

use App\Mystory;
use Illuminate\Http\Request;

class MystoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mystory  $mystory
     * @return \Illuminate\Http\Response
     */
    public function show(Mystory $mystory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mystory  $mystory
     * @return \Illuminate\Http\Response
     */
    public function edit(Mystory $mystory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mystory  $mystory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mystory $mystory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mystory  $mystory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mystory $mystory)
    {
        //
    }
}
