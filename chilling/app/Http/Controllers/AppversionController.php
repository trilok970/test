<?php

namespace App\Http\Controllers;

use App\Appversion;
use Illuminate\Http\Request;
use Validator;
class AppversionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $datas = Appversion::get();
        // return view('admin.appversion.index',compact('datas'));
        $appversion = Appversion::find(1);
        return view('admin.appversion.edit',compact('appversion'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('admin.appversion.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator::make($request->all(),[
            'android_version' => 'required',
            'ios_version' => 'required',
            'android_force_update' => 'required',
            'ios_force_update' => 'required',
            'android_maintenance' => 'required',
            'ios_maintenance' => 'required',
            'android_message' => 'required',
            'ios_message' => 'required',
            ]);
            if($validator->fails())
            {
                return back()
                ->withInput()
                ->withErrors($validator);
            }
            $appversion = new Appversion;
            $appversion->android_version = $request->android_version;
            $appversion->ios_version = $request->ios_version;
            $appversion->android_force_update = $request->android_force_update;
            $appversion->ios_force_update = $request->ios_force_update;
            $appversion->android_maintenance = $request->android_maintenance;
            $appversion->ios_maintenance = $request->ios_maintenance;
            $appversion->android_message = $request->android_message;
            $appversion->ios_message = $request->ios_message;
            if($appversion->save())
            {
                return redirect('admin/appversion')->with('message','Appversion added successfully');
            }
            else
            {
                return back()->with('message','Appversion not added');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appversion  $appversion
     * @return \Illuminate\Http\Response
     */
    public function show(Appversion $appversion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appversion  $appversion
     * @return \Illuminate\Http\Response
     */
    public function edit(Appversion $appversion)
    {

        return view('admin.appversion.edit',compact('appversion'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appversion  $appversion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appversion $appversion)
    {
        $validator = validator::make($request->all(),[
            'android_version' => 'required',
            'ios_version' => 'required',
            'android_force_update' => 'required',
            'ios_force_update' => 'required',
            'android_maintenance' => 'required',
            'ios_maintenance' => 'required',
            'android_message' => 'required',
            'ios_message' => 'required',
            ]);
            if($validator->fails())
            {
                return back()
                ->withInput()
                ->withErrors($validator);
            }


            $appversion->android_version = $request->android_version;
            $appversion->ios_version = $request->ios_version;
            $appversion->android_force_update = $request->android_force_update;
            $appversion->ios_force_update = $request->ios_force_update;
            $appversion->android_maintenance = $request->android_maintenance;
            $appversion->ios_maintenance = $request->ios_maintenance;
            $appversion->android_message = $request->android_message;
            $appversion->ios_message = $request->ios_message;
            if($appversion->save())
            {
                return redirect('admin/appversion')->with('message','Appversion updated successfully');
            }
            else
            {
                return back()->with('message','Appversion not updated');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appversion  $appversion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appversion $appversion)
    {
        //
    }
}
