<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Auth;
use Validator;
use DB;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // echo "string";dd();
         $datas = Category::where(['is_deleted'=>0])->orderByDesc('id')->get();
         return view('admin/category.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin/category.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function check_name($name)
    {
        return Category::where(['name'=>$name,'is_deleted'=>0])->count();
    }
    public function store(Request $request)
    {
        //
        $validator = validator::make($request->all(),[
        'name' => 'required',
        'description' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        if($this->check_name($request->name) > 0)
        {
            $validator = $validator->errors()->add('name','The name has already been taken.');
            return back()->withInput()->withErrors($validator);
        }
        $category = new Category;
        $category->name = ucfirst($request->name);
        $category->user_id = Auth::user()->id;
        $category->description = $request->description ?? "";
        if($category->save())
        {
            return redirect('admin/category')->with('message','Category added Successfully');
        }
        else
        {
            return back()->with('message','Category Not Added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
        return view('admin/category.edit',compact('category'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
        $validator = validator::make($request->all(),[
        'name' => 'required',
        'description' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }

        if($request->name != $category->name && $this->check_name($request->name) > 0)
        {
            $validator = $validator->errors()->add('name','The name has already been taken.');
            return back()->withInput()->withErrors($validator);
        }

        $category->name = ucfirst($request->name);
        $category->description = $request->description ?? "";
        if($category->save())
        {
            return redirect('admin/category')->with('message','Category Updated Successfully');
        }
        else
        {
            return back()->with('message','Category Not Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
        $category->is_deleted = 1;
        if($category->save())
        {
            return redirect('admin/category')->with('message','Category Deleted Successfully');
        }
        else
        {
            return back()->with('message','Category Not Deleted');
        }
    }
}
