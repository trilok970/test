<?php

namespace App\Http\Controllers;

use App\PlaylistCategory;
use Illuminate\Http\Request;

class PlaylistCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PlaylistCategory  $playlistCategory
     * @return \Illuminate\Http\Response
     */
    public function show(PlaylistCategory $playlistCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PlaylistCategory  $playlistCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(PlaylistCategory $playlistCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PlaylistCategory  $playlistCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PlaylistCategory $playlistCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PlaylistCategory  $playlistCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlaylistCategory $playlistCategory)
    {
        //
    }
}
