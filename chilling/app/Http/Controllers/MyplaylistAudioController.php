<?php

namespace App\Http\Controllers;

use App\MyplaylistAudio;
use Illuminate\Http\Request;

class MyplaylistAudioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MyplaylistAudio  $myplaylistAudio
     * @return \Illuminate\Http\Response
     */
    public function show(MyplaylistAudio $myplaylistAudio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MyplaylistAudio  $myplaylistAudio
     * @return \Illuminate\Http\Response
     */
    public function edit(MyplaylistAudio $myplaylistAudio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MyplaylistAudio  $myplaylistAudio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MyplaylistAudio $myplaylistAudio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MyplaylistAudio  $myplaylistAudio
     * @return \Illuminate\Http\Response
     */
    public function destroy(MyplaylistAudio $myplaylistAudio)
    {
        //
    }
}
