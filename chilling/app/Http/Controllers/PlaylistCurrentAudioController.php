<?php

namespace App\Http\Controllers;

use App\PlaylistCurrentAudio;
use Illuminate\Http\Request;

class PlaylistCurrentAudioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PlaylistCurrentAudio  $playlistCurrentAudio
     * @return \Illuminate\Http\Response
     */
    public function show(PlaylistCurrentAudio $playlistCurrentAudio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PlaylistCurrentAudio  $playlistCurrentAudio
     * @return \Illuminate\Http\Response
     */
    public function edit(PlaylistCurrentAudio $playlistCurrentAudio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PlaylistCurrentAudio  $playlistCurrentAudio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PlaylistCurrentAudio $playlistCurrentAudio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PlaylistCurrentAudio  $playlistCurrentAudio
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlaylistCurrentAudio $playlistCurrentAudio)
    {
        //
    }
}
