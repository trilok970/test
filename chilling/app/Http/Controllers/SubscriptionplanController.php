<?php

namespace App\Http\Controllers;

use App\Subscriptionplan;
use Illuminate\Http\Request;
use Auth;
use Validator;
use DB;
class SubscriptionplanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Subscriptionplan::where(['is_deleted'=>0])->orderByDesc('id')->get();
         return view('admin/subscriptionplan.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/subscriptionplan.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function check_name($title)
    {
        return Subscriptionplan::where(['title'=>$title,'is_deleted'=>0])->count();
    }
    public function store(Request $request)
    {
         $validator = validator::make($request->all(),[
        'title' => 'required',
        'price' => 'required',
        'feature' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        if($this->check_name($request->name) > 0)
        {
            $validator = $validator->errors()->add('name','The title has already been taken.');
            return back()->withInput()->withErrors($validator);
        }
        $subscriptionplan = new Subscriptionplan;
        $subscriptionplan->title = ucfirst($request->title);
        $subscriptionplan->price = $request->price ?? 0;
        $subscriptionplan->feature = $request->feature ?? "";
        if($subscriptionplan->save())
        {
            return redirect('admin/subscriptionplan')->with('message','Subscriptionplan added successfully');
        }
        else
        {
            return back()->with('message','Subscriptionplan not added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subscriptionplan  $subscriptionplan
     * @return \Illuminate\Http\Response
     */
    public function show(Subscriptionplan $subscriptionplan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subscriptionplan  $subscriptionplan
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscriptionplan $subscriptionplan)
    {
        return view('admin/subscriptionplan.edit',compact('subscriptionplan'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subscriptionplan  $subscriptionplan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subscriptionplan $subscriptionplan)
    {
         $validator = validator::make($request->all(),[
        'title' => 'required',
        'price' => 'required',
        'feature' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        if($request->title != $subscriptionplan->title && $this->check_name($request->name) > 0)
        {
            $validator = $validator->errors()->add('name','The title has already been taken.');
            return back()->withInput()->withErrors($validator);
        }
        $subscriptionplan->title = ucfirst($request->title);
        $subscriptionplan->price = $request->price ?? 0;
        $subscriptionplan->feature = $request->feature ?? "";
        if($subscriptionplan->save())
        {
            return redirect('admin/subscriptionplan')->with('message','Subscriptionplan updated successfully');
        }
        else
        {
            return back()->with('message','Subscriptionplan not updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subscriptionplan  $subscriptionplan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscriptionplan $subscriptionplan)
    {
         $subscriptionplan->is_deleted = 1;
        if($subscriptionplan->save())
        {
            return redirect('admin/subscriptionplan')->with('message','Subscriptionplan deleted successfully');
        }
        else
        {
            return back()->with('message','Subscriptionplan not deleted');
        }
    }
}
