<?php

namespace App\Http\Controllers;

use App\Playlist;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Channel;
use App\Category;
use App\SubCategory;
use App\PlaylistAudio;
use App\Song;
use App\Banner;
use App\Audio;
use App\PlaylistCategory;
use DB;
use App\Http\Controllers\UserFcmTokenController;

class PlaylistController extends Controller
{
    public $fcm;
    public $paginate_no;

  public function __construct()
    {
        $this->paginate_no = config('constants.paginate_no');
        $this->api_per_page = config('constants.api_per_page');
        $this->fcm = new UserFcmTokenController;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datas = Playlist::where(['is_deleted'=>0])->orderBy('id','desc')->orderBy('order_number','asc')->get();
        // echo "<pre>";
        // print_r($datas);
        // exit;
        $colors  = $this->colors();
        return view('admin/playlist.index',compact('datas','colors'))->with('get_category')->with('get_channel');
    }
    public function colors()
    {
     return $color = array("success","danger","warning","info","light","dark","secondary");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $channels = Channel::where(['is_deleted'=>0,'status'=>1])->get();
        $categories = Category::where(['is_deleted'=>0,'status'=>1])->get();
        $audios = Audio::where(['is_deleted'=>0,'status'=>1])->get();
        return view('admin/playlist.create',compact('channels','categories','audios'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function check_category_and_channel($channel_id,$category_id)
    {
        return Playlist::where(['is_deleted'=>0,'channel_id'=>$channel_id,'category_id'=>$category_id])->count();
    }
    protected function check_name($name)
    {
        return Playlist::where(['name'=>$name,'is_deleted'=>0])->count();
    }
    public function store(Request $request)
    {
       // return $request;
        // echo "<pre>";
        // print_r($_POST);
        // exit;

        //
        $validator = validator::make($request->all(),[
            'channel_id' => 'required',
            // 'category_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'audio_id' => 'required',
            'image' => 'required|mimes:jpeg,jpg,gif,png|max:2048',
            'thumbnail' => 'required|mimes:jpeg,jpg,gif,png|max:1048',
        ]);
        if($validator->fails())
        {
            // echo "<pre>";
            // print_r($validator->errors());
            // exit;
            return back()
            ->withInput()
            ->withErrors($validator);
        }
    // if($this->check_category_and_channel($request->channel_id,$request->category_id) > 0)
    //    {

    //     return back()->with('error_message','This channel is already exists with this category');
    //     exit;
    //    }
        if($this->check_name($request->name) > 0)
        {
            $validator = $validator->errors()->add('name','The title has already been taken.');
            return back()->withInput()->withErrors($validator);
        }
        $playlist = new Playlist;

        if($request->hasFile('image') && $request->image->isValid())
           {
                $playlist->image  =  image_upload($request->image,"images",'public');
            }
            else
            {
                $playlist->image  ='images/default.jpg';
            }
        if($request->hasFile('thumbnail') && $request->thumbnail->isValid())
           {
                $playlist->thumbnail  =  image_upload($request->thumbnail,"thumbnail",'public');
            }
            else
            {
                $playlist->thumbnail  ='thumbnail/default.jpg';
            }
       // DB::beginTransaction();

        $playlist->name = ucfirst($request->name);
        $playlist->description = $request->description ?? "";
        $playlist->user_id = Auth::user()->id;
        $playlist->channel_id = $request->channel_id ?? 0;
        $playlist->category_id =  0;
        if($playlist->save())
        {
            if($request->category_id)
            {
                foreach ($request->category_id as $category) {
                $Playlist_category = new PlaylistCategory;
                $Playlist_category->playlist_id = $playlist->id;
                $Playlist_category->category_id = $category;
                $Playlist_category->save();
            }
            }
            if($request->audio_id)
            {
                $audio_array = $request->audio_id;
                rsort($audio_array);
                $i = 1;
                foreach ($audio_array as $audio) {
                $playlistaudio = new PlaylistAudio;
                $playlistaudio->playlist_id = $playlist->id;
                $playlistaudio->audio_id = $audio;
                $playlistaudio->order_number = $i;
                $playlistaudio->save();
                $i++;
                }
            }


             // Send notification to all users
            //  $this->fcm->sendNotificationToAllUsers("New playlist added","1 new playlist added in playlists","audio");


            return redirect('admin/playlist')->with('message','Playlist Added Successfully');
        }
        else
        {
            return back()->with('message','Playlist Not Added');
        }
        // DB::commit();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Playlist  $playlist
     * @return \Illuminate\Http\Response
     */
    public function show(Playlist $playlist)
    {
        $max_order_number = PlaylistAudio::where(['playlist_id'=>$playlist->id])->max('order_number');

        $audios = PlaylistAudio::where(['playlist_id'=>$playlist->id])->with('audio.AudioCategory.category')
        ->whereHas('audio',function($q){
            $q->orderBy('id','desc')->orderBy('order_number','asc');
         });
         if($max_order_number == 0) {
            $audios =  $audios->orderBy('id','desc')->orderBy('order_number','asc')->get();
         }
         else {
            $audios =  $audios->orderBy('order_number','asc')->get();
         }

        //  echo "<pre>";
        //  print_r($audios);
        //  exit;
        $colors  = $this->colors();
        return view('admin.playlist.show',compact('playlist','colors','audios'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Playlist  $playlist
     * @return \Illuminate\Http\Response
     */
    public function edit(Playlist $playlist)
    {
        //
        $channels = Channel::where(['is_deleted'=>0,'status'=>1])->get();
        $categories = Category::where(['is_deleted'=>0,'status'=>1])->get();
        $subcategories = SubCategory::where(['is_deleted'=>0,'category_id'=>$playlist->category_id])->get();
        $audios = Audio::where(['is_deleted'=>0,'status'=>1])->orderBy('id','desc')->orderBy('order_number','asc')->get();


        return view('admin/playlist.edit',compact('playlist','channels','categories','subcategories','playlist'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Playlist  $playlist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Playlist $playlist)
    {
        //  echo "<pre>";
        // print_r($_POST);
        // exit;
        //
       

        $validator = validator::make($request->all(),[
            'channel_id' => 'required',
            // 'category_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'audio_id' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        // if($request->channel_id!=$playlist->channel_id || $request->category_id!=$playlist->category_id)
        // {
        //     if($this->check_category_and_channel($request->channel_id,$request->category_id) > 0)
        //    {
        //     // echo "tt";exit;
        //     return back()->with('error_message','This channel is already exists with this category');
        //     exit;
        //    }
        // }
    if($request->name != $playlist->name && $this->check_name($request->name) > 0)
        {
            $validator = $validator->errors()->add('name','The title has already been taken.');
            return back()->withInput()->withErrors($validator);
        }


        if($request->hasFile('image') && $request->image->isValid())
        {
                $validator = validator::make($request->all(), [
                    'image' => 'required|mimes:jpeg,jpg,gif,png|max:2048',
                ]);
                if ($validator->fails()) {
                    return back()->withInput()->withErrors($validator);
                }
                $playlist->image  =  image_upload($request->image,"images",'public');
        }
        if($request->hasFile('thumbnail') && $request->thumbnail->isValid())
        {
                $playlist->thumbnail  =  image_upload($request->thumbnail,"thumbnail",'public');
        }

        $playlist->name = ucfirst($request->name);
        $playlist->user_id = Auth::user()->id;
        $playlist->description = $request->description ?? "";
        $playlist->channel_id = $request->channel_id ?? 0;
        $playlist->category_id =  0;
        if($playlist->save())
        {
            if($request->category_id)
            {
            PlaylistCategory::where(['playlist_id'=>$playlist->id])->delete();
                
                foreach ($request->category_id as $category) {
                $Playlist_category = new PlaylistCategory;
                $Playlist_category->playlist_id = $playlist->id;
                $Playlist_category->category_id = $category;
                $Playlist_category->save();
            }
            }
            if($request->audio_id)
            {
                $audio_array = $request->audio_id;
                PlaylistAudio::where(['playlist_id'=>$playlist->id])->whereNotIn('audio_id',$audio_array)->delete();
            // rsort($audio_array);
            $i = 1;
            foreach ($audio_array as $audio) {
                $playlistaudio = PlaylistAudio::where(['audio_id'=>$audio,'playlist_id'=>$playlist->id])->first();
                if($playlistaudio) {
                    $playlistaudio = $playlistaudio;
                }
                else {
                    $playlistaudio = new PlaylistAudio;
                }
                $playlistaudio->playlist_id = $playlist->id;
                $playlistaudio->audio_id = $audio;
                $playlistaudio->save();
                $i++;
            }
            }
            // Send notification to all users
            // $this->fcm->sendNotificationToAllUsers("New playlist updated","1 new playlist updated in playlists","playlist");
            return redirect('admin/playlist')->with('message','Playlist updated successfully');
        }
        else
        {
            return back()->with('message','Playlist not updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Playlist  $playlist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Playlist $playlist)
    {
        //
        $playlist->is_deleted = 1;
        if($playlist->save())
        {
            return redirect('admin/playlist')->with('message','Playlist deleted successfully');
        }
        else
        {
            return back()->with('message','Playlist not deleted');
        }
    }
    public function songs_add(Request $request,$id)
    {
        $playlist = Playlist::find($id);
        $songs = Song::where(['playlist_id'=>$id,'is_deleted'=>0])->orderByDesc('id')->get();


        return view('admin/playlist.songs-add',compact('playlist','songs'));

    }
    public function songs_add_post(Request $request,$id)
    {
        // echo "<pre>";
        // print_r($_POST);
        // print_r($_FILES);
        // exit;
        $playlist = Playlist::find($id);


        if($request->hasFile('file'))
        {

            $allowedfileExtension=["mp3","mpeg","3gp","aa","aac","aax","act","aiff","alac","amr","ape","au","awb","dct","dss","dvf","flac","gsm","iklax","ivs","m4a","m4b","m4p","mmf","mpc","msv","nmf","opus","raw","ra","rm","rf64","sln","tta","voc","vox","wav","wma","wv","webm","8svx","cda","ogg"];
            $files = $request->file('file');



            foreach($files as $file)
            {
                 $filename = $file->getClientOriginalName();
                 $extension = $file->getClientOriginalExtension();

                $check=in_array($extension,$allowedfileExtension);
                //dd($check);
                if($check)
                {

                    foreach ($request->file as $photo)
                    {
                    $filename = "audio".time().$filename;
                    $extension = $photo->getClientOriginalExtension();

                    $photo->move(public_path('audio'),$filename);
                    Song::create([
                    'playlist_id' => $id,
                    'file' => $filename,
                    'extension' => $extension
                    ]);
                    }

                    return redirect('admin/playlist')->with('message','Audio Files Uploaded Successfully');
                }
                else
                {

                return back()->with('error_message','Sorry Only Upload Audio Files');
                }
            }
        }
        else
        {
         return back()->with('message','Audio Files Uploaded Successfully');
        }


    }

    public function songs_delete(Request $request)
    {
        $id = $request->id;
        $songs = Song::find($id);
        $songs->is_deleted = 1;
        if($songs->save())
        {
            $data['status'] = 1;
        }
        else
        {
            $data['status'] = 0;
        }

        return response()->json($data);
    }
    public function banner_add(Request $request,$id)
    {
        $playlist = Playlist::find($id);
        $banner = Banner::where(['playlist_id'=>$id])->first();
        // echo "<pre>";
        // print_r($playlist->banner);
        // if($playlist->banner)
        // echo count($playlist->banner);
        // exit;
        return view('admin.playlist.banner-add',compact('playlist','banner'));
    }
    public function banner_add_post(Request $request,$playlist_id)
    {

        $validator = validator::make($request->all(),[
        'status' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        $banner_count = Banner::where('id',$request->id)->count();
        if($banner_count > 0)
            $banner = Banner::find($request->id);
        else
            $banner = new Banner;

        if($request->hasFile('image') && $request->image->isValid())
        {
            $banner->image  =  image_upload($request->image,"images",'public');
        }

        //     echo "<pre>";
        // print_r($_POST);
        // exit;

        $banner->user_id = Auth::user()->id;
        $banner->playlist_id = $playlist_id;
        $banner->status = $request->status;
        if($banner->save())
        {

            $playlist = Playlist::find($playlist_id);
            $playlist->banner_id = $banner->id;
            $playlist->save();
            return redirect('admin/playlist')->with('message','Banner Added Successfully');
        }
        else
        {


            return back()->with('error_message','Something Went Wrong');
        }


    }


    //////////////////////////////////////////////////////////////////////////////////////////////
}
