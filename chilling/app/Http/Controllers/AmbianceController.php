<?php

namespace App\Http\Controllers;

use App\Ambiance;
use Illuminate\Http\Request;
use Validator;
use DB;
class AmbianceController extends Controller
{
    public $paginate_no;
    public $segment;

  public function __construct()
    {
        $this->paginate_no = config('constants.paginate_no');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->query())
        {
            $search = trim($request->search);
            $datas = Ambiance::where(['is_deleted'=>0])
            ->where(function($query) use ($search){
                $query->orWhere('title','like','%'.$search.'%');
                $query->orWhere('tags','like','%'.$search.'%');
                $query->orWhere('author','like','%'.$search.'%');
                $query->orWhere('narrator','like','%'.$search.'%');
                $query->orWhere('created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
            })
            ->orderByDesc('id')->paginate($this->paginate_no);
        }
        else
        {
            $datas = Ambiance::where(['is_deleted'=>0])->orderByDesc('id')->paginate($this->paginate_no);
            $search = '';
        }
       // echo "<pre>";
       // print_r($datas);
       // exit;

        return view('admin/ambiance.index', compact('datas','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/ambiance.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator::make($request->all(), [
            'title' => 'required|max:255',
            // 'tags' => 'required',
            // 'author' => 'required|max:255',
            // 'narrator' => 'required|max:255',
            // 'description' => 'required|max:255',
            'file' => 'required|max:9048',
            // 'image' => 'required|mimes:jpeg,jpg,gif,png|max:2048',
        ]);
        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }
        $audio = new Ambiance;

        // if ($request->hasFile('image') && $request->image->isValid()) {
        //     $audio->image  =  image_upload($request->image,"images",'public');

        // } else {
        //     $audio->image   = 'images/default.jpg';
        // }
        if ($request->hasFile('file') && $request->file->isValid()) {
            $extensionAudio = $request->file->extension();
            $extensionAudio = str_replace("oga","ogg",$extensionAudio);
            $extensionAudio = str_replace("ogg","ogg",$extensionAudio);
            if(in_array($extensionAudio,['mp3','MP3','wav','WAV','ogg','OGG','oga','OGA'])) {
                $fileArray  =  audio_upload($request->file,"audio",'public');
                $audio->file  = $fileArray['fileName'];
                $fileArray['extension'] = str_replace("oga","mp3",$fileArray['extension']);
                $fileArray['extension'] = str_replace("ogg","mp3",$fileArray['extension']);

                $audio->extension = $fileArray['extension'];
            }
            else {
                $validator = $validator->errors()->add('file','The file must be a file of type: mp3, MP3, wav, WAV, ogg, OGG.');
                return back()
                ->withInput()
                ->withErrors($validator);
            }


        } else {
            $audio->file  = '';
        }

        $audio->title = ucwords($request->title);
        // $audio->tags = $request->tags;
        // $audio->author = $request->author;
        // $audio->narrator = $request->narrator;
        // $audio->description = $request->description;
        if ($audio->save()) {

            return redirect('admin/ambiance')->with('message', 'Ambiance added successfully');
        }
    }
    public function colors()
    {
     return $color = array("success","danger","warning","info","light","dark","secondary");
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Ambiance  $ambiance
     * @return \Illuminate\Http\Response
     */
    public function show(Ambiance $ambiance)
    {
        $colors  = $this->colors();

        return view('admin/ambiance.show', compact('ambiance','colors'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ambiance  $ambiance
     * @return \Illuminate\Http\Response
     */
    public function edit(Ambiance $ambiance)
    {
        return view('admin/ambiance.edit', compact('ambiance'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ambiance  $ambiance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ambiance $ambiance)
    {
        $validator = validator::make($request->all(), [
            'title' => 'required|max:255',
            // 'tags' => 'required',
            // 'author' => 'required|max:255',
            // 'narrator' => 'required|max:255',
            // 'description' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        // if ($request->hasFile('image') && $request->image->isValid()) {
        //     $validator = validator::make($request->all(), [
        //         'image' => 'required|mimes:jpeg,jpg,gif,png|max:2048',
        //     ]);
        //     if ($validator->fails()) {
        //         return back()->withInput()->withErrors($validator);
        //     }
        //     $ambiance->image  =  image_upload($request->image,"images",'public');

        // }
        if ($request->hasFile('file') && $request->file->isValid()) {
            $validator = validator::make($request->all(), [
                'file' => 'required|max:9048',
            ]);
            if ($validator->fails()) {
                return back()
                    ->withInput()
                    ->withErrors($validator);
            }
            $extensionAudio = $request->file->extension();
            $extensionAudio = str_replace("oga","mp3",$extensionAudio);
            $extensionAudio = str_replace("ogg","mp3",$extensionAudio);
            if(in_array($extensionAudio,['mp3','MP3','wav','WAV','ogg','OGG','oga','OGA'])) {
                $fileArray  =  audio_upload($request->file,"audio",'public');
                $ambiance->file  = $fileArray['fileName'];
                $fileArray['extension'] = str_replace("oga","mp3",$fileArray['extension']);
                $fileArray['extension'] = str_replace("ogg","mp3",$fileArray['extension']);


                $ambiance->extension = $fileArray['extension'];
             }
             else {
                $validator = $validator->errors()->add('file','The file must be a file of type: mp3, MP3, wav, WAV, ogg, OGG.');
                return back()
                ->withInput()
                ->withErrors($validator);
            }
        }


        $ambiance->title = ucwords($request->title);
        $ambiance->ip = request()->server('SERVER_ADDR');
         
        // $ambiance->tags = $request->tags;
        // $ambiance->author = $request->author;
        // $ambiance->narrator = $request->narrator;
        // $ambiance->description = $request->description;
        // $ambiance->image = $fileName;
        if ($ambiance->save()) {


            return redirect('admin/ambiance')->with('message', 'Ambiance updated successfully.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ambiance  $ambiance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ambiance $ambiance)
    {
        $ambiance->is_deleted = 1;
        if ($ambiance->save()) {
            return redirect('admin/ambiance')->with('message', 'Ambiance deleted successfully.');
        } else {
            return back()->with('message', 'Ambiance not deleted');
        }
    }
    public function makeDefault($id)
    {
        $ambiance = Ambiance::find($id);
        $ambiance->is_default = 1;
        if ($ambiance->save()) {
            DB::table('ambiances')->where('id','!=',$id)->update(['is_default'=>0]);
            return redirect('admin/ambiance')->with('message', 'Ambiance song added to default.');
        } else {
            return back()->with('message', 'Ambiance not found');
        }
    }
}
