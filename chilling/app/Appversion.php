<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Appversion extends Model
{
    //
    protected $appends = ['badge'];
    public function notificationCount($user_id)
    {
        return Notification::select('read_unread')->where(['user_id'=>$user_id,'read_unread'=>0])->count() ?? 0;
    }
    public function getBadgeAttribute()
    {
        $user = Auth::user();
      if($user) {
          return $this->notificationCount($user->id);
      }
      else {
          return 0;
      }

    }

}
