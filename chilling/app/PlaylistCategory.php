<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaylistCategory extends Model
{
    //
    public function playlist()
    {
    	return $this->belongsTo(Playlist::class)->orderBy('order_number','asc');
    }
    public function category()
    {
    	return $this->belongsTo(Category::class);
    }
}
