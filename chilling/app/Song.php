<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    //
    protected $fillable = [
        'file', 'playlist_id','extension','title','image'
    ];
    public function playlist()
    {
    	return $this->belongsTo(Playlist::class);
    }
}
