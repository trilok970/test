<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AudioCategory extends Model
{
    //
 
    public function category()
    {
    	return $this->belongsTo(Category::class);
    }
    public function audio()
    {
    	return $this->belongsTo(Audio::class);
    }
}
