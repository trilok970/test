<?php
use App\Lib\VIDEOSTREAM;
use wapmorgan\Mp3Info\Mp3Info;

    if (!function_exists('image_upload')) {
    function image_upload($file,$filePath,$fileType) {
        if($file) {
            $diskName = env('DISK');
            $extension = $file->extension();
            $fileName  = $filePath.'/' . time() . ".$extension";
            if($diskName == 'local') {

                $ff = $file->move(public_path($filePath), $fileName);

            }
            else if($diskName == 's3') {
                $filePath = $fileName;
                $disk = \Storage::disk('s3');
				$disk->put($filePath, fopen($file, 'r+'), $fileType);
            }
        }

        return $fileName ?? "";
        }
    }
    if (!function_exists('audio_upload')) {
        function audio_upload($file,$filePath,$fileType) {

            if($file) {
                $diskName = env('DISK');
                $extension = $file->extension();
                $fileName  = $filePath.'/' . time() . ".$extension";

                if($diskName == 'local') {

                    $ff = $file->move(public_path($filePath), $fileName);
                }
                else if($diskName == 's3') {
                    $disk = \Storage::disk('s3');


                    if($extension == 'ogg' || $extension == 'oga') {

                        $ff = $file->move(public_path($filePath), $fileName);
                        

                        $fileNameOld = $fileName;
                        $MOVIE = new VIDEOSTREAM();
                        $result = $MOVIE->covertM4AtoMp4($fileName);

                        // dd($result);exit;
                        
                        $fileName = str_replace("ogg","mp3",$fileName);
                        $file_converted = public_path().'/'.$fileName;
                        $file_converted1 = public_path().'/'.$fileNameOld;

                        $disk->put($fileName, fopen($file_converted, 'r+'), $fileType);

                        // dd($result);exit;

                        // $duration = get_audio_duration($ff);
                        file_delete($file_converted);
                        file_delete($file_converted1);
                    }
                    else {
                        $disk->put($fileName, fopen($file, 'r+'), $fileType);

                    }
                }
            }
            $array = array("fileName"=>$fileName ?? "",'finalFilePath'=>$finalFilePath ?? "","extension"=>$extension ?? "","duration"=>$duration ?? "");
            return $array;
            }
        }

 if (!function_exists('file_delete')) {
        function file_delete($file) {
            if(\File::exists($file)) {
                \File::delete($file);
            }
        }
    }
    if (!function_exists('get_audio_duration')) {
        function get_audio_duration($file) {
            if($file) {
                $audio1 = new Mp3Info($file, true);
                return $audio1->duration ?? 0;
            }
        }
    }
    if (!function_exists('image_upload_social')) {
        function image_upload_social($fileName,$filePath,$imgurl) {
            if($fileName) {
                $diskName = env('DISK');
                $ppath = public_path().'/'.$filePath.'/';

                if($diskName == 'local') {
                    file_put_contents($ppath.$fileName,file_get_contents($imgurl));
                }
                else if($diskName == 's3') {

	               file_put_contents($ppath.$fileName,file_get_contents($imgurl));
                $destinationPath = '/images/';
                $ppath = public_path().'/'.$destinationPath;
                $file = ($ppath.$fileName);
                $filePath = $filePath.'/'.$fileName;

                $disk = \Storage::disk('s3');
                $disk->put($filePath, fopen($file, 'r+'), 'public');
                file_delete($file);
                }
            }

            return $fileName ?? "";
            }
        }


