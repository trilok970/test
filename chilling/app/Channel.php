<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    //
    public function playlists()
    {
    	return $this->hasMany(Playlist::class)->orderBy('order_number','asc');
    }
    public function viewCount()
    {
        return $this->view_count;
    }

}


