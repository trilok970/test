<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('register', 'API\PassportController@register');
Route::post('login', 'API\PassportController@login');
Route::post('forgot-password', 'API\PassportController@forgot_password');
Route::post('social-check', 'API\PassportController@social_check');
Route::post('password/reset', 'API\PassportController@reset');
Route::get('test-mail', 'API\PassportController@test_mail');
Route::get('privacy-policy', 'API\PassportController@privacy_policy');
Route::get('about-us', 'API\PassportController@about_us');
Route::get('terms-and-conditions', 'API\PassportController@terms_and_conditions');
Route::get('login-register-banner', 'API\PassportController@login_register_banner');
Route::get('app-version', 'API\PassportController@app_version_without_auth');



Route::group([
    'prefix' => 'auth'
], function () {
Route::group(['middleware' => ['auth:api','user_accessible']], function(){
Route::post('details', 'API\PassportController@details');

Route::post('edit-profile', 'API\PassportController@edit_profile');
Route::get('get-profile', 'API\PassportController@get_profile');
Route::post('change-password', 'API\PassportController@change_password');
Route::post('logout', 'API\PassportController@logout');
Route::get('categories', 'API\PassportController@categories');
Route::get('subcategories', 'API\PassportController@subcategories');
Route::get('channels', 'API\PassportController@channels');
Route::get('channels-new', 'API\PassportController@channels_new');
Route::get('playlists', 'API\PassportController@playlists');
Route::get('playlist/{playlist_id}/banner', 'API\PassportController@get_playlist_banner');
Route::get('categories/{id}/playlists', 'API\PassportController@categories_playlists');
Route::get('channels/{id}/playlists', 'API\PassportController@channels_playlists');
Route::get('channels/{channel_id}/category/{category_id}', 'API\PassportController@channels_category_playlists');
Route::get('banners', 'API\PassportController@banners');
Route::get('channels/{channel_id}/categories', 'API\PassportController@channels_categories');
Route::get('playlists/{playlist_id}', 'API\PassportController@playlist_audios');
Route::get('playlists-new/{playlist_id}', 'API\PassportController@playlist_audios_new');
Route::get('ambiance-songs', 'API\PassportController@ambiance_songs');
Route::post('mystory', 'API\PassportController@mystory_add');
Route::get('mystory', 'API\PassportController@mystory'); 
Route::get('mystory/{id}', 'API\PassportController@mystory_show');
Route::post('mystory/{id}', 'API\PassportController@mystory_update');
Route::delete('mystory/{id}', 'API\PassportController@mystory_delete');
Route::post('audios', 'API\PassportController@audios');
Route::post('myplaylist', 'API\PassportController@myplaylist_add');
Route::get('myplaylist', 'API\PassportController@myplaylist');
Route::get('myplaylist/{id}', 'API\PassportController@myplaylist_show');
Route::post('myplaylist/{id}', 'API\PassportController@myplaylist_update');
Route::delete('myplaylist/{id}', 'API\PassportController@myplaylist_delete');
Route::post('myplaylist/{myplaylist_id}/audio-add', 'API\PassportController@myplaylist_audio_add');
Route::post('myplaylist/{myplaylist_id}/audio-delete', 'API\PassportController@myplaylist_audio_delete');

Route::post('make-favourite/', 'API\PassportController@makeFavorite');
Route::get('subscriptionplan', 'API\PassportController@subscriptionplans');
Route::post('notification-enable', 'API\PassportController@notification_enable');
Route::post('user-subscription', 'API\PassportController@userSubscription');
Route::get('notifications', 'API\PassportController@notifications');
Route::get('user-subscription', 'API\PassportController@userSubscriptionList');
Route::get('view-count/{id}/{type}', 'API\PassportController@viewCount');
Route::get('favourites', 'API\PassportController@favourites');
Route::get('search-content/{type}', 'API\PassportController@search_content');
Route::get('app-version', 'API\PassportController@app_version');
Route::post('notification-read', 'API\PassportController@notification_read');
Route::post('subscription-check', 'API\PassportController@subscriptionCheck');
Route::post('receipt-save', 'API\PassportController@receiptSave');
Route::get('send-email-verification-link', 'API\PassportController@sendEmailVerificationLink');
Route::get('get-last-ten-listening-audio', 'API\PassportController@getLastTenListeningAudio');
Route::post('playlist-sleep-time', 'API\PassportController@playlistSleepTime');
Route::post('audio-duration-update', 'API\PassportController@audioDurationUpdate');







});

});
