<?php

use Illuminate\Support\Facades\Route;
if (App::environment('production')) {
    URL::forceScheme('https');
}

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/clear-cache-all', function() {
    Artisan::call('cache:clear');
    dd("Cache Clear All");
});

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

//Route::get('email/verify/{id}', 'VerifyEmailController@verify');

Route::get('/','HomeController@admin');

Auth::routes();
Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->name('home');

Route::get('success','UserContoller@success_message');
Route::get('privacy-policy','DashboardController@privacy_policy');
Route::get('check-mail/{email}', 'DashboardController@check_mail');
Route::get('send-test-email', 'EmailController@sendEmail');
Route::get('receipt-validate','DashboardController@receiptValidator');
Route::get('receipt-validate-check/{email?}','DashboardController@receiptValidatorCheck');
Route::get('ios-receipt-validate-old','DashboardController@iosReceiptValidator');
Route::get('ios-receipt-validate/{email?}','DashboardController@IosReceiptValidate');


Route::group(['prefix'=>'admin',],function(){

	Route::get('/', 'UserContoller@login');
	Route::post('login', [ 'as' => 'admin.login', 'uses' => 'UserContoller@login']);
	Route::post('do-login','UserContoller@do_login')->name('admin.do.login');
	Route::get('logout','UserContoller@logout');
	Route::view('forgot_password', 'auth.reset_password')->name('admin.password.reset');
	Route::post('reset-password','UserContoller@reset_password');
	Route::get('notification','UserFcmTokenController@sendNotification');
	Route::get('notification1/{user_id}','UserFcmTokenController@sendNotification1');
	Route::get('create-table','DashboardController@createTable');


	Route::middleware(['admin'])->group(function () {

        Route::get('dashboard','DashboardController@index');
        Route::get('my-profile','DashboardController@myProfile')->name('my.profile');
        Route::get('edit-profile','DashboardController@editProfile')->name('edit.profile');
        Route::post('edit-profile','DashboardController@editProfilePost')->name('edit.profile.post');
		Route::get('change-password','DashboardController@changePassword')->name('change.password');
        Route::post('change-password','DashboardController@changePasswordPost')->name('change.password.post');
		Route::resource('user','UserContoller');
		Route::post('update-status','UserContoller@update_status');
		Route::get('ajax','UserContoller@ajax');
		Route::resource('cms','CmsController');
		Route::resource('content','ContentController');
		Route::resource('category','CategoryController');
		Route::resource('subcategory','SubCategoryController');
		Route::resource('channel','ChannelController');
		Route::resource('playlist','PlaylistController');
		Route::resource('banner','BannerController');
		Route::get('songs-add/{id}','PlaylistController@songs_add');
		Route::post('songs-add/{id}','PlaylistController@songs_add_post');
		Route::post('songs-delete','PlaylistController@songs_delete');
		Route::get('banner-add/{id}','PlaylistController@banner_add');
		Route::post('banner-add/{id}','PlaylistController@banner_add_post');
		Route::get('profile','UserContoller@profile')->name('profile');
		Route::get('animated-banner','UserContoller@animatedBanner')->name('animated-banner');
		Route::get('login-register-banner','UserContoller@loginRegisterBanner')->name('login-register-banner');
		Route::post('login-register-banner','UserContoller@UpdateloginRegister')->name('update-login-register-banner');
		Route::post('animated-banner','UserContoller@UpdateAnimatedBanner');
		Route::get('ambiance-audio','UserContoller@ambianceAudio')->name('ambiance-audio');
		Route::resource('audio','AudioController');
        Route::resource('ambiance','AmbianceController');

        Route::POST('update-order','AudioController@update_order');
		Route::get('channel/playlists/{id}','ChannelController@show');
		Route::get('audio-search','AudioController@audio_search');

		Route::resource('user-story','UserStoryController');
        Route::resource('subscriptionplan','SubscriptionplanController');
        Route::get('user-export/{key_val?}','UserContoller@userExport')->name('user.export');
        Route::get('notification','UserContoller@notificationShow')->name('notification');
        Route::post('notification','UserContoller@notificationSend')->name('notification.send');
        Route::get('user-subscribe/{id}','UserContoller@userSubscribe');
        Route::get('user-unsubscribe/{id}','UserContoller@userUnsubscribe');
        Route::get('audios','AudioController@audios');
        Route::get('ambiance/{id}/default','AmbianceController@makeDefault');
        Route::get('playlist-audio-free/{id}','PlaylistAudioController@playlist_audio_free');
        Route::get('graph-data/{key_val}','DashboardController@graph_date');
        Route::get('top-ten-listening-stories-export/{key_val}','DashboardController@topTenListeningStoriesExport')->name('user.top.ten.listening.stories.export');
        Route::get('top-ten-favourite-stories-export/{key_val}','DashboardController@topTenFavouriteStoriesExport')->name('user.top.ten.favourite.stories.export');
        Route::get('top-ten-playlist-export/{key_val}','DashboardController@topTenPlaylistExport')->name('user.top.ten.playlist.export');
        Route::resource('appversion','AppversionController');



	});


});


