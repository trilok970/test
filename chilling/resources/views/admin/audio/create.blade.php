@extends('layouts.admin')
@section('title','Audio Create')
@section('content')
@push('stylesheets')
<link rel="stylesheet" href="{{url('dist/css/selectize.css')}}" />
<link rel="stylesheet" href="{{url('dist/css/selectize.min.css')}}" />

@endpush
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Audio</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/audio')}}">Audio</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Add</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">

                   <div class="col-md-12">
            @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
            @if(session('error_message'))
                <p class="alert alert-danger">{{session('error_message')}}</p>
            @endif

                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url('admin/audio')}}" method="post" id="exampleValidation">
                    @csrf
                   <div class="card">


                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url('admin/audio')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Audio" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Audio
                                        </a>
                                    </div>
                            </div>



                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Title</label>
                                    <div class="col-md-10">
                                        <textarea type="text" class="form-control" id="title" name="title" placeholder="Enter Title">{{old('title')}}</textarea>
                                        <span class="text-danger">{{$errors->first('title')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Category</label>
                                    <div class="col-md-10">
                                        <select type="text" class="selectize-multiple" id="category_id" name="category_id[]" onchange="show_subcategory()" multiple data-live-search="true">
                                        @foreach($categories as $cat)
                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endforeach
                                        </select>
                                        <span class="text-danger">{{str_replace("id","",$errors->first('category_id'))}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Tags</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="tags" name="tags" placeholder="Enter Tags" value="{{old('tags')}}">
                                        <span class="text-danger">{{$errors->first('tags')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Author</label>
                                    <div class="col-md-10">
                                        <textarea type="text" class="form-control" id="author" name="author" placeholder="Enter Author" >{{old('author')}}</textarea>
                                        <span class="text-danger">{{$errors->first('author')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Narrator</label>
                                    <div class="col-md-10">
                                        <textarea type="text" class="form-control" id="narrator" name="narrator" placeholder="Enter Narrator">{{old('narrator')}}</textarea>
                                        <span class="text-danger">{{$errors->first('narrator')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Description</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" id="description" name="description" placeholder="Enter Description" >{{old('description')}}</textarea>
                                        <span class="text-danger">{{$errors->first('description')}}</span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Image</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control image" id="image" name="image" inc_val="1" placeholder="Choose image" value="{{old('image')}}">
                                        <span class="text-danger">{{$errors->first('image')}}</span>
                                    </div>
                                    <div class="col-lg-4">
                                    <img style="display: none;"  id="image_preview"  src="" width="140" class="pull-right" alt="User Image">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Audio File</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control" id="file" name="file" placeholder="Choose image" value="{{old('image')}}">
                                        <input type='hidden' name='duration' id='duration' value="0" />
                                        <audio id='audio'></audio>
                                        <span class="text-danger">{{$errors->first('file')}}</span>
                                    </div>
                                    <div class="col-lg-4">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Sample Song</label>
                                    <div class="col-md-10">
                                        <div class="custom-control custom-checkbox mr-sm-2">
                                            <input type="checkbox" class="custom-control-input" name="is_free" id="is_free" @if(old('is_free')) checked @endif value="1">
                                            <label class="custom-control-label" for="is_free">Add as free</label>
                                        </div>
                                        <span class="text-danger">{{$errors->first('is_free')}}</span>
                                    </div>
                                </div>








                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url('admin/audio')}}" class="btn btn-danger resetBtn">Cancel</a>

                                </div>
                            </div>
                        </div>
                    </form>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
        @push('scripts')
        <script src="{{url('dist/js/selectize.min.js')}}"></script>
        <script src="{{url('dist/js/form-selectize.min.js')}}"></script>
        <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/ckeditor/ckeditor.js"></script>

          <script>

        $(document).ready(function(){
            CKEDITOR.editorConfig = function (config) {
                config.language = 'es';
                config.uiColor = '#F7B42C';
                config.height = 300;
                config.toolbarCanCollapse = true;

            };
            CKEDITOR.replace('author');
            CKEDITOR.replace('narrator');
            CKEDITOR.replace('description');
            CKEDITOR.replace('title');

            /* validate */
            $('#select-beast').selectize({
                create: true,
                sortField: 'text'
            });
            function readURL1(input)
            {
                if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                $('#image_preview').attr('src', e.target.result);

                $('#image_preview').hide();
                $('#image_preview').fadeIn(650);
                }
            reader.readAsDataURL(input.files[0]);
                }
            }

            $("#image").change(function() {
            readURL1(this);
            });

        });
    </script>
    <script>

        //register canplaythrough event to #audio element to can get duration
        var f_duration =0; //store duration
        document.getElementById('audio').addEventListener('canplaythrough', function(e){
         //add duration in the input field #f_du
         f_duration = Math.round(e.currentTarget.duration);
         document.getElementById('duration').value = f_duration;
         URL.revokeObjectURL(obUrl);
        });

        //when select a file, create an ObjectURL with the file and add it in the #audio element
        var obUrl;
        document.getElementById('file').addEventListener('change', function(e){
         var file = e.currentTarget.files[0];
         //check file extension for audio/video type
         if(file.name.match(/\.(avi|mp3|mp4|mpeg|ogg)$/i)){
         obUrl = URL.createObjectURL(file);
         document.getElementById('audio').setAttribute('src', obUrl);
         }
        });
        </script>
    @endpush
@endsection
