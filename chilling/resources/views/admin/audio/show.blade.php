@extends('layouts.admin')
@section('title','Audio Show')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Audio</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/audio')}}">Audio</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">View</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">

                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><h5 class="card-title">Audio Details</h5></label>
                                    <div class="col-md-10">
                                         <a href="{{url('admin/audio')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Audios" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Audios
                                        </a>
                                    </div>
                                </div>

                                <div class="table-responsive">


                                     <table id="example1" class="table table-striped table-bordered" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <td><b>{!! $audio->title !!}<a href="{{url('admin/audios?title='.strip_tags($audio->title))}}"> <i class="fas fa-info-circle"></i></a></b></td>
                                            </tr>
                                            <tr>
                                                <th>Category</th>
                                                <td>
                                                @foreach($audio->AudioCategory as $value)
                                                      <span class="badge badge-{{$colors[$loop->index]}}"><b>{{$value->category->name}}</b></span>

                                                @endforeach

                                                </td>

                                            </tr>
                                            <tr>
                                                <th>Tags</th>
                                                <td><b>{{$audio->tags}}</b></td>
                                            </tr>
                                            <tr>
                                                <th>Author</th>
                                                <td><b>{!! $audio->author !!}<a href="{{url('admin/audios?author='.strip_tags($audio->author))}}"> <i class="fas fa-info-circle"></i></a></b></td>
                                            </tr>
                                            <tr>
                                                <th>Narrator</th>
                                                <td><b>{!! $audio->narrator !!}<a href="{{url('admin/audios?narrator='.strip_tags($audio->narrator))}}"> <i class="fas fa-info-circle"></i></a></b></td>
                                            </tr>
                                            <tr>
                                                <th>Description</th>
                                                <td><b>{!! $audio->description !!}<a href="{{url('admin/audios?description='.strip_tags($audio->description))}}"> <i class="fas fa-info-circle"></i></a></b></td>
                                            </tr>
                                            <tr>
                                                <th>Image</th>
                                                <td><a target="_blank" href="{{$audio->image}}"><img src="{{$audio->image}}" width="50" /></a></td>
                                            </tr>
                                            <tr>
                                                <th>File</th>
                                                <td>
                                                <audio controls>
                                                <source src="{{ $audio->file }}" type="audio/{{$audio->extension}}">
                                                Your browser does not support the audio element.
                                                </audio></td>
                                            </tr>
                                            <tr>
                                                <th>Sample Song</th>
                                                <td>@if($audio->is_free == 1) <span class="badge badge-success"><b>Yes</b></span> @else <span class="badge badge-danger"><b>No</b></span>@endif</td>
                                            </tr>

                                        </thead>



                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

@endsection
