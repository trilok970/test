@extends('layouts.admin')
@section('title','Audios')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Audios</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/audio')}}">Audio</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">View</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">


                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><h5 class="card-title"><b>  {{ $key }} </b></h5></label>
                                    <div class="col-md-10">

                                    {{ $val }}

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><h5 class="card-title">All Audios</h5></label>
                                    <div class="col-md-10">
                                         <a href="{{url('admin/audio')}}" class="btn btn-info pull-right btn-sm"  title="All Audios" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Audios
                                        </a>
                                    </div>
                                </div>
                                <div class="table-responsive">

                                     <table id="example1" class="table" width="100%">
                                        <!-- <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Title</th>
                                                <th>Category</th>
                                                <th>Tags</th>
                                                <th>Author</th>
                                                <th>Narrator</th>
                                                <th>Description</th>
                                                <th>File</th>
                                            </tr>
                                        </thead> -->
                                        <tbody id="tbody">
                                            @php $i=1; @endphp
                                            @foreach($audios as $row)
                                            <tr data-post-id={{$row->id}}>
                                            <td><span class="filter_text"><a target="_blank" href="{{$row->image}}"><img src="{{$row->image}}" width="50" /></a></span></td>
                                                <td>{{$i}}</td>
                                                <td><b><span class="filter_text">{!! $row->title !!}
                                                <!-- <br>{{$row->author}}
                                                <br>  @foreach($row->AudioCategory as $value)
                                                      <span class="badge badge-{{$colors[$loop->index]}}">{{$value->category->name}}</span>
                                                    @endforeach
                                                <br>{{$row->narrator}}
                                                <br>{{$row->tags}}
                                                <br>{{$row->description}} -->

                                                </span></b></td>
                                                <!-- <td><b><span class="filter_text">
                                                    @foreach($row->AudioCategory as $value)
                                                      <span class="badge badge-{{$colors[$loop->index]}}">{{$value->category->name}}</span>
                                                    @endforeach</span></b>
                                                </td>
                                                <td><b><span class="filter_text">{{$row->tags}}</span></b></td>
                                                <td><b><span class="filter_text"><a href="{{url('admin/audios?author='.$row->author)}}">{{$row->author}}</a></span></b></td>
                                                <td><b><span class="filter_text"><a href="{{url('admin/audios?narrator='.$row->narrator)}}">{{$row->narrator}}</a></span></b></td></td>
                                                <td><b><span class="filter_text"><a href="{{url('admin/audios?description='.$row->description)}}">{{$row->description}}</a></span></b></td></td> -->
                                                <td><span class="filter_text"><audio controls>
                                                <source src="{{ $row->file }}" type="audio/{{$row->extension}}">
                                                Your browser does not support the audio element.
                                                </audio></span></td>
                                            </tr>
                                            @php $i++; @endphp
                                            @endforeach
                                        </tbody>

                                    </table>
                                </div>
                                {{ $audios->appends(request()->query())}}

                            </div>
                        </div>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            @push('scripts')
            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

            <script >
                $(function () {
                    $('#example1').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true,
                'pageLength'  : 50,
                "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                "aoColumns": [
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                  { "bSortable": false },
                                  { "bSortable": false }
                              ]

              });


/////////////   REPLACE TR VALUE //////////////////////////
    $( "#tbody" ).sortable({
        revert: true,
        cancel: ".filter_text",
    update  : function(event, ui)
    {
    var post_order_ids = new Array();
    var type = 'playlist_audio';
    $('#tbody tr').each(function(){
    post_order_ids.push($(this).data("post-id"));
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
    url:"{{url('admin/update-order')}}",
    method:"POST",
    data:{post_order_ids:post_order_ids,type:type},
    success:function(data)
    {
        if(data)
        {
            $(".alert-danger").hide();
            $(".alert-success ").show();
        }else{
            $(".alert-success").hide();
            $(".alert-danger").show();
        }
    }
    });
    }
    });


            });
            </script>
         @endpush

@endsection
