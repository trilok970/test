@extends('layouts.admin')
@section('title','Appversion')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Appversion</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/appversion')}}">Appversion</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">List</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">

                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"> <h5 class="card-title">All Contents</h5></label>
                                    <div class="col-md-10">

                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table id="example1" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Appversion</th>
                                                <th>Updated</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=1; @endphp
                                            @foreach($datas as $row)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>{{$row->version}}</td>
                                                <td>{{date("d-m-Y",strtotime($row->updated_at))}}</td>
                                                <td>
                                                    <a href="{{url('admin/appversion/'.$row->id.'/edit')}}" class="btn btn-info btn-sm"><i class="fa far fa-edit"></i></a>
                                                </td>
                                            </tr>
                                            @php $i++; @endphp
                                            @endforeach

                                    </table>
                                </div>

                            </div>
                        </div>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            @push('scripts')
            <script >
                $(function () {
              $('#example1').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true,
                'pageLength'  : 50,
                "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                "aoColumns": [
                                null,
                                null,
                                null,
                                  { "bSortable": false }
                              ]

              });


            });
            </script>
         @endpush

@endsection
