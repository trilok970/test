@extends('layouts.admin')
@section('title','Appversion Edit')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Appversion</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/appversion')}}">Appversion</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">

                   <div class="col-md-12">
                @if(session('message'))
                    <p class="alert alert-success">{{session('message')}}</p>
                @endif
                    @if($appversion)
                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url('admin/appversion/'.$appversion->id ?? 1)}}" method="post" id="exampleValidation">
                    @method("PUT")
                    @else
                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url('admin/appversion')}}" method="post" id="exampleValidation">
                    @endif
                    @csrf

                   <div class="card">


                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url('admin/appversion')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="Appversion" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            Appversion
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Android Version</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="android_version" name="android_version" placeholder="Enter Android Version" value="{{$appversion->android_version ?? ''}}">
                                        <span class="text-danger">{{$errors->first('android_version')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Ios Version</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="ios_version" name="ios_version" placeholder="Enter Android Version" value="{{$appversion->ios_version ?? ''}}">
                                        <span class="text-danger">{{$errors->first('ios_version')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Android Force Update</label>
                                    <div class="col-md-10">
                                        <select  class="form-control" id="android_force_update" name="android_force_update" >
                                        <option value="">Select Android Force Update</option>
                                        <option value="1" @if($appversion) {{ ($appversion->android_force_update == 1) ? "selected":""}} @endif>Yes</option>
                                        <option value="0" @if($appversion) {{ ($appversion->android_force_update == 0) ? "selected":""}} @endif>No</option>
                                        </select>
                                        <span class="text-danger">{{$errors->first('android_force_update')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Ios Force Update</label>
                                    <div class="col-md-10">
                                        <select  class="form-control" id="ios_force_update" name="ios_force_update" >
                                        <option value="">Select Ios Force Update</option>
                                        <option value="1" @if($appversion) {{ ($appversion->ios_force_update == 1) ? "selected":""}} @endif>Yes</option>
                                        <option value="0" @if($appversion) {{ ($appversion->ios_force_update == 0) ? "selected":""}} @endif>No</option>
                                        </select>
                                        <span class="text-danger">{{$errors->first('ios_force_update')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Android Maintenance</label>
                                    <div class="col-md-10">
                                        <select  class="form-control" id="android_maintenance" name="android_maintenance" >
                                        <option value="">Select Android Maintenance</option>
                                        <option value="1" @if($appversion) {{ ($appversion->android_maintenance == 1) ? "selected":""}} @endif>Yes</option>
                                        <option value="0" @if($appversion) {{ ($appversion->android_maintenance == 0) ? "selected":""}} @endif>No</option>
                                        </select>
                                        <span class="text-danger">{{$errors->first('android_maintenance')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Ios Maintenance</label>
                                    <div class="col-md-10">
                                        <select  class="form-control" id="ios_maintenance" name="ios_maintenance" >
                                        <option value="">Select Ios Maintenance</option>
                                        <option value="1" @if($appversion){{ ($appversion->ios_maintenance == 1) ? "selected":""}} @endif>Yes</option>
                                        <option value="0" @if($appversion) {{ ($appversion->ios_maintenance == 0) ? "selected":""}} @endif>No</option>
                                        </select>
                                        <span class="text-danger">{{$errors->first('ios_maintenance')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Android Message</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" id="android_message" name="android_message" placeholder="Enter Android Message" >{{$appversion->android_message ?? ''}}</textarea>
                                        <span class="text-danger">{{$errors->first('android_message')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Ios Message</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" id="ios_message" name="ios_message" placeholder="Enter Ios Message" >{{$appversion->ios_message ?? ''}}</textarea>
                                        <span class="text-danger">{{$errors->first('ios_message')}}</span>
                                    </div>
                                </div>





                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url('admin/appversion')}}" class="btn btn-danger resetBtn">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

@endsection
