@extends('layouts.admin')
@section('title','Contents Edit')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Contents</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/content')}}">Contents</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">

                   <div class="col-md-12">


                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url('admin/content/'.$content->id)}}" method="post" id="exampleValidation">
                    @csrf
                    @method("PUT")
                   <div class="card">


                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url('admin/content')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Content" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Content
                                        </a>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Title</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="{{$content->title}}">
                                        <span class="text-danger">{{$errors->first('title')}}</span>
                                    </div>
                                </div>
                               <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Sub Title</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="sub_title" name="sub_title" placeholder="Enter Sub Title" value="{{$content->sub_title}}">
                                        <span class="text-danger">{{$errors->first('sub_title')}}</span>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Description</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" id="editor1" name="description" placeholder="Enter Content" value="{{$content->description}}" >{{$content->description}}</textarea>
                                        <span class="text-danger">{{$errors->first('description')}}</span>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Image</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control" id="image" name="image" placeholder="Choose image" value="{{$content->image}}">

                                    </div>
                                    <div class="col-lg-4">
                                    <img  id="image_preview"  src="{{$content->image}}" width="140" class="pull-right" alt="User Image">
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Status</label>
                                    <div class="col-md-10">



                                        <div class="form-group">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="status1" name="status" value="1" class="custom-control-input" {{$content->status==1 ? "checked":""}}>
                                                <label class="custom-control-label" for="status1">Active</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="status2" name="status" class="custom-control-input" value="0" {{$content->status==0 ? "checked":""}}>
                                                <label class="custom-control-label" for="status2">In-Active</label>
                                            </div>
                                        </div>



                                    </div>
                                </div>


                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url('admin/content')}}" class="btn btn-danger resetBtn">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

           <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/ckeditor/ckeditor.js"></script>

          <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/angular.min.js"></script>
          <script>
        $(document).ready(function(){
              CKEDITOR.editorConfig = function (config) {
            config.language = 'es';
            config.uiColor = '#F7B42C';
            config.height = 300;
            config.toolbarCanCollapse = true;

        };
        CKEDITOR.replace('editor1');

            /* validate */



        $("#exampleValidation").validate({
            validClass: "success",
            rules: {
                title: {
                    required: true
                },
                sub_title: {
                    required: true
                },
                description: {
                    required: true
                },
            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
        });


     function readURL(input)
    {
        if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
        $('#image_preview').attr('src', e.target.result);

        $('#image_preview').hide();
        $('#image_preview').fadeIn(650);
        }
       reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function() {
    readURL(this);
    });



        });






    </script>
@endsection
