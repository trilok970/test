@extends('layouts.admin')
@section('title','Contents')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Contents</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/content')}}">Contents</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">List</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">

                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"> <h5 class="card-title">All Contents</h5></label>
                                    <div class="col-md-10">
                                         <a href="{{url('admin/content/create')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Content" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            Add Content
                                        </a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Title</th>
                                                <th>Sub Title</th>
                                                <th>Updated</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=1; @endphp
                                            @foreach($datas as $row)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>{{$row->title}}</td>
                                                <td>{{$row->sub_title}}</td>
                                                <td>{{date('Y-m-d',strtotime($row->updated_at))}}</td>
                                                <td>
                                                @if($row->status==1)
                                                <a href="javascript:;" title="Active" class="btn btn-xs btn-success inactive-button" data-id="1"><b>Active</b></a>
                                                @else
                                                <a href="javascript:;" title="In-Active" class="btn btn-xs btn-danger inactive-button" data-id="1"><b>In-Active</b></a>
                                                @endif
                                                </td>
                                                <td>
                                                    <a href="{{url('admin/content/'.$row->id)}}" title="View" class="btn btn-primary btn-sm" data-edit_id="0"><i class="fas fa-eye"></i></a>
                                                    <a href="{{url('admin/content/'.$row->id.'/edit')}}" class="btn btn-info btn-sm"><i class="fa far fa-edit"></i></a></td>
                                            </tr>
                                            @php $i++; @endphp
                                            @endforeach

                                    </table>
                                </div>

                            </div>
                        </div>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

@endsection
