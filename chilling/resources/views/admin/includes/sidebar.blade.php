 <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/dashboard')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/notification')}}" aria-expanded="false"><i class="mdi mdi-bell"></i><span class="hide-menu">Notifications</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/user')}}" aria-expanded="false"><i class="mdi mdi-account-circle"></i><span class="hide-menu">Users</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/user-story')}}" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Users Story</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/category')}}" aria-expanded="false"><i class="mdi mdi-library-plus"></i><span class="hide-menu">Category</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/audio')}}" aria-expanded="false"><i class="mdi mdi-library-music"></i><span class="hide-menu">Audio</span></a></li>

                        <!-- <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/subcategory')}}" aria-expanded="false"><i class="mdi mdi-library-plus"></i><span class="hide-menu">SubCategory</span></a></li> -->

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/channel')}}" aria-expanded="false"><i class="mdi mdi-presentation"></i><span class="hide-menu">Channel</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/playlist')}}" aria-expanded="false"><i class="mdi mdi-play-circle"></i><span class="hide-menu">Playlist</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/subscriptionplan')}}" aria-expanded="false"><i class="mdi mdi-credit-card-plus"></i><span class="hide-menu">Subscription Plan</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/banner')}}" aria-expanded="false"><i class="mdi mdi-folder-multiple-image"></i><span class="hide-menu">Banner</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Settings </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="{{url('admin/cms')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> CMS Pages </span></a></li>
                                <li class="sidebar-item"><a href="{{url('admin/content')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Content </span></a></li>
                                <li class="sidebar-item"><a href="{{url('admin/animated-banner')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Animated Banner </span></a></li>
                                <li class="sidebar-item"><a href="{{url('admin/login-register-banner')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Login Register Banner </span></a></li>
                                <li class="sidebar-item"><a href="{{url('admin/ambiance')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Ambiance Audio </span></a></li>
                                <li class="sidebar-item"><a href="{{url('admin/appversion')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Appversion </span></a></li>

                            </ul>
                        </li>



                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
