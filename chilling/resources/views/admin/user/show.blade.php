@extends('layouts.admin')
@section('title','User Profile')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">User</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/user')}}">User</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">View</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">

                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><h5 class="card-title"><i class="fa fa-list"></i> User Details</h5></label>
                                    <div class="col-md-10">
                                         <a href="{{url('admin/parent')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Users" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Users
                                        </a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                   <table class="table table-striped table-bordered">
                               <tbody>

                                    <tr>
                                        <th style="width:30%">First Name</th>
                                        <td style="width:70%">{{$user->first_name}} {{$user->last_name}}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:30%">Last Name</th>
                                        <td style="width:70%">{{$user->last_name}}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:30%">Email</th>
                                        <td style="width:70%">{{$user->email}}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:30%">Country Code</th>
                                        <td style="width:70%">{{$user->country_code}}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:30%">Phone Number</th>
                                        <td style="width:70%">{{$user->phone_number}}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:30%">No of children</th>
                                        <td style="width:70%">{{$user->no_of_children}}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:30%">Location</th>
                                        <td style="width:70%">{{$user->location}}</td>
                                    </tr>
                                                                        <tr>
                                        <th style="width:30%">Image</th>
                                        <td style="width:70%">
                                            <div class="avatar-xxl">
                                                <a target="_blank" href="{{url('images/'.$user->profile_pic)}}"><img src="{{url('images/'.$user->profile_pic)}}" class="avatar-img" width="100"> </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td>@if($user->status==1) <span class="text-success">Active</span> @else <span class="text-danger">Inactive</span> @endif</td>
                                    </tr>
                                </tbody>
                            </table>
                                </div>

                            </div>
                        </div>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

@endsection
