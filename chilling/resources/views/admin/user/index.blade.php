@extends('layouts.admin')
@section('title','Users')
@section('content')
<style>
.cus_box_info
{
    background-color: #24313C; padding: 10px; margin: 10px;
}

.cus_body_box
{
    background-color: #1C2331; padding: 10px; margin: 10px;color:white;
}

.cus_head_box
{
    height: 40px; padding-top:10px; margin-top: 0px; background-color:#1AB188;
}

.detail_cus  { padding-top: 7px; margin-top: 7px; }
.box_footer_cus  { background-color: #1AB188; padding-top: 7px; margin-top: 7px; }


.status_checkbox{
visibility: hidden;
}

/* SLIDE THREE */
.slideparam {
width: 80px;
height: 26px;
background: #333;
margin: 2px auto;

-webkit-border-radius: 50px;
-moz-border-radius: 50px;
border-radius: 50px;
position: relative;

-webkit-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);
-moz-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);
box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);

}

.slideparam:after {
content: 'OFF';
font: 12px/26px Arial, sans-serif;
color: #7a121d;
position: absolute;
right: 10px;
z-index: 0;
font-weight: bold;
text-shadow: 1px 1px 0px rgba(255,255,255,.15);
}

.slideparam:before {
content: 'ON';
font: 12px/26px Arial, sans-serif;
color: #00bf00;
position: absolute;
left: 10px;
z-index: 0;
font-weight: bold;
}

.slideparam label {
display: block;
width: 34px;
height: 20px;
cursor: pointer;
-webkit-border-radius: 50px;
-moz-border-radius: 50px;
border-radius: 50px;

-webkit-transition: all .4s ease;
-moz-transition: all .4s ease;
-o-transition: all .4s ease;
-ms-transition: all .4s ease;
transition: all .4s ease;
position: absolute;
top: 3px;
left: 3px;
z-index: 1;

-webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
-moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
background: #fff;


filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfff4', endColorstr='#b3bead',GradientType=0 );
}

.slideparam input[type=checkbox]:checked + label {
left: 43px;
}


 .slideparam input[type=checkbox]:checked + label:after {
   background: #27ae60;
}

.slideparam label:after {
   content:'';
   width: 10px;
   height: 10px;
   position: absolute;
   top: 5px;
   left: 12px;
   background: red;
   border-radius: 50%;
   box-shadow: inset 0px 1px 1px black, 0px 1px 0px rgba(255, 255, 255, 0.9);
 }

</style>
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">@if(request()->verified && request()->verified == 1) Verified @endif Users</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/user')}}">Users</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">List</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">

                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"> <h5 class="card-title"></h5></label>
                                    <div class="col-md-10">
                                         <!-- <a href="{{url('admin/user-export')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="Export User List" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-excel"></i>
                                            </span>
                                            Export User List
                                        </a> -->
                                    </div>
                                </div>
                                <form action="{{url()->current()}}" >
                                 <div class="form-group row">
                                    <label class="col-md-1 m-t-15"><b>Search</b></label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="search" name="search" placeholder="Enter Name,Phone no,Email ..." value="{{$search}}" required="">

                                    </div>
                                    <div class="col-md-1">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                    </div>

                                    <div class="col-md-2">
                                    <a href="{{url()->current()}}" class="btn btn-danger">Reset</a>
                                    </div>
                                    <div class="col-md-5">
                                    <a href="@if(request()->verified && request()->verified == 1)  {{url('admin/user-export?verified=1')}} @else  {{url('admin/user-export')}} @endif" class="btn btn-success pull-right" data-toggle="tooltip" title="Export User List" style="float:right;">
                                            <span class="btn-label">
                                            <i class="fas fa-file-excel"></i>
                                            </span>
                                            Export User List
                                        </a>
                                    </div>

                                </div>
                            </form>

                                <div class="table-responsive m-t-20">
                                    <table id="example1" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Name</th>
                                                <th>Phone Number</th>
                                                <th>Email</th>
                                                <th>CreatedAt</th>
                                                <th>UserType</th>
                                                <th>Profile Pic</th>
                                                <th>Subscription</th>
                                                <th>Status</th>
                                                <th style="width:70px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i= $users->toArray()['from'] ?? 1; @endphp
                                            @foreach($users as $row)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>{{$row->fullname}}</td>
                                                <td>@if($row->phone_number)({{ $row->country_code}}) {{($row->phone_number == null || $row->phone_number == 'null')?"":$row->phone_number}}@endif</td>
                                                <td>{{$row->email}}</td>
                                                <td>{{date('Y-m-d',strtotime($row->created_at))}}</td>
                                                <td>{{$row->social_type ?? "Web"}}</td>
                                                <td><a target="_blank" href="{{$row->profile_pic}}"><img src="{{$row->profile_pic}}" width="30" /></a></td>
                                                <td class="subscribe_{{ $row->id }}">@if($row->is_premium == 1) <span class="badge badge-success">Active</span>@else <span class="badge badge-danger">Inactive</span>@endif</td>
                                                <td><center><div class="slideparam">
                    <input type_status="users" type="checkbox" id="{{$row->id}}" onChange="save_admin_message_settings({{$row->id}})" name="status" value="1"  class="status_{{$row->id}} status_checkbox"   {{($row->status==1)?"checked":""}}  />
                    <label for="{{$row->id}}" ></label>
                    </div> </center></td>
                                                <td>
                                                @if($row->is_premium==1 && $row->subscription_plan_admin==1)
                                                @php $classname = 'user-unsubscribe';$title = 'Subscribed User'; $url = url('admin/user-unsubscribe/'.$row->id); @endphp
                                                @elseif($row->is_premium==1 && $row->subscription_plan_admin==0)
                                                @php $classname = 'disabled'; $title = 'User'; $url = url('admin/user-subscribe/'.$row->id);  @endphp
                                                @else
                                                @php $classname = 'user-subscribe'; $title = 'Unsubscribed User'; $url = url('admin/user-subscribe/'.$row->id); @endphp
                                                @endif
                                    <div class="float-left mr-2">
                                      <a id="u{{$row->id}}" uid="{{$row->id}}" data-original-title="{{$title}}" data-toggle="tooltip" href="{{ $url }}" class="btn @if($classname=='user-subscribe' || $classname=='disabled')btn-info @else btn-warning @endif btn-sm btn-darken-3 tab-order {{$classname}}" > <i class="fas fa-star"></i></a>
                                    </div>
                                    <div class="float-left">
                                        <form action="{{ url('admin/user/'.$row->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">

                                        <button title="Delete" data-toggle="tooltip" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> </button>
                                         </form>

                                    </div>

                                                </td>
                                            </tr>
                                            @php $i++; @endphp
                                            @endforeach

                                    </table>
                                </div>
                                {{ $users->appends(request()->query())}}

                            </div>
                        </div>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

          @push('scripts')
            <script >
                $(function () {
              $('#example1').DataTable({
                'paging'      : false,
                'lengthChange': true,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true,
                'pageLength'  : 50,
                "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                "aoColumns": [
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                  { "bSortable": false },
                                  { "bSortable": false },
                                  { "bSortable": false },
                                  { "bSortable": false }
                              ]

              });




            });
            </script>
            <script>


    $(document).on('click','.user-subscribe',function(evt) {
        evt.preventDefault();

        var url = $(this).attr('href');
        var postData = $(this).attr('uid');

        console.log("#"+postData);
        swal({
        title: "Are you sure?",
        text: "Are you sure,you want to change subscribed to this user!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, change it!",
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {

        $("#status").attr('disabled',true);
             $.get(url,postData,function(out) {
            if(out.result == 1) {
                // alert(out.msg);
                $("#u"+postData).removeClass(out.removeClass);
                $("#u"+postData).addClass(out.addClass);
                $("#u"+postData).removeClass(out.removeClass1);
                $("#u"+postData).addClass(out.addClass1);
                $("#u"+postData).attr('data-original-title',out.title).tooltip('show');
                $("#u"+postData).prop('href',out.url);
                $(".subscribe_"+postData).html(out.activeButton);
                swal ( "Success" ,  out.msg+"!" ,  "success" );



            } else {
                swal ( "Opps" ,  out.msg+"!" ,  "error" );

                // alert(out.msg);
            }
        });

        } else {
            swal("Cancelled", "User is not subscribed :)", "error");
        }
    }
);

    });

    $(document).on('click','.user-unsubscribe',function(evt) {
        evt.preventDefault();

        var url = $(this).attr('href');
        var postData = $(this).attr('uid');

        console.log("#"+postData);
        swal({
        title: "Are you sure?",
        text: "Are you sure,you want to change unsubscribed to this user!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, change it!",
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {

        $("#status").attr('disabled',true);
             $.get(url,postData,function(out) {
            if(out.result == 1) {
                // alert(out.msg);
                $("#u"+postData).removeClass(out.removeClass);
                $("#u"+postData).addClass(out.addClass);
                $("#u"+postData).removeClass(out.removeClass1);
                $("#u"+postData).addClass(out.addClass1);
                $("#u"+postData).attr('data-original-title',out.title).tooltip('show');
                $("#u"+postData).prop('href',out.url);
                $(".subscribe_"+postData).html(out.activeButton);

                swal ( "Success" ,  out.msg+"!" ,  "success" );



            } else {
                swal ( "Opps" ,  out.msg+"!" ,  "error" );

                // alert(out.msg);
            }
        });

        } else {
            swal("Cancelled", "User is subscribed :)", "error");
        }
    }
);

    });
</script>
         @endpush
@endsection
