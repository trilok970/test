@extends('layouts.admin')
@section('title','Notifications')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Notification</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>

                                    <li class="breadcrumb-item active" aria-current="page">Notification</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">

                   <div class="col-md-12">
                   @if(session('message'))
                        <p class="alert alert-success">{{session('message')}}</p>
                    @endif

                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url('admin/notification')}}" method="post" id="exampleValidation">
                    @csrf
                   <div class="card">


                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Send notification to all users</label>
                                    <div class="col-md-10">
                                         <!-- <a href="{{url('admin/notification')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All notification" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All notification
                                        </a> -->
                                    </div>
                            </div>

                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Title</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="{{old('title')}}">
                                        <span class="text-danger">{{$errors->first('title')}}</span>
                                    </div>
                                </div>



                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Description</label>
                                    <div class="col-md-10">
                                       <textarea onkeyup="countChar(this)" class="form-control" id="description" name="description" placeholder="Enter Content" value="{{old('description')}}" >{{old('description')}}</textarea>
                                       <span id="charNum" class="error">100 </span>
                                       <span  class="error">Character Left</span>
                                        <span class="text-danger">{{$errors->first('description')}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Image</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control image" id="image" name="image" inc_val="1" placeholder="Choose image" value="{{old('image')}}">
                                        <span class="text-danger">{{$errors->first('image')}}</span>
                                    </div>
                                    <div class="col-lg-4">
                                    <img style="display: none;"  id="image_preview"  src="" width="140" class="pull-right" alt="User Image">
                                    </div>
                                </div>



                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url('admin/notification')}}" class="btn btn-danger resetBtn">Cancel</a>

                                </div>
                            </div>
                        </div>
                    </form>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->


          <script>
          	function countChar(val) {
        var len = val.value.length;
        if (len >= 100) {
          val.value = val.value.substring(0, 100);
        } else {
          $('#charNum').text(100 - len);
        }
      }
        $(document).ready(function(){

            /* validate */






     function readURL(input)
    {
        if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
        $('#image_preview').attr('src', e.target.result);

        $('#image_preview').hide();
        $('#image_preview').fadeIn(650);
        }
       reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function() {
    readURL(this);
    });



        });






    </script>
@endsection
