@extends('layouts.admin')
@section('title','Animated Banner')
@section('content')
<style>
.cus_box_info
{
    background-color: #24313C; padding: 10px; margin: 10px;
}

.cus_body_box
{
    background-color: #1C2331; padding: 10px; margin: 10px;color:white;
}

.cus_head_box
{
    height: 40px; padding-top:10px; margin-top: 0px; background-color:#1AB188;
}

.detail_cus  { padding-top: 7px; margin-top: 7px; }
.box_footer_cus  { background-color: #1AB188; padding-top: 7px; margin-top: 7px; }


.status_checkbox{
visibility: hidden;
}

/* SLIDE THREE */
.slideparam {
width: 80px;
height: 26px;
background: #333;
margin: 2px auto;

-webkit-border-radius: 50px;
-moz-border-radius: 50px;
border-radius: 50px;
position: relative;

-webkit-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);
-moz-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);
box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);

}

.slideparam:after {
content: 'OFF';
font: 12px/26px Arial, sans-serif;
color: #7a121d;
position: absolute;
right: 10px;
z-index: 0;
font-weight: bold;
text-shadow: 1px 1px 0px rgba(255,255,255,.15);
}

.slideparam:before {
content: 'ON';
font: 12px/26px Arial, sans-serif;
color: #00bf00;
position: absolute;
left: 10px;
z-index: 0;
font-weight: bold;
}

.slideparam label {
display: block;
width: 34px;
height: 20px;
cursor: pointer;
-webkit-border-radius: 50px;
-moz-border-radius: 50px;
border-radius: 50px;

-webkit-transition: all .4s ease;
-moz-transition: all .4s ease;
-o-transition: all .4s ease;
-ms-transition: all .4s ease;
transition: all .4s ease;
position: absolute;
top: 3px;
left: 3px;
z-index: 1;

-webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
-moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
background: #fff;


filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfff4', endColorstr='#b3bead',GradientType=0 );
}

.slideparam input[type=checkbox]:checked + label {
left: 43px;
}


 .slideparam input[type=checkbox]:checked + label:after {
   background: #27ae60;
}

.slideparam label:after {
   content:'';
   width: 10px;
   height: 10px;
   position: absolute;
   top: 5px;
   left: 12px;
   background: red;
   border-radius: 50%;
   box-shadow: inset 0px 1px 1px black, 0px 1px 0px rgba(255, 255, 255, 0.9);
 }

</style>
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Animated Banner</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/animated-banner')}}">Animated Banner</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">List</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                    @if(session('message'))
                      <p class="alert alert-success">{{session('message')}}</p>
                    @endif
                    @if(session('error_message'))
                      <p class="alert alert-danger">{{session('error_message')}}</p>
                    @endif

                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url('admin/animated-banner/')}}" method="post" id="exampleValidation">
                    @csrf
                    <input type="hidden" name="id" id="id" value="{{$setting->id}}" />
                   <div class="card">
                    <div class="card-body">


                      <div class="form-group row">
                        <label class="col-md-2 m-t-15">Banner</label>
                        <div class="col-md-12">
                          <input type="file" class="form-control image" id="image" name="animated_banner" inc_val="1" placeholder="Choose image">
                          <small class="text-danger">Please upload max 5 mb file. And upload image size height = 600px , width =  1200px.</small>
                          <span class="text-danger">{{$errors->first('animated_banner')}}</span>
                        </div>
                        <div class="col-md-12 m-t-15">
                          <?php
                            $pos = strrpos($setting->animated_banner,".");
                            $ext = substr($setting->animated_banner,($pos+1));
                          ?>

                          @if($ext != 'mp4')
                            <img id="image_preview"  src="{{$setting->animated_banner}}" width="270" class="pull-left" alt="User Image">
                          @else
                            <video width="320" height="240" controls>
                              <source src="{{$setting->animated_banner}}" type="video/mp4">
                              Your browser does not support the video tag.
                            </video>
                          @endif
                        </div>
                      </div>
                    </div>
                    <div class="border-top">
                      <div class="card-body">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{url('admin/dashboard')}}" class="btn btn-danger resetBtn">Cancel</a>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->


@endsection
