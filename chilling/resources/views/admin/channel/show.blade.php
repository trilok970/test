@extends('layouts.admin')
@section('title','Channel Playlist Show')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Channel Playlist</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/channel')}}">Channel</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Playlist</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">

                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><h5 class="card-title">Channel Details</h5></label>
                                    <div class="col-md-10">
                                         <a href="{{url('admin/channel')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Channel" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Channel
                                        </a>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table id="example1" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Channel Name</th>
                                                <th>Created On</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=1; @endphp

                                            <tr>
                                                <td>{{$channel->name}}</td>
                                                <td>{{date('d-M-Y',strtotime($channel->created_at))}}</td>


                                            </tr>



                                    </table>
                                     <div class="form-group row">
                                    <label class="col-md-12 m-t-15"><h5 class="card-title">Playlists</h5></label>

                                   </div>
                                     <table id="example1" class="table table-striped table-bordered" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr. No.</th>
                                                <th>Title</th>
                                                <th>Category Name</th>
                                                <th>Image</th>
                                                <th>Thumbnail</th>
                                                <th>Created On</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody">
                                            @php $i=1; @endphp
                                            @if($channel->playlists)
                                            @foreach($channel->playlists as $playlist)
                                            <tr  data-post-id={{$playlist->id}}>
                                                <td><b><span class="filter_text">{{$i}}</span></b></td>
                                                <td><b><span class="filter_text">{{$playlist->name}}</span></b></td>
                                                <td><b><span class="filter_text"> @foreach($playlist->PlaylistCategory as $value)
                                                      <span class="badge badge-{{$colors[$loop->index]}}">{{$value->category->name}}</span>

                                                    @endforeach </span> </b></td>
                                                <td><span class="filter_text"><a target="_blank" href="{{url($playlist->image)}}"><img src="{{url($playlist->image)}}" width="30" /></a></span></td>
                                                <td><span class="filter_text"><a target="_blank" href="{{url($playlist->thumbnail)}}"><img src="{{url($playlist->thumbnail)}}" width="30" /></a></span></td>
                                                <td><span class="filter_text"><b>{{date('d-M-Y',strtotime($playlist->created_at))}}</b></td>
                                     <td> <a title="Audio" href="{{ url('admin/playlist/'.$playlist->id)}}" class="btn btn-success btn-darken-3 tab-order btn-sm" > <i class="fa fa-eye"></i></a></span>
                                     </td>
                                            </tr>
                                            @php $i++; @endphp
                                            @endforeach
                                            @endif


                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

            @push('scripts')
            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

            <script >
                $(function () {


/////////////   REPLACE TR VALUE //////////////////////////
    $( "#tbody" ).sortable({
        revert: true,
        cancel: ".filter_text",
    update  : function(event, ui)
    {
    var post_order_ids = new Array();
    var type = 'playlist';
    $('#tbody tr').each(function(){
    post_order_ids.push($(this).data("post-id"));
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
    url:"{{url('admin/update-order')}}",
    method:"POST",
    data:{post_order_ids:post_order_ids,type:type},
    success:function(data)
    {
        if(data)
        {
            $(".alert-danger").hide();
            $(".alert-success ").show();
        }else{
            $(".alert-success").hide();
            $(".alert-danger").show();
        }
    }
    });
    }
    });


            });
            </script>
         @endpush

@endsection
