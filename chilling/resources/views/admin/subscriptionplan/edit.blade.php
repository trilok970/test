@extends('layouts.admin')
@section('title','Subscriptionplan Edit')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Subscription Plan</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/subscriptionplan')}}">Subscription Plan</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">

                   <div class="col-md-12">


                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url('admin/subscriptionplan/'.$subscriptionplan->id)}}" method="post" id="exampleValidation">
                    @csrf
                    @method("PUT")
                   <div class="card">


                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url('admin/subscriptionplan')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Subscription Plan" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Subscription Plan
                                        </a>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Title</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="{{$subscriptionplan->title}}">
                                        <span class="text-danger">{{$errors->first('title')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Price</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="price" name="price" placeholder="Enter Price" value="{{$subscriptionplan->price}}">
                                        <span class="text-danger">{{$errors->first('price')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Feature</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="feature" name="feature" placeholder="Enter Feature" value="{{$subscriptionplan->feature}}">
                                        <span class="text-danger">{{$errors->first('feature')}}</span>
                                    </div>
                                </div>



                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url('admin/subscriptionplan')}}" class="btn btn-danger resetBtn">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

          <script>
        $(document).ready(function(){


            /* validate */



        // $("#exampleValidation").validate({
        //     validClass: "success",
        //     rules: {
        //         title: {
        //             required: true
        //         },
        //         sub_title: {
        //             required: true
        //         },
        //         description: {
        //             required: true
        //         },
        //     },
        //     highlight: function(element) {
        //         $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        //     },
        //     success: function(element) {
        //         $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        //     },
        // });


     function readURL(input)
    {
        if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
        $('#image_preview').attr('src', e.target.result);

        $('#image_preview').hide();
        $('#image_preview').fadeIn(650);
        }
       reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function() {
    readURL(this);
    });



        });






    </script>
@endsection
