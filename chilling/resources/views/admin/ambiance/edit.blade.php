@extends('layouts.admin')
@section('title','Ambiance Edit')
@section('content')
@push('stylesheets')
<link rel="stylesheet" href="{{url('dist/css/selectize.css')}}" />
<link rel="stylesheet" href="{{url('dist/css/selectize.min.css')}}" />

@endpush
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Ambiance</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/ambiance')}}">Ambiance</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">

                   <div class="col-md-12">
            @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
            @if(session('error_message'))
                <p class="alert alert-danger">{{session('error_message')}}</p>
            @endif

                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url('admin/ambiance/'.$ambiance->id)}}" method="post" id="exampleValidation">
                    @csrf
                    @method("PUT")
                   <div class="card">


                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url('admin/ambiance')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Ambiance" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Ambiance
                                        </a>
                                    </div>
                            </div>



                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Title</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="{{$ambiance->title}}">
                                        <span class="text-danger">{{$errors->first('title')}}</span>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Audio File</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control" id="file" name="file" placeholder="Choose file" >
                                        <span class="text-danger">{{$errors->first('file')}}</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <audio controls>
                                            <source src="{{ $ambiance->file }}" type="audio/{{$ambiance->extension}}">
                                            Your browser does not support the ambiance element.
                                            </audio>
                                    </div>
                                </div>








                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url('admin/ambiance')}}" class="btn btn-danger resetBtn">Cancel</a>

                                </div>
                            </div>
                        </div>
                    </form>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
        @push('scripts')
        <script src="{{url('dist/js/selectize.min.js')}}"></script>
        <script src="{{url('dist/js/form-selectize.min.js')}}"></script>

          <script>

        $(document).ready(function(){

            /* validate */

            $('#select-beast').selectize({
                create: true,
                sortField: 'text'
            });


            function readURL1(input)
            {
                if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                $('#image_preview').attr('src', e.target.result);

                $('#image_preview').hide();
                $('#image_preview').fadeIn(650);
                }
            reader.readAsDataURL(input.files[0]);
                }
            }

            $("#image").change(function() {
            readURL1(this);
            });

        });



    </script>
    @endpush
@endsection
