@extends('layouts.admin')
@section('title','Dashboard')
@section('content')
@push('stylesheets')
<link rel="stylesheet" type="text/css" href="{{url('assets/libs/newchart/Chart.css')}}">

@endpush
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashbaord</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                @if(session('message'))
                    <p class="alert alert-success">{{session('message')}}</p>
                @endif
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">

                    <!-- Column -->
                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <a href="{{url('admin/user')}}">
                            <div class="card card-hover">
                                <div class="box bg-cyan text-center">
                                    <h1 class="font-light text-white"><i class="mdi mdi-account-circle"></i></h1>
                                    <h6 class="text-white" data-toggle='tooltip' title="Users"> Users ({{$user_count}}) </h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <a href="{{url('admin/user?verified=1')}}">
                            <div class="card card-hover">
                                <div class="box bg-primary text-center">
                                    <h1 class="font-light text-white"><i class="mdi mdi-account-circle"></i></h1>
                                    <h6 class="text-white" data-toggle='tooltip' title="Users">Verified Users ({{$verified_user_count}}) </h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <a href="{{url('admin/user-story')}}">
                            <div class="card card-hover">
                                <div class="box bg-secondary text-center">
                                    <h1 class="font-light text-white"><i class="mdi mdi-receipt"></i></h1>
                                    <h6 class="text-white" data-toggle='tooltip' title="User Story"> User Story ({{$user_story_count}}) </h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <a href="{{url('admin/category')}}">
                            <div class="card card-hover">
                                <div class="box bg-info text-center">
                                    <h1 class="font-light text-white"><i class="mdi mdi-library-plus"></i></h1>
                                    <h6 class="text-white" data-toggle='tooltip' title="Category"> Category ({{$category_count}}) </h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <a href="{{url('admin/audio')}}">
                            <div class="card card-hover">
                                <div class="box bg-success text-center">
                                    <h1 class="font-light text-white"><i class="mdi mdi-library-music"></i></h1>
                                    <h6 class="text-white" data-toggle='tooltip' title="Audio"> Audio ({{$audio_count}}) </h6>
                                </div>
                            </div>
                        </a>
                    </div>

                   <div class="col-md-6 col-lg-3 col-xlg-3">
                        <a href="{{url('admin/channel')}}">
                            <div class="card card-hover">
                                <div class="box bg-danger text-center">
                                    <h1 class="font-light text-white"><i class="mdi mdi-presentation"></i></h1>
                                    <h6 class="text-white" data-toggle='tooltip' title="Channels"> Channels ({{$channel_count}}) </h6>
                                </div>
                            </div>
                        </a>
                    </div>


                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <a href="{{url('admin/playlist')}}">
                            <div class="card card-hover">
                                <div class="box bg-warning text-center">
                                    <h1 class="font-light text-white"><i class="mdi mdi-play-circle"></i></h1>
                                    <h6 class="text-white" data-toggle='tooltip' title="Playlist"> Playlist ({{$playlist_count}}) </h6>
                                </div>
                            </div>
                        </a>
                    </div>







                </div>
                <!-- Chart-3 -->
                <div class="row">

                    <div class="col-md-12 text-right">
                        <input type="hidden" name="graph_url" id="graph_url" value="{{url('admin/graph-data')}}" />
                        <a class="btn btn-info pull-right graph" key_val="week" url="{{url('admin/graph-data/week')}}"  href="{{url('admin/playlist')}}">This Week</a>
                         <a class="btn btn-primary pull-right graph" key_val="month" url="{{url('admin/graph-data/month')}}" href="{{url('admin/playlist')}}">This Month</a>
                        <a class="btn btn-success pull-right graph" key_val="year" url="{{url('admin/graph-data/year')}}" href="{{url('admin/playlist')}}">This Year</a>
                    </div>

                    <div class="col-md-12 m-t-10">


                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                <div class="col-md-6">
                                    <h5 class="card-title">User Chart</h5>
                                    </div>
                                    <div class="col-md-6"> <a href="" class="btn btn-success pull-right user-export-url" data-toggle="tooltip" title="Export User List" style="float:right;">
                                        <span class="btn-label">
                                        <i class="fas fa-file-excel"></i>
                                        </span>
                                        Export User List
                                    </a>
                                    </div>
                                </div>


                                <div class="flot-chart">
                                    <!-- <div class="flot-chart-content" id="flot-line-chart"></div> -->
                                <canvas id="myChart" width="400" height="100"></canvas>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="card-title">Top Ten Listining Stories</h5>
                                        </div>
                                        <div class="col-md-6"> <a href="" class="btn btn-success pull-right top-ten-listening-stories-export-url" data-toggle="tooltip" title="Export Listining Stories" style="float:right;">
                                            <span class="btn-label">
                                            <i class="fas fa-file-excel"></i>
                                            </span>
                                            Export Listining Stories
                                        </a>
                                        </div>
                                </div>
                                <div class="flot-chart">
                                    <!-- <div class="flot-chart-content" id="flot-line-chart"></div> -->
                                    <canvas id="toptenaudio" width="400" height="100"></canvas>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="card-title">Top Ten Favourite Stories</h5>
                                    </div>
                                    <div class="col-md-6"> <a href="" class="btn btn-success pull-right top-ten-favourite-stories-export-url" data-toggle="tooltip" title="Export Favourite Stories" style="float:right;">
                                            <span class="btn-label">
                                            <i class="fas fa-file-excel"></i>
                                            </span>
                                            Export Favourite Stories
                                        </a>
                                    </div>
                                </div>
                                <div class="flot-chart">
                                    <!-- <div class="flot-chart-content" id="flot-line-chart"></div> -->
                                    <canvas id="toptenfavouriteaudio" width="400" height="100"></canvas>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="card-title">Top Ten Playlists</h5>
                                    </div>
                                    <div class="col-md-6"> <a href="" class="btn btn-success pull-right top-ten-playlist-export-url" data-toggle="tooltip" title="Export Playlist" style="float:right;">
                                            <span class="btn-label">
                                            <i class="fas fa-file-excel"></i>
                                            </span>
                                            Export Playlist
                                        </a>
                                    </div>
                                </div>
                                <div class="flot-chart">
                                    <!-- <div class="flot-chart-content" id="flot-line-chart"></div> -->
                                    <canvas id="toptenplaylists" width="400" height="100"></canvas>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="user-lables" id="user-lables" />
                <input type="hidden" data-url="{{url('admin/graph-data/year')}}" name="graph_data" id="graph_data"
                data-user-lables = ""
                data-user-val = ""
                data-user-count = ""

                data-top-ten-audios-values-lables = ""
                data-top-ten-audios-values-val = ""
                data-top-ten-audios-values-count = ""

                data-favourites-audios-values-lables = ""
                data-favourites-audios-values-val = ""
                data-favourites-audios-values-count = ""

                data-top-ten-playlist-values-lables = ""
                data-top-ten-playlist-values-val = ""
                data-top-ten-playlist-values-count = ""

                data-value="10"



                />

                <!-- End chart-3


                -->
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

            @push('scripts')
    <script src="{{url('assets/libs/newchart/Chart.js')}}" ></script>


           <script>
            $(document).ready(function(){
                var url = $("#graph_data").data('url');

                getDefaultChart(url,"year");
                $(".graph").click(function(evt){
                    evt.preventDefault();
                    var key_val = $(this).attr('key_val');
                    var url = $(this).attr('url');

                    getDefaultChart(url,key_val);


                })
            });
function convertText(ele) {
    console.log(ele);
    console.log(">>>>>"+$(ele).text());
  return $(ele).text();
}
function getDefaultChart(url,key_val)
{

    $.get(url,key_val,function(out){
    if(out.result == 1)
    {
        result = JSON.stringify(out);
        usermonthdatakeys = JSON.stringify(out.usermonthdatakeys);
        usermonthdata = JSON.stringify(out.usermonthdata);
        user_count = JSON.stringify(out.user_count);

        top_ten_audios_keys = JSON.stringify(out.top_ten_audios_keys);
        top_ten_audios_values = JSON.stringify(out.top_ten_audios_values);
        top_ten_audios_count = JSON.stringify(out.top_ten_audios_count);

        favourites_audios_keys = JSON.stringify(out.favourites_audios_keys);
        favourites_audios_values = JSON.stringify(out.favourites_audios_values);
        favourites_audios_count = JSON.stringify(out.favourites_audios_count);

        top_ten_playlist_keys = JSON.stringify(out.top_ten_playlist_keys);
        top_ten_playlist_values = JSON.stringify(out.top_ten_playlist_values);
        top_ten_playlist_count = JSON.stringify(out.top_ten_playlist_count);

        user_export_url = (out.user_export_url);
        top_ten_listening_stories_url = (out.top_ten_listening_stories_url);
        top_ten_favourite_stories_url = (out.top_ten_favourite_stories_url);
        top_ten_playlist_export_url = (out.top_ten_playlist_export_url);

        console.log("result.user_export_url >> "+user_export_url);
        console.log("result.top_ten_listening_stories_url >> "+top_ten_listening_stories_url);


        // console.log(result);
            console.log("result.usermonthdatakeys >> "+usermonthdatakeys);
            console.log("result.usermonthdatakeys >> "+out.usermonthdatakeys);
            console.log("result.usermonthdata >> "+usermonthdata);
            console.log("result.user_count >> "+user_count);

            console.log("result.top_ten_audios_keys >> "+top_ten_audios_keys);
            console.log("result.top_ten_audios_values >> "+top_ten_audios_values.text);
            console.log("result.top_ten_audios_count >> "+top_ten_audios_count);

            console.log("result.favourites_audios_keys >> "+favourites_audios_keys);
            console.log("result.favourites_audios_values >> "+favourites_audios_values);
            console.log("result.favourites_audios_count >> "+favourites_audios_count);

            console.log("result.top_ten_playlist_keys >> "+top_ten_playlist_keys);
            console.log("result.top_ten_playlist_values >> "+top_ten_playlist_values);
            console.log("result.top_ten_playlist_count >> "+top_ten_playlist_count);

            $("#user-lables").val(usermonthdatakeys);
            $("#graph_data").attr('data-user-lables',usermonthdatakeys);



            $("#graph_data").attr('data-user-val',usermonthdata);
            $("#graph_data").attr('data-user-count',user_count);

            $("#graph_data").attr('data-top-ten-audios-values-lables',top_ten_audios_keys);
            $("#graph_data").attr('data-top-ten-audios-values-val',top_ten_audios_values);
            $("#graph_data").attr('data-top-ten-audios-values-count',top_ten_audios_count);

            $("#graph_data").attr('data-favourites-audios-values-lables',favourites_audios_keys);
            $("#graph_data").attr('data-favourites-audios-values-val',favourites_audios_values);
            $("#graph_data").attr('data-favourites-audios-values-count',favourites_audios_count);

            $("#graph_data").attr('data-top-ten-playlist-values-lables',top_ten_playlist_keys);
            $("#graph_data").attr('data-top-ten-playlist-values-val',top_ten_playlist_values);
            $("#graph_data").attr('data-top-ten-playlist-values-count',top_ten_playlist_count);

            $(".user-export-url").attr('href',user_export_url);
            $(".top-ten-listening-stories-export-url").attr('href',top_ten_listening_stories_url);
            $(".top-ten-favourite-stories-export-url").attr('href',top_ten_favourite_stories_url);
            $(".top-ten-playlist-export-url").attr('href',top_ten_playlist_export_url);



            $("#graph_data").load(location.href+" #graph_data>*","");
             makeGraph();
    }

    });
}

function makeGraph()
{
    var user_lables  = JSON.parse($("#graph_data").attr('data-user-lables'));
    var user_val     = JSON.parse($("#graph_data").attr('data-user-val'));
    var user_count   = JSON.parse($("#graph_data").attr('data-user-count'));

    var top_ten_audios_values_lables  = JSON.parse($("#graph_data").attr('data-top-ten-audios-values-lables'));
    var top_ten_audios_values_val  = JSON.parse($("#graph_data").attr('data-top-ten-audios-values-val'));
    var top_ten_audios_values_count  = JSON.parse($("#graph_data").attr('data-top-ten-audios-values-count'));


    var favourites_audios_values_lables  = JSON.parse($("#graph_data").attr('data-favourites-audios-values-lables'));
    var favourites_audios_values_val  = JSON.parse($("#graph_data").attr('data-favourites-audios-values-val'));
    var favourites_audios_values_count  = JSON.parse($("#graph_data").attr('data-favourites-audios-values-count'));

    var top_ten_playlist_values_lables  = JSON.parse($("#graph_data").attr('data-top-ten-playlist-values-lables'));
    var top_ten_playlist_values_val  = JSON.parse($("#graph_data").attr('data-top-ten-playlist-values-val'));
    var top_ten_playlist_values_count  = JSON.parse($("#graph_data").attr('data-top-ten-playlist-values-count'));

    console.log("user-lables >> "+ user_lables);
    console.log("user_val >> "+user_val);

    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: user_lables,
            datasets: [{
                label: user_count,
                data: user_val,
                backgroundColor: [
                    'rgba(255, 255, 0, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'

                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    scaleLabel: {
                    display: true,
                    labelString: 'No of users'
                }
                }]
            }
        }
    });

    var ctx = document.getElementById('toptenaudio').getContext('2d');
    var toptenaudio = new Chart(ctx, {
        type: 'line',
        data: {
            labels: top_ten_audios_values_lables,
            datasets: [{
                label: top_ten_audios_values_count,
                data: top_ten_audios_values_val,
                backgroundColor: [
                    'rgba(255, 0, 0, 0.6);',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 10, 784, 0.2)',
                    'rgba(111, 170, 520, 0.2)',
                    'rgba(1, 1, 1, 0.2)',

                ],
                borderColor: [
                    'rgba(0, 255, 0, 0.3)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    scaleLabel: {
                    display: true,
                    labelString: 'No of views'
                }
                }],
                xAxes: [{
                    scaleLabel: {
                    display: true,
                    labelString: 'Stories titles'
                }
                }]
            }
        }
    });
    toptenaudio.render();

    var ctx = document.getElementById('toptenfavouriteaudio').getContext('2d');
    var toptenfavouriteaudio = new Chart(ctx, {
        type: 'line',
        data: {
            labels: favourites_audios_values_lables,
            datasets: [{
                label: favourites_audios_values_count,
                data: favourites_audios_values_val,
                backgroundColor: [
                    'rgba(0, 255, 0, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 10, 784, 0.2)',
                    'rgba(111, 170, 520, 0.2)',
                    'rgba(1, 1, 1, 0.2)',
                    'rgba(100, 1, 1, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    scaleLabel: {
                    display: true,
                    labelString: 'No of favourite'
                }
                }],
                xAxes: [{
                    scaleLabel: {
                    display: true,
                    labelString: 'Stories titles'
                }
                }]
            }
        }
    });
    toptenfavouriteaudio.render();

    var ctx = document.getElementById('toptenplaylists').getContext('2d');
    var toptenplaylists = new Chart(ctx, {
        type: 'line',
        data: {
            labels: top_ten_playlist_values_lables,
            datasets: [{
                label: top_ten_playlist_values_count,
                data: top_ten_playlist_values_val,
                backgroundColor: [
                    'rgba(0, 0, 255, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 10, 784, 0.2)',
                    'rgba(111, 170, 520, 0.2)',
                    'rgba(1, 1, 1, 0.2)',
                    'rgba(100, 1, 1, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    scaleLabel: {
                    display: true,
                    labelString: 'No of views'
                }
                }],
                xAxes: [{
                    scaleLabel: {
                    display: true,
                    labelString: 'Playlist titles'
                }
                }]

            }
        }
    });
    toptenplaylists.render();

}

</script>
            @endpush

@endsection
