var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http, {pingInterval: 5000, pingTimeout: 3000, cookie: false});
var request = require('request');
var mysql = require('mysql');
const fs = require('fs');
var async = require('async');
const {base64encode, base64decode} = require('nodejs-base64');
const Promise = require('promise');
var dateTime = require('node-datetime');
var basepath = "http://demo2server.com/kweeke/";
//var basepath = "http://192.168.0.150/kweeke/";
//var basepath = "http://testing.demo2server.com/kweeke/";
//var basepath = "http://testing.demo2server.com/kweeke-testing/";
var sockets = {};
var onlineUser = [];

http.listen(9138, function () {
    console.log('listening on *:9138');
});

// demo2server.com
var con = mysql.createConnection({
 host: "23.239.31.218",
 user: "SysAdmin",
 password: "9H3rtzios",
 database: "kweeke"
 });


//Rakesh's localhost
/*var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "kweeke"
});*/


//testing.demo2server.com
/*var con = mysql.createConnection({
 host: "localhost",
 user: "root",
 password: "<nhzIndia@2018>",
 database: "kweeke"
 });
 */

//QA testing.demo2server.com
/*var con = mysql.createConnection({
 host: "localhost",
 user: "root",
 password: "<nhzIndia@2018>",
 database: "kweeke_testing"
 });
 */

con.connect(function (err) {
    if (err)
        throw err;
    console.log("Connected Successfully!");
});

io.on('connection', function (socket) {
    var device_id = socket.handshake.query.device_id;
    var user_id = socket.handshake.query.user_id;

    //console.log("user_id", user_id);
    if (onlineUser.indexOf(user_id) === -1) {
        onlineUser.push(user_id);
    }
    console.log("onlineUser", onlineUser);
    var newobj = {};
    newobj[device_id] = socket;
    var uid = user_id;
    if (sockets[uid] === undefined) {
        sockets[uid] = [];
    }
    sockets[uid].push(newobj);

    con.query('Select is_read, GROUP_CONCAT(id SEPARATOR ",") as read_status_id, CASE WHEN user_id = "' + user_id + '" THEN other_user_id ELSE user_id END AS result1 from message where ( is_read="-1" ) and (other_user_id ="' + user_id + '" OR user_id ="' + user_id + '") group by result1', function (error, results, getMessageCallback) {
        if (error)
            throw error;
        if (results.length > 0) {

            var getMessageCallback = function (data, callback) {

                async.map(data, function (valmmm, callback) {

                    if (sockets[valmmm.result1] != undefined) {
                        con.query("update message set is_read='0' where id IN (" + valmmm.read_status_id + ") ");

                        var parts = valmmm.read_status_id.split(",");
                        var resultId = parts[parts.length - 1]; //

                        var rowschatQuerydata = {'msg_id': valmmm.read_status_id, 'is_read': 0, 'user_id': valmmm.result1, 'other_user_id': user_id};


                        sendMessageOtherfSync(valmmm.result1, rowschatQuerydata, 'read_message_update_response');

                    }

                }, callback);

            };

            getMessageCallback(results);

        }

    });


    socket.on('send_message', function (msg) {
        var is_read = '-1';
        if (onlineUser.indexOf(msg.other_user_id) > -1) {
            is_read = '0';
        }
        var formdata = {
            user_id: msg.user_id,
            other_user_id: msg.other_user_id,
            msg: msg.msg,
            message_type: msg.message_type,
            thumb_image: msg.thumb_image,
            other_data: msg.other_data,
            is_read: is_read
                    //token: msg._token,
        };
        console.log("formdata==", formdata);
        saveMessage(formdata);

    });
    socket.on('send_group_message', function (requestData) {
        console.log("requestData==", requestData);
        if (requestData.message_type == "TEXT") {
            requestData.msg = base64encode(requestData.msg);
        }
        var dt = dateTime.create();
        var datetime = dt.format('Y-m-d H:M:S');
        var message = {"status": false, "code": 400, "requestData": requestData, "responseData": [], "msg": "Something went wrong. Try again"};
        var msg_sql = "INSERT INTO `message` (user_id, other_user_id,msg,thumb_image,other_data,message_type,conversation_id,created_at,updated_at) VALUES (" + requestData.user_id + "," + requestData.group_id + ",'" + requestData.msg + "','" + requestData.thumb_image + "','" + requestData.other_data + "','" + requestData.message_type + "','-1','" + datetime + "','" + datetime + "')";
        con.query(msg_sql, function (err, result) {
            if (err)
                throw err;
            else {
                getLastSentGroupMsg(result.insertId).then(function (lastGroupMsg) {
                    message.status = true;
                    message.code = 200;
                    message.msg = "Message sent successfully";
                    console.log("user id", requestData.user_id);
                    message.user_status = lastGroupMsg[0].user_status;
                    message.responseData = lastGroupMsg[0];
                    socket.emit('multi_group_user', message);
                    //requestData.group_member_id = [8, 98, 113];
                    var split_members = requestData.group_member_id.split(',');
                    split_members.forEach(function (value, index) {
                        console.log("value is ",value);
                        if (value != undefined) {
                            var is_read = -1;
                            if (onlineUser.indexOf(value) > -1) {
                                is_read = 0;
                            }
                            message.msg = "You have received a new message.";
                            sendGroupMessage(value, message, 'receive_group_message', function () {
                                var insert_sql = "INSERT INTO group_message_read_status (group_id, group_member_id,msg_id,is_read,sent_at,delivered_at,read_at) VALUES (" + requestData.group_id + ", " + value + "," + result.insertId + "," + is_read + ",'" + datetime + "','" + datetime + "','" + datetime + "')";
                                con.query(insert_sql, function (err) {
                                    if (err) {
                                        throw err;
                                    }
                                });
                            });
                        }

                    });
                });
            }
        });
    });
    socket.on('get_group_user_thread', function (data) {
        var message = {"status": false, "code": 400, "first_message_id": 0, "total_record": 0, "type": data.type, "requestData": data, "responseData": [], "type": data.type, "msg": "Something went wrong. Try again"};
        getUserStatus(data.user_id).then(function (user_status) {
            getGroupHistory(data, function (message) {
                message.user_status = user_status;
                //message.responseData = GroupHistory;
                message.status = true;
                message.code = 200;
                message.msg = "Group chat data";
                console.log('get_group_user_thread_response', message);
                socket.emit('get_group_user_thread_response', message);
            })
        });
    });
    socket.on('read_group_message_update', function (data) {
        var dt = dateTime.create();
        var datetime = dt.format('Y-m-d H:M:S');
        var message = {status: false, code: 400, requestData: data, responseData: [], msg: "Something went wrong. Try again"};
        getUserStatus(data.user_id).then(function (user_status) {
            con.query("UPDATE `group_message_read_status` SET `is_read`=1,`read_at`='" + datetime + "' WHERE group_member_id=" + data.user_id + " AND msg_id IN (" + data.msg_id + ") ", function (err) {
                if (err) {
                    throw err;
                } else {
                    message.user_status = user_status;
                    message.status = true;
                    message.code = 200;
                    message.responseData = data;
                    message.msg = "Messages read successfully.";
                    console.log('read_group_message_update_response', message);
                    socket.emit('read_group_message_update_response', message);
                }
            });
        });
    });
    socket.on('get_message_history', function (data) {
        var user_status = "";
        getUserStatus(data.user_id).then(function (response) {
            user_status = response;
        });
        getInbox(data, function (err, data) {
            if (err) {
                // error handling code goes here
                console.log("ERROR : ", err);
                socket.emit('get_message_history_response', err);
            } else {
                // code to execute on data retrieval
                data.user_status = user_status;
                console.log('get_message_history_response====', data);
                socket.emit('get_message_history_response', data);
            }
            console.log('get_message_history_response', data);
        });
    });
    // update read msg status
    socket.on('read_message_update', function (data) {
        console.log("read_message_update", data);
        var messageid = data.msg_id;
        var is_read_status = data.is_read;
        var split_msg = messageid.split(',');

        var select_sql = "UPDATE `message` set is_read=" + is_read_status + "  WHERE id in (" + split_msg + ")";
        //console.log(select_sql);
        con.query(select_sql, function (err, result) {
            if (err) {
                throw err;
            } else {
                console.log("read_message_update_response", data);
                sendMessageOtherfSync(data.other_user_id, data, 'read_message_update_response');
                /*if (split_msg.length == 1) {
                 sendMessageOtherfSync(data.other_user_id, data, 'read_message_update_response');
                 } else {
                 sendMessageOtherfSync(data.other_user_id, data, 'read_message_update_response');
                 } */
            }
        });
    });
// Check user status
    socket.on('check_user_status', function (data) {
        var message = {};
        var blocked_qry = "SELECT users.id as user_id,status, (SELECT count(*) FROM user_block where user_id=users.id and other_id=" + data.other_user_id + ") as is_blocked_by_me, (SELECT count(*) FROM user_block where user_id=" + data.other_user_id + " and other_id=users.id) as is_blocked FROM users where id=" + data.user_id;
        con.query(blocked_qry, function (err, result) {
            if (err) {
                message = {"status": false, "code": 400, "responseData": [], "requestData": data, "msg": "Something went wrong. Try again"};
            } else {
                getUserStatus(data.other_user_id).then(function (response) {
                    message = {"status": true, "code": 200, "responseData": {
                            "is_blocked": (result[0].is_blocked >= 1) ? true : false, "is_blocked_by_me": (result[0].is_blocked_by_me >= 1) ? true : false, "other_user_id": data.other_user_id, "other_user_status": response, "status": result[0].status, "user_id": data.user_id
                        }, "requestData": data, "msg": "User status"};
                    //console.log("final data to emit", message);
                    socket.emit('check_user_status_response', message);
                });
            }
        });
    });
// Get other profile
    socket.on('get_other_profile', function (data) {
        console.log("get profile request data ======", data);
        var message = {};
        var get_other_profile = "select u.id,u.country_code,c.name as country_name, u.name, u.profile_picture,u.backgroud_picture, u.email,u.phone_code, u.country_code, u.phone_number, IFNULL(u.username,'') as username from users u LEFT JOIN countries c  ON u.country_code = c.sortname where u.id = " + data.user_id;
        con.query(get_other_profile, function (err, result) {
            if (err) {
                throw err;
                message = {"status": false, "code": 400, "responseData": [], "requestData": data, "msg": "Something went wrong. Try again"};
            } else {
                //console.log("get profile response ======", result);
                var user_status = "";
                getUserStatus(data.user_id).then(function (response) {
                    user_status = response;
                });

                var userInfoData = {id: result[0].id, name: result[0].name, profile_picture: (result[0].profile_picture == "") ? basepath + "public/img/avatar.jpg" : result[0].profile_picture, backgroud_picture: (result[0].backgroud_picture == "") ? basepath + "public/img/default_profile_background.png" : result[0].backgroud_picture, email: result[0].email, phone_code: result[0].phone_code, country_code: result[0].country_code, phone_number: result[0].phone_number, username: result[0].username, is_friend: 0, country_name: (result[0].country_name) ? result[0].country_name : ""};
                getFavourites(data.user_id, function (err, favResponse) {
                    if (err) {
                        socket.emit('get_other_profile_response', err);
                    } else {

                        message = {"status": true, "code": 200, "responseData": {
                                userInfo: userInfoData, reviews: [], favourites: favResponse
                            }, "requestData": data, "msg": "User data.", user_status: user_status};
                        console.log("get_other_profile_response", message);
                        socket.emit('get_other_profile_response', message);
                    }
                });
            }
        });
    });

    // get msg history

    socket.on('get_user_thread', function (data) {
        var user_status = "";
        getUserStatus(data.user_id).then(function (response) {
            user_status = response;
        });

        getChatHistory(data, function (err, data) {
            if (err) {
                // error handling code goes here
                console.log("ERROR : ", err);
                socket.emit('get_user_thread_response', err);
            } else {
                // code to execute on data retrieval
                data.user_status = user_status;
                socket.emit('get_user_thread_response', data);
            }
            console.log('get_user_thread_response===========', data);
        });
    });

    socket.on('search', function (data) {
        console.log("search request data=", data);
        message = {"status": true, "code": 200, "requestData": data};
        searchConversations(data).then(function (conversations) {
            message = {"status": true, "code": 200, "requestData": data};
            searchFriends(data).then(function (friends) {
                //message.responseData.friends = JSON.parse(JSON.stringify(friends));
                console.log("search friends data=", message);
                searchPeoples(data).then(function (peoples) {
                    message.responseData = {conversations: JSON.parse(JSON.stringify(conversations)),
                        friends: JSON.parse(JSON.stringify(friends)),
                        peoples: JSON.parse(JSON.stringify(peoples))
                    };
                    //console.log("search peoples data=",message);
                    console.log("search response data console=", message.responseData);
                    socket.emit("search_response", message);
                });
            });
        });
    });


    socket.on('clear_chat', function (data) {
        var update_sql = "UPDATE message SET deleted_user_id = IF(deleted_user_id > 0 && deleted_user_id != " + data.user_id + ", -1, " + data.user_id + ") WHERE conversation_id =" + data.conversation_id;
        con.query(update_sql, function (err, result) {
            if (err)
                socket.emit('clear_chat_response', err);
            else {
                socket.emit('clear_chat_response', {
                    status: true,
                    msg: "Chat cleared successfully.",
                    conversation_id: data.conversation_id
                });
            }
        });
    });
    socket.on('user_block_unblock', function (data) {
        con.query("SELECT count(*) as record_exist FROM `user_block` WHERE user_id=" + data.user_id + "  AND other_id = " + data.other_user_id, function (err, result) {
            if (err) {
                socket.emit('user_block_unblock_response', err);
            } else {
                if (result[0].record_exist > 0) {
                    con.query("DELETE FROM `user_block` WHERE user_id = " + data.user_id + " AND other_id = " + data.other_user_id, function (delete_err) {
                        if (err) {
                            socket.emit('user_block_unblock_response', delete_err);
                        } else {

                            console.log("User unblocked successfully.");
                            var responseData = {
                                status: true,
                                msg: "User unblocked successfully.",
                                user_id: data.user_id,
                                other_user_id: data.other_user_id,
                                block_status: 0
                            };
                            socket.emit('user_block_unblock_response', responseData);
                            sendMessageOtherfSync(data.other_user_id, responseData, 'user_block_unblock_response');
                        }
                    });

                } else {
                    var dt = dateTime.create();
                    var datetime = dt.format('Y-m-d H:M:S');

                    con.query("INSERT INTO `user_block` (`user_id`, `other_id`,`created_at`,`updated_at`) VALUES (" + data.user_id + ", " + data.other_user_id + ",'" + datetime + "','" + datetime + "')", function (insert_err) {
                        if (err) {
                            socket.emit('user_block_unblock_response', insert_err);
                        } else {
                            console.log("User blocked successfully.");
                            var responseData = {
                                status: true,
                                msg: "User blocked successfully.",
                                user_id: data.user_id,
                                other_user_id: data.other_user_id,
                                block_status: 1
                            };
                            socket.emit('user_block_unblock_response', responseData);
                            sendMessageOtherfSync(data.other_user_id, responseData, 'user_block_unblock_response');
                        }
                    });
                }
            }
        });
    });

    // Check group status
    socket.on('check_user_group_status', function (data) {console.log('check_user_group_status called!!');
        var message = {};
        var check_qry = "SELECT count(*) as is_group_exist, (SELECT count(*) FROM group_member where userid=" + data.user_id + " and group_id="+data.group_id+") as is_member, (SELECT count(*) FROM users where id= (SELECT tb.merchant_id FROM `table_bookings` tb left join `group` g on g.table_booking_no=tb.booking_no WHERE g.id=" + data.group_id + " ) ) as is_restaurant_exist, 0 as is_order_complete FROM `group` where id=" + data.group_id;
        console.log("check_user_group_status check_qry =", check_qry);
        con.query(check_qry, function (err, result) {
            if (err) {
                message = {"status": false, "code": 400, "responseData": [], "requestData": data, "msg": "Something went wrong. Try again"};
                console.log("check_user_group_status response =", message);
            } else {
                message = {"status": true, "code": 200, "responseData": result[0], "requestData": data, "msg": "Group User status"};
                console.log("check_user_group_status response =", message);
                socket.emit('check_user_group_status_response', message);
            }
        });
    });

});
function sendMessageSelfSync(sender_user_id, device_id, message, callback) {

    // console.log("sendMessageSelfSync==", sender_user_id);
    //console.log("sockets[sender_user_id]", sockets[sender_user_id]);
    console.log("sockets connected ==", sockets[sender_user_id]);
    if (sockets[sender_user_id] != undefined) {
        var lengths = sockets[sender_user_id].length - 1;
        var newArraySelf = new Array();
        for (var i in sockets[sender_user_id]) {
            //if (Object.keys(sockets[user_id][i]) != device_id) {
            var sid = Object.keys(sockets[sender_user_id][i]);
            if (!newArraySelf.includes(sid)) {
                if (sockets[sender_user_id][i][sid] != undefined) {
                    sockets[sender_user_id][i][sid].emit('multi_user', message);
                }
                //}
                if (lengths == i) {
                    callback('true');
                }
            }
            newArraySelf.push(sid);
        }
    }
}
function getGroupHistory(requestData, callback) {
    var select_sql = "SELECT m.*,u.name,u.id as userid,u.status as user_status,group_info.id as group_id,group_info.group_name,group_info.image as group_pic ";
    select_sql += "FROM message m INNER JOIN users u ON m.user_id = u.id INNER JOIN `group` group_info ON m.other_user_id = group_info.id ";
    select_sql += "WHERE m.other_user_id=" + requestData.group_id;
    if (typeof requestData.last_id != 'undefined' && requestData.last_id > 0) {
        if (typeof requestData.type != 'undefined' && requestData.type == "before") {
            select_sql += " AND m.id < " + requestData.last_id;
        } else if (typeof requestData.type != 'undefined' && requestData.type == "after") {
            select_sql += " AND m.id > " + requestData.last_id;
        }
    }
    select_sql += " AND  m.conversation_id = -1 AND m.deleted_user_id != -1 AND m.deleted_user_id != " + requestData.user_id + "  ORDER BY m.id DESC LIMIT " + requestData.limit;
    console.log("select_sql==", select_sql);
    con.query(select_sql, function (err, messages) {
        if (err) {
            callback(err, messages);
        } else {
            messages.forEach(function (result) {
                if (result.message_type == "TEXT") {
                    result.msg = base64decode(result.msg);
                }
            });
            if (typeof messages !== 'undefined' && messages.length > 0) {
                message = {"status": true, "code": 200, "first_message_id": 0, "total_record": 0, "type": requestData.type, "responseData": JSON.parse(JSON.stringify(messages)), "requestData": requestData, "msg": "Message chat history"};
                con.query("SELECT count(*) as unread_msg_count FROM `group_message_read_status` where group_member_id =" + requestData.user_id + " AND group_id="+requestData.group_id+" AND is_read != 1", function (err, result) {
                    if (err) {
                        callback(err, message);
                    } else {
                        //message.total_unread_message = result[0].unread_msg_count;
                        con.query("SELECT id FROM `message` WHERE conversation_id =-1 and other_user_id=" + requestData.group_id + " ORDER BY `id` asc limit 1", function (err, result) {
                            if (err) {
                                callback(err, message);
                            } else {
                                message.first_message_id = result[0].id;
                                con.query("SELECT count(*) as total_record FROM `message` where conversation_id =-1 and other_user_id=" + requestData.group_id, function (err, result) {
                                    if (err) {
                                        callback(err, message);
                                    } else {
                                        message.total_record = result[0].total_record;
                                        //console.log('actual response is =',message);
                                        callback(message);
                                    }
                                });
                            }
                        });
                    }
                });
            }else{
            	message = {"status": true, "code": 200, "first_message_id": 0, "total_record": 0, "type": requestData.type, "responseData": JSON.parse(JSON.stringify(messages)), "requestData": requestData, "msg": "Message chat history"};
            	callback(message);
            }
        }
    });
}

function sendMessageOtherfSync(other_user_id, message, call_event, callback) {
    console.log("sendMessageOtherfSync", other_user_id);
    var sdata = sockets[other_user_id];
    console.log("other user id----", other_user_id);
    if (sdata != undefined) {
        var newArray = new Array();
        for (var i in sdata) {
            var sid = Object.keys(sdata[i]);
            console.log("sid------" + sid);
            if (!newArray.includes(sid)) {
                if (sockets[other_user_id][i][sid] != undefined) {
                    sockets[other_user_id][i][sid].emit(call_event, message);
                }
                /*if (sockets[other_user_id].length - 1 == i) {
                    callback('true');
                }*/
            }

            console.log("newArray", newArray);
            newArray.push(sid);
        }
    }
}
function sendGroupMessage(user_id, message, call_event, callback) {
    var sdata = sockets[user_id];
    if (sdata != undefined) {
        var newArray = new Array();
        for (var i in sdata) {
            var sid = Object.keys(sdata[i]);
            if (!newArray.includes(sid)) {
                if (sockets[user_id][i][sid] != undefined) {
                    sockets[user_id][i][sid].emit(call_event, message);
                }
                if (sockets[user_id].length - 1 == i) {
                    callback('true');
                }
            }
            newArray.push(sid);
        }
    }
}

function saveMessage(formdata) {
    var user_id = formdata.user_id;
    var other_user_id = formdata.other_user_id;
    var msg = formdata.msg;
    var thumb_image = formdata.thumb_image;
    var message_type = formdata.message_type;
    var other_data = formdata.other_data;
    var is_read = formdata.is_read;
    conversion_id = 0;
    var dt = dateTime.create();
    var datetime = dt.format('Y-m-d H:M:S');
    var select_sql = "SELECT id FROM `conversations` WHERE (user_id=" + user_id + " AND other_user_id=" + other_user_id + ") OR (user_id=" + other_user_id + " AND other_user_id=" + user_id + ")";
    //console.log(select_sql);
    con.query(select_sql, function (err, result) {
        if (err)
            throw err;
        console.log("already exist length==", result.length);
        if (result.length <= 0) {
            var insert_sql = "INSERT INTO `conversations` (user_id, other_user_id,created_at,updated_at) VALUES (" + user_id + "," + other_user_id + ",'" + datetime + "','" + datetime + "')";
            con.query(insert_sql, function (err, result) {
                if (err)
                    throw err;
                else {
                    conversion_id = result.insertId;
                    if (message_type == "TEXT") {
                        msg = base64encode(msg);
                    }
                    var msg_sql = "INSERT INTO `message` (user_id, other_user_id,msg,thumb_image,other_data,message_type,conversation_id,created_at,updated_at) VALUES (" + user_id + "," + other_user_id + ",'" + msg + "','" + thumb_image + "','" + other_data + "','" + message_type + "'," + conversion_id + ",'" + datetime + "','" + datetime + "')";
                    con.query(msg_sql, function (err, result) {
                        if (err)
                            throw err;
                        else {
                            var user_status = "";
                            getUserStatus(user_id).then(function (response) {
                                user_status = response;
                            });
                            var singMsgResponse = "";
                            getSingleMsg(conversion_id, function (err, data) {
                                if (err) {
                                    throw err;
                                } else {
                                    // code to execute on data retrieval
                                    data.user_status = user_status;
                                    singMsgResponse = data;
                                    singMsgResponse.requestData = formdata;
                                    console.log("singMsgResponse========", singMsgResponse);
                                    sendMessageSelfSync(formdata.user_id, formdata.device_id, singMsgResponse, function () {
                                        console.log("self");
                                        sendMessageOtherfSync(formdata.other_user_id, singMsgResponse, 'receive_message');
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            conversion_id = result[0].id;
            //update updated_at time
            var update_sql = "UPDATE `conversations` set updated_at='" + datetime + "' where id = " + conversion_id;
            con.query(update_sql, function (err, result) {
                if (err)
                    throw err;
                else {
                    if (message_type == "TEXT") {
                        msg = base64encode(msg);
                    }
                    var msg_sql = "INSERT INTO `message` (user_id, other_user_id,msg,thumb_image,other_data,message_type,conversation_id,created_at,updated_at) VALUES (" + user_id + "," + other_user_id + ",'" + msg + "','" + thumb_image + "','" + other_data + "','" + message_type + "'," + conversion_id + ",'" + datetime + "','" + datetime + "')";
                    console.log("msg_sql==", msg_sql);
                    con.query(msg_sql, function (err, result) {
                        if (err)
                            throw err;
                        else {
                            var user_status = "";
                            getUserStatus(user_id).then(function (response) {
                                user_status = response;
                            });
                            var singMsgResponse = "";
                            getSingleMsg(conversion_id, function (err, data) {
                                if (err) {
                                    throw err;
                                } else {
                                    // code to execute on data retrieval
                                    data.user_status = user_status;
                                    singMsgResponse = data;
                                    singMsgResponse.requestData = formdata;
                                    console.log("singMsgResponse========", singMsgResponse);
                                    sendMessageSelfSync(formdata.user_id, formdata.device_id, singMsgResponse, function () {
                                        console.log("self");
                                        sendMessageOtherfSync(formdata.other_user_id, singMsgResponse, 'receive_message');
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

function getInbox(requestData, callback) {
    var user_id = requestData.user_id;
    var limit = requestData.limit;
    var select_sql = "SELECT m.*,u.name,u.id as userid,other_user.id as other_userid,other_user.name as other_user_name FROM message m ";
    select_sql += "INNER JOIN users u ON m.user_id = u.id INNER JOIN users other_user ON m.other_user_id = other_user.id INNER JOIN conversations c ON m.conversation_id = c.id ";
    select_sql += "WHERE m.id IN ( SELECT MAX(id) FROM message ";
    select_sql += "WHERE user_id = " + user_id + " OR other_user_id =" + user_id + " GROUP BY conversation_id) AND m.deleted_user_id != -1 AND m.deleted_user_id != " + user_id + " ORDER BY m.id DESC ";
    var total_conversations = select_sql;
    select_sql += " LIMIT " + limit;
    console.log("select_sql====", select_sql);
    var message = {"status": false, "code": 400, "total_record": 0, "total_unread_message": 0, "responseData": [], "requestData": requestData, "msg": "Something went wrong. Try again"};
    con.query(select_sql, function (err, conversations) {
        if (err) {
            throw err;
//            callback(err, message);
        } else {
            con.query(total_conversations, function (err, t_conversation) {
                if (err) {
                    callback(err, message);
                } else {
                    var total_unread_message = 0;
                    if(typeof conversations !== 'undefined' && conversations.length > 0){
                        conversations.forEach(function (result, index) {
                            result.user_info = {
                                'userid': result.userid,
                                'name': result.name
                            };
                            result.other_user_info = {
                                'userid': result.other_userid,
                                'name': result.other_user_name
                            };
                            var unread_message = 0;
                            con.query("SELECT count(*) as unread_msg_count FROM `message` where conversation_id=" + result.conversation_id + " AND user_id != " + user_id + " AND is_read !=1 AND deleted_user_id != -1 AND deleted_user_id != "+ user_id, function (err, unread_result) {
                                if (err) {
                                    callback(err, message);
                                }
                                result.unread_message = unread_result[0].unread_msg_count;

                                total_unread_message = total_unread_message + unread_result[0].unread_msg_count;
                                message = {"status": true, "code": 200, "total_record": t_conversation.length, "responseData": JSON.parse(JSON.stringify(conversations)), "requestData": requestData, "msg": "Chat history list"};
                                if (index === conversations.length - 1) {
                                    message.total_unread_message = total_unread_message;
                                    callback(null, message);
                                }
                            });
                            result.unread_message = unread_message;
                            if (result.message_type == "TEXT") {
                                result.msg = base64decode(result.msg)
                            }
                        });
                    }else{
                        message.status=true;
                        message.msg = "No record found.";
                        callback(null, message);
                    }
                }

            });
        }
    });
}
function getChatHistory(requestData, callback) {
    var select_sql = "SELECT m.*,u.name,u.id as userid,u.status as user_status,other_user.id as other_userid,other_user.name as other_user_name ";
    select_sql += "FROM message m INNER JOIN users u ON m.user_id = u.id INNER JOIN users other_user ON m.other_user_id = other_user.id ";
    select_sql += "WHERE ((m.user_id=" + requestData.user_id + " AND m.other_user_id=" + requestData.other_user_id + ") OR (m.user_id=" + requestData.other_user_id + " AND m.other_user_id=" + requestData.user_id + ")) ";
    if (typeof requestData.last_id != 'undefined' && requestData.last_id > 0) {
        if (typeof requestData.type != 'undefined' && requestData.type == "before") {
            select_sql += " AND m.id < " + requestData.last_id;
        } else if (typeof requestData.type != 'undefined' && requestData.type == "after") {
            select_sql += " AND m.id > " + requestData.last_id;
        }
    }

    select_sql += " AND m.conversation_id != -1 AND m.deleted_user_id != -1 AND m.deleted_user_id != " + requestData.user_id + "  ORDER BY m.id DESC LIMIT " + requestData.limit;
    console.log("chat history query==", select_sql);
    var message = {"status": false, "code": 400, "first_message_id": 0, "total_record": 0, "total_unread_message": 0, "type": requestData.type, "responseData": [], "requestData": requestData, "msg": "Something went wrong. Try again"};
    con.query(select_sql, function (err, messages) {
        if (err) {
            callback(err, message);
        } else {
            //console.log(JSON.stringify(messages));
            messages.forEach(function (result) {

                /* result.msg = {
                 'user_id': result.user_id,
                 'other_user_id': result.other_user_id,
                 'msg': result.msg,
                 'conversation_id': result.id,
                 'is_read': result.is_read
                 }; */
                result.user_info = {
                    'userid': result.userid,
                    'name': result.name
                };
                result.other_user_info = {
                    'userid': result.other_userid,
                    'name': result.other_user_name
                };
                if (result.message_type == "TEXT") {
                    result.msg = base64decode(result.msg);
                }
            });

            if (typeof messages !== 'undefined' && messages.length > 0) {
                message = {"status": true, "code": 200, "first_message_id": 0, "total_record": 0, "type": requestData.type, "responseData": JSON.parse(JSON.stringify(messages)), "requestData": requestData, "msg": "Message chat history"};
                con.query("SELECT count(*) as unread_msg_count FROM `message` where conversation_id =" + messages[0].conversation_id + " AND is_read != 1 AND user_id != " + requestData.user_id +" AND deleted_user_id != "+ requestData.user_id+" AND deleted_user_id != -1", function (err, result) {
                    if (err) {
                        callback(err, message);
                    } else {
                        message.total_unread_message = result[0].unread_msg_count;
                        con.query("SELECT id FROM `message` WHERE conversation_id=" + messages[0].conversation_id + "  AND message.deleted_user_id != -1 AND message.deleted_user_id != " + requestData.user_id + " ORDER BY `id` asc limit 1", function (err, result) {
                            if (err) {
                                callback(err, message);
                            } else {
                                message.first_message_id = result[0].id;
                                con.query("SELECT count(*) as total_record FROM `message` where conversation_id =" + messages[0].conversation_id, function (err, result) {
                                    if (err) {
                                        callback(err, message);
                                    } else {
                                        message.total_record = result[0].total_record;
                                        callback(null, message);
                                    }
                                });
                            }
                        });
                    }
                });
            } else {

                con.query("SELECT m.* FROM message m WHERE ((m.user_id=" + requestData.user_id + " AND m.other_user_id=" + requestData.other_user_id + ") OR (m.user_id=" + requestData.other_user_id + " AND m.other_user_id=" + requestData.user_id + ")) AND m.deleted_user_id != -1 AND m.deleted_user_id != " + requestData.user_id + " LIMIT 1", function (err, result) {
                    if (err) {
                        callback(err, null);
                    } else {
                        if (result[0] && result[0].id) {
                            first_message_id = result[0].id;
                        } else {
                            first_message_id = 0;
                        }
                        message = {"status": true, "code": 400, "first_message_id": first_message_id, "total_record": 0, "total_unread_message": 0, "type": requestData.type, "responseData": messages, "requestData": requestData, "msg": "No thread found."};
                        callback(null, message);

                    }
                });
            }
        }
    });
}
function getSingleMsg(conversation_id, callback) {
    //var select_sql = "SELECT * FROM message WHERE conversation_id=" + conversation_id + " order by id desc limit 1";
    var select_sql = "SELECT m.*,u.name,u.id as userid,u.status as user_status,other_user.id as other_userid,other_user.name as other_user_name ";
    select_sql += "FROM message m INNER JOIN users u ON m.user_id = u.id INNER JOIN users other_user ON m.other_user_id = other_user.id ";
    select_sql += "WHERE m.conversation_id =" + conversation_id;
    select_sql += " ORDER BY m.id DESC limit 1";
    var message = {"status": true, "code": 400, "responseData": [], "msg": "Something went wrong. Try again"};
    con.query(select_sql, function (err, result) {
        if (result[0].message_type == "TEXT") {
            result[0].msg = base64decode(result[0].msg);
        }
        if (err) {
            callback(err, message);
        } else {
            if (typeof result !== 'undefined' && result.length > 0) {

                message = {"status": true, "code": 200, "responseData": JSON.parse(JSON.stringify(result[0])), "msg": "Message sent successfully"};
            } else {
                message.msg = "No record found."
            }
        }
        callback(null, message);
    });
}
function getLastSentGroupMsg(msg_id) {
    return new Promise(function (resolve, reject) {
        var select_sql = "SELECT m.*,u.name,u.status as user_status,group_info.id as group_id,group_info.group_name,group_info.image as group_pic FROM message m ";
        select_sql += " INNER JOIN users u ON m.user_id = u.id ";
        select_sql += " INNER JOIN `group` group_info ON m.other_user_id = group_info.id ";
        select_sql += " WHERE m.id =" + msg_id + " ORDER BY m.id DESC limit 1 ";
        console.log("select_sql==", select_sql);
        var message = {"status": true, "code": 400, "responseData": [], "msg": "Something went wrong. Try again"};
        con.query(select_sql, function (err, result) {
            if (result.length > 0 && result[0].message_type == "TEXT") {
                result[0].msg = base64decode(result[0].msg);
            }
            if (err) {
                reject(err);
            } else {
                if (typeof result !== 'undefined' && result.length > 0) {
                    message = {"status": true, "code": 200, "responseData": JSON.parse(JSON.stringify(result[0])), "msg": "Message sent successfully"};
                } else {
                    message.msg = "No record found."
                }
            }
            resolve(result);
        });
    });
}

function getUserStatus(user_id) {
    return new Promise(function (resolve, reject) {
        var user_sql = "SELECT status FROM `users` WHERE id=" + user_id + " LIMIT 1";
        console.log("user_sql==", user_sql);
        con.query(user_sql, function (err, users) {
            if (err)
                reject(err);
            if (users.length > 0) {
                resolve(users[0].status);
            }
        });
    });
}

function getFavourites(user_id, callback) {
    var returnResponse = [];
    var select_sql = "select `merchant_id` from `orders` where `user_id` = " + user_id + " group by `merchant_id`";
    con.query(select_sql, function (err, result) {
        if (err) {
            callback(err, null);
        } else {
            var merchantIdArr = new Array();
            result.forEach(function (result) {
                merchantIdArr.push(result.merchant_id);
            });
            console.log(merchantIdArr.length);
            if (typeof merchantIdArr !== 'undefined' && merchantIdArr.length > 0) {
                var fav_select_sql = "select users.id as restaurant_id, users.business_name, users.cuisines as cuisine, profile_picture, country_code, countries.id as country_id from favourite right join users on users.id = favourite.type_id inner join countries on users.country_code = countries.sortname where type = 'RESTAURANT' and user_id = " + user_id + " or users.id in (" + merchantIdArr + ") group by restaurant_id";
                con.query(fav_select_sql, function (err, favresult) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, favresult);
                    }
                });
            } else {
                callback(null, returnResponse);
            }
        }

    });
}

function searchConversations(data) {
    console.log("data=========", data);
    return new Promise(function (resolve, reject) {


        var select_sql = "SELECT m.*,u.name,u.id as userid,other_user.id as other_userid,other_user.name as other_user_name FROM message m ";
        select_sql += "INNER JOIN users u ON m.user_id = u.id INNER JOIN users other_user ON m.other_user_id = other_user.id INNER JOIN conversations c ON m.conversation_id = c.id ";
        select_sql += "WHERE m.id IN ( SELECT MAX(id) FROM message ";
        select_sql += "WHERE user_id = " + data.user_id + " OR other_user_id =" + data.user_id;
        select_sql += " GROUP BY conversation_id) AND m.deleted_user_id != -1 AND m.deleted_user_id != " + data.user_id;

        select_sql += " and conversation_id in (SELECT DISTINCT m.conversation_id FROM message m ";
        select_sql += " INNER JOIN users u ON m.user_id = u.id ";
        select_sql += " INNER JOIN users other_user ON m.other_user_id = other_user.id ";
        select_sql += " WHERE m.deleted_user_id != - 1 AND m.deleted_user_id != " + data.user_id;
        select_sql += " and (CONVERT(FROM_BASE64(m.msg)USING latin1) LIKE '%" + data.search_keyword + "%' or (CASE WHEN m.user_id = " + data.user_id + " THEN other_user.name WHEN m.other_user_id = " + data.user_id + " THEN u.name END) LIKE '" + data.search_keyword + "%') ";
        select_sql += " and (m.user_id = " + data.user_id + " or m.other_user_id = " + data.user_id + ") ";
        select_sql += " and m.message_type = 'TEXT') ORDER BY m.id DESC ";
        /*var search_qry = 'SELECT c.id as conversation_id,m.*,u.name ,other_user.name as other_user_name FROM `conversations` c ';
         search_qry += ' left JOIN message m on c.id = m.conversation_id ';
         search_qry += ' left JOIN users u on u.id = m.user_id ';
         search_qry += ' left JOIN users other_user on other_user.id = m.other_user_id ';
         search_qry += ' where (FROM_BASE64(m.msg) LIKE "%' + data.search_keyword + '%" or u.name LIKE "%' + data.search_keyword + '%") and m.deleted_user_id != -1  and m.deleted_user_id !=' + data.user_id + ' and (c.user_id=' + data.user_id + ' or c.other_user_id=' + data.user_id + ') ';
         search_qry += ' AND m.id = (SELECT id FROM message WHERE c.id = m.conversation_id ORDER BY id DESC LIMIT 1) ';
         search_qry += ' and m.message_type="TEXT" group BY c.id order by m.id desc'; */
        console.log("searchConversations===", select_sql);
        con.query(select_sql, function (err, search_results) {
            console.log("seach Conversations result = " + search_results);
            if (err)
                reject(err);
            var total_unread_message = 0;
            if (search_results != undefined && search_results.length > 0) {
                search_results.forEach(function (result, index) {

                    var unread_message = 0;
                    con.query("SELECT count(*) as unread_msg_count FROM `message` where conversation_id=" + result.conversation_id + " AND user_id != " + data.user_id + " AND is_read !=1", function (err, unread_result) {
                        if (err) {
                            resolve(err);
                        }
                        result.unread_message = unread_result[0].unread_msg_count;

                        total_unread_message = total_unread_message + unread_result[0].unread_msg_count;
                        if (index === search_results.length - 1) {
                            search_results.total_unread_message = total_unread_message;
                            resolve(null, search_results);
                        }
                    });
                    result.unread_message = unread_message;
                    if (result.message_type == "TEXT") {
                        result.msg = base64decode(result.msg)
                    }
                });
                resolve(search_results);
            } else {
                resolve(search_results);
            }
        });
    });
}
function searchFriends(data) {
    return new Promise(function (resolve, reject) {
        var search_qry = 'select DISTINCT `u`.`id`, `name`, `profile_picture`, `email`, `phone_number`, IFNULL(username,"") as username from `friends` `f` ';
        search_qry += ' left join `users` `u` on u.id = (CASE WHEN f.user_id = ' + data.user_id + ' THEN f.friend_id WHEN f.friend_id = ' + data.user_id + ' THEN f.user_id END) ';
        search_qry += ' where `f`.`status` = 1 ';
        search_qry += ' and `u`.`id` not in (select IFNULL(GROUP_CONCAT( DISTINCT CASE WHEN user_id = ' + data.user_id + ' THEN other_id WHEN other_id = ' + data.user_id + ' THEN user_id END ),"") as user_id from `user_block` where `user_id` = ' + data.user_id + ' or `other_id` = ' + data.user_id + ') ';
        search_qry += '  and (u.name LIKE "' + data.search_keyword + '%" or u.username LIKE "' + data.search_keyword + '%") order by `u`.`name` asc';
        console.log("search_qry===", search_qry);
        con.query(search_qry, function (err, search_results) {
            console.log("seach Friends result = " + search_results);
            if (err)
                reject(err);
            resolve(search_results);
        });
    });
}
function searchPeoples(data) {
    return new Promise(function (resolve, reject) {
        var search_qry = 'select u.`id`, `name`, `profile_picture`, `email`, `phone_code`, `country_code`, `phone_number`, ';
        search_qry += ' IFNULL(username, "") as username, IF(f.friend_id= ' + data.user_id + ', 1, 0) as sent_by, IFNULL((6371 * acos (cos ( radians(' + data.lat + ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' + data.lng + ') ) + sin ( radians(' + data.lat + ') ) * sin( radians( lat ) ))) ,0) AS distance from `users` u ';
        search_qry += ' left join `friends` `f` on u.id = (CASE WHEN f.user_id = ' + data.user_id + ' THEN f.friend_id WHEN f.friend_id = ' + data.user_id + ' THEN f.user_id END) ';
        search_qry += ' where `u`.`status` = "1" and `u`.`profile_seen` = 1 and u.`id` != ' + data.user_id + ' and `role_id` = 4 ';
        search_qry += ' and `u`.`id` not in (select IFNULL(GROUP_CONCAT( DISTINCT CASE WHEN user_id = ' + data.user_id + ' THEN other_id WHEN other_id = ' + data.user_id + ' THEN user_id END ),"") as user_id from `user_block` where `user_id` = ' + data.user_id + ' or `other_id` = ' + data.user_id + ') ';
        search_qry += ' and (u.name LIKE "' + data.search_keyword + '%" or u.username LIKE "' + data.search_keyword + '%") ';
        search_qry += ' and (f.status is NULL or f.status=0)'
        search_qry += ' order by f.status desc, sent_by desc, `distance` asc';
        console.log("search_qry===", search_qry);
        con.query(search_qry, function (err, search_results) {
            console.log("seach Peoples result = " + search_results);
            if (err)
                reject(err);
            if (search_results.length > 0) {
                search_results.forEach(function (result, index) {
                    var is_friend_qry = "SELECT * FROM friends WHERE (user_id=" + data.user_id + " AND friend_id=" + result.id + ") OR (user_id=" + result.id + " AND friend_id=" + data.user_id + ")";
                    con.query(is_friend_qry, function (err, friend_result) {
                        if (err) {
                            reject(err, null);
                        } else {
                            if (friend_result.length > 0) {
                                if (friend_result[0] && friend_result[0].status == 1) {
                                    result.is_friend = 1;
                                } else if (friend_result[0] && friend_result[0].status == 0 && friend_result[0].user_id == data.user_id) {
                                    result.is_friend = 2;
                                } else if (friend_result[0] && friend_result[0].status == 0 && friend_result[0].friend_id == data.user_id) {
                                    result.is_friend = 3;
                                }
                            } else {
                                result.is_friend = 0;
                            }
                            //callback(null, message);

                        }
                        if (index === search_results.length - 1) {
                            resolve(search_results);
                        }
                    });
                });
            } else {
                resolve(search_results);
            }
        });
    });
}
