@extends('layouts.admin')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">User Story</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/user-story')}}">User Story</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">View</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">

                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><h5 class="card-title">{{ $story->user->fullname}} Story</h5></label>
                                    <div class="col-md-10">
                                         <a href="{{url('admin/user-story')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Story" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Story
                                        </a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                   <table class="table table-striped table-bordered">
                               <tbody>

                                    <tr>
                                        <th style="width:30%">UserName</th>
                                        <td style="width:70%">{{$story->user->fullname}}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:30%">Category</th>
                                        <td style="width:70%">{{$story->category->name}}</td>
                                    </tr>
                                    <!-- <tr>
                                        <th style="width:30%">Genre</th>
                                        <td style="width:70%">{{ $story->genre }}</td>
                                    </tr> -->
                                    <tr>
                                        <th style="width:30%">Content</th>
                                        <td style="width:70%">{{ $story->content }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td>{{($story->status == 1)?'Active':'InActive'}}</td>
                                    </tr>
                                </tbody>
                            </table>
                                </div>

                            </div>
                        </div>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

@endsection
