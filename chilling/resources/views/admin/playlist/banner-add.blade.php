@extends('layouts.admin')
@section('title','Banner Add')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Banner Add</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/playlist')}}">Playlist</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Add</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">

                   <div class="col-md-12">
                    @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
            @if(session('error_message'))
                <p class="alert alert-danger">{{session('error_message')}}</p>
            @endif

                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url('admin/banner-add/'.$playlist->id)}}" method="post" id="exampleValidation">
                    @csrf
                   <div class="card">


                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url('admin/playlist')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Playlist" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Playlist
                                        </a>
                                    </div>
                            </div>

                               <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Palylist Title</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control"  value="{{$playlist->name}}" readonly="readonly">

                                    </div>
                                </div>

                               <input type="hidden" name="id" value="{{$playlist->banner->id ?? 0}}">
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Banner Image</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control" id="image" name="image" placeholder="Choose image"  multiple="">
                                        <small class="text-danger">Please upload 295 * 500 pixel image</small>


                                    </div>
                                    <div class="col-lg-4">

                                    <img style="@if($playlist->banner)display:block;@else display: none; @endif"  id="image_preview"  src="@if($playlist->banner) {{$banner->image}} @endif" width="70" class="pull-right" alt="User Image">
                                    </div>
                                </div>
                                @if($playlist->banner)
                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Status</label>
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="status1" name="status" value="1" class="custom-control-input" {{$playlist->banner->status==1 ? "checked":""}}>
                                                <label class="custom-control-label" for="status1">Active</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="status2" name="status" class="custom-control-input" value="0" {{$playlist->banner->status==0 ? "checked":""}}>
                                                <label class="custom-control-label" for="status2">In-Active</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @else
                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Status</label>
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="status1" name="status" value="1" class="custom-control-input" checked="">
                                                <label class="custom-control-label" for="status1">Active</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="status2" name="status" class="custom-control-input" value="0">
                                                <label class="custom-control-label" for="status2">In-Active</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif

                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url('admin/playlist')}}" class="btn btn-danger resetBtn">Cancel</a>

                                </div>
                            </div>
                        </div>
                    </form>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

          <script>

        $(document).ready(function(){

            /* validate */

        $(".close").click(function(){
            var id = $(this).attr('id');
            var img_url = $("#img_url").val();

            console.log("id >> "+id);
            if(id)
            {
                bootbox.confirm({
                message: "Are you sure you want to delete this audio ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {


                if(result)
                {
                    $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                    });
                    $.ajax({
                        url:"{{url('admin/songs-delete')}}",
                        type:"post",
                        beforeSend:function(){
                            $("#loadingmessage").show();
                        },
                        data:{id:id},
                        success:function(data){
                            $("#loadingmessage").hide();
                            if(data.status==1)
                            {
                                $("#div_"+id).hide();
                                var msg= "Audio Deleted Successfully";
                                var title= "Success";
                                var type= "success";
                                notification_msg(msg,title,type);
                            }
                            else
                            {
                                var msg= "Audio Not Deleted";
                                var title= "Error";
                                var type= "error";
                                notification_msg(msg,title,type);
                            }


                        }
                    }); // Ajax close
                }
                else
                {
                    var msg= "Audio Not Deleted";
                    var title= "Error";
                    var type= "error";
                    notification_msg(msg,title,type);
                }

                }
            });
            }

        })

        $("#exampleValidation").validate({
            validClass: "success",
            rules: {
                name: {
                    required: false,
                    extension: "mp3|mpeg|3gp|aa|aac|aax|act|aiff|alac|amr|ape|au|awb|dct|dss|dvf|flac|gsm|iklax|ivs|m4a|m4b|m4p|mmf|mpc|msv|nmf|opus|raw|ra|rm|rf64|sln|tta|voc|vox|wav|wma|wv|webm|8svx|cda|ogg"

                },

            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
        });


     function readURL(input)
    {
        if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
        $('#image_preview').attr('src', e.target.result);

        $('#image_preview').hide();
        $('#image_preview').fadeIn(650);
        }
       reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function() {
    readURL(this);
    });



        });






    </script>
@endsection
