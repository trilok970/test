@extends('layouts.admin')
@section('title','Playlist Edit')
@section('content')
@push('stylesheets')
<link rel="stylesheet" href="{{url('dist/css/selectize.css')}}" />
<link rel="stylesheet" href="{{url('dist/css/selectize.min.css')}}" />

@endpush
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Playlist</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/playlist')}}">Playlist</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Add</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">

                   <div class="col-md-12">
            @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
            @if(session('error_message'))
                <p class="alert alert-danger">{{session('error_message')}}</p>
            @endif

                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url('admin/playlist/'.$playlist->id)}}" method="post" id="exampleValidation">
                    @csrf
                    @method('PUT')
                   <div class="card">


                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url('admin/playlist')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Playlist" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Playlist
                                        </a>
                                    </div>
                            </div>

                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Channel</label>
                                    <div class="col-md-10">
                                        <select type="text" class="form-control" id="channel_id" name="channel_id" placeholder="" value="{{old('channel_id')}}">
                                        <option value="">Select Channel</option>
                                        @foreach($channels as $channel)
                                        <option value="{{$channel->id}}" {{$playlist->channel_id==$channel->id ? "selected":""}}>{{$channel->name}}</option>
                                        @endforeach
                                        </select>
                                        <span class="text-danger">{{$errors->first('channel_id')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Category</label>
                                    <div class="col-md-10">
                                        <select type="text" class="selectize-multiple" id="category_id" name="category_id[]"  multiple data-live-search="true">
                                        <!-- <option value="">Select Category</option> -->


                                        @foreach($categories as $cat)
                                        <option value="{{$cat->id}}"
                                        @if(in_array($cat->id,$playlist->PlaylistCategory->pluck('category_id')->toArray())) selected @endif >{{$cat->name}}</option>
                                        @endforeach
                                        </select>
                                        <span class="text-danger">{{str_replace("id","",$errors->first('category_id'))}}</span>
                                    </div>
                                </div>
                               <!--  <div class="form-group row">
                                    <label class="col-md-2 m-t-15">SubCategory</label>
                                    <div class="col-md-10">
                                        <select type="text" class="form-control" id="subcategory_id" name="subcategory_id" placeholder="" value="{{old('subcategory_id')}}">
                                        <option value="">Select SubCategory</option>
                                       @foreach($subcategories as $sub)
                                        <option value="{{$sub->id}}" {{$playlist->subcategory_id==$sub->id ? "selected":""}}>{{$sub->name}}</option>
                                        @endforeach
                                        </select>
                                        <span class="text-danger">{{$errors->first('subcategory_id')}}</span>
                                    </div>
                                </div>-->
                               <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Title</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Title" value="{{$playlist->name}}">
                                        <span class="text-danger">{{$errors->first('name')}}</span>
                                    </div>
                                </div>
                              <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Description</label>
                                    <div class="col-md-10">
                                       <textarea class="form-control" id="description" name="description" placeholder="Enter Description" value="{{$playlist->description}}" >{{$playlist->description}}</textarea>
                                        <span class="text-danger">{{$errors->first('description')}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Image</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control" id="image" name="image" placeholder="Choose image" value="{{old('image')}}">
                                        <small class="text-danger">Please upload 384 * 960 pixel image</small>


                                    </div>
                                    <div class="col-lg-4">
                                    <img  id="image_preview"  src="{{$playlist->image}}" width="70" class="pull-right" alt="User Image">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Thumbnail</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control image" id="thumbnail" name="thumbnail" inc_val="1" placeholder="Choose image" value="">
                                        <small class="text-danger">Please upload 128 * 320 pixel image</small>
                                        <span class="text-danger">{{$errors->first('thumbnail')}}</span>
                                    </div>
                                    <div class="col-lg-4">
                                    <img  id="thumbnail_preview"  src="{{$playlist->thumbnail}}" width="70" class="pull-right" alt="User Image">
                                    </div>
                                </div>


                                 <!-- <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><b><i class="fa fa-plus-circle"></i> Add Audio Files</b></label>

                               </div> -->
                              <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Audio Files</label>
                                    <div class="col-md-10">
                                        <!-- <select name="audio_id[]" id="audio_id" class="selectize-multiple" multiple>
                                         @foreach($playlist->PlaylistAudio as $audio)
                                         <option value="{{$audio->id}}" @if(in_array($audio->id,$playlist->PlaylistAudio->pluck('audio_id')->toArray())) selected @endif >{{$audio->title}}</option>
                                         @endforeach
                                        </select> -->
                                        <div class="input-group w-100 ">
                                            <div class="input-group-prepend w-75">
                                            <input type="hidden" name="playlist_id" id="playlist_id" value="{{$playlist->id}}" />
                                            <input class="form-control" name="search" id="search" placeholder="Enter Title, Tags, Author, Narrator, Created On ..." value="{{old('search')}}" />

                                            </div>
                                            <button id="search_button" type="button" class="btn btn-success">Search</button>

                                        </div>

                                        <div id="search_div">
                                        <small class="text-danger">Type anything for audio file search</small>

                                        <div class="table-responsive" >
                                            <table id="example1" class="table table-striped table-bordered" style="width:82%;"  >
                                                <thead>
                                                    <tr>
                                                        <th>Sr</th>
                                                        <th>Title</th>
                                                        <th>Tags</th>
                                                        <th>Author</th>
                                                        <th>Narrator</th>
                                                        <th>Image</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($playlist->PlaylistAudio as $row1)

                                                    <tr>
                                                    <td><input type="checkbox" checked name="audio_id[]" id="audio_id" value="{{$row1->audio->id ?? 0}}" /></td>
                                                    <td>{!! $row1->audio->title ?? "" !!}</td>
                                                    <td>{{  $row1->audio->tags ?? ""}}</td>
                                                    <td>{!! $row1->audio->author ?? "" !!}</td>
                                                    <td>{!! $row1->audio->narrator ?? "" !!}</td>
                                                    <td><a target="_blank" href="{{$row1->audio->image}}"><img src="{{$row1->audio->image}}" width="50" /></a></td>

                                                </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                            <div id="tr_div"></div>
                                        </div>

                                        </div>


                                        <span class="text-danger">{{str_replace("id","",$errors->first('audio_id'))}}</span>

                                    </div>
                                </div>
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url('admin/playlist')}}" class="btn btn-danger resetBtn">Cancel</a>

                                </div>
                            </div>
                        </div>
                    </form>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
    @push('scripts')

        <script src="{{url('dist/js/selectize.min.js')}}"></script>
        <script src="{{url('dist/js/form-selectize.min.js')}}"></script>
        <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/ckeditor/ckeditor.js"></script>
        <script>


        $(document).ready(function(){
            CKEDITOR.editorConfig = function (config) {
                config.language = 'es';
                config.uiColor = '#F7B42C';
                config.height = 300;
                config.toolbarCanCollapse = true;

            };
            CKEDITOR.replace('description');
            $("#search_button").click(function(){
                var search = $("#search").val();
                var playlist_id = $("#playlist_id").val();
                $('#tbody tr').each(function(){
                post_order_ids.push($(this).data("post-id"));
                });
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                url:"{{url('admin/audio-search')}}",
                method:"Get",
                data:{search:search,playlist_id:playlist_id},
                success:function(data)
                {
                    if(data)
                    {
                        $("#tr_div").html(data);
                    }else{
                        $(".alert-success").hide();
                        $(".alert-danger").show();
                    }
                }
                });
            });
            $('#select-beast').selectize({
                create: true,
                sortField: 'text'
            });

            function readURL(input)
            {
                if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                $('#image_preview').attr('src', e.target.result);

                $('#image_preview').hide();
                $('#image_preview').fadeIn(650);
                }
            reader.readAsDataURL(input.files[0]);
                }
            }

            $("#image").change(function() {
            readURL(this);
            });

            function readURL1(input)
            {
                if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                $('#thumbnail_preview').attr('src', e.target.result);

                $('#thumbnail_preview').hide();
                $('#thumbnail_preview').fadeIn(650);
                }
            reader.readAsDataURL(input.files[0]);
                }
            }

            $("#thumbnail").change(function() {
            readURL1(this);
            });



        });

    </script>
    @endpush

@endsection
