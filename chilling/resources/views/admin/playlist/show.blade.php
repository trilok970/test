@extends('layouts.admin')
@section('title','Playlist Show')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Playlist</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/playlist')}}">Playlist</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">View</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">

                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><h5 class="card-title">All Playlists</h5></label>
                                    <div class="col-md-10">
                                         <a href="{{url('admin/playlist')}}" class="btn btn-info pull-right btn-sm"  title="All Playlists" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Playlists
                                        </a>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table id="example1" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Channel Name</th>
                                                <th>Category Name</th>
                                                <th>Image</th>
                                                <th>Thumbnail</th>
                                                <th>Created On</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=1; @endphp

                                            <tr >
                                                <td>{{$playlist->name}}</td>
                                                <td>{!! $playlist->description !!}</td>
                                                <td>{{$playlist->channel->name}}</td>
                                                <td>
                                                    @foreach($playlist->PlaylistCategory as $value)
                                                      <span class="badge badge-{{$colors[$loop->index]}}">{{$value->category->name}}</span>

                                                    @endforeach
                                                </td>
                                                <td><a target="_blank" href="{{$playlist->image}}"><img src="{{$playlist->image}}" width="50" /></a></td>
                                                <td><a target="_blank" href="{{$playlist->thumbnail}}"><img src="{{$playlist->thumbnail}}" width="50" /></a></td>
                                                <td>{{date('d-M-Y',strtotime($playlist->created_at))}}</td>


                                            </tr>



                                    </table>
                                     <div class="form-group row">
                                    <label class="col-md-12 m-t-15"><h5 class="card-title">Audio Files</h5></label>

                                   </div>
                                     <table id="example1" class="table table-striped table-bordered" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Category</th>
                                                <th>Tags</th>
                                                <th>Author</th>
                                                <th>Narrator</th>
                                                <th>Description</th>
                                                <th>Created</th>
                                                <th>Image</th>
                                                <th>File</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody">
                                            @php $i=1; @endphp
                                            @foreach($audios as $row)
                                            <tr data-post-id={{$row->id}}>
                                                <td><b><span class="filter_text">{!! $row->audio->title !!}<a href="{{url('admin/audios?title='.strip_tags($row->audio->title))}}"> <i class="fas fa-info-circle"></i></a></span></b></td>
                                                <td><b><span class="filter_text">
                                                @foreach($row->audio->AudioCategory as $value)
                                                      <span class="badge badge-{{$colors[$loop->index]}}">{{$value->category->name}}</span>
                                                @endforeach
                                                </span></b></td>
                                                <td><b><span class="filter_text">{{$row->audio->tags}}</span></b></td>
                                                <td><b><span class="filter_text">{!!$row->audio->author!!}<a href="{{url('admin/audios?author='.strip_tags($row->audio->author))}}"> <i class="fas fa-info-circle"></i></a></span></b></td>
                                                <td><b><span class="filter_text">{!!$row->audio->narrator!!}<a href="{{url('admin/audios?narrator='.strip_tags($row->audio->narrator))}}"> <i class="fas fa-info-circle"></i></a></span></b></td></td>
                                                <td><b><span class="filter_text">{!!$row->audio->description!!}<a href="{{url('admin/audios?description='.strip_tags($row->audio->description))}}"> <i class="fas fa-info-circle"></i></a></span></b></td></td>
                                                <td><b><span class="filter_text("> {{ date("Y-m-d",strtotime($row->audio->created_at))}}</span></b></td></td>
                                                <td><span class="filter_text"><a target="_blank" href="{{$row->audio->image}}"><img src="{{$row->audio->image}}" width="50" /></a></span></td>
                                                <td><span class="filter_text"><audio controls>
                                                <source src="{{ $row->audio->file }}" type="audio/{{$row->audio->extension}}">
                                                Your browser does not support the audio element.
                                                </audio></span></td>
                                            </tr>
                                            @endforeach


                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            @push('scripts')
            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

            <script >
                $(function () {


/////////////   REPLACE TR VALUE //////////////////////////
    $( "#tbody" ).sortable({
        revert: true,
        cancel: ".filter_text",
    update  : function(event, ui)
    {
    var post_order_ids = new Array();
    var type = 'playlist_audio';
    $('#tbody tr').each(function(){
    post_order_ids.push($(this).data("post-id"));
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
    url:"{{url('admin/update-order')}}",
    method:"POST",
    data:{post_order_ids:post_order_ids,type:type},
    success:function(data)
    {
        if(data)
        {
            $(".alert-danger").hide();
            $(".alert-success ").show();
        }else{
            $(".alert-success").hide();
            $(".alert-danger").show();
        }
    }
    });
    }
    });


            });
            </script>
         @endpush

@endsection
