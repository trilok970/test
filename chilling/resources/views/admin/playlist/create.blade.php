@extends('layouts.admin')
@section('title','Playlist Create')
@section('content')
@push('stylesheets')
<link rel="stylesheet" href="{{url('dist/css/selectize.css')}}" />
<link rel="stylesheet" href="{{url('dist/css/selectize.min.css')}}" />

@endpush
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Playlist</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/playlist')}}">Playlist</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Add</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">

                   <div class="col-md-12">
            @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
            @if(session('error_message'))
                <p class="alert alert-danger">{{session('error_message')}}</p>
            @endif

                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url('admin/playlist')}}" method="post" id="exampleValidation">
                    @csrf
                   <div class="card">


                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url('admin/playlist')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Playlist" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Playlist
                                        </a>
                                    </div>
                            </div>

                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Channel</label>
                                    <div class="col-md-10">
                                        <select type="text" class="form-control" id="channel_id" name="channel_id" value="{{old('channel_id')}}">
                                        <option value="">Select Channel</option>
                                        @foreach($channels as $channel)
                                            <option value="{{$channel->id}}" @if(old("channel_id") == $channel->id) {{'selected'}} @endif>{{$channel->name}}</option>
                                        @endforeach
                                        </select>
                                        <span class="text-danger">{{$errors->first('channel_id')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Category</label>
                                    <div class="col-md-10">
                                        <select type="text" class="selectize-multiple" id="category_id" name="category_id[]" onchange="show_subcategory()" multiple data-live-search="true">
                                        @foreach($categories as $cat)
                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endforeach
                                        </select>
                                        <span class="text-danger">{{str_replace("id","",$errors->first('category_id'))}}</span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Title</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Title" value="{{old('name')}}">
                                        <span class="text-danger">{{$errors->first('name')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Description</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" id="description" name="description" placeholder="Enter Description" >{{old('description')}}</textarea>
                                        <span class="text-danger">{{$errors->first('description')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Image</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control image" id="image" name="image" inc_val="1" placeholder="Choose image" value="{{old('image')}}">
                                        <small class="text-danger">Please upload 384 * 960 pixel image</small>
                                        <span class="text-danger">{{$errors->first('image')}}</span>
                                    </div>
                                    <div class="col-lg-4">
                                    <img style="display: none;"  id="image_preview"  src="" width="70" class="pull-right" alt="User Image">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Thumbnail</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control image" id="thumbnail" name="thumbnail" inc_val="1" placeholder="Choose image" value="">
                                        <small class="text-danger">Please upload 128 * 320 pixel image</small>
                                        <span class="text-danger">{{$errors->first('thumbnail')}}</span>
                                    </div>
                                    <div class="col-lg-4">
                                    <img style="display: none;"  id="thumbnail_preview"  src="" width="70" class="pull-right" alt="User Image">
                                    </div>
                                </div>

                              <!--  <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><b><i class="fa fa-plus-circle"></i> Add Audio Files</b></label>

                               </div> -->
                               <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Audio Files</label>
                                    <div class="col-md-10">
                                        <!-- <select name="audio_id[]" id="audio_id" class="selectize-multiple" multiple>
                                         @foreach($audios as $audio)
                                            <option value="{{$audio->id}}" >{{$audio->title}}</option>
                                         @endforeach
                                        </select> -->
                                        <div class="input-group w-100 ">
                                            <div class="input-group-prepend w-75">
                                            <input class="form-control" name="search" id="search" placeholder="Enter Title, Tags, Author, Narrator, Created On ..." value="{{old('search')}}" />

                                            </div>
                                            <button id="search_button" type="button" class="btn btn-success">Search</button>

                                        </div>

                                        <div id="search_div"><small class="text-danger">Type anything for audio file search</small></div>


                                        <span class="text-danger">{{str_replace("id","",$errors->first('audio_id'))}}</span>

                                    </div>
                                </div>





                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url('admin/playlist')}}" class="btn btn-danger resetBtn">Cancel</a>

                                </div>
                            </div>
                        </div>
                    </form>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
        @push('scripts')

        <script src="{{url('dist/js/selectize.min.js')}}"></script>
        <script src="{{url('dist/js/form-selectize.min.js')}}"></script>
        <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/ckeditor/ckeditor.js"></script>
        <script>

        $(document).ready(function(){
            CKEDITOR.editorConfig = function (config) {
                config.language = 'es';
                config.uiColor = '#F7B42C';
                config.height = 300;
                config.toolbarCanCollapse = true;

            };
            CKEDITOR.replace('description');

            $("#search_button").click(function(){
                var search = $("#search").val();
                $('#tbody tr').each(function(){
                post_order_ids.push($(this).data("post-id"));
                });
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                url:"{{url('admin/audio-search')}}",
                method:"Get",
                data:{search:search},
                success:function(data)
                {
                    if(data)
                    {
                        $("#search_div").html(data);
                    }else{
                        $(".alert-success").hide();
                        $(".alert-danger").show();
                    }
                }
                });
            });

            /* validate */
           $('#select-beast').selectize({
                create: true,
                sortField: 'text'
            });
            function readURL(input)
            {
                if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                $('#image_preview').attr('src', e.target.result);

                $('#image_preview').hide();
                $('#image_preview').fadeIn(650);
                }
            reader.readAsDataURL(input.files[0]);
                }
            }

            $("#image").change(function() {
            readURL(this);
            });

            function readURL1(input)
            {
                if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                $('#thumbnail_preview').attr('src', e.target.result);

                $('#thumbnail_preview').hide();
                $('#thumbnail_preview').fadeIn(650);
                }
            reader.readAsDataURL(input.files[0]);
                }
            }

            $("#thumbnail").change(function() {
            readURL1(this);
            });

        });



    </script>
    @endpush
@endsection
