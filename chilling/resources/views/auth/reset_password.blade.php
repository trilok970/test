<!DOCTYPE html>
<html dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('assets/images/favicon.png')}}">

    <title>{{ env('APP_NAME') }} Forgot Password</title>
    <!-- Custom CSS -->
    <link href="{{url('dist/css/style.min.css')}}" rel="stylesheet">
    <link href="{{url('dist/css/custom.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div class="main-wrapper">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-dark">
            <div class="auth-box bg-dark border-secondary" style="margin: 0%;">
                <div id="loginform">
                    <div class="text-center p-t-20 p-b-20">
                        <span class="db">
                            <img style="width:40%;" src="{{url('assets/images/chilling.png')}}" alt="logo" />
                            <h1>Forgot Password</h1>
                        </span>
                    </div>
                    <!-- Form -->
                     @if(session('error_message'))
                <p class="alert alert-danger">{{session('error_message')}}</p>
            @endif

            <form method="post" class="form-horizontal m-t-20" id="loginform" action="{{url('admin/reset-password')}}">
                @csrf
                <input type="hidden" name="email" placeholder="Enter email" value="{{request()->get('email')}}" class="@error('email') is-invalid @enderror">

                <div class="row p-b-30">
                    <div class="col-12">
                        <span class="text-danger">{{$errors->first('email')}}</span>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-lock"></i></span>
                            </div>
                            <input type="password" placeholder="Enter new password" class="@error('password') is-invalid @enderror form-control form-control-lg"  name="password" id="password" aria-label="Password" aria-describedby="basic-addon1" required="">

                        </div>
                        <span class="text-danger">{{$errors->first('password')}}</span>
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-lock"></i></span>
                            </div>
                            <input type="password" class="form-control form-control-lg" placeholder="Password Confirmation" name="password_confirmation" id="password_confirmation" aria-label="Confirm Password" aria-describedby="basic-addon1" required="">

                        </div>
                        <span class="text-danger">{{$errors->first('password_confirmation')}}</span>
                        <input hidden name="token" placeholder="token" value="{{request()->get('token')}}">
                        <span class="text-danger">{{$errors->first('token')}}</span>
                    </div>
                </div>
                <div class="row border-secondary">
                    <div class="col-12">
                        <div class="form-group" align="center">
                            <div class="">
                                <!-- <button class="btn btn-info" id="to-recover" type="button"><i class="fa fa-lock m-r-5"></i> Lost password?</button> -->
                                <button class="btn btn-success btn-lg" type="submit">Submit</button>
                            </div>
                            {{-- <p class="slogan mt-3">Embrace Your Fear</p> --}}
                        </div>
                    </div>
                </div>
            </form>
                </div>

            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->
    <script src="{{url('assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{url('assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{url('assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- ============================================================== -->
    <!-- This page plugin js -->
    <!-- ============================================================== -->
    <script>
$(function() {

    $(".alert-success").fadeOut(5000);
    $(".alert-danger").fadeOut(5000);
});

    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
    // ==============================================================
    // Login and Recover Password
    // ==============================================================
    $('#to-recover').on("click", function() {
        $("#loginform").slideUp();
        $("#recoverform").fadeIn();
    });
    $('#to-login').click(function(){

        $("#recoverform").hide();
        $("#loginform").fadeIn();
    });
    </script>

</body>

</html>
