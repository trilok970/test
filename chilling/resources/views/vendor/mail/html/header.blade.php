<tr>
<td class="header">
<a href="{{url('assets/images/chilling.jpg')}}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="{{url('assets/images/chilling.jpg')}}" class="logo" alt="{{config('app.name')}} Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
