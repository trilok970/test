<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>{{ $page_title }}</title>

  <!-- Font Awesome -->
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

<!-- Theme style -->
<link href="{{url('dist/css/style.min.css')}}" rel="stylesheet">



</head>

<body>



  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3 align="center" class="text-center"> {{ $page_title }}</h3>

{!! $cms->content ?? "" !!}


      </div>
    </div>
  </div>



</body>

</html>
