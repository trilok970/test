<?php
require_once("SimpleHtmlDom.php");
$date = date("m/d/Y");
$game_type = "win4";

if(isset($_REQUEST['date'])) {
	$date = date("m/d/Y",strtotime($_REQUEST['date']));
}
if(isset($_REQUEST['game_type'])) {
	$game_type = strtoupper($_REQUEST['game_type']);
}
echo saveinfo($date,$game_type);
function saveinfo($date,$game_type)
{
	$gameNameIn = $game_type;
	$d = date("d",strtotime($date));
	$m = date("m",strtotime($date));
	$y = date("Y",strtotime($date));

//https://nylottery.ny.gov/draw-game?game=win4
	$html = file_get_html('https://nylottery.ny.gov/draw-game?game=win4');

// echo "<pre>";
// echo($html);
$ch = curl_init();

// Set the URL
curl_setopt($ch, CURLOPT_URL, 'https://nylottery.ny.gov/draw-game?game=win4');

// Set the HTTP method
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

// Return the response instead of printing it out
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

// Send the request and store the result in $response
$response = curl_exec($ch);

echo 'HTTP Status Code: ' . curl_getinfo($ch, CURLINFO_HTTP_CODE) . PHP_EOL;
echo 'Response Body: ' . $response . PHP_EOL;

// Close cURL resource to free up system resources
curl_close($ch);
exit;
    // Find all article blocks
foreach($html->find('div.styles__GameDetail-sc-6ev79m-4') as $article) {
    $item['title']     = $article->find('div.title', 0)->plaintext;
    $item['intro']    = $article->find('div.intro', 0)->plaintext;
    $item['details'] = $article->find('div.details', 0)->plaintext;
    $articles[] = $item;
}

print_r($articles);
	$title = $html->find('table', 0);

	if($title) {
		$mid_no = $html->find('table', 0)->find('tr',0)->find('td',0);
		$mid_winner = $html->find('table', 0)->find('tr',2)->find('td',0);
		$mid_total_payout = $html->find('table', 0)->find('tr',2)->find('td',1);



		$mid_no = trim(preg_replace('/<[^>]*>/','',$mid_no));
		$mid_no = preg_replace('/[^0-9]/','',$mid_no);
		
		
		
		$mid_fireball = substr($mid_no,"-1");

		$mid_winner = trim(preg_replace('/<[^>]*>/','',$mid_winner));
		$mid_total_payout = trim(preg_replace('/<[^>]*>/','',$mid_total_payout));

		


		if($game_type == 'PICK3') {
			$eve_no = $html->find('table', 0)->find('tr',0)->find('td',1);
			$eve_no = trim(preg_replace('/<[^>]*>/','',$eve_no));
			$eve_no = preg_replace('/[^0-9]/','',$eve_no);
			$eve_winning_no = substr($eve_no,"0","3");
			$eve_fireball = substr($eve_no,"-1");
			$mid_winning_no = substr($mid_no,"0","3");
			$eve_winning_no = substr($eve_no,"0","3");
			$eve_winner = $html->find('table', 0)->find('tr',2)->find('td',2);
			$eve_total_payout = $html->find('table', 0)->find('tr',2)->find('td',3);
			
			$eve_winner = trim(preg_replace('/<[^>]*>/','',$eve_winner));
			$eve_total_payout = trim(preg_replace('/<[^>]*>/','',$eve_total_payout));
		}
		else {
			$eve_no = $html->find('table', 0)->find('tr',3)->find('td',0);
			$eve_no = trim(preg_replace('/<[^>]*>/','',$eve_no));
			$eve_no = preg_replace('/[^0-9]/','',$eve_no);
			$eve_winning_no = substr($eve_no,"0","3");
			$eve_fireball = substr($eve_no,"-1");

			$mid_winning_no = substr($mid_no,"0","4");
			$eve_winning_no = substr($eve_no,"0","4");

			$eve_winner = $html->find('table', 0)->find('tr',5)->find('td',0);
			$eve_total_payout = $html->find('table', 0)->find('tr',5)->find('td',1);
			
			$eve_winner = trim(preg_replace('/<[^>]*>/','',$eve_winner));
			$eve_total_payout = trim(preg_replace('/<[^>]*>/','',$eve_total_payout));
		}

	$data = array(
		"date"=>$date,
		"game_type"=>$game_type,
		"midday_winning_no"=>$mid_winning_no,
		"midday_fireball"=>$mid_fireball,
		"midday_winner"=>$mid_winner,
		"midday_total_payout"=>$mid_total_payout,
		"evening_winning_no"=>$eve_winning_no,
		"evening_fireball"=>$eve_fireball,
		"evening_winner"=>$eve_winner,
		"evening_total_payout"=>$eve_total_payout,
		);
	// $dd[] = $name;
	// echo $title."<br>\n";
	// echo $name."<br>\n";
	// echo $contact_no."<br>\n";
	// echo $email."<br>\n";


	return json_encode(["status"=>true,"message"=>"Lottery data found.","data"=>$data]);
			// echo "<pre>";
			// var_dump($dd);
	}
	else {
		return json_encode(["status"=>false,"message"=>"Lottery data not found.","data"=>[]]);
	}
}

?>