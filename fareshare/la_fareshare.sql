-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 22, 2021 at 09:26 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `la_fareshare`
--

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `restaurant_id` int(10) UNSIGNED NOT NULL,
  `discount` float DEFAULT NULL,
  `tax` float DEFAULT NULL,
  `total_amount` float DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `user_id`, `restaurant_id`, `discount`, `tax`, `total_amount`, `grand_total`, `created_at`, `updated_at`) VALUES
(15, 154, 22, 0, 0, 7, 7, '2021-02-12 11:08:16', '2021-02-12 11:08:16');

-- --------------------------------------------------------

--
-- Table structure for table `cart_items`
--

CREATE TABLE `cart_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `coupon_code_id` int(11) DEFAULT NULL,
  `qty` smallint(6) NOT NULL,
  `price` float NOT NULL,
  `sub_total` float NOT NULL,
  `member_id` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_items`
--

INSERT INTO `cart_items` (`id`, `cart_id`, `item_id`, `coupon_code_id`, `qty`, `price`, `sub_total`, `member_id`, `created_at`, `updated_at`) VALUES
(39, 15, 36, NULL, 1, 7, 7, 0, '2021-02-12 11:08:16', '2021-02-12 11:08:16');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(200) NOT NULL,
  `status` tinyint(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'Appetizers', 0, '2020-07-01 05:39:21', '2021-02-05 11:12:24'),
(2, 0, 'Burgers', 0, '2020-07-01 05:39:21', '2021-02-05 11:12:28'),
(3, 0, 'Desserts', 1, '2020-07-01 05:39:21', '2021-02-05 13:12:28'),
(4, 0, 'Breads', 0, '2020-07-01 05:39:21', '2021-02-15 11:25:21'),
(5, 0, 'Sweets', 0, '2020-07-01 05:39:21', '2021-02-05 11:12:40'),
(6, 0, 'Curries', 1, '2020-07-01 05:39:21', '2021-02-05 13:12:38'),
(7, 0, 'Rolls', 0, '2020-07-01 05:39:21', '2021-02-05 11:12:52'),
(8, 2, 'Vegetarian', 0, '2020-07-01 05:39:21', '2021-02-05 11:13:02'),
(9, 2, 'Non-Vegetarian', 0, '2020-07-01 05:39:21', '2021-02-05 11:12:57'),
(10, 1, 'Vegetarian', 0, '2020-07-01 05:39:21', '2021-02-05 11:13:06'),
(11, 1, 'Non-Vegetarian', 0, '2020-07-01 05:39:21', '2021-02-05 11:19:20'),
(12, 3, 'Vegetarian', 0, '2020-07-01 05:39:21', '2021-02-05 11:16:21'),
(13, 3, 'Non-Vegetarian', 0, '2020-07-01 05:39:21', '2021-02-05 11:16:27'),
(14, 7, 'Non-Vegetarian', 0, '2020-07-01 05:39:21', '2021-02-05 11:17:32'),
(15, 0, 'Continental foods', 1, '2020-07-01 05:39:21', '2021-02-05 13:13:58'),
(16, 15, 'abc', 0, '2020-07-01 05:39:21', '2021-02-05 11:18:08'),
(17, 15, 'Hamus', 0, '2020-07-01 05:39:21', '2021-02-05 11:18:14'),
(18, 0, 'Starters', 1, '2021-02-05 10:51:49', '2021-02-05 13:12:54'),
(19, 0, 'Veg Curries', 0, '2021-02-05 10:52:14', '2021-02-05 11:17:19'),
(20, 0, 'Non-Veg Curreies', 0, '2021-02-05 10:52:37', '2021-02-05 11:17:14'),
(21, 0, 'Desserts', 0, '2021-02-05 10:53:01', '2021-02-05 11:16:52'),
(22, 0, 'Breads', 0, '2021-02-05 10:53:16', '2021-02-05 11:16:58'),
(23, 0, 'Accompaniments', 1, '2021-02-05 10:54:56', '2021-02-05 13:13:12'),
(24, 4, 'Bread', 1, '2021-02-15 11:23:53', '2021-02-15 11:24:20');

-- --------------------------------------------------------

--
-- Table structure for table `cms`
--

CREATE TABLE `cms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms`
--

INSERT INTO `cms` (`id`, `title`, `slug`, `content`, `meta_title`, `meta_tags`, `meta_description`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Privacy Policy', 'privacy-policy', '<p>text</p>', NULL, NULL, NULL, 0, 0, NULL, NULL),
(2, 'Terms And Conditions', 'terms-and-condition', '<p>text</p>', NULL, NULL, NULL, 0, 0, NULL, NULL),
(3, 'Driver Agreement', 'driver-agreement', '<h3 class=\"text-center\" style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">DRIVER AGREEMENT</h3><p style=\"color: rgb(33, 37, 41);\">Name________________</p><p style=\"color: rgb(33, 37, 41);\">Full Legal Address______________________________________</p><p style=\"color: rgb(33, 37, 41);\">This Driver Agreement (the “Agreement”) is entered into as of _________ __, 20____ (the “Effective Date”) by and between _________, a _______ [individual, corporation/limited liability company/partnership/etc.] (the “Driver” or “You”), and Fair Share, LLC, a limited liability company (the “Company,” and together with the Driver referred to as the “Parties”).</p><h3 class=\"text-center\" style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">ABOUT THE FAIR SHARE PLATFORM</h3><p style=\"color: rgb(33, 37, 41);\">Company has created an app based platform which connects its paying Users to available Drivers for certain services.</p><p style=\"color: rgb(33, 37, 41);\">The Fair Share platform provides a service allowing its Users to order food and beverages from certain affiliated restaurants or “Vendors” and have their order delivered to them by a Fair Share Driver. As a Driver you will be matched with a User through the platform. You will have the ability to accept or decline a delivery request with certain restrictions. If you choose to accept the Delivery request then you will be provided with pertinent information such as User name, Vendor pick up location and pick up time, Order Number if applicable, Delivery location.</p><p style=\"color: rgb(33, 37, 41);\">The platform features a Driver “Wallet” wherein your payments are maintained until such time as you choose to cash it out.</p><p style=\"color: rgb(33, 37, 41);\">Whereas the Driver wishes to perform services to the Company and the Company wishes to pay the Driver for these services, and for other good consideration, the Parties hereby agree as follows:</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">1. ENGAGEMENT SERVICES</h3><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">a. Engagement</h4><p style=\"color: rgb(33, 37, 41);\">The Company retains the Driver to provide, and the Driver shall provide, the following services: Once a Delivery Job is offered to and accepted by the Driver via the Fair Share app, then the Driver shall promptly arrive at the designated Vendor’s location within the time frame provided, provide the necessary Order information to the Vendor, pick up the Order and promptly deliver same to the designated User.</p><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">b. Services.</h4><p style=\"color: rgb(33, 37, 41);\">Without limiting the scope of Services described above, the Driver shall:</p><p style=\"color: rgb(33, 37, 41);\">(i) perform the Services set forth in&nbsp;<span style=\"font-weight: bolder;\">1(a)</span>&nbsp;above;</p><p style=\"color: rgb(33, 37, 41);\">(ii) devote as much productive time, energy, and ability to the performance of their duties under this agreement as may be necessary to provide the required Services in a timely manner;</p><p style=\"color: rgb(33, 37, 41);\">(iii) perform the Services in a safe, good, and workmanlike manner maintaining a professional demeanor at all times and agreeing to abide by local traffic law;</p><p style=\"color: rgb(33, 37, 41);\">(iv) communicate with the Company about any issues with the app, Vendors or Users that they may encounter;</p><p style=\"color: rgb(33, 37, 41);\">(v) supply their own automobile and maintain liability insurance in the policy limits required by law;</p><p style=\"color: rgb(33, 37, 41);\">(vi) provide Services that are satisfactory and acceptable to the Company;</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">2. COMPENSATION</h3><p style=\"color: rgb(33, 37, 41);\">You will be compensated in the form of tips to be paid by the Users. Users will be given the opportunity to add a tip based on percentage through the app as well prior to Delivery. The default tip amount will be up to 15%. No other compensation will be given.</p><p style=\"color: rgb(33, 37, 41);\">Changes to compensation are subject to change without notice please review periodically any updates on our main FairShare, LLC website under the employee handbook manual link.</p><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">(a) No Payments in Certain Circumstances.</h4><p style=\"color: rgb(33, 37, 41);\">No payment will be payable to the Driver under any of the following circumstances:</p><p style=\"color: rgb(33, 37, 41);\">A. if prohibited under applicable government law, regulation, or policy;</p><p style=\"color: rgb(33, 37, 41);\">B. if the Driver did not directly perform or complete the Services described in&nbsp;<span style=\"font-weight: bolder;\">1(a)</span>;</p><p style=\"color: rgb(33, 37, 41);\">C. if the Driver did not perform the Services to the reasonable satisfaction of the Company; or</p><p style=\"color: rgb(33, 37, 41);\">D. if the Services performed occurred after the expiration or termination of the Term, unless otherwise agreed in writing.</p><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">c. No Other Compensation.</h4><p style=\"color: rgb(33, 37, 41);\">The compensation set out above will be the Driver’s sole compensation under this agreement.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">Expenses</h3><p style=\"color: rgb(33, 37, 41);\">Any ordinary and necessary expenses incurred by the Driver or their staff in the performance of this agreement will be the Driver’s sole responsibility.</p><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">e. Background Check.</h4><p style=\"color: rgb(33, 37, 41);\">Prior to any driver being able to perform actions which would reflect He/She as an independent contractor under Fare Share, all drivers are required to undergo a background check. The cost for this service per driver is in the amount of 10.00 which should be paid at the time of application through the Fare Share website (Fareshare.info.). Applications should be completed through the Fare Share app which can be found in the Google Play app Store and or the Apple store. The fee for the background check at the time of application is nonrefundable. Any offense that is consonsider any potential risk will be at the consideration of the Admin and at the Admins discretion solely.</p><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">f. Taxes.</h4><p style=\"color: rgb(33, 37, 41);\">The Driver is solely responsible for the payment of all income, social security, employment-related, or other taxes incurred as a result of the performance of the Services by the Driver under this agreement, and for all obligations, reports, and timely notifications relating to those taxes. The Company has no obligation to pay or withhold any sums for those taxes.</p><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">g. Other Benefits</h4><p style=\"color: rgb(33, 37, 41);\">The Driver has no claim against the Company under this agreement or otherwise for vacation pay, sick leave, retirement benefits, social security, worker’s compensation, health or disability benefits, unemployment insurance benefits, or employee benefits of any kind.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">3. PAYMENTS</h3><p style=\"color: rgb(33, 37, 41);\">Company will transfer your earned Fare and any additional tips to your “Wallet” featured on the App within ____ (days/hours) after User has confirmed that Delivery has been made. Under no circumstances will Driver be compensated until Company receives payment from Users.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">4. CANCELLATIONS</h3><p style=\"color: rgb(33, 37, 41);\">If the User cancels their Order prior to delivery then no compensation will be earned.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">5. TERM AND TERMINATION</h3><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">a. Term</h4><p style=\"color: rgb(33, 37, 41);\">This agreement will become effective once it is executed by Driver. This agreement will continue until the Services have been satisfactorily completed and the Contractor has been paid in full for such Services.</p><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">b. Termination</h4><p style=\"color: rgb(33, 37, 41);\">This agreement may be terminated:</p><p style=\"color: rgb(33, 37, 41);\">(1) by either party on provision of 3 days’ written notice to the other party, with or without cause;</p><p style=\"color: rgb(33, 37, 41);\">(2) by either party for a material breach of any provision of this agreement by the other party, if the other party’s material breach is not cured within 3 days of receipt of written notice of the breach;</p><p style=\"color: rgb(33, 37, 41);\">(3) automatically, on the death of the Contractor.</p><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">c. Effect of Termination</h4><p style=\"color: rgb(33, 37, 41);\">After the termination of this agreement for any reason, the Company shall promptly pay the Contractor for Services rendered before the effective date of the termination. No other compensation, of any nature or type, will be payable after the termination of this agreement.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">6. NATURE OF RELATIONSHIP</h3><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">h. Independent Contractor Status</h4><p style=\"color: rgb(33, 37, 41);\">(a) The relationship of the parties under this agreement is one of independent Contractors, and no joint venture, partnership, agency, employer-employee, or similar relationship is created in or by this agreement. Neither party may assume or create obligations on the other party’s behalf, and neither party may take any action that creates the appearance of such authority.</p><p style=\"color: rgb(33, 37, 41);\">(b) The Driver has the right to accept or refuse Delivery Jobs in their sole discretion.</p><p style=\"color: rgb(33, 37, 41);\">(c) The Driver will be responsible for their own transportation to and from Delivery Jobs.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">7. INDEMNIFICATION</h3><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">i. Of Company by Driver.</h4><p style=\"color: rgb(33, 37, 41);\">At all times after the effective date of this agreement, the Contractor shall indemnify the Company and its members, managers, owners, successors, and assigns (collectively, the “Company Indemnitees”) from all damages, liabilities, expenses, claims, or judgments (including interest, penalties, reasonable attorneys’ fees, accounting fees, and expert witness fees) (collectively, the “Claims”) that any Company Indemnitee may incur and that arise from:</p><p style=\"color: rgb(33, 37, 41);\">A. the Driver’s gross negligence or willful misconduct arising from the Contractor’s carrying out of their obligations under this agreement;</p><p style=\"color: rgb(33, 37, 41);\">B. the Driver’s breach of any of their obligations or representations under this agreement; or</p><p style=\"color: rgb(33, 37, 41);\">C. the Driver’s breach of their express representation that he/she is an independent contractor and in compliance with all applicable laws related to work as an independent contractor. If a regulatory body or court of competent jurisdiction finds that the Driver is not an independent contractor or is not in compliance with applicable laws related to work as an independent contractor, based on the Driver’s own actions, the Driver will assume full responsibility and liability for all taxes, assessments, and penalties imposed against the Driver or the Company resulting from that contrary interpretation, including taxes, assessments, and penalties that would have been deducted from the Driver’s earnings if the Driver had been on the Company’s payroll and employed as a Company employee.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">8. ASSIGNMENT</h3><p style=\"color: rgb(33, 37, 41);\">Neither party may assign this agreement or delegate any of their duties to any third party without the written consent of the other party.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">9. SUCCESSORS AND ASSIGNS</h3><p style=\"color: rgb(33, 37, 41);\">All references in this Agreement to the Parties shall be deemed to include, as applicable, a reference to their respective successors and assigns. The provisions of this Agreement shall be binding on and shall insure to the benefit of the successors and assigns of the Parties.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">10. CHOICE OF LAW</h3><p style=\"color: rgb(33, 37, 41);\">The parties agree that the laws of the State of New Jersey shall govern any dispute relating to this Agreement and further agree to submit themselves to the exclusive jurisdiction of the Courts of the State of New Jersey with regard to the adjudication of any disputes resulting therefrom.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">SIGNATURE AND DATE</h3><p style=\"color: rgb(33, 37, 41);\">Signature ______________________</p><p style=\"color: rgb(33, 37, 41);\">Date ______________</p>', NULL, NULL, NULL, 0, 0, NULL, NULL),
(4, 'Vendor Agreement', 'vendor-agreement', '<h3 class=\"text-center\" style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">Vendor Agreement Document</h3><p style=\"color: rgb(33, 37, 41);\">This Vendor Agreement (the “Agreement”) is entered into as of _________ __, 20____ (the “Effective Date”) by and between _________, a _______ [individual, corporation/limited liability company/partnership/etc.] (the “Vendor” or “You”), and Fair Share, LLC, a limited liability company (the “Company,” and together with the Vendor referred to as the “Parties”).</p><h3 class=\"text-center\" style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">ABOUT THE FAIR SHARE PLATFORM</h3><p style=\"color: rgb(33, 37, 41);\">Company has created an app based platform which connects its paying Users to participating restaurants or “Vendors” to order food and beverages for pick-up and delivery provided by third party Drivers.</p><p style=\"color: rgb(33, 37, 41);\">The Fair Share platform allows you to upload your take out menu so that your restaurant and your menu is made available to our Users. Our Users may place orders, pay for those orders and have the orders picked up and delivered by a third party affiliated Driver all through our app.</p><p style=\"color: rgb(33, 37, 41);\">As a Vendor you will be relayed the information to fulfil the order and you will make the order available for pick-up and delivery. We will collect all payments and will tack on our service fees and Driver fees to your menu charges to be paid by the User. We will transmit your payment to you after payment is received from User.</p><p style=\"color: rgb(33, 37, 41);\">The platform features a Vendor “Wallet” wherein your payments are maintained until such time as you choose to cash it out.</p><p style=\"color: rgb(33, 37, 41);\">Whereas the Vendor wishes to participate and be featured on the Fair Share platform, and the Vendor wishes to pay the Company for its services, and for other good consideration, the Parties hereby agree as follows:</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">1. ENGAGEMENT SERVICES</h3><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">a. Engagement</h4><p style=\"color: rgb(33, 37, 41);\">The Vendor retains the Company to provide, and the Company shall provide, the following services: Once the Vendor information including text and photos, is uploaded to the Fair Share app it will be included in a catalog of Vendors which will be made available to our Users. When Orders are placed then Company will relay the Order information to Vendor in order to be fulfilled. A third party Driver will be give pertinent information in order to pick up the User’s Order and deliver it to them.</p><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">b. Vendor Obligations.</h4><p style=\"color: rgb(33, 37, 41);\">Vendor is responsible for maintaining an up to date menu on the app. Vendor is responsible for accurately and promptly fulfilling orders and making same available to the third party Drivers. Orders shall be packaged in a professional and safe using quality packaging materials manner ensuring the the freshness and temperature of the food.</p><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">c. Company obligations.</h4><p style=\"color: rgb(33, 37, 41);\">Company is responsible for relaying accurate information to the Vendor in a timely fashion; Company is responsible for transmitting payment to Vendor through the Wallet feature once orders have been fulfilled.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">2. FAIR SHARE FEES</h3><p style=\"color: rgb(33, 37, 41);\">Vendor shall pay Company a flat fee of $__________ per month in order to be featured on the platform. Company will also be collecting a service fee/driver fee from the User which will be added to the menu prices available to the Users. This additional fee will be agreed upon by the parties.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">3. PAYMENTS</h3><p style=\"color: rgb(33, 37, 41);\">Company will transfer your payment to your “Wallet” featured on the platform once payment is received from User. Under no circumstances will Vendor be compensated until Company receives payment from Users.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">4. CANCELLATIONS</h3><p style=\"color: rgb(33, 37, 41);\">If the User cancels their Order prior to delivery then no compensation will be earned.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">5. TERM AND TERMINATION</h3><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">a. Term</h4><p style=\"color: rgb(33, 37, 41);\">This agreement will become effective once it is executed by Vendor. This agreement will continue until the Services have been satisfactorily completed and the Contractor has been paid in full for such Services.</p><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">b. Termination</h4><p style=\"color: rgb(33, 37, 41);\">This agreement may be terminated:</p><p style=\"color: rgb(33, 37, 41);\">(1) by either party on provision of 3 days’ written notice to the other party, with or without cause;</p><p style=\"color: rgb(33, 37, 41);\">(2) by either party for a material breach of any provision of this agreement by the other party, if the other party’s material breach is not cured within 3 days of receipt of written notice of the breach;</p><p style=\"color: rgb(33, 37, 41);\">(3) automatically, on the death of the Contractor.</p><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">c. Effect of Termination</h4><p style=\"color: rgb(33, 37, 41);\">After the termination of this agreement for any reason, the Company shall promptly pay the Contractor for Services rendered before the effective date of the termination. No other compensation, of any nature or type, will be payable after the termination of this agreement.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">6. NATURE OF RELATIONSHIP</h3><h4 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">d. Independent Contractor Status.</h4><p style=\"color: rgb(33, 37, 41);\">(a) The relationship of the parties under this agreement is one of independent Contractors, and no joint venture, partnership, agency, employer-employee, or similar relationship is created in or by this agreement. Neither party may assume or create obligations on the other party’s behalf, and neither party may take any action that creates the appearance of such authority.</p><p style=\"color: rgb(33, 37, 41);\">(b) The Vendor has the right to accept or refuse Delivery Jobs in their sole discretion.</p><p style=\"color: rgb(33, 37, 41);\">(c) The Vendor will be responsible for their own transportation to and from Delivery Jobs.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">7. INDEMNIFICATION</h3><p style=\"color: rgb(33, 37, 41);\">e. Of Company by Vendor. At all times after the effective date of this agreement, the Contractor shall indemnify the Company and its members, managers, owners, successors, and assigns (collectively, the “Company Indemnities”) from all damages, liabilities, expenses, claims, or judgements (including interest, penalties, reasonable attorneys’ fees, accounting fees, and expert witness fees) (collectively, the “Claims”) that any Company Indemnity may incur and that arise from:</p><p style=\"color: rgb(33, 37, 41);\">A. the Vendor’s gross negligence or wilful misconduct arising from the Contractor’s carrying out of their obligations under this agreement;</p><p style=\"color: rgb(33, 37, 41);\">B. the Vendor’s breach of any of their obligations or representations under this agreement; or</p><p style=\"color: rgb(33, 37, 41);\">C. the Vendor’s breach of their express representation that he/she is an independent contractor and in compliance with all applicable laws related to work as an independent contractor. If a regulatory body or court of competent jurisdiction finds that the Vendor is not an independent contractor or is not in compliance with applicable laws related to work as an independent contractor, based on the Vendor’s own actions, the Vendor will assume full responsibility and liability for all taxes, assessments, and penalties imposed against the Vendor or the Company resulting from that contrary interpretation, including taxes, assessments, and penalties that would have been deducted from the Vendor’s earnings if the Vendor had been on the Company’s payroll and employed as a Company employee.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">8. ASSIGNMENT</h3><p style=\"color: rgb(33, 37, 41);\">Neither party may assign this agreement or delegate any of their duties to any third party without the written consent of the other party.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">9. SUCCESSORS AND ASSIGNS</h3><p style=\"color: rgb(33, 37, 41);\">All references in this Agreement to the Parties shall be deemed to include, as applicable, a reference to their respective successors and assigns. The provisions of this Agreement shall be binding on and shall insure to the benefit of the successors and assigns of the Parties.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">10. CHOICE OF LAW</h3><p style=\"color: rgb(33, 37, 41);\">The parties agree that the laws of the State of New Jersey shall govern any dispute relating to this Agreement and further agree to submit themselves to the exclusive jurisdiction of the Courts of the State of New Jersey with regard to the adjudication of any disputes resulting therefrom.</p><h3 style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41);\">SIGNATURE AND DATE</h3><p style=\"color: rgb(33, 37, 41);\">Signature ______________________</p><p style=\"color: rgb(33, 37, 41);\">Date ______________</p>', NULL, NULL, NULL, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `continentals`
--

CREATE TABLE `continentals` (
  `id` int(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(20) DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_ta` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `continentals`
--

INSERT INTO `continentals` (`id`, `name`, `status`, `created_at`, `updated_ta`) VALUES
(1, 'European ', 1, '2020-05-25 18:41:43', '2020-05-25 18:41:43'),
(2, 'Italian', 1, '2020-05-25 18:41:56', '2020-05-25 18:41:56'),
(3, 'American', 1, '2020-05-25 18:42:06', '2020-05-25 18:42:06'),
(4, 'French', 1, '2020-05-25 18:42:15', '2020-05-25 18:42:15'),
(5, 'Mediterranean', 1, '2020-05-25 18:42:24', '2020-05-25 18:42:24'),
(6, 'Chinese', 1, '2020-05-25 18:42:32', '2020-05-25 18:42:32');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`, `phonecode`) VALUES
(1, 'AF', 'Afghanistan', 93),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Algeria', 213),
(4, 'AS', 'American Samoa', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguilla', 1264),
(8, 'AQ', 'Antarctica', 0),
(9, 'AG', 'Antigua And Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaijan', 994),
(16, 'BS', 'Bahamas The', 1242),
(17, 'BH', 'Bahrain', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarus', 375),
(21, 'BE', 'Belgium', 32),
(22, 'BZ', 'Belize', 501),
(23, 'BJ', 'Benin', 229),
(24, 'BM', 'Bermuda', 1441),
(25, 'BT', 'Bhutan', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia and Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 55),
(31, 'IO', 'British Indian Ocean Territory', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Cambodia', 855),
(37, 'CM', 'Cameroon', 237),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 238),
(40, 'KY', 'Cayman Islands', 1345),
(41, 'CF', 'Central African Republic', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Christmas Island', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoros', 269),
(49, 'CG', 'Republic Of The Congo', 242),
(50, 'CD', 'Democratic Republic Of The Congo', 242),
(51, 'CK', 'Cook Islands', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 225),
(54, 'HR', 'Croatia (Hrvatska)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Cyprus', 357),
(57, 'CZ', 'Czech Republic', 420),
(58, 'DK', 'Denmark', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'Dominican Republic', 1809),
(62, 'TP', 'East Timor', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'XA', 'External Territories of Australia', 61),
(71, 'FK', 'Falkland Islands', 500),
(72, 'FO', 'Faroe Islands', 298),
(73, 'FJ', 'Fiji Islands', 679),
(74, 'FI', 'Finland', 358),
(75, 'FR', 'France', 33),
(76, 'GF', 'French Guiana', 594),
(77, 'PF', 'French Polynesia', 689),
(78, 'TF', 'French Southern Territories', 0),
(79, 'GA', 'Gabon', 241),
(80, 'GM', 'Gambia The', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Germany', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Greece', 30),
(86, 'GL', 'Greenland', 299),
(87, 'GD', 'Grenada', 1473),
(88, 'GP', 'Guadeloupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey and Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea-Bissau', 245),
(94, 'GY', 'Guyana', 592),
(95, 'HT', 'Haiti', 509),
(96, 'HM', 'Heard and McDonald Islands', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong S.A.R.', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italy', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japan', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordan', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenya', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Korea North', 850),
(116, 'KR', 'Korea South', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kyrgyzstan', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Latvia', 371),
(121, 'LB', 'Lebanon', 961),
(122, 'LS', 'Lesotho', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libya', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lithuania', 370),
(127, 'LU', 'Luxembourg', 352),
(128, 'MO', 'Macau S.A.R.', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malaysia', 60),
(133, 'MV', 'Maldives', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Man (Isle of)', 44),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976),
(147, 'MS', 'Montserrat', 1664),
(148, 'MA', 'Morocco', 212),
(149, 'MZ', 'Mozambique', 258),
(150, 'MM', 'Myanmar', 95),
(151, 'NA', 'Namibia', 264),
(152, 'NR', 'Nauru', 674),
(153, 'NP', 'Nepal', 977),
(154, 'AN', 'Netherlands Antilles', 599),
(155, 'NL', 'Netherlands The', 31),
(156, 'NC', 'New Caledonia', 687),
(157, 'NZ', 'New Zealand', 64),
(158, 'NI', 'Nicaragua', 505),
(159, 'NE', 'Niger', 227),
(160, 'NG', 'Nigeria', 234),
(161, 'NU', 'Niue', 683),
(162, 'NF', 'Norfolk Island', 672),
(163, 'MP', 'Northern Mariana Islands', 1670),
(164, 'NO', 'Norway', 47),
(165, 'OM', 'Oman', 968),
(166, 'PK', 'Pakistan', 92),
(167, 'PW', 'Palau', 680),
(168, 'PS', 'Palestinian Territory Occupied', 970),
(169, 'PA', 'Panama', 507),
(170, 'PG', 'Papua new Guinea', 675),
(171, 'PY', 'Paraguay', 595),
(172, 'PE', 'Peru', 51),
(173, 'PH', 'Philippines', 63),
(174, 'PN', 'Pitcairn Island', 0),
(175, 'PL', 'Poland', 48),
(176, 'PT', 'Portugal', 351),
(177, 'PR', 'Puerto Rico', 1787),
(178, 'QA', 'Qatar', 974),
(179, 'RE', 'Reunion', 262),
(180, 'RO', 'Romania', 40),
(181, 'RU', 'Russia', 70),
(182, 'RW', 'Rwanda', 250),
(183, 'SH', 'Saint Helena', 290),
(184, 'KN', 'Saint Kitts And Nevis', 1869),
(185, 'LC', 'Saint Lucia', 1758),
(186, 'PM', 'Saint Pierre and Miquelon', 508),
(187, 'VC', 'Saint Vincent And The Grenadines', 1784),
(188, 'WS', 'Samoa', 684),
(189, 'SM', 'San Marino', 378),
(190, 'ST', 'Sao Tome and Principe', 239),
(191, 'SA', 'Saudi Arabia', 966),
(192, 'SN', 'Senegal', 221),
(193, 'RS', 'Serbia', 381),
(194, 'SC', 'Seychelles', 248),
(195, 'SL', 'Sierra Leone', 232),
(196, 'SG', 'Singapore', 65),
(197, 'SK', 'Slovakia', 421),
(198, 'SI', 'Slovenia', 386),
(199, 'XG', 'Smaller Territories of the UK', 44),
(200, 'SB', 'Solomon Islands', 677),
(201, 'SO', 'Somalia', 252),
(202, 'ZA', 'South Africa', 27),
(203, 'GS', 'South Georgia', 0),
(204, 'SS', 'South Sudan', 211),
(205, 'ES', 'Spain', 34),
(206, 'LK', 'Sri Lanka', 94),
(207, 'SD', 'Sudan', 249),
(208, 'SR', 'Suriname', 597),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 47),
(210, 'SZ', 'Swaziland', 268),
(211, 'SE', 'Sweden', 46),
(212, 'CH', 'Switzerland', 41),
(213, 'SY', 'Syria', 963),
(214, 'TW', 'Taiwan', 886),
(215, 'TJ', 'Tajikistan', 992),
(216, 'TZ', 'Tanzania', 255),
(217, 'TH', 'Thailand', 66),
(218, 'TG', 'Togo', 228),
(219, 'TK', 'Tokelau', 690),
(220, 'TO', 'Tonga', 676),
(221, 'TT', 'Trinidad And Tobago', 1868),
(222, 'TN', 'Tunisia', 216),
(223, 'TR', 'Turkey', 90),
(224, 'TM', 'Turkmenistan', 7370),
(225, 'TC', 'Turks And Caicos Islands', 1649),
(226, 'TV', 'Tuvalu', 688),
(227, 'UG', 'Uganda', 256),
(228, 'UA', 'Ukraine', 380),
(229, 'AE', 'United Arab Emirates', 971),
(230, 'GB', 'United Kingdom', 44),
(231, 'US', 'United States', 1),
(232, 'UM', 'United States Minor Outlying Islands', 1),
(233, 'UY', 'Uruguay', 598),
(234, 'UZ', 'Uzbekistan', 998),
(235, 'VU', 'Vanuatu', 678),
(236, 'VA', 'Vatican City State (Holy See)', 39),
(237, 'VE', 'Venezuela', 58),
(238, 'VN', 'Vietnam', 84),
(239, 'VG', 'Virgin Islands (British)', 1284),
(240, 'VI', 'Virgin Islands (US)', 1340),
(241, 'WF', 'Wallis And Futuna Islands', 681),
(242, 'EH', 'Western Sahara', 212),
(243, 'YE', 'Yemen', 967),
(244, 'YU', 'Yugoslavia', 38),
(245, 'ZM', 'Zambia', 260),
(246, 'ZW', 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `driver_details`
--

CREATE TABLE `driver_details` (
  `id` int(20) NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `driver_code` varchar(20) DEFAULT NULL,
  `driving_licence_number` varchar(255) DEFAULT NULL,
  `driving_licence_expiry_date` varchar(255) DEFAULT NULL,
  `driving_licence_image` varchar(255) DEFAULT NULL,
  `vehicle_registration_number` varchar(255) DEFAULT NULL,
  `vehicle_registration_expiry_date` varchar(255) DEFAULT NULL,
  `vehicle_registration_image` varchar(255) DEFAULT NULL,
  `vehicle_insurance_number` varchar(255) DEFAULT NULL,
  `vehicle_insurance_expiry_date` varchar(255) DEFAULT NULL,
  `vehicle_insurance_image` varchar(255) DEFAULT NULL,
  `vehicle_make` varchar(255) DEFAULT NULL,
  `vehicle_model` varchar(255) DEFAULT NULL,
  `vehicle_year` varchar(255) DEFAULT NULL,
  `vehicle_color` varchar(255) DEFAULT NULL,
  `status` tinyint(2) UNSIGNED ZEROFILL DEFAULT 01,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `driver_details`
--

INSERT INTO `driver_details` (`id`, `user_id`, `driver_code`, `driving_licence_number`, `driving_licence_expiry_date`, `driving_licence_image`, `vehicle_registration_number`, `vehicle_registration_expiry_date`, `vehicle_registration_image`, `vehicle_insurance_number`, `vehicle_insurance_expiry_date`, `vehicle_insurance_image`, `vehicle_make`, `vehicle_model`, `vehicle_year`, `vehicle_color`, `status`, `created_at`, `updated_at`) VALUES
(8, 137, 'FSD137', 'HHHHHH7272HH', '2024-02-05T12:10:51.503+05:30', 'storage/app/public/drivers/137/licence/137_driving_licence_1612507532.', 'RJ52HH8282', '2025-02-05T12:10:51.504+05:30', 'storage/app/public/drivers/137/vehicle_registration/137_vehicle_registration_1612507532.', 'JJJJJJ72727HH', '2024-02-05T12:10:51.505+05:30', 'storage/app/public/drivers/137/vehicle_insurance/137_vehicle_insurance_1612507532.', 'Dummy', '2020', '2017-02-05T12:10:51.506+05:30', 'Red', 01, '2021-02-05 06:45:32', '2021-02-05 06:45:32');

-- --------------------------------------------------------

--
-- Table structure for table `driver_partners`
--

CREATE TABLE `driver_partners` (
  `id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `driver_ratings`
--

CREATE TABLE `driver_ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `order_id` bigint(20) NOT NULL DEFAULT 0,
  `rating` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 1,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `group_members`
--

CREATE TABLE `group_members` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('Accepted','Rejected','Requested') NOT NULL DEFAULT 'Requested',
  `payment_method` varchar(200) DEFAULT NULL,
  `payment_method_status` enum('Accepted','Rejected') NOT NULL DEFAULT 'Rejected',
  `payment_amount` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_members`
--

INSERT INTO `group_members` (`id`, `group_id`, `user_id`, `status`, `payment_method`, `payment_method_status`, `payment_amount`) VALUES
(1, 8, 140, 'Accepted', 'Wallet', 'Accepted', 0),
(2, 8, 140, 'Requested', NULL, 'Rejected', 0),
(3, 9, 140, 'Accepted', 'Wallet', 'Accepted', 0),
(4, 9, 148, 'Accepted', NULL, 'Rejected', 0),
(5, 9, 149, 'Requested', NULL, 'Rejected', 0),
(6, 9, 150, 'Requested', NULL, 'Rejected', 0),
(14, 12, 155, 'Accepted', 'Wallet', 'Accepted', 0),
(15, 12, 134, 'Requested', NULL, 'Rejected', 0),
(17, 13, 155, 'Accepted', 'Wallet', 'Accepted', 0),
(18, 13, 140, 'Requested', NULL, 'Rejected', 0),
(19, 12, 140, 'Requested', NULL, 'Rejected', 0),
(20, 12, 155, 'Requested', NULL, 'Rejected', 0);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_title` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `item_price` float NOT NULL,
  `item_description` text DEFAULT NULL,
  `coupon_code` varchar(60) DEFAULT '0',
  `coupon_code_percentage` float DEFAULT NULL,
  `item_type` enum('Vegetarian','Non-Vegetarian') NOT NULL DEFAULT 'Vegetarian',
  `status` tinyint(1) UNSIGNED ZEROFILL NOT NULL COMMENT '0-Delete, 1-Active, 2-Unavailable',
  `restaurant_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `item_title`, `category_id`, `sub_category_id`, `item_price`, `item_description`, `coupon_code`, `coupon_code_percentage`, `item_type`, `status`, `restaurant_id`, `created_at`, `updated_at`) VALUES
(1, 'dish 1', 1, 10, 30, 'this item contains 10% discount  on itself', 'code10', 10, 'Vegetarian', 1, 1, '2020-07-17 07:02:56', '2020-07-17 07:13:50'),
(2, 'dish 2', 2, 8, 20, '5 % discount', 'code5', 5, 'Non-Vegetarian', 1, 1, '2020-07-17 07:16:32', '2020-07-17 07:16:32'),
(3, 'dish 3', 3, 12, 35, 'item with 5 % discount', 'code5', 5, 'Non-Vegetarian', 1, 1, '2020-07-17 07:20:15', '2020-07-17 07:20:15'),
(4, 'dish 1 1', 1, 11, 40, 'non veg appetizers', 'code5', 0, 'Non-Vegetarian', 1, 1, '2020-07-17 07:21:52', '2020-07-17 07:21:52'),
(5, 'dish 1 2', 1, 10, 50, 'another item  40% off', 'code 40', 40, 'Vegetarian', 1, 1, '2020-07-17 10:52:45', '2020-07-17 10:52:45'),
(6, 'dish 2 1', 2, 9, 30, '30 % off', 'code30', 30, 'Vegetarian', 1, 1, '2020-07-17 11:04:54', '2020-07-17 11:04:54'),
(7, 'R 1  M 1', 1, 10, 30, '10 % discount', 'r1m110', 10, 'Non-Vegetarian', 1, 5, '2020-07-20 08:52:01', '2020-07-20 08:52:01'),
(8, 'R 1 M 2', 1, 11, 30, '10 % discount', 'r1m210', 10, 'Vegetarian', 1, 5, '2020-07-20 08:53:15', '2020-07-20 08:53:15'),
(9, 'R 1 M 3', 2, 8, 30, '20% discount', 'r1m320', 20, 'Vegetarian', 1, 5, '2020-07-20 08:54:29', '2020-07-20 08:54:29'),
(10, 'R 1 M 4', 2, 9, 40, '20% off', 'r1m420', 20, 'Non-Vegetarian', 1, 5, '2020-07-20 08:55:51', '2020-07-20 08:55:51'),
(11, 'R 1 m5', 3, 12, 20, '10% off', 'r1m510', 10, 'Vegetarian', 1, 5, '2020-07-20 08:57:31', '2020-07-20 08:57:31'),
(12, 'R 2 M 1', 1, 10, 20, '10% off', 'r2m110', 10, 'Vegetarian', 1, 6, '2020-07-20 09:01:25', '2020-07-20 09:01:25'),
(13, 'R 2 M 2', 1, 11, 20, '20% off', 'r2m220', 20, 'Non-Vegetarian', 1, 6, '2020-07-20 09:02:31', '2020-07-20 09:02:31'),
(14, 'R 2 M 3', 2, 8, 30, '10% off', 'r2m310', 10, 'Vegetarian', 1, 6, '2020-07-20 09:03:50', '2020-07-20 09:03:50'),
(15, 'R 2 M 4', 2, 9, 30, '20 % off', 'r1m420', 20, 'Non-Vegetarian', 1, 6, '2020-07-20 09:05:08', '2020-07-20 09:05:08'),
(16, 'R 2 M 5', 3, 12, 40, '20 % off', 'r2m520', 20, 'Vegetarian', 1, 6, '2020-07-20 09:06:53', '2020-07-20 09:06:53'),
(17, 'Dish', 2, 8, 10, 'Description', '0', 0, 'Vegetarian', 1, 15, '2020-12-08 12:40:16', '2020-12-08 12:40:16'),
(18, 'dish123', 4, NULL, 10, 'test dish', '0', 0, 'Vegetarian', 1, 15, '2020-12-08 12:44:55', '2020-12-08 12:44:55'),
(19, 'burger 2', 2, 8, 20, 'This is burger 2.', '0', 0, 'Vegetarian', 1, 15, '2020-12-11 05:43:56', '2020-12-11 05:43:56'),
(20, 'Vegetable Samosa', 18, 10, 5, 'Deep fried conical pastries stuffed with spiced mashed potatoes and nuts', '0', 0, 'Vegetarian', 1, 22, '2021-02-05 08:58:45', '2021-02-05 11:06:22'),
(21, 'Paneer Pakora', 18, 10, 7.5, 'Cottage cheese coated with gram flour and deep fried', '0', 0, 'Vegetarian', 1, 22, '2021-02-05 09:01:20', '2021-02-05 11:06:45'),
(22, 'Chilli Honey Garlic Potatoes', 18, 10, 10, 'Juliennes of potatoes, deep fried and tossed up with zesty sauce, Yummy!', '0', 0, 'Vegetarian', 1, 22, '2021-02-05 09:03:03', '2021-02-05 11:07:04'),
(23, 'Chicken Manchurian', 18, 11, 20, 'Chicken Manchurian', '0', 0, 'Non-Vegetarian', 1, 22, '2021-02-05 09:14:02', '2021-02-05 11:07:19'),
(24, 'Mix Vegetable Raita', 23, 16, 10, 'Fresh yoghurt with chopped onion,cucumber', '0', 0, 'Vegetarian', 1, 22, '2021-02-05 09:29:11', '2021-02-05 11:13:56'),
(25, 'Masala Boondi Raitha', 23, 16, 5, 'Tiny fried balls of gram flour batter soaked in Yoghurt', '0', 0, 'Vegetarian', 1, 22, '2021-02-05 09:30:26', '2021-02-05 11:14:24'),
(26, 'Plain Yogurt', 15, NULL, 10, 'Plain Yogurt', '0', 0, 'Vegetarian', 1, 22, '2021-02-05 09:36:54', '2021-02-05 09:36:54'),
(27, 'Chicken Tikka Masala', 6, NULL, 17, 'Juicy Chicken pieces grilled in tandoor then cooked with onion based gravy, perfect with hot naan', '0', 0, 'Non-Vegetarian', 1, 22, '2021-02-05 09:39:45', '2021-02-05 09:39:45'),
(28, 'Butter Chicken', 6, NULL, 20, 'Boneless tandoori Chicken simmered in creamy tomato gravy. An all time favourite of Indian Kitchen', '0', 0, 'Non-Vegetarian', 1, 22, '2021-02-05 09:41:28', '2021-02-05 09:41:28'),
(29, 'Chicken Masala', 6, NULL, 20, 'Boneless chicken cooked in mild spicy golden homemade sauce', '0', 0, 'Non-Vegetarian', 1, 22, '2021-02-05 09:42:51', '2021-02-05 09:42:51'),
(30, 'Paneer Butter Masala', 6, NULL, 18, 'Homemade paneer cooked with thick creamy tomato sauce flavored with fenugreek leaves', '0', 0, 'Vegetarian', 1, 22, '2021-02-05 09:47:35', '2021-02-05 09:47:35'),
(31, 'Kadai Paneer', 6, NULL, 25, 'Home made cottage cheese with onion and capsicum in a special kadai gravy with spices', '0', 0, 'Vegetarian', 1, 22, '2021-02-05 09:49:39', '2021-02-05 09:49:39'),
(32, 'Shahi Malai Kofta', 6, NULL, 20, 'Mixed vegetables and cottage cheese dumplings simmered in a creamy almond sauce', 'SHAMIANA', 10, 'Non-Vegetarian', 1, 22, '2021-02-05 09:51:20', '2021-02-05 09:51:20'),
(33, 'Roti Basket', 4, NULL, 25, 'Assortment of rot is - Plain, Butter, Pudina and Lacha', '0', 0, 'Vegetarian', 1, 22, '2021-02-05 09:53:25', '2021-02-05 09:53:25'),
(34, 'Lachha Paratha', 4, NULL, 10, 'Multi layer whole wheat bread topped with hot ghee', '0', 0, 'Vegetarian', 1, 22, '2021-02-05 09:54:19', '2021-02-05 09:54:19'),
(35, 'Garlic Naan', 4, NULL, 15, 'Hand made bread from white flour garnished with garlic and basil', 'NAAN10', 10, 'Vegetarian', 1, 22, '2021-02-05 09:55:32', '2021-02-05 09:55:32'),
(36, 'Kulfi', 3, 0, 7, 'Milk condensed half and frosted like ice cream flavored with Pistachio or Cardamoms', '0', 0, 'Vegetarian', 1, 22, '2021-02-05 09:56:35', '2021-02-12 11:04:28'),
(37, 'Gulab Jamun', 3, 0, 150, 'Milk dumplings fried and simmered in sugar syrup', '0', 0, 'Vegetarian', 1, 22, '2021-02-05 09:57:28', '2021-02-15 09:08:07'),
(38, 'Ras Malai', 3, 0, 40, 'Creamed cheese patties served in reduced milk, garnished with pistachios. a classic dessert', 'RASMALAI', 20, 'Vegetarian', 2, 22, '2021-02-05 09:58:46', '2021-02-12 11:04:25'),
(39, 'Matka Chicken Special', 6, NULL, 50, 'Indian chicken curry typically starts with whole spices, heated in oil. A sauce is then made with onions, ginger, garlic, and tomatoes, and powdered spices.', '0', 0, 'Non-Vegetarian', 1, 30, '2021-02-10 11:16:07', '2021-02-10 11:16:07'),
(40, 'Butter Paneer Masala', 6, NULL, 30, 'A flavorful Creamy Dish prepared in Rich Tomato Gravy', '0', 0, 'Vegetarian', 1, 30, '2021-02-10 11:17:21', '2021-02-10 11:17:21'),
(41, 'Garlic Naan', 4, NULL, 15, 'Garlic naan made by flour and garlic.', '0', 0, 'Vegetarian', 1, 30, '2021-02-10 11:19:13', '2021-02-10 11:19:13'),
(42, 'Laccha Pratha', 4, NULL, 10, 'Served in 5 layers.', '0', 0, 'Vegetarian', 1, 30, '2021-02-10 11:20:05', '2021-02-10 11:20:05'),
(43, 'Veg Manchurian Dry', 18, NULL, 10, 'Delectable Veggie balls, deep fried and cooked in manchurian sauce', '0', 0, 'Vegetarian', 1, 30, '2021-02-10 11:22:31', '2021-02-10 11:22:31'),
(44, 'Veg Gole Kebab [8 Pieces] Veg Gole Kebab [8 Pieces]', 18, NULL, 15, 'Goli Kebabs is a Mughlai recipe made using mutton, gram flour, almonds and apricots', '0', 0, 'Non-Vegetarian', 1, 30, '2021-02-10 11:23:31', '2021-02-10 11:23:31'),
(45, 'Green Salad', 23, NULL, 5, 'We served all varities in green salad.', '0', 0, 'Vegetarian', 1, 30, '2021-02-10 11:25:18', '2021-02-10 11:25:18'),
(46, 'Fruit Raita', 23, NULL, 7, 'Fruit raita made by fresh fruits and curd.', '0', 0, 'Vegetarian', 1, 30, '2021-02-10 11:26:10', '2021-02-10 11:26:10'),
(47, 'Choco Chip Ice Cream', 3, NULL, 8, 'Fresh ice cream made by choclate.', '0', 0, 'Vegetarian', 1, 30, '2021-02-10 11:29:49', '2021-02-10 11:29:49'),
(48, 'Belgian Nutty Buddy Cone Ice Cream', 3, NULL, 10, 'Belgian Nutty Buddy Cone Ice Cream with choclate and come.', '0', 0, 'Vegetarian', 1, 30, '2021-02-10 11:30:47', '2021-02-10 11:30:47'),
(49, 'Chilly Paneer Gravy', 15, NULL, 15, 'Chinese food made by the fresh paneer.', '0', 0, 'Vegetarian', 1, 30, '2021-02-10 11:32:34', '2021-02-10 11:32:34'),
(50, 'Strawberry Ice Cream', 3, NULL, 20, 'Strawberry Ice Cream with a good flavour.', '0', 0, 'Vegetarian', 1, 31, '2021-02-10 11:39:49', '2021-02-10 11:39:49'),
(51, 'Tutti Frutti Ice Cream', 3, NULL, 15, 'Mix icecraems with different flavours.', '0', 0, 'Vegetarian', 1, 31, '2021-02-10 11:40:44', '2021-02-10 11:40:44'),
(52, 'Bread Basket', 4, NULL, 20, '1 Naan+2 Massi Roti+1 Laccha Pratha+1 Tandori Roti', '0', 0, 'Vegetarian', 1, 31, '2021-02-10 11:42:00', '2021-02-10 11:42:00'),
(53, 'Tandoori Roti', 4, NULL, 10, 'Tandoori roti prepared in tandoor.', '0', 0, 'Vegetarian', 1, 31, '2021-02-10 11:42:46', '2021-02-10 11:42:46'),
(54, 'Kadai Paneer', 6, NULL, 30, 'Paneer (Indian cottage cheese) and bell peppers cooked in a spicy masala', '0', 0, 'Vegetarian', 1, 31, '2021-02-10 11:44:18', '2021-02-10 11:44:18'),
(55, 'Butter Chicken', 6, NULL, 30, 'Indian Butter Chicken (Murgh Makhani) Authentic in house made sauce of butter, spices, tomato and ginger garlic mix with pieces of chicken', '0', 0, 'Non-Vegetarian', 1, 31, '2021-02-10 11:45:47', '2021-02-10 11:45:47'),
(56, 'Veg Chowmein', 18, NULL, 10, 'Veg chowmein made by the noodles and different Chinese sauces', '0', 0, 'Vegetarian', 1, 31, '2021-02-10 11:48:24', '2021-02-10 11:48:24'),
(57, 'Fish Fry [6 Pieces]', 18, NULL, 15, 'Fish fry made by fresh fish and served with green chutney.', '0', 0, 'Non-Vegetarian', 1, 31, '2021-02-10 11:49:43', '2021-02-10 11:49:43'),
(58, 'Masala Papad', 23, NULL, 10, 'Papad served with different vegetables and Lemon.', '0', 0, 'Vegetarian', 1, 31, '2021-02-10 11:51:22', '2021-02-10 11:51:22'),
(59, 'Boondi Raita', 23, NULL, 8, 'Boondi Raita made by balls and fresh curd.', '0', 0, 'Vegetarian', 1, 31, '2021-02-10 11:52:31', '2021-02-10 11:52:31'),
(60, 'Veg Spring Roll', 15, NULL, 10, 'Veg spring roll served with red spicy chutney', '0', 0, 'Vegetarian', 1, 31, '2021-02-10 11:54:33', '2021-02-10 11:54:33'),
(61, 'Chicken Spring Roll', 15, NULL, 12, 'Chicken Spring Roll made by fresh chicken.', '0', 0, 'Non-Vegetarian', 1, 31, '2021-02-10 11:55:41', '2021-02-10 11:55:41'),
(62, 'Cream and Cookies ice Cream', 3, NULL, 10, 'Cream and Cookies ice Cream', '0', 0, 'Vegetarian', 1, 32, '2021-02-10 12:05:48', '2021-02-10 12:05:48'),
(63, 'Bread Basket', 4, NULL, 20, 'Bread basket', '0', 0, 'Vegetarian', 1, 32, '2021-02-10 12:06:26', '2021-02-10 12:06:26'),
(64, 'Lal Maas', 6, NULL, 30, 'Lal Maas', '0', 0, 'Non-Vegetarian', 1, 32, '2021-02-10 12:07:32', '2021-02-10 12:07:32'),
(65, 'Honey Chilli Potato', 18, NULL, 15, 'Sweet, spicy & slightly sour crispy appetizer made with potatoes, bell peppers,Honey, garlic, chilli sauce & soya sauce', '0', 0, 'Vegetarian', 1, 32, '2021-02-10 12:09:00', '2021-02-10 12:09:00'),
(66, 'Veg Soup', 23, NULL, 15, 'Veg Soup', '0', 0, 'Vegetarian', 1, 32, '2021-02-10 12:10:06', '2021-02-10 12:10:06'),
(67, 'Chicken Tikka Roll', 15, NULL, 20, 'Chicken Tikka Roll', '0', 0, 'Non-Vegetarian', 1, 32, '2021-02-10 12:11:04', '2021-02-10 12:11:04'),
(68, 'milk', 23, NULL, 5, 'hot milk with flavord', 'hotmilk', 90, 'Vegetarian', 1, 22, '2021-02-12 06:17:05', '2021-02-12 06:17:05');

-- --------------------------------------------------------

--
-- Table structure for table `item_images`
--

CREATE TABLE `item_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_images`
--

INSERT INTO `item_images` (`id`, `item_id`, `image`) VALUES
(1, 1, 'storage/app/public/items/item_391561594969376.jpeg'),
(2, 1, 'storage/app/public/items/item_2213801594969376.jpeg'),
(3, 2, 'storage/app/public/items/item_2042051594970192.jpeg'),
(4, 2, 'storage/app/public/items/item_755141594970192.jpeg'),
(5, 3, 'storage/app/public/items/item_2042051594970415.jpeg'),
(6, 3, 'storage/app/public/items/item_755141594970415.jpeg'),
(7, 4, 'storage/app/public/items/item_107911594970512.jpeg'),
(8, 4, 'storage/app/public/items/item_421531594970512.jpeg'),
(9, 4, 'storage/app/public/items/item_755141594970512.jpeg'),
(10, 5, 'storage/app/public/items/item_107911594983165.jpeg'),
(11, 5, 'storage/app/public/items/item_421531594983165.jpeg'),
(12, 6, 'storage/app/public/items/item_199341594983894.jpeg'),
(13, 7, 'storage/app/public/items/item_2213801595235121.jpeg'),
(14, 7, 'storage/app/public/items/item_391561595235121.jpeg'),
(15, 8, 'storage/app/public/items/item_1887761595235195.jpeg'),
(16, 8, 'storage/app/public/items/item_2213801595235195.jpeg'),
(17, 9, 'storage/app/public/items/item_391561595235269.jpeg'),
(18, 10, 'storage/app/public/items/item_755141595235351.jpeg'),
(19, 11, 'storage/app/public/items/item_2042051595235451.jpeg'),
(20, 12, 'storage/app/public/items/item_2287521595235685.jpeg'),
(21, 13, 'storage/app/public/items/item_592621595235751.jpeg'),
(22, 14, 'storage/app/public/items/item_72701595235830.jpeg'),
(23, 15, 'storage/app/public/items/item_465481595235908.jpeg'),
(24, 16, 'storage/app/public/items/item_1305511595236013.jpeg'),
(25, 16, 'storage/app/public/items/item_623641595236013.jpeg'),
(26, 17, 'storage/app/public/items/item_5797941607431216.jpeg'),
(27, 18, 'storage/app/public/items/item_3127521607431495.jpeg'),
(28, 19, 'storage/app/public/items/item_7207221607665436.jpeg'),
(29, 20, 'storage/app/public/items/item_209991612515525.jpeg'),
(30, 21, 'storage/app/public/items/item_55991612515680.jpeg'),
(31, 22, 'storage/app/public/items/item_330111612515783.jpeg'),
(32, 23, 'storage/app/public/items/item_110931612516442.jpeg'),
(33, 24, 'storage/app/public/items/item_50751612517351.jpeg'),
(34, 25, 'storage/app/public/items/item_634481612517426.jpeg'),
(35, 26, 'storage/app/public/items/item_33191612517814.jpeg'),
(36, 27, 'storage/app/public/items/item_197891612517985.jpeg'),
(37, 28, 'storage/app/public/items/item_164271612518088.jpeg'),
(38, 29, 'storage/app/public/items/item_88671612518171.jpeg'),
(39, 30, 'storage/app/public/items/item_224521612518455.jpeg'),
(40, 31, 'storage/app/public/items/item_224521612518579.jpeg'),
(41, 32, 'storage/app/public/items/item_229511612518680.jpeg'),
(42, 33, 'storage/app/public/items/item_285501612518805.jpeg'),
(43, 34, 'storage/app/public/items/item_88331612518859.jpeg'),
(44, 35, 'storage/app/public/items/item_191371612518932.jpeg'),
(45, 36, 'storage/app/public/items/item_256511612518995.jpeg'),
(46, 37, 'storage/app/public/items/item_146661612519048.jpeg'),
(47, 38, 'storage/app/public/items/item_59141612519126.jpeg'),
(48, 39, 'storage/app/public/items/item_145071612955767.jpeg'),
(49, 40, 'storage/app/public/items/item_223641612955841.jpeg'),
(50, 41, 'storage/app/public/items/item_191371612955953.jpeg'),
(51, 42, 'storage/app/public/items/item_88331612956005.jpeg'),
(52, 43, 'storage/app/public/items/item_281691612956151.jpeg'),
(53, 44, 'storage/app/public/items/item_259491612956211.jpeg'),
(54, 45, 'storage/app/public/items/item_295871612956318.jpeg'),
(55, 46, 'storage/app/public/items/item_230381612956370.jpeg'),
(56, 47, 'storage/app/public/items/item_168071612956589.jpeg'),
(57, 48, 'storage/app/public/items/item_175961612956647.jpeg'),
(58, 49, 'storage/app/public/items/item_351021612956754.jpeg'),
(59, 50, 'storage/app/public/items/item_133071612957189.jpeg'),
(60, 51, 'storage/app/public/items/item_167351612957244.jpeg'),
(61, 52, 'storage/app/public/items/item_285501612957320.jpeg'),
(62, 53, 'storage/app/public/items/item_67351612957366.jpeg'),
(63, 54, 'storage/app/public/items/item_339391612957458.jpeg'),
(64, 55, 'storage/app/public/items/item_246791612957547.jpeg'),
(65, 56, 'storage/app/public/items/item_206851612957704.jpeg'),
(66, 57, 'storage/app/public/items/item_252811612957783.jpeg'),
(67, 58, 'storage/app/public/items/item_333571612957882.jpeg'),
(68, 59, 'storage/app/public/items/item_256311612957951.jpeg'),
(69, 60, 'storage/app/public/items/item_196851612958073.jpeg'),
(70, 61, 'storage/app/public/items/item_233061612958141.jpeg'),
(71, 62, 'storage/app/public/items/item_178451612958748.jpeg'),
(72, 63, 'storage/app/public/items/item_285501612958786.jpeg'),
(73, 64, 'storage/app/public/items/item_201551612958852.jpeg'),
(74, 65, 'storage/app/public/items/item_247701612958940.jpeg'),
(75, 66, 'storage/app/public/items/item_224621612959006.jpeg'),
(76, 67, 'storage/app/public/items/item_98671612959064.jpeg'),
(77, 68, 'storage/app/public/items/item_316271613110625.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `title`, `message`, `type`, `created_at`, `updated_at`) VALUES
(1, 134, 'Order Placed', 'Order placed successfully', 'order', '2021-02-05 11:57:01', '2021-02-05 11:57:01'),
(2, 22, 'Order Placed', 'Order received successfully', 'order', '2021-02-05 11:57:02', '2021-02-05 11:57:02'),
(3, 134, 'Order Placed', 'Order placed successfully', 'order', '2021-02-05 12:07:53', '2021-02-05 12:07:53'),
(4, 22, 'Order Placed', 'Order received successfully', 'order', '2021-02-05 12:07:54', '2021-02-05 12:07:54'),
(5, 134, 'Order Placed', 'Order placed successfully', 'order', '2021-02-05 12:56:25', '2021-02-05 12:56:25'),
(6, 22, 'Order Placed', 'Order received successfully', 'order', '2021-02-05 12:56:25', '2021-02-05 12:56:25'),
(7, 140, 'Group Created', 'You are added in group', 'group', '2021-02-10 05:37:36', '2021-02-10 05:37:36'),
(8, 140, 'Group Created', 'You are added in group', 'group', '2021-02-10 05:37:36', '2021-02-10 05:37:36'),
(9, 140, 'Order Placed', 'Order placed successfully', 'order', '2021-02-10 08:43:20', '2021-02-10 08:43:20'),
(10, 22, 'Order Placed', 'Order received successfully', 'order', '2021-02-10 08:43:21', '2021-02-10 08:43:21'),
(11, 140, 'Order Placed', 'Order placed successfully', 'order', '2021-02-10 08:45:11', '2021-02-10 08:45:11'),
(12, 22, 'Order Placed', 'Order received successfully', 'order', '2021-02-10 08:45:11', '2021-02-10 08:45:11'),
(13, 140, 'Group Created', 'You are added in group', 'group', '2021-02-11 11:42:56', '2021-02-11 11:42:56'),
(14, 148, 'Group Created', 'You are added in group', 'group', '2021-02-11 11:42:57', '2021-02-11 11:42:57'),
(15, 149, 'Group Created', 'You are added in group', 'group', '2021-02-11 11:42:57', '2021-02-11 11:42:57'),
(16, 150, 'Group Created', 'You are added in group', 'group', '2021-02-11 11:42:57', '2021-02-11 11:42:57'),
(17, 140, 'Poll Create', 'poll created you make order', 'poll', '2021-02-11 17:14:37', '2021-02-11 17:14:37'),
(18, 148, 'Poll Create', 'poll created you make order', 'poll', '2021-02-11 17:14:37', '2021-02-11 17:14:37'),
(19, 149, 'Poll Create', 'poll created you make order', 'poll', '2021-02-11 17:14:37', '2021-02-11 17:14:37'),
(20, 150, 'Poll Create', 'poll created you make order', 'poll', '2021-02-11 17:14:37', '2021-02-11 17:14:37'),
(21, 140, 'Order Placed', 'Order placed successfully', 'order', '2021-02-12 12:34:01', '2021-02-12 12:34:01'),
(22, 22, 'Order Placed', 'Order received successfully', 'order', '2021-02-12 12:34:01', '2021-02-12 12:34:01'),
(23, 140, 'Poll Closed', 'poll closed you make order', 'poll', '2021-02-13 11:25:29', '2021-02-13 11:25:29'),
(24, 140, 'Poll Closed', 'poll closed you make order', 'poll', '2021-02-13 11:25:29', '2021-02-13 11:25:29'),
(25, 148, 'Poll Closed', 'poll closed you make order', 'poll', '2021-02-13 11:25:29', '2021-02-13 11:25:29'),
(26, 148, 'Poll Closed', 'poll closed you make order', 'poll', '2021-02-13 11:25:29', '2021-02-13 11:25:29'),
(27, 149, 'Poll Closed', 'poll closed you make order', 'poll', '2021-02-13 11:25:29', '2021-02-13 11:25:29'),
(28, 149, 'Poll Closed', 'poll closed you make order', 'poll', '2021-02-13 11:25:29', '2021-02-13 11:25:29'),
(29, 150, 'Poll Closed', 'poll closed you make order', 'poll', '2021-02-13 11:25:29', '2021-02-13 11:25:29'),
(30, 150, 'Poll Closed', 'poll closed you make order', 'poll', '2021-02-13 11:25:29', '2021-02-13 11:25:29'),
(31, 155, 'Group Created', 'You are added in group', 'group', '2021-02-15 06:04:13', '2021-02-15 06:04:13'),
(32, 134, 'Group Created', 'You are added in group', 'group', '2021-02-15 06:04:13', '2021-02-15 06:04:13'),
(33, 140, 'Group Created', 'You are added in group', 'group', '2021-02-15 06:04:13', '2021-02-15 06:04:13'),
(34, 155, 'Group Created', 'You are added in group', 'group', '2021-02-15 09:34:48', '2021-02-15 09:34:48'),
(35, 134, 'Group Created', 'You are added in group', 'group', '2021-02-15 09:34:48', '2021-02-15 09:34:48'),
(36, 140, 'Group Created', 'You are added in group', 'group', '2021-02-15 09:34:49', '2021-02-15 09:34:49'),
(37, 155, 'Group Created', 'You are added in group', 'group', '2021-02-15 10:11:19', '2021-02-15 10:11:19'),
(38, 134, 'Group Created', 'You are added in group', 'group', '2021-02-15 10:11:20', '2021-02-15 10:11:20'),
(39, 140, 'Group Created', 'You are added in group', 'group', '2021-02-15 10:11:20', '2021-02-15 10:11:20'),
(40, 155, 'Group Created', 'You are added in group', 'group', '2021-02-15 10:13:42', '2021-02-15 10:13:42'),
(41, 140, 'Group Created', 'You are added in group', 'group', '2021-02-15 10:13:42', '2021-02-15 10:13:42'),
(42, 155, 'Order Placed', 'Order placed successfully', 'order', '2021-02-15 12:06:18', '2021-02-15 12:06:18'),
(43, 22, 'Order Placed', 'Order received successfully', 'order', '2021-02-15 12:06:18', '2021-02-15 12:06:18'),
(44, 138, 'Order Status Update', 'Your order is accepted', 'order', '2021-02-15 12:08:39', '2021-02-15 12:08:39');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `restaurant_id` int(10) UNSIGNED NOT NULL,
  `driver_id` int(10) UNSIGNED DEFAULT NULL,
  `special_instruction` varchar(255) DEFAULT NULL,
  `order_type` enum('Pickup','Delivery') NOT NULL DEFAULT 'Delivery',
  `discount` float NOT NULL,
  `tax` float NOT NULL,
  `total_amount` float NOT NULL,
  `grand_total` float NOT NULL,
  `payment_type` enum('wallet','card','cod') NOT NULL DEFAULT 'cod',
  `status` enum('Accept','Order In Kitchen','Ready To Pickup','Order Picked up','Delivered','Decline','Processing') NOT NULL DEFAULT 'Processing',
  `payment_status` enum('Pending','Success','Cancel','Refund') NOT NULL DEFAULT 'Pending',
  `order_group_type` enum('Individual','Group') NOT NULL,
  `user_group_id` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `restaurant_id`, `driver_id`, `special_instruction`, `order_type`, `discount`, `tax`, `total_amount`, `grand_total`, `payment_type`, `status`, `payment_status`, `order_group_type`, `user_group_id`, `created_at`, `updated_at`) VALUES
(1, 134, 22, NULL, 'undefined', 'Pickup', 0, 0, 57, 57, 'cod', 'Processing', 'Pending', 'Individual', 0, '2021-02-05 11:57:01', '2021-02-05 11:57:01'),
(2, 134, 22, NULL, 'undefined', 'Pickup', 0, 0, 325, 325, 'cod', 'Processing', 'Pending', 'Individual', 0, '2021-02-05 12:07:53', '2021-02-05 12:07:53'),
(3, 134, 22, NULL, 'undefined', 'Pickup', 0, 0, 191, 191, 'cod', 'Processing', 'Pending', 'Individual', 0, '2021-02-05 12:56:25', '2021-02-05 12:56:25'),
(4, 140, 22, NULL, 'undefined', 'Pickup', 0, 0, 59, 59, 'wallet', 'Processing', 'Success', 'Individual', 0, '2021-02-10 08:43:20', '2021-02-10 08:43:20'),
(5, 140, 22, NULL, 'undefined', 'Delivery', 0, 0, 15, 15, 'wallet', 'Processing', 'Success', 'Individual', 0, '2021-02-10 08:45:11', '2021-02-10 08:45:11'),
(6, 140, 22, NULL, 'undefined', 'Pickup', 0, 0, 28, 28, 'cod', 'Processing', 'Pending', 'Individual', 0, '2021-02-12 12:34:01', '2021-02-12 12:34:01'),
(7, 155, 22, NULL, 'undefined', 'Pickup', 0, 0, 20, 20, 'cod', 'Accept', 'Pending', 'Individual', 0, '2021-02-15 12:06:18', '2021-02-15 12:08:39');

-- --------------------------------------------------------

--
-- Table structure for table `order_deliveries`
--

CREATE TABLE `order_deliveries` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `status` enum('Accept','Decline','Processing','') NOT NULL DEFAULT 'Processing',
  `driver_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `qty` smallint(6) NOT NULL,
  `price` float NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `coupon_code_id` int(11) DEFAULT NULL,
  `sub_total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `item_id`, `qty`, `price`, `order_id`, `coupon_code_id`, `sub_total`) VALUES
(1, 36, 1, 7, 1, NULL, 7),
(2, 37, 1, 15, 1, NULL, 15),
(3, 33, 1, 25, 1, NULL, 25),
(4, 34, 1, 10, 1, NULL, 10),
(5, 36, 3, 7, 2, NULL, 21),
(6, 37, 2, 15, 2, NULL, 30),
(7, 38, 2, 40, 2, NULL, 80),
(8, 33, 2, 25, 2, NULL, 50),
(9, 34, 2, 10, 2, NULL, 20),
(10, 35, 2, 15, 2, NULL, 30),
(11, 27, 2, 17, 2, NULL, 34),
(12, 28, 3, 20, 2, NULL, 60),
(13, 36, 12, 7, 3, NULL, 84),
(14, 37, 1, 15, 3, NULL, 15),
(15, 27, 1, 17, 3, NULL, 17),
(16, 38, 1, 40, 3, NULL, 40),
(17, 33, 1, 25, 3, NULL, 25),
(18, 34, 1, 10, 3, NULL, 10),
(19, 37, 3, 15, 4, NULL, 45),
(20, 36, 2, 7, 4, NULL, 14),
(21, 37, 1, 15, 5, NULL, 15),
(22, 36, 4, 7, 6, NULL, 28),
(23, 29, 1, 20, 7, NULL, 20);

-- --------------------------------------------------------

--
-- Table structure for table `order_shipping_addresses`
--

CREATE TABLE `order_shipping_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `location` varchar(255) NOT NULL,
  `house_no` varchar(255) NOT NULL,
  `landmark` varchar(255) NOT NULL,
  `lat` float DEFAULT NULL,
  `lon` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `otps`
--

CREATE TABLE `otps` (
  `id` int(11) UNSIGNED NOT NULL,
  `mobile_number` varchar(25) NOT NULL,
  `otp` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `otps`
--

INSERT INTO `otps` (`id`, `mobile_number`, `otp`, `created_at`, `updated_at`) VALUES
(2, '189520358980', '893705', '2021-02-09 10:31:36', '2021-02-09 10:31:36'),
(3, '189520358981', '694708', '2021-02-09 10:34:27', '2021-02-09 10:34:27'),
(4, '189520358982', '145263', '2021-02-09 10:34:49', '2021-02-09 10:34:49'),
(6, '189520358983', '324856', '2021-02-09 10:58:12', '2021-02-09 10:58:12'),
(7, '1895203589', '025649', '2021-02-09 11:14:50', '2021-02-09 11:14:50'),
(8, '18952035891', '340892', '2021-02-09 11:22:06', '2021-02-09 11:22:06'),
(9, '919875632697', '963418', '2021-02-10 09:53:40', '2021-02-10 09:53:40'),
(11, '919649196950', '906457', '2021-02-10 10:42:29', '2021-02-10 10:42:29'),
(12, '919649196951', '850216', '2021-02-10 10:43:38', '2021-02-10 10:43:38'),
(13, '919649196952', '396107', '2021-02-10 10:46:11', '2021-02-10 10:46:11'),
(14, '919649196953', '948762', '2021-02-10 10:48:22', '2021-02-10 10:48:22'),
(15, '919875632698', '260384', '2021-02-10 11:35:14', '2021-02-10 11:35:14'),
(16, '919875632699', '167509', '2021-02-10 11:58:59', '2021-02-10 11:58:59'),
(17, '19638527410', '817239', '2021-02-12 10:55:06', '2021-02-12 10:55:06'),
(18, '1123123123', '984071', '2021-02-15 04:30:06', '2021-02-15 04:30:06'),
(19, '1456456456', '659732', '2021-02-15 04:49:45', '2021-02-15 04:49:45'),
(20, '14564564560', '817649', '2021-02-15 04:58:02', '2021-02-15 04:58:02'),
(21, '19595956565', '310452', '2021-02-15 11:17:20', '2021-02-15 11:17:20'),
(22, '18952035898', '342016', '2021-02-15 12:25:07', '2021-02-15 12:25:07');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `poll_restaurants`
--

CREATE TABLE `poll_restaurants` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poll_restaurants`
--

INSERT INTO `poll_restaurants` (`id`, `poll_id`, `restaurant_id`) VALUES
(1, 8, 22),
(2, 8, 32);

-- --------------------------------------------------------

--
-- Table structure for table `poll_votes`
--

CREATE TABLE `poll_votes` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `poll_votes`
--

INSERT INTO `poll_votes` (`id`, `poll_id`, `restaurant_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 8, 22, 140, '2021-02-11 11:45:01', '2021-02-11 11:45:01'),
(2, 8, 32, 148, '2021-02-11 11:46:51', '2021-02-11 11:46:51');

-- --------------------------------------------------------

--
-- Table structure for table `restaurants`
--

CREATE TABLE `restaurants` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `restaurant_name` varchar(100) DEFAULT NULL,
  `restaurant_picture` varchar(100) DEFAULT NULL,
  `opening_time` varchar(100) DEFAULT NULL,
  `closing_time` varchar(100) DEFAULT NULL,
  `land_mark` varchar(255) DEFAULT NULL,
  `restaurant_type` varchar(255) NOT NULL DEFAULT 'Vegetarian',
  `description` text DEFAULT NULL,
  `rating` float NOT NULL DEFAULT 0,
  `status` tinyint(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Also Known as Vendors';

--
-- Dumping data for table `restaurants`
--

INSERT INTO `restaurants` (`id`, `user_id`, `restaurant_name`, `restaurant_picture`, `opening_time`, `closing_time`, `land_mark`, `restaurant_type`, `description`, `rating`, `status`, `created_at`, `updated_at`) VALUES
(22, 138, 'Shamiana', 'public/img/default-vendor.jpg', '2021-02-05T11:00:01.718+05:30', '2021-02-05T23:00:22.952+05:30', NULL, 'Vegetarian,Non-Vegetarian', 'Hotel restaurant serving breakfast buffets, weekend brunches and a range of international meals.', 0, 1, '2021-02-05 06:59:53', '2021-02-05 08:53:36'),
(24, 141, 'stark restaurant', 'public/img/default-vendor.jpg', NULL, NULL, NULL, 'Vegetarian', NULL, 0, 1, '2021-02-09 10:31:38', '2021-02-09 10:31:38'),
(25, 142, 'taste restaurant', 'public/img/default-vendor.jpg', NULL, NULL, NULL, 'Vegetarian', NULL, 0, 1, '2021-02-09 10:34:29', '2021-02-09 10:34:29'),
(26, 143, 'taste restaurant', 'public/img/default-vendor.jpg', NULL, NULL, NULL, 'Vegetarian', NULL, 0, 1, '2021-02-09 10:35:20', '2021-02-09 10:35:20'),
(27, 144, 'grill store', 'public/img/default-vendor.jpg', NULL, NULL, NULL, 'Vegetarian', NULL, 0, 1, '2021-02-09 10:58:54', '2021-02-09 10:58:54'),
(28, 145, 'taste restaurant', 'public/img/default-vendor.jpg', NULL, NULL, NULL, 'Vegetarian', NULL, 0, 1, '2021-02-09 11:15:03', '2021-02-09 11:15:03'),
(29, 146, 'taste restaurant', 'public/img/default-vendor.jpg', NULL, NULL, NULL, 'Vegetarian', NULL, 0, 1, '2021-02-09 11:22:26', '2021-02-09 11:22:26'),
(30, 147, 'Monarch Restaurant', 'public/img/default-vendor.jpg', '2021-02-10T13:00:45.816+05:30', '2021-02-10T23:30:45.817+05:30', NULL, 'Vegetarian', 'Fine Dining - North Indian, Chinese, Continental, Beverages, Desserts', 0, 1, '2021-02-10 09:53:42', '2021-02-10 11:12:39'),
(31, 152, 'Suvarna Mahal', 'public/img/default-vendor.jpg', '2021-02-10T14:00:03.478+05:30', '2021-02-10T22:00:03.479+05:30', NULL, 'Vegetarian,Non-Vegetarian', 'Lavishly appointed hotel dining room offering the royal cuisines of India\'s northern regions', 0, 1, '2021-02-10 11:35:16', '2021-02-10 11:38:02'),
(32, 153, 'Okra', 'public/img/default-vendor.jpg', '2021-02-10T13:00:39.718+05:30', '2021-02-10T23:00:39.719+05:30', NULL, 'Vegetarian', 'Private Dining Room, Sophisticated, Worth the Price, Luxury, Brunch, Breakfast', 0, 1, '2021-02-10 11:59:01', '2021-02-10 12:04:40'),
(33, 156, 'NH NINE', 'public/img/default-vendor.jpg', '2021-02-15T08:22:14.908+05:30', '2021-02-15T00:22:14.961+05:30', NULL, 'Vegetarian', 'description', 0, 1, '2021-02-15 04:49:50', '2021-02-15 05:53:30'),
(34, 157, 'Quuens park', 'public/img/default-vendor.jpg', NULL, NULL, NULL, 'Vegetarian', NULL, 0, 1, '2021-02-15 04:58:06', '2021-02-15 04:58:06');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_continentals`
--

CREATE TABLE `restaurant_continentals` (
  `id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `continental_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `restaurant_continentals`
--

INSERT INTO `restaurant_continentals` (`id`, `restaurant_id`, `continental_id`) VALUES
(12, 30, 6),
(13, 30, 4),
(14, 30, 3),
(15, 30, 2),
(16, 31, 6),
(17, 31, 5),
(18, 31, 4),
(19, 31, 3),
(20, 31, 2),
(21, 32, 6),
(22, 32, 5),
(23, 32, 4),
(24, 32, 3),
(25, 32, 2),
(27, 33, 6),
(40, 22, 6),
(41, 22, 5),
(42, 22, 4),
(43, 22, 3);

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_favourites`
--

CREATE TABLE `restaurant_favourites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `restaurant_id` bigint(20) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 1,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `restaurant_favourites`
--

INSERT INTO `restaurant_favourites` (`id`, `user_id`, `restaurant_id`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(5, 134, 22, 1, 0, '2021-02-05 08:54:54', '2021-02-05 08:54:54'),
(6, 140, 22, 1, 0, '2021-02-12 12:31:54', '2021-02-12 12:31:54'),
(7, 155, 22, 1, 0, '2021-02-15 11:26:42', '2021-02-15 11:26:42');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_media`
--

CREATE TABLE `restaurant_media` (
  `id` int(20) NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `restaurant_id` int(20) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `restaurant_media`
--

INSERT INTO `restaurant_media` (`id`, `user_id`, `restaurant_id`, `image`, `created_at`, `updated_at`) VALUES
(6, 138, 22, 'storage/app/public/vendors/vendor_431791612515216.jpeg', '2021-02-05 08:53:36', '2021-02-05 08:53:36'),
(7, 138, 22, 'storage/app/public/vendors/vendor_368261612515216.jpeg', '2021-02-05 08:53:36', '2021-02-05 08:53:36'),
(8, 138, 22, 'storage/app/public/vendors/vendor_372551612515216.jpeg', '2021-02-05 08:53:36', '2021-02-05 08:53:36'),
(9, 147, 30, 'storage/app/public/vendors/vendor_470811612955559.jpeg', '2021-02-10 11:12:39', '2021-02-10 11:12:39'),
(10, 147, 30, 'storage/app/public/vendors/vendor_330451612955559.jpeg', '2021-02-10 11:12:39', '2021-02-10 11:12:39'),
(11, 147, 30, 'storage/app/public/vendors/vendor_529921612955559.jpeg', '2021-02-10 11:12:39', '2021-02-10 11:12:39'),
(12, 152, 31, 'storage/app/public/vendors/vendor_480241612957082.jpeg', '2021-02-10 11:38:02', '2021-02-10 11:38:02'),
(13, 152, 31, 'storage/app/public/vendors/vendor_340371612957082.jpeg', '2021-02-10 11:38:02', '2021-02-10 11:38:02'),
(14, 152, 31, 'storage/app/public/vendors/vendor_415511612957082.jpeg', '2021-02-10 11:38:02', '2021-02-10 11:38:02'),
(15, 152, 31, 'storage/app/public/vendors/vendor_314431612957082.jpeg', '2021-02-10 11:38:02', '2021-02-10 11:38:02'),
(16, 153, 32, 'storage/app/public/vendors/vendor_381311612958680.jpeg', '2021-02-10 12:04:40', '2021-02-10 12:04:40'),
(17, 153, 32, 'storage/app/public/vendors/vendor_363191612958680.jpeg', '2021-02-10 12:04:40', '2021-02-10 12:04:40'),
(18, 153, 32, 'storage/app/public/vendors/vendor_337841612958680.jpeg', '2021-02-10 12:04:40', '2021-02-10 12:04:40'),
(19, 153, 32, 'storage/app/public/vendors/vendor_355031612958680.jpeg', '2021-02-10 12:04:40', '2021-02-10 12:04:40'),
(21, 156, 33, 'storage/app/public/vendors/vendor_191531613368532.jpeg', '2021-02-15 05:55:32', '2021-02-15 05:55:32'),
(22, 156, 33, 'storage/app/public/vendors/vendor_1377271613368532.jpeg', '2021-02-15 05:55:32', '2021-02-15 05:55:32');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_ratings`
--

CREATE TABLE `restaurant_ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `restaurant_id` bigint(20) NOT NULL DEFAULT 0,
  `rating` int(11) NOT NULL DEFAULT 0,
  `review` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` smallint(6) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`) VALUES
(1, 'Admin'),
(2, 'User'),
(3, 'Vendor'),
(4, 'Driver');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `stripe_customer_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` tinyint(5) UNSIGNED DEFAULT NULL,
  `mobile_number` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_picture` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_type` enum('Google','Facebook','Twitter') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_status` tinyint(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) UNSIGNED ZEROFILL DEFAULT 0,
  `is_admin` tinyint(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 0,
  `role` enum('admin','user','vendor','driver') COLLATE utf8mb4_unicode_ci DEFAULT 'user' COMMENT '[''Admin'', ''User/Customer'', ''Vendor'', ''Driver'']',
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signup_role_id` int(11) DEFAULT NULL,
  `forgot_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notification` int(11) NOT NULL DEFAULT 1,
  `sign` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `stripe_customer_id`, `stripe_user_id`, `email`, `user_name`, `country_code`, `mobile_number`, `profile_picture`, `email_verified_at`, `password`, `address`, `city`, `state`, `country`, `zip`, `social_type`, `social_id`, `social_status`, `remember_token`, `status`, `is_admin`, `role`, `lat`, `lon`, `signup_role_id`, `forgot_token`, `notification`, `sign`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'admin@mailinator.com', 'Admin', NULL, NULL, NULL, NULL, '$2y$12$MvzpelK8BWfFcVRFjNFKSe369hnBLKwypw4dTR7qXMiBlCwXoVvc6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'JgNm6rBVD0xut2mkGddIRQO6wBeN5JYLwcryXxz12CGyuHiLzLOYePV4o1Qs', 0, 1, 'admin', NULL, NULL, 1, NULL, 1, '', NULL, NULL),
(134, NULL, NULL, 'robert@yopmail.com', 'Robert Disney', 91, '9649196949', NULL, NULL, '$2y$10$d8Et/7lSifoGxYJnLO66BeCNvW4zyzfpmQ03WTCwZ8qJqRObfpWzi', 'Asopa Hospital, Ajmer Road, Heera Nagar, Tagore Nagar, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302024', NULL, NULL, 0, NULL, 1, 0, 'user', '26.891434', '75.7414252', 2, '2c669734534262bda934d740e0132b6f1612505260', 1, NULL, '2021-02-05 06:07:40', '2021-02-05 06:07:40'),
(135, NULL, NULL, 'pattrick@yopmail.com', 'Pattrick Lennister', 91, '9649196936', NULL, NULL, '$2y$10$4EVimFp1owFTbYop5pD5nud2sFmuzjioTcZT3ShYOD/QvEQNk6.Fe', 'Nandpuri Colony, Ramnagar, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302007', NULL, NULL, 0, NULL, 1, 0, 'user', '26.8942566', '75.780377', 2, 'ade67e12d441527d0a020f69811a17381612505496', 1, NULL, '2021-02-05 06:11:36', '2021-02-05 06:11:36'),
(136, NULL, NULL, 'mark@yopmail.com', 'Mark rose', 91, '96491969632', NULL, NULL, '$2y$10$DPflzzbnFs6a8fZTIWZPIev6Y/VmZg3neBm.muokuEue6edb4e08S', 'Rambagh Palace, Jaipur, Bhawani Singh Road, Rambagh, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302005', NULL, NULL, 0, NULL, 1, 0, 'user', '26.8981053', '75.8081498', 2, '4d43bb66c84f7c61dd79af6a5d4eb33f1612505558', 1, NULL, '2021-02-05 06:12:38', '2021-02-05 06:12:38'),
(137, NULL, NULL, 'harry@yopmail.com', 'Harry Saze', 91, '9649491696', NULL, NULL, '$2y$10$bHYWeJ/EiRIvzZKQEr.Yt.326OYMxzVz92yB4NZj8ZYX1Qa0OroFO', 'RJ 14, Ajmer Road, Opposite Indus Ind Bank, Ajmera Garden, Shanti Nagar, DCM, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302019', NULL, NULL, 0, NULL, 1, 0, 'driver', '26.895303', '75.750557', 4, '21671d81c1f55e03527cb6b7b98c9a561612507532', 1, 'storage/app/public/users/signs/sign_1612507532.png', '2021-02-05 06:45:32', '2021-02-05 08:28:03'),
(138, 'cus_IwztkuZEzse1If', NULL, 'shamiana@yopmail.com', 'Shamiana', 91, '9875632696', NULL, NULL, '$2y$10$7kHK6AF.q2QA3x88T37dI.cxjsAA/f83ZkffcvHJyTvKh7yU0P3aC', 'Nirman Nagar, Brijlalpura, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302019', NULL, NULL, 0, NULL, 1, 0, 'vendor', '26.8911287', '75.7474829', 3, '66b9467c2804dd55577f999d6abfa5af1612508393', 1, 'storage/app/public/users/signs/sign_1612508393.png', '2021-02-05 06:59:53', '2021-02-15 12:09:31'),
(140, NULL, NULL, 'mac@mailinator.com', 'mac', 1, '8952035898', NULL, NULL, '$2y$10$tYEHk4uxruLPnfNIK5783eUG15YZIoXTH7dGQQkhGql9BSFEo5qSW', 'The NineHertz, Ajmer Road, Uma Colony, Nirman Nagar, DCM, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302019', NULL, NULL, 0, NULL, 1, 0, 'user', '26.8938114', '75.74720839999999', 2, '6c3305c45fc752001628d94c3496dbb51612862759', 1, NULL, '2021-02-09 09:25:59', '2021-02-15 12:25:28'),
(141, NULL, NULL, 'starkres@mailinator.com', 'stark restaurant', 1, '89520358980', NULL, NULL, '$2y$10$rPh3BeM5x8J/bN3boNjQ5uQAcZUbh5Dad36ODDR7g4rOxdjcximaG', 'Nirman Nagar, Brijlalpura, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302019', NULL, NULL, 0, NULL, 0, 0, 'vendor', '26.8911287', '75.7474829', 3, '22fea7a76176a70767473ab0d0cc10db1612866698', 1, 'storage/app/public/users/signs/sign_1612866698.png', '2021-02-09 10:31:38', '2021-02-10 09:45:46'),
(142, NULL, NULL, 'taste@mailinator.com', 'taste restaurant', 1, '89520358981', NULL, NULL, '$2y$10$dYhHG6E8FYSE5o7p5Yod8OFIS/GiseO/Deg3y/ePjeuxCABtOR4om', 'Nirman Nagar, Brijlalpura, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302019', NULL, NULL, 0, NULL, 0, 0, 'vendor', '26.8911287', '75.7474829', 3, 'ffeeb8b1cd89966a0e8156a37e5a10c21612866869', 1, 'storage/app/public/users/signs/sign_1612866869.png', '2021-02-09 10:34:29', '2021-02-10 09:45:50'),
(143, NULL, NULL, 'taste1@mailinator.com', 'taste restaurant', 1, '89520358982', NULL, NULL, '$2y$10$CJQBFMBt7nf60E65DS96meSceEZwQBCSBJRpcJyjBCy7c0/UHXSPa', 'Nirman Nagar, Brijlalpura, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302019', NULL, NULL, 0, NULL, 0, 0, 'vendor', '26.8911287', '75.7474829', 3, 'c25b7ae4d9593ea98af327e33ce990cd1612866920', 1, 'storage/app/public/users/signs/sign_1612866920.png', '2021-02-09 10:35:20', '2021-02-09 10:35:20'),
(144, NULL, NULL, 'grill@mailinator.com', 'grill store', 1, '89520358983', NULL, NULL, '$2y$10$16Pn7bCp7N28STHHcGElEuv0eL1Flk6Zx6MxPPJBe25wCfm8smKi6', 'Nirman Nagar, Brijlalpura, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302019', NULL, NULL, 0, NULL, 0, 0, 'vendor', '26.8911287', '75.7474829', 3, 'ee095fdea1345680a1cb4837bcc0868a1612868334', 1, 'storage/app/public/users/signs/sign_1612868334.png', '2021-02-09 10:58:54', '2021-02-10 09:45:54'),
(145, NULL, NULL, 'taste6@mailinator.com', 'taste restaurant', 1, '895203589', NULL, NULL, '$2y$10$Worv1I.L29TPh3U3w3wa7.XT2CtpV4NT91362J2A0P6btXWbSBoYS', 'Nirman Nagar, Brijlalpura, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302019', NULL, NULL, 0, NULL, 0, 0, 'vendor', '26.8911287', '75.7474829', 3, 'e1aa7cde71ccc6f9f1ee856a7d3ab7151612869303', 1, 'storage/app/public/users/signs/sign_1612869303.png', '2021-02-09 11:15:03', '2021-02-09 11:15:03'),
(146, NULL, NULL, 'taste61@mailinator.com', 'taste restaurant', 1, '8952035891', NULL, NULL, '$2y$10$3glRMmsE01TzcnFLiHNtHeu8PXKWBjWvVAiZ7.s6kOfqyn495PGEG', 'Nirman Nagar, Brijlalpura, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302019', NULL, NULL, 0, NULL, 0, 0, 'vendor', '26.8911287', '75.7474829', 3, '874c634f4b6f628aaa348402f367a6471612869746', 1, 'storage/app/public/users/signs/sign_1612869746.png', '2021-02-09 11:22:26', '2021-02-09 11:22:26'),
(147, NULL, NULL, 'monarch@yopmail.com', 'Monarch Restaurant', 91, '9875632697', NULL, NULL, '$2y$10$8HWxSEhoVYYWj58YxXg06.uvDvI7zHd0EFgYGYY3OtpniDIhFmzx2', 'Holiday Inn Jaipur City Centre, Sardar Patel Marg, Shivaji Nagar, Road, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302001', NULL, NULL, 0, NULL, 1, 0, 'vendor', '26.90294', '75.792486', 3, '7cefbed78ad6d8f531fa51ba592dad341612950822', 1, 'storage/app/public/users/signs/sign_1612950822.png', '2021-02-10 09:53:42', '2021-02-10 09:53:52'),
(148, NULL, NULL, 'harry1@yopmail.com', 'Harry', 91, '9649196950', 'storage/app/public/users/profile_pictures/profile_1612954567.jpeg', NULL, '$2y$10$jiE8UXUI0IkSuubYz90Jiepxe/99LhwibtNmv/t.VjKhgTmdpU2Ui', 'Lifestyle Stores, opposite Nehru Bhawani Singh Marg, Durgadas Colony, C Scheme, Ashok Nagar, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302006', NULL, NULL, 0, NULL, 1, 0, 'user', '26.9023187', '75.7938357', 2, 'c96f9c9f180f354acc7f6671ecb2d8ee1612953750', 1, NULL, '2021-02-10 10:42:30', '2021-02-10 10:56:07'),
(149, NULL, NULL, 'james@yopmail.com', 'James', 91, '9649196951', 'storage/app/public/users/profile_pictures/profile_1612954615.jpeg', NULL, '$2y$10$xf8hKmsydTbu3fYUIMvfK.qNSO46OiwkFAZ4eri2V2J5cP9UKspRe', 'Rambagh Palace, Jaipur, Bhawani Singh Road, Rambagh, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302005', NULL, NULL, 0, NULL, 1, 0, 'user', '26.8981053', '75.8081498', 2, '767a33c24d91cf08d605ba38ffc57f5b1612953820', 1, NULL, '2021-02-10 10:43:40', '2021-02-10 10:56:55'),
(150, NULL, NULL, 'lily@yopmail.com', 'Lily', 91, '9649196952', 'storage/app/public/users/profile_pictures/profile_1612954440.jpeg', NULL, '$2y$10$zPlZ33DmRBWdplXzGSmLyOQu6jMq9ab/8mr.H6HQpOTRu83TTIxEy', 'Hotel Bella Casa Jaipur, Tonk Road, Choti Chopad, Chandrakala Colony, Durgapura, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302018', NULL, NULL, 0, NULL, 1, 0, 'user', '26.8404241', '75.7947605', 2, 'db075aad956cb05f18b55cd82143849e1612953973', 1, NULL, '2021-02-10 10:46:13', '2021-02-10 10:54:00'),
(151, NULL, NULL, 'joy@yopmail.com', 'Joy', 91, '9649196953', 'storage/app/public/users/profile_pictures/profile_1612954336.jpeg', NULL, '$2y$10$jwbSs6w5bnb9jPGb5ysOyu5k9qODMRl7mAmYnPMW4oWsMkrGI8iVy', 'Jaipur Marriott Hotel, Jawahar Circle, Jai Jawan-2, Choti Chopad, Chandrakala Colony, Mata colony, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302015', NULL, NULL, 0, NULL, 1, 0, 'user', '26.8417597', '75.79618889999999', 2, 'c323d6e3ef1626bfb174624c1252abd61612954104', 1, NULL, '2021-02-10 10:48:24', '2021-02-10 10:52:16'),
(152, NULL, NULL, 'suvarna@yopmail.com', 'Suvarna Mahal', 91, '9875632698', NULL, NULL, '$2y$10$NNLT1rpeF.hBlrYfHkmoI.TDNcNtc2yXZx.7hTP/VCaFIXJzEoDtG', 'Rambagh Palace, Jaipur, Bhawani Singh Road, Rambagh, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302005', NULL, NULL, 0, NULL, 1, 0, 'vendor', '26.8981053', '75.8081498', 3, '3f94f558942d86c1e31aec58e6665d3f1612956916', 1, 'storage/app/public/users/signs/sign_1612956916.png', '2021-02-10 11:35:16', '2021-02-10 11:35:23'),
(153, NULL, NULL, 'okra@yopmail.com', 'Okra', 91, '9875632699', NULL, NULL, '$2y$10$WbUWvdJtsf7FC.Q.RO8U7uqNLN.bXXtaR6HNwya6PbD2gTk13K19W', 'Jaipur Marriott Hotel, Jawahar Circle, Jai Jawan-2, Choti Chopad, Chandrakala Colony, Mata colony, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302015', NULL, NULL, 0, NULL, 1, 0, 'vendor', '26.8417597', '75.79618889999999', 3, '2683f938c9f55e5a7d88e73b8bc460441612958341', 1, 'storage/app/public/users/signs/sign_1612958341.png', '2021-02-10 11:59:01', '2021-02-10 11:59:11'),
(154, NULL, NULL, 'jack@mailinator.com', 'jack', 1, '9638527410', NULL, NULL, '$2y$10$0NjGrzcfuPZKLZFK2bW.R.t1j/KhnmqSDU2W7ZreeV.jzE24gEiF6', 'Asopa Hospital, Ajmer Road, Heera Nagar, Tagore Nagar, Jaipur, Rajasthan, India', 'Jaipur', 'Rajasthan', NULL, '302024', NULL, NULL, 0, NULL, 1, 0, 'user', '26.891434', '75.7414252', 2, '6bd9c66c358fbaada0bb0a46522eac401613127313', 1, NULL, '2021-02-12 10:55:13', '2021-02-12 10:55:13'),
(155, NULL, NULL, 'joe@yopmail.com', 'Joe', 1, '123123123', NULL, NULL, '$2y$10$.3kApy2Df5RdiHBOvtSu/.CZvoM65y.hlfuZUgHeVv.6hLGHXxh3i', 'Ozone Park, Queens, NY, USA', 'Queens', 'NY', NULL, '41048', NULL, NULL, 0, NULL, 1, 0, 'user', '40.6794072', '-73.8507279', 2, '41396729469d5add7ba11682faa300721613363409', 1, NULL, '2021-02-15 04:30:09', '2021-02-15 12:30:05'),
(156, 'cus_IwurFJRImuM3t9', NULL, 'james123@yopmail.com', 'NH NINE', 1, '456456456', NULL, NULL, '$2y$10$vwk8SuIVSza9cVY9W75k6.au676moUEL0jUf9bulmikWwNBVUjqpm', 'Ozone Park, Queens, NY, USA', 'Queens', 'NY', NULL, '41048', NULL, NULL, 0, NULL, 1, 0, 'vendor', '40.6794072', '-73.8507279', 3, 'fd7fbdd9e2ff1fab9948a40f8f8bac741613364590', 1, 'storage/app/public/users/signs/sign_1613364590.png', '2021-02-15 04:49:50', '2021-02-15 06:57:39'),
(157, NULL, NULL, 'rest1@yopmail.com', 'Quuens park', 1, '4564564560', NULL, NULL, '$2y$10$ex0uKeOWtWZCir8I9upGzeAidD5xtk8aDBDoknE.sxlGJ3ktZPmyi', 'Queens, NY, USA', 'Queens', 'NY', NULL, '41048', NULL, NULL, 0, NULL, 1, 0, 'vendor', '40.7282239', '-73.7948516', 3, '387ae0e8f945dff88a5f645914a4019e1613365086', 1, 'storage/app/public/users/signs/sign_1613365086.png', '2021-02-15 04:58:06', '2021-02-15 05:02:07');

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `house_number` varchar(255) DEFAULT NULL,
  `landmark` varchar(255) DEFAULT NULL,
  `address_type` enum('Home','Office','Other') DEFAULT NULL,
  `lat` varchar(20) DEFAULT NULL,
  `lon` varchar(20) DEFAULT NULL,
  `is_default` tinyint(1) UNSIGNED ZEROFILL NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_addresses`
--

INSERT INTO `user_addresses` (`id`, `user_id`, `location`, `house_number`, `landmark`, `address_type`, `lat`, `lon`, `is_default`) VALUES
(27, 140, 'Ozone Park, Queens, NY, USA', 'hsi', 'zjsk', 'Home', '40.6794072', '-73.8507279', 0),
(28, 140, 'Jaipur, Rajasthan, India', '62b', 'vshsu', 'Home', '26.9124336', '75.7872709', 0),
(29, 140, 'Shamiana, Britannia Road West, Mississauga, ON, Canada', 'vzh', 'gssh', 'Home', '43.609035', '-79.6959249', 0),
(30, 140, 'Nirman Nagar, Brijlalpura, Jaipur, Rajasthan, India', 'haj', 'gsus', 'Other', '26.8911287', '75.7474829', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_banks`
--

CREATE TABLE `user_banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `branch` varchar(200) DEFAULT NULL,
  `account_no` varchar(100) DEFAULT NULL,
  `ifsc_code` varchar(15) DEFAULT NULL,
  `is_default` tinyint(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 0,
  `status` tinyint(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_contact_syncs`
--

CREATE TABLE `user_contact_syncs` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `contact_no` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_devices`
--

CREATE TABLE `user_devices` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `device_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_type` enum('ANDROID','IOS') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `log_status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_devices`
--

INSERT INTO `user_devices` (`id`, `user_id`, `device_id`, `device_type`, `log_status`, `created_at`, `updated_at`) VALUES
(118, 137, 'c2ADSqd8IBo:APA91bFrINa9RxgSlHA_vnk7aS2LwxTOrSLwprsllJ7Sn-dIrFMQ8UYYX0QDfBMMaGGHmEDd4NtbNsk1Zedg0kKURByLpstIvaMhmvVel8y2rmC7CYHseGhYQNc_amox53KgNeTkjOkN', 'ANDROID', 1, '2021-02-05 06:45:32', '2021-02-05 06:47:41'),
(127, 138, 'fBFksEcvht8:APA91bH22M8C8lOBJ6ofrVqsH169fx8SqOMqdmso3mVrS-hbaJsIIbxAT1J7dNMU8QeGu6gmzGHZ6huA6sBVx1zJ1Md9zkZB4Ca0ss2KabKqN3SVlYUZv44vYaOmVkggjKIp8FtEtR9c', 'ANDROID', 1, '2021-02-05 08:31:42', '2021-02-05 08:31:42'),
(131, 134, 'e_j4ojTJNNE:APA91bEaHa8aj1mY7rswX8hDYSH9HVLBut_J-sN8TK4kCJ1wNmA_fozuInx308nHQsWNzetAYyIie6C3rS5mWhDd0D2iTywxPWjRodyjeJ-FL0eWVO5jAEbdd6IJfWs3xE17fFadWZcL', 'ANDROID', 1, '2021-02-05 11:21:58', '2021-02-05 11:21:58'),
(132, 134, 'dg6C0-tcU14:APA91bHXXhVl5veW2ssFbnQcQcvALYYKDqlDNRvvHPjrSufRzcka4LF44Co3XhsoB_ZgWFY9EnFCXVMEV72fPEoDDR9ZR8XK75XTTvVZ-u5jBBa-321EGxUFUi9lJCaXpk88LKUFWExg', 'ANDROID', 1, '2021-02-09 06:25:43', '2021-02-09 06:25:43'),
(137, 140, 'e0IvZ1n4Xpw:APA91bGF00vc7mph8s1rQHJJ3kbLSpCzYj97lRRzwvZMFioUDzksjx_NGVdahfQwDebxs3tYBB-OQJYVGbkUP3R15JAYQ8kJix_IoJCJ9Pbkanl2U-lRes7-OH9Lbo2JeVKtpOeY5j6E', 'ANDROID', 1, '2021-02-10 06:43:51', '2021-02-10 06:43:51'),
(142, 147, 'cU8y68J6YBc:APA91bH09J_uCxH43TydJspVr4zdfsJzTj5sFGSgtFiRpBNVHU5G62v5SkPajxX_N8KJ-gETkPJ0BGAl6u1JACwiY1BAH48xdUAb8wHp5xNRsSQkBOcs5AhrJGJ-q-PVykLacPj0w3LJ', 'ANDROID', 1, '2021-02-10 10:35:11', '2021-02-10 10:35:11'),
(149, 149, 'euu1P2M9uUo:APA91bFU9YVXlHUbshW2OH--7N8MowkI3AAT7VfVRPf8kkHt54MzbnnVMz5wBKO80xmeP4QDNPM8ZSo79ybLBOMiuSA5OqAlqWkeiOhl5gU0mNHIIKinhW3bVWjOZjZTZBfuJ2XSVZhg', 'ANDROID', 1, '2021-02-10 10:56:39', '2021-02-10 10:56:39'),
(152, 153, 'ekhSrluF4aM:APA91bEnm7du6HxbAHkgVbcj812hfPtCBjSwEY9Ol9nOOv6LQ-piQtbc2HPVPsIx6MAfJ2eGwN7Cv6dedcOJKa8-ARaY1hbTbM2s2KJQT8IGc7BQoS0ZBAA0ZrL-XlgoBniCmtr5mdob', 'ANDROID', 1, '2021-02-10 11:59:02', '2021-02-10 12:01:42'),
(157, 138, 'NOTFOUND', 'IOS', 1, '2021-02-12 08:43:11', '2021-02-12 08:43:11'),
(177, 155, 'NOTFOUND', 'ANDROID', 1, '2021-02-15 12:42:39', '2021-02-15 12:42:39');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `creator_id` int(11) NOT NULL COMMENT 'user_id who is creator',
  `status` varchar(60) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `group_name`, `creator_id`, `status`, `created_at`, `updated_at`) VALUES
(8, 'Test', 140, 'GROUP_CREATED', '2021-02-10 05:37:36', '2021-02-10 05:37:36'),
(9, 'Enjoy', 140, 'POLL_CLOSED', '2021-02-11 11:42:56', '2021-02-13 11:25:29'),
(12, 'Jaipur friends', 155, 'GROUP_CREATED', '2021-02-15 10:11:19', '2021-02-15 10:11:19'),
(13, 'Groups', 155, 'GROUP_CREATED', '2021-02-15 10:13:42', '2021-02-15 10:13:42');

-- --------------------------------------------------------

--
-- Table structure for table `user_group_polls`
--

CREATE TABLE `user_group_polls` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `poll_max_time` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group_polls`
--

INSERT INTO `user_group_polls` (`id`, `group_id`, `creator_id`, `poll_max_time`, `created_at`, `updated_at`) VALUES
(8, 9, 140, '2021-02-12 17:14:00', '2021-02-11 17:14:37', '2021-02-11 17:14:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `user_id`, `role_id`) VALUES
(56, 134, 2),
(57, 135, 2),
(58, 136, 2),
(59, 137, 4),
(60, 138, 3),
(62, 140, 2),
(63, 141, 3),
(64, 142, 3),
(65, 143, 3),
(66, 144, 3),
(67, 145, 3),
(68, 146, 3),
(69, 147, 3),
(70, 148, 2),
(71, 149, 2),
(72, 150, 2),
(73, 151, 2),
(74, 152, 3),
(75, 153, 3),
(76, 154, 2),
(77, 155, 2),
(78, 156, 3),
(79, 157, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user_searches`
--

CREATE TABLE `user_searches` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `search_text` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

CREATE TABLE `wallets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `amount` double(8,2) NOT NULL DEFAULT 0.00,
  `status` int(11) NOT NULL DEFAULT 1,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wallets`
--

INSERT INTO `wallets` (`id`, `user_id`, `amount`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(12, 140, 4898.00, 1, 0, '2021-02-10 00:00:00', '2021-02-12 12:34:01'),
(13, 155, -20.00, 1, 0, '2021-02-15 12:06:18', '2021-02-15 12:06:18');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_percentages`
--

CREATE TABLE `wallet_percentages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT 0,
  `percentage` double(8,2) NOT NULL DEFAULT 0.00,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wallet_percentages`
--

INSERT INTO `wallet_percentages` (`id`, `role_id`, `percentage`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 5.00, 1, '2020-11-24 05:26:00', '2020-11-24 05:26:00'),
(2, 4, 30.00, 1, '2020-11-24 05:29:05', '2020-11-24 05:29:05');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_transactions`
--

CREATE TABLE `wallet_transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `wallet_id` bigint(20) NOT NULL DEFAULT 0,
  `stripe_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paytype` double(8,2) NOT NULL DEFAULT 0.00,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'add',
  `status` int(11) NOT NULL DEFAULT 1,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wallet_transactions`
--

INSERT INTO `wallet_transactions` (`id`, `user_id`, `wallet_id`, `stripe_token`, `paytype`, `comment`, `transaction_type`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(139, 140, 1, 'na', 0.00, '5000 added to wallet', 'add', 1, 0, NULL, NULL),
(140, 140, 12, '', -59.00, '59 debited from your wallet by order id : 4', 'debit', 1, 0, '2021-02-10 08:43:20', '2021-02-10 08:43:20'),
(141, 140, 12, '', -15.00, '15 debited from your wallet by order id : 5', 'debit', 1, 0, '2021-02-10 08:45:11', '2021-02-10 08:45:11'),
(142, 140, 12, '', -28.00, '28 debited from your wallet by order id : 6', 'debit', 1, 0, '2021-02-12 12:34:01', '2021-02-12 12:34:01'),
(143, 155, 13, '', -20.00, '20 debited from your wallet by order id : 7', 'debit', 1, 0, '2021-02-15 12:06:18', '2021-02-15 12:06:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_items`
--
ALTER TABLE `cart_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms`
--
ALTER TABLE `cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `continentals`
--
ALTER TABLE `continentals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_details`
--
ALTER TABLE `driver_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `driver_partners`
--
ALTER TABLE `driver_partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_ratings`
--
ALTER TABLE `driver_ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_members`
--
ALTER TABLE `group_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_images`
--
ALTER TABLE `item_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_deliveries`
--
ALTER TABLE `order_deliveries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_shipping_addresses`
--
ALTER TABLE `order_shipping_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otps`
--
ALTER TABLE `otps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `poll_restaurants`
--
ALTER TABLE `poll_restaurants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poll_votes`
--
ALTER TABLE `poll_votes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restaurants`
--
ALTER TABLE `restaurants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `restaurant_continentals`
--
ALTER TABLE `restaurant_continentals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restaurant_favourites`
--
ALTER TABLE `restaurant_favourites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restaurant_media`
--
ALTER TABLE `restaurant_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `restaurant_id` (`restaurant_id`);

--
-- Indexes for table `restaurant_ratings`
--
ALTER TABLE `restaurant_ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_banks`
--
ALTER TABLE `user_banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_contact_syncs`
--
ALTER TABLE `user_contact_syncs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_devices`
--
ALTER TABLE `user_devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group_polls`
--
ALTER TABLE `user_group_polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_searches`
--
ALTER TABLE `user_searches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallets`
--
ALTER TABLE `wallets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet_percentages`
--
ALTER TABLE `wallet_percentages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet_transactions`
--
ALTER TABLE `wallet_transactions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `cart_items`
--
ALTER TABLE `cart_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `cms`
--
ALTER TABLE `cms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `continentals`
--
ALTER TABLE `continentals`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `driver_details`
--
ALTER TABLE `driver_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `driver_partners`
--
ALTER TABLE `driver_partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `driver_ratings`
--
ALTER TABLE `driver_ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `group_members`
--
ALTER TABLE `group_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `item_images`
--
ALTER TABLE `item_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `order_deliveries`
--
ALTER TABLE `order_deliveries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `order_shipping_addresses`
--
ALTER TABLE `order_shipping_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `otps`
--
ALTER TABLE `otps`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `poll_restaurants`
--
ALTER TABLE `poll_restaurants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `poll_votes`
--
ALTER TABLE `poll_votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `restaurants`
--
ALTER TABLE `restaurants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `restaurant_continentals`
--
ALTER TABLE `restaurant_continentals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `restaurant_favourites`
--
ALTER TABLE `restaurant_favourites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `restaurant_media`
--
ALTER TABLE `restaurant_media`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `restaurant_ratings`
--
ALTER TABLE `restaurant_ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;

--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `user_banks`
--
ALTER TABLE `user_banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_contact_syncs`
--
ALTER TABLE `user_contact_syncs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_devices`
--
ALTER TABLE `user_devices`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;

--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user_group_polls`
--
ALTER TABLE `user_group_polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `user_searches`
--
ALTER TABLE `user_searches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wallets`
--
ALTER TABLE `wallets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `wallet_percentages`
--
ALTER TABLE `wallet_percentages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wallet_transactions`
--
ALTER TABLE `wallet_transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `driver_details`
--
ALTER TABLE `driver_details`
  ADD CONSTRAINT `delete_driver_details_on_delete_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `restaurants`
--
ALTER TABLE `restaurants`
  ADD CONSTRAINT `restaurants_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `restaurant_media`
--
ALTER TABLE `restaurant_media`
  ADD CONSTRAINT `restaurant_media_ibfk_1` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `restaurant_media_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_devices`
--
ALTER TABLE `user_devices`
  ADD CONSTRAINT `user_devices_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
