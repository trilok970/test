<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Models\Cms;
use App\Models\UserDevice;
use Session;
use App\Http\Controllers\Controller;
use Mail;
use App\Mail\UserMail;
class UsersController extends Controller {


    public function accountConfirmation(Request $request) {
        $data = $request->all();

        $users = User::where('forgot_token',$data['token'])->first();
        if ($users) {
            $users->status = 1;
            $users->forgot_token = null;
            $users->save();

            $msg = ['message_success' => 'Account activated. You can login now.'];

            return view('front.users.register_status',compact('msg'));
        } else {
            $msg = ['message_error' => 'Link Expired.'];
            return view('front.users.register_status',compact('msg'));
        }

        return view('front.users.register_status');
    }

    public function emailverify(Request $request) {
        $data = $request->all();

        $place = Place::where('token',$data['token'])->first();
        if ($place) {
            $place->email_verify = 1;
            $place->token = null;
            $place->save();

            $msg = ['message_success' => 'Email verified.'];

            return view('front.users.place_register_email_verify',compact('msg'));
        } else {
            $msg = ['message_error' => 'Link Expired.'];
            return view('front.users.register_status',compact('msg'));
        }

        return view('front.users.register_status');
    }


    public function resetPassword(Request $request) {
        $data = $request->all();
        $users = User::where('forgot_token',$data['token'])->first();
        if (empty($users)) {
            return view('front/users/linkexpire');
        }
        return view('front/users/reset_password');
    }



    public function updateResetPassword(Request $request) {

        $validator = Validator::make($request->all(),[
            'new_password' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            // If validation fails. if want json response copy it from other functions.
            $response['status'] = false;
            $response['message'] = $this->validationHandle($validator->messages());
            $response['data'] = [];

            $msg = ['status' => false, 'message_error' => $response['message']];
            return view('front.users.reset_password',compact('msg'));
        } else {
            $data = $request->all();
            // $user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

            $users = User::where('forgot_token',$data['token'])->first();
            // $users = 1;
            if ($users) {
                // check if new password is same as the Previous password.
                if (Hash::check($data['new_password'], $users->password)) {
                    $msg = ['message_error' => 'New Password is already in use before. Please enter a fresh password.'];
                    return view('front.users.reset_password',compact('msg'));
                }

                $users->password = Hash::make($request->input('new_password'));
                $users->forgot_token = null;
                $users->save();

                $msg = ['status' => true, 'message_success' => 'Password updated successfully.'];
                return view('front.users.reset_password',compact('msg'));

            } else {
                $msg = ['status' => true, 'message_error' => 'Link Expired.'];
                return view('front.users.reset_password',compact('msg'));

            }

        }

        ###############

    }


    public function forgotPassword(Request $request) {
        // echo $request->token; die();
        $token =  $request->token;

        $base_url =  url('/');
        $data = $request->all();
        $title_page = 'Login';
        if ($request->isMethod('post')) {

            try {

                $validation = array(
                    'password' => 'required|confirmed|min:6',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } else{

                    $users = User::where('forgot_token', $token)->first();

                    if ($users) {

                        $password = Hash::make($request->input('password'));    // creating password hash / encryption.
                        $users->password = $password;
                        $users->forgot_token = null;
                        $users->save();

                        Session::flash('success', 'Password updated successfully.');
                        return redirect()->back()->withInput();
                    } else {
                        Session::flash('danger', 'Link Expired.');
                        return redirect()->back()->withInput();
                    }
                }


            } catch (\Exception $e) {
                $msg = $e->getMessage();
                Session::flash('danger', $msg);
                return redirect()->back()->withInput();
            }
        }

        return view('front/users/forgot_password');
        // return view('front/users/forgot_password',compact('users'));
    }

    public function forgotPasswordPlaces(Request $request) {
        // echo $request->token; die();
        $token =  $request->token;

        $base_url =  url('/');
        $data = $request->all();
        $title_page = 'Login';
        if ($request->isMethod('post')) {
            // print_r($data); die();
            try {

                $validation = array(
                    'password' => 'required|confirmed|min:6',
                );

                $validator = Validator::make($request->all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
                } else{

                    $place = Place::where('token', $token)->first();

                    if ($place) {

                        $password = Hash::make($request->input('password'));    // creating password hash / encryption.
                        $place->password = $password;
                        $place->token = null;
                        $place->save();

                        Session::flash('success', 'Password updated successfully.');
                        return redirect()->back()->withInput();
                    } else {
                        Session::flash('danger', 'Link Expired.');
                        return redirect()->back()->withInput();
                    }
                }


            } catch (\Exception $e) {
                $msg = $e->getMessage();
                Session::flash('danger', $msg);
                return redirect()->back()->withInput();
            }
        }

        return view('front/users/forgot_password');
        // return view('front/users/forgot_password',compact('users'));
    }

    public function check_mail($email)
    {


       Mail::to($email)->send(new UserMail());
        if (Mail::failures())
        {
            echo  "response showing failed emails";
        }

           echo "Email sent successfully";


    }
    public function privacy_policy()
    {

        $page_title = 'Privacy Policy';
        $cms = Cms::where('slug','privacy-policy')->first();
        return view('privacy-policy',compact('page_title','cms'));
    }
    public function terms_and_condition()
    {

        $page_title = 'Terms And Conditions';
        $cms = Cms::where('slug','terms-and-condition')->first();
        return view('terms-and-condition',compact('page_title','cms'));
    }





}
