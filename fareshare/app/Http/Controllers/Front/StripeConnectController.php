<?php
namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Session;
// use Stripe;
use App\User;

use App\lib\Stripe;
class StripeConnectController extends Controller
{

    public function createConnect(Request $request){
        // http://testing.demo2server.com/fareshare/create-connect?state=foo_state&merchantId=khytg4g4dqfk56yy&code=d7ddb235fdb492459be218976691b527
// echo "check";exit;
        try {


            $code = $request->get('code');
            $state = $request->get('state');
            if($code || $state){
                $post = [
                    'client_secret' => env('STRIPE_SECRET'),
                    'code' => $code,
                    'grant_type'   => 'authorization_code',
                ];

                $ch = curl_init('https://connect.stripe.com/oauth/token');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                // execute!
                $response = curl_exec($ch);
                // echo "<pre>";
                // print_r($response);
                // exit;
                // close the connection, release resources used
                curl_close($ch);
                // do anything you want with your response
                $res = json_decode($response,true);

                if($res){

                    $user = User::where('stripe_customer_id',$state)->first();

                    if($user){

                        $user->stripe_user_id = $res['stripe_user_id'];
                        $user->save();
                        //code for stripe
                        // \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                        $stripe = new \Stripe\StripeClient(
                          env('STRIPE_SECRET')
                        );
                        $stripe->accounts->updateCapability(
                          $res['stripe_user_id'],
                          'transfers',
                          ['requested' => true]
                        );


                       return redirect('create-connect-success');
                    }else{
                        return abort(500);
                    }
                }else{
                    return abort(500);
                }
            }else{
                return abort(500);
            }

            // For Braintree
            // $code = $request->get('code');
            // $state = $request->get('state');
            // if($code || $state){

            //     $client_id = env('BTREE_CLIENT_ID');
            //     $client_secret = env('BTREE_CLIENT_SECRET');
            //     $gateway = new \Braintree\Gateway([
            //         'clientId' => $client_id,
            //         'clientSecret' => $client_secret
            //     ]);
            //     $result = $gateway->oauth()->createTokenFromCode([
            //         'code' => $code
            //     ]);


            //     $accessToken = $result->credentials->accessToken;
            //     $expiresAt = $result->credentials->expiresAt;
            //     $refreshToken = $result->credentials->refreshToken;

            //     // echo "<pre>";
            //     // print_r($result);
            //     // exit;

            //     if($result){

            //         $user = User::where('braintree_customer_id',$state)->first();

            //         if($user){

            //             $user->accessToken = $accessToken;
            //             $user->refreshToken = $refreshToken;
            //             $user->expiresAt = $expiresAt;
            //             $user->save();





            //            return redirect('create-connect-success');
            //         }else{
            //             return abort(500);
            //         }
            //     }else{
            //         return abort(500);
            //     }
            // }else{
            //     return abort(500);
            // }
        } catch (\Exception $e) {
            return abort(500);
        }
    }
     public function createConnect1(Request $request){
        // http://testing.demo2server.com/fareshare/create-connect?state=foo_state&merchantId=khytg4g4dqfk56yy&code=d7ddb235fdb492459be218976691b527

        try {

            $code = $request->get('code');
            $state = $request->get('state');
            if($code || $state){
                $post = [
                    'client_secret' => env('STRIPE_SECRET'),
                    'code' => $code,
                    'grant_type'   => 'authorization_code',
                ];

                $ch = curl_init('https://connect.stripe.com/oauth/token');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                // execute!
                $response = curl_exec($ch);

                // close the connection, release resources used
                curl_close($ch);
                // do anything you want with your response
                $res = json_decode($response,true);

                if($res){

                    $user = User::where('braintree_customer_id',$state)->first();

                    if($user){

                        $user->stripe_user_id = $res['stripe_user_id'];
                        $user->save();
                        //code for stripe
                        // \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                        $stripe = new \Stripe\StripeClient(
                          env('STRIPE_SECRET')
                        );
                        $stripe->accounts->updateCapability(
                          $res['stripe_user_id'],
                          'transfers',
                          ['requested' => true]
                        );


                       return redirect('create-connect-success');
                    }else{
                        return abort(500);
                    }
                }else{
                    return abort(500);
                }
            }else{
                return abort(500);
            }
        } catch (\Exception $e) {
            return abort(500);
        }
    }

    public function createConnectSuccess(){

      echo  $message = "You are all set with your stripe account, please wait while we redirect you to the app. <br> Redirecting...";
        // return view('front.stripe.stripe-connect',compact('message'));
    }

}
