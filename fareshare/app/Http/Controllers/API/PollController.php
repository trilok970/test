<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserGroupPoll;
use Exception;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Illuminate\Support\Facades\Validator;
// use App\Models\UserGroupPoll;
use App\Models\PollVote;
use App\Models\GroupMember;
use App\Models\User;
use App\Models\UserGroup;
use App\Models\Restaurant;
use App\Http\Controllers\Admin\UserFcmTokenController;


class PollController extends Controller
{
    public $fcm;

    public function __construct()
    {
        $this->api_per_page = config('constants.api_per_page');
        $this->fcm = new UserFcmTokenController;
    }

    public function create(Request $request)
    {

        try {
            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
            $validator = Validator::make($request->all(), [
                'group_id' => 'required',
                'restaurants_id' => 'required',
                'max_time' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }
            date_default_timezone_set("Asia/Kolkata");
            $max_time = explode(':',$request->max_time);
            $current_time = date('Y-m-d H:i');

            $poll_expiry_time = date('Y-m-d H:i',strtotime('+'.$max_time[0].' hour +'.$max_time[1].' minutes',strtotime($current_time)));

            $userGroup = UserGroup::where('id',$request->group_id)->first();

            DB::beginTransaction();
            $userGroupPoll = UserGroupPoll::create([
                'group_id' => $request->group_id,
                'creator_id' => $userId,
                'poll_max_time' => $poll_expiry_time,
            ]);

            $req_restaurants = $request->restaurants_id;
            $restaurants = [];

            foreach ($req_restaurants as $restaurant) {
                array_push($restaurants,['restaurant_id' => $restaurant]);
            }

            $userGroupPoll->restaurants()->createMany($restaurants);

            $userGroup = UserGroup::where('id',$request->group_id)->first();
            $userGroup->status = 'POLL_CREATED';
            $userGroup->save();
            DB::commit();

            $restaurants = $userGroupPoll->restaurants()->with('restaurantDetail')->get();


            $groupMembers = GroupMember::where([
                'group_id' => $userGroupPoll->group_id,
            ])->with('user')->get();


            $userGroupPoll->restaurants = $restaurants;
            $userGroupPoll->members = $groupMembers;

            $response['status'] = true;
            $response['data'] = $userGroupPoll;
            $response['message'] = "Poll Created Successfully";

            $message = config('constants.poll_create');

            $members = GroupMember::where('group_id',$request->group_id)->get();
            if($members)
            foreach ($members as $key => $value) {
            $this->fcm->sendNotification($value->user_id,"Poll Create",$message,"poll",$request->group_id);

            }

        } catch(Exception $e) {
            $response['status'] = false;
            $response['message'] = "Error!!! Something went wrong ".$e->getMessage();
        }

        return response()->json($response);
    }
    public function delete(Request $request)
    {
        try {

            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
            $validator = Validator::make($request->all(), [
                'poll_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }

            $pollId = $request->poll_id;

            DB::transaction(function () use ($pollId) {
                $userGroupPoll = UserGroupPoll::where('id',$pollId)->first();
                $userGroupPoll->restaurants()->delete();
                $userGroupPoll->delete();
            });

            $response['status'] = true;
            $response['message'] = "Poll Deleted Successfully.";
        }catch(Exception $e) {
            $response['status'] = false;
            $response['message'] = "Error!!! Something went wrong ".$e->getMessage();
        }

        return response()->json($response);
    }

    public function voteRestaurant(Request $request)
    {
        try {
            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
            $validator = Validator::make($request->all(), [
                'group_id' => 'required',
                'poll_id' => 'required',
                'restaurant_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }

            $pollVote = PollVote::where(['poll_id' => $request->poll_id,'restaurant_id' => $request->restaurant_id,'user_id' => $userId])->first();
            if(!empty($pollVote)) {
                return response()->json(['status'=>false,'message' => 'Your vote has been already submitted.']);
            }
            $user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
            $userGroupPoll = UserGroupPoll::where('group_id',$request->group_id)->where('id',$request->poll_id)->first();
            if($userGroupPoll) {
                $current_time = date('Y-m-d H:i');
                $result_arr = array();
                if(strtotime($current_time)<= strtotime($userGroupPoll->poll_max_time)){
                    $row = new PollVote();
                    $row->user_id = $user_id;
                    $row->poll_id = $userGroupPoll->id;
                    $row->restaurant_id = $request->restaurant_id;
                    if($row->save()){
                        $group_info = UserGroup::where('id',$request->group_id)->with('members','members.user')->first();
                        $result_arr = [
                            "restaurant_info" => $row->voted_restaurant_info,
                            "group_info" => $group_info,
                            // "members" => $row->poll_info->members,7
                        ];

                        return response()->json(['status' => true, 'message' => "Successfully Voted Restaurant.","data"=>$result_arr]);
                    }
                }else{
                    if($userGroupPoll->creator_id == $userId) {
                        $userGroupPoll->restaurants()->delete();
                        $userGroup = UserGroup::where('id',$request->group_id)->first();
                        $userGroup->status = 'GROUP_CREATED';
                        $userGroup->save();
                        UserGroupPoll::where('group_id',$request->group_id)->delete();
                        return response()->json(['status' => false, 'message' => "This poll has been deleted.Please create a new one."]);
                    }
                    return response()->json(['status' => false, 'message' => "You have reached out of time."]);
                }

            }
            else {
                return response()->json(['status' => false, 'message' => "Group poll not exits."]);
            }

        } catch(Exception $e) {
            $response['status'] = false;
            $response['message'] = "Error!!! Something went wrong ".$e->getLine()."-".$e->getMessage();
        }

        return response()->json($response);
    }

    public function pollDetail(Request $request)
    {
        try {
            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
            $validator = Validator::make($request->all(), [
                'group_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }else{
                $user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
                $row = UserGroupPoll::where('group_id',$request->group_id)->first();

                $result_arr = array();
                $rest_info = array();

                foreach($row->restaurants as $res){
                    $votes = PollVote::where('poll_id',$res->poll_id)->where('restaurant_id',$res->restaurant_id)->select('id','user_id')->with('voted_user_info')->get();
                    $rest_info[] = [
                        "restaurant_detail" => $res->restaurantDetail,
                        "total_votes" => count($votes),
                        "user_detail" =>$votes,
                    ];
                }


                $result_arr = [
                    "id"=> $row->id,
                    "group_id"=> $row->group_id,
                    "creator_id"=> $row->creator_id,
                    "restaurants"=> $rest_info,
                ];
                return response()->json(['status' => true, 'message' => "Poll Detail", 'data' => $result_arr]);

            }

        } catch(Exception $e) {
            $response['status'] = false;
            $response['message'] = "Error!!! Something went wrong ".$e->getMessage().$e->getLine();
        }

        return response()->json($response);
    }

    public function pollResultInfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'poll_id' => 'required',
            'group_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $pollVoteResult = PollVote::where('poll_id',$request->poll_id)->groupBy('restaurant_id')->select(DB::raw("count(*) as count"),'restaurant_id')->orderBy('count','desc')->first();

        // $userGroup = UserGroup::where('id',$request->group_id)->with('members')->first();
        $userGroup = UserGroup::where('id',$request->group_id)->with(['members'=>function($q) {
            $q->where('status','Accepted');
        }])->first();
        if($pollVoteResult) {
            $restaurant = Restaurant::where('id',$pollVoteResult->restaurant_id)->first();
        }
        else {
            $restaurant ='';
        }

        $userGroup->restaurant = $restaurant ?? "";
        $userGroupPoll = $userGroup->polls()->latest()->first();
        $userGroup->poll = $userGroupPoll;
        return response()->json(['status' => true, 'message' => "Poll Vote Result", 'data' => $userGroup]);
    }
    public function checkGroupAdminVote($group_id) {
        $userGroupPollInfo = UserGroupPoll::where('group_id',$group_id)->first();
        $adminVote = 0;
        $membersVote = 0;
        if($userGroupPollInfo) {
            $adminVote = PollVote::where(['poll_id'=>$userGroupPollInfo->id,'user_id'=>$userGroupPollInfo->creator_id])->count();
            $membersVote = PollVote::where(['poll_id'=>$userGroupPollInfo->id])->where('user_id','!=',$userGroupPollInfo->creator_id)->count();
        }

            return array('adminVote'=>$adminVote,'membersVote'=>$membersVote);

    }
    public function closePoll(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'group_id' => 'required',
                'poll_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }
            $voteInfo = $this->checkGroupAdminVote($request->group_id);
            if($voteInfo['adminVote'] > 0) {
                DB::beginTransaction();

                $userGroup = UserGroup::where('id',$request->group_id)->first();
                $userGroup->status = 'POLL_CLOSED';
                $userGroup->save();

                $creator =  $userGroup->creatorGroupMember()->first();
                $creator->payment_method = 'Wallet';
                $creator->save();
                DB::commit();

                $message = config('constants.poll_close');

                $members = GroupMember::where('group_id',$request->group_id)->get();
                if($members)
                foreach ($members as $key => $value) {
                $this->fcm->sendNotification($value->user_id,"Poll Closed",$message,"poll",$request->group_id);

                }
                return response()->json(['status' => true, 'message' => "Poll Closed"]);
            }
            else if($voteInfo['adminVote'] == 1 && $voteInfo['membersVote'] == 0) {
                $this->updateGroupAndPoll($request->group_id,$request->poll_id);
                return response()->json(['status' => true, 'message' => "There is only 1 vote for admin so Poll Closed"]);
            }
            else {
                    $this->updateGroupAndPoll($request->group_id,$request->poll_id);
                return response()->json(['status' => true, 'message' => "Poll Closed"]);
            }
        } catch(Exception $e) {
            return response()->json(['status' => true, 'message' => $e->getLine().'-'.$e->getMessage()]);
        }
    }
    public function updateGroupAndPoll($group_id,$poll_id) {
        $userGroupPollInfo = UserGroupPoll::where('group_id',$group_id)->first();
        if($userGroupPollInfo) {
            DB::transaction(function () use ($poll_id) {
                    $userGroupPoll = UserGroupPoll::where('id',$poll_id)->first();
                    $userGroupPoll->restaurants()->delete();
                    $userGroupPoll->delete();
                });
        PollVote::where('poll_id',$poll_id)->delete();
        $userGroup = UserGroup::where('id',$group_id)->first();
        $userGroup->status = 'GROUP_CREATED';
        $userGroup->save();
        }
    }
    public function updateMenuExpireTime(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'group_id' => 'required|exists:user_groups,id',
                'menu_expire_time' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }
            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;


            $userGroupPoll = UserGroupPoll::where(['group_id'=>$request->group_id,'creator_id'=>$userId])->first();
            if($userGroupPoll) {
                date_default_timezone_set("Asia/Kolkata");

                $max_time = explode(':',$request->menu_expire_time);
                $current_time = date('Y-m-d H:i');

                $menu_expire_time = date('Y-m-d H:i',strtotime('+'.$max_time[0].' hour +'.$max_time[1].' minutes',strtotime($current_time)));
                $userGroupPoll->menu_expire_time = $menu_expire_time;
                if($userGroupPoll->save()) {
                    return response()->json(['status' => true, 'message' => "Menu expire time saved successfully."]);
                }
                else {
                    return response()->json(['status' => false, 'message' => "Menu expire time not saved."]);
                }
            }
            else {
                return response()->json(['status' => false, 'message' => "Group not found."]);
            }

        } catch(Exception $e) {
            return response()->json(['status' => true, 'message' => $e->getMessage()]);
        }
    }
}
