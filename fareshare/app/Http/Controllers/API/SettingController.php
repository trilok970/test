<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Session;
class SettingController extends Controller
{

    public function settings(){
        try {
            $setting_field_arr = config('constants.setting_field_arr');
            $site_settings = Setting::select('slug','name','value')->whereIn('slug', $setting_field_arr)->get();
            $response['status'] = true;
            $response['message'] = "Settings list.";
            $response['data'] = $site_settings;
            return response()->json($response);

        } catch (\Exception $e) {
            $msg = $e->getMessage();
            $response['status'] = false;
            $response['message'] = $msg;
            $response['data'] = [];
            return response()->json($response);
        }
    }// end function.



}
