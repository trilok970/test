<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use JWTAuth;
use Hash;

use Exception;
use DB;

use App\Models\DriverRating;

class DriverRatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'rating' => 'required|numeric|min:1|max:5',
            'order_id' => 'required|numeric|min:1',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>"false",'message' => $validator->errors()->first()]);
        }

        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

       
        if($userId)
        {
            

            $driver_rating = DriverRating::where(['user_id'=>$userId,'order_id'=>$request->order_id])->first();
            if($driver_rating)
                $driver_rating = $driver_rating;
            else
                $driver_rating = new DriverRating;
            
            $driver_rating->user_id = $userId;
            $driver_rating->rating = $request->rating;
            $driver_rating->order_id = $request->order_id;
            $driver_rating->save();

            $response['status'] = true;
            $response['message'] = "Driver rating added successfully.";
            $response['data'] = $driver_rating;

            return response()->json($response);
        }
        else
        {
            $response['status'] = false;
            $response['message'] = "Driver rating not added.";
            $response['data'] = '';
            return response()->json($response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DriverRating  $driverRating
     * @return \Illuminate\Http\Response
     */
    public function show(DriverRating $driverRating)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DriverRating  $driverRating
     * @return \Illuminate\Http\Response
     */
    public function edit(DriverRating $driverRating)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DriverRating  $driverRating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DriverRating $driverRating)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DriverRating  $driverRating
     * @return \Illuminate\Http\Response
     */
    public function destroy(DriverRating $driverRating)
    {
        //
    }
}
