<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use JWTAuth;
use Hash;
use Exception;
use DB;
use App\Models\RestaurantFavourite;

class RestaurantFavouriteController extends Controller
{
    public function favouriteRestaurantList(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

        $restaurant_favourite = RestaurantFavourite::where(['user_id'=>$userId])->with('rastuarant')->get();
        if($restaurant_favourite) {
            $response['status'] = true;
            $response['message'] = "Favourite restaurant list.";
            $response['data'] = $restaurant_favourite;
        }
        else {
            $response['status'] = false;
            $response['message'] = "Favourite restaurant list not found.";
            $response['data'] = [];
        }
        return response()->json($response);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(), [
            'restaurant_id' => 'required|numeric|min:1',
            'status' => 'required|numeric|min:0|max:1',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>"false",'message' => $validator->errors()->first()]);
        }

        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;


        if($userId)
        {
            $restaurant_favourite = RestaurantFavourite::where(['user_id'=>$userId,'restaurant_id'=>$request->restaurant_id])->first();
            if($restaurant_favourite)
                $restaurant_favourite = $restaurant_favourite;
            else
                $restaurant_favourite = new RestaurantFavourite;

            $restaurant_favourite->user_id = $userId;
            $restaurant_favourite->restaurant_id = $request->restaurant_id;
            $restaurant_favourite->status = $request->status;
            $restaurant_favourite->save();

            if($request->status==1)
                $type = "favourited";
            else $type = "un-favourited";

            $response['status'] = true;
            $response['message'] = "Restaurant $type successfully.";
            $response['data'] = $restaurant_favourite;

            return response()->json($response);
        }
        else
        {
            $response['status'] = false;
            $response['message'] = "Restaurant un-favourited successfully.";
            $response['data'] = '';
            return response()->json($response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RestaurantFavourite  $restaurantFavourite
     * @return \Illuminate\Http\Response
     */
    public function show(RestaurantFavourite $restaurantFavourite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RestaurantFavourite  $restaurantFavourite
     * @return \Illuminate\Http\Response
     */
    public function edit(RestaurantFavourite $restaurantFavourite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RestaurantFavourite  $restaurantFavourite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RestaurantFavourite $restaurantFavourite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RestaurantFavourite  $restaurantFavourite
     * @return \Illuminate\Http\Response
     */
    public function destroy(RestaurantFavourite $restaurantFavourite)
    {
        //
    }
}
