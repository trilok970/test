<?php

namespace App\Http\Controllers\API;

use App\DwollaCustomerFunding;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use JWTAuth;
use Hash;

use Exception;
use Illuminate\Support\Facades\DB;

use App\Models\Wallet;
use App\Models\WalletTransaction;
use Stripe;
use App\lib\Dwolla;
use CreateDwollaMastersTable;

class WalletController extends Controller
{
    public $dwolla;
    public function __construct()
    {
        $this->api_per_page = config('constants.api_per_page');
        $this->dwolla = new Dwolla;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function check_dwolla_customer($customer) {

        // echo "<pre>";
        // print_r($customer);
        // exit;
        if($customer && ($customer->dwolla_customer_id == '' || $customer->dwolla_customer_id == null)) {
            // using your customer id we will create
            // brain tree customer id with same id
            $dwolla_customer_id = $this->dwolla->createPersonalVerifiedCustomer($customer);

            // save your braintree customer id
            if( $dwolla_customer_id) {
                $customer->dwolla_customer_id = $dwolla_customer_id;
                $customer->save();
            }
        }
    }
    public function addCustomerFundingSource(Request $request){
        $validator = Validator::make($request->all(), [
            'routing_number' => 'required',
            'account_number' => 'required',
            'bank_account_type' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        try {
            $user = JWTAuth::toUser(JWTAuth::getToken());
            $userId = $user->id;

            // check dwolla customer
            $this->check_dwolla_customer($user);

            $bankInfo['routingNumber'] = $request->routing_number;
            $bankInfo['accountNumber'] = $request->account_number;
            $bankInfo['bankAccountType'] = $request->bank_account_type;
            $bankInfo['name'] = ucfirst($user->user_name)."'s ".$request->bank_account_type." ".substr($request->account_number,-4) ;
            $bankInfo['dwolla_customer_id'] = $user->dwolla_customer_id;

            $funding_source_id = $this->dwolla->addCustomerFundingSource($bankInfo);


            $response['status'] = true;
            $response['message'] = "Account number added successfully.";
            $response['data'] = $funding_source_id;
        }
        catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = $e->getLine()." ".$e->getMessage();

            if($e){
                $exception = json_decode($e->getResponseBody(),true);
                if(isset($exception['_embedded']))
                $response['message'] =$exception['_embedded']['errors'][0]['message'] ;
                else {
                    $response['message'] = $exception['message'];
                    if(strpos($exception['message'],"Bank already exists") !== false){
                    $response['message'] = "Account number has already been taken";
                    }

                }
            //  return   $response['message'] = $e->getResponseBody() ; 22f8db07-10dc-47e6-bd5a-af622aba42fa
            }
        }
        return response()->json($response);
    }
    public function retriveCustomerFundingSources() {
        try {
            $user = JWTAuth::toUser(JWTAuth::getToken());
            $userId = $user->id;

            // check dwolla customer
            $this->check_dwolla_customer($user);

            $funding_sources = $this->dwolla->retriveFundingSourceCustomers($user->dwolla_customer_id);

            foreach($funding_sources as $funding_source) {
                if($funding_source->removed == false) {
                    $funding_data['id'] =  $funding_source->id ?? "";
                    $funding_data['status'] =  $funding_source->status ?? "";
                    $funding_data['type'] =  $funding_source->type ?? "";
                    $funding_data['bankAccountType'] =  $funding_source->bankAccountType ?? "";
                    $funding_data['name'] =  $funding_source->name ?? "";
                    $funding_data['created'] =  $funding_source->created ?? "";
                    $funding_data['bankName'] =  $funding_source->bankName ?? "";
                    $funding_array[] = $funding_data;
                }
            }
            $response['status'] = true;
            $response['message'] = "Customer funding source list.";
            $response['data'] = $funding_array;
        }
        catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = $e->getLine()." ".$e->getMessage();

            if($e){
                $exception = json_decode($e->getResponseBody(),true);
                if($exception['_embedded'])
                $response['message'] =$exception['_embedded']['errors'][0]['message'] ;
                else
                $response['message'] =$exception['message'];
            //  return   $response['message'] = $e->getResponseBody() ;
            }
        }
        return response()->json($response);
    }
    public function deleteCustomerFundingSource(Request $request) {
        try {
            $user = JWTAuth::toUser(JWTAuth::getToken());
            $userId = $user->id;

            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>"false",'message' => $validator->errors()->first()]);
            }

            // check dwolla customer
            $this->check_dwolla_customer($user);

            $funding_sources = $this->dwolla->removeCustomerFundingSource(base64_decode($request->id));


            $response['status'] = true;
            $response['message'] = "Account deleted successfully.";
            $response['data'] = [];
        }
        catch (Exception $e) {
            $response['status'] = false;
            $response['data'] = [];
            $response['message'] = $e->getLine()." ".$e->getMessage();

            if($e){
                $exception = json_decode($e->getResponseBody(),true);
                if(isset($exception['_embedded']))
                $response['message'] =$exception['_embedded']['errors'][0]['message'] ;
                else
                $response['message'] =$exception['message'];
            //  return   $response['message'] = $e->getResponseBody() ;
            }
        }
        return response()->json($response);
    }
    public function store(Request $request)
    {
        //
         $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'token' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>"false",'message' => $validator->errors()->first()]);
        }
        try {
        $user = JWTAuth::toUser(JWTAuth::getToken());
        $userId = $user->id;

        $resData = $this->dwolla->sendMoneyToMaster(base64_decode($request->token),$request->amount);


        if($resData)
        {
            DB::beginTransaction();

            $wallet = Wallet::where(['user_id'=>$userId])->first();
            if($wallet)
                $wallet = $wallet;
            else
                $wallet = new Wallet;



            $amount = ($wallet->amount ?? 0) + $request->amount;

            $wallet->user_id = $userId;
            $wallet->amount = $amount;
            $wallet->save();

            $response['status'] = true;
            $response['message'] = "Wallet money added successfully.";
            $response['data'] = $wallet;

            $this->walletTransactionsAdd($userId,$resData,$request->amount,$wallet->id);
            DB::commit();

        }
        else
        {
            $response['status'] = false;
            $response['message'] = "Wallet money not added.";
            $response['data'] = '';
        }
    }
    catch (Exception $e) {
        $response['status'] = false;
        $response['message'] = $e->getLine()." ".$e->getMessage();

        if($e){
            $exception = json_decode($e->getResponseBody(),true);
            if($exception['_embedded'])
            $response['message'] =$exception['_embedded']['errors'][0]['message'] ;
            else
            $response['message'] =$exception['message'];
        //  return   $response['message'] = $e->getResponseBody() ;
        }
    }
    return response()->json($response);


    }
    public function walletToBankTransfer(Request $request){

        $validator = Validator::make($request->all(), [
           'amount' => 'required|numeric',
           'token' => 'required',
       ]);

       if ($validator->fails()) {
           return response()->json(['status'=>"false",'message' => $validator->errors()->first()]);
       }
    try{
       $user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

       $user = User::where('id',$user_id)->first();

       $transfer_amount = $this->dwolla->sendMoneyToCustomer(base64_decode($request->token),$request->amount);

       if($transfer_amount){

           $this->walletTransactionsWithdraw($user_id,$transfer_amount,$request->amount);

           return response()->json(['status' => 'true','message'=>'Money Transferred Successfully.']);

       }else{
         return response()->json(['status' => 'false','message'=>$transfer_amount['message']]);
       }

    }
    catch (Exception $e) {
        $response['status'] = false;
        $response['message'] = $e->getLine()." ".$e->getMessage();

        if($e){
            $exception = json_decode($e->getResponseBody(),true);
            if($exception['_embedded'])
            $response['message'] =$exception['_embedded']['errors'][0]['message'] ;
            else
            $response['message'] =$exception['message'];
        //  return   $response['message'] = $e->getResponseBody() ;
        return response()->json($response);
        }
    }

   }
    public function check_braintree_customer($customer) {

        // echo "<pre>";
        // print_r($customer);
        // exit;
        if($customer && ($customer->braintree_customer_id == '' || $customer->braintree_customer_id == null)) {
            // using your customer id we will create
            // brain tree customer id with same id
            $response = \Braintree_Customer::create([
                'firstName' => $customer->user_name,
                'lastName' => '',
                'company' => 'Nine Hertz India',
                'email' => $customer->email,
                'phone' => $customer->mobile_number,
                'fax' => '419.555.1235',
                'website' => 'http://example.com'
            ]);

            // save your braintree customer id
            if( $response->success) {
                $customer->braintree_customer_id = $response->customer->id;
                $customer->save();
            }
        }
    }

     public function braintree_payment($customer,$payment_method_nonce,$amount)
     {
        //  echo "<pre>";
        //  print_r($_POST);
        //  dd($request);
        //  exit;
         $this->check_braintree_customer($customer);
           // get your logged in customer


           // when client hit checkout button
           if($payment_method_nonce)
           {
            //    echo "check";exit;
                // brain tree customer payment nouce

                // make sure that if we do not have customer nonce already
                // then we create nonce and save it to our database
                if ( !$customer->braintree_nonce )
                {
                      // once we recieved customer payment nonce
                      // we have to save this nonce to our customer table
                      // so that next time user does not need to enter his credit card details
                      $result = \Braintree_PaymentMethod::create([
                        'customerId' => $customer->braintree_customer_id,
                        'paymentMethodNonce' => $payment_method_nonce
                      ]);


                      // save this nonce to customer table
                      $customer->braintree_nonce = $result->paymentMethod->token;
                      $customer->save();
                }

                // process the customer payment
                $client_nonce = \Braintree_PaymentMethodNonce::create($customer->braintree_nonce);
                $result = \Braintree_Transaction::sale([
                     'amount' => $amount,
                     'options' => [ 'submitForSettlement' => True ],
                     'paymentMethodNonce' => $client_nonce->paymentMethodNonce->nonce
                ]);

                // check to see if braintree has processed
                // our client purchase transaction
                if( !empty($result->transaction) ) {
                    // your customer payment is done successfully
                    // echo "<pre>";
                    // print_r($result);
                    // exit;
                    $data['result'] = 1;
                    $data['message'] = "your customer payment is done successfully";
                }
                else {
                    foreach($result->errors->deepAll() AS $error) {
                        $error[] = ($error->code . ": " . $error->message . "\n");
                    }
                    $data['result'] = 0;
                    $data['message'] = $error;
                }
           }
           else {
               $data['result'] = 0;
               $data['message'] = "Token is required";
           }

           return $data;
     }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_old(Request $request)
    {
        //


         $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'stripe_token' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>"false",'message' => $validator->errors()->first()]);
        }

        $user = JWTAuth::toUser(JWTAuth::getToken());
        $userId = $user->id;

        $resData = $this->braintree_payment($user,base64_decode($request->stripe_token),$request->amount);

        // comment for stripe payment gateway
        // Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        // $stripe =  Stripe\Charge::create ([
        //         "amount" => $request->amount,
        //         "currency" => "usd",
        //         "source" => $request->stripe_token,
        //         "description" => "Add payment in wallet."
        // ]);
        if($resData['result'] == 1)
        {
            DB::beginTransaction();

            $wallet = Wallet::where(['user_id'=>$userId])->first();
            if($wallet)
                $wallet = $wallet;
            else
                $wallet = new Wallet;



            $amount = ($wallet->amount ?? 0) + $request->amount;

            $wallet->user_id = $userId;
            $wallet->amount = $amount;
            $wallet->save();

            $response['status'] = true;
            $response['message'] = "Wallet money added successfully.";
            $response['data'] = $wallet;

            $this->walletTransactionsAdd($userId,$request->stripe_token,$request->amount,$wallet->id);
            DB::commit();

            return response()->json($response);
        }
        else
        {
            $response['status'] = false;
            $response['message'] = "Wallet money not added.";
            $response['data'] = '';
            return response()->json($response);
        }


    }
    public function walletTransactionsAdd($userId,$stripe_token,$amount,$wallet_id)
    {
            $WalletTransaction = new WalletTransaction;

            $WalletTransaction->user_id = $userId;
            $WalletTransaction->wallet_id = $wallet_id;
            $WalletTransaction->paytype = $amount;
            $WalletTransaction->stripe_token = $stripe_token;
            $WalletTransaction->comment = number_format($amount,2)." credited to your wallet";
            $WalletTransaction->transaction_type = "credit";
            $WalletTransaction->save();
    }
    public function walletTransactionsWithdraw($userId,$stripe_token,$wallet_amount)
    {
             $wallet = Wallet::where(['user_id'=>$userId])->first();
            if($wallet)
                $wallet = $wallet;
            else
                $wallet = new Wallet;

            $amount = (float)($wallet->amount ?? 0) - (float)$wallet_amount;

            $wallet->user_id = $userId;
            $wallet->amount = $amount;
            $wallet->save();
            $WalletTransaction = new WalletTransaction;

            $WalletTransaction->user_id = $userId;
            $WalletTransaction->wallet_id = $wallet->id;
            $WalletTransaction->paytype = -$wallet_amount;
            $WalletTransaction->stripe_token = $stripe_token;
            $WalletTransaction->comment = number_format($wallet_amount,2)." withdraw from your wallet";
            $WalletTransaction->transaction_type = "withdraw";
            $WalletTransaction->save();
    }
    public function getwallet($userId)
    {
        return Wallet::where('user_id',$userId)->first();
    }
    public function walletTransactions()
    {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $wallet = $this->getwallet($userId)->amount ?? 0;
        $walletTransactions = WalletTransaction::where(['user_id'=>$userId])->orderBy('id','desc')->paginate($this->api_per_page);
        if($walletTransactions)
        {
            $response['status'] = true;
            $response['message'] = "Wallet transactions found successfully.";
            $response['wallet'] = $wallet;
            $response['data'] = $walletTransactions;

            return response()->json($response);
        }
        else
        {
            $response['status'] = false;
            $response['message'] = "Wallet transactions not found.";
            $response['data'] = '';
            return response()->json($response);
        }
    }

    protected function update_user_stripe_details($id,$stripe_customer_id)
    {
        $user = User::where('id',$id)->first();
        $user->stripe_customer_id = $stripe_customer_id;
        $user->save();
        return $user;
    }
    public function stripeConnection()
    {


        $user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

        $user = User::where('id',$user_id)->first();

        $customer_token = \App\lib\Stripe::createCustomer($user->email);

        if($user->stripe_customer_id==null || $user->stripe_customer_id == '')
        {
        $user = $this->update_user_stripe_details($user->id,$customer_token['customer_id']);
        }

        if($user->stripe_user_id=='' && $user->stripe_user_id==null){
              $client_id = env('CLIENT_ID');

              $state = $user->stripe_customer_id;
              $email = $user->email;
              $redirect_uri = url(route('create-connect'));
              $name = $user->full_name;
              $url = "https://connect.stripe.com/express/oauth/authorize?redirect_uri=$redirect_uri&client_id=$client_id&state=$state&stripe_user[email]=$email&suggested_capabilities[]=card_payments&scope=read_write&always_prompt=true&stripe_user[name]=$name";
              // $url = "https://connect.stripe.com/express/oauth/authorize?redirect_uri=$redirect_uri&client_id=$client_id&state=$state&stripe_user[email]=$email&scope=read_write&always_prompt=true";

              return response()->json(['status' => 'true','url'=>$url]);
          }
        exit;
        // For Braintree
        $this->check_braintree_customer($user);

        if($user->braintree_nonce == '' && $user->braintree_nonce == null){
            $client_id = env('BTREE_CLIENT_ID');
            $client_secret = env('BTREE_CLIENT_SECRET');
            $redirect_uri = "http://testing.demo2server.com/fareshare/create-connect";
            $state = $user->braintree_customer_id;
            $gateway = new \Braintree\Gateway([
                'clientId' => $client_id,
                'clientSecret' => $client_secret
            ]);

            $url = $gateway->oauth()->connectUrl([
                'redirectUri' => $redirect_uri,
                'scope' => 'shared_vault_transactions',
                'state' => $state
            ]);
              return response()->json(['status' => 'true','url'=>$url]);
          }

    }

    public function walletToBankTransfer_old(Request $request){

         $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>"false",'message' => $validator->errors()->first()]);
        }

        $user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

        $user = User::where('id',$user_id)->first();

        $transfer_amount = \App\lib\Stripe::transfer($request->amount,$user->stripe_user_id);

        if($transfer_amount['status'] == 1){

            $this->walletTransactionsWithdraw($user_id,$user->stripe_user_id,$request->amount);


            return response()->json(['status' => 'true','message'=>'Money Transferred Successfully.']);

        }else{
          return response()->json(['status' => 'false','message'=>$transfer_amount['message']]);
        }

    }






    /**
     * Display the specified resource.
     *
     * @param  \App\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function show(Wallet $wallet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function edit(Wallet $wallet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wallet $wallet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wallet $wallet)
    {
        //
    }
}
