<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use JWTAuth;
use Hash;

use Exception;
use Illuminate\Support\Facades\DB;

use App\Models\Wallet;
use Stripe;
use App\Models\WalletTransaction;

class WalletTransactionController extends Controller
{
    public function __construct()
    {
        $this->api_per_page = config('constants.api_per_page');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $walletTransactions = WalletTransaction::where(['user_id'=>$userId])->orderBy('id','desc')->paginate($this->api_per_page);
        if($walletTransactions)
        {
            $response['status'] = true;
            $response['message'] = "Wallet transaction found successfully.";
            $response['data'] = $walletTransactions;

            return response()->json($response);
        }
        else
        {
            $response['status'] = false;
            $response['message'] = "Wallet transaction not found.";
            $response['data'] = '';
            return response()->json($response);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WalletTransaction  $walletTransaction
     * @return \Illuminate\Http\Response
     */
    public function show(WalletTransaction $walletTransaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WalletTransaction  $walletTransaction
     * @return \Illuminate\Http\Response
     */
    public function edit(WalletTransaction $walletTransaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WalletTransaction  $walletTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WalletTransaction $walletTransaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WalletTransaction  $walletTransaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(WalletTransaction $walletTransaction)
    {
        //
    }
}
