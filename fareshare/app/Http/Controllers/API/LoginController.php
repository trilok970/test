<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use JWTAuth;
use Hash;
use App\Models\Otp;
use App\Models\UserDevice;
use App\Models\Country;
use App\Models\Driver;
use App\Models\Restaurant;
use App\Models\RestaurantMedia;
use App\Models\RestaurantContinental;
use App\Models\Continental;
use App\Models\UserRole;
use App\Models\Role;
use App\Models\Cms;

use File;
use Mail;
use Illuminate\Support\Facades\Storage;

class LoginController extends Controller
{

    protected function checkuser(Request $request) {

        $validator = Validator::make($request->all(), [
                        'country_code' => 'required|numeric|digits_between:0,5',
                        'mobile_number' => 'required|numeric|digits_between:7,15',
                    ]);

        if ($validator->fails()) {

            $response['status'] = "false";
            $response['message'] = $validator->errors()->first();
            $response['data'] = (object)[];
            return response()->json($response);

        } else {

            $details = ['mobile_number' => $request->mobile_number,'country_code' => $request->country_code];

            $user = User::where($details)->first();
            if (isset($user) && !empty($user)) {
                $mobile_number = $request->country_code.$request->mobile_number;
                $otp = substr(str_shuffle("0123456789"), 0, 6);
                Otp::where('mobile_number',$mobile_number)->delete();
                $res = Otp::Create(['mobile_number' => $mobile_number,'otp' => $otp]);

                // Send OTP Code
                //$this->sendOtp($phone_number, $country_code, $otp_code);

                $response['status'] = "true";
                $response['message'] = "OTP code send successfully.";
                $response['data'] = (object) ["otp" => $otp];
            } else {
                $response['status'] = "false";
                $response['message'] = "Please create an account. Your phone number does'nt exist.";
                $response['data'] = (object)[];
            }
            return response()->json($response);
        }
    }


    // user login
    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'role' => 'required',
            'country_code' => 'required',
            'mobile_number' => 'required',
            'password' => 'required',
            'device_id' => 'required',
            'device_type' => 'required|in:ANDROID,IOS'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>"false",'message' => $validator->errors()->first()]);
        }

        $user = User::where('country_code', $request->country_code)
        ->where('mobile_number', $request->mobile_number)
        ->first();

        if(!$user) {
            return response()->json([
                'status' => "false",
                'message' => 'User not found.',
                'data' => (object)[]
            ]);
        }

        $role = UserRole::join('roles as R', 'R.id', '=', 'user_roles.role_id')
        ->where('user_roles.user_id', $user->id)
        ->where('R.role', ucfirst($request->role))
        ->first();

        if($role) {
            $user->role = strtolower($request->role);
            $user->save();
        } else {
            return response()->json([
                'status' => "false",
                'message' => 'User not found.',
                'data' => (object)[]
            ]);
        }

        if ($user->status == 0){
            return response()->json([
                'status' => "false",
                'message' => 'Your account is deactivated contact to administrator.',
                'data' => (object)[]
            ],401);

        }

        if(Hash::check($request->password,$user->password)){
            $jwtToken = JWTAuth::fromUser($user);

            // try to auth and get the token using api authentication
            if (!$jwtToken) {
                // if the credentials are wrong we send an unauthorized error in json format
                return response()->json(['status'=>"false",'message' => 'Unauthorized'],401);
            }

            $request->log_status = 1;
            $this->_updateUserDevice($user->id, $request);

            if($user->role == "driver") {
                $user->driver;
            } else if($user->role == "vendor") {
                $user->restaurant;
                $user->restaurantMedia;
                $user->restaurant->restaurantContinental;

                //dd($user->restaurant);

                $user->restaurant->opening_time_full = $user->restaurant->opening_time;
                $date = date_create($user->restaurant->opening_time);
                if($user->restaurant->opening_time) {
                    $user->restaurant->opening_time = date_format($date,"h:i a");
                } else {
                    $user->restaurant->opening_time = null;

                }

                $user->restaurant->closing_time_full = $user->restaurant->closing_time;
                $date2 = date_create($user->restaurant->closing_time);
                if($user->restaurant->closing_time) {
                    $user->restaurant->closing_time = date_format($date2,"h:i a");
                } else {
                    $user->restaurant->closing_time = null;
                }

            }

            return response()->json([
                'status' => "true",
                'token' => $jwtToken,
                'type' => 'bearer', // you can ommit this
                'expires' => auth('api')->factory()->getTTL() * 60, // time to expiration
                'data' => $user
            ]);

        } else {

            return response()->json([
                'status' => "false",
                'message' => 'Password Incorrect.',
                'data' => (object)[]
            ]);
        }

    }


    /**
     * Send Sign-up OTP
     *
     * @param  array  $request
     * @return \Illuminate\Http\Response
    */
    protected function otp(Request $request) {

        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'country_code'  => 'required|numeric|digits_between:0,5',
            'mobile_number'  =>   'required|numeric|unique:users|digits_between:7,15',
            'device_type'   => 'required|in:ANDROID,IOS',
            'device_id'  => 'required',
        ]);

        if(isset($data['user_id']) && $data['user_id'] && $data['email']) {
            $validator = Validator::make($request->all(), [
                'email'  =>   'email|unique:users,email,'.$data['user_id'],
            ]);
        }

        if ($validator->fails()){

            $response['status'] = "false";
            $response['message'] = $validator->errors()->first();
            $response['data'] = (object)[];
            return response()->json($response);

        }else{
                if ($validator->fails()){
                    $response['status'] = "false";
                    $response['message'] = $validator->errors()->first();
                    $response['data'] = (object)[];
                }else{

                    try{

                        $mobile_number = $data['country_code'].$data['mobile_number'];

                        $otp = substr(str_shuffle("0123456789"), 0, 6);

                        $otp_in_db =  Otp::select('otp','created_at')->where('mobile_number',$mobile_number)->first();
                        if($otp_in_db){
                           //dd($otp_in_db->created_at);
                           $date1 = date_create(date('Y-m-d H:i:s'));
                           $date2 = date_create($otp_in_db->created_at);
                           $diff=date_diff($date1,$date2);
                           $time_minutes = (int) $diff->format('%i');

                           //if($time_minutes > 1){
                                Otp::where('otp',$otp_in_db->otp)->delete();
                           //}
                           //else $otp = $otp_in_db->otp;
                        }

                        $response['status'] = "true";
                        $response['message'] = "OTP code send successfully.";
                        $response['otp'] = $otp;

                        Otp::where('mobile_number',$mobile_number)->delete();
                        $res = Otp::Create(['mobile_number' => $mobile_number,'otp' => $otp]);

                    } catch (\Exception $e) {
                        $response['status'] = "false";
                        $response['message'] = $e->getMessage();
                        $response['otp'] = "";
                    }
                }
                return response()->json($response);
        }

    }

    /**
     * Send Sign-up Resend OTP
     *
     * @param  array  $request
     * @return \Illuminate\Http\Response
    */
    protected function resendOtp(Request $request) {
        $data = $request->all();
        // return response()->json($data);
        $utype= (isset($data['user_type']))?$data['user_type'] : 'new';

        if($utype=='exist')
        {
            $rules =  [
                    'user_type'         =>'nullable',
                    'country_code'      => 'required|numeric|digits_between:0,5',
                    'mobile_number'      =>  'required|numeric|digits_between:7,15',
                ];
        } else {
            $rules =  [
                    'user_type'         =>'nullable',
                    'country_code'      => 'required|numeric|digits_between:0,5',
                    'mobile_number'      =>  'required|numeric|unique:users|digits_between:7,15',
                ];
        }

        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()){
            $response['status'] = "false";
            $response['message'] = $validator->errors()->first();
            $response['data'] = (object)[];
            return response()->json($response);
        } else {
            if ($validator->fails()) {
                $response['status'] = "false";
                $response['message'] = $validator->errors()->first();
                $response['data'] = (object)[];
            } else {
                try {
                    $mobile_number = $data['country_code'].$data['mobile_number'];
                    $otp = substr(str_shuffle("0123456789"), 0, 6);
                    $otp_in_db =  Otp::select('otp','created_at')->where('mobile_number',$mobile_number)->first();

                    if($otp_in_db) {
                        //dd($otp_in_db->created_at);
                        $date1 = date_create(date('Y-m-d H:i:s'));
                        $date2 = date_create($otp_in_db->created_at);
                        $diff=date_diff($date1,$date2);
                        $time_minutes = (int) $diff->format('%i');

                        //if($time_minutes > 1){
                        Otp::where('otp',$otp_in_db->otp)->delete();
                        //}
                        //else $otp = $otp_in_db->otp;
                    }

                    $response['status'] = "true";
                    $response['message'] = "OTP code send successfully.";
                    $response['otp'] = $otp;

                    Otp::where('mobile_number',$mobile_number)->delete();

                    $res = Otp::Create(['mobile_number' => $mobile_number,'otp' => $otp]);

                } catch (\Exception $e) {
                    $response['status'] = "false";
                    $response['message'] = $e->getMessage();
                    $response['otp'] = "";
                }
            }
            return response()->json($response);
        }

    }

    /**
     * Verify OTP in login, sign-up and forgot
     *
     * @param  array  $request
     * @return \Illuminate\Http\Response
    */
    protected function otpVerify(Request $request) {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'otp'           => 'required',
            'country_code'      => 'required|numeric|digits_between:0,5',
            'mobile_number'      =>  'required|numeric|digits_between:7,15',
        ]);

        if ($validator->fails()){
            $response['status'] = "false";
            $response['message'] = $validator->errors()->first();
            $response['data'] = (object)[];
            return response()->json($response);
        }else{
                if ($validator->fails()){

                    $response['status'] = "false";
                    $response['message'] = $validator->errors()->first();
                    $response['data'] = (object)[];

                }else{

                    try{

                        $mobile_number = $data['country_code'].$data['mobile_number'];
                        $mobile_number = str_replace(' ','',$mobile_number);

                        // attempt to do the login
                        $user = Otp::where('mobile_number',$mobile_number)->first();
                        if(!$user){
                            $response['status'] = "false";
                            $response['message'] = "Phone number does not exist.";
                            $response['data'] = (object)[];
                            return response()->json($response);
                        }else{

                            $otp_check = Otp::where('mobile_number',$mobile_number)->where('otp',$data['otp'])->exists();

                            if(!$otp_check){
                                $response['status'] = "false";
                                $response['message'] = 'Please enter correct OTP.';
                                $response['data'] = (object)[];
                                return response()->json($response);
                            }


                            $response['status'] = "true";
                            $response['message'] = 'OTP verified successfully.';
                            $response['data'] = (object)[];
                            return response()->json($response);


                        }

                    } catch (\Exception $e) {

                        $response['status'] = "false";
                        $response['message'] = $e->getMessage();
                        $response['otp'] = "";

                    }

                }

                return response()->json($response);
        }

    }

    // This method use for user logout
    protected function userLogout(Request $request) {
        $data = $request->all();

        try{
            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        } catch (\Exception $e) {
            $response['status'] = "false";
            $response['message'] = 'Invalid user token';
            $response['data'] = (object)[];

            return response()->json($response);
        }

        $validator = Validator::make($request->all(), [
            'device_type'       => 'required|in:ANDROID,IOS',
            'device_id'      => 'required',
        ]);

        if ($validator->fails()){

            $response['status'] = "false";
            $response['message'] = $validator->errors()->first();
            return response()->json($response);

        }else{

            $userDevice = UserDevice::where(['device_type'=> $data['device_type'],'device_id'=>$data['device_id']])->first();

            if($userDevice){
                UserDevice::where(['device_type'=> $data['device_type'],'device_id'=>$data['device_id']])->delete();
                JWTAuth::invalidate();
                $response['status'] = "true";
                $response['message'] = "Logout successfully";
                return response()->json($response);
            }else{
                $response['status'] = "true";
                $response['message'] = "Logout successfully";
                return response()->json($response);
            }
        }
    }

    /**

     * The has created user in storage.

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

    */
    protected function signup(Request $request) {
        $base_url =  url('/');
        $data = $request->all();

        $social_id = (isset($data['social_id']))?$data['social_id']:'';

        if ($data['role']=='vendor') {
            $validator = Validator::make($request->all(), [
                'otp'               => 'required',
                'country_code'      => 'required|numeric|digits_between:0,5',
                'mobile_number'     => 'required|numeric|digits_between:7,15|unique:users,mobile_number',
                'email'             => 'nullable|unique:users,email|max:255',
                'restaurant_name'         => 'required|min:3|max:200',
                'password'          => 'required|min:8|max:30',
                'confirm_password'  => 'required_with:password|same:password|min:8|max:30',
                'device_type'       => 'required|in:ANDROID,IOS',
                'role'              => 'required|in:vendor',
                'device_id'         => 'required',
                'sign'              => 'required|mimes:jpeg,jpg,gif,png|max:2048',
                'dob'               => 'required',
                'ssn'               => 'required|numeric|digits:4',
            ]);
        } elseif ($data['role']=='driver') {
            $validator = Validator::make($request->all(), [
                'otp'               => 'required',
                'country_code'      => 'required|numeric|digits_between:0,5',
                'mobile_number'     => 'required|numeric|digits_between:7,15|unique:users,mobile_number',
                'email'             => 'nullable|unique:users,email|max:255',
                'user_name'         => 'required|min:3|max:200',
                'password'          => 'required|min:8|max:30',
                'confirm_password'  => 'required_with:password|same:password|min:8|max:30',
                'device_type'       => 'required|in:ANDROID,IOS',
                'role'              => 'required|in:driver',
                'device_id'         => 'required',
                'sign'              => 'required|mimes:jpeg,jpg,gif,png|max:2048',
                'dob'               => 'required',
                'ssn'               => 'required|numeric|digits:4',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'otp'               => 'required',
                'country_code'      => 'required|numeric|digits_between:0,5',
                'mobile_number'     => 'required|numeric|digits_between:7,15|unique:users,mobile_number',
                'email'             => 'nullable|unique:users,email|max:255',
                'user_name'         => 'required|min:3|max:200',
                'password'          => 'required|min:8|max:30',
                'confirm_password'  => 'required_with:password|same:password|min:8|max:30',
                'device_type'       => 'required|in:ANDROID,IOS',
                'role'              => 'required|in:user',
                'device_id'         => 'required',
                'dob'               => 'required',
                'ssn'               => 'required|numeric|digits:4',
            ]);
        }


        if ($validator->fails()) {

            $response['status'] = "false";
            $response['message'] = $validator->errors()->first();
            $response['data'] = (object)[];
            return response()->json($response);

        } else {
            try{

                $mobile_number = $data['country_code'].$data['mobile_number'];
                $mobile_number = str_replace(' ','',$mobile_number);

                $otp_check = Otp::where('mobile_number',$mobile_number)->where('otp',$data['otp'])->exists();

                if(!$otp_check){
                    $response['status'] = "false";
                    $response['message'] = 'Please enter correct OTP.';
                    $response['data'] = (object)[];
                    return response()->json($response);
                }

                $user = new User;
                if ($data['role']=='vendor') {
                    $user->user_name = $data['restaurant_name'];
                } else {
                    $user->user_name = $data['user_name'];
                }

                if($request->sign) {
                    $png_url = "sign_".time().'.'.$request->sign->extension();
                    $request->sign->move(storage_path('app/public/users/signs/'), $png_url);
                    $user->sign = 'storage/app/public/users/signs/' . $png_url;
                }

                $user->email = $data['email'];
                $user->country_code = $data['country_code'];
                $user->password = bcrypt($data['password']);
                $user->mobile_number = $data['mobile_number'];
                if(isset($data['address'])) $user->address = $data['address'];
                $user->role = $data['role'];
                if(isset($data['city'])) $user->city = $data['city'];
                if(isset($data['state'])) $user->state = $data['state'];
                if(isset($data['zip'])) $user->zip = $data['zip'];
                if(isset($data['lat'])) $user->lat = $data['lat'];
                if(isset($data['lon'])) $user->lon = $data['lon'];
                if(isset($request->ssn)) $user->ssn = $request->ssn;
                if(isset($request->dob)) $user->dob = date('Y-m-d',strtotime($request->dob));
                if (($data['role']=='driver') || ($data['role']=='vendor')) {
                    $user->status = 0;
                } else {
                    $user->status = 1;
                }

                $hash = md5($data['email']).time();
                $user->forgot_token = $hash;

                $user->save();

                $role = Role::where('role','Like',strtolower($data['role']))->first();
                if($role) {
                    $userRole = new UserRole();
                    $userRole->user_id = $user->id;
                    $userRole->role_id = $role->id;
                    $userRole->save();

                    $user->signup_role_id = $role->id;
                    $user->save();
                }


                $response['message'] = "Customer has been registered successfully.";

                // if role is Vendor/Restaurant
                if ($data['role']=='vendor') {


                    $addData = array(
                        'user_id' => $user->id,
                        'restaurant_name' => $data['restaurant_name'],
                    );

                    $this->addRestaurant($request, $user->id);
                    $response['message'] = "Restaurant have been successfully registered!. Please wait for the admin approval or Check email for account confirmation link.";
                    $user->restaurant;

                    ##### email send #####

                    // $from_mail = 'oliver7415@googlemail.com';
                    $from_mail =  env("MAIL_FROM_ADDRESS");
                    $site_title = env("APP_NAME");

                    // Send email
                    $sdata=[];
                    $sdata['name'] = $user->user_name;
                    $sdata['token'] = $hash;    // hash will be send in the url.
                    $sdata['base_url'] = $base_url;
                    $email = $user->email;

                    // Mail::send('email.registermail', ['data' => $sdata], function ($m) use ($data, $from_mail, $site_title, $email) {
                    //     $m->from($from_mail, $site_title);
                    //     $m->to($email, 'Name of Reciever')->subject('Account registration email.');
                    // });

                    ##### email send. ends #####

                } elseif ($data['role']=='driver') {

                    $this->addDriver($request, $user->id);
                    $response['message'] = "Driver have been successfully registered!. Please wait for the admin approval";
                    $user->driver;
                }

                if($user){
                    UserDevice::deviceHandle([
                        "id"       =>  $user->id,
                        "device_type"   =>  $data['device_type'],
                        "device_id"  =>  $data['device_id'],
                    ]);
                }


                $jwtToken = JWTAuth::fromUser($user);
                //dd($jwtResponse);
                $response['data'] = $user;
                $response['token'] = $jwtToken;
                $response['status'] = "true";

            } catch (\Exception $e) {
                $response['status'] = "false";
                $response['message'] = $e->getLine().' : '.$e->getMessage();
                $response['data'] = (object)[];
            }

            return response()->json($response);

        }

    }

    protected function forgotPassword(Request $request) {

        $validator = Validator::make($request->all(), [
                    'country_code' => 'required|numeric|digits_between:0,5',
                    'mobile_number' => 'required|numeric|digits_between:7,15',
        ]);
        if ($validator->fails()){

            $response['status'] = "false";
            $response['message'] = $validator->errors()->first();
            $response['data'] = (object)[];
            return response()->json($response);

        }else{

            $details = ['mobile_number'=>$request->mobile_number,'country_code'=>$request->country_code];

            $user = User::where($details)->first();
            if (isset($user) && !empty($user)) {
                $mobile_number = $request->country_code.$request->mobile_number;
                $otp = substr(str_shuffle("0123456789"), 0, 6);
                Otp::where('mobile_number',$mobile_number)->delete();
                $res = Otp::Create(['mobile_number' => $mobile_number,'otp' => $otp]);

                // Send OTP Code
                //$this->sendOtp($phone_number, $country_code, $otp_code);

                $response['status'] = "true";
                $response['message'] = "OTP code send successfully.";
                $response['data'] = (object) ["otp" => $otp];
            } else {
                $response['status'] = "false";
                $response['message'] = "Please create an account. Your phone number does'nt exist.";
                $response['data'] = (object)[];
            }
            return response()->json($response);
        }
    }


    /*
        9.) Forgot password(for changing password) [Done]
        Test Url: http://testing.demo2server.com/fareshare/api/v2/forgot-password-save
        Url: forgot-password-save
        Method:POST
        Required Parameters : password, password_confirmation
        Response:
    */
    protected function forgotPasswordSave(Request $request) {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:8',
            'password_confirmation' => 'required|min:8',
        ]);

        if ($validator->fails()){
            $response['status'] = "false";
            $response['message'] = $validator->errors()->first();
            $response['data'] = (object)[];
            return response()->json($response);
        } else {
            // $user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
            $user_id = $request->input('user_id');
            $user = User::where('id', $user_id)->first();

            if (!$user) {
                $response['message'] = "Invalid User";
                $response['status'] = "false";
                $response['data'] = (object)[];
            } else {

                if(isset($request->password) && isset($user->password) ){

                    if (Hash::check($request->password, $user->password)) {
                        $user->password = bcrypt($request->password);
                        $user->save();
                        $profile = User::find($user->id);
                        $response['status'] = "true";
                        $response['message'] = "Password is updated successfully.";
                        $response['data'] = $profile;
                    }else {
                        $response['message'] = "Current password is incorrect.";
                        $response['status'] = "false";
                        $response['data'] = (object)[];
                    }
                }
            }
        }
        return response()->json($response);
    }

    protected function resetPassword(Request $request) {

        $validator = Validator::make($request->all(), [
                        'country_code' => 'required|numeric',
                        'mobile_number' => 'required|numeric|digits_between:7,15',
                        'password' => 'required|min:8',
                    ]);

        if ($validator->fails()){
            $response['status'] = "false";
            $response['message'] = $validator->errors()->first();
            $response['data'] = (object)[];
            return response()->json($response);
        } else {
            $response['status'] = "false";
            $response['message'] = "The phone number you entered could not be found in our records.";

            $credentials = ['mobile_number' => $request->mobile_number, 'country_code' => $request->country_code];

            $user = User::where($credentials)->first();
            if (isset($user)) {
                $user->password = bcrypt($request->password);
                $user->save();
                $response['status'] = "true";
                $response['message'] = "Your password updated successfully";
                $response['data'] = (object)[];
            } else {
                $response['message'] = "The phone number you entered could not be found in our records.";
            }
        }
        return response()->json($response);
    }

    public function getProfile(Request $request) {

        if($request->isMethod('post')){
            try{
                // dd(JWTAuth::toUser(JWTAuth::getToken()));
                $userStatus = JWTAuth::toUser(JWTAuth::getToken())->status;
                if ($userStatus == 0){
                    return response()->json([
                        'status' => "false",
                        'message' => 'Your account is deactivated contact to administrator.',
                        'data' => (object)[]
                    ],401);

                }

                $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

                $profile = User::find($userId);
                if ($profile->role=='vendor') {
                    $profile->restaurant;
                    $profile->restaurantMedia;
                } elseif ($profile->role=='driver') {
                    $profile->driver;
                }
                $response['status'] = "true";
                $response['message'] = "User profile data.";
                $response['data'] = $profile;
            } catch (\Exception $e) {
                $response['status'] = "false";
                $response['message'] = $e->getMessage();
                $response['data'] = (object)[];
            }

            return response()->json($response);
        }
    }


    protected function changePassword(Request $request) {
        $userStatus = JWTAuth::toUser(JWTAuth::getToken())->status;
        if ($userStatus == 0){
            return response()->json([
                'status' => "false",
                'message' => 'Your account is deactivated contact to administrator.',
                'data' => (object)[]
            ],401);
        }

        $user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
        $validator = Validator::make($request->all(), [
                    'current_password' => 'required',
                    'new_password' => 'required|min:8',
        ]);
        if ($validator->fails()){

            $response['status'] = "false";
            $response['message'] = $validator->errors()->first();
            $response['data'] = (object)[];
            return response()->json($response);

        }else{

            $user = User::where('id', $user_id)->first();

            if (!$user) {
                $response['message'] = "Invalid User";
                $response['status'] = "false";
                $response['data'] = (object)[];
            } else {

                if(isset($request->current_password) && isset($user->password) ){

                    if (Hash::check($request->current_password, $user->password)) {
                        $user->password = bcrypt($request->new_password);
                        $user->save();
                        $profile = User::find($user->id);
                        $response['status'] = "true";
                        $response['message'] = "Password is updated successfully.";
                        $response['data'] = $profile;
                    }else {
                        $response['message'] = "Current password is incorrect.";
                        $response['status'] = "false";
                        $response['data'] = (object)[];
                    }
                }
            }
        }
        return response()->json($response);
    }




    public function _updateUserDevice($user_id, $data)
    {
        $device = UserDevice::where('user_id',$user_id)
            ->where('device_id',$data->device_id)
            ->where('device_type',$data->device_type)
            ->first();
        if($device) {
            $device->device_id = $data->device_id;
            $device->log_status = $data->log_status;
            $device->save();
        } else {
            UserDevice::create(['user_id' => $user_id, 'device_id' => $data->device_id, 'device_type'=>$data->device_type, 'log_status' => $data->log_status]);
        }
    }

    public function updateProfile(Request $request) {
        $userStatus = JWTAuth::toUser(JWTAuth::getToken())->status;
        if ($userStatus == 0){
            return response()->json([
                'status' => "false",
                'message' => 'Your account is deactivated contact to administrator.',
                'data' => (object)[]
            ],401);
        }

        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $validator = Validator::make($request->all(), [
            'country_code'      => 'numeric|digits_between:0,5',
            'mobile_number' => 'numeric|digits_between:7,15|unique:users,mobile_number,'.$userId,
            'email' => 'email|unique:users,email,'.$userId,

        ]);

        if ($validator->fails()) {
            $response['status'] = "false";
            $response['message'] = $validator->errors()->first();
            $response['data'] = (object)[];
            return response()->json($response);
        }
        $user = User::find($userId);

        if($request->profile_picture) {
            $png_url = "profile_".time().'.'.$request->profile_picture->extension();
            $request->profile_picture->move(storage_path('app/public/users/profile_pictures/'), $png_url);
            $user->profile_picture = 'storage/app/public/users/profile_pictures/' . $png_url;
        }

        if(isset($request->mobile_number)) $user->mobile_number = $request->mobile_number;
        if(isset($request->email)) $user->email = $request->email;
        if(isset($request->country_code)) $user->country_code = $request->country_code;
        if(isset($request->user_name)) $user->user_name = $request->user_name;
        if(isset($request->city)) $user->city = $request->city;
        if(isset($request->state)) $user->state = $request->state;
        if(isset($request->zip)) $user->zip = $request->zip;
        if(isset($request->address)) $user->address = $request->address;
        if(isset($request->lat)) $user->lat = $request->lat;
        if(isset($request->lon)) $user->lon = $request->lon;
        if(isset($request->ssn)) $user->ssn = $request->ssn;
        if(isset($request->dob)) $user->dob = date('Y-m-d',strtotime($request->dob));
        $user->save();

        $response['status'] = "true";
        $response['message'] = 'Customer Profile updated successfully.';
        $response['data'] = $user;
        return response()->json($response);
    }

    public function updateProfileVendor(Request $request) {
        // print_r( $request->continental); die;
        $userStatus = JWTAuth::toUser(JWTAuth::getToken())->status;

        if ($userStatus == 0){
            return response()->json([
                'status' => "false",
                'message' => 'Your account is deactivated contact to administrator.',
                'data' => (object)[]
            ],401);

        }

        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

        // $text = (string) $request->del_image_ids;
        // preg_match_all("/\[([^\]]*)\]/", $text, $matches);
        // $delFiles = ($matches[1][0]);

        $delFiles = (array) $request->del_image_ids;
        // $delFiles = [15,16];
        if ($delFiles) {
            // $delIds = explode(',', $delFiles);
            if (count($delFiles)>0) {
                $restaurantMedia = RestaurantMedia::whereIn('id', $delFiles)->get();
                foreach ($restaurantMedia as $media) {
                    $vendorimg = str_replace('storage/', '', $media->image);
                    if (file_exists(storage_path($vendorimg))) {
                        unlink(storage_path($vendorimg));
                    }
                }
                RestaurantMedia::whereIn('id', $delFiles)->delete();
            }
        }

        $limit = 0;
        $rest = RestaurantMedia::where('user_id', $userId)->get();
        $existingImg = count($rest);
        $limit = (5 - $existingImg);

        // Image check.
        $files = $request->file('restaurant_picture');
        if (isset($files)) {
            if ($limit==0) {
                $response['status'] = "false";
                $response['message'] = 'You can not upload any more image maximum limit(5) reached.';
                $response['data'] = [];
                return response()->json($response);
            }

            if (count($files) > $limit) {
                $txt = ($limit>1) ? 'images' : 'image' ;
                $response['status'] = "false";
                $response['message'] = 'You can upload maximum ' . $limit .' ' . $txt .'.';
                $response['data'] = [];
                return response()->json($response);
            }
        }


        $validator = Validator::make($request->all(), [
            'country_code'      => 'numeric|digits_between:0,5',
            'mobile_number' => 'numeric|digits_between:7,15|unique:users,mobile_number,'.$userId,
            'email' => 'email|unique:users,email,'.$userId,

        ]);

        if ($validator->fails()) {
            $response['status'] = "false";
            $response['message'] = $validator->errors()->first();
            $response['data'] = (object)[];
            return response()->json($response);
        }
        $user = User::find($userId);

        if($request->profile_picture) {
            $png_url = "profile_".time().'.'.$request->profile_picture->extension();
            $request->profile_picture->move(storage_path('app/public/users/profile_pictures/'), $png_url);
            $user->profile_picture = 'storage/app/public/users/profile_pictures/' . $png_url;
        }

        // description  and country
        if(isset($request->mobile_number)) $user->mobile_number = $request->mobile_number;
        if(isset($request->email)) $user->email = $request->email;
        if(isset($request->country_code)) $user->country_code = $request->country_code;
        if(isset($request->restaurant_name)) $user->user_name = $request->restaurant_name;
        if(isset($request->city)) $user->city = $request->city;
        if(isset($request->state)) $user->state = $request->state;
        if(isset($request->zip)) $user->zip = $request->zip;
        if(isset($request->address)) $user->address = $request->address;
        if(isset($request->country)) $user->country = $request->country;
        if(isset($request->lat)) $user->lat = $request->lat;
        if(isset($request->lon)) $user->lon = $request->lon;
        if(isset($request->ssn)) $user->ssn = $request->ssn;
        if(isset($request->dob)) $user->dob = date('Y-m-d',strtotime($request->dob));
        $user->save();

        // Restaurant Information Save.
        $rest = Restaurant::where('user_id', $userId)->first();
        if($rest){
            if(isset($request->restaurant_name)) $rest->restaurant_name = $request->restaurant_name;
            // if(isset($request->continental)) $rest->continental = $request->continental;
            if(isset($request->opening_time)) $rest->opening_time = $request->opening_time;
            if(isset($request->closing_time)) $rest->closing_time = $request->closing_time;
            if(isset($request->land_mark)) $rest->land_mark = $request->land_mark;
            if(isset($request->restaurant_type)) $rest->restaurant_type = $request->restaurant_type;
            if(isset($request->description)) $rest->description = $request->description;

            // Multi vendor image upload.
            $files = $request->file('restaurant_picture');

            if($request->file('restaurant_picture'))
            {
                $destinationPath = storage_path('app/public/vendors/');
                foreach ($files as $file) {

                    $name = 'vendor_'.$file->getSize().time().'.'.$file->extension();
                    $file->move($destinationPath,$name);
                    $restaurantMedia = new RestaurantMedia;
                    $restaurantMedia->user_id = $user->id;
                    $restaurantMedia->restaurant_id = $rest->id;
                    $restaurantMedia->image = 'storage/app/public/vendors/'.$name;
                    $restaurantMedia->save();
                }
            }

            $rest->save();

            $continentals = $request->continental;
            RestaurantContinental::where(['restaurant_id' => $rest->id])->delete();
            foreach ($continentals as $continental) {
                $rContinental = RestaurantContinental::where(['restaurant_id' => $rest->id, 'continental_id' => $continental])->get();
                if (count($rContinental)==0) {
                    $restaurantContinental = new RestaurantContinental;
                    $restaurantContinental->restaurant_id = $rest->id;
                    $restaurantContinental->continental_id = $continental;
                    $restaurantContinental->save();
                }
            }
        }

        $user->restaurant->opening_time_full = $user->restaurant->opening_time;
        $date = date_create($user->restaurant->opening_time);
        $user->restaurant->opening_time = date_format($date,"h:i a");

        $user->restaurant->closing_time_full = $user->restaurant->closing_time;
        $date2 = date_create($user->restaurant->closing_time);
        $user->restaurant->closing_time = date_format($date2,"h:i a");

        $user->restaurant;
        $user->restaurant->restaurantContinental;
        $user->restaurantMedia;
        $response['status'] = "true";
        $response['message'] = 'Vendor Profile updated successfully.';
        $response['data'] = $user;
        return response()->json($response);
    }

    public function updateProfileDriver(Request $request) {
        $userStatus = JWTAuth::toUser(JWTAuth::getToken())->status;
        if ($userStatus == 0){
            return response()->json([
                'status' => "false",
                'message' => 'Your account is deactivated contact to administrator.',
                'data' => (object)[]
            ],401);
        }

        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $validator = Validator::make($request->all(), [
            'country_code'      => 'numeric|digits_between:0,5',
            'mobile_number' => 'numeric|digits_between:7,15|unique:users,mobile_number,'.$userId,
            'email' => 'email|unique:users,email,'.$userId,

        ]);

        if ($validator->fails()) {
            $response['status'] = "false";
            $response['message'] = $validator->errors()->first();
            $response['data'] = (object)[];
            return response()->json($response);
        }
        $user = User::find($userId);

        if($request->profile_picture) {
            $png_url = "profile_".time().'.'.$request->profile_picture->extension();
            $request->profile_picture->move(storage_path('app/public/users/profile_pictures/'), $png_url);
            $user->profile_picture = 'storage/app/public/users/profile_pictures/' . $png_url;
        }

        if(isset($request->mobile_number)) $user->mobile_number = $request->mobile_number;
        if(isset($request->email)) $user->email = $request->email;
        if(isset($request->country_code)) $user->country_code = $request->country_code;
        if(isset($request->user_name)) $user->user_name = $request->user_name;
        if(isset($request->city)) $user->city = $request->city;
        if(isset($request->state)) $user->state = $request->state;
        if(isset($request->zip)) $user->zip = $request->zip;
        if(isset($request->address)) $user->address = $request->address;
        if(isset($request->lat)) $user->lat = $request->lat;
        if(isset($request->lon)) $user->lon = $request->lon;
        if(isset($request->ssn)) $user->ssn = $request->ssn;
        if(isset($request->dob)) $user->dob = date('Y-m-d',strtotime($request->dob));
        $user->save();

        $driver_id = $userId;

        // Restaurant Information Save.
        $rest = Driver::where('user_id', $userId)->first();
        if($rest){
            if(isset($request->driving_licence_number)) $rest->driving_licence_number = $request->driving_licence_number;
            if(isset($request->driving_licence_expiry_date)) $rest->driving_licence_expiry_date = $request->driving_licence_expiry_date;
            if(isset($request->vehicle_registration_number)) $rest->vehicle_registration_number = $request->vehicle_registration_number;
            if(isset($request->vehicle_registration_expiry_date)) $rest->vehicle_registration_expiry_date = $request->vehicle_registration_expiry_date;
            if(isset($request->vehicle_insurance_number)) $rest->vehicle_insurance_number = $request->vehicle_insurance_number;
            if(isset($request->vehicle_insurance_expiry_date)) $rest->vehicle_insurance_expiry_date = $request->vehicle_insurance_expiry_date;
            if(isset($request->vehicle_make)) $rest->vehicle_make = $request->vehicle_make;
            if(isset($request->vehicle_model)) $rest->vehicle_model = $request->vehicle_model;
            if(isset($request->vehicle_year)) $rest->vehicle_year = $request->vehicle_year;
            if(isset($request->vehicle_color)) $rest->vehicle_color = $request->vehicle_color;

            // Upload driving licence
            if($request->driving_licence_image) {
                $png_url = $driver_id."_driving_licence_".time().'.'.$request->driving_licence_image->extension();
                $request->driving_licence_image->move(storage_path('app/public/drivers/'.$driver_id.'/licence/'), $png_url);
                $rest->driving_licence_image = 'storage/app/public/drivers/'.$driver_id.'/licence/' . $png_url;
            }

            // Upload vehicle registration Image
            if($request->vehicle_registration_image) {
                $png_url = $driver_id."_vehicle_registration_".time().'.'.$request->vehicle_registration_image->extension();
                $request->vehicle_registration_image->move(storage_path('app/public/drivers/'.$driver_id.'/vehicle_registration/'), $png_url);
                $rest->vehicle_registration_image = 'storage/app/public/drivers/'.$driver_id.'/vehicle_registration/' . $png_url;
            }

            // Upload vehicle insurance Image
            if($request->vehicle_insurance_image) {
                $png_url = $driver_id."_vehicle_insurance_".time().'.'.$request->vehicle_insurance_image->extension();
                $request->vehicle_insurance_image->move(storage_path('app/public/drivers/'.$driver_id.'/vehicle_insurance/'), $png_url);
                $rest->vehicle_insurance_image = 'storage/app/public/drivers/'.$driver_id.'/vehicle_insurance/' . $png_url;
            }

            $rest->save();
        }

        $user->driver;

        $response['status'] = "true";
        $response['message'] = 'Driver Profile updated successfully.';
        $response['data'] = $user;
        return response()->json($response);
    }

    /**
     * get counties list api
     * Field['']
     */
    public function countries()
    {

        $countries = Country::select('id','name','phonecode')->get();

        $response['status'] = "true";
        $response['message'] = 'Country retrieved successfully.';
        $response['data'] = $countries;
        return response()->json($response);
    }


    public function addRestaurant($request, $user_id)
    {
        $base_url =  url('/');

        /*try
        {*/
            // Restaurant Information Save.
            $restaurant = new Restaurant;
            $restaurant->user_id = $user_id;
            if(isset($request->restaurant_name)) $restaurant->restaurant_name = $request->restaurant_name;
            if(isset($request->continental)) $restaurant->continental = $request->continental;
            if(isset($request->opening_time)) $restaurant->opening_time = $request->opening_time;
            if(isset($request->closing_time)) $restaurant->closing_time = $request->closing_time;
            if(isset($request->land_mark)) $restaurant->land_mark = $request->land_mark;
            if(isset($request->restaurant_type)) $restaurant->restaurant_type = $request->restaurant_type;
            if($request->restaurant_picture) {
                $png_url = "vendors_".time().'.'.$request->restaurant_picture->extension();
                $request->restaurant_picture->move(storage_path('app/public/vendors/'), $png_url);
                $restaurant->restaurant_picture = 'storage/app/public/vendors/' . $png_url;
            } else {
                $restaurant->restaurant_picture = 'public/img/default-vendor.jpg' ;
            }

            $restaurant->save();

            return $restaurant;
        /*} catch (\Exception $e) {
            return $e->getLine() . ' : ' . $e->getMessage();
        } */

    }


    public function addDriver($request, $driver_id)
    {
        $base_url =  url('/');
        /*try
        {*/
            // Driver Information Save.
            $driver = new Driver;
            $driver->user_id = $driver_id;
           if(isset($request->driving_licence_number)) $driver->driving_licence_number = $request->driving_licence_number;
            if(isset($request->driving_licence_expiry_date)) $driver->driving_licence_expiry_date = $request->driving_licence_expiry_date;
            if(isset($request->vehicle_registration_number)) $driver->vehicle_registration_number = $request->vehicle_registration_number;
            if(isset($request->vehicle_registration_expiry_date)) $driver->vehicle_registration_expiry_date = $request->vehicle_registration_expiry_date;

            if(isset($request->vehicle_insurance_number)) $driver->vehicle_insurance_number = $request->vehicle_insurance_number;
            if(isset($request->vehicle_insurance_expiry_date)) $driver->vehicle_insurance_expiry_date = $request->vehicle_insurance_expiry_date;

            if(isset($request->vehicle_make)) $driver->vehicle_make = $request->vehicle_make;
            if(isset($request->vehicle_model)) $driver->vehicle_model = $request->vehicle_model;
            if(isset($request->vehicle_year)) $driver->vehicle_year = $request->vehicle_year;
            if(isset($request->vehicle_color)) $driver->vehicle_color = $request->vehicle_color;

            // Upload driving licence
            // if($request->driving_licence_image) {
                $file = $request->file('driving_licence_image');
                if($request->file('driving_licence_image'))
                {
                    $destinationPath = storage_path('app/public/drivers/'.$driver_id.'/licence/');
                    $name = $driver_id."_driving_licence_".time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$name);
                    $driver->driving_licence_image = 'storage/app/public/drivers/'.$driver_id.'/licence/' . $name;
                }
            // }

            // Upload vehicle registration Image
            // if($request->vehicle_registration_image) {
                $file = $request->file('vehicle_registration_image');
                if($request->file('vehicle_registration_image'))
                {
                    $destinationPath = storage_path('app/public/drivers/'.$driver_id.'/vehicle_registration/');
                    $name = $driver_id."_vehicle_registration_".time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$name);
                    $driver->vehicle_registration_image = 'storage/app/public/drivers/'.$driver_id.'/vehicle_registration/' . $name;
                }
            // }

            // Upload vehicle insurance Image
            // if($request->vehicle_insurance_image) {
                $file = $request->file('vehicle_insurance_image');
                if($request->file('vehicle_insurance_image'))
                {
                    $destinationPath = storage_path('app/public/drivers/'.$driver_id.'/vehicle_insurance/');
                    $name = $driver_id."_vehicle_insurance_".time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$name);
                    $driver->vehicle_insurance_image = 'storage/app/public/drivers/'.$driver_id.'/vehicle_insurance/' . $name;
                }
            // }
            $driver->driver_code = "FSD".$driver_id;
            $driver->save();

            return $driver;
        /*} catch (\Exception $e) {
            return $e->getLine() . ' : ' . $e->getMessage();
        } */

    }


    public function getContinentals(Request $request) {

        if($request->isMethod('post')){
            try{
                $userStatus = JWTAuth::toUser(JWTAuth::getToken())->status;
                if ($userStatus == 0){
                    return response()->json([
                        'status' => "false",
                        'message' => 'Your account is deactivated contact to administrator.',
                        'data' => (object)[]
                    ],401);
                }
                // dd(JWTAuth::toUser(JWTAuth::getToken()));
                $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

                $continental = Continental::orderBy('id','desc')->get();
                $response['status'] = "true";
                $response['message'] = "Continental data.";
                $response['data'] = $continental;
            } catch (\Exception $e) {
                $response['status'] = "false";
                $response['message'] = $e->getMessage();
                $response['data'] = (object)[];
            }

            return response()->json($response);
        }
    }
    public function agreement_document($type)
    {
        if($type=='driver')
        $slug = 'driver-agreement';
        else
        $slug = 'vendor-agreement';

        $cms = Cms::where(['slug'=>$slug])->first();
        $response['status'] = "true";
        $response['message'] = 'Agreement details.';
        $response['data'] = $cms;
        return response()->json($response);

    }
    public function privacy_policy()
    {

        $slug = 'privacy-policy';

        $cms = Cms::where(['slug'=>$slug])->first();
        $response['status'] = "true";
        $response['message'] = 'Privacy policy details.';
        $response['data'] = $cms;
        return response()->json($response);

    }
    public function terms_and_condition()
    {
        $slug = 'terms-and-condition';

        $cms = Cms::where(['slug'=>$slug])->first();
        $response['status'] = "true";
        $response['message'] = 'Terms and condition details.';
        $response['data'] = $cms;
        return response()->json($response);

    }
















    ////////////////////////////////////////////
}
