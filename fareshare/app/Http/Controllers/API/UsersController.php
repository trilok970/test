<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GroupMember;
use App\Models\PollRestaurants;
use App\User;
use Validator;
use JWTAuth;
use Hash;
use App\Models\UserAddress;
use App\Models\UserBank;
use App\Models\UserGroup;
use App\Models\UserGroupPoll;
use App\Models\UserSearch;
use App\Models\Order;
use Exception;
use App\Http\Controllers\Admin\UserFcmTokenController;
use App\Models\Cart;
use App\Models\Item;
use App\Models\PollVote;
use App\Models\Setting;
use App\Models\Wallet;
use App\Models\WalletPercentage;
use App\Models\WalletTransaction;
use Illuminate\Support\Facades\DB;
use Svg\Tag\Group;
use File;
use Response;
class UsersController extends Controller
{

    public $fcm;

    public function __construct()
    {
        $this->api_per_page = config('constants.api_per_page');
        $this->fcm = new UserFcmTokenController;
    }

    public function addAddress(Request $request) {
    	$validator = Validator::make($request->all(), [
            'location' => 'required',
            'house_number' => 'required',
            'landmark' => 'required',
            'address_type' => 'required',
            'lat' => 'required',
            'lon' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        if($request->is_default==1) {
           UserAddress::where('user_id',$userId)->update(['is_default'=>0]);
        }

        $userAddress = new UserAddress();
        $userAddress->location = $request->location;
        $userAddress->house_number = $request->house_number;
        $userAddress->landmark = $request->landmark;
        $userAddress->address_type = $request->address_type;
        $userAddress->lat = $request->lat;
        $userAddress->lon = $request->lon;
        $userAddress->user_id = $userId;
        $userAddress->is_default = $request->is_default ?? 0;
        $userAddress->save();



        $response['status'] = true;
        $response['message'] = "Address saved successfully.";
        $response['data'] = $userAddress;
    	return response()->json($response);
    }

    public function updateAddress(Request $request) {
    	$validator = Validator::make($request->all(), [
            'location' => 'required',
            'house_number' => 'required',
            'landmark' => 'required',
            'address_type' => 'required',
            'lat' => 'required',
            'lon' => 'required',
            'address_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $userAddress =  UserAddress::findOrFail($request->address_id);
        if($request->is_default==1) {
           UserAddress::where('user_id',$userId)->update(['is_default'=>0]);
        }

        $userAddress->location = $request->location;
        $userAddress->house_number = $request->house_number;
        $userAddress->landmark = $request->landmark;
        $userAddress->address_type = $request->address_type;
        $userAddress->lat = $request->lat;
        $userAddress->lon = $request->lon;
        $userAddress->is_default = $request->is_default ?? 0;
        $userAddress->user_id = $userId;
        $userAddress->save();



        $response['status'] = true;
        $response['message'] = "Address updated successfully.";
        $response['data'] = $userAddress;
    	return response()->json($response);
    }

    public function getAddresses() {
    	$userId = JWTAuth::toUser(JWTAuth::getToken())->id;
    	$addresses = UserAddress::where('user_id',$userId)->get();

    	$response['status'] = true;
        $response['message'] = "Address list.";
        $response['data'] = $addresses;
    	return response()->json($response);
    }

    public function addBank(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

        $validator = Validator::make($request->all(), [
            'bank_name' => 'required',
            'branch' => 'required',
            'account_no' => 'required',
            'ifsc_code' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }
        if($request->is_default==1) {
           UserBank::where('user_id',$userId)->update(['is_default'=>0]);
        }

        $bank = new UserBank();
        $bank->bank_name = $request->bank_name;
        $bank->branch = $request->branch;
        $bank->account_no = $request->account_no;
        $bank->ifsc_code = $request->ifsc_code;
        $bank->is_default = $request->is_default?? 0;
        $bank->status = 1;
        $bank->user_id = $userId;
        $bank->save();



        $banks = UserBank::where('user_id',$userId)->where('status','1')->get();
        $response['status'] = true;
        $response['message'] = "Bank detail added successfully.";
        $response['data'] = $banks;
        return response()->json($response);

    }

    public function getBank() {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

        $banks = UserBank::where('user_id',$userId)->where('status','1')->get();
        $response['status'] = true;
        $response['message'] = "Bank list.";
        $response['data'] = $banks;
        return response()->json($response);

    }

    public function updateBank(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

        $validator = Validator::make($request->all(), [
            'bank_id' => 'required',
            'bank_name' => 'required',
            'branch' => 'required',
            'account_no' => 'required',
            'ifsc_code' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }
        if($request->is_default==1) {
           UserBank::where('user_id',$userId)->update(['is_default'=>0]);
        }
        $bank =  UserBank::find($request->bank_id);
        $bank->bank_name = $request->bank_name;
        $bank->branch = $request->branch;
        $bank->account_no = $request->account_no;
        $bank->ifsc_code = $request->ifsc_code;
        $bank->is_default = $request->is_default;
        //$bank->status = $request->status;
        $bank->save();


        $banks = UserBank::where('user_id',$userId)->where('status','1')->get();
        $response['status'] = true;
        $response['message'] = "Bank detail updated successfully.";
        $response['data'] = $banks;
        return response()->json($response);

    }

    public function deleteBank(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

        $validator = Validator::make($request->all(), [
            'bank_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $bank =  UserBank::find($request->bank_id);
        $bank->status = 0;
        $bank->save();

        $banks = UserBank::where('user_id',$userId)->where('status','1')->get();
        $response['status'] = true;
        $response['message'] = "Bank detail deleted successfully.";
        $response['data'] = $banks;
        return response()->json($response);

    }

    public function deleteAddress(Request $request)
    {
        try {
            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

            $validator = Validator::make($request->all(), [
                'address_id' => 'required'
            ]);

            UserAddress::where(['id' => $request->address_id, 'user_id' => $userId,])->delete();
            $response['status'] = true;
            $response['message'] = "User Address deleted successfully.";
        } catch(Exception $e) {
            $response['status'] = false;
            $response['message'] = "Somting went wrong. Try again.";
        }
        return response()->json($response);
    }

    public function searchCustomer(Request $request) {
        $validator = Validator::make($request->all(), [
            'keyword' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $customers = User::select('users.*')->where('status', 1)
        ->join('user_roles as UR',function($join){
            $join->on('UR.user_id', '=', 'users.id')->where('UR.role_id', 2);
        })
        //->leftJoin('')
        ->where('user_name', 'Like', $request->keyword.'%')
        ->pluck('id');

        $orders = Order::whereIn('user_id',$customers)->with('user')->with('customer')->with('items')->get();
        if(count($orders) == 0) {
            $orders = Order::where('id',$request->keyword)->with('user')->with('customer')->with('items')->get();
        }
        $response['status'] = true;
        $response['message'] = "Search customer data.";
        $response['data'] = $orders;
        return response()->json($response);

    }


    public function createUserSearch(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $validator = Validator::make($request->all(), [
            'search_text' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $userSearch = UserSearch::firstOrCreate([
            'user_id' => $userId,
            'search_text' => $request->search_text,
        ]);

        $response['status'] = true;
        $response['message'] = "Search created successfully.";
        $response['data'] = $userSearch;
        return response()->json($response);
    }


    public function getRecentUserSearch(Request $request)
    {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

        $searchResults = UserSearch::where('user_id',$userId)->orderBy('updated_at','desc')->get();

        $response['status'] = true;
        $response['message'] = "User Recent Search";
        $response['data'] = $searchResults;
        return response()->json($response);
    }
    public function getContacts(Request $request){

        try{
            $validator = Validator::make($request->all(), [
                'contacts'=>'required',
             ]);
            if ($validator->fails())
            {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status'=>false,'message'=>$error]);
            }else{
                $input = $request->all();
                $user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

                if($request->group_id && $request->group_id > 0) {
                    $group = UserGroup::where('id',$request->group_id)->with(['members'=>function($q) {
                        $q->where('status','!=','Rejected');
                    }])->first();
                    if($group) {
                        $groupMemberIds = $group->members->pluck('user_id');
                    }
                }
                else {
                        $groupMemberIds = '';
                }
                $contacts = json_decode(stripslashes($input['contacts']),true);
                $data = json_encode(['Example 1','Example 2','Example 3',]);
                $fileName = time() . '_datafile.json';
                // File::put(public_path('/uploads/json/'.$fileName),json_encode($contacts));
                // return Response::download(public_path('/uploads/jsonfile/'.$fileName));
                // exit;
                // dd($request->all());
                // echo "<pre>";
                // print_r($contacts);exit;
                $new_array = [];
                if(isset($contacts) && !empty($contacts)){
                    foreach($contacts as $con){
                        $is_user = 'NEW';
                        if(is_array($con['phone'])){

                            //$mystring = mb_substr($con['phone'][0],2,7);
                            //dd($mystring);
                            $ph1='';$ph2='';
                            $str = strlen($con['phone'][0]);

                            if(isset($con['phone'][0]) && strlen($con['phone'][0]) > 7){
                                //$ph1 = !empty($con['phone'][0])? mb_substr($con['phone'][0],3,10) : '';
                                $ph1 = !empty($con['phone'][0])? substr($con['phone'][0], -7) : '';
                                //$ph2 = !empty($con['phone'][1])? mb_substr($con['phone'][1],3,10) : '';
                                $ph2 = !empty($con['phone'][1])? substr($con['phone'][1], -7) : '';

                                //dd($ph1);
                                if($ph2==''){
                                    $checkuser = User::select('id','profile_picture','user_name')->where('mobile_number','LIKE','%'.$ph1.'%')->where('role','user')
                                    ->when($groupMemberIds,function($q) use ($groupMemberIds) {
                                        $q->whereNotIn('id',$groupMemberIds);
                                    })
                                    ->first();
                                }else{
                                    $checkuser = User::select('id','profile_picture','user_name')->where('mobile_number','LIKE','%'.$ph1.'%')->orWhere('mobile_number','LIKE','%'.$ph2.'%')->where('role','user')
                                    ->when($groupMemberIds,function($q) use ($groupMemberIds) {
                                        $q->whereNotIn('id',$groupMemberIds);
                                    })
                                    ->first();
                                }
                            }
                            //$checkuser = User::select('id','profile_picture')->whereRaw('CONCAT("+",country_code,phone_number)',$con['phone'])->first();
                        }else{
                            $ph1 = !empty($con['phone'])? substr($con['phone'], -7) : '';
                            // echo $ph1;die;
                            $checkuser = User::select('id','profile_picture','user_name')->where('mobile_number','LIKE','%'.$ph1.'%')->where('role','user')
                            ->when($groupMemberIds,function($q) use ($groupMemberIds) {
                                $q->whereNotIn('id',$groupMemberIds);
                            })
                            ->first();


                        }
                        //dd($checkuser);
                        //$checkcount = User::select('id')->where('country_code',$con->country_code)->where('phone_number',$con->phone_number)->count();
                        // print_r($checkuser);die;
                        if(isset($checkuser) && !empty($checkuser)){

                          $is_user = 'USER';
                        }

                        //$users = new Contacts();
                        $user_name = "";
                        $profile_picture = "";
                        if($is_user == "USER"){
                             $user_name = (isset($checkuser->user_name) && !empty($checkuser->user_name))? $checkuser->user_name : '';
                             $profile_picture = !empty($checkuser->profile_picture)? $checkuser->profile_picture : '';
                        }else{
                             $user_name = (isset($con['name']) && !empty($con['name']))? $con['name'] : '';
                             $profile_picture = (isset($con['profile_picture']) && !empty($con['profile_picture']))? $con['profile_picture'] : '';
                        }
                        $new_array['contacts'][]= [

                                    "user_id" => (isset($checkuser->id) && !empty($checkuser->id))? $checkuser->id : "",
                                    "user_name" => $user_name,
                                    "mobile_number" => $con['phone'],
                                    "profile_picture" => $profile_picture,
                                    "is_user" =>  $is_user
                                ];
                                    // print_r($new_array);die;

                    }//end foreach
                }
                return response()->json(['status' => true, 'message' => "Contacts",'data'=>$new_array]);

            }
        }
        catch(\Exception $e){
            return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
       }

    }


    public function createGroup(Request $request) {
        try {
            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

            $validator = Validator::make($request->all(), [
                'group_name' => 'required',
                'participants_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }
            DB::beginTransaction();

            $userGroup = UserGroup::create([
                'group_name' => $request->group_name,
                'creator_id' => $userId,
                'status' => 'GROUP_CREATED'
            ]);
            $request_members = $request->participants_id;

            $members = [['user_id' => $userId,'status' => 'Accepted','payment_method' => 'Wallet','payment_method_status' => 'Accepted']];
            foreach($request_members as $member) {
                array_push($members,['user_id' => $member]);
            }

            $members = $userGroup->members()->createMany($members);

            DB::commit();
            $userGroupDetail = UserGroup::where('id',$userGroup->id)->with('members.user')->first();
            $response['data'] = $userGroupDetail;
            $response['status'] = true;
            $response['message'] = "User Group Created Successfully";

            $message = config('constants.group_created');
            foreach ($members as $key => $value) {
            $this->fcm->sendNotification($value->user_id,"Group Created",$message,"group",$userGroup->id);

            }


        }catch(Exception $e) {
            $response['status'] = false;
            $response['message'] = "Something went wrong ".$e->getMessage();
        }
        return response()->json($response);
    }

    public function userGroupList(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $user = User::where('id',$userId)->first();

        $userGroups = $user->groups()->with(['group','group.members.user','group.members'=>function($q) {
            $q->where('status','!=','Rejected');
        }])->get();

        $response['status'] = true;
        $response['message'] = "User Groups List";
        $response['data'] = $userGroups;

        return response()->json($response);
    }


    public function deleteUserGroup(Request $request)
    {
        try {
            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
            $validator = Validator::make($request->all(), [
                'group_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }

            $groupId = $request->group_id;

            DB::transaction(function () use ($groupId) {
                GroupMember::where('group_id',$groupId)->delete();
                UserGroup::find($groupId)->delete();
            });

            $response['status'] = true;
            $response['message'] = "User Groups Deleted Successfully";

        } catch(Exception $e) {
            $response['status'] = false;
            $response['message'] = "Error!!! Something went wrong";
        }

        return response()->json($response);
    }

    public function updateGroupRequestStatus(Request $request)
    {
        try {
            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
            $validator = Validator::make($request->all(), [
                'group_id' => 'required',
                'status' => 'required|in:Accepted,Rejected'
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }

            $userGroup = GroupMember::where(['user_id' => $userId,'group_id' => $request->group_id])->first();
            if($userGroup) {
                if($request->status == 'Rejected') {
                    $groupCount = GroupMember::where(['group_id' => $request->group_id])->where('status','!=','Rejected')->count();
                    if($groupCount == 1) {
                        UserGroup::where('id', $request->group_id)->delete();
                        GroupMember::where('group_id', $request->group_id)->delete();
                        $response['status'] = true;
                        $response['data']  = $userGroup;
                        $response['message'] = "Group member status updated success";
                        return response()->json($response);
                        exit;
                    }

                }
                $userGroup->status = $request->status;
                $userGroup->save();

                $group = UserGroup::where('id', $request->group_id)->first();
                $userGroupPoll = UserGroupPoll::where('group_id',$request->group_id)->first();

                if(!empty($userGroupPoll)) {
                    $restaurants = $userGroupPoll->restaurants()->with('restaurantDetail')->get();
                    $groupMembers = GroupMember::where([
                        'group_id' => $userGroupPoll->group_id,
                        'status' => 'Accepted',
                    ])->with('user')->get();
                    $userGroupPoll->restaurants = $restaurants;
                    $userGroupPoll->members = $groupMembers;
                }

                $userGroup->group = $group;
                $userGroup->poll = $userGroupPoll;

                $response['status'] = true;
                $response['data']  = $userGroup;
                $response['message'] = "Group member status updated success";
            }
            else {
                $response['status'] = false;
                $response['data']  = [];
                $response['message'] = "You are no longer member of this group.";
            }
        }catch(Exception $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }
        return response()->json($response);
    }


    public function groupDetail(Request $request)
    {
        try {

            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
            $validator = Validator::make($request->all(), [
                'group_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }

            $groupId = $request->group_id;

            $userGroupDetail = UserGroup::where('id',$groupId)->with('members.user')->first();
            $userGroupPoll = UserGroupPoll::where('group_id',$groupId)->with('restaurants.restaurantDetail')->get();
            $userGroupDetail['restaurants'] = $userGroupPoll;
            return $userGroupDetail;
            $response['status'] = true;
            $response['message'] = "Group Data - Members and Restaurants";
        } catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = "Error!!! Something went wrong";
        }
        return response()->json($response);
    }

    public function addGroupMember(Request $request)
    {
        try {
            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

            $groupId = $request->group_id;
            $userIdToAdd = $request->other_user_id;

            $validator = Validator::make($request->all(), [
                'group_id' => 'required',
                'other_user_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }
            $members = [];
            foreach($request->other_user_id as $user_id) {
                $member = ['group_id' => $request->group_id,'user_id' => $user_id];
                array_push($members,$member);
            }
            $userGroup = UserGroup::where('id',$groupId)->first();
            $userGroup->members()->createMany($members);
            $response['status'] = true;
            $response['message'] = "Member added successfully";
        } catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = "Error!!! Something went wrong";
        }
        return response()->json($response);
    }

    public function deleteGroupMember(Request $request)
    {
        try {
            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

            $groupId = $request->group_id;
            $userIdToDelete = $request->other_user_id;

            $validator = Validator::make($request->all(), [
                'group_id' => 'required',
                'other_user_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }
            GroupMember::where([
                'group_id' => $request->group_id,
                'user_id' => $request->other_user_id
            ])->delete();
            $response['status'] = true;
            $response['message'] = "Member deleted successfully";
        } catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = "Error!!! Something went wrong";
        }
        return response()->json($response);
    }

    public function listGroupMembers(Request $request)
    {
        try {
            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

            $groupId = $request->group_id;

            $validator = Validator::make($request->all(), [
                'group_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }
            $groupMembers = GroupMember::where([
                'group_id' => $request->group_id,
            ])->where('status','!=','Rejected')->with('user')->get();
            $response['status'] = true;
            $response['message'] = "Member deleted successfully";
            $response['data'] = $groupMembers;
        } catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = "Error!!! Something went wrong";
        }
        return response()->json($response);
    }

    public function updateDefaultAddress(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'address_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }

            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
            $userAddress =  UserAddress::findOrFail($request->address_id);


            UserAddress::where('user_id',$userId)->update(['is_default'=>0]);


            $userAddress->is_default = 1;
            $userAddress->save();

            $response['status'] = true;
            $response['message'] = "Default Address updated successfully.";
            $response['data'] = $userAddress;
        } catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = "Error!!! Something went wrong";
        }
        return response()->json($response);
    }

    public function groupInvitationList(Request $request)
    {
        try {
            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
            $userGroups =  UserGroup::where('creator_id',$userId)->pluck('id')->toArray();
            $members = GroupMember::whereNotIn('group_id',$userGroups)->where(['user_id'=>$userId,'status' => 'Requested'])->with('group')->get();


            $response['status'] = true;
            $response['message'] = "Group invitation list";
            $response['data'] = $members;
        } catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = "Error!!! Something went wrong";
        }
        return response()->json($response);
    }

    public function checkGroupStatus(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'group_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }

            $userGroup = UserGroup::where('id',$request->group_id)->with(['members.user','members'=>function($q) {
                $q->where('status','!=','Rejected');
            }])->first();
            if(!empty($userGroup)) {
                $userGroupPoll = $userGroup->polls()->latest()->first();
                $userGroup->poll = $userGroupPoll;
            }
            if(!empty($userGroupPoll)) {
                $restaurants = $userGroupPoll->restaurants()->with('restaurantDetail')->get();
                $userGroupPoll->restaurants = $restaurants;

                $votes = $userGroupPoll->votes()->with('voted_user_info')->get();
                $userGroupPoll->votes = $votes;
            }
            $orderCount = Order::where(['user_group_id'=>$request->group_id])->where('status','!=','Delivered')
                    ->where('status','!=','Delivered')
                    ->where('status','!=','Decline')
                    ->where('status','!=','Order Picked up')
                    ->count();
            if($orderCount > 0) {
                $userGroup->order = 1;
            } else {
                $userGroup->order = 0;
            }
            $response['status'] = true;
            $response['message'] = "Group info with status";
            $response['data'] = $userGroup;
        } catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = "Error!!! Something went wrong ".$e->getMessage();
        }
        return response()->json($response);
    }

    public function selectPaymentMethod(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'group_id' => 'required',
        //     'payment_mode' => 'required'
        // ]);

        // if ($validator->fails()) {
        //     return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        // }
        try {
            $validator = Validator::make($request->all(), [
                'group_id' => 'required',
                'payment_mode' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }

            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;



            $member = GroupMember::where(['user_id' => $userId,'group_id' => $request->group_id])->first();
            $member->payment_method = $request->payment_mode;
            $member->save();

            $userGroup = UserGroup::where('id',$request->group_id)->with('members','members.user')->first();
            $user = User::find($userId);

            $message = "$user->user_name select $request->payment_mode method for payment";
            $this->fcm->sendNotification($userGroup->creator_id,"Payment Method",$message,"payment_method",$request->group_id);


            $response['status'] = true;
            $response['message'] = "Select payment method";
            $response['data'] = $userGroup;
        } catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = "Error!!! Something went wrong ".$e->getMessage();
        }
        return response()->json($response);
    }
    public function getDriverDeliveryFee() {
        return WalletPercentage::where(['role_id'=>4])->first()->percentage ?? 0;
    }
    public function getOrderTax() {
        return Setting::where(['slug'=>'tax'])->first()->value ?? 0;
    }
    public function cartGrandTotal($group_id) {
        $carts = Cart::where('group_id', $group_id)->get();
        $discount = 0;
        $tax = 0;
        $deliveryFee = 0;
        $cartTotalAmt = 0;
        $cartGrandTotalAmt = 0;
        foreach($carts as $cart) {
            $cartIds[] = $cart->id;
            $cartItems = $cart->cartItems;

            if($cartItems) {

                foreach ($cartItems as $key => $item) {
                    $itemDetail = Item::where('id',$item->item_id)->first();
                    if($itemDetail->coupon_code_percentage > 0) {
                        $discount += (($itemDetail->item_price * $itemDetail->coupon_code_percentage)/100);
                        $item->price = $itemDetail->item_price - (($itemDetail->item_price * $itemDetail->coupon_code_percentage)/100);
                    }

                    $sub_total = ($item->qty * $item->price);

                    $cartTotalAmt += $sub_total ?? 0;

                }
            }
        }
        if($this->getOrderTax() > 0) {
            $tax = (($cartTotalAmt * $this->getOrderTax())/100);
        }
        // echo $cartTotalAmt;
        // echo "<br>". $tax;
        // exit;
        if($this->getDriverDeliveryFee() > 0 && $cartTotalAmt > 0) {
            $deliveryFee = $this->getDriverDeliveryFee();
        }
        $cartGrandTotalAmt = $cartTotalAmt + $tax + $deliveryFee;

        return array("total"=>$cartTotalAmt,"grand_total"=>$cartGrandTotalAmt,"tax"=>$tax,"delivery_fee"=>$deliveryFee);
    }
    public function paymentModeDetails(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'group_id' => 'required',
                'poll_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }
            $poll_id = $request->poll_id;
            $pollMembers = PollVote::where('poll_id',$poll_id)->pluck('user_id')->toArray();
            $userGroup = UserGroup::where('id',$request->group_id)->with('members','members.user')
            ->whereHas('members',function($q) use ($pollMembers){
                $q->whereIn('user_id',$pollMembers);
            })->first();
            $cartRes = Cart::where('group_id',$request->group_id)->with('restaurant')->first();
            $orderCount = Order::where(['user_group_id'=>$request->group_id])->where('status','!=','Delivered')
                    ->where('status','!=','Delivered')
                    ->where('status','!=','Decline')
                    ->where('status','!=','Order Picked up')
                    ->count();

            $cartGrandTotalArray = $this->cartGrandTotal($request->group_id);
            $userGroup->total = $cartGrandTotalArray['total'];
            $userGroup->grand_total = $cartGrandTotalArray['grand_total'];
            $userGroup->tax = $cartGrandTotalArray['tax'];
            $userGroup->delivery_fee = $cartGrandTotalArray['delivery_fee'];
            $userGroup->restaurant = $cartRes->restaurant ?? "";
            if($orderCount > 0) {
                $userGroup->order = 1;
            } else {
                $userGroup->order = 0;
            }
            $response['status'] = true;
            $response['message'] = "Payment Mode Details";
            $response['data'] = $userGroup;
        } catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = "Error!!! Something went wrong ".$e->getMessage();
        }
        return response()->json($response);
    }

    public function confirmCOD(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'group_id' => 'required',
                'other_user_id' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }

            $member = GroupMember::where(['user_id' => $request->other_user_id,'group_id' => $request->group_id])->first();
            if($member) {
                $member->payment_method_status = "Accepted";
                $member->payment_status = "Paid";
                $member->save();
                $userGroup = UserGroup::where('id',$request->group_id)->with('members','members.user')->first();
                $response['status'] = true;
                $response['message'] = "Confirm COD";
                $response['data'] = $userGroup;
            }
            else {
                $response['status'] = true;
                $response['message'] = "Group member not found.";
                $response['data'] = [];
            }
        } catch (Exception $e) {
                $response['status'] = false;
                $response['message'] = "Error!!! Something went wrong ".$e->getLine()." ".$e->getMessage();
            }
            return response()->json($response);
        }
        public function payMoneyToGroupAdmin(Request $request)
        {
            try {
                $validator = Validator::make($request->all(), [
                    'group_id' => 'required|exists:user_groups,id',
                ]);

                if ($validator->fails()) {
                    return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
                }
                $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

                $userGroup = UserGroup::where('id',$request->group_id)->first();
                if($userGroup) {
                    $userCart = Cart::where(['user_id'=>$userId,'group_id'=>$request->group_id])->first();
                    if($userCart) {
                        // Amount debited from user wallet
                        $groupMember = GroupMember::where(['group_id'=>$request->group_id,'user_id'=>$userId])->first();
                        if(strtolower($groupMember->payment_method) == 'wallet') {
                            $user = $this->get_user_details($userId);
                            $with_msg = "Order amount $userCart->grand_total is debited and credited to group owner wallet.";
                            $this->walletTransactions($user->id,$user->dwolla_customer_id ?? "",$userCart->grand_total,$with_msg,"debit");

                             // Amount credited to group admin wallet
                             $groupAdmin = $this->get_user_details($userGroup->creator_id);
                             $with_msg = "Order amount $userCart->grand_total received by  ".ucwords($user->user_name)."";
                             $this->walletTransactions($userGroup->creator_id,$groupAdmin->dwolla_customer_id ?? "",$userCart->grand_total,$with_msg,"credit");

                             $groupMember->payment_status = "Paid";
                             $groupMember->payment_amount = $userCart->grand_total;
                             $groupMember->save();
                        }else{
                            $groupMember->status = "Order Placed";
                            
                             $groupMember->save();                            
                        }

                         $response['status'] = true;
                         $response['message'] = "Payment done successfully";
                         $response['data'] = $userGroup;
                         return response()->json($response);
                         exit;
                        }
                        else {
                            $response['status'] = false;
                            $response['message'] = "User cart not found";
                            $response['data'] = $userGroup;
                            return response()->json($response);
                            exit;
                        }
                }
                else {
                    $response['status'] = false;
                    $response['message'] = "There is no user group found.";
                    $response['data'] = $userGroup;
                    return response()->json($response);
                    exit;
                }

            } catch (Exception $e) {
                $response['status'] = false;
                $response['message'] = "Error!!! Something went wrong ".$e->getMessage();
            }
        }
        public function walletTransactions($userId,$stripe_token,$wallet_amount,$message,$transaction_type)
        {
                $wallet = Wallet::where(['user_id'=>$userId])->first();
                if($wallet)
                    $wallet = $wallet;
                else
                    $wallet = new Wallet();


                if($transaction_type=='debit')
                $amount = (float)($wallet->amount ?? 0) - (float)$wallet_amount;
                else if($transaction_type=='credit')
                $amount = (float)($wallet->amount ?? 0) + (float)$wallet_amount;


                $wallet->user_id = $userId;
                $wallet->amount = $amount;
                $wallet->save();

                $WalletTransaction = new WalletTransaction();

                $WalletTransaction->user_id = $userId;
                $WalletTransaction->wallet_id = $wallet->id;
                if($transaction_type=='debit')
                $WalletTransaction->paytype = -$wallet_amount;
                else
                $WalletTransaction->paytype = $wallet_amount;
                $WalletTransaction->stripe_token = $stripe_token;
                $WalletTransaction->comment = $message;
                $WalletTransaction->transaction_type = $transaction_type;
                $WalletTransaction->save();
        }
        public function get_user_details($user_id)
        {
            return User::find($user_id);
        }

        public function notification_enable(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'notification' => 'required|numeric|min:0|max:1',
            ]);
            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }
            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
            if($userId)
            {
                $user = User::find($userId);
                $user->notification = $request->notification;
                $user->save();

                if($request->notification==1)
                $type = "enable";
                else $type = "disable";

                $response['status'] = true;
                $response['message'] = "Notification $type";
                $response['data'] = $user;
                return response()->json($response);
            }


        }

        public function codEnable(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'cod' => 'required|numeric|min:0|max:1',
            ]);
            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }
            $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
            if($userId)
            {
                $user = User::find($userId);
                $user->cod = $request->cod;
                $user->save();

                if($request->cod==1)
                $type = "enabled";
                else $type = "disabled";

                $response['status'] = true;
                $response['message'] = "Cod $type successfully";
                $response['data'] = $user;
                return response()->json($response);
            }


        }
        ////////////////////////////////////////////////////////////////////////////
}
