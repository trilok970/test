<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Session;
class SettingController extends Controller
{

    public function settings(){
        try {
            $setting_field_arr = config('constants.setting_field_arr');
            $site_settings = Setting::whereIn('slug', $setting_field_arr)->get();
            // echo "<pre>";
            // print_r($site_settings);
            // exit;


            $page_title = 'Settings';



            return view('admin.settings.settings',compact('page_title', 'site_settings'));
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back()->withInput();
        }
    }// end function.


    public function settingsUpdate(Request $request){
        try {
            $data = $request->all();

            $setting_field_arr = config('constants.setting_field_arr');
            $settings_sess = Session::get('settings');
            $site_settings_types = Setting::whereIn('slug', $setting_field_arr)
            ->pluck('type', 'slug')
            ->toArray();
            // echo "<pre>";
            // print_r($data);
            // exit;
            foreach($setting_field_arr as $slug){
                if(isset($data[$slug])) {
                    $value_save = null;
                    // if($site_settings_types[$slug] == 'FILE') {

                    //     if ($request->file($slug)) {
                    //         $destinationPath = '/uploads/others/';
                    //         $response_data = Uploader::doUpload($request->file($slug), $destinationPath);
                    //         if ($response_data['status'] == true) {
                    //             $value_save = $response_data['file'];
                    //         }
                    //     }
                    // } else {
                    //     $value_save = $data[$slug];
                    // }
                    $value_save = $data[$slug];

                    if($value_save !== null){
                        Setting::where('slug', $slug)->update(['value'=>$value_save]);
                        if(isset($settings_sess[$slug])){
                            $settings_sess[$slug] = $value_save;
                        }
                    }

                }
            }
            Session::put('settings', $settings_sess);

            Session::flash('alert-success', 'Settings has been updated succesfully.');
            // return redirect()->route('admin.dashboard');
            return redirect()->back();

        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('warning', $msg);
            return redirect()->back()->withInput();
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
