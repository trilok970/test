<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Order;
use App\Models\Driver;
use App\Models\Restaurant;
use View;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('agreement_document','text');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $page_title = 'Dashboard';
        $count_user = User::where('role','user')->count();
        $count_order = Order::count();
        $count_driver = Driver::count();
        $count_restaurant = Restaurant::count();
        return view('admin.dashboard', compact('page_title','count_user','count_restaurant','count_driver','count_order'));
    }
    public function agreement_document($type)
    {
        if($type=='driver')
        {
            $page_title = 'Driver Agreement Document';
            return view('driver-agreement-document', compact('page_title'));
        }
        else
        {
            $page_title = 'Vendor Agreement Document';
            return view('vendor-agreement-document', compact('page_title'));

        }

    }
    public function text()
    {
        $page_title = 'Driver Agreement Document';

        $user = User::find(79);
        $mpdf = new \Mpdf\Mpdf();
        $html = view('admin.drivers.driver-agreement', compact('user','page_title'))->render();
        // $view = View::make('admin.drivers.driver-agreement', [
        //     'user' => 'Hello World !',
        //     'page_title' =>  $page_title,
        // ]);

        // $html = $view->render();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
exit;
        // return view('admin.drivers.driver-agreement', compact('page_title'));
        // return view('text', compact('page_title'));

    }
}
