<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\WalletPercentage;
use App\Models\Role;
use Illuminate\Http\Request;
use Validator;
class WalletPercentageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->sortable_columns = [
            0 => 'id',
            1 => 'role_id',
            2 => 'percentage',
        ];
    }

	public function index(Request $request) {
		if($request->ajax())
        {
            $totalCms       = WalletPercentage::count();
            $limit          = $request->input('length');
            $start          = $request->input('start');
            $search         = $request['search']['value'];
            $orderby        = $request['order']['0']['column'];
            $order          = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw           = $request['draw'];

            $response       = WalletPercentage::getWalletPercentageModel($limit , $start , $search, $this->sortable_columns[$orderby], $order);

            if(!$response){
                $page       = [];
                $paging     = [];
            }
            else{
                $page       = $response;
                $paging     = $response;
            }

            $data = array();

            foreach ($page as $wallet) {
            	$u['id']            = ($wallet->id);
                $u['role_id']       = ucwords($wallet->role->role);
                if($wallet->role_id==4)
                $u['percentage']    = " $".$wallet->percentage;
                else
                $u['percentage']    = $wallet->percentage;

                $u['created_at']    = date('d/m/Y', strtotime($wallet->created_at));

                $status         = view('admin.wallet-percentage.status', ['id' => $wallet->id, 'status' => $wallet->status]);
                $u['status']        = $status->render();

                $actions            = view('admin.wallet-percentage.actions', ['id' => $wallet->id, 'status' => $wallet->status ]);
                $u['actions']       = $actions->render();

                $data[] = $u;

                unset($u);
            }
// echo "<pre>";
// print_r($data);
// exit;
            $return = [
                "draw"              =>  intval($draw),
                "recordsFiltered"   =>  intval( $totalCms),
                "recordsTotal"      =>  intval( $totalCms),
                "data"              =>  $data
            ];
            return $return;
        }

		$page_title = 'Wallet Percentage List';
        return view('admin.wallet-percentage.index', compact('page_title'));
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::whereIn('id',[1,4])->pluck('role','id');
		$page_title = 'Create Wallet Percentage';
       // return view('admin.wallet-percentage.create', compact('page_title', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $walletpercentage = new WalletPercentage;

        $validator = Validator::make($request->all(), [
            'percentage'  =>   'required|numeric|min:1|max:100',
            'role_id'  =>   'required|in:1,4|numeric|min:1|max:4|unique:wallet_percentages,role_id,'.$walletpercentage->id,

        ],
        [
            'percentage.required' => "The amount feild is required.",
            'percentage.numeric' => "The amount must be a number.",
            'percentage.min' => "The amount must be at least 1.",
            'percentage.max' => "The amount may not be greater than 100.",
        ]);

        if($validator->fails()) {
            $errors = $validator->errors();

            $request->session()->flash('alert-danger', 'Errors! Please correct the following errors and submit again.');
            return back()->withErrors($errors)->withInput();
        }
		$walletpercentage->role_id = $request->role_id ?? 0;
		$walletpercentage->percentage = $request->percentage;
		$walletpercentage->save();

		$request->session()->flash('alert-success', 'Wallet percentage created successfully.');
		return redirect('/admin/walletpercentage');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WalletPercentage  $walletPercentage
     * @return \Illuminate\Http\Response
     */
    public function show(WalletPercentage $walletPercentage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WalletPercentage  $walletPercentage
     * @return \Illuminate\Http\Response
     */
    public function edit(WalletPercentage $walletpercentage)
    {
        $roles = Role::whereIn('id',[1,4])->pluck('role','id');
		$page_title = 'Create Wallet Percentage';
        return view('admin.wallet-percentage.edit', compact('page_title', 'roles','walletpercentage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WalletPercentage  $walletPercentage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WalletPercentage $walletpercentage)
    {

		$validator = Validator::make($request->all(), [
            'percentage'  =>   'required|numeric|min:1|max:100',
            'role_id'  =>   'required|in:1,4|numeric|min:1|max:4|unique:wallet_percentages,role_id,'.$walletpercentage->id,

        ],
        [
            'percentage.required' => "The amount feild is required.",
            'percentage.numeric' => "The amount must be a number.",
            'percentage.min' => "The amount must be at least 1.",
            'percentage.max' => "The amount may not be greater than 100.",
        ]);

        if($validator->fails()) {
            $errors = $validator->errors();


            $request->session()->flash('alert-danger', 'Errors! Please correct the following errors and submit again.');
            return back()->withErrors($errors)->withInput();
        }
		$walletpercentage->role_id = $request->role_id ?? 0;
		$walletpercentage->percentage = $request->percentage;
		$walletpercentage->save();

		$request->session()->flash('alert-success', 'Wallet percentage updated successfully.');
		return redirect('/admin/walletpercentage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WalletPercentage  $walletPercentage
     * @return \Illuminate\Http\Response
     */
    public function destroy(WalletPercentage $walletPercentage)
    {
        //
    }
    /**
     * Active
     *
     * @param  int  $id
     */
    public function active($id)
    {

        $row = WalletPercentage::find($id);
        $row->status = 1;
        $row->save();

        return response()->json(array('success' => true));
    }

    /**
     * In-Active
     *
     * @param  int  $id
     */
    public function inActive($id)
    {

        $row = WalletPercentage::find($id);
        $row->status = 0;
        $row->save();

        return response()->json(array('success' => true));
    }
}
