<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserGroup;
use App\User;

class UserGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $totalCms       = UserGroup::count();
            $limit          = $request->input('length');
            $start          = $request->input('start');
            $search         = $request['search']['value'];
            $orderby        = $request['order']['0']['column'];
            $order          = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw           = $request['draw'];

            $response       = UserGroup::getUserGroupModel($limit , $start , $search, '', $order);

            if(!$response){
                $page       = [];
                $paging     = [];
            }
            else{
                $page       = $response;
                $paging     = $response;
            }

            $data = array();

            foreach ($page as $userGroup) {
            	$u['id']                =   $userGroup->id;
                $u['group_name']        =   $userGroup->group_name;
                $u['user_name']         =   $userGroup->user_name;
                $u['email']             =   $userGroup->email;
                $u['mobile']            =   $userGroup->mobile_number;
                $u['created_at']        =   date('d/m/Y', strtotime($userGroup->created_at));
                $u['actions']           =   '<a href="user-group/'.$userGroup->id.'/members" title="members"><i class="fas fa-users"></i></a>';

                $data[] = $u;

                unset($u);
            }
            $return = [
                "draw"              =>  intval($draw),
                "recordsFiltered"   =>  intval( $totalCms),
                "recordsTotal"      =>  intval( $totalCms),
                "data"              =>  $data
            ];
            return $return;
        }
        $page_title = 'User Groups List';
        return view('admin.user-groups.index', compact('page_title'));
    }

    public function members(Request $request,$id)
    {
        $page_title = 'Group members';
        $userGroup = UserGroup::where('id',$id)->with(['members.user','members'=>function($q) {
            $q->where('status','!=','Rejected');
        }])->first();
        if($userGroup) {
            $members = $userGroup->members()->with('user')->get();
            return view('admin.user-groups.members', compact('page_title','userGroup','members'));

        }
        else {
            $members = [];
        return view('404');

        }
    }
}
