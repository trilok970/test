<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use App\Models\UserDevice as fcmToken;
use App\Models\Notification;
class UserFcmTokenController extends Controller
{
    /**
     *
     * @var user
     * @var FCMToken
     */
    protected $user;
    protected $fcmToken;

    /**
     * Constructor
     *
     * @param
     */
    public function __construct()
    {

    }

     /**
     * Functionality to send notification.
     *
    */

    // public function sendNotification()
    public function sendNotification($user_id,$title,$description,$type,$order_id=null)
    {
    	// $user_id=79;$title="Test notification from laravel ".date("d-m-Y h:i:s");$description="test from web";
        $tokens = [];
        $apns_ids = [];
        $responseData = [];

        $this->notification_entry($user_id,$title,$description,$type,$order_id);


	// for Android

        if ($FCMTokenData = fcmToken::where('user_id',$user_id)->where('device_id','!=',null)->where('device_type','ANDROID')->select('device_id')->get())
        {
            foreach ($FCMTokenData as $key => $value)
            {
                $tokens[] = $value->device_id;

            }

            $msg = array('type'=>$type,'title' => $title,'body'  => $description,'subtitle' => 'This is a subtitle', "click_action" => "FCM_PLUGIN_ACTIVITY",

                  );

            $fields = array
                    (
                        'registration_ids'  => $tokens,
                        'notification'  => $msg
                    );


            $headers = array
                    (
                        'Authorization: key=' . env('FCM_SERVER_KEY'),
                        'Content-Type: application/json'
                    );

            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch );

            if ($result === FALSE)
            {
                die('FCM Send Error: ' . curl_error($ch));
            }
            $result = json_decode($result,true);
            // echo "<pre>";
            // print_r($result);
            // exit();
            // dd($result);
            $responseData['android'] =[
                       "result" =>$result
                    ];

            curl_close( $ch );

        }

	// for IOS

        if ($FCMTokenData = fcmToken::where('user_id',$user_id)->where('device_id','!=',null)->where('device_type','IOS')->select('device_id')->get())
        {
           foreach ($FCMTokenData as $key => $value)
            {
                $apns_ids[] = $value->token;
            }

            $url = "https://fcm.googleapis.com/fcm/send";

            $serverKey = env('FCM_SERVER_KEY');
            $title = $title;
            $body = $description;
            $notification = array('type'=>$type, 'title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');
            $arrayToSend = array('registration_ids' => $apns_ids, 'notification' => $notification,'priority'=>'high');
            $json = json_encode($arrayToSend);
            $headers = array();
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Authorization: key='. $serverKey;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            //Send the request
            $result = curl_exec($ch);

            if ($result === FALSE)
            {
                die('FCM Send Error: ' . curl_error($ch));
            }
            $result = json_decode($result,true);
            $responseData['ios'] =[
                        "result" =>$result
                    ];

            //Close request
            curl_close($ch);
        }
        // echo "<pre>";
        // print_r($responseData);
        // exit();
         return $responseData;

     }
     public function notification_entry($user_id,$title,$message,$type,$order_id)
     {
        $notification = new Notification;
        $notification->user_id = $user_id;
        $notification->title = $title;
        $notification->message = $message;
        $notification->type = $type;
        $notification->order_id = $order_id;
        $notification->save();
     }

}
