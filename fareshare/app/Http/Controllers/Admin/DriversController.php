<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\User;
use App\Models\Driver;

class DriversController extends Controller
{

	public function __construct()
    {
        $this->sortable_columns = [
            0 => 'id',
            1 => 'user_name',
            2 => 'email',
            3 => 'mobile_number',
            4 => 'address',
            5 => 'role',
        ];
    }

	public function index(Request $request) {
		if($request->ajax())
        {
            $totalCms       = Driver::count();
            $limit          = $request->input('length');
            $start          = $request->input('start');
            $search         = $request['search']['value'];
            $orderby        = $request['order']['0']['column'];
            $order          = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw           = $request['draw'];

            $response       = Driver::getDriverModel($limit , $start , $search, $this->sortable_columns[$orderby], $order);

            if(!$response){
                $page       = [];
                $paging     = [];
            }
            else{
                $page       = $response;
                $paging     = $response;
            }

            $data = array();

            foreach ($page as $driver) {
            	$u['id']            = ($driver->id);
                $u['user_name']     = ucwords($driver->user_name);
                $u['email']         = ($driver->email);
                $u['mobile_number'] = '+'.$driver->country_code .'-' . ($driver->mobile_number);
                $u['address']       = ($driver->address);
                $u['role']          = (ucfirst($driver->role));
                $u['created_at']    = date('d/m/Y', strtotime($driver->created_at));

                $status             = view('admin.drivers.status', ['id' => $driver->id, 'status' => $driver->status]);
                $u['status']        = $status->render();

                $actions            = view('admin.drivers.actions', ['id' => $driver->id, 'status' => $driver->status]);

                $u['actions']       = $actions->render();

                $data[] = $u;

                unset($u);
            }

            $return = [
                "draw"              =>  intval($draw),
                "recordsFiltered"   =>  intval( $totalCms),
                "recordsTotal"      =>  intval( $totalCms),
                "data"              =>  $data
            ];
            return $return;
        }

		$page_title = 'Drivers List';
        return view('admin.drivers.index', compact('page_title'));
	}

	/**
     * Active
     *
     * @param  int  $id
     */
    public function active($id)
    {
        $driver = User::find($id);
        $driver->status = '1';
        $driver->save();

        return response()->json(array('success' => true));
    }

    /**
     * In-Active
     *
     * @param  int  $id
     */
    public function inActive($id)
    {
        $driver = User::find($id);
        $driver->status = '0';
        $driver->save();

        return response()->json(array('success' => true));
    }

    public function detail($id) {
		$page_title = 'Driver Detail';
        $driver = User::where('id',$id)->with('driver')->first();

        return view('admin.drivers.detail', compact('page_title', 'driver'));
    }
    public function driver_agreement($id)
    {
        $user = User::find($id);
        $page_title = 'Driver Agreement Document';


        try {
            $mpdf = new \Mpdf\Mpdf([
              'tempDir' => __DIR__ . '/../tmp', // uses the current directory's parent "tmp" subfolder
              'setAutoTopMargin' => 'stretch',
              'setAutoBottomMargin' => 'stretch'
            ]);

        $html = view('admin.drivers.driver-agreement', compact('user','page_title'))->render();


        $mpdf->WriteHTML($html);
        $mpdf->Output();
          } catch (\Mpdf\MpdfException $e) {
              print "Creating an mPDF object failed with" . $e->getMessage();
          }
    }
/////////////////////////////////////////////////////////////////////
}
