<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Exception;

class PaymentController extends Controller
{
    public function check_braintree_customer() {
        $customer = User::where('email','trilok_kumar@ninehertzindia.com')->first();
        // echo "<pre>";
        // print_r($customer);
        // exit;
        if($customer && ($customer->braintree_customer_id == '' || $customer->braintree_customer_id == null)) {
            // using your customer id we will create
            // brain tree customer id with same id
            $response = \Braintree_Customer::create([
                'firstName' => $customer->user_name,
                'lastName' => '',
                'company' => 'Nine Hertz India',
                'email' => $customer->email,
                'phone' => $customer->mobile_number,
                'fax' => '419.555.1235',
                'website' => 'http://example.com'
            ]);

            // save your braintree customer id
            if( $response->success) {
                $customer->braintree_customer_id = $response->customer->id;
                $customer->save();
            }
        }
    }
     public function checkout(Request $request)
     {
      //  echo base64_decode("dG9rZW5jY19iaF94NjQ0ejlfdmJieG44X2R6ODRteF9iY3Fxdjdfc3k3");
        //  echo "<pre>";
        //  print_r($_POST);
        //  dd($request);
        //  exit;
         $this-> check_braintree_customer();
           // get your logged in customer
           $customer = User::where('email','trilok_kumar@ninehertzindia.com')->first();

           // when client hit checkout button
           if( $request->get('payment_method_nonce') )
           {
            //    echo "check";exit;
                // brain tree customer payment nouce
                $payment_method_nonce = $request->get('payment_method_nonce');

                // make sure that if we do not have customer nonce already
                // then we create nonce and save it to our database
                if ( !$customer->braintree_nonce )
                {
                      // once we recieved customer payment nonce
                      // we have to save this nonce to our customer table
                      // so that next time user does not need to enter his credit card details
                      $result = \Braintree_PaymentMethod::create([
                        'customerId' => $customer->braintree_customer_id,
                        'paymentMethodNonce' => $payment_method_nonce
                      ]);


                      // save this nonce to customer table
                      $customer->braintree_nonce = $result->paymentMethod->token;
                      $customer->save();
                }

                // process the customer payment
                $client_nonce = \Braintree_PaymentMethodNonce::create($customer->braintree_nonce);
                $result = \Braintree_Transaction::sale([
                     'amount' => 500,
                     'options' => [ 'submitForSettlement' => True,'storeInVaultOnSuccess' => true, ],
                     'paymentMethodNonce' => $client_nonce->paymentMethodNonce->nonce,
                    //  'customerId' =>  $customer->braintree_customer_id
                ]);

                // check to see if braintree has processed
                // our client purchase transaction
                if( !empty($result->transaction) ) {
                    // your customer payment is done successfully
                    echo "<pre>";
                    print_r($result);
                    exit;
                }
                else {
                    foreach($result->errors->deepAll() AS $error) {
                        echo($error->code . ": " . $error->message . "\n");
                    }
                }
           }

           return view('braintree', [
              'braintree_customer_id' => $customer->braintree_customer_id
           ]);
     }
     public function merchant() {
        $merchantAccountParams = [
            'individual' => [
              'firstName' => 'Jane',
              'lastName' => 'Doe',
              'email' => 'jane@14ladders.com',
              'phone' => '5553334444',
              'dateOfBirth' => '1981-11-19',
              'ssn' => '456-45-4567',
              'address' => [
                'streetAddress' => '111 Main St',
                'locality' => 'Chicago',
                'region' => 'IL',
                'postalCode' => '60622'
              ]
            ],
            'business' => [
              'legalName' => 'Jane\'s Ladders',
              'dbaName' => 'Jane\'s Ladders',
              'taxId' => '98-7654321',
              'address' => [
                'streetAddress' => '111 Main St',
                'locality' => 'Chicago',
                'region' => 'IL',
                'postalCode' => '60622'
              ]
            ],
            'funding' => [
              'descriptor' => 'Blue Ladders',
              'destination' => \Braintree\MerchantAccount::FUNDING_DESTINATION_BANK,
              'email' => 'funding@blueladders.com',
              'mobilePhone' => '5555555555',
              'accountNumber' => '1123581321',
              'routingNumber' => '071101307'
            ],
            'tosAccepted' => true,
            'masterMerchantAccountId' => "14ladders_marketplace",
            'id' => "blue_ladders_store"
          ];
            $client_id = env('BTREE_CLIENT_ID');
            $client_secret = env('BTREE_CLIENT_SECRET');
            $gateway = new \Braintree\Gateway([
                'environment' => env('BTREE_ENVIRONMENT'),
                'merchantId' => env('BTREE_MERCHANT_ID'),
                'publicKey' => env('BTREE_PUBLIC_KEY'),
                'privateKey' => env('BTREE_PRIVATE_KEY')
            ]);

          $result = $gateway->merchantAccount()->create($merchantAccountParams);
          echo "<pre>";
          print_r($result->errors->deepAll());
     }
     public function adyen() {
        //  try {
             $client = new \Adyen\Client();

             $client->setXApiKey("AQE8gXvdQNmbKVEb5WOzk3A9h8eURZtMA4oEdmxYw1CujHJRt8lyGcdRECNNGvQl4XUgqBlDq+duw4/6toBCEMFdWw2+5HzctViMSCJMYAc=-uTGzr6yRvwv5gwOntBoFOpoVkzFJGSff6cqTAXULeWI=-)b(,Xr5.W4=fQQCb");


            // $client->setUsername("admin");
            // $client->setPassword("Ninehertz@970");
            $client->setEnvironment(\Adyen\Environment::TEST);
            $client->setTimeout(30);

             $service = new \Adyen\Service\Checkout($client);

             $json = '{
                   "card": {
                     "encryptedCardNumber" => "test_4111111111111111",
                     "encryptedExpiryMonth" => "test_03",
                     "encryptedExpiryYear" => "test_2030",
                     "encryptedSecurityCode" => "test_737"
                     "holderName": "John Smith"
                   },
                   "amount": {
                     "value": 1500,
                     "currency": "EUR"
                   },
                   "reference": "payment-test",
                   "merchantAccount": "NineHertzIndiaPvtLtdECOM"
             }';

             $params = json_decode($json, true);

             $result = $service->payments($params);
             echo "<pre>";
             print_r($result);
        //  }
        //  catch(Exception $e) {
        //      echo "Error: ".$e->getMessage();
        //  }
     }

     public function dwolla() {
      $key = "wLTixjtrZ3eVxZtTYHSyqD1wuT5YFNB4P8Ezi5OLGPZPADLDRg";
      $secret = "FcuVLx5OpXiwH54fd63wmRLv1Sdif3bCvvSSHM7iSLsIGw1Duc";
      $timestamp = time(); // for current time stamp
      $order_id = rand(999999,1000); // any unique order id
      $signature = hash_hmac('sha1', "{$key}&{$timestamp}&{$order_id}", $secret);
      $callback_url = url('dwolla-callback');
      $redirect_url = url('dwolla-redirect');
      return view('dwolla1',compact('key','secret','timestamp','order_id','callback_url','redirect_url','signature'));
     }
    public function verifyGatewaySignature($proposedSignature, $checkoutId, $amount) {
        $signature = hash_hmac("sha1", "{$checkoutId}&{$amount}", $secret);
        return $signature == $proposedSignature;
    }
    public function dwolla_callback() {
        $key = "wLTixjtrZ3eVxZtTYHSyqD1wuT5YFNB4P8Ezi5OLGPZPADLDRg";
        $secret = "FcuVLx5OpXiwH54fd63wmRLv1Sdif3bCvvSSHM7iSLsIGw1Duc";



        if (array_key_exists("error", $_GET)) {
         // find out what happened:
         $error_description = $_GET['error_description']; // "User Cancelled"
         exit($error_description);
        }


        echo $checkoutId = $_GET['checkoutId'];
        echo "<br/>";
        echo $amount = $_GET['amount'];
        echo "<br/>";
        echo $signature = $_GET['signature'];
        echo "<br/>";
        echo $orderid = $_GET['orderId'];
        echo "<br/>";

        $sigantureValid = $this->verifyGatewaySignature($signature, $checkoutId, $amount);

        if (!$sigantureValid) {
         exit("Bad signature!");
        }

        $status = $_GET['status'];

        if ($status == "Completed") {
         // do something useful with the checkout results:
         echo $status;
        }
     }
}
