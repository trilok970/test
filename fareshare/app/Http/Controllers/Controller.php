<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function getDistance($fromLat, $fromLon, $toLat, $toLon)
    {

            $key = env('GOOGLE_MAP_KEY');

            $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$fromLat.','.$fromLon.'&destinations='.$toLat.'%2C'.$toLon.'&mode=driving&key='.$key;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $response_a = json_decode($response, true);
            $status = $response_a['rows'][0]['elements'][0]['status'];
            if ( $status == 'ZERO_RESULTS' || $status == 'NOT_FOUND' )
            {
                return null;
            }
            $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
            if($dist && $time) {
                return (float) $dist;
            } else {
                return null;
            }

    }

    public function getTime($fromLat, $fromLon, $toLat, $toLon)
    {

        $key = env('GOOGLE_MAP_KEY');

            $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$fromLat.','.$fromLon.'&destinations='.$toLat.'%2C'.$toLon.'&mode=driving&key='.$key;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $response_a = json_decode($response, true);
            $status = $response_a['rows'][0]['elements'][0]['status'];
            if ( $status == 'ZERO_RESULTS' || $status == 'NOT_FOUND' )
            {
                return null;
            }
            $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
            if($dist && $time) {
                return $time. " away";
            } else {
                return null;
            }

    }

    function validationHandle($validation)
    {
        foreach ($validation->getMessages() as $field_name => $messages){
            if(!isset($firstError)){

                $firstError =$messages[0];
            }
        }
        return $firstError;
    }

}
