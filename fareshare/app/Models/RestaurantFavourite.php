<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class RestaurantFavourite extends Model
{
    //
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    public function rastuarant()
    {
    	return $this->belongsTo(Restaurant::class,'restaurant_id');
    }
}
