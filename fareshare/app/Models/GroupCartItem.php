<?php

namespace App\Models;

use App\Models\GroupCart;
use Illuminate\Database\Eloquent\Model;

class GroupCartItem extends Model
{
    protected $fillable = ['group_cart_id', 'item_id', 'coupon_code_id', 'qty', 'price', 'sub_total','member_id'];

    public function group_cart() {
        return $this->belongsTo(GroupCart::class);
    }
}
