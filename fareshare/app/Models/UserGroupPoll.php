<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroupPoll extends Model
{
    protected $table = 'user_group_polls';
    protected $fillable = ['creator_id','group_id','poll_max_time'];

    public function userGroup() {
        return $this->belongsTo(UserGroup::class,'group_id');
    }

    public function restaurants()
    {
        return $this->hasMany(PollRestaurants::class,'poll_id');
    }

    public function votes()
    {
        return $this->hasMany(PollVote::class,'poll_id');
    }

}
