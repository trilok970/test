<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use DB;

class Restaurant extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'restaurants';

    protected $appends = [ 'continental', 'total_reviews', 'restaurant_image','total_ratings'];

    public function getRestaurantImageAttribute($value) {
        $image = RestaurantMedia::where('restaurant_id', $this->id)->first();
        if($image) {
            return $image->image;
        } else {
            return null;
        }
    }

    public function distanceTime($restaurant_id, $lat, $lon)
    {

        $key = env('GOOGLE_MAP_KEY');
        $restaurant = Restaurant::select('U.lat','U.lon')->join('users as U', 'U.id','=','restaurants.user_id')->find($restaurant_id);

        if($restaurant->lat && $restaurant->lon && $lat && $lon) {
            $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$lat.','.$lon.'&destinations='.$restaurant->lat.'%2C'.$restaurant->lon.'&mode=driving&key='.$key;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $response_a = json_decode($response, true);
            $status = $response_a['rows'][0]['elements'][0]['status'] ?? 0;
            if ( $status == 'ZERO_RESULTS' || $status == 'NOT_FOUND' )
            {
                return array(null, null);
            }
            $dist = $response_a['rows'][0]['elements'][0]['distance']['text'] ?? 0;
            $time = $response_a['rows'][0]['elements'][0]['duration']['text'] ?? 0;
            if($dist && $time) {
                return array((float) $dist, $time. " away");
            } else {
                return array(null, null);
            }

        }
        return array(null, null);
    }

    public function getTotalReviewsAttribute()
    {
        return $this->hasOne(RestaurantRating::class)->count();
    }
    public function getTotalRatingsAttribute()
    {
        return round($this->hasOne(RestaurantRating::class)->avg('rating'),1);
    }

    public function owner(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function media()
    {
        return $this->hasMany('App\Models\RestaurantMedia');
    }

    public static function getRestaurantModel($limit, $offset, $search, $orderby,$order)
    {
        $orderby  = $orderby ? $orderby : 'id';
        $order    = $order ? $order : 'desc';

        $q        = User::where('role','vendor')->orderBy('created_at', 'desc');

        if($search && !empty($search)){
            $q->where(function($query) use ($search) {
                $query->orWhere('email', 'LIKE', $search.'%');
                $query->orWhere('user_name', 'LIKE', $search.'%');
                $query->orWhere('mobile_number', 'LIKE', $search.'%');
                $query->orWhere('city', 'LIKE', $search.'%');
                $query->orWhere('state', 'LIKE', $search.'%');
                $query->orWhere('country', 'LIKE', $search.'%');
                // $query->orWhere('address', 'LIKE', $search.'%');
            });
        }

        $response   =   $q->orderBy($orderby, $order)
                            ->offset($offset)
                            ->limit($limit)
                            ->get();

        $response   =   json_decode(json_encode($response));
        return $response;
    }

    public function restaurantContinental() {
        return $this->hasMany(RestaurantContinental::class, 'restaurant_id')->select('C.id', 'C.name')->join('continentals as C', 'C.id', '=', 'restaurant_continentals.continental_id');
    }

    /*public function getOpeningTimeFullAttribute()
    {
        return date("H:i A", strtotime($this->opening_time));
    }

    public function getClosingTimeFullAttribute()
    {
        return date("H:i A", strtotime($this->closing_time));
    }*/

    public function getContinentalAttribute()
    {
        // return RestaurantContinental::select(DB::raw('group_concat(continental_id) as continental'))->where('restaurant_id', $this->id)->first()->continental;
        return DB::table('restaurant_continentals')
        ->join('continentals', 'restaurant_continentals.continental_id', '=', 'continentals.id')
        ->select(DB::raw('group_concat(continentals.name) as continental_names'))->first()->continental_names;
    }

    public function getContinentalNamesAttribute()
    {
        return DB::table('restaurant_continentals')
        ->join('continentals', 'restaurant_continentals.continental_id', '=', 'continentals.id')
        ->select(DB::raw('group_concat(continentals.name) as continental_names'))->first()->continental_names;
    }

    public function categories($restaurant_id) {
        $categories = Category::where('status',1)->where('parent_id',0)->get();
        $categories->each(function($category, $key) use($restaurant_id) {
             $category->items = Item::where('status',1)->where('restaurant_id', $restaurant_id)->where('category_id', $category->id)->get();
        });

        return $categories;
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }
    public function RestaurantRating()
    {
        return $this->hasOne(RestaurantRating::class);
    }
    public function RestaurantFavourite()
    {
        return $this->hasOne(RestaurantFavourite::class);
    }
}
