<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DriverPartner extends Model
{
	protected $fillable = [
        'restaurant_id', 'driver_id'
    ];

    protected $table = 'driver_partners';

    public $timestamps = false;
    public function driver(){
        return $this->belongsTo(Driver::class,'driver_id');
    }
}
