<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'item_title', 'category_id', 'status','sub_category_id', 'item_price','discounted_price', 'item_description', 'coupon_code','coupon_code_percentage', 'item_type', 'status', 'restaurant_id'
    ];

    protected $appends = ['item_image', 'category_title', 'sub_category_title'];

    public function getItemImageAttribute($value) {
        $image = ItemImage::where('item_id', $this->id)->first();
        if($image) {
            return $image->image;
        } else {
            return null;
        }
    }

    public function getCategoryTitleAttribute($value) {
        $category = Category::where('id', $this->category_id)->first();
        if($category) {
            return $category->title;
        } else {
            return null;
        }
    }

    public function getSubCategoryTitleAttribute($value) {
        $category = Category::where('id', $this->sub_category_id)->first();
        if($category) {
            return $category->title;
        } else {
            return null;
        }
    }

    public static function getItemModel($limit, $offset, $search, $orderby,$order)
    {
        $orderby  = $orderby ? $orderby : 'id';
        $order    = $order ? $order : 'desc';

        $q        = Item::select('items.*', 'C.title as cat_title', 'R.user_name as restaurant_name');
        $q->leftJoin('categories as C', 'C.id', '=', 'items.category_id');
        $q->leftJoin('users as R', 'R.id', '=', 'items.restaurant_id');
        if($search && !empty($search)){
            $q->where(function($query) use ($search) {
                $query->orWhere('items.item_title', 'LIKE', $search.'%');
                $query->orWhere('R.user_name', 'LIKE', $search.'%');
                $query->orWhere('C.title', 'LIKE', $search.'%');
            });
        }

        $response   =   $q->with('restaurant')->orderBy($orderby, $order)
                            ->offset($offset)
                            ->limit($limit)
                            ->get();

        $response   =   json_decode(json_encode($response));
        return $response;
    }

    public function item_images(){
    	return $this->hasMany(ItemImage::class, 'item_id');
    }

    public function restaurant() {
        return $this->belongsTo(Restaurant::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
