<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $fillable = [
        'user_id', 'location','house_number', 'landmark', 'address_type', 'lat', 'lon', 'is_default'
    ];

    public $timestamps = false;
}
