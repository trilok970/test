<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantMedia extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'restaurant_media';
}
