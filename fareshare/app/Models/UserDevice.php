<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDevice extends Model
{
    protected $fillable = [
        'user_id', 'device_id', 'device_type', 'log_status'
    ];

    public static function deviceHandle($data){

        $userDevice = UserDevice::where(['device_type'=> $data['device_type'],'device_id'=>$data['device_id']])->first();

        if($userDevice){

            UserDevice::where(['device_type'=> $data['device_type'],'device_id'=>$data['device_id']])->delete();
            self::createDevice($data);

        }else{

            self::createDevice($data);

        }

        return true;
        
    } 
    
    public static function createDevice($data){
		
        $device = UserDevice::create([
            "user_id"       =>  $data['id'],
            "device_type"   =>  $data['device_type'],
            "device_id"  =>  $data['device_id'],
        ]);                
    }


}
