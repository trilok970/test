<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Restaurant;
use App\Models\Continental;
use App\User;

class RestaurantContinental extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'restaurant_continentals';

    public $timestamps = false;

    public function getContinentalIdAttribute($value)
    {
    	$continental = Continental::where('id', $value)->first();
        return $continental->name;
    }

    public function getRestaurantIdAttribute($value)
    {
    	$restaurant = Restaurant::where('id', $value)->first();
        return $restaurant->restaurant_name;
    }

}
