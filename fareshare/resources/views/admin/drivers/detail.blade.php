@extends('layouts.admin')

@section('content')
<?php

  // echo "<pre>";
  // print_r($driver);
  // die;
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ $page_title }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">{{ $page_title }}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
              <h3 class="card-title">{{ $page_title }} # {{ $driver->user_name }}</h3>
              </div>
              <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:25%">Username:</th>
                        <td>{{ ucfirst($driver->user_name) }}</td>
                      </tr>
                      <tr>
                        <th>Email</th>
                        <td>{{ $driver->email }}</td>
                      </tr>
                      <tr>
                        <th>Mobile No.:</th>
                        <td>{{ $driver->country_code }}-{{ $driver->mobile_number }}</td>
                      </tr>
                      <tr>
                        <th>Dob:</th>
                        <td>{{ date("d-m-Y",strtotime($driver->dob)) }}</td>
                      </tr>
                      <tr>
                        <th>SSN:</th>
                        <td>{{ $driver->ssn }}</td>
                      </tr>
                      <tr>
                        <th>Address:</th>
                        <td>{{ $driver->address }}</td>
                      </tr>
                      <tr>
                        <th>City:</th>
                        <td>{{ ucfirst($driver->city) }}</td>
                      </tr>
                      <tr>
                        <th>State:</th>
                        <td>{{ ucfirst($driver->state) }}</td>
                      </tr>
                      @if($driver->driver)
                        <tr>
                          <th>Zip:</th>
                          <td>{{ ($driver->zip) }}</td>
                        </tr>
                      @endif
                      <tr>
                        <th>Profile Picture:</th>
                        <td>
                          @if($driver->profile_picture)
                          <div class="widget-user-image">
                            <a href="{{ url($driver->profile_picture) }}" target="_blank">
                              <img class="img-circle elevation-2 img-lg" src="{{ asset($driver->profile_picture) }}" alt="{{ $driver->user_name }}">
                            </a>

                          </div>
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <th>Status:</th>
                        <td>
                          @if($driver->status == 1)
                          <span class="badge badge-success">Active</span>
                          @else
                          <span class="badge badge-danger">In Active</span>
                          @endif
                        </td>
                      </tr>
                    </table>
                  </div>
              </div>
            </div>
          </div>

          @if($driver->driver)
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
              <h3 class="card-title">{{ $page_title }} # {{ $driver->user_name }}</h3>
              </div>
              <div class="card-body">
                  <div class="table-responsive">

                    <table class="table">

                        <tr>
                            <th style="width:25%">Id:</th>
                            <td>{{ ucfirst($driver->driver->id) }}</td>
                        </tr>
                        <tr>
                            <th style="width:25%">Driver Code:</th>
                            <td>{{ ucfirst($driver->driver->driver_code) }}</td>
                          </tr>
                        <tr>
                          <th style="width:25%">Name:</th>
                          <td>{{ ucfirst($driver->driver->name) }}</td>
                        </tr>

                        <tr>
                        <th>Address : </th>
                        <td>{{ $driver->driver->address }}</td>
                        </tr>

                        <tr>
                          <th>Driving Licence Number:</th>
                          <td>{{ $driver->driver->driving_licence_number }}</td>
                        </tr>

                        <tr>
                          <th>Driving Licence Expiry Date:</th>
                          <td>{{ $driver->driver->driving_licence_expiry_date }}</td>
                        </tr>

                        <tr>
                          <th>Driving Licence Picture:</th>
                          <td>
                            @if($driver->driver->driving_licence_image)
                            <div class="widget-user-image">
                              <a href="{{ url($driver->driver->driving_licence_image) }}" target="_blank">
                                <img class="img-thumbnail elevation-2 img-lg" src="{{ asset($driver->driver->driving_licence_image) }}" alt="{{ $driver->driver->name }}">
                              </a>
                            </div>
                            @endif
                          </td>
                        </tr>

                        <tr>
                          <th>Vehicle Registration Number:</th>
                          <td>{{ ($driver->driver->vehicle_registration_number) }}</td>
                        </tr>

                        <tr>
                          <th>Vehicle Registration Expiry:</th>
                          <td>{{ ($driver->driver->vehicle_registration_expiry_date) }}</td>
                        </tr>

                        <tr>
                          <th>Vehicle Registration Picture:</th>
                          <td>
                            @if($driver->driver->vehicle_registration_image)
                            <div class="widget-user-image">
                              <a href="{{ url($driver->driver->vehicle_registration_image) }}" target="_blank">
                                <img class="img-thumbnail elevation-2 img-lg" src="{{ asset($driver->driver->vehicle_registration_image) }}" alt="{{ $driver->driver->name }}">
                              </a>
                            </div>
                            @endif
                          </td>
                        </tr>

                        <tr>
                          <th>Vehicle Insurance Number:</th>
                          <td>{{ ($driver->driver->vehicle_insurance_number) }}</td>
                        </tr>

                        <tr>
                          <th>Vehicle Insurance Expiry:</th>
                          <td>{{ ($driver->driver->vehicle_insurance_expiry_date) }}</td>
                        </tr>

                        <tr>
                          <th>Vehicle Insurance Picture:</th>
                          <td>
                            @if($driver->driver->vehicle_insurance_image)
                            <div class="widget-user-image">
                              <a href="{{ url($driver->driver->vehicle_insurance_image) }}" target="_blank">
                                <img class="img-thumbnail elevation-2 img-lg" src="{{ asset($driver->driver->vehicle_insurance_image) }}" alt="{{ $driver->driver->name }}">
                              </a>
                            </div>
                            @endif
                          </td>
                        </tr>

                        <tr>
                          <th>Vehicle Make:</th>
                          <td>{{ ($driver->driver->vehicle_make) }}</td>
                        </tr>

                        <tr>
                          <th>Vehicle Model:</th>
                          <td>{{ ($driver->driver->vehicle_model) }}</td>
                        </tr>

                        <tr>
                          <th>Vehicle Year:</th>
                          <td>{{ ($driver->driver->vehicle_year) }}</td>
                        </tr>

                        <tr>
                          <th>Vehicle Color:</th>
                          <td>{{ ($driver->driver->vehicle_color) }}</td>
                        </tr>

                    </table>

                  </div>
              </div>
            </div>
          </div>
          @endif
      </div>

    </section>
    <!-- /.content -->
  </div>
@endsection

@section('styles')

@endsection

@section('scripts')

<script type="text/javascript">
$(document).ready(function () {



});
</script>
@endsection
