@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ $page_title }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ url('/admin/cms') }}">Cms</a></li>
              <li class="breadcrumb-item active">{{ $page_title }}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    	<form action="{{ route('admin.cms.update',[$cms->id]) }}" method="post" id="cmsForm">
         	@csrf
         	@method('PUT')
      <div class="row">
        <div class="col-12">
        	<div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">{{ $page_title }}</h3>
            </div>
            
            <div class="card-body">
            	@include('admin.partials.alert_msg')
              <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control @error('email') is-invalid @enderror" value="{{ old('title', $cms->title) }}">
                @error('title')
                    <span class="invalid-feedback">{{ $message }}</span>
                @enderror
              </div>
              <div class="form-group">
                <label for="slug">Slug</label>
                <input type="text" name="slug" id="slug" class="form-control @error('slug') is-invalid @enderror" value="{{ old('slug', $cms->slug) }}">
                @error('slug')
                    <span class="invalid-feedback">{{ $message }}</span>
                @enderror
              </div>
              <div class="form-group">
                <label for="content">Project Description</label>
                <textarea name="content" id="content" class="textarea @error('content') is-invalid @enderror" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('content', $cms->content) }}</textarea>
                @error('content')
                    <span class="invalid-feedback">{{ $message }}</span>
                @enderror
              </div>
              <div class="form-group">
                <label for="status">Status</label>
                <select name="status" class="form-control custom-select @error('status') is-invalid @enderror">
                  <option selected disabled>Select one</option>
                  <option value="1" {{ old('status', $cms->status) == 1 ? 'selected' : '' }}>Active</option>
                  <option value="0" {{ old('status', $cms->status) == 0 ? 'selected' : '' }}>In Active</option>
                </select>
                @error('status')
                    <span class="invalid-feedback">{{ $message }}</span>
                @enderror
              </div>
            </div>
            <!-- /.card-body -->
        	
          </div>  
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <a href="{{ url('/admin/cms') }}" class="btn btn-secondary">Cancel</a>
          <input type="submit" value="Save Changes" class="btn btn-success float-right">
        </div>
      </div>
      </form>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('styles')
<!-- summernote -->
  <link rel="stylesheet" href="{{ asset('public/admin/plugins/summernote/summernote-bs4.css')}}">
@endsection

@section('scripts')
<!-- Summernote -->
<script src="{{ asset('public/admin/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- jquery-validation -->
<script src="{{ asset('public/admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('public/admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function () {
  
  $('.textarea').summernote();

  $('#cmsForm').validate({
    rules: {
      title: {
        required: true,
      },
      slug: {
        required: true,
      },
      content: {
        required: true,
        minlength: 50
      }
    },
    messages: {
      title: {
        required: "Please enter a title",
      },
      slug: {
        required: "Please enter a slug",
      },
      content: {
        required: "Please provide a content",
        minlength: "Your content must be at least 50 characters long"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  
});
</script>
@endsection
