<a class="btn btn-primary btn-sm" href="{{ url('admin/cms-detail',$id)}}">
  View
</a>

<a class="btn btn-info btn-sm" href="{{ url('admin/cms/edit',$id)}}">
  Edit
</a>

@if($status == 0)
<a class="btn btn-success btn-sm active-button" href="#" data-id="{{ $id }}">
  Active
</a>
@else
<a class="btn btn-danger btn-sm inactive-button" href="#" data-id="{{ $id }}">
  In Active
</a>
@endif
