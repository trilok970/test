<div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('public/admin/img/avatar5.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->user_name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('admin.dashboard') }}" class="nav-link {{ Request::is('admin/dashboard') ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview {{ Request::is('admin/profile') || Request::is('admin/change-password') ? 'menu-open' : '' }}">
            <a href="#" class="nav-link {{ Request::is('admin/profile') || Request::is('admin/change-password') ? 'active' : '' }}">
              <i class="nav-icon ion ion-person-add"></i>
              <p>
                Admin
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin.profile')}}" class="nav-link {{ Request::is('admin/profile') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Profile</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.change.password')}}" class="nav-link {{ Request::is('admin/change-password') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Change Password</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.user.list') }}" class="nav-link {{ Request::is('admin/user-list') ? 'active' : '' }}">
              <i class="nav-icon fa fa-users" aria-hidden="true"></i>
              <p>
                User List
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.vendor.list') }}" class="nav-link {{ Request::is('admin/vendor-list') ? 'active' : '' }}">
              <i class="nav-icon fa fa-id-badge" aria-hidden="true"></i>
              <p>
                Vendors
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.driver.list') }}" class="nav-link {{ Request::is('admin/driver-list') ? 'active' : '' }}">
              <i class="nav-icon fa fa-id-card" aria-hidden="true"></i><i class=""></i>
              <p>
                Drivers
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ url('admin/orders') }}" class="nav-link {{ Request::is('admin/orders') ? 'active' : '' }}">
              <i class="nav-icon fab fa-first-order" aria-hidden="true"></i>
              <p>
                Orders
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.categories') }}" class="nav-link {{ Request::is('admin/categories') ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt" aria-hidden="true"></i>
              <p>
                Category List
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.items') }}" class="nav-link {{ Request::is('admin/items') ? 'active' : '' }}">
              <i class="nav-icon fas fa-clipboard-list" aria-hidden="true"></i>
              <p>
                Item List
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.user-group') }}" class="nav-link {{ Request::is('admin/user-group') ? 'active' : '' }}">
              <i class="nav-icon fas fa-users" aria-hidden="true"></i>
              <p>
                User Groups
              </p>
            </a>
          </li>


          <li class="nav-item has-treeview {{ Request::is('admin/walletpercentage') || Request::is('admin/cms') ? 'menu-open' : '' }}{{  Request::is('admin/settings') ? 'menu-open' : '' }} ">
            <a href="#" class="nav-link {{ Request::is('admin/walletpercentage') || Request::is('admin/cms') ? 'active' : ''   }}{{ Request::is('admin/settings') ? 'active' : '' }}">
              <i class="nav-icon ion ion-settings"></i>
              <p>
                Setting
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{route('admin.settings')}}" class="nav-link {{ Request::is('admin/settings') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Setting</p>
                    </a>
                  </li>
              <li class="nav-item">
                <a href="{{url('admin/walletpercentage')}}" class="nav-link {{ Request::is('admin/walletpercentage') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Wallet Setting</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.cms.index')}}" class="nav-link {{ Request::is('admin/cms') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>CMS Pages</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>Logout</p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </li>


        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
