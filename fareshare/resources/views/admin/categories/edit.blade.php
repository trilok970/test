@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ $page_title }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ url('/admin/categories') }}">Category</a></li>
              <li class="breadcrumb-item active">{{ $page_title }}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    	<form action="{{ route('admin.categories.update',[$category->id]) }}" method="post" id="cmsForm">
         	@csrf
         	@method('PUT')
      <div class="row">
        <div class="col-12">
        	<div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">{{ $page_title }}</h3>
            </div>
            
            <div class="card-body">
            	@include('admin.partials.alert_msg')
              <div class="form-group">
                <label for="parent_id">Parent Category</label>
                {!! Form::select('parent_id', $categories,$category->parent_id, ['class'=>'form-control', 'placeholder' => 'Select Parent Category']) !!}
                @error('parent_id')
                    <span class="invalid-feedback">{{ $message }}</span>
                @enderror
              </div>

              <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control @error('email') is-invalid @enderror" value="{{ old('title', $category->title) }}">
                @error('title')
                    <span class="invalid-feedback">{{ $message }}</span>
                @enderror
              </div>
              
            <!-- /.card-body -->
        	
          </div>
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="{{ url('/admin/categories') }}" class="btn btn-secondary">Cancel</a>
          </div>  
        </div>
      </div>
      
      </form>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('styles')
<!-- summernote -->
  <link rel="stylesheet" href="{{ asset('public/admin/plugins/summernote/summernote-bs4.css')}}">
@endsection

@section('scripts')
<!-- Summernote -->
<script src="{{ asset('public/admin/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- jquery-validation -->
<script src="{{ asset('public/admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('public/admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function () {
  
  $('.textarea').summernote();

  $('#cmsForm').validate({
    rules: {
      title: {
        required: true,
      }
    },
    messages: {
      title: {
        required: "Please enter a title",
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  
});
</script>
@endsection
