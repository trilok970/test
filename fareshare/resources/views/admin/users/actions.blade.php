<a class="btn btn-primary btn-xs" href="{{ url('admin/user-detail',$id)}}">
  View
</a>

@if($status == 0)
<a class="btn btn-success btn-xs active-button" href="#" data-id="{{ $id }}">
  Active
</a>
@else
<a class="btn btn-danger btn-xs inactive-button" href="#" data-id="{{ $id }}">
  In Active
</a>
@endif
