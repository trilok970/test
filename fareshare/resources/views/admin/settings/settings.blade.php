@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ $page_title }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('admin.settings') }}">{{ $page_title }}</a></li>
              <li class="breadcrumb-item active">Update</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">{{ $page_title }}</h3>
                </div>
                {!! Form::open(['url' => route('admin.settings'),'class'=>'form-horizontal validate', 'enctype'=>'multipart/form-data']) !!}
                {{ csrf_field() }}
                <div class="card-body">
                    @include('admin.partials.alert_msg')


                    <?php $i = 0;

                    foreach($site_settings as $key => $setting) {
                        $form_div = 0;
                        if($i==0 || $i%2 == 0) {

                    ?>
                        <div class="form-group">
                    <?php } ?>

                            <div class="col-md-6">
                                <label class="control-label" for="<?= $setting->slug ?>"><?= $setting->name ?><span class="required">*</span>
                                </label>
                                <div class="">
                                    <?php if($setting->type == 'TEXT') { ?>
                                    {!! Form::text($setting->slug, $setting->value, ['class'=>'form-control', 'placeholder'=>$setting->name,'required'=>true]) !!}
                                    <?php } else if($setting->type == 'NUMBER') { ?>
                                    {!! Form::number($setting->slug, $setting->value, ['class'=>'form-control', 'placeholder'=>$setting->name,'required'=>true,'step' => '1','min'=>0,'pattern'=> '^\d*(\.\d{0,2})?$']) !!}
                                    <?php } else if($setting->type == 'FILE') { ?>
                                        {!! Form::file($setting->slug); !!}
                                        <?php
                                        if (!empty($setting->value)) {
                                            ?>
                                            <br><br>
                                            <img src="<?= asset($setting->value) ?>" style="max-width: 200px;" />
                                            <?php
                                        }
                                        ?>
                                    <?php }?>
                                </div>
                            </div>
                            <?php

                            if($i%2 != 0) {
                            ?>
                        </div>
                        <?php } ?>
                    <?php
                        $i++;
                    }
                    ?>
                <!-- /.card-body -->

              </div>
              <div class="card-footer">
                <?= Form::submit('Update', ['class' => 'btn btn-primary ']) ?>

                <a href="{!! route('admin.dashboard') !!}" class="btn btn-secondary">Cancel</a>
              </div>
              {{ Form::close() }}

            </div>
          </div>

    </section>
    <!-- /.content -->
  </div>
@endsection

@section('styles')
<!-- summernote -->
  <link rel="stylesheet" href="{{ asset('public/admin/plugins/summernote/summernote-bs4.css')}}">
@endsection

@section('scripts')
<!-- Summernote -->
<script src="{{ asset('public/admin/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- jquery-validation -->
<script src="{{ asset('public/admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('public/admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function () {

  $('.textarea').summernote();

  $('#cmsForm').validate({
    rules: {
      percentage: {
        required: true,
      }
    },
    messages: {
      title: {
        required: "Please enter a percentage value",
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });

});
</script>
@endsection
