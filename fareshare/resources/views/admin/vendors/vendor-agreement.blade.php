<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>{{ $page_title }}</title>




</head>

<body>



  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3 align="center" class="text-center"> {{ $page_title }}</h3>

<p>This Vendor Agreement (the “Agreement”) is entered into as of _________ __, 20____ (the “Effective Date”) by and between _________, a _______ [individual, corporation/limited liability company/partnership/etc.] (the “Vendor” or “You”), and Fai Share, LLC, a limited liability company (the “Company,” and together with the Vendor referred to as the “Parties”).</p>

<h3 align="center" class="text-center">ABOUT THE FAIR SHARE PLATFORM</h3>

<p>Company has created an app based platform which connects its paying Users to participating restaurants or “Vendors” to order food and beverages for pick-up and delivery provided by third party Drivers.</p>

<p>The Fair Share platform allows you to upload your take out menu so that your restaurant and your menu is made available to our Users. Our Users may place orders, pay for those orders and have the orders picked up and delivered by a third party affiliated Driver all through our app.</p>

<p>As a Vendor you will be relayed the information to fulfill the order and you will make the order available for pick-up and delivery.  We will collect all payments and will tack on our service fees and Driver fees to your menu charges to be paid by the User. We will transmit your payment to you after payment is received from User.</p>

<p>The platform features a Vendor “Wallet” wherein your payments are maintained until such time as you choose to cash it out.</p>

<p>Whereas the Vendor wishes to participate and be featured on the Fair Share platform, and the Vendor wishes to pay the Company for its services, and for other good consideration, the Parties hereby agree as follows:</p>

<h3>1. ENGAGEMENT SERVICES</h3>

<h4>a. Engagement</h4><p> The Vendor retains the Company to provide, and the Company shall provide, the following services: Once the Vendor information including text and photos, is uploaded to the Fair Share app it will be included in a catalog of Vendors which will be made available to our Users.  When Orders are placed then Company will relay the Order information to Vendor in order to be fulfilled. A third party Driver will be give pertinent information in order to pick up the User’s Order and deliver it to them.</p>

<h4>b. Vendor Obligations.</h4><p> Vendor is responsible for maintaining an up to date menu on the app. Vendor is responsible for accurately and promptly fulfilling orders and making same available to the third party Drivers. Orders shall be packaged in a professional and safe using quality packaging materials manner ensuring the the freshness and temperature of the food.</p>

<h4>c. Company obligations.</h4><p> Company is responsible for relaying accurate information to the Vendor in a timely fashion; Company is responsible for transmitting payment to Vendor through the Wallet feature once orders have been fulfilled.</p>


<h3>2.	FAIR SHARE FEES</h3><p> Vendor shall pay Company a flat fee of $__________ per month in order to be featured on the platform.  Company will also be collecting a service fee/driver fee from the User which will be added to the menu prices available to the Users. This additional fee will be agreed upon by the parties.</p>

<h3>3.	PAYMENTS</h3><p> Company will transfer your payment to your “Wallet” featured on the platform once payment is received from User.  Under no circumstances will Vendor be compensated until Company receives payment from Users.</p>

<h3>4.	CANCELLATIONS</h3><p> If the User cancels their Order prior to delivery then no compensation will be earned.</p>

<h3>5.	TERM AND TERMINATION</h3>

<h4>a. Term</h4><p> This agreement will become effective once it is executed by Vendor. This agreement will continue until the Services have been satisfactorily completed and the Contractor has been paid in full for such Services.</p>

<h4>b. Termination</h4><p> This agreement may be terminated:</p>

<p>(1) by either party on provision of 3 days’ written notice to the other party, with or without cause;</p>

<p>(2)	by either party for a material breach of any provision of this agreement by the other party, if the other party’s material breach is not cured within 3 days of receipt of written notice of the breach;</p>

<p>(3)	automatically, on the death of the Contractor. </p>

<h4>c. Effect of Termination</h4><p> After the termination of this agreement for any reason, the Company shall promptly pay the Contractor for Services rendered before the effective date of the termination. No other compensation, of any nature or type, will be payable after the termination of this agreement.</p>



<h3>6.	NATURE OF RELATIONSHIP </h3>

<h4>d. Independent Contractor Status. </h4>

<p>(a) The relationship of the parties under this agreement is one of independent Contractors, and no joint venture, partnership, agency, employer-employee, or similar relationship is created in or by this agreement. Neither party may assume or create obligations on the other party’s behalf, and neither party may take any action that creates the appearance of such authority.</p>

<p>(b) The Vendor has the right to accept or refuse Delivery Jobs in their sole discretion. </p>

<p>(c) The Vendor will be responsible for their own transportation to and from Delivery Jobs. </p>


<h3>7.	INDEMNIFICATION</h3>

<p>e. Of Company by Vendor. At all times after the effective date of this agreement, the Contractor shall indemnify the Company and its members, managers, owners, successors, and assigns (collectively, the “Company Indemnitees”) from all damages, liabilities, expenses, claims, or judgments (including interest, penalties, reasonable attorneys’ fees, accounting fees, and expert witness fees) (collectively, the “Claims”) that any Company Indemnitee may incur and that arise from:</p>

    <p>A. the Vendor’s gross negligence or willful misconduct arising from the Contractor’s carrying out of their obligations under this agreement;</p>

    <p>B. the Vendor’s breach of any of their obligations or representations under this agreement; or</p>

    <p>C. the Vendor’s breach of their express representation that he/she is an independent contractor and in compliance with all applicable laws related to work as an independent contractor. If a regulatory body or court of competent jurisdiction finds that the Vendor is not an independent contractor or is not in compliance with applicable laws related to work as an independent contractor, based on the Vendor’s own actions, the Vendor will assume full responsibility and liability for all taxes, assessments, and penalties imposed against the Vendor or the Company resulting from that contrary interpretation, including taxes, assessments, and penalties that would have been deducted from the Vendor’s earnings if the Vendor had been on the Company’s payroll and employed as a Company employee.</p>

<h3>8.	ASSIGNMENT</h3>

<p>Neither party may assign this agreement or delegate any of their duties to any third party without the written consent of the other party. </p>

<h3>9.	SUCCESSORS AND ASSIGNS</h3>

<p>All references in this Agreement to the Parties shall be deemed to include, as applicable, a reference to their respective successors and assigns. The provisions of this Agreement shall be binding on and shall insure to the benefit of the successors and assigns of the Parties.  </p>

<h3>10.	 CHOICE OF LAW</h3>

<p>The parties agree that the laws of the State of New Jersey shall govern any dispute relating to this Agreement and further agree to submit themselves to the exclusive jurisdiction of the Courts of the State of New Jersey with regard to the adjudication of any disputes resulting therefrom.</p>


<h3>SIGNATURE AND DATE</h3>

<p>Signature : @if($user->sign)<img src="{{$user->sign}}" width="100" height="100"/>@endif</p>
<p>Date : {{ date('d/m/Y', strtotime($user->created_at)) }}  </p>

      </div>
    </div>
  </div>



</body>

</html>
