@extends('layouts.admin')

@section('content')
<?php
  // echo "<pre>";
  // print_r($restaurant);
  // die;
  // echo $restaurant->restaurant_picture;die;
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ $page_title }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">{{ $page_title }}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ $page_title }} #{{ $restaurant->user_name }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <!-- /.col -->
                <div class="col-12">
                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:25%">Restaurant Name:</th>
                        <td>{{ ucfirst($restaurant->restaurant->restaurant_name) }}</td>
                      </tr>
                      <tr>
                        <th>Continental</th>
                        <td>
                            @isset($restaurant->restaurant->continental)
                                @php
                                    $continentals =explode(",",$restaurant->restaurant->continental);
                                    $items = App\Models\Continental::whereIn('id',$continentals)->pluck('name');
                                    echo $items->join(", ");
                                @endphp

                            @endisset
                      </tr>
                      <tr>
                        <th>Opening Time.:</th>
                        <td>{{ Carbon\Carbon::parse($restaurant->restaurant->opening_time)->format('H:i A') }}</td>
                      </tr>
                      <tr>
                        <th>Closing Time.:</th>
                        <td>{{ Carbon\Carbon::parse($restaurant->restaurant->closing_time)->format('h:i A') }}</td>
                      </tr>
                      <tr>
                        <th>landmark:</th>
                        <td>{{ $restaurant->restaurant->land_mark }}</td>
                      </tr>
                      <tr>
                        <th>Address:</th>
                        <td>{{ $restaurant->address }}</td>
                      </tr>
                      <tr>
                        <th>Restaurant Type:</th>
                        <td>{{ $restaurant->restaurant->restaurant_type }}</td>
                      </tr>
                      <tr>
                        <th>Profile Picture:</th>
                        <td>
                          @if($restaurant->restaurant->restaurant_name)
                          <div class="widget-user-image">
                            <a href="{{ url($restaurant->restaurant->restaurant_picture) }}" target="_blank">
                              <img class="img-thumbnail elevation-2 img-lg" src="{{ url($restaurant->restaurant->restaurant_picture) }}" alt="{{ $restaurant->restaurant->restaurant_name }}">
                            </a>
                          </div>
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <th>Other Media:</th>
                        <td>
                          <div class="row">
                            <?php foreach ($restaurant->restaurantMedia as $key => $value) {  ?>
                            <div class="col-md-4">
                              <div class="thumbnail">
                                <a href="{{ url($value->image) }}" target="_blank">
                                  <img src="{{ url($value->image) }}" class="img-thumbnail" alt="Image" style="width:100%">
                                </a>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <th>Status:</th>
                        <td>
                          @if($restaurant->status == 1)
                          <span class="badge badge-success">Active</span>
                          @else
                          <span class="badge badge-danger">In Active</span>
                          @endif
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('styles')

@endsection

@section('scripts')

<script type="text/javascript">
$(document).ready(function () {



});
</script>
@endsection
