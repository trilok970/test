@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ $page_title }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">{{ $page_title }}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    @php
    function dateFormat($date)
    {
        return date("d-M-Y",strtotime($date));
    }
    @endphp
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-7">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ $page_title }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              @include('admin.partials.alert_msg')
              <div class="table-responsive">
                <table class="table table-bordered table-hover">
                  <tr>
                      <th>ID</th><td>{{$order->id}}</td>
                      <th>Order Type</th><td>{{$order->order_type}}</td>
                  </tr>

                  <tr>
                      <th>Discount</th>
                      <td>{{$order->discount}}</td>
                      <th>Tax</th>
                      <td>{{$order->tax}}</td>
                  </tr>
                  <tr>
                      <th>Total Amount</th><td>{{$order->total_amount}}</td>
                      <th>Grand Total</th><td>{{$order->grand_total}}</td>
                  </tr>
                  <tr>
                    <th>Payment Type</th><td>{{$order->payment_type}}</td>
                    <th>Created At</th><td>{{dateFormat($order->created_at)}}</td>
                  </tr>
                    <tr>
                      <th>Status</th><td>{{$order->status}}</td>
                      <th>Payment Status</th><td>{{ucfirst($order->payment_status)}}</td>
                    </tr>
                    <tr>
                      <th>Order Group Type</th><td>{{$order->order_group_type}}</td>
                    </tr>
                  </tr>
                </table>
                </div>


            </div>
          </div>
        </div>

        <div class="col-lg-5">
            <div class="card">
                <div class="card-header">
                  Customer Detail
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Id</th>
                            <td>{{$order->user->id}}</td>
                            <th>Name</th>
                            <td>{{$order->user->user_name}}</td>
                        </tr>
                        <tr>
                            <th>Mobile</th>
                            <td>({{$order->user->country_code}}) {{$order->user->mobile_number}}</td>
                            <th>Email</th>
                            <td>{{$order->user->email}}</td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td>{{$order->user->address}}</td>
                            <th>City</th>
                            <td>{{$order->user->city}}</td>
                        </tr>
                        <tr>
                            <th>State</th>
                            <td>{{$order->user->state}}</td>
                            <th>Country</th>
                            <td>{{$order->user->country}}</td>
                        </tr>
                        <tr>
                            <th>Zipcode</th>
                            <td>{{$order->user->zip}}</td>
                        </tr>
                    </table>
                    </div>
                </div>
            </div>
        </div>

         <!-- /.row -->

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><b>Order Items</b></div>
                    <div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <tr>
                                <th>Item Id</th>
                                <th>Item Name</th>
                                <th>Restaurant</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Subtotal</th>
                            </tr>
                            @foreach ($order->items as $item)
                                <tr>
                                    <td>{{$item->item_id}}</td>
                                    <td>{{$item->item->item_title}}</td>
                                    <td>{{$order->restaurant->restaurant_name}}</td>
                                    <td>{{$item->qty}}</td>
                                    <td>${{$item->price}}</td>
                                    <td>${{$item->sub_total}}</td>
                                </tr>
                            @endforeach
                        </table>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><b>Payment Details</b></div>
                    <div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped">
                            <tr>
                                <th>Payment Type</th>
                                <th>{{$order->payment_type}}</th>
                            </tr>
                            <tr>
                                <th>Payment Status</th>
                                <th>{{ucfirst($order->payment_status)}}</th>
                            </tr>
                            <tr>
                                <th>Order Total</th>
                                <th>${{$order->total_amount}}</th>
                            </tr>
                            <tr>
                                <th>Tax</th>
                                <th>${{$order->tax}}</th>
                            </tr>
                            <tr>
                                <th>Discount</th>
                                <th>${{$order->discount}}</th>
                            </tr>
                            <tr>
                                <th>Grand Amount</th>
                                <th>${{$order->grand_total}}</th>
                            </tr>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><b>Restaurant Details</b></div>
                    <div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped">
                            <tr>
                                <th>Restaurant Id</th>
                                <td>{{$order->restaurant->id}}</td>
                            </tr>
                            <tr>
                                <th>Restaurant Name</th>
                                <td>{{$order->restaurant->restaurant_name}}</td>
                            </tr>
                            <tr>
                                <th>Restaurant Pic</th>
                                <td> <a target="_blank" href="{{url($order->restaurant->restaurant_picture)}}" ><img src="{{url($order->restaurant->restaurant_picture)}}" width="100" /></a></td>
                            </tr>
                            <tr>
                                <th>Opening Time</th>
                                <td>{{ Carbon\Carbon::parse($order->restaurant->opening_time)->format('H:i A') }}</td>
                            </tr>
                            <tr>
                                <th>Closing Time</th>
                                <td>{{Carbon\Carbon::parse($order->restaurant->closing_time)->format('H:i A')}}</td>
                            </tr>
                            <tr>
                                <th>Land Mark</th>
                                <td>{{$order->restaurant->lend_mark}}</td>
                            </tr>
                            <tr>
                                <th>Restaurant Type</th>
                                <td>{{$order->restaurant->restaurant_type}}</td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{{$order->restaurant->description}}</td>
                            </tr>
                            <tr>
                                <th>Rating</th>
                                <td>{{$order->restaurant->rating}}</td>
                            </tr>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            @if($order->driver_id)

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><b>Driver Details</b></div>
                    <div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped">
                            <tr>
                                <th>Id</th>
                                <td>{{$order->driver->user->id}}</td>
                                <th>Name</th>
                                <td>{{$order->driver->user->user_name}}</td>
                            </tr>
                            <tr>
                                <th>Mobile No.</th>
                                <td>({{$order->driver->user->country_code}}) {{$order->driver->user->mobile_number}}</td>
                                <th>Pic</th>
                                <td>@if($order->driver->user->profile_picture) <a target="_blank" href="{{url($order->driver->user->profile_picture)}}" ><img src="{{url($order->driver->user->profile_picture)}}" width="100" /></a>@endif</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$order->driver->user->email}}</td>
                                <th>Address</th>
                                <td>{{$order->driver->user->address}}</td>
                            </tr>
                            <tr>
                                <th>City</th>
                                <td>{{$order->driver->user->city}}</td>
                                <th>State</th>
                                <td>{{$order->driver->user->state}}</td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td>{{$order->driver->user->country}}</td>
                                <th>Zipcode</th>
                                <td>{{$order->driver->user->zip}}</td>
                            </tr>
                            @if($order->driver)
                            <tr>
                                <th>Driver Code</th>
                                <td>{{$order->driver->driver_code}}</td>
                                <th>Driving Licence Number</th>
                                <td>{{$order->driver->driving_licence_number}}</td>
                            </tr>
                            <tr>
                                <th>Driving Licence Expiry Date</th>
                                <td>{{dateFormat($order->driver->driving_licence_expiry_date)}}</td>
                                <th>Driving Licence Image</th>
                                <td>@if($order->driver->driving_licence_image) <a target="_blank" href="{{url($order->driver->driving_licence_image)}}" ><img src="{{url($order->driver->driving_licence_image)}}" width="100" /></a>@endif</td>
                            </tr>
                            <tr>
                                <th>Vehicle Registration Number</th>
                                <td>{{$order->driver->vehicle_registration_number}}</td>
                                <th>Vehicle Registration Expiry Date</th>
                                <td>{{dateFormat($order->driver->vehicle_registration_expiry_date)}}</td>
                            </tr>
                            <tr>
                                <th>Vehicle Registration Image</th>
                                <td>@if($order->driver->vehicle_registration_image) <a target="_blank" href="{{url($order->driver->vehicle_registration_image)}}" ><img src="{{url($order->driver->vehicle_registration_image)}}" width="100" /></a>@endif</td>
                                <th>Vehicle Insurance Number</th>
                                <td>{{$order->driver->vehicle_insurance_number}}</td>
                            </tr>
                            <tr>
                                <th>Vehicle Insurance Expiry Date</th>
                                <td>{{dateFormat($order->driver->vehicle_insurance_expiry_date)}}</td>
                                <th>vehicle_insurance_image</th>
                                <td>@if($order->driver->vehicle_insurance_image) <a target="_blank" href="{{url($order->driver->vehicle_insurance_image)}}" ><img src="{{url($order->driver->vehicle_insurance_image)}}" width="100" /></a>@endif</td>
                            </tr>
                            <tr>
                                <th>Vehicle Make</th>
                                <td>{{$order->driver->vehicle_make}}</td>
                                <th>Vehicle Model</th>
                                <td>{{$order->driver->vehicle_model}}</td>
                            </tr>
                            <tr>
                                <th>Vehicle Year</th>
                                <td>{{$order->driver->vehicle_year}}</td>
                                <th>Vehicle Color</th>
                                <td>{{$order->driver->vehicle_color}}</td>
                            </tr>
                            @endif

                        </table>
                        </div>
                    </div>
                </div>
            </div>
            @endif



      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection



@section('scripts')


<script type="text/javascript">
$(document).ready(function () {





});
</script>
@endsection
