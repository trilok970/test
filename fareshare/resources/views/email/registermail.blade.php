<style type="text/css">
  body,
  html, 
  .body {
    background: #f3f3f3 !important;
  }
  .header {
    background: #f3f3f3;
  }
</style>
<!-- move the above styles into your custom stylesheet -->

<spacer size="16"></spacer>

<container>

  <row class="header">
    <columns>

      <spacer size="16"></spacer>
      
      <h2 class="text-center">Hi, {{ $data['name'] }}</h2>
    </columns>
  </row>
  <row>
    <columns>

      <h1 class="text-center">Account Activation</h1>
      
      <spacer size="16"></spacer>

      <p class="text-center">Click the link below to activate your account.</p>
        <a href="{{ $data['base_url'] }}/account-confirmation/?token={{ $data['token'] }}" style="background-color:#40b0ed; color:#FFFFFF; padding:10px 25px; display:inline-block; text-decoration:none; margin-top:15px; margin-bottom:25px; margin-top:20px; border-radius:3px;">
            Activate
        </a>

    </columns>
  </row>

  <spacer size="16"></spacer>
</container>